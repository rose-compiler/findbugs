package edu.umd.cs.findbugs.ba.vna;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004,2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.vna.*;
/**
 * Object which stores which fields are loaded and stored by the instructions in
 * a method (including through inner-class access methods), and also which
 * fields are loaded/stored by the overall method. The main purpose is for doing
 * redundant load elimination and forward substitution more efficiently, but it
 * might be useful in other situations.
 * 
 * @author David Hovemeyer
 */
/**
     * Count number of times a field is loaded and/or stored in the method.
     */
/** Get the number of times the field is loaded. */
/** Get the number of times the field is stored. */
// Fields
// private MethodGen methodGen;
/**
     * Constructor. Constructs an empty object.
     * 
     * @param methodGen
     *            the method being analyzed for loads/stores
     */
// this.methodGen = methodGen;
/**
     * Get the number of times given field is loaded and stored within the
     * method.
     * 
     * @param field
     *            the field
     * @return the load/store count object
     */
/**
     * Add a load of given field at given instruction.
     * 
     * @param handle
     *            the instruction
     * @param field
     *            the field
     */
/**
     * Add a store of given field at given instruction.
     * 
     * @param handle
     *            the instruction
     * @param field
     *            the field
     */
/**
     * Get the field loaded or stored at given instruction, if any.
     * 
     * @param handle
     *            the instruction
     * @return the field loaded or stored at the instruction, or null if the
     *         instruction is not a load or store
     */
/**
     * Return whether or not the given field is loaded by any instruction in the
     * method.
     * 
     * @param field
     *            the field
     * @return true if the field is loaded somewhere in the method, false if it
     *         is never loaded
     */
/**
     * Return whether or not the given instruction is a load.
     * 
     * @param handle
     *            the instruction
     * @return true if the instruction is a load, false if not
     */
// vim:ts=4
abstract interface package-info {
}
