/**
 * Unpacked code for a method. Contains set of all opcodes in the method, as
 * well as a map of bytecode offsets to opcodes.
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import edu.umd.cs.findbugs.ba.MethodBytecodeSet;
public class UnpackedCode extends java.lang.Object {
    private edu.umd.cs.findbugs.ba.MethodBytecodeSet bytecodeSet;
    private short[] offsetToBytecodeMap;
    public UnpackedCode(edu.umd.cs.findbugs.ba.MethodBytecodeSet bytecodeSet, short[] offsetToBytecodeMap) {
        super();
        this.bytecodeSet = bytecodeSet;
        this.offsetToBytecodeMap = offsetToBytecodeMap;
    }
/**
     * @return Returns the bytecodeSet.
     */
    public edu.umd.cs.findbugs.ba.MethodBytecodeSet getBytecodeSet() {
        return bytecodeSet;
    }
/**
     * @return Returns the offsetToBytecodeMap.
     */
    public short[] getOffsetToBytecodeMap() {
        return offsetToBytecodeMap;
    }
}
