/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An edge in the InheritanceGraph. Source vertex is the subtype, target vertex
 * is a direct supertype.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.ch;
import edu.umd.cs.findbugs.ba.ch.*;
import edu.umd.cs.findbugs.graph.AbstractEdge;
public class InheritanceEdge extends edu.umd.cs.findbugs.graph.AbstractEdge<edu.umd.cs.findbugs.ba.ch.InheritanceEdge, edu.umd.cs.findbugs.ba.ch.ClassVertex> {
    public InheritanceEdge(edu.umd.cs.findbugs.ba.ch.ClassVertex source, edu.umd.cs.findbugs.ba.ch.ClassVertex target) {
        super(source,target);
    }
}
