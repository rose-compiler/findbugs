/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A BugCode is an abbreviation that is shared among some number of BugPatterns.
 * For example, the code "HE" is shared by all of the BugPatterns that represent
 * hashcode/equals violations.
 * 
 * @author David Hovemeyer
 * @see BugPattern
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
public class BugCode extends java.lang.Object implements java.lang.Comparable<edu.umd.cs.findbugs.BugCode> {
    final private java.lang.String abbrev;
    final private int cweid;
    final private java.lang.String description;
/**
     * Constructor.
     * 
     * @param abbrev
     *            the abbreviation for the bug code
     * @param description
     *            a short textual description of the class of bug pattern
     *            represented by this bug code
     */
    public BugCode(java.lang.String abbrev, java.lang.String description) {
        super();
        this.abbrev = abbrev;
        this.description = description;
        this.cweid = 0;
    }
    public BugCode(java.lang.String abbrev, java.lang.String description, int cweid) {
        super();
        this.abbrev = abbrev;
        this.description = description;
        this.cweid = cweid;
    }
/**
     * Get the abbreviation for this bug code.
     */
    public java.lang.String getAbbrev() {
        return abbrev;
    }
/**
     * Get the short textual description of the bug code.
     */
    public java.lang.String getDescription() {
        return description;
    }
/**
     * Get the abbreviation fo this bug code.
     */
    public java.lang.String toString() {
        return "BugCode[" + abbrev + "]";
    }
/**
     * @return Returns the cweid.
     */
    public int getCWEid() {
        return cweid;
    }
    public int compareTo(edu.umd.cs.findbugs.BugCode o) {
        return abbrev.compareTo(o.abbrev);
    }
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if ( !(o instanceof edu.umd.cs.findbugs.BugCode)) {
            return false;
        }
        edu.umd.cs.findbugs.BugCode other = (edu.umd.cs.findbugs.BugCode) (o) ;
        return abbrev.equals(other.abbrev);
    }
    public int hashCode() {
        return abbrev.hashCode();
    }
}
