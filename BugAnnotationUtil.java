/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Utility methods for BugAnnotation classes.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
abstract public class BugAnnotationUtil extends java.lang.Object {
    public BugAnnotationUtil() {
    }
/**
     * Write a BugAnnotation as XML.
     * 
     * @param xmlOutput
     *            the XMLOutput
     * @param elementName
     *            name of element for BugAnnotation
     * @param annotation
     *            the BugAnnotation
     * @param attributeList
     *            the XML attribute list
     * @param addMessages
     *            true if descriptive messages should be added
     * @throws IOException
     */
    public static void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, java.lang.String elementName, edu.umd.cs.findbugs.BugAnnotation annotation, edu.umd.cs.findbugs.xml.XMLAttributeList attributeList, boolean addMessages) throws java.io.IOException {
        edu.umd.cs.findbugs.SourceLineAnnotation src = null;
        if (annotation instanceof edu.umd.cs.findbugs.BugAnnotationWithSourceLines) src = ((edu.umd.cs.findbugs.BugAnnotationWithSourceLines) (annotation) ).getSourceLines();
        if (addMessages || src != null) {
            xmlOutput.openTag(elementName,attributeList);
            if (src != null) src.writeXML(xmlOutput,addMessages,false);
            if (addMessages) {
                xmlOutput.openTag(edu.umd.cs.findbugs.BugAnnotation.MESSAGE_TAG);
                xmlOutput.writeText(annotation.toString());
                xmlOutput.closeTag(edu.umd.cs.findbugs.BugAnnotation.MESSAGE_TAG);
            }
            xmlOutput.closeTag(elementName);
        }
        else {
            xmlOutput.openCloseTag(elementName,attributeList);
        }
    }
}
