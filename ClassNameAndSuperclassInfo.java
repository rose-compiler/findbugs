package edu.umd.cs.findbugs.classfile.analysis;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.analysis.*;
/**
 * Represents the class name, superclass name, and interface list of a class.
 * 
 * @author David Hovemeyer
 */
/**
         * @param accessFlags
         *            The accessFlags to set.
         */
/**
         * @param classDescriptor
         *            The classDescriptor to set.
         */
/**
         * @param codeBaseEntry
         *            The codeBaseEntry to set.
         */
/**
         * @param interfaceDescriptorList
         *            The interfaceDescriptorList to set.
         */
/**
         * @param superclassDescriptor
         *            The superclassDescriptor to set.
         */
/**
         * @param referencedClassDescriptorList
         *            The referencedClassDescriptorList to set.
         */
/**
     * Constructor.
     * 
     * @param classDescriptor
     *            ClassDescriptor representing the class name
     * @param superclassDescriptor
     *            ClassDescriptor representing the superclass name
     * @param interfaceDescriptorList
     *            ClassDescriptors representing implemented interface names
     * @param codeBaseEntry
     *            codebase entry class was loaded from
     * @param accessFlags
     *            class's access flags
     * @param usesConcurrency
     *            TODO
     */
/* TODO: We aren't doing anything with this */
/**
     * @return Returns the accessFlags.
     */
/**
     * @return Returns the majorVersion.
     */
/**
     * @return Returns the minorVersion.
     */
/**
     * @return Returns the classDescriptor.
     */
/**
     * @return Returns the codeBaseEntry.
     */
/**
     * @return Returns the interfaceDescriptorList.
     */
/**
     * @return Returns the called class descriptor list.
     */
/**
     * @return Returns the superclassDescriptor.
     */
abstract interface package-info {
}
