/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce UnconditionalValueDerefDataflow objects for
 * analyzed methods.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefAnalysis;
import edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefDataflow;
import edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class UnconditionalValueDerefDataflowFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefDataflow> {
/**
     * Constructor.
     */
    public UnconditionalValueDerefDataflowFactory() {
        super("unconditional value dereference analysis",edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefDataflow.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefDataflow analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.generic.MethodGen methodGen = this.getMethodGen(analysisCache,descriptor);
        if (methodGen == null) {
            throw new edu.umd.cs.findbugs.ba.MethodUnprofitableException(descriptor);
        }
        edu.umd.cs.findbugs.ba.CFG cfg = this.getCFG(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnd = this.getValueNumberDataflow(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefAnalysis analysis = new edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefAnalysis(this.getReverseDepthFirstSearch(analysisCache,descriptor), this.getDepthFirstSearch(analysisCache,descriptor), cfg, this.getMethod(analysisCache,descriptor), methodGen, vnd, this.getAssertionMethods(analysisCache,descriptor.getClassDescriptor()));
        edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow inv = this.getIsNullValueDataflow(analysisCache,descriptor);
        analysis.clearDerefsOnNonNullBranches(inv);
// XXX: hack to clear derefs on not-null branches
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = this.getTypeDataflow(analysisCache,descriptor);
        analysis.setTypeDataflow(typeDataflow);
// XXX: type analysis is needed to resolve method calls for
// checking whether call targets unconditionally dereference parameters
        edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefDataflow dataflow = new edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefDataflow(cfg, analysis);
        dataflow.execute();
        if (edu.umd.cs.findbugs.ba.ClassContext.DUMP_DATAFLOW_ANALYSIS) {
            dataflow.dumpDataflow(analysis);
        }
        if (edu.umd.cs.findbugs.ba.deref.UnconditionalValueDerefAnalysis.DEBUG) {
            edu.umd.cs.findbugs.ba.ClassContext.dumpDataflowInformation(this.getMethod(analysisCache,descriptor),cfg,vnd,inv,dataflow,typeDataflow);
        }
        return dataflow;
    }
}
