/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.util.Iterator;
import org.dom4j.DocumentException;
public class NewResults extends java.lang.Object {
    private edu.umd.cs.findbugs.SortedBugCollection origCollection;
    private edu.umd.cs.findbugs.SortedBugCollection newCollection;
    public NewResults(java.lang.String origFilename, java.lang.String newFilename) throws org.dom4j.DocumentException, java.io.IOException {
        this(new edu.umd.cs.findbugs.SortedBugCollection(),new edu.umd.cs.findbugs.SortedBugCollection());
        origCollection.readXML(origFilename);
        newCollection.readXML(newFilename);
    }
    public NewResults(edu.umd.cs.findbugs.SortedBugCollection origCollection, edu.umd.cs.findbugs.SortedBugCollection newCollection) {
        super();
        this.origCollection = origCollection;
        this.newCollection = newCollection;
    }
    public edu.umd.cs.findbugs.SortedBugCollection execute() {
        edu.umd.cs.findbugs.SortedBugCollection result = new edu.umd.cs.findbugs.SortedBugCollection();
        for (java.util.Iterator<edu.umd.cs.findbugs.BugInstance> i = newCollection.iterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.BugInstance bugInstance = i.next();
            if ( !origCollection.contains(bugInstance)) {
                result.add(bugInstance);
            }
        }
        return result;
    }
    public static void main(java.lang.String[] argv) throws java.lang.Exception {
        if (argv.length != 3) {
            java.lang.System.err.println("Usage: " + edu.umd.cs.findbugs.NewResults.class.getName() + " <orig results> <new results> <output file>");
            java.lang.System.exit(1);
        }
        java.lang.String origFilename = argv[0];
        java.lang.String newFilename = argv[1];
        java.lang.String outputFilename = argv[2];
        edu.umd.cs.findbugs.NewResults op = new edu.umd.cs.findbugs.NewResults(origFilename, newFilename);
        edu.umd.cs.findbugs.SortedBugCollection result = op.execute();
        result.writeXML(outputFilename);
    }
}
// vim:ts=4
