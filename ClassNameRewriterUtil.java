/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Utility methods for using a ClassNameRewriter.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.model;
import edu.umd.cs.findbugs.model.*;
import java.util.Iterator;
import edu.umd.cs.findbugs.FieldAnnotation;
import edu.umd.cs.findbugs.MethodAnnotation;
import edu.umd.cs.findbugs.ba.SignatureParser;
abstract public class ClassNameRewriterUtil extends java.lang.Object {
    public ClassNameRewriterUtil() {
    }
/**
     * Rewrite a method signature.
     * 
     * @param classNameRewriter
     *            a ClassNameRewriter
     * @param methodSignature
     *            a method signature
     * @return the rewritten method signature
     */
    public static java.lang.String rewriteMethodSignature(edu.umd.cs.findbugs.model.ClassNameRewriter classNameRewriter, java.lang.String methodSignature) {
        if (classNameRewriter != edu.umd.cs.findbugs.model.IdentityClassNameRewriter.instance()) {
            edu.umd.cs.findbugs.ba.SignatureParser parser = new edu.umd.cs.findbugs.ba.SignatureParser(methodSignature);
            java.lang.StringBuilder buf = new java.lang.StringBuilder();
            buf.append('\u0028');
            for (java.util.Iterator<java.lang.String> i = parser.parameterSignatureIterator(); i.hasNext(); ) {
                buf.append(rewriteSignature(classNameRewriter,i.next()));
            }
            buf.append('\u0029');
            buf.append(rewriteSignature(classNameRewriter,parser.getReturnTypeSignature()));
            methodSignature = buf.toString();
        }
        return methodSignature;
    }
/**
     * Rewrite a signature.
     * 
     * @param classNameRewriter
     *            a ClassNameRewriter
     * @param signature
     *            a signature (parameter, return type, or field)
     * @return rewritten signature with class name updated if required
     */
    public static java.lang.String rewriteSignature(edu.umd.cs.findbugs.model.ClassNameRewriter classNameRewriter, java.lang.String signature) {
        if (classNameRewriter != edu.umd.cs.findbugs.model.IdentityClassNameRewriter.instance() && signature.startsWith("L")) {
            java.lang.String className = signature.substring(1,signature.length() - 1).replace('\u002f','\u002e');
            className = classNameRewriter.rewriteClassName(className);
            signature = "L" + className.replace('\u002e','\u002f') + ";";
        }
        return signature;
    }
/**
     * Rewrite a MethodAnnotation to update the class name, and any class names
     * mentioned in the method signature.
     * 
     * @param classNameRewriter
     *            a ClassNameRewriter
     * @param annotation
     *            a MethodAnnotation
     * @return the possibly-rewritten MethodAnnotation
     */
    public static edu.umd.cs.findbugs.MethodAnnotation convertMethodAnnotation(edu.umd.cs.findbugs.model.ClassNameRewriter classNameRewriter, edu.umd.cs.findbugs.MethodAnnotation annotation) {
        if (classNameRewriter != edu.umd.cs.findbugs.model.IdentityClassNameRewriter.instance()) {
            annotation = new edu.umd.cs.findbugs.MethodAnnotation(classNameRewriter.rewriteClassName(annotation.getClassName()), annotation.getMethodName(), rewriteMethodSignature(classNameRewriter,annotation.getMethodSignature()), annotation.isStatic());
        }
        return annotation;
    }
/**
     * Rewrite a FieldAnnotation to update the class name and field signature,
     * if needed.
     * 
     * @param classNameRewriter
     *            a ClassNameRewriter
     * @param annotation
     *            a FieldAnnotation
     * @return the possibly-rewritten FieldAnnotation
     */
    public static edu.umd.cs.findbugs.FieldAnnotation convertFieldAnnotation(edu.umd.cs.findbugs.model.ClassNameRewriter classNameRewriter, edu.umd.cs.findbugs.FieldAnnotation annotation) {
        if (classNameRewriter != edu.umd.cs.findbugs.model.IdentityClassNameRewriter.instance()) {
            annotation = new edu.umd.cs.findbugs.FieldAnnotation(classNameRewriter.rewriteClassName(annotation.getClassName()), annotation.getFieldName(), rewriteSignature(classNameRewriter,annotation.getFieldSignature()), annotation.isStatic());
        }
        return annotation;
    }
}
