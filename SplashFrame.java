/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/*
 *  If long load times are getting you down, uncomment this class's instantiation in driver
 * and add an extra 4 seconds to your load time, but get to watch a dancing bug for the remainder!
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
public class SplashFrame extends javax.swing.JWindow {
// g.drawImage(new ImageIcon("bugSplash3.png"),0 ,0 ,null);
    private static class Viewer extends javax.swing.JPanel {
        private java.awt.Image image;
        private java.awt.Image image2;
        private java.awt.Image imageR;
        private java.awt.Image image2R;
        boolean swap = false;
        boolean reverse = true;
        int callCount = 0;
        int xpos = 0;
        int ypos = 0;
        int farRight;
        public Viewer(java.awt.Image i1, java.awt.Image i2, java.awt.Image i1r, java.awt.Image i2r) {
            super();
            image = i1;
            image2 = i2;
            imageR = i1r;
            image2R = i2r;
            java.awt.MediaTracker mediaTracker = new java.awt.MediaTracker(this);
            mediaTracker.addImage(image,0);
            mediaTracker.addImage(image2,1);
            mediaTracker.addImage(imageR,2);
            mediaTracker.addImage(image2R,3);
            try {
                mediaTracker.waitForID(0);
                mediaTracker.waitForID(1);
                mediaTracker.waitForID(2);
                mediaTracker.waitForID(3);
            }
            catch (java.lang.InterruptedException ie){
                java.lang.System.err.println(ie);
                java.lang.System.exit(1);
            }
            animator = new java.lang.Thread(new java.lang.Runnable() {
                public void run() {
                    int deltaX = 1;
                    while (true) {
                        if (java.lang.Thread.currentThread().isInterrupted()) return;
                        callCount++;
                        if (callCount == 10) {
                            swap =  !swap;
                            callCount = 0;
                        }
                        xpos += deltaX;
                        try {
                            java.lang.Thread.sleep(20);
                        }
                        catch (java.lang.InterruptedException e){
                            break;
                        }
                        if (xpos > edu.umd.cs.findbugs.gui2.SplashFrame.Viewer.this.getSize().width - image.getWidth(null)) {
                            deltaX =  -1;
                            reverse =  !reverse;
                        }
                        if (xpos < 0) {
                            deltaX = 1;
                            reverse =  !reverse;
                        }
                        edu.umd.cs.findbugs.gui2.SplashFrame.Viewer.this.repaint();
                    }
                }
            }, "FindBugs Splash screen thread");
            animator.setDaemon(true);
            animator.setPriority(java.lang.Thread.MIN_PRIORITY);
        }
        public void animate() {
            animator.start();
        }
        public void setPreferredSize(java.awt.Dimension d) {
            super.setPreferredSize(d);
        }
        private java.awt.Image imageToDraw() {
            if (swap) {
                if ( !reverse) return image;
                return imageR;
            }
            else {
                if ( !reverse) return image2;
                return image2R;
            }
        }
        public void paint(java.awt.Graphics graphics) {
            super.paint(graphics);
            graphics.drawImage(this.imageToDraw(),xpos,ypos,null);
        }
    }
    private static java.lang.Thread animator;
    public SplashFrame() {
        super(new java.awt.Frame());
        java.awt.Toolkit toolkit = java.awt.Toolkit.getDefaultToolkit();
        java.awt.Image image = toolkit.getImage(edu.umd.cs.findbugs.gui2.MainFrame.class.getResource("SplashBug1.png"));
        java.awt.Image image2 = toolkit.getImage(edu.umd.cs.findbugs.gui2.MainFrame.class.getResource("SplashBug2B.png"));
        java.awt.Image imageReverse = toolkit.getImage(edu.umd.cs.findbugs.gui2.MainFrame.class.getResource("SplashBug1reverse.png"));
        java.awt.Image image2Reverse = toolkit.getImage(edu.umd.cs.findbugs.gui2.MainFrame.class.getResource("SplashBug2reverseB.png"));
        javax.swing.JLabel l = new javax.swing.JLabel(new javax.swing.ImageIcon(edu.umd.cs.findbugs.gui2.MainFrame.class.getResource("umdFindbugs.png")));
        javax.swing.JPanel p = new javax.swing.JPanel();
        edu.umd.cs.findbugs.gui2.SplashFrame.Viewer viewer = new edu.umd.cs.findbugs.gui2.SplashFrame.Viewer(image, image2, imageReverse, image2Reverse);
        final javax.swing.JPanel bottom = viewer;
        p.setBackground(java.awt.Color.white);
        bottom.setBackground(java.awt.Color.white);
        p.add(l);
        this.getContentPane().add(p,java.awt.BorderLayout.CENTER);
        this.getContentPane().add(bottom,java.awt.BorderLayout.SOUTH);
        this.pack();
        java.awt.Dimension labelSize = l.getPreferredSize();
        p.setPreferredSize(new java.awt.Dimension(labelSize.width + 50, labelSize.height + 20));
        p.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.BLACK,1));
        bottom.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.BLACK,1));
        java.awt.Dimension panelSize = p.getPreferredSize();
        bottom.setPreferredSize(new java.awt.Dimension(panelSize.width, image.getHeight(null) + 2));
        this.setLocationRelativeTo(null);
        this.pack();
        viewer.animate();
    }
    public static void main(java.lang.String[] args) {
        (new edu.umd.cs.findbugs.gui2.SplashFrame()).setVisible(true);
    }
    public void setVisible(boolean b) {
        super.setVisible(b);
        if ( !b) animator.interrupt();
    }
}
// graphics.clearRect(0,0,500,500);
