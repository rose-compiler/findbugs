/** A synthetic instruction that converts a reference to a boolean value,
 * translating any nonnull value to 1 (true), and null value to 0 (false).
 * 
 */
package edu.umd.cs.findbugs.bcel.generic;
import edu.umd.cs.findbugs.bcel.generic.*;
public class NONNULL2Z extends edu.umd.cs.findbugs.bcel.generic.NullnessConversationInstruction {
    public NONNULL2Z() {
        super(org.apache.bcel.Constants.IMPDEP2);
    }
}
