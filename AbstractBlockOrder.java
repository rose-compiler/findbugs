/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Abstract base class for BlockOrder variants. It allows the subclass to
 * specify just a Comparator for BasicBlocks, and handles the work of doing the
 * sorting and providing Iterators.
 * 
 * @see BlockOrder
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
abstract public class AbstractBlockOrder extends java.lang.Object implements edu.umd.cs.findbugs.ba.BlockOrder {
    final private java.util.ArrayList<edu.umd.cs.findbugs.ba.BasicBlock> blockList;
    final private java.util.Comparator<edu.umd.cs.findbugs.ba.BasicBlock> comparator;
    public AbstractBlockOrder(edu.umd.cs.findbugs.ba.CFG cfg, java.util.Comparator<edu.umd.cs.findbugs.ba.BasicBlock> comparator) {
        super();
        this.comparator = comparator;
// Put the blocks in an array
        int numBlocks = cfg.getNumBasicBlocks();
        int count = 0;
        edu.umd.cs.findbugs.ba.BasicBlock[] blocks = new edu.umd.cs.findbugs.ba.BasicBlock[numBlocks];
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.BasicBlock> i = cfg.blockIterator(); i.hasNext(); ) {
            blocks[count++] = i.next();
        }
        assert count == numBlocks;
        java.util.Arrays.sort(blocks,comparator);
        blockList = new java.util.ArrayList<edu.umd.cs.findbugs.ba.BasicBlock>(numBlocks);
        for (int i = 0; i < numBlocks; ++i) blockList.add(blocks[i]);
    }
// Sort the blocks according to the comparator
// Put the ordered blocks into an array list
    public java.util.Iterator<edu.umd.cs.findbugs.ba.BasicBlock> blockIterator() {
        return blockList.iterator();
    }
    public int compare(edu.umd.cs.findbugs.ba.BasicBlock b1, edu.umd.cs.findbugs.ba.BasicBlock b2) {
        return comparator.compare(b1,b2);
    }
}
// vim:ts=4
