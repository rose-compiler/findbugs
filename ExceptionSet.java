package edu.umd.cs.findbugs.ba.type;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.type.*;
/**
 * Class for keeping track of exceptions that can be thrown by an instruction.
 * We distinguish <em>explicit</em> and <em>implicit</em> exceptions. Explicit
 * exceptions are explicitly declared, thrown, or caught. Implicit exceptions
 * are runtime faults (NPE, array out of bounds) not explicitly handled by the
 * user code.
 * 
 * @author David Hovemeyer
 * @see TypeAnalysis
 */
/**
     * Object to iterate over the exception types in the set.
     */
/**
     * Constructor. Creates an empty set.
     */
/**
     * Return an exact copy of this object.
     */
/**
     * Get the least (lowest in the lattice) common supertype of the exceptions
     * in the set. Returns the special TOP type if the set is empty.
     */
// This probably means that we're looking at an
// infeasible exception path.
// Compute first common superclass
// This should only happen if the class hierarchy
// is incomplete. We'll just be conservative.
// Cache and return the result
/**
     * Return an iterator over thrown exceptions.
     */
/**
     * Return whether or not the set is empty.
     */
/**
     * Checks to see if the exception set is a singleton set containing just the
     * named exception
     * 
     * @param exceptionName
     *            (in dotted format)
     * @return true if it is
     */
/**
     * Add an explicit exception.
     * 
     * @param type
     *            type of the exception
     */
/**
     * Add an implicit exception.
     * 
     * @param type
     *            type of the exception
     */
/**
     * Add an exception.
     * 
     * @param type
     *            the exception type
     * @param explicit
     *            true if the exception is explicitly declared or thrown, false
     *            if implicit
     */
/**
     * Add all exceptions in the given set.
     * 
     * @param other
     *            the set
     */
/**
     * Remove all exceptions from the set.
     */
/**
     * Return whether or not a universal exception handler was reached by the
     * set.
     */
/**
     * Mark the set as having reached a universal exception handler.
     */
/**
     * Return whether or not the set contains any checked exceptions.
     */
/**
     * Return whether or not the set contains any explicit exceptions.
     */
// vim:ts=4
abstract interface package-info {
}
