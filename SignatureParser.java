package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * A simple class to parse method signatures.
 * 
 * @author David Hovemeyer
 */
/**
     * Constructor.
     * 
     * @param signature
     *            the method signature to be parsed
     */
/**
     * Get an Iterator over signatures of the method parameters.
     * 
     * @return Iterator which returns the parameter type signatures in order
     */
/**
     * Get the method return type signature.
     * 
     * @return the method return type signature
     */
/**
     * Get the number of parameters in the signature.
     * 
     * @return the number of parameters
     */
/**
     * Determine whether or not given signature denotes a reference type.
     * 
     * @param signature
     *            a signature
     * @return true if signature denotes a reference type, false otherwise
     */
/**
     * Get the number of parameters passed to method invocation.
     * 
     * @param inv
     * @param cpg
     * @return int number of parameters
     */
/**
     * Return how many stack frame slots a type whose signature is given will
     * occupy. long and double values take 2 slots, while all other kinds of
     * values take 1 slot.
     * 
     * @param sig
     *            a type signature
     * @return number of stack frame slots a value of the given type will occupy
     */
// vim:ts=4
abstract interface package-info {
}
