/**
 * Analysis engine to produce ConstantDataflow objects for an analyzed method.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.ba.constant.ConstantAnalysis;
import edu.umd.cs.findbugs.ba.constant.ConstantDataflow;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class ConstantDataflowFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.constant.ConstantDataflow> {
    public ConstantDataflowFactory() {
        super("constant propagation analysis",edu.umd.cs.findbugs.ba.constant.ConstantDataflow.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.constant.ConstantDataflow analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.generic.MethodGen methodGen = this.getMethodGen(analysisCache,descriptor);
        if (methodGen == null) return null;
        edu.umd.cs.findbugs.ba.constant.ConstantAnalysis analysis = new edu.umd.cs.findbugs.ba.constant.ConstantAnalysis(methodGen, this.getDepthFirstSearch(analysisCache,descriptor));
        edu.umd.cs.findbugs.ba.constant.ConstantDataflow dataflow = new edu.umd.cs.findbugs.ba.constant.ConstantDataflow(this.getCFG(analysisCache,descriptor), analysis);
        dataflow.execute();
        return dataflow;
    }
}
