package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Cached data for a source file. Contains a map of line numbers to byte
 * offsets, for quick searching of source lines.
 *
 * @author David Hovemeyer
 * @see SourceFinder
 */
/**
     * Helper object to build map of line number to byte offset for a source
     * file.
     */
// if (ch < 0) throw new IllegalStateException();
// Need to see next character to know if it's a
// line terminator.
// We consider a bare CR to be an end of line
// if it is not followed by a new line.
// Mac OS has historically used a bare CR as
// its line terminator.
/**
     * Constructor.
     *
     * @param dataSource
     *            the SourceFileDataSource object which will provide the data of
     *            the source file
     */
/**
     * Get the full path name of the source file (with directory).
     */
/**
     * Get an InputStream on data.
     *
     * @return an InputStream on the data in the source file, starting from
     *         given offset
     */
/**
     * Get an InputStream on data starting at given offset.
     *
     * @param offset
     *            the start offset
     * @return an InputStream on the data in the source file, starting at the
     *         given offset
     */
/**
     * Add a source line byte offset. This method should be called for each line
     * in the source file, in order.
     *
     * @param offset
     *            the byte offset of the next source line
     */
// Grow the line number map.
/**
     * Get the byte offset in the data for a source line. Note that lines are
     * considered to be zero-index, so the first line in the file is numbered
     * zero.
     *
     * @param line
     *            the line number
     * @return the byte offset in the file's data for the line, or -1 if the
     *         line is not valid
     */
// Line 0 starts at offset 0
// Copy all of the data from the file into the byte array output
// stream
/**
     * Set the source file data.
     *
     * @param data
     *            the data
     */
// vim:ts=4
abstract interface package-info {
}
