/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba.ca;
import edu.umd.cs.findbugs.ba.ca.*;
public class Call extends java.lang.Object {
    final private java.lang.String className;
    final private java.lang.String methodName;
    final private java.lang.String methodSig;
    public Call(java.lang.String className, java.lang.String methodName, java.lang.String methodSig) {
        super();
        this.className = className;
        this.methodName = methodName;
        this.methodSig = methodSig;
    }
    public java.lang.String getClassName() {
        return className;
    }
    public java.lang.String getMethodName() {
        return methodName;
    }
    public java.lang.String getMethodSig() {
        return methodSig;
    }
    public boolean equals(java.lang.Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) return false;
        edu.umd.cs.findbugs.ba.ca.Call other = (edu.umd.cs.findbugs.ba.ca.Call) (obj) ;
        return this.className.equals(other.className) && this.methodName.equals(other.methodName) && this.methodSig.equals(other.methodSig);
    }
    public int hashCode() {
        return className.hashCode() + methodName.hashCode() + methodSig.hashCode();
    }
}
