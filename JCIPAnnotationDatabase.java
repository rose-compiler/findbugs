/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.HashMap;
import java.util.Map;
import org.apache.bcel.classfile.ElementValue;
public class JCIPAnnotationDatabase extends java.lang.Object {
    public JCIPAnnotationDatabase() {
    }
    java.util.Map<edu.umd.cs.findbugs.ba.ClassMember, java.util.Map<java.lang.String, org.apache.bcel.classfile.ElementValue>> memberAnnotations = new java.util.HashMap<edu.umd.cs.findbugs.ba.ClassMember, java.util.Map<java.lang.String, org.apache.bcel.classfile.ElementValue>>();
    java.util.Map<java.lang.String, java.util.Map<java.lang.String, org.apache.bcel.classfile.ElementValue>> classAnnotations = new java.util.HashMap<java.lang.String, java.util.Map<java.lang.String, org.apache.bcel.classfile.ElementValue>>();
    public java.lang.Object getClassAnnotation(java.lang.String dottedClassName, java.lang.String annotationClass) {
        assert dottedClassName.indexOf('\u002f') ==  -1;
        return this.getEntryForClass(dottedClassName).get(annotationClass);
    }
    public boolean hasClassAnnotation(java.lang.String dottedClassName, java.lang.String annotationClass) {
        assert dottedClassName.indexOf('\u002f') ==  -1;
        return this.getEntryForClass(dottedClassName).containsKey(annotationClass);
    }
    public java.lang.Object getFieldAnnotation(edu.umd.cs.findbugs.ba.XField field, java.lang.String annotationClass) {
        return this.getEntryForClassMember(field).get(annotationClass);
    }
    public boolean hasFieldAnnotation(edu.umd.cs.findbugs.ba.XField field, java.lang.String annotationClass) {
        return this.getEntryForClassMember(field).containsKey(annotationClass);
    }
    public java.lang.Object getMethodAnnotation(edu.umd.cs.findbugs.ba.XMethod method, java.lang.String annotationClass) {
        return this.getEntryForClassMember(method).get(annotationClass);
    }
    public boolean hasMethodAnnotation(edu.umd.cs.findbugs.ba.XMethod method, java.lang.String annotationClass) {
        return this.getEntryForClassMember(method).containsKey(annotationClass);
    }
    public java.util.Map<java.lang.String, org.apache.bcel.classfile.ElementValue> getEntryForClassMember(edu.umd.cs.findbugs.ba.ClassMember member) {
        java.util.Map<java.lang.String, org.apache.bcel.classfile.ElementValue> map = memberAnnotations.get(member);
        if (map == null) {
            map = new java.util.HashMap<java.lang.String, org.apache.bcel.classfile.ElementValue>();
            memberAnnotations.put(member,map);
        }
        return map;
    }
    public java.util.Map<java.lang.String, org.apache.bcel.classfile.ElementValue> getEntryForClass(java.lang.String dottedClassName) {
        assert dottedClassName.indexOf('\u002f') ==  -1;
        java.util.Map<java.lang.String, org.apache.bcel.classfile.ElementValue> map = classAnnotations.get(dottedClassName);
        if (map == null) {
            map = new java.util.HashMap<java.lang.String, org.apache.bcel.classfile.ElementValue>(3);
            classAnnotations.put(dottedClassName,map);
        }
        return map;
    }
}
