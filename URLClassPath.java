package edu.umd.cs.findbugs.ba;
/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * A work-alike class to use instead of BCEL's ClassPath class. The main
 * difference is that URLClassPath can load classfiles from URLs.
 * 
 * @author David Hovemeyer
 */
/**
     * Interface describing a single classpath entry.
     */
/**
         * Open an input stream to read a resource in the codebase described by
         * this classpath entry.
         * 
         * @param resourceName
         *            name of resource to load: e.g., "java/lang/Object.class"
         * @return an InputStream, or null if the resource wasn't found
         * @throws IOException
         *             if an I/O error occurs
         */
/**
         * Get filename or URL as string.
         */
/**
         * Close the underlying resource.
         */
/**
     * Classpath entry class to load files from a zip/jar file in the local
     * filesystem.
     */
/*
         * (non-Javadoc)
         * 
         * @see
         * edu.umd.cs.findbugs.URLClassPath.Entry#openStream(java.lang.String)
         */
/*
         * (non-Javadoc)
         * 
         * @see edu.umd.cs.findbugs.URLClassPath.Entry#getURL()
         */
// Ignore
/**
     * Classpath entry class to load files from a directory in the local
     * filesystem.
     */
/**
         * Constructor.
         * 
         * @param dirName
         *            name of the local directory
         * @throws IOException
         *             if dirName is not a directory
         */
/*
         * (non-Javadoc)
         * 
         * @see
         * edu.umd.cs.findbugs.URLClassPath.Entry#openStream(java.lang.String)
         */
/*
         * (non-Javadoc)
         * 
         * @see edu.umd.cs.findbugs.URLClassPath.Entry#getURL()
         */
// Nothing to do here
/**
     * Classpath entry class to load files from a remote archive URL. It uses
     * jar URLs to specify individual files within the remote archive.
     */
/**
         * Constructor.
         * 
         * @param remoteArchiveURL
         *            the remote zip/jar file URL
         */
/*
         * (non-Javadoc)
         * 
         * @see
         * edu.umd.cs.findbugs.URLClassPath.Entry#openStream(java.lang.String)
         */
/*
         * (non-Javadoc)
         * 
         * @see edu.umd.cs.findbugs.URLClassPath.Entry#getURL()
         */
// Nothing to do
/**
     * Classpath entry class to load files from a remote directory URL.
     */
/**
         * Constructor.
         * 
         * @param remoteDirURL
         *            URL of the remote directory; must end in "/"
         */
/*
         * (non-Javadoc)
         * 
         * @see
         * edu.umd.cs.findbugs.URLClassPath.Entry#openStream(java.lang.String)
         */
/*
         * (non-Javadoc)
         * 
         * @see edu.umd.cs.findbugs.URLClassPath.Entry#getURL()
         */
// Nothing to do
// Fields
/**
     * Constructor. Creates a classpath with no elements.
     */
/**
     * Add given filename/URL to the classpath. If no URL protocol is given, the
     * filename is assumed to be a local file or directory. Remote directories
     * must be specified with a "/" character at the end of the URL.
     * 
     * @param fileName
     *            filename or URL of codebase (directory or archive file)
     * @throws IOException
     *             if entry is invalid or does not exist
     */
/**
     * Return the classpath string.
     * 
     * @return the classpath string
     */
/**
     * Open a stream to read given resource.
     * 
     * @param resourceName
     *            name of resource to load, e.g. "java/lang/Object.class"
     * @return input stream to read resource, or null if resource could not be
     *         found
     * @throws IOException
     *             if an IO error occurs trying to determine whether or not the
     *             resource exists
     */
// Try each classpath entry, in order, until we find one
// that has the resource. Catch and ignore IOExceptions.
// FIXME: The following code should throw IOException.
//
// URL.openStream() does not seem to distinguish
// whether the resource does not exist, vs. some
// transient error occurring while trying to access it.
// This is unfortunate, because we really should throw
// an exception out of this method in the latter case,
// since it means our knowledge of the classpath is
// incomplete.
//
// Short of reimplementing HTTP, etc., ourselves,
// there is probably nothing we can do about this problem.
// Ignore
/**
     * Look up a class from the classpath.
     * 
     * @param className
     *            name of class to look up
     * @return the JavaClass object for the class
     * @throws ClassNotFoundException
     *             if the class couldn't be found
     */
// Ignore
/**
     * Close all underlying resources.
     */
/**
     * Get the URL protocol of given URL string.
     * 
     * @param urlString
     *            the URL string
     * @return the protocol name ("http", "file", etc.), or null if there is no
     *         protocol
     */
/**
     * Get the file extension of given fileName.
     * 
     * @return the file extension, or null if there is no file extension
     */
/**
     * Determine if given file extension indicates an archive file.
     * 
     * @param fileExtension
     *            the file extension (e.g., ".jar")
     * @return true if the file extension indicates an archive, false otherwise
     */
// vim:ts=4
abstract interface package-info {
}
