/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A visitor to model the effect of instructions on the status of the resource
 * (in this case, Streams).
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import org.apache.bcel.Constants;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InvokeInstruction;
import edu.umd.cs.findbugs.ba.BasicBlock;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.ResourceValue;
import edu.umd.cs.findbugs.ba.ResourceValueFrame;
import edu.umd.cs.findbugs.ba.ResourceValueFrameModelingVisitor;
public class StreamFrameModelingVisitor extends edu.umd.cs.findbugs.ba.ResourceValueFrameModelingVisitor {
    private edu.umd.cs.findbugs.detect.StreamResourceTracker resourceTracker;
    private edu.umd.cs.findbugs.detect.Stream stream;
    private edu.umd.cs.findbugs.ba.Location location;
    public StreamFrameModelingVisitor(org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.detect.StreamResourceTracker resourceTracker, edu.umd.cs.findbugs.detect.Stream stream) {
        super(cpg);
        this.resourceTracker = resourceTracker;
        this.stream = stream;
    }
    public void transferInstruction(org.apache.bcel.generic.InstructionHandle handle, edu.umd.cs.findbugs.ba.BasicBlock basicBlock) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        this.location = new edu.umd.cs.findbugs.ba.Location(handle, basicBlock);
// Record what Location we are analyzing
        final org.apache.bcel.generic.Instruction ins = handle.getInstruction();
        final edu.umd.cs.findbugs.ba.ResourceValueFrame frame = this.getFrame();
        int status =  -1;
        boolean created = false;
// Is a resource created, opened, or closed by this instruction?
        edu.umd.cs.findbugs.ba.Location creationPoint = stream.getLocation();
        if (handle == creationPoint.getHandle() && basicBlock == creationPoint.getBasicBlock()) {
// Resource creation
            if (stream.isOpenOnCreation()) {
                status = edu.umd.cs.findbugs.ba.ResourceValueFrame.OPEN;
                stream.setOpenLocation(location);
                resourceTracker.addStreamOpenLocation(location,stream);
            }
            else {
                status = edu.umd.cs.findbugs.ba.ResourceValueFrame.CREATED;
            }
            created = true;
        }
        else if (resourceTracker.isResourceOpen(basicBlock,handle,cpg,stream,frame)) {
            status = edu.umd.cs.findbugs.ba.ResourceValueFrame.OPEN;
            stream.setOpenLocation(location);
            resourceTracker.addStreamOpenLocation(location,stream);
        }
        else if (resourceTracker.isResourceClose(basicBlock,handle,cpg,stream,frame)) {
            status = edu.umd.cs.findbugs.ba.ResourceValueFrame.CLOSED;
        }
        this.analyzeInstruction(ins);
// Model use of instance values in frame slots
// If needed, update frame status
        if (status !=  -1) {
            frame.setStatus(status);
            if (created) frame.setValue(frame.getNumSlots() - 1,edu.umd.cs.findbugs.ba.ResourceValue.instance());
        }
    }
    protected boolean instanceEscapes(org.apache.bcel.generic.InvokeInstruction inv, int instanceArgNum) {
        org.apache.bcel.generic.ConstantPoolGen cpg = this.getCPG();
        java.lang.String className = inv.getClassName(cpg);
// System.out.print("[Passed as arg="+instanceArgNum+" at " + inv +
// "]");
        boolean escapes = (inv.getOpcode() == org.apache.bcel.Constants.INVOKESTATIC || instanceArgNum != 0);
        java.lang.String methodName = inv.getMethodName(cpg);
        java.lang.String methodSig = inv.getSignature(cpg);
        if (inv.getOpcode() == org.apache.bcel.Constants.INVOKEVIRTUAL && (methodName.equals("load") || methodName.equals("loadFromXml") || methodName.equals("store") || methodName.equals("save")) && className.equals("java.util.Properties")) escapes = false;
        if (inv.getOpcode() == org.apache.bcel.Constants.INVOKEVIRTUAL && (methodName.equals("load") || methodName.equals("store")) && className.equals("java.security.KeyStore")) escapes = false;
        if (inv.getOpcode() == org.apache.bcel.Constants.INVOKEVIRTUAL && "getChannel".equals(methodName) && "()Ljava/nio/channels/FileChannel;".equals(methodSig)) escapes = true;
        if (edu.umd.cs.findbugs.detect.FindOpenStream.DEBUG && escapes) {
            java.lang.System.out.println("ESCAPE at " + location + " at call to " + className + "." + methodName + ":" + methodSig);
        }
// Record the fact that this might be a stream escape
        if (stream.getOpenLocation() != null) resourceTracker.addStreamEscape(stream,location);
        return escapes;
    }
}
// vim:ts=3
