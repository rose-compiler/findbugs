/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Method property recording which parameters are have some property
 * (originally, which were required to be nonnull, now made more generic)
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.interproc;
import edu.umd.cs.findbugs.ba.interproc.*;
import java.util.BitSet;
import java.util.Iterator;
public class ParameterProperty extends java.lang.Object {
/**
     * Maximum number of parameters that can be represented by a
     * ParameterProperty.
     */
    final public static int MAX_PARAMS = 32;
    private int bits;
/**
     * Constructor. Parameters are all assumed not to be non-null.
     */
    public ParameterProperty() {
        super();
        this.bits = 0;
    }
/**
     * Constructor. Parameters are all assumed not to be non-null.
     */
    public ParameterProperty(int bits) {
        super();
        this.bits = bits;
    }
/**
     * Get the non-null param bitset.
     * 
     * @return the non-null param bitset
     */
    public int getParamsWithProperty() {
        return bits;
    }
    public java.lang.Iterable<java.lang.Integer> iterable() {
        return new java.lang.Iterable<java.lang.Integer>() {
            public java.util.Iterator<java.lang.Integer> iterator() {
                return new java.util.Iterator<java.lang.Integer>() {
                    int nextInt = 0;
                    {
                        this.advanceNextInt();
                    }
                    private void advanceNextInt() {
                        while ( !edu.umd.cs.findbugs.ba.interproc.ParameterProperty.this.hasProperty(nextInt) && nextInt < 32) nextInt++;
                        if (nextInt >= 32) nextInt =  -1;
                    }
                    public boolean hasNext() {
                        return nextInt >= 0;
                    }
                    public java.lang.Integer next() {
                        int result = nextInt;
                        nextInt++;
                        this.advanceNextInt();
                        return result;
                    }
                    public void remove() {
                        throw new java.lang.UnsupportedOperationException();
                    }
                };
            }
        };
    }
/**
     * Set the non-null param bitset.
     * 
     * @param nonNullParamSet
     *            the non-null param bitset
     */
    public void setParamsWithProperty(int nonNullParamSet) {
        this.bits = nonNullParamSet;
    }
/**
     * Set the non-null param set from given BitSet.
     * 
     * @param nonNullSet
     *            BitSet indicating which parameters are non-null
     */
    public void setParamsWithProperty(java.util.BitSet nonNullSet) {
        for (int i = 0; i < 32; ++i) {
            this.setParamWithProperty(i,nonNullSet.get(i));
        }
    }
/**
     * Set whether or not a parameter might be non-null.
     * 
     * @param param
     *            the parameter index
     * @param hasProperty
     *            true if the parameter might be non-null, false otherwise
     */
    public void setParamWithProperty(int param, boolean hasProperty) {
        if (param < 0 || param > 31) return;
        if (hasProperty) {
            bits |= (1 << param);
        }
        else {
            bits &=  ~(1 << param);
        }
    }
/**
     * Return whether or not a parameter might be non-null.
     * 
     * @param param
     *            the parameter index
     * @return true if the parameter might be non-null, false otherwise
     */
    public boolean hasProperty(int param) {
        if (param < 0 || param > 31) return false;
        else return (bits & (1 << param)) != 0;
    }
/**
     * Given a bitset of null arguments passed to the method represented by this
     * property, return a bitset indicating which null arguments correspond to
     * an non-null param.
     * 
     * @param nullArgSet
     *            bitset of null arguments
     * @return bitset intersecting null arguments and non-null params
     */
    public java.util.BitSet getMatchingParameters(java.util.BitSet nullArgSet) {
        java.util.BitSet result = new java.util.BitSet();
        for (int i = 0; i < 32; ++i) {
            result.set(i,nullArgSet.get(i) && this.hasProperty(i));
        }
        return result;
    }
    public java.util.BitSet getAsBitSet() {
        java.util.BitSet result = new java.util.BitSet();
        if (this.isEmpty()) return result;
        for (int i = 0; i < 32; ++i) {
            result.set(i,this.hasProperty(i));
        }
        return result;
    }
/**
     * Return whether or not the set of non-null parameters is empty.
     * 
     * @return true if the set is empty, false if it contains at least one
     *         parameter
     */
    public boolean isEmpty() {
        return bits == 0;
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append('\u007b');
        for (int i = 0; i < 32; ++i) {
            if (this.hasProperty(i)) {
                if (buf.length() > 1) buf.append('\u002c');
                buf.append(i);
            }
        }
        buf.append('\u007d');
        return buf.toString();
    }
/**
     * Intersect this set with the given set. Useful for summarizing the
     * properties of multiple methods.
     * 
     * @param targetDerefParamSet
     *            another set
     */
    public void intersectWith(edu.umd.cs.findbugs.ba.interproc.ParameterProperty targetDerefParamSet) {
        bits &= targetDerefParamSet.bits;
    }
/**
     * Make this object the same as the given one.
     * 
     * @param other
     *            another ParameterNullnessProperty
     */
    public void copyFrom(edu.umd.cs.findbugs.ba.interproc.ParameterProperty other) {
        this.bits = other.bits;
    }
}
