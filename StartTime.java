/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.lang.management.ManagementFactory;
public class StartTime extends java.lang.Object {
    public StartTime() {
    }
/**
     * The start time this FindBugs instance was started
     */
    final public static long START_TIME = java.lang.System.currentTimeMillis();
    final public static long VM_START_TIME;
    static {
        long vmStartTime = START_TIME;
        try {
            vmStartTime = java.lang.management.ManagementFactory.getRuntimeMXBean().getStartTime();
        }
        catch (java.lang.Throwable e){
// ignore
            assert true;
        }
        VM_START_TIME = vmStartTime;
    }
}
