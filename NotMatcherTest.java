/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 *  Author: Graham Allan
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.containsString;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.Test;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.xml.OutputStreamXMLOutput;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class NotMatcherTest extends java.lang.Object {
    private static class TestMatcher extends java.lang.Object implements edu.umd.cs.findbugs.filter.Matcher {
        final private boolean alwaysMatches;
        public TestMatcher(boolean alwaysMatches) {
            super();
            this.alwaysMatches = alwaysMatches;
        }
        public boolean match(edu.umd.cs.findbugs.BugInstance bugInstance) {
            return alwaysMatches;
        }
        public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean disabled) throws java.io.IOException {
            xmlOutput.openTag("TestMatch");
            xmlOutput.closeTag("TestMatch");
        }
    }
    public NotMatcherTest() {
    }
    final private edu.umd.cs.findbugs.BugInstance bug = new edu.umd.cs.findbugs.BugInstance("UUF_UNUSED_FIELD", 0);
    public void invertsResultsFromWrappedMatcher_doesntMatchWhenWrappedDoesMatch() throws java.lang.Exception {
        edu.umd.cs.findbugs.filter.Matcher wrappedMatcher = new edu.umd.cs.findbugs.filter.NotMatcherTest.TestMatcher(true);
        edu.umd.cs.findbugs.filter.NotMatcher notMatcher = new edu.umd.cs.findbugs.filter.NotMatcher();
        notMatcher.addChild(wrappedMatcher);
        assertFalse(notMatcher.match(bug));
    }
    public void invertsResultsFromWrappedMatcher_doesMatchWhenWrappedDoesnt() throws java.lang.Exception {
        edu.umd.cs.findbugs.filter.Matcher wrappedMatcher = new edu.umd.cs.findbugs.filter.NotMatcherTest.TestMatcher(false);
        edu.umd.cs.findbugs.filter.NotMatcher notMatcher = new edu.umd.cs.findbugs.filter.NotMatcher();
        notMatcher.addChild(wrappedMatcher);
        assertTrue(notMatcher.match(bug));
    }
    public void writeXMLOutputAddsNotTagsAroundWrappedMatchersOutput() throws java.lang.Exception {
        edu.umd.cs.findbugs.filter.Matcher wrappedMatcher = new edu.umd.cs.findbugs.filter.NotMatcherTest.TestMatcher(true);
        edu.umd.cs.findbugs.filter.NotMatcher notMatcher = new edu.umd.cs.findbugs.filter.NotMatcher();
        notMatcher.addChild(wrappedMatcher);
        java.lang.String xmlOutputCreated = this.writeXMLAndGetStringOutput(notMatcher);
        assertTrue(containsString("<Not>").matches(xmlOutputCreated));
        assertTrue(containsString("<TestMatch>").matches(xmlOutputCreated));
        assertTrue(containsString("</TestMatch>").matches(xmlOutputCreated));
        assertTrue(containsString("</Not>").matches(xmlOutputCreated));
    }
    public void canReturnChildMatcher() {
        edu.umd.cs.findbugs.filter.Matcher wrappedMatcher = new edu.umd.cs.findbugs.filter.NotMatcherTest.TestMatcher(true);
        edu.umd.cs.findbugs.filter.NotMatcher notMatcher = new edu.umd.cs.findbugs.filter.NotMatcher();
        notMatcher.addChild(wrappedMatcher);
        assertSame("Should return child matcher.",wrappedMatcher,notMatcher.originalMatcher());
    }
    public void throwsExceptionWhenTryingToGetNonExistentChildMatcher() {
        new edu.umd.cs.findbugs.filter.NotMatcher().originalMatcher();
    }
    private java.lang.String writeXMLAndGetStringOutput(edu.umd.cs.findbugs.filter.NotMatcher notMatcher) throws java.io.IOException {
        java.io.ByteArrayOutputStream outputStream = new java.io.ByteArrayOutputStream();
        edu.umd.cs.findbugs.xml.XMLOutput xmlOutput = new edu.umd.cs.findbugs.xml.OutputStreamXMLOutput(outputStream);
        notMatcher.writeXML(xmlOutput,false);
        xmlOutput.finish();
        java.lang.String xmlOutputCreated = outputStream.toString();
        return xmlOutputCreated;
    }
}
