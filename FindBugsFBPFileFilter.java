/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.io.File;
final public class FindBugsFBPFileFilter extends edu.umd.cs.findbugs.gui2.FindBugsFileFilter {
    final public static edu.umd.cs.findbugs.gui2.FindBugsFBPFileFilter INSTANCE = new edu.umd.cs.findbugs.gui2.FindBugsFBPFileFilter();
    public FindBugsFBPFileFilter() {
        super();
    }
    public boolean accept(java.io.File arg0) {
        return arg0.getName().endsWith(".fbp") || arg0.isDirectory();
    }
    public java.lang.String getDescription() {
        return "FindBugs project file (.fbp)";
    }
    edu.umd.cs.findbugs.gui2.SaveType getSaveType() {
        return edu.umd.cs.findbugs.gui2.SaveType.FBP_FILE;
    }
}
