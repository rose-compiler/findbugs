/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A slightly more intellegent way of comparing BugInstances from two versions
 * to see if they are the "same". Uses class and method hashes to try to handle
 * renamings, at least for simple cases. (<em>Hashes disabled for the
 * time being.</em>) Uses opcode context to try to identify code that is the
 * same, even if it moves within the method. Also compares by bug abbreviation
 * rather than bug type, since the "same" bug can change type if the context
 * changes (e.g., "definitely null" to "null on simple path" for a null pointer
 * dereference). Also, we often change bug types between different versions of
 * FindBugs.
 * 
 * @see edu.umd.cs.findbugs.BugInstance
 * @see edu.umd.cs.findbugs.VersionInsensitiveBugComparator
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import edu.umd.cs.findbugs.model.ClassNameRewriter;
public class FuzzyBugComparator extends java.lang.Object implements edu.umd.cs.findbugs.WarningComparator {
// Don't use hashes for now. Still ironing out issues there.
/**
     * Filter ignored BugAnnotations from given Iterator.
     */
    private static class FilteringBugAnnotationIterator extends java.lang.Object implements java.util.Iterator<edu.umd.cs.findbugs.BugAnnotation> {
        java.util.Iterator<edu.umd.cs.findbugs.BugAnnotation> iter;
        edu.umd.cs.findbugs.BugAnnotation next;
        public FilteringBugAnnotationIterator(java.util.Iterator<edu.umd.cs.findbugs.BugAnnotation> iter) {
            super();
            this.iter = iter;
        }
        private void findNext() {
            if (next == null) {
                while (iter.hasNext()) {
                    edu.umd.cs.findbugs.BugAnnotation candidate = iter.next();
                    if ( !ignore(candidate)) {
                        next = candidate;
                        break;
                    }
                }
            }
        }
/*
         * (non-Javadoc)
         * 
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext() {
            this.findNext();
            return next != null;
        }
/*
         * (non-Javadoc)
         * 
         * @see java.util.Iterator#next()
         */
        public edu.umd.cs.findbugs.BugAnnotation next() {
            this.findNext();
            if (next == null) throw new java.util.NoSuchElementException();
            edu.umd.cs.findbugs.BugAnnotation result = next;
            next = null;
            return result;
        }
/*
         * (non-Javadoc)
         * 
         * @see java.util.Iterator#remove()
         */
        public void remove() {
            throw new java.lang.UnsupportedOperationException();
        }
    }
    final private static boolean DEBUG = false;
    final private static boolean USE_HASHES = false;
    final private static long serialVersionUID = 1L;
/**
     * Keep track of which BugCollections the various BugInstances have come
     * from.
     */
    private java.util.IdentityHashMap<edu.umd.cs.findbugs.BugInstance, edu.umd.cs.findbugs.BugCollection> bugCollectionMap;
    private edu.umd.cs.findbugs.model.ClassNameRewriter classNameRewriter;
/**
     * Map of class hashes to canonicate class names used for comparison
     * purposes.
     */
// private Map<ClassHash, String> classHashToCanonicalClassNameMap;
    public FuzzyBugComparator() {
        super();
        if (DEBUG) java.lang.System.out.println("Created fuzzy comparator");
        this.bugCollectionMap = new java.util.IdentityHashMap<edu.umd.cs.findbugs.BugInstance, edu.umd.cs.findbugs.BugCollection>();
    }
// this.classHashToCanonicalClassNameMap = new TreeMap<ClassHash,
// String>();
/**
     * Register a BugCollection. This allows us to find the class and method
     * hashes for BugInstances to be compared.
     * 
     * @param bugCollection
     *            a BugCollection
     */
    public void registerBugCollection(edu.umd.cs.findbugs.BugCollection bugCollection) {
    }
// For now, nothing to do
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.WarningComparator#setClassNameRewriter(edu.umd.cs
     * .findbugs.model.MovedClassMap)
     */
    public void setClassNameRewriter(edu.umd.cs.findbugs.model.ClassNameRewriter classNameRewriter) {
        this.classNameRewriter = classNameRewriter;
    }
    public int compare(edu.umd.cs.findbugs.BugInstance lhs, edu.umd.cs.findbugs.BugInstance rhs) {
        int cmp;
        if (DEBUG) java.lang.System.out.println("Fuzzy comparison");
// Bug abbreviations must match.
        edu.umd.cs.findbugs.BugPattern lhsPattern = lhs.getBugPattern();
        edu.umd.cs.findbugs.BugPattern rhsPattern = rhs.getBugPattern();
        if (lhsPattern == null || rhsPattern == null) {
            if (DEBUG) {
                if (lhsPattern == null) java.lang.System.out.println("Missing pattern: " + lhs.getType());
                if (rhsPattern == null) java.lang.System.out.println("Missing pattern: " + rhs.getType());
            }
            java.lang.String lhsCode = this.getCode(lhs.getType());
            java.lang.String rhsCode = this.getCode(rhs.getType());
            if ((cmp = lhsCode.compareTo(rhsCode)) != 0) return cmp;
        }
        else {
            if ((cmp = lhsPattern.getAbbrev().compareTo(rhsPattern.getAbbrev())) != 0) return cmp;
        }
        edu.umd.cs.findbugs.BugCollection lhsCollection = bugCollectionMap.get(lhs);
        edu.umd.cs.findbugs.BugCollection rhsCollection = bugCollectionMap.get(rhs);
// Scan through bug annotations, comparing fuzzily if possible
        java.util.Iterator<edu.umd.cs.findbugs.BugAnnotation> lhsIter = new edu.umd.cs.findbugs.FuzzyBugComparator.FilteringBugAnnotationIterator(lhs.annotationIterator());
        java.util.Iterator<edu.umd.cs.findbugs.BugAnnotation> rhsIter = new edu.umd.cs.findbugs.FuzzyBugComparator.FilteringBugAnnotationIterator(rhs.annotationIterator());
        while (lhsIter.hasNext() && rhsIter.hasNext()) {
            edu.umd.cs.findbugs.BugAnnotation lhsAnnotation = lhsIter.next();
            edu.umd.cs.findbugs.BugAnnotation rhsAnnotation = rhsIter.next();
            if (DEBUG) java.lang.System.out.println("Compare annotations: " + lhsAnnotation + "," + rhsAnnotation);
            cmp = lhsAnnotation.getClass().getName().compareTo(rhsAnnotation.getClass().getName());
// Annotation classes must match exactly
            if (cmp != 0) {
                if (DEBUG) java.lang.System.out.println("annotation class mismatch: " + lhsAnnotation.getClass().getName() + "," + rhsAnnotation.getClass().getName());
                return cmp;
            }
            if (lhsAnnotation.getClass() == edu.umd.cs.findbugs.ClassAnnotation.class) cmp = this.compareClasses(lhsCollection,rhsCollection,(edu.umd.cs.findbugs.ClassAnnotation) (lhsAnnotation) ,(edu.umd.cs.findbugs.ClassAnnotation) (rhsAnnotation) );
            else if (lhsAnnotation.getClass() == edu.umd.cs.findbugs.MethodAnnotation.class) cmp = this.compareMethods(lhsCollection,rhsCollection,(edu.umd.cs.findbugs.MethodAnnotation) (lhsAnnotation) ,(edu.umd.cs.findbugs.MethodAnnotation) (rhsAnnotation) );
            else if (lhsAnnotation.getClass() == edu.umd.cs.findbugs.SourceLineAnnotation.class) cmp = this.compareSourceLines(lhsCollection,rhsCollection,(edu.umd.cs.findbugs.SourceLineAnnotation) (lhsAnnotation) ,(edu.umd.cs.findbugs.SourceLineAnnotation) (rhsAnnotation) );
            else cmp = lhsAnnotation.compareTo(rhsAnnotation);
// everything else just compare directly
            if (cmp != 0) return cmp;
        }
// Number of bug annotations must match
        if ( !lhsIter.hasNext() &&  !rhsIter.hasNext()) {
            if (DEBUG) java.lang.System.out.println("Match!");
            return 0;
        }
        else return (lhsIter.hasNext() ? 1 :  -1);
    }
/**
     * @param type
     * @return the code of the Bug
     */
    private java.lang.String getCode(java.lang.String type) {
        int bar = type.indexOf('\u005f');
        if (bar < 0) return "";
        else return type.substring(0,bar);
    }
    private static int compareNullElements(java.lang.Object a, java.lang.Object b) {
        if (a != null) return 1;
        else if (b != null) return  -1;
        else return 0;
    }
    public int compareClasses(edu.umd.cs.findbugs.BugCollection lhsCollection, edu.umd.cs.findbugs.BugCollection rhsCollection, edu.umd.cs.findbugs.ClassAnnotation lhsClass, edu.umd.cs.findbugs.ClassAnnotation rhsClass) {
        if (lhsClass == null || rhsClass == null) {
            return compareNullElements(lhsClass,rhsClass);
        }
        else {
            return this.compareClassesByName(lhsCollection,rhsCollection,lhsClass.getClassName(),rhsClass.getClassName());
        }
    }
// Compare classes: either exact fully qualified name must match, or class
// hash must match
    public int compareClassesByName(edu.umd.cs.findbugs.BugCollection lhsCollection, edu.umd.cs.findbugs.BugCollection rhsCollection, java.lang.String lhsClassName, java.lang.String rhsClassName) {
        lhsClassName = this.rewriteClassName(lhsClassName);
        rhsClassName = this.rewriteClassName(rhsClassName);
        return lhsClassName.compareTo(rhsClassName);
    }
/**
     * @param className
     * @return the rewritten class name
     */
    private java.lang.String rewriteClassName(java.lang.String className) {
        if (classNameRewriter != null) {
            className = classNameRewriter.rewriteClassName(className);
        }
        return className;
    }
// Compare methods: either exact name and signature must match, or method
// hash must match
    public int compareMethods(edu.umd.cs.findbugs.BugCollection lhsCollection, edu.umd.cs.findbugs.BugCollection rhsCollection, edu.umd.cs.findbugs.MethodAnnotation lhsMethod, edu.umd.cs.findbugs.MethodAnnotation rhsMethod) {
        if (lhsMethod == null || rhsMethod == null) {
            return compareNullElements(lhsMethod,rhsMethod);
        }
// Compare for exact match
        int cmp = lhsMethod.compareTo(rhsMethod);
        return cmp;
    }
/**
     * For now, just look at the 2 preceeding and succeeding opcodes for fuzzy
     * source line matching.
     */
    final private static int NUM_CONTEXT_OPCODES = 2;
/**
     * Compare source line annotations.
     * 
     * @param rhsCollection
     *            lhs BugCollection
     * @param lhsCollection
     *            rhs BugCollection
     * @param lhs
     *            a SourceLineAnnotation
     * @param rhs
     *            another SourceLineAnnotation
     * @return comparison of lhs and rhs
     */
    public int compareSourceLines(edu.umd.cs.findbugs.BugCollection lhsCollection, edu.umd.cs.findbugs.BugCollection rhsCollection, edu.umd.cs.findbugs.SourceLineAnnotation lhs, edu.umd.cs.findbugs.SourceLineAnnotation rhs) {
        if (lhs == null || rhs == null) {
            return compareNullElements(lhs,rhs);
        }
// Classes must match fuzzily.
        int cmp = this.compareClassesByName(lhsCollection,rhsCollection,lhs.getClassName(),rhs.getClassName());
        if (cmp != 0) return cmp;
        return 0;
    }
// See "FindBugsAnnotationDescriptions.properties"
    final private static java.util.HashSet<java.lang.String> significantDescriptionSet = new java.util.HashSet<java.lang.String>();
    static {
        significantDescriptionSet.add("CLASS_DEFAULT");
        significantDescriptionSet.add("CLASS_EXCEPTION");
        significantDescriptionSet.add("CLASS_REFTYPE");
        significantDescriptionSet.add(edu.umd.cs.findbugs.ClassAnnotation.SUPERCLASS_ROLE);
        significantDescriptionSet.add(edu.umd.cs.findbugs.ClassAnnotation.IMPLEMENTED_INTERFACE_ROLE);
        significantDescriptionSet.add(edu.umd.cs.findbugs.ClassAnnotation.INTERFACE_ROLE);
        significantDescriptionSet.add("METHOD_DEFAULT");
        significantDescriptionSet.add(edu.umd.cs.findbugs.MethodAnnotation.METHOD_CALLED);
        significantDescriptionSet.add("METHOD_DANGEROUS_TARGET");
        significantDescriptionSet.add("METHOD_DECLARED_NONNULL");
        significantDescriptionSet.add("FIELD_DEFAULT");
        significantDescriptionSet.add("FIELD_ON");
        significantDescriptionSet.add("FIELD_SUPER");
        significantDescriptionSet.add("FIELD_MASKED");
        significantDescriptionSet.add("FIELD_MASKING");
        significantDescriptionSet.add("FIELD_STORED");
        significantDescriptionSet.add("TYPE_DEFAULT");
        significantDescriptionSet.add("TYPE_EXPECTED");
        significantDescriptionSet.add("TYPE_FOUND");
        significantDescriptionSet.add("LOCAL_VARIABLE_NAMED");
        significantDescriptionSet.add("INT_NULL_ARG");
        significantDescriptionSet.add("INT_MAYBE_NULL_ARG");
        significantDescriptionSet.add("INT_NONNULL_PARAM");
        significantDescriptionSet.add("SOURCE_LINE_DEFAULT");
    }
    public static boolean ignore(edu.umd.cs.findbugs.BugAnnotation annotation) {
        return  !significantDescriptionSet.contains(annotation.getDescription());
    }
}
