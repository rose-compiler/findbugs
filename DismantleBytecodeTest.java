/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.visitclass;
import edu.umd.cs.findbugs.visitclass.*;
import junit.framework.TestCase;
import org.apache.bcel.Constants;
public class DismantleBytecodeTest extends junit.framework.TestCase {
    public DismantleBytecodeTest() {
    }
    public void testAreOppositeBranches() {
        assertTrue(edu.umd.cs.findbugs.visitclass.DismantleBytecode.areOppositeBranches(org.apache.bcel.Constants.IF_ACMPEQ,org.apache.bcel.Constants.IF_ACMPNE));
        assertTrue(edu.umd.cs.findbugs.visitclass.DismantleBytecode.areOppositeBranches(org.apache.bcel.Constants.IF_ICMPEQ,org.apache.bcel.Constants.IF_ICMPNE));
        assertTrue(edu.umd.cs.findbugs.visitclass.DismantleBytecode.areOppositeBranches(org.apache.bcel.Constants.IF_ICMPLT,org.apache.bcel.Constants.IF_ICMPGE));
        assertTrue(edu.umd.cs.findbugs.visitclass.DismantleBytecode.areOppositeBranches(org.apache.bcel.Constants.IFNE,org.apache.bcel.Constants.IFEQ));
        assertTrue(edu.umd.cs.findbugs.visitclass.DismantleBytecode.areOppositeBranches(org.apache.bcel.Constants.IFLT,org.apache.bcel.Constants.IFGE));
        assertTrue(edu.umd.cs.findbugs.visitclass.DismantleBytecode.areOppositeBranches(org.apache.bcel.Constants.IFNULL,org.apache.bcel.Constants.IFNONNULL));
    }
}
