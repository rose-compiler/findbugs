/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
// Note: Don't remove the final, if anyone extends this class, bad things could
// happen, since a thread is started in this class's constructor.
/**
 *Creating an instance of this class runs a FindBugs analysis, and pops up a nice progress window
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.FindBugsProgress;
import edu.umd.cs.findbugs.Project;
final public class AnalyzingDialog extends edu.umd.cs.findbugs.gui2.FBDialog implements edu.umd.cs.findbugs.FindBugsProgress {
/**
     * 
     * @param project
     *            The Project to analyze
     * @param callback
     *            contains what to do if the analysis is interrupted and what to
     *            do if it finishes normally
     * @param joinThread
     *            Whether or not this constructor should return before the
     *            analysis is complete. If true, the constructor does not return
     *            until the analysis is either finished or interrupted.
     */
// Why was this set to false before?
// TODO there should be a call to dispose() here, but it seems to
// cause repainting issues
    private class AnalysisThread extends java.lang.Thread {
        public AnalysisThread() {
        }
        {
            this.setPriority(edu.umd.cs.findbugs.gui2.Driver.getPriority());
            this.setName("Analysis Thread");
        }
        public void run() {
            if (project == null) throw new java.lang.NullPointerException("null project");
            edu.umd.cs.findbugs.BugCollection data;
            try {
                data = edu.umd.cs.findbugs.gui2.BugLoader.doAnalysis(project,edu.umd.cs.findbugs.gui2.AnalyzingDialog.this);
            }
            catch (java.lang.InterruptedException e){
                callback.analysisInterrupted();
// We don't have to clean up the dialog because the
// cancel button handler does this already.
                return;
            }
            catch (java.io.IOException e){
                java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.gui2.AnalyzingDialog.class.getName()).log(java.util.logging.Level.WARNING,"IO Error while performing analysis",e);
                callback.analysisInterrupted();
                this.scheduleDialogCleanup();
                this.scheduleErrorDialog("Analysis failed",e.getClass().getSimpleName() + ": " + e.getMessage());
                return;
            }
            catch (java.lang.Throwable e){
                callback.analysisInterrupted();
                this.scheduleDialogCleanup();
                this.scheduleErrorDialog("Analysis failed",e.getClass().getSimpleName() + ": " + e.getMessage());
                return;
            }
            analysisFinished = true;
            this.scheduleDialogCleanup();
            callback.analysisFinished(data);
            edu.umd.cs.findbugs.gui2.MainFrame.getInstance().newProject();
        }
// Analysis succeeded
        private void scheduleDialogCleanup() {
            javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
                public void run() {
                    edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.setVisible(false);
                }
            });
        }
/*
                 * (non-Javadoc)
                 * 
                 * @see java.lang.Runnable#run()
                 */
        private void scheduleErrorDialog(final java.lang.String title, final java.lang.String message) {
            javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
                public void run() {
                    javax.swing.JOptionPane.showMessageDialog(edu.umd.cs.findbugs.gui2.MainFrame.getInstance(),message,title,javax.swing.JOptionPane.ERROR_MESSAGE);
                }
            });
        }
    }
    private volatile boolean analysisFinished = false;
    private edu.umd.cs.findbugs.Project project;
    private edu.umd.cs.findbugs.gui2.AnalysisCallback callback;
    private edu.umd.cs.findbugs.gui2.AnalyzingDialog.AnalysisThread analysisThread = new edu.umd.cs.findbugs.gui2.AnalyzingDialog.AnalysisThread();
    private int count;
    private int goal;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JButton cancelButton;
    public AnalyzingDialog(final edu.umd.cs.findbugs.Project project) {
        this(project,new edu.umd.cs.findbugs.gui2.AnalysisCallback() {
            public void analysisFinished(edu.umd.cs.findbugs.BugCollection results) {
                edu.umd.cs.findbugs.gui2.MainFrame instance = edu.umd.cs.findbugs.gui2.MainFrame.getInstance();
                assert results.getProject() == project;
                instance.setBugCollection(results);
                try {
                    instance.releaseDisplayWait();
                }
                catch (java.lang.Exception e){
                    java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.gui2.AnalyzingDialog.class.getName()).log(java.util.logging.Level.FINE,"",e);
                }
                results.reinitializeCloud();
            }
            public void analysisInterrupted() {
                edu.umd.cs.findbugs.gui2.MainFrame instance = edu.umd.cs.findbugs.gui2.MainFrame.getInstance();
                instance.updateProjectAndBugCollection(null);
                instance.releaseDisplayWait();
            }
        },false);
    }
    public AnalyzingDialog(edu.umd.cs.findbugs.Project project, edu.umd.cs.findbugs.gui2.AnalysisCallback callback, boolean joinThread) {
        super();
        if (project == null) throw new java.lang.NullPointerException("null project");
        this.project = project;
        this.callback = callback;
        this.initComponents();
        edu.umd.cs.findbugs.gui2.MainFrame.getInstance().acquireDisplayWait();
        try {
            analysisThread.start();
            if (joinThread) try {
                analysisThread.join();
            }
            catch (java.lang.InterruptedException e){
            }
        }
        finally {
            if (joinThread) edu.umd.cs.findbugs.gui2.MainFrame.getInstance().releaseDisplayWait();
        }
    }
    private void initComponents() {
        statusLabel = new javax.swing.JLabel(" ");
        progressBar = new javax.swing.JProgressBar();
        progressBar.setStringPainted(true);
        cancelButton = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.cancel_btn","Cancel"));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.cancel();
            }
        });
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.cancel();
            }
        });
        javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
            public void run() {
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.setLayout(new javax.swing.BoxLayout(edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.getContentPane(), javax.swing.BoxLayout.Y_AXIS));
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.add(statusLabel);
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.add(progressBar);
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.add(cancelButton);
                statusLabel.setAlignmentX(CENTER_ALIGNMENT);
                progressBar.setAlignmentX(CENTER_ALIGNMENT);
                cancelButton.setAlignmentX(CENTER_ALIGNMENT);
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.pack();
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.setSize(300,edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.getHeight());
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.setLocationRelativeTo(edu.umd.cs.findbugs.gui2.MainFrame.getInstance());
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.setResizable(false);
                edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.setModal(true);
                try {
                    edu.umd.cs.findbugs.gui2.AnalyzingDialog.this.setVisible(true);
                }
                catch (java.lang.Throwable e){
                    project.getGuiCallback().showMessageDialog("ERROR DURING ANALYSIS:\n\n" + e.getClass().getSimpleName() + ": " + e.getMessage());
                }
            }
        });
    }
    private void cancel() {
        if ( !analysisFinished) {
            analysisThread.interrupt();
            this.setVisible(false);
        }
    }
    private void updateStage(java.lang.String stage) {
        statusLabel.setText(stage);
    }
    private void incrementCount() {
        count++;
        javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
            public void run() {
                progressBar.setString(count + "/" + goal);
                progressBar.setValue(count);
            }
        });
    }
    private void updateCount(final int count, final int goal) {
        this.count = count;
        this.goal = goal;
        javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
            public void run() {
                progressBar.setString(count + "/" + goal);
                progressBar.setValue(count);
                progressBar.setMaximum(goal);
            }
        });
    }
    public void finishArchive() {
        this.incrementCount();
    }
    public void finishClass() {
        this.incrementCount();
    }
    public void finishPerClassAnalysis() {
        this.updateStage(edu.umd.cs.findbugs.L10N.getLocalString("progress.finishing_analysis","Finishing analysis..."));
    }
    public void reportNumberOfArchives(int numArchives) {
        this.updateStage(edu.umd.cs.findbugs.L10N.getLocalString("progress.scanning_archives","Scanning archives..."));
        this.updateCount(0,numArchives);
    }
    int pass = 0;
    public void startAnalysis(int numClasses) {
        pass++;
        java.lang.String localString = edu.umd.cs.findbugs.L10N.getLocalString("progress.analyzing_classes","Analyzing classes...");
        this.updateStage(localString + ", pass " + pass + "/" + classesPerPass.length);
        this.updateCount(0,numClasses);
    }
/*
                 * (non-Javadoc)
                 * 
                 * @see java.lang.Runnable#run()
                 */
    int[] classesPerPass;
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.FindBugsProgress#predictPassCount(int[])
     */
    public void predictPassCount(int[] classesPerPass) {
        this.classesPerPass = classesPerPass;
    }
    public void startArchive(java.lang.String name) {
    }
}
// noop
