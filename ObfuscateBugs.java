/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2010, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.IOException;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.Obfuscate;
import edu.umd.cs.findbugs.Project;
import edu.umd.cs.findbugs.ProjectPackagePrefixes;
import edu.umd.cs.findbugs.SortedBugCollection;
public class ObfuscateBugs extends java.lang.Object {
    static class CommandLine extends edu.umd.cs.findbugs.config.CommandLine {
        public CommandLine() {
        }
        public void handleOption(java.lang.String option, java.lang.String optionalExtraPart) {
            throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
/*
         * (non-Javadoc)
         * 
         * @see
         * edu.umd.cs.findbugs.config.CommandLine#handleOptionWithArgument(java
         * .lang.String, java.lang.String)
         */
        protected void handleOptionWithArgument(java.lang.String option, java.lang.String argument) throws java.io.IOException {
            throw new java.lang.IllegalArgumentException("Unknown option : " + option);
        }
    }
    edu.umd.cs.findbugs.BugCollection bugCollection;
    public ObfuscateBugs() {
        super();
    }
    public ObfuscateBugs(edu.umd.cs.findbugs.BugCollection bugCollection) {
        super();
        this.bugCollection = bugCollection;
    }
    public void setBugCollection(edu.umd.cs.findbugs.BugCollection bugCollection) {
        this.bugCollection = bugCollection;
    }
    public edu.umd.cs.findbugs.workflow.ObfuscateBugs execute() {
        edu.umd.cs.findbugs.ProjectPackagePrefixes foo = new edu.umd.cs.findbugs.ProjectPackagePrefixes();
        for (edu.umd.cs.findbugs.BugInstance b : bugCollection.getCollection())foo.countBug(b);
;
        foo.report();
        return this;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.workflow.ObfuscateBugs.CommandLine commandLine = new edu.umd.cs.findbugs.workflow.ObfuscateBugs.CommandLine();
        int argCount = commandLine.parse(args,0,2,"Usage: " + edu.umd.cs.findbugs.workflow.ObfuscateBugs.class.getName() + " [options] [<xml results>] ");
        edu.umd.cs.findbugs.SortedBugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (argCount < args.length) bugCollection.readXML(args[argCount++]);
        else bugCollection.readXML(java.lang.System.in);
        edu.umd.cs.findbugs.SortedBugCollection results = bugCollection.createEmptyCollectionWithMetadata();
        edu.umd.cs.findbugs.Project project = results.getProject();
        project.getSourceDirList().clear();
        project.getFileList().clear();
        project.getAuxClasspathEntryList().clear();
        results.getProjectStats().getPackageStats().clear();
        results.clearMissingClasses();
        results.clearErrors();
        for (edu.umd.cs.findbugs.BugInstance bug : bugCollection){
            results.add(edu.umd.cs.findbugs.Obfuscate.obfuscate(bug),false);
        }
;
        if (argCount == args.length) {
            results.writeXML(java.lang.System.out);
        }
        else {
            results.writeXML(args[argCount++]);
        }
    }
}
