/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * CheckedAnalysisException subtype to indicate that a required class was
 * missing.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile;
import edu.umd.cs.findbugs.classfile.*;
public class MissingClassException extends edu.umd.cs.findbugs.classfile.ResourceNotFoundException {
    private edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor;
/**
     * Constructor.
     * 
     * @param classDescriptor
     *            missing class
     */
    public MissingClassException(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor) {
        super(classDescriptor.toResourceName());
        this.classDescriptor = classDescriptor;
    }
/**
     * Constructor.
     * 
     * @param classDescriptor
     *            missing class
     * @param cause
     *            underlying cause
     */
    public MissingClassException(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor, java.lang.Throwable cause) {
        super(classDescriptor.toResourceName(),cause);
        this.classDescriptor = classDescriptor;
    }
/**
     * @return Returns the classDescriptor.
     */
    public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() {
        return classDescriptor;
    }
}
