/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//   Copyright (C) 2003  William Pugh <pugh@cs.umd.edu>
//   http://www.cs.umd.edu/~pugh
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public
//   License as published by the Free Software Foundation; either
//   version 2.1 of the License, or (at your option) any later version.
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
package edu.umd.cs.findbugs.io;
import edu.umd.cs.findbugs.io.*;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import javax.annotation.CheckForNull;
import javax.annotation.WillNotClose;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.util.Util;
public class IO extends java.lang.Object {
    public IO() {
    }
    static java.lang.ThreadLocal<byte[]> myByteBuf = new java.lang.ThreadLocal<byte[]>() {
        protected byte[] initialValue() {
            return new byte[4096];
        }
    };
    static java.lang.ThreadLocal<char[]> myCharBuf = new java.lang.ThreadLocal<char[]>() {
        protected char[] initialValue() {
            return new char[4096];
        }
    };
    public static java.lang.String readAll(java.io.InputStream in) throws java.io.IOException {
        return readAll(edu.umd.cs.findbugs.charsets.UTF8.reader(in));
    }
    public static java.lang.String readAll(java.io.Reader reader) throws java.io.IOException {
        java.io.BufferedReader r = new java.io.BufferedReader(reader);
        java.io.StringWriter w = new java.io.StringWriter();
        copy(r,w);
        return w.toString();
    }
    public static long copy(java.io.InputStream in, java.io.OutputStream out) throws java.io.IOException {
        return copy(in,out,java.lang.Long.MAX_VALUE);
    }
    public static long copy(java.io.Reader in, java.io.Writer out) throws java.io.IOException {
        return copy(in,out,java.lang.Long.MAX_VALUE);
    }
    public static long copy(java.io.InputStream in, java.io.OutputStream out, long maxBytes) throws java.io.IOException {
        long total = 0;
        int sz = 0;
        byte[] buf = myByteBuf.get();
        while (maxBytes > 0 && (sz = in.read(buf,0,(int) (java.lang.Math.min(maxBytes,buf.length)) )) > 0) {
            total += sz;
            maxBytes -= sz;
            out.write(buf,0,sz);
        }
        return total;
    }
    public static long copy(java.io.Reader in, java.io.Writer out, long maxChars) throws java.io.IOException {
        long total = 0;
        int sz;
        char[] buf = myCharBuf.get();
        while (maxChars > 0 && (sz = in.read(buf,0,(int) (java.lang.Math.min(maxChars,buf.length)) )) > 0) {
            total += sz;
            maxChars -= sz;
            out.write(buf,0,sz);
        }
        return total;
    }
/**
     * Close given InputStream, ignoring any resulting exception.
     * 
     */
    public static void close(java.io.Closeable c) {
        if (c == null) {
            return;
        }
        try {
            c.close();
        }
        catch (java.io.IOException e){
        }
    }
/**
     * Close given InputStream, ignoring any resulting exception.
     * 
     * @param inputStream
     *            the InputStream to close; may be null (in which case nothing
     *            happens)
     */
    public static void close(java.io.InputStream inputStream) {
        if (inputStream == null) {
            return;
        }
        try {
            inputStream.close();
        }
        catch (java.io.IOException e){
        }
    }
/**
     * Close given OutputStream, ignoring any resulting exception.
     * 
     * @param outputStream
     *            the OutputStream to close; may be null (in which case nothing
     *            happens)
     */
    public static void close(java.io.OutputStream outputStream) {
        if (outputStream == null) {
            return;
        }
        try {
            outputStream.close();
        }
        catch (java.io.IOException e){
        }
    }
/**
     * Provide a skip fully method. Either skips the requested number of bytes
     * or throws an IOException;
     * 
     * @param in
     *            The input stream on which to perform the skip
     * @param bytes
     *            Number of bytes to skip
     * @throws EOFException
     *             if we reach EOF and still need to skip more bytes
     * @throws IOException
     *             if in.skip throws an IOException
     */
    public static void skipFully(java.io.InputStream in, long bytes) throws java.io.IOException {
        if (bytes < 0) throw new java.lang.IllegalArgumentException("Can't skip " + bytes + " bytes");
        long remaining = bytes;
        while (remaining > 0) {
            long skipped = in.skip(remaining);
            if (skipped <= 0) throw new java.io.EOFException("Reached EOF while trying to skip a total of " + bytes);
            remaining -= skipped;
        }
    }
    public static boolean verifyURL(java.net.URL u) {
        if (u == null) return false;
        java.io.InputStream i = null;
        try {
            java.net.URLConnection uc = u.openConnection();
            i = uc.getInputStream();
            int firstByte = i.read();
            i.close();
            return firstByte >= 0;
        }
        catch (java.lang.Exception e){
            return false;
        }
        finally {
            edu.umd.cs.findbugs.util.Util.closeSilently(i);
        }
    }
}
