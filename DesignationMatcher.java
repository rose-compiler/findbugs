/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Match bug instances having one of given codes or patterns.
 * 
 * @author rafal@caltha.pl
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import java.io.IOException;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class DesignationMatcher extends java.lang.Object implements edu.umd.cs.findbugs.filter.Matcher {
    private edu.umd.cs.findbugs.filter.StringSetMatch designations;
/**
     * Constructor.
     * 
     * @param designations
     *            comma-separated list of designations
     */
    public DesignationMatcher(java.lang.String designations) {
        super();
        this.designations = new edu.umd.cs.findbugs.filter.StringSetMatch(designations);
    }
    public boolean match(edu.umd.cs.findbugs.BugInstance bugInstance) {
        return designations.match(bugInstance.getUserDesignationKey());
    }
    public java.lang.String toString() {
        return "Designations(designations=" + designations + ")";
    }
    public int hashCode() {
        return designations.hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.filter.DesignationMatcher)) return false;
        edu.umd.cs.findbugs.filter.DesignationMatcher other = (edu.umd.cs.findbugs.filter.DesignationMatcher) (o) ;
        return designations.equals(other.designations);
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean disabled) throws java.io.IOException {
        xmlOutput.startTag("Designation");
        if (disabled) xmlOutput.addAttribute("disabled","true");
        this.addAttribute(xmlOutput,"designation",designations);
        xmlOutput.stopTag(true);
    }
    public void addAttribute(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, java.lang.String name, edu.umd.cs.findbugs.filter.StringSetMatch matches) throws java.io.IOException {
        java.lang.String value = matches.toString();
        if (value.length() != 0) xmlOutput.addAttribute(name,value);
    }
}
