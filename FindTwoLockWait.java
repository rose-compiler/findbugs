/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.MONITORENTER;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.StatelessDetector;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.LockDataflow;
final public class FindTwoLockWait extends java.lang.Object implements edu.umd.cs.findbugs.Detector, edu.umd.cs.findbugs.StatelessDetector {
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    private org.apache.bcel.classfile.JavaClass javaClass;
    private java.util.Collection<edu.umd.cs.findbugs.BugInstance> possibleWaitBugs = new java.util.LinkedList<edu.umd.cs.findbugs.BugInstance>();
    private java.util.Collection<edu.umd.cs.findbugs.SourceLineAnnotation> possibleNotifyLocations = new java.util.LinkedList<edu.umd.cs.findbugs.SourceLineAnnotation>();
    public FindTwoLockWait(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public java.lang.Object clone() {
        try {
            return super.clone();
        }
        catch (java.lang.CloneNotSupportedException e){
            throw new java.lang.AssertionError(e);
        }
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        javaClass = classContext.getJavaClass();
        possibleWaitBugs.clear();
        possibleNotifyLocations.clear();
        org.apache.bcel.classfile.Method[] methodList = javaClass.getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
            if (methodGen == null) continue;
            if ( !this.preScreen(methodGen)) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("Error analyzing " + method.toString(),e);
            }
        }
;
        if ( !possibleNotifyLocations.isEmpty()) for (edu.umd.cs.findbugs.BugInstance bug : possibleWaitBugs){
            for (edu.umd.cs.findbugs.SourceLineAnnotation notifyLine : possibleNotifyLocations)bug.addSourceLine(notifyLine).describe("SOURCE_NOTIFICATION_DEADLOCK");
;
            bugReporter.reportBug(bug);
        }
;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
        org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.LockDataflow dataflow = classContext.getLockDataflow(method);
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> j = cfg.locationIterator(); j.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = j.next();
            this.visitLocation(classContext,location,methodGen,dataflow);
        }
    }
    public boolean preScreen(org.apache.bcel.generic.MethodGen mg) {
        org.apache.bcel.generic.ConstantPoolGen cpg = mg.getConstantPool();
        int lockCount = mg.isSynchronized() ? 1 : 0;
        boolean sawWaitOrNotify = false;
        org.apache.bcel.generic.InstructionHandle handle = mg.getInstructionList().getStart();
        while (handle != null &&  !(lockCount >= 2 && sawWaitOrNotify)) {
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if (ins instanceof org.apache.bcel.generic.MONITORENTER) ++lockCount;
            else if (ins instanceof org.apache.bcel.generic.INVOKEVIRTUAL) {
                org.apache.bcel.generic.INVOKEVIRTUAL inv = (org.apache.bcel.generic.INVOKEVIRTUAL) (ins) ;
                java.lang.String methodName = inv.getMethodName(cpg);
                if (methodName.equals("wait") || methodName.startsWith("notify")) sawWaitOrNotify = true;
            }
            handle = handle.getNext();
        }
        return lockCount >= 2 && sawWaitOrNotify;
    }
    public void visitLocation(edu.umd.cs.findbugs.ba.ClassContext classContext, edu.umd.cs.findbugs.ba.Location location, org.apache.bcel.generic.MethodGen methodGen, edu.umd.cs.findbugs.ba.LockDataflow dataflow) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        org.apache.bcel.generic.ConstantPoolGen cpg = methodGen.getConstantPool();
        if (edu.umd.cs.findbugs.ba.Hierarchy.isMonitorWait(location.getHandle().getInstruction(),cpg)) {
            int count = dataflow.getFactAtLocation(location).getNumLockedObjects();
            if (count > 1) {
// A wait with multiple locks held?
                java.lang.String sourceFile = javaClass.getSourceFileName();
                possibleWaitBugs.add(new edu.umd.cs.findbugs.BugInstance(this, "TLW_TWO_LOCK_WAIT", HIGH_PRIORITY).addClassAndMethod(methodGen,sourceFile).addSourceLine(classContext,methodGen,sourceFile,location.getHandle()));
            }
        }
        if (edu.umd.cs.findbugs.ba.Hierarchy.isMonitorNotify(location.getHandle().getInstruction(),cpg)) {
            int count = dataflow.getFactAtLocation(location).getNumLockedObjects();
            if (count > 1) {
// A notify with multiple locks held?
                java.lang.String sourceFile = javaClass.getSourceFileName();
                possibleNotifyLocations.add(edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,methodGen,sourceFile,location.getHandle()));
            }
        }
    }
    public void report() {
    }
}
// vim:ts=3
