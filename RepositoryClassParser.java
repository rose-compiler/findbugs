/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A special version of ClassParser that automatically enters parsed classes
 * into the Repository. This allows us to use the Repository to inspect the
 * class hierarchy, based on the current class path.
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.io.IOException;
import java.io.InputStream;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
public class RepositoryClassParser extends java.lang.Object {
    private org.apache.bcel.classfile.ClassParser classParser;
/**
     * Constructor.
     * 
     * @param inputStream
     *            the input stream from which to read the class file
     * @param fileName
     *            filename of the class file
     */
    public RepositoryClassParser(java.io.InputStream inputStream, java.lang.String fileName) {
        super();
        classParser = new org.apache.bcel.classfile.ClassParser(inputStream, fileName);
    }
/**
     * Constructor.
     * 
     * @param fileName
     *            name of the class file
     * @throws IOException
     *             if the file cannot be read
     */
    public RepositoryClassParser(java.lang.String fileName) {
        super();
        classParser = new org.apache.bcel.classfile.ClassParser(fileName);
    }
/**
     * Constructor.
     * 
     * @param zipFile
     *            name of a zip file containing the class
     * @param fileName
     *            name of the zip entry within the class
     * @throws IOException
     *             if the zip entry cannot be read
     */
    public RepositoryClassParser(java.lang.String zipFile, java.lang.String fileName) {
        super();
        classParser = new org.apache.bcel.classfile.ClassParser(zipFile, fileName);
    }
/**
     * Parse the class file into a JavaClass object. If succesful, the new
     * JavaClass is entered into the Repository.
     * 
     * @return the parsed JavaClass
     * @throws IOException
     *             if the class cannot be parsed
     */
    public org.apache.bcel.classfile.JavaClass parse() throws java.io.IOException {
        org.apache.bcel.classfile.JavaClass jclass = classParser.parse();
        org.apache.bcel.Repository.addClass(jclass);
        return jclass;
    }
}
// vim:ts=4
