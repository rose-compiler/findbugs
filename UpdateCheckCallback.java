package edu.umd.cs.findbugs.updates;
import edu.umd.cs.findbugs.updates.*;
import java.util.List;
import edu.umd.cs.findbugs.GlobalOptions;
abstract public interface UpdateCheckCallback extends edu.umd.cs.findbugs.GlobalOptions {
    abstract void pluginUpdateCheckComplete(java.util.List<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates, boolean force);
}
