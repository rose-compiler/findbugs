package edu.umd.cs.findbugs.ba.bcp;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.bcp.*;
/**
 * A PatternElement to match a method invocation. Currently, we don't allow
 * variables in this element (for arguments and return value). This would be a
 * good thing to add. We also don't distinguish between invokevirtual,
 * invokeinterface, and invokespecial.
 * <p/>
 * <p>
 * Invoke objects match by class name, method name, method signature, and
 * <em>mode</em>.
 * <p/>
 * <p>
 * Names and signatures may be matched in several ways:
 * <ol>
 * <li>By an exact match. This is the default behavior.
 * <li>By a regular expression. If the string provided to the Invoke constructor
 * begins with a "/" character, the rest of the string is treated as a regular
 * expression.
 * <li>As a subclass match. This only applies to class name matches. If the
 * first character of a class name string is "+", then the rest of the string is
 * treated as the name of a base class. Any subclass or subinterface of the
 * named type will be accepted.
 * </ol>
 * <p/>
 * <p>
 * The <em>mode</em> specifies what kind of invocations in the Invoke element
 * matches. It is specified as the bitwise combination of the following values:
 * <ol>
 * <li> <code>INSTANCE</code>, which matches ordinary instance method invocations
 * <li> <code>STATIC</code>, which matches static method invocations
 * <li> <code>CONSTRUCTOR</code>, which matches object constructor invocations
 * </ol>
 * The special mode <code>ORDINARY_METHOD</code> is equivalent to
 * <code>INSTANCE|STATIC</code>. The special mode <code>ANY</code> is equivalent
 * to <code>INSTANCE|STATIC|CONSTRUCTOR</code>.
 * 
 * @author David Hovemeyer
 * @see PatternElement
 */
/**
     * Match ordinary (non-constructor) instance invocations.
     */
/**
     * Match static invocations.
     */
/**
     * Match object constructor invocations.
     */
/**
     * Match ordinary methods (everything except constructors).
     */
/**
     * Match both static and instance invocations.
     */
/**
     * Constructor.
     * 
     * @param className
     *            the class name of the method; may be specified exactly, as a
     *            regexp, or as a subtype match
     * @param methodName
     *            the name of the method; may be specified exactly or as a
     *            regexp
     * @param methodSig
     *            the signature of the method; may be specified exactly or as a
     *            regexp
     * @param mode
     *            the mode of invocation
     */
// See if the instruction is an InvokeInstruction
// Intersection of actual and desired modes must be nonempty.
// Check class name, method name, and method signature.
// It's a match!
// vim:ts=4
abstract interface package-info {
}
