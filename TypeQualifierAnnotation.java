/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A type qualifier applied to a field, method, parameter, or return value.
 * 
 * @author Bill Pugh
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.jsr305;
import edu.umd.cs.findbugs.ba.jsr305.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.meta.When;
import edu.umd.cs.findbugs.util.DualKeyHashMap;
public class TypeQualifierAnnotation extends java.lang.Object {
    final public edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue typeQualifier;
    final public javax.annotation.meta.When when;
    public TypeQualifierAnnotation(edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue typeQualifier, javax.annotation.meta.When when) {
        super();
        this.typeQualifier = typeQualifier;
        this.when = when;
    }
    final public static edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation OVERRIDES_BUT_NO_ANNOTATION = new edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation(null, null) {
        public java.lang.String toString() {
            return "Overrides but no annotation";
        }
    };
// private static DualKeyHashMap <TypeQualifierValue, When,
// TypeQualifierAnnotation> map = new DualKeyHashMap <TypeQualifierValue,
// When, TypeQualifierAnnotation> ();
    private static java.lang.ThreadLocal<edu.umd.cs.findbugs.util.DualKeyHashMap<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue, javax.annotation.meta.When, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation>> instance = new java.lang.ThreadLocal<edu.umd.cs.findbugs.util.DualKeyHashMap<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue, javax.annotation.meta.When, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation>>() {
        protected edu.umd.cs.findbugs.util.DualKeyHashMap<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue, javax.annotation.meta.When, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation> initialValue() {
            return new edu.umd.cs.findbugs.util.DualKeyHashMap<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue, javax.annotation.meta.When, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation>();
        }
    };
    public static void clearInstance() {
        instance.remove();
    }
// public static synchronized @NonNull TypeQualifierAnnotation
// getValue(ClassDescriptor desc, Object value, When when) {
// return getValue(TypeQualifierValue.getValue(desc, value), when);
// }
// When lattice:
//
// In subtypes:
// - return value type must be at least as narrow as supertypes
// - parameter types must be at least as wide as supertypes
//
// TOP TOP is invalid as return type:
// / | \ means that no When value is narrow enough
// / | \ for combination of supertype annotations
// / | \
    // Always Unknown Never ^ Narrower
// \ | / |
// \ | / |
// \ | / |
// Maybe v Wider
//
    final private static javax.annotation.meta.When TOP = null;
    final private static javax.annotation.meta.When[][] combineReturnValueMatrix = {{javax.annotation.meta.When.ALWAYS}, {javax.annotation.meta.When.ALWAYS, javax.annotation.meta.When.UNKNOWN}, {javax.annotation.meta.When.ALWAYS, javax.annotation.meta.When.UNKNOWN, javax.annotation.meta.When.MAYBE}, {TOP, TOP, javax.annotation.meta.When.NEVER, javax.annotation.meta.When.NEVER}};
// ALWAYS UNKNOWN MAYBE NEVER
/* ALWAYS */
/* UNKNOWN */
/* MAYBE */
/* NEVER */
    final private static javax.annotation.meta.When[][] combineParameterMatrix = {{javax.annotation.meta.When.ALWAYS}, {javax.annotation.meta.When.UNKNOWN, javax.annotation.meta.When.UNKNOWN}, {javax.annotation.meta.When.MAYBE, javax.annotation.meta.When.MAYBE, javax.annotation.meta.When.MAYBE}, {javax.annotation.meta.When.MAYBE, javax.annotation.meta.When.UNKNOWN, javax.annotation.meta.When.MAYBE, javax.annotation.meta.When.NEVER}};
// ALWAYS UNKNOWN MAYBE NEVER
/* ALWAYS */
/* UNKNOWN */
/* MAYBE */
/* NEVER */
/**
     * Combine return type annotations.
     * 
     * @param a
     *            a TypeQualifierAnnotation used on a return value
     * @param b
     *            another TypeQualifierAnnotation used on a return value
     * @return combined return type annotation that is at least as narrow as
     *         both <code>a</code> or <code>b</code>, or null if no such
     *         TypeQualifierAnnotation exists
     */
    public static edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation combineReturnTypeAnnotations(edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation a, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation b) {
        return combineAnnotations(a,b,combineReturnValueMatrix);
    }
/**
     * 
     * @param a
     *            a TypeQualifierAnnotation used on a method parameter
     * @param b
     *            another TypeQualifierAnnotation used on a method parameter
     * @return combined parameter annotation that is at least as wide as both a
     *         and b
     */
    public static edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation combineParameterAnnotations(edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation a, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation b) {
        return combineAnnotations(a,b,combineParameterMatrix);
    }
    private static edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation combineAnnotations(edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation a, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation b, javax.annotation.meta.When[][] mergeMatrix) {
        assert a.typeQualifier.equals(b.typeQualifier);
        javax.annotation.meta.When aWhen = a.when;
        javax.annotation.meta.When bWhen = b.when;
        if (aWhen.ordinal() < bWhen.ordinal()) {
            javax.annotation.meta.When tmp = aWhen;
            aWhen = bWhen;
            bWhen = tmp;
        }
        javax.annotation.meta.When combined = mergeMatrix[aWhen.ordinal()][bWhen.ordinal()];
        if (combined != null) {
            return getValue(a.typeQualifier,combined);
        }
        else {
            return null;
        }
    }
    public static java.util.Collection<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation> getValues(java.util.Map<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue, javax.annotation.meta.When> map) {
        java.util.Collection<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation> result = new java.util.LinkedList<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation>();
        for (java.util.Map.Entry<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue, javax.annotation.meta.When> e : map.entrySet()){
            result.add(getValue(e.getKey(),e.getValue()));
        }
;
        return result;
    }
    public static edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation getValue(edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue desc, javax.annotation.meta.When when) {
        edu.umd.cs.findbugs.util.DualKeyHashMap<edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue, javax.annotation.meta.When, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation> map = instance.get();
        edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation result = map.get(desc,when);
        if (result != null) return result;
        result = new edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation(desc, when);
        map.put(desc,when,result);
        return result;
    }
    public int hashCode() {
        return typeQualifier.hashCode() * 37 + when.hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation)) return false;
        edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation other = (edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation) (o) ;
        return typeQualifier.equals(other.typeQualifier) && when.equals(other.when);
    }
    public java.lang.String toString() {
        return typeQualifier + ":" + when;
    }
}
