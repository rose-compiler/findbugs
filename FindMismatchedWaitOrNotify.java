/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.BitSet;
import java.util.Collection;
import java.util.Iterator;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.BugAccumulator;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.StatelessDetector;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.LockDataflow;
import edu.umd.cs.findbugs.ba.LockSet;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.ba.vna.ValueNumberFrame;
final public class FindMismatchedWaitOrNotify extends java.lang.Object implements edu.umd.cs.findbugs.Detector, edu.umd.cs.findbugs.StatelessDetector {
    final private edu.umd.cs.findbugs.BugReporter bugReporter;
    final private edu.umd.cs.findbugs.BugAccumulator bugAccumulator;
    public FindMismatchedWaitOrNotify(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
        this.bugAccumulator = new edu.umd.cs.findbugs.BugAccumulator(bugReporter);
    }
    public java.lang.Object clone() {
        try {
            return super.clone();
        }
        catch (java.lang.CloneNotSupportedException e){
            throw new java.lang.AssertionError(e);
        }
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.JavaClass jclass = classContext.getJavaClass();
        org.apache.bcel.classfile.Method[] methodList = jclass.getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
            if (methodGen == null) continue;
// Don't bother analyzing the method unless there is both locking
// and a method call.
            java.util.BitSet bytecodeSet = classContext.getBytecodeSet(method);
            if (bytecodeSet == null) continue;
            if ( !(bytecodeSet.get(org.apache.bcel.Constants.MONITORENTER) && bytecodeSet.get(org.apache.bcel.Constants.INVOKEVIRTUAL))) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("FindMismatchedWaitOrNotify: caught exception",e);
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("FindMismatchedWaitOrNotify: caught exception",e);
            }
        }
;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
        org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
        if (methodGen == null) return;
        org.apache.bcel.generic.ConstantPoolGen cpg = methodGen.getConstantPool();
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow = classContext.getValueNumberDataflow(method);
        edu.umd.cs.findbugs.ba.LockDataflow dataflow = classContext.getLockDataflow(method);
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.INVOKEVIRTUAL)) continue;
            org.apache.bcel.generic.INVOKEVIRTUAL inv = (org.apache.bcel.generic.INVOKEVIRTUAL) (ins) ;
            java.lang.String methodName = inv.getName(cpg);
            java.lang.String methodSig = inv.getSignature(cpg);
            if (edu.umd.cs.findbugs.ba.Hierarchy.isMonitorWait(methodName,methodSig) || edu.umd.cs.findbugs.ba.Hierarchy.isMonitorNotify(methodName,methodSig)) {
                int numConsumed = inv.consumeStack(cpg);
                if (numConsumed == org.apache.bcel.Constants.UNPREDICTABLE) throw new edu.umd.cs.findbugs.ba.DataflowAnalysisException("Unpredictable stack consumption", methodGen, handle);
                edu.umd.cs.findbugs.ba.vna.ValueNumberFrame frame = vnaDataflow.getFactAtLocation(location);
                if ( !frame.isValid()) continue;
                if (frame.getStackDepth() - numConsumed < 0) throw new edu.umd.cs.findbugs.ba.DataflowAnalysisException("Stack underflow", methodGen, handle);
                edu.umd.cs.findbugs.ba.vna.ValueNumber ref = frame.getValue(frame.getNumSlots() - numConsumed);
                edu.umd.cs.findbugs.ba.LockSet lockSet = dataflow.getFactAtLocation(location);
                int lockCount = lockSet.getLockCount(ref.getNumber());
                if (lockCount == 0) {
                    java.util.Collection<edu.umd.cs.findbugs.ba.vna.ValueNumber> lockedValueNumbers = lockSet.getLockedValueNumbers(frame);
                    boolean foundMatch = false;
                    for (edu.umd.cs.findbugs.ba.vna.ValueNumber v : lockedValueNumbers)if (frame.veryFuzzyMatch(ref,v)) {
                        foundMatch = true;
                        break;
                    }
;
                    if ( !foundMatch) {
                        java.lang.String type = methodName.equals("wait") ? "MWN_MISMATCHED_WAIT" : "MWN_MISMATCHED_NOTIFY";
                        java.lang.String sourceFile = classContext.getJavaClass().getSourceFileName();
                        int priority = method.isPublic() ? NORMAL_PRIORITY : LOW_PRIORITY;
                        bugAccumulator.accumulateBug(new edu.umd.cs.findbugs.BugInstance(this, type, priority).addClassAndMethod(methodGen,sourceFile),edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,methodGen,sourceFile,handle));
                    }
                }
            }
        }
        bugAccumulator.reportAccumulatedBugs();
    }
// Probably dead code
// Report as medium priority only if the method is
// public.
// Non-public methods may be properly locked in a
// calling context.
    public void report() {
    }
}
// vim:ts=3
