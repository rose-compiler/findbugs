/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Class representing the set of opcodes used in a method.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.BitSet;
import org.apache.bcel.Constants;
public class MethodBytecodeSet extends java.util.BitSet {
    public MethodBytecodeSet() {
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder("[");
        for (int i = this.nextSetBit(0); i >= 0; i = this.nextSetBit(i + 1)) {
            buf.append(org.apache.bcel.Constants.OPCODE_NAMES[i]).append(", ");
        }
        buf.setLength(buf.length() - 2);
        buf.append("]");
        return buf.toString();
    }
}
