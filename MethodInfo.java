package edu.umd.cs.findbugs.classfile.analysis;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.analysis.*;
/**
 * @author pugh
 */
/**
         * @param methodCallCount
         */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.XMethod#isReturnTypeReferenceType()
     */
/*
     * (non-Javadoc)
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#getAccessFlags()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isFinal()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isPrivate()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isProtected()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isPublic()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isResolved()
     */
/**
     * Destructively add an annotation. We do this for "built-in" annotations
     * that might not be directly evident in the code. It's not a great idea in
     * general, but we can get away with it as long as it's done early enough
     * (i.e., before anyone asks what annotations this method has.)
     *
     * @param annotationValue
     *            an AnnotationValue representing a method annotation
     */
/**
     * Destructively add a parameter annotation.
     *
     * @param param
     *            parameter (0 == first parameter)
     * @param annotationValue
     *            an AnnotationValue representing a parameter annotation
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.XMethod#getMethodDescriptor()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.XMethod#isVarArgs()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.XMethod#usesConcurrency()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.XMethod#bridgeFrom()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.XMethod#bridgeTo()
     */
abstract interface package-info {
}
