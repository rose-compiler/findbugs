package edu.umd.cs.findbugs.ba.bcp;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.bcp.*;
/**
 * Match a ByteCodePattern against the code of a method, represented by a CFG.
 * Produces some number of ByteCodePatternMatch objects, which indicate how the
 * pattern matched the bytecode instructions in the method.
 * <p/>
 * <p>
 * This code is a hack and should probably be rewritten.
 * 
 * @author David Hovemeyer
 * @see ByteCodePattern
 */
/**
     * Constructor.
     * 
     * @param pattern
     *            the ByteCodePattern to look for examples of
     * @param classContext
     *            ClassContext for the class to analyze
     * @param method
     *            the Method to analyze
     */
/**
     * Search for examples of the ByteCodePattern.
     * 
     * @return this object
     * @throws DataflowAnalysisException
     *             if the ValueNumberAnalysis did not produce useful values for
     *             the method
     */
// Scan instructions of basic block for possible matches
// Add successors of the basic block (which haven't been visited
// already)
/**
     * Return an Iterator over the ByteCodePatternMatch objects representing
     * successful matches of the ByteCodePattern.
     */
/**
     * Attempt to begin a match.
     * 
     * @param basicBlock
     *            the basic block
     * @param instructionIterator
     *            the instruction iterator positioned just before the first
     *            instruction to be matched
     */
// For debugging - number the paths through the CFG
/**
     * Object representing the current state of the matching algorithm. Provides
     * convenient methods to implement the various steps of the algorithm.
     */
/**
         * Constructor. Builds the start state.
         * 
         * @param basicBlock
         *            the initial basic block
         * @param instructionIterator
         *            the instructionIterator indicating where to start matching
         * @param patternElement
         *            the first PatternElement of the pattern
         */
/**
         * Constructor.
         */
/**
         * Make an exact copy of this object.
         */
/**
         * Get basic block.
         */
/**
         * Get current pattern element.
         */
/**
         * Get current pattern element match.
         */
/**
         * Determine if the match is complete.
         */
// We're done when we reach the end of the chain
// of pattern elements.
/**
         * Get a ByteCodePatternMatch representing the complete match.
         */
/**
         * Try to produce a new state that will finish matching the current
         * element and start matching the next element. Returns null if the
         * current element is not complete.
         */
// Current element is not complete, or we already
// forked at this point
// Create state to advance to matching next pattern element
// at current basic block and instruction.
// Now that this state has forked from this element
// at this instruction, it must not do so again.
/**
         * Determine if the current pattern element can continue to match
         * instructions.
         */
/**
         * Determine if there are more instructions in the same basic block.
         */
/**
         * Match current pattern element with next instruction in basic block.
         * Returns MatchResult if match succeeds, null otherwise.
         */
// Move to location of next instruction to be matched
/**
         * Determine if it is possible to continue matching in a successor basic
         * block.
         */
/**
         * Get most recently matched instruction.
         */
/**
         * Return a new State for continuing the overall pattern match in a
         * successor basic block.
         * 
         * @param edge
         *            the Edge leading to the successor basic block
         * @param matchResult
         *            a MatchResult representing the match of the last
         *            instruction in the predecessor block; null if none
         */
// If we have just matched an instruction, then we allow the
// matching PatternElement to choose which edges are acceptable.
// This allows PatternElements to select particular control edges;
// for example, only continue the pattern on the true branch
// of an "if" comparison.
/**
         * Determine if we need to look for a dominated instruction at this
         * point in the search.
         */
/**
         * Return Iterator over states representing dominated instructions that
         * continue the match.
         */
// Find the referenced instruction.
// Find all basic blocks dominated by the dominator block.
// This block is dominated by the dominator block.
// Each instruction in the block which matches the
// current pattern
// element is a new state continuing the match.
// Get the ValueNumberFrames before and after the instruction
// Try to match the instruction against the pattern element.
// Successful match!
// Update state to reflect that the match has occurred.
/**
     * Match a sequence of pattern elements, starting at the given one. The
     * InstructionIterator should generally be positioned just before the next
     * instruction to be matched. However, it may be positioned at the end of a
     * basic block, in which case nothing will happen except that we will try to
     * continue the match in the non-backedge successors of the basic block.
     */
// Have we reached the end of the pattern?
// This is a complete match.
// If we've reached the minimum number of occurrences for this
// pattern element, we can advance to the next pattern element
// without trying
// to match this instruction again. We make sure that we only
// advance to
// the next element once for this matchCount.
// If we've reached the maximum number of occurrences for this
// pattern element, then we can't continue.
// Are we looking for an instruction dominated by an earlier
// matched instruction?
// Is there another instruction in this basic block?
// Try to match it.
// Continue the match at each successor instruction,
// using the same PatternElement.
// Easy case; continue matching in the same basic block.
// We've reached the end of the basic block.
// Try to advance to the successors of this basic block,
// ignoring loop backedges.
// CFGs can have duplicate edges
// See if we can continue matching in the successor basic
// block.
// vim:ts=4
abstract interface package-info {
}
