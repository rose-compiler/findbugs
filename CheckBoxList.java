/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * A list of JCheckBoxes! How convenient!
 * 
 * Adapted from: http://www.devx.com/tips/Tip/5342
 * 
 * @author Trevor Harmon
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
public class CheckBoxList extends javax.swing.JList {
    protected class CellRenderer extends java.lang.Object implements javax.swing.ListCellRenderer {
        public CellRenderer() {
        }
        public java.awt.Component getListCellRendererComponent(javax.swing.JList list, java.lang.Object value, int index, boolean isSelected, boolean cellHasFocus) {
            javax.swing.JCheckBox checkbox = (javax.swing.JCheckBox) (value) ;
            checkbox.setBackground(isSelected ? edu.umd.cs.findbugs.gui2.CheckBoxList.this.getSelectionBackground() : edu.umd.cs.findbugs.gui2.CheckBoxList.this.getBackground());
            checkbox.setForeground(isSelected ? edu.umd.cs.findbugs.gui2.CheckBoxList.this.getSelectionForeground() : edu.umd.cs.findbugs.gui2.CheckBoxList.this.getForeground());
            checkbox.setEnabled(edu.umd.cs.findbugs.gui2.CheckBoxList.this.isEnabled());
            checkbox.setFont(edu.umd.cs.findbugs.gui2.CheckBoxList.this.getFont());
            checkbox.setFocusPainted(false);
            checkbox.setBorderPainted(true);
            checkbox.setBorder(isSelected ? javax.swing.UIManager.getBorder("List.focusCellHighlightBorder") : noFocusBorder);
            return checkbox;
        }
    }
    private static javax.swing.border.Border noFocusBorder = new javax.swing.border.EmptyBorder(1, 1, 1, 1);
    public CheckBoxList() {
        super();
        this.setCellRenderer(new edu.umd.cs.findbugs.gui2.CheckBoxList.CellRenderer());
        this.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent e) {
                int index = edu.umd.cs.findbugs.gui2.CheckBoxList.this.locationToIndex(e.getPoint());
                if (index !=  -1) {
                    javax.swing.JCheckBox checkbox = (javax.swing.JCheckBox) (edu.umd.cs.findbugs.gui2.CheckBoxList.this.getModel().getElementAt(index)) ;
                    checkbox.setSelected( !checkbox.isSelected());
                    edu.umd.cs.findbugs.gui2.CheckBoxList.this.repaint();
                }
            }
        });
        this.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    }
    public CheckBoxList(java.lang.Object[] list) {
        this();
        this.setListData(list);
    }
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (int i = 0; i < this.getModel().getSize(); i++) ((javax.swing.JCheckBox) (this.getModel().getElementAt(i)) ).setEnabled(enabled);
    }
}
