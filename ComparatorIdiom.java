/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.DeepSubtypeAnalysis;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.ch.Subtypes2;
import edu.umd.cs.findbugs.util.ClassName;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
public class ComparatorIdiom extends edu.umd.cs.findbugs.visitclass.PreorderVisitor implements edu.umd.cs.findbugs.Detector {
    edu.umd.cs.findbugs.BugReporter bugReporter;
    public ComparatorIdiom(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        classContext.getJavaClass().accept(this);
    }
    public void visit(org.apache.bcel.classfile.JavaClass obj) {
        if (edu.umd.cs.findbugs.ba.ch.Subtypes2.instanceOf(obj,"java.util.Comparator") &&  !edu.umd.cs.findbugs.util.ClassName.isAnonymous(this.getClassName()) &&  !edu.umd.cs.findbugs.ba.ch.Subtypes2.instanceOf(obj,"java.io.Serializable")) {
            int priority = NORMAL_PRIORITY;
            if (obj.isInterface() || obj.isAbstract()) return;
            double easilySerializable = 1.0;
            for (org.apache.bcel.classfile.Field f : obj.getFields()){
                try {
                    if (f.getName().startsWith("this$")) return;
                    java.lang.String signature = f.getSignature();
                    char firstChar = signature.charAt(0);
                    if (firstChar == '\u004c' || firstChar == '\u005b') easilySerializable *= edu.umd.cs.findbugs.DeepSubtypeAnalysis.isDeepSerializable(signature);
                }
                catch (java.lang.ClassNotFoundException e){
                    easilySerializable = 0.0;
                    break;
                }
            }
;
            if (easilySerializable < 0.9) priority = LOW_PRIORITY;
            bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "SE_COMPARATOR_SHOULD_BE_SERIALIZABLE", priority).addClass(this));
        }
    }
    public void report() {
    }
}
