/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import javax.annotation.Nonnull;
public class InnerClassAccess extends java.lang.Object {
    private java.lang.String methodName;
    private java.lang.String methodSig;
    private edu.umd.cs.findbugs.ba.XField field;
    private boolean isLoad;
    public InnerClassAccess(java.lang.String methodName, java.lang.String methodSig, edu.umd.cs.findbugs.ba.XField field, boolean isLoad) {
        super();
        this.methodName = methodName;
        this.methodSig = methodSig;
        this.field = field;
        this.isLoad = isLoad;
    }
    public java.lang.String getMethodName() {
        return methodName;
    }
    public java.lang.String getMethodSignature() {
        return methodSig;
    }
    public edu.umd.cs.findbugs.ba.XField getField() {
        return field;
    }
    public boolean isStatic() {
        return field.isStatic();
    }
    public boolean isLoad() {
        return isLoad;
    }
}
// vim:ts=4
