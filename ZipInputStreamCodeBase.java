package edu.umd.cs.findbugs.classfile.impl;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.impl.*;
/**
 * Implementation of ICodeBase to read from a zip file or jar file.
 * 
 * @author David Hovemeyer
 */
/**
     * Constructor.
     * 
     * @param codeBaseLocator
     *            the codebase locator for this codebase
     * @param file
     *            the File containing the zip file (may be a temp file if the
     *            codebase was copied from a nested zipfile in another codebase)
     */
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.ICodeBase#lookupResource(java.lang.String)
     */
// Translate resource name, in case a resource name
// has been overridden and the resource is being accessed
// using the overridden name.
/*
         * (non-Javadoc)
         * 
         * @see edu.umd.cs.findbugs.classfile.ICodeBaseIterator#hasNext()
         */
/*
         * (non-Javadoc)
         * 
         * @see edu.umd.cs.findbugs.classfile.ICodeBaseIterator#next()
         */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getPathName()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#close()
     */
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
abstract interface package-info {
}
