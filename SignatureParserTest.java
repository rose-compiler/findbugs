package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.Iterator;
import junit.framework.Assert;
import junit.framework.TestCase;
public class SignatureParserTest extends junit.framework.TestCase {
    public SignatureParserTest() {
    }
    edu.umd.cs.findbugs.ba.SignatureParser noParams;
    edu.umd.cs.findbugs.ba.SignatureParser manyParams;
    protected void setUp() {
        noParams = new edu.umd.cs.findbugs.ba.SignatureParser("()V");
        manyParams = new edu.umd.cs.findbugs.ba.SignatureParser("(IJFDZLjava/lang/String;B)Ljava/lang/Object;");
    }
    public void testNoParams() {
        java.util.Iterator<java.lang.String> i = noParams.parameterSignatureIterator();
        junit.framework.Assert.assertFalse(i.hasNext());
    }
    public void testManyParams() {
        java.util.Iterator<java.lang.String> i = manyParams.parameterSignatureIterator();
        junit.framework.Assert.assertTrue(i.hasNext());
        junit.framework.Assert.assertEquals(i.next(),"I");
        junit.framework.Assert.assertTrue(i.hasNext());
        junit.framework.Assert.assertEquals(i.next(),"J");
        junit.framework.Assert.assertTrue(i.hasNext());
        junit.framework.Assert.assertEquals(i.next(),"F");
        junit.framework.Assert.assertTrue(i.hasNext());
        junit.framework.Assert.assertEquals(i.next(),"D");
        junit.framework.Assert.assertTrue(i.hasNext());
        junit.framework.Assert.assertEquals(i.next(),"Z");
        junit.framework.Assert.assertTrue(i.hasNext());
        junit.framework.Assert.assertEquals(i.next(),"Ljava/lang/String;");
        junit.framework.Assert.assertTrue(i.hasNext());
        junit.framework.Assert.assertEquals(i.next(),"B");
        junit.framework.Assert.assertFalse(i.hasNext());
    }
}
// vim:ts=4
