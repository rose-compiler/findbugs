/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An implementation of org.apache.bcel.util.Repository that uses the
 * AnalysisCache as its backing store.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.util.ClassPath;
import org.apache.bcel.util.Repository;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.internalAnnotations.SlashedClassName;
import edu.umd.cs.findbugs.util.ClassName;
public class AnalysisCacheToRepositoryAdapter extends java.lang.Object implements org.apache.bcel.util.Repository {
/**
     * Constructor.
     */
    public AnalysisCacheToRepositoryAdapter() {
        super();
    }
/*
     * (non-Javadoc)
     * 
     * @see org.apache.bcel.util.Repository#clear()
     */
    public void clear() {
        throw new java.lang.UnsupportedOperationException();
    }
/*
     * (non-Javadoc)
     * 
     * @see org.apache.bcel.util.Repository#findClass(java.lang.String)
     */
    public org.apache.bcel.classfile.JavaClass findClass(java.lang.String className) {
        java.lang.String slashedClassName = edu.umd.cs.findbugs.util.ClassName.toSlashedClassName(className);
        edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor = edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getClassDescriptor(slashedClassName);
        return edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().probeClassAnalysis(org.apache.bcel.classfile.JavaClass.class,classDescriptor);
    }
/*
     * (non-Javadoc)
     * 
     * @see org.apache.bcel.util.Repository#getClassPath()
     */
    public org.apache.bcel.util.ClassPath getClassPath() {
        throw new java.lang.UnsupportedOperationException();
    }
/*
     * (non-Javadoc)
     * 
     * @see org.apache.bcel.util.Repository#loadClass(java.lang.String)
     */
    public org.apache.bcel.classfile.JavaClass loadClass(java.lang.String className) throws java.lang.ClassNotFoundException {
        if (className.length() == 0) throw new java.lang.IllegalArgumentException("Request to load empty class");
        className = edu.umd.cs.findbugs.util.ClassName.toSlashedClassName(className);
        edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor = edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getClassDescriptor(className);
        try {
            return edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getClassAnalysis(org.apache.bcel.classfile.JavaClass.class,classDescriptor);
        }
        catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
            throw new java.lang.ClassNotFoundException("Exception while looking for class " + className, e);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see org.apache.bcel.util.Repository#loadClass(java.lang.Class)
     */
    public org.apache.bcel.classfile.JavaClass loadClass(java.lang.Class cls) throws java.lang.ClassNotFoundException {
        return this.loadClass(cls.getName());
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.bcel.util.Repository#removeClass(org.apache.bcel.classfile
     * .JavaClass)
     */
    public void removeClass(org.apache.bcel.classfile.JavaClass arg0) {
        throw new java.lang.UnsupportedOperationException();
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.bcel.util.Repository#storeClass(org.apache.bcel.classfile.
     * JavaClass)
     */
    public void storeClass(org.apache.bcel.classfile.JavaClass cls) {
        throw new java.lang.UnsupportedOperationException();
    }
}
