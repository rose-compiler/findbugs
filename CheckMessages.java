/*
 * Check FindBugs XML message files
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Ensure that the XML messages files in a FindBugs plugin are valid and
 * complete.
 */
package edu.umd.cs.findbugs.tools.xml;
import edu.umd.cs.findbugs.tools.xml.*;
import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import edu.umd.cs.findbugs.xml.XMLUtil;
public class CheckMessages extends java.lang.Object {
    private static class CheckMessagesException extends org.dom4j.DocumentException {
/**
         *
         */
        final private static long serialVersionUID = 1L;
        public CheckMessagesException(java.lang.String msg, edu.umd.cs.findbugs.tools.xml.CheckMessages.XMLFile xmlFile, org.dom4j.Node node) {
            super("In " + xmlFile.getFilename() + ", " + node.toString() + ": " + msg);
        }
        public CheckMessagesException(java.lang.String msg, edu.umd.cs.findbugs.tools.xml.CheckMessages.XMLFile xmlFile) {
            super("In " + xmlFile.getFilename() + ": " + msg);
        }
    }
    private static class XMLFile extends java.lang.Object {
        private java.lang.String filename;
        private org.dom4j.Document document;
        public XMLFile(java.lang.String filename) throws org.dom4j.DocumentException {
            super();
            this.filename = filename;
            java.io.File file = new java.io.File(filename);
            org.dom4j.io.SAXReader saxReader = new org.dom4j.io.SAXReader();
            this.document = saxReader.read(file);
        }
        public java.lang.String getFilename() {
            return filename;
        }
        public org.dom4j.Document getDocument() {
            return document;
        }
/**
         * Get iterator over Nodes selected by given XPath expression.
         */
        public java.util.Iterator<org.dom4j.Node> xpathIterator(java.lang.String xpath) {
            java.util.List<org.dom4j.Node> nodes = edu.umd.cs.findbugs.xml.XMLUtil.selectNodes(document,xpath);
            return nodes.iterator();
        }
/**
         * Build collection of the values of given attribute in all nodes
         * matching given XPath expression.
         */
        public java.util.Set<java.lang.String> collectAttributes(java.lang.String xpath, java.lang.String attrName) throws org.dom4j.DocumentException {
            java.util.Set<java.lang.String> result = new java.util.HashSet<java.lang.String>();
            for (java.util.Iterator<org.dom4j.Node> i = this.xpathIterator(xpath); i.hasNext(); ) {
                org.dom4j.Node node = i.next();
                java.lang.String value = this.checkAttribute(node,attrName).getValue();
                result.add(value);
            }
            return result;
        }
        public org.dom4j.Attribute checkAttribute(org.dom4j.Node node, java.lang.String attrName) throws org.dom4j.DocumentException {
            if ( !(node instanceof org.dom4j.Element)) throw new edu.umd.cs.findbugs.tools.xml.CheckMessages.CheckMessagesException("Node is not an element", this, node);
            org.dom4j.Element element = (org.dom4j.Element) (node) ;
            org.dom4j.Attribute attr = element.attribute(attrName);
            if (attr == null) throw new edu.umd.cs.findbugs.tools.xml.CheckMessages.CheckMessagesException("Missing " + attrName + " attribute", this, node);
            return attr;
        }
        public org.dom4j.Element checkElement(org.dom4j.Node node, java.lang.String elementName) throws org.dom4j.DocumentException {
            if ( !(node instanceof org.dom4j.Element)) throw new edu.umd.cs.findbugs.tools.xml.CheckMessages.CheckMessagesException("Node is not an element", this, node);
            org.dom4j.Element element = (org.dom4j.Element) (node) ;
            org.dom4j.Element child = element.element(elementName);
            if (child == null) throw new edu.umd.cs.findbugs.tools.xml.CheckMessages.CheckMessagesException("Missing " + elementName + " element", this, node);
            return child;
        }
        public java.lang.String checkNonEmptyText(org.dom4j.Node node) throws org.dom4j.DocumentException {
            if ( !(node instanceof org.dom4j.Element)) throw new edu.umd.cs.findbugs.tools.xml.CheckMessages.CheckMessagesException("Node is not an element", this, node);
            org.dom4j.Element element = (org.dom4j.Element) (node) ;
            java.lang.String text = element.getText();
            if (text.equals("")) throw new edu.umd.cs.findbugs.tools.xml.CheckMessages.CheckMessagesException("Empty text in element", this, node);
            return text;
        }
    }
    private java.util.Set<java.lang.String> declaredDetectorsSet;
    private java.util.Set<java.lang.String> declaredAbbrevsSet;
    public CheckMessages(java.lang.String pluginDescriptorFilename) throws org.dom4j.DocumentException {
        super();
        edu.umd.cs.findbugs.tools.xml.CheckMessages.XMLFile pluginDescriptorDoc = new edu.umd.cs.findbugs.tools.xml.CheckMessages.XMLFile(pluginDescriptorFilename);
        declaredDetectorsSet = pluginDescriptorDoc.collectAttributes("/FindbugsPlugin/Detector","class");
        declaredAbbrevsSet = pluginDescriptorDoc.collectAttributes("/FindbugsPlugin/BugPattern","abbrev");
    }
/**
     * Check given messages file for validity.
     * 
     * @throws DocumentException
     *             if the messages file is invalid
     */
    public void checkMessages(edu.umd.cs.findbugs.tools.xml.CheckMessages.XMLFile messagesDoc) throws org.dom4j.DocumentException {
        for (java.util.Iterator<org.dom4j.Node> i = messagesDoc.xpathIterator("/MessageCollection/Detector"); i.hasNext(); ) {
            org.dom4j.Node node = i.next();
            messagesDoc.checkAttribute(node,"class");
            messagesDoc.checkElement(node,"Details");
        }
        for (java.util.Iterator<org.dom4j.Node> i = messagesDoc.xpathIterator("/MessageCollection/BugPattern"); i.hasNext(); ) {
            org.dom4j.Node node = i.next();
            messagesDoc.checkAttribute(node,"type");
            messagesDoc.checkElement(node,"ShortDescription");
            messagesDoc.checkElement(node,"LongDescription");
            messagesDoc.checkElement(node,"Details");
        }
        for (java.util.Iterator<org.dom4j.Node> i = messagesDoc.xpathIterator("/MessageCollection/BugCode"); i.hasNext(); ) {
            org.dom4j.Node node = i.next();
            messagesDoc.checkAttribute(node,"abbrev");
            messagesDoc.checkNonEmptyText(node);
        }
// Detector elements must all have a class attribute
// and details child element.
// BugPattern elements must all have type attribute
// and ShortDescription, LongDescription, and Details
// child elements.
// BugCode elements must contain abbrev attribute
// and have non-empty text
// Check that all Detectors are described
        java.util.Set<java.lang.String> describedDetectorsSet = messagesDoc.collectAttributes("/MessageCollection/Detector","class");
        this.checkDescribed("Bug detectors not described by Detector elements",messagesDoc,declaredDetectorsSet,describedDetectorsSet);
// Check that all BugCodes are described
        java.util.Set<java.lang.String> describedAbbrevsSet = messagesDoc.collectAttributes("/MessageCollection/BugCode","abbrev");
        this.checkDescribed("Abbreviations not described by BugCode elements",messagesDoc,declaredAbbrevsSet,describedAbbrevsSet);
    }
    public void checkDescribed(java.lang.String description, edu.umd.cs.findbugs.tools.xml.CheckMessages.XMLFile xmlFile, java.util.Set<java.lang.String> declared, java.util.Set<java.lang.String> described) throws org.dom4j.DocumentException {
        java.util.Set<java.lang.String> notDescribed = new java.util.HashSet<java.lang.String>();
        notDescribed.addAll(declared);
        notDescribed.removeAll(described);
        if ( !notDescribed.isEmpty()) throw new edu.umd.cs.findbugs.tools.xml.CheckMessages.CheckMessagesException(description + ": " + notDescribed.toString(), xmlFile);
    }
    public static void main(java.lang.String[] argv) throws java.lang.Exception {
        if (argv.length < 2) {
            java.lang.System.err.println("Usage: " + edu.umd.cs.findbugs.tools.xml.CheckMessages.class.getName() + " <plugin descriptor xml> <bug description xml> [<bug description xml>...]");
            java.lang.System.exit(1);
        }
        java.lang.String pluginDescriptor = argv[0];
        try {
            edu.umd.cs.findbugs.tools.xml.CheckMessages checkMessages = new edu.umd.cs.findbugs.tools.xml.CheckMessages(pluginDescriptor);
            for (int i = 1; i < argv.length; ++i) {
                java.lang.String messagesFile = argv[i];
                java.lang.System.out.println("Checking messages file " + messagesFile);
                checkMessages.checkMessages(new edu.umd.cs.findbugs.tools.xml.CheckMessages.XMLFile(messagesFile));
            }
        }
        catch (org.dom4j.DocumentException e){
            java.lang.System.err.println("Could not verify messages files: " + e.getMessage());
            java.lang.System.exit(1);
        }
        java.lang.System.out.println("Messages files look OK!");
    }
}
// vim:ts=3
