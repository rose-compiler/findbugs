/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.jaif;
import edu.umd.cs.findbugs.jaif.*;
import java.io.StringReader;
import junit.framework.TestCase;
public class JAIFScannerTest extends junit.framework.TestCase {
    public JAIFScannerTest() {
    }
    private edu.umd.cs.findbugs.jaif.JAIFScanner getScanner(java.lang.String text) {
        return new edu.umd.cs.findbugs.jaif.JAIFScanner(new java.io.StringReader(text));
    }
    private void checkToken(edu.umd.cs.findbugs.jaif.JAIFScanner scanner, java.lang.String lexeme, edu.umd.cs.findbugs.jaif.JAIFTokenKind kind) throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.nextToken();
        assertEquals(lexeme,t.lexeme);
        assertEquals(kind,t.kind);
    }
    public void testScanColon() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner(":");
        this.checkToken(scanner,":",edu.umd.cs.findbugs.jaif.JAIFTokenKind.COLON);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanParens() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("()");
        this.checkToken(scanner,"(",edu.umd.cs.findbugs.jaif.JAIFTokenKind.LPAREN);
        this.checkToken(scanner,")",edu.umd.cs.findbugs.jaif.JAIFTokenKind.RPAREN);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanComma() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner(",");
        this.checkToken(scanner,",",edu.umd.cs.findbugs.jaif.JAIFTokenKind.COMMA);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanEquals() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("=");
        this.checkToken(scanner,"=",edu.umd.cs.findbugs.jaif.JAIFTokenKind.EQUALS);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanIdentifier() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("  	  		@foobar Baz123   ( Boing Boing) @Yum@Yum __123  $plotz");
        this.checkToken(scanner,"@foobar",edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.checkToken(scanner,"Baz123",edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.checkToken(scanner,"(",edu.umd.cs.findbugs.jaif.JAIFTokenKind.LPAREN);
        this.checkToken(scanner,"Boing",edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.checkToken(scanner,"Boing",edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.checkToken(scanner,")",edu.umd.cs.findbugs.jaif.JAIFTokenKind.RPAREN);
        this.checkToken(scanner,"@Yum",edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.checkToken(scanner,"@Yum",edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.checkToken(scanner,"__123",edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.checkToken(scanner,"$plotz",edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanFloatingPointLiteral() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("1e1f    2.f     .3f     0f      3.14f   6.022137e+23f");
        this.checkToken(scanner,"1e1f",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"2.f",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,".3f",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"0f",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"3.14f",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"6.022137e+23f",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanFloatingPointLiteral2() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("1e1     2.      .3      0.0     3.14    1e-9d   1e137");
        this.checkToken(scanner,"1e1",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"2.",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,".3",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"0.0",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"3.14",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"1e-9d",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"1e137",edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanOctalLiteral() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("0237   01575L  027365l");
        this.checkToken(scanner,"0237",edu.umd.cs.findbugs.jaif.JAIFTokenKind.OCTAL_LITERAL);
        this.checkToken(scanner,"01575L",edu.umd.cs.findbugs.jaif.JAIFTokenKind.OCTAL_LITERAL);
        this.checkToken(scanner,"027365l",edu.umd.cs.findbugs.jaif.JAIFTokenKind.OCTAL_LITERAL);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanHexLiteral() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("0xDEADbeef   0xcafeBabeL   0X123EEfl");
        this.checkToken(scanner,"0xDEADbeef",edu.umd.cs.findbugs.jaif.JAIFTokenKind.HEX_LITERAL);
        this.checkToken(scanner,"0xcafeBabeL",edu.umd.cs.findbugs.jaif.JAIFTokenKind.HEX_LITERAL);
        this.checkToken(scanner,"0X123EEfl",edu.umd.cs.findbugs.jaif.JAIFTokenKind.HEX_LITERAL);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanDecimalLiteral() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("1234     5678L    91919191l");
        this.checkToken(scanner,"1234",edu.umd.cs.findbugs.jaif.JAIFTokenKind.DECIMAL_LITERAL);
        this.checkToken(scanner,"5678L",edu.umd.cs.findbugs.jaif.JAIFTokenKind.DECIMAL_LITERAL);
        this.checkToken(scanner,"91919191l",edu.umd.cs.findbugs.jaif.JAIFTokenKind.DECIMAL_LITERAL);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
    public void testScanStringLiteral() throws java.lang.Exception {
        edu.umd.cs.findbugs.jaif.JAIFScanner scanner = this.getScanner("\"hello\"    \"foobie bletch\"  \"\\\"\"  \"\\\\\\6\\45\\037\"  \"\\b\\t\\f\\n\"  ");
        this.checkToken(scanner,"\"hello\"",edu.umd.cs.findbugs.jaif.JAIFTokenKind.STRING_LITERAL);
        this.checkToken(scanner,"\"foobie bletch\"",edu.umd.cs.findbugs.jaif.JAIFTokenKind.STRING_LITERAL);
        this.checkToken(scanner,"\"\\\"\"",edu.umd.cs.findbugs.jaif.JAIFTokenKind.STRING_LITERAL);
        this.checkToken(scanner,"\"\\\\\\6\\45\\037\"",edu.umd.cs.findbugs.jaif.JAIFTokenKind.STRING_LITERAL);
        this.checkToken(scanner,"\"\\b\\t\\f\\n\"",edu.umd.cs.findbugs.jaif.JAIFTokenKind.STRING_LITERAL);
        this.checkToken(scanner,"\n",edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE);
    }
}
