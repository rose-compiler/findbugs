/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import org.apache.bcel.Constants;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.FieldDescriptor;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
abstract public class AbstractField extends edu.umd.cs.findbugs.ba.AbstractClassMember implements edu.umd.cs.findbugs.ba.XField {
    public AbstractField(java.lang.String className, java.lang.String fieldName, java.lang.String fieldSig, int accessFlags) {
        super(className,fieldName,fieldSig,accessFlags);
    }
    public boolean isVolatile() {
        return (this.getAccessFlags() & org.apache.bcel.Constants.ACC_VOLATILE) != 0;
    }
    final public boolean isSynthetic() {
        return (this.getAccessFlags() & org.apache.bcel.Constants.ACC_SYNTHETIC) != 0;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.XField#getFieldDescriptor()
     */
    public edu.umd.cs.findbugs.classfile.FieldDescriptor getFieldDescriptor() {
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getFieldDescriptor(this.getClassDescriptor().getClassName(),this.getName(),this.getSignature(),this.isStatic());
    }
}
// vim:ts=4
