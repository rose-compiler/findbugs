package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Prune a CFG to remove infeasible exception edges. In order to determine what
 * kinds of exceptions can be thrown by explicit ATHROW instructions, type
 * analysis must first be performed on the unpruned CFG.
 * 
 * @author David Hovemeyer
 * @see CFG
 * @see edu.umd.cs.findbugs.ba.type.TypeAnalysis
 */
/**
     * A momento to remind us of how we classified a particular exception edge.
     * If pruning and classifying succeeds, then these momentos can be applied
     * to actually change the state of the edges. The issue is that the entire
     * pruning/classifying operation must either fail or succeed as a whole.
     * Thus, we don't commit any CFG changes until we know everything was
     * successful.
     */
/**
     * Constructor.
     * 
     * @param cfg
     *            the CFG to prune
     * @param methodGen
     *            the method
     * @param typeDataflow
     *            initialized TypeDataflow object for the CFG, indicating the
     *            types of all stack locations
     */
/**
     * Prune infeasible exception edges from the CFG. If the method returns
     * normally, then the operation was successful, and the CFG should no longer
     * contain infeasible exception edges. If ClassNotFoundException or
     * DataflowAnalysisException are thrown, then the operation was
     * unsuccessful,
     * <em>but the CFG is still valid because it was not modified</em>. If a
     * runtime exception is thrown, then the CFG may be partially modified and
     * should be considered invalid.
     */
// Mark edges to delete,
// mark edges to set properties of
// No exceptions are actually thrown on this edge,
// so we can delete the edge.
// Some exceptions appear to be thrown on the edge.
// Mark to indicate if any of the exceptions are checked,
// and if any are explicit (checked or explicitly declared
// or thrown unchecked).
// Remove deleted edges
// Mark edges
/**
     * @return true if modified
     */
// vim:ts=4
abstract interface package-info {
}
