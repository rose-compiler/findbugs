/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import javax.annotation.CheckForNull;
import edu.umd.cs.findbugs.Detector;
public class CheckReturnValueAnnotation extends edu.umd.cs.findbugs.ba.AnnotationEnumeration<edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation> {
    final int priority;
    final java.lang.String pattern;
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_UNKNOWN = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("UnknownCheckReturnValue", 0, edu.umd.cs.findbugs.Priorities.EXP_PRIORITY);
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_HIGH = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("CheckReturnValueHigh", 1, edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY);
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_MEDIUM = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("CheckReturnValue", 2, edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY);
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_LOW = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("CheckReturnValueLow", 3, edu.umd.cs.findbugs.Priorities.LOW_PRIORITY);
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_IGNORE = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("OkToIgnoreReturnValue", 4, edu.umd.cs.findbugs.Priorities.IGNORE_PRIORITY);
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_VERY_HIGH = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("CheckReturnValueVeryHigh", 5, edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY - 1);
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_LOW_BAD_PRACTICE = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("CheckReturnValueLowBadPractice", 6, "RV_RETURN_VALUE_IGNORED_BAD_PRACTICE", edu.umd.cs.findbugs.Priorities.LOW_PRIORITY);
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_MEDIUM_BAD_PRACTICE = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("CheckReturnValueMediumBadPractice", 7, "RV_RETURN_VALUE_IGNORED_BAD_PRACTICE", edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY);
    final public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation CHECK_RETURN_VALUE_INFERRED = new edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation("CheckReturnValueInferred", 8, "RV_RETURN_VALUE_IGNORED_INFERRED", edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY);
    final private static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation[] myValues = {CHECK_RETURN_VALUE_UNKNOWN, CHECK_RETURN_VALUE_HIGH, CHECK_RETURN_VALUE_MEDIUM, CHECK_RETURN_VALUE_LOW, CHECK_RETURN_VALUE_IGNORE, CHECK_RETURN_VALUE_VERY_HIGH, CHECK_RETURN_VALUE_LOW_BAD_PRACTICE, CHECK_RETURN_VALUE_MEDIUM_BAD_PRACTICE, CHECK_RETURN_VALUE_INFERRED};
    public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation parse(java.lang.String priority) {
        if (priority == null) return CHECK_RETURN_VALUE_MEDIUM;
        if (priority.endsWith("HIGH")) return CHECK_RETURN_VALUE_HIGH;
        if (priority.endsWith("MEDIUM")) return CHECK_RETURN_VALUE_MEDIUM;
        if (priority.endsWith("LOW")) return CHECK_RETURN_VALUE_LOW;
        throw new java.lang.IllegalArgumentException("Bad priority: " + priority);
    }
    public static edu.umd.cs.findbugs.ba.CheckReturnValueAnnotation[] values() {
        return myValues.clone();
    }
    public int getPriority() {
        return priority;
    }
    public java.lang.String getPattern() {
        return pattern;
    }
    public CheckReturnValueAnnotation(java.lang.String s, int i, int p) {
        super(s,i);
        pattern = "RV_RETURN_VALUE_IGNORED";
        priority = p;
    }
    public CheckReturnValueAnnotation(java.lang.String s, int i, java.lang.String pattern, int p) {
        super(s,i);
        this.pattern = pattern;
        priority = p;
    }
}
