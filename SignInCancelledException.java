package edu.umd.cs.findbugs.cloud;
import edu.umd.cs.findbugs.cloud.*;
public class SignInCancelledException extends java.lang.Exception {
    public SignInCancelledException() {
        super("User is not signed into FindBugs Cloud");
    }
    public SignInCancelledException(java.lang.Throwable cause) {
        super("User is not signed into FindBugs Cloud",cause);
    }
}
