/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004-2006 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.Iterator;
import java.util.Set;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKEINTERFACE;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InvokeInstruction;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.MethodAnnotation;
import edu.umd.cs.findbugs.Priorities;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.BasicBlock;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Hierarchy2;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.SignatureConverter;
import edu.umd.cs.findbugs.ba.XFactory;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.type.TypeFrame;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
public class CallToUnconditionalThrower extends edu.umd.cs.findbugs.visitclass.PreorderVisitor implements edu.umd.cs.findbugs.Detector {
    static boolean DEBUG = false;
    edu.umd.cs.findbugs.BugReporter bugReporter;
    edu.umd.cs.findbugs.ba.AnalysisContext analysisContext;
    public CallToUnconditionalThrower(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.Detector#report()
     */
    public void report() {
    }
// TODO Auto-generated method stub
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
        if (method.isSynthetic() || (method.getAccessFlags() & org.apache.bcel.Constants.ACC_BRIDGE) == org.apache.bcel.Constants.ACC_BRIDGE) return;
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        org.apache.bcel.generic.ConstantPoolGen cpg = classContext.getConstantPoolGen();
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = classContext.getTypeDataflow(method);
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.BasicBlock> i = cfg.blockIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.BasicBlock basicBlock = i.next();
            if ( !basicBlock.isExceptionThrower()) continue;
            org.apache.bcel.generic.InstructionHandle thrower = basicBlock.getExceptionThrower();
            org.apache.bcel.generic.Instruction ins = thrower.getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.InvokeInstruction)) continue;
            org.apache.bcel.generic.InvokeInstruction inv = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
            boolean foundThrower = false;
            boolean foundNonThrower = false;
            if (inv instanceof org.apache.bcel.generic.INVOKEINTERFACE) continue;
            java.lang.String className = inv.getClassName(cpg);
            edu.umd.cs.findbugs.ba.Location loc = new edu.umd.cs.findbugs.ba.Location(thrower, basicBlock);
            edu.umd.cs.findbugs.ba.type.TypeFrame typeFrame = typeDataflow.getFactAtLocation(loc);
            edu.umd.cs.findbugs.ba.XMethod primaryXMethod = edu.umd.cs.findbugs.ba.XFactory.createXMethod(inv,cpg);
            java.util.Set<edu.umd.cs.findbugs.ba.XMethod> targetSet = null;
            try {
                if (className.startsWith("[")) continue;
                java.lang.String methodSig = inv.getSignature(cpg);
                if ( !methodSig.endsWith("V")) continue;
                targetSet = edu.umd.cs.findbugs.ba.Hierarchy2.resolveMethodCallTargets(inv,typeFrame,cpg);
                for (edu.umd.cs.findbugs.ba.XMethod xMethod : targetSet){
                    if (DEBUG) java.lang.System.out.println("	Found " + xMethod);
                    boolean isUnconditionalThrower = xMethod.isUnconditionalThrower() &&  !xMethod.isUnsupported() &&  !xMethod.isSynthetic();
                    if (isUnconditionalThrower) {
                        foundThrower = true;
                        if (DEBUG) java.lang.System.out.println("Found thrower");
                    }
                    else {
                        foundNonThrower = true;
                        if (DEBUG) java.lang.System.out.println("Found non thrower");
                    }
                }
;
            }
            catch (java.lang.ClassNotFoundException e){
                analysisContext.getLookupFailureCallback().reportMissingClass(e);
            }
            boolean newResult = foundThrower &&  !foundNonThrower;
            if (newResult) bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "TESTING", edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY).addClassAndMethod(classContext.getJavaClass(),method).addString("Call to method that always throws Exception").addMethod(primaryXMethod).describe(edu.umd.cs.findbugs.MethodAnnotation.METHOD_CALLED).addSourceLine(classContext,method,loc));
        }
    }
// Check if it's a method invocation.
// if (primaryXMethod.isAbstract()) continue;
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        analysisContext = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext();
        org.apache.bcel.classfile.Method[] methodList = classContext.getJavaClass().getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("Error checking for infinite recursive loop in " + edu.umd.cs.findbugs.ba.SignatureConverter.convertMethodSignature(classContext.getJavaClass(),method),e);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("Error checking for infinite recursive loop in " + edu.umd.cs.findbugs.ba.SignatureConverter.convertMethodSignature(classContext.getJavaClass(),method),e);
            }
        }
;
    }
}
