/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Warning properties for null pointer dereference and redundant null comparison
 * warnings.
 *
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import edu.umd.cs.findbugs.props.AbstractWarningProperty;
import edu.umd.cs.findbugs.props.PriorityAdjustment;
public class NullDerefProperty extends edu.umd.cs.findbugs.props.AbstractWarningProperty {
    public NullDerefProperty(java.lang.String name, edu.umd.cs.findbugs.props.PriorityAdjustment priorityAdjustment) {
        super(name,priorityAdjustment);
    }
/** Redundant null comparison is of a checked null value. */
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty CHECKED_VALUE = new edu.umd.cs.findbugs.detect.NullDerefProperty("CHECKED_VALUE", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
/** Redundant null comparison is of a checked null value. */
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty LONG_RANGE_NULL_SOURCE = new edu.umd.cs.findbugs.detect.NullDerefProperty("LONG_RANGE_NULL_SOURCE", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_MEDIUM);
/** dereference always on exception path */
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty ALWAYS_ON_EXCEPTION_PATH = new edu.umd.cs.findbugs.detect.NullDerefProperty("ALWAYS_ON_EXCEPTION_PATH", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_MEDIUM);
/** Redundant nullcheck of previously dereferenced value. */
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty WOULD_HAVE_BEEN_A_KABOOM = new edu.umd.cs.findbugs.detect.NullDerefProperty("WOULD_HAVE_BEEN_A_KABOOM", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
/** Redundant nullcheck created dead code. */
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty CREATED_DEAD_CODE = new edu.umd.cs.findbugs.detect.NullDerefProperty("CREATED_DEAD_CODE", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty DEREFS_ARE_CLONED = new edu.umd.cs.findbugs.detect.NullDerefProperty("DEREFS_ARE_CLONED", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_MEDIUM);
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty CLOSING_NULL = new edu.umd.cs.findbugs.detect.NullDerefProperty("CLOSING_NULL", edu.umd.cs.findbugs.props.PriorityAdjustment.PEGGED_HIGH);
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty DEREFS_ARE_INLINED_FINALLY_BLOCKS = new edu.umd.cs.findbugs.detect.NullDerefProperty("DEREFS_ARE_INLINED_FINALLY_BLOCKS", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_MEDIUM);
    final public static edu.umd.cs.findbugs.detect.NullDerefProperty DEREFS_IN_CATCH_BLOCKS = new edu.umd.cs.findbugs.detect.NullDerefProperty("DEREFS_IN_CATCH_BLOCKS", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_MEDIUM);
}
