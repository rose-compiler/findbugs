/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce LiveLocalStoreDataflow objects for analyzed
 * methods.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.LiveLocalStoreAnalysis;
import edu.umd.cs.findbugs.ba.LiveLocalStoreDataflow;
import edu.umd.cs.findbugs.ba.ReverseDepthFirstSearch;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class LiveLocalStoreDataflowFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.LiveLocalStoreDataflow> {
/**
     * Constructor.
     */
    public LiveLocalStoreDataflowFactory() {
        super("live local stores analysis",edu.umd.cs.findbugs.ba.LiveLocalStoreDataflow.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.LiveLocalStoreDataflow analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.generic.MethodGen methodGen = this.getMethodGen(analysisCache,descriptor);
        if (methodGen == null) {
            return null;
        }
        edu.umd.cs.findbugs.ba.CFG cfg = this.getCFG(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.ReverseDepthFirstSearch rdfs = this.getReverseDepthFirstSearch(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.LiveLocalStoreAnalysis analysis = new edu.umd.cs.findbugs.ba.LiveLocalStoreAnalysis(methodGen, rdfs, this.getDepthFirstSearch(analysisCache,descriptor));
        edu.umd.cs.findbugs.ba.LiveLocalStoreDataflow dataflow = new edu.umd.cs.findbugs.ba.LiveLocalStoreDataflow(cfg, analysis);
        dataflow.execute();
        if (edu.umd.cs.findbugs.ba.ClassContext.DUMP_DATAFLOW_ANALYSIS) {
            edu.umd.cs.findbugs.ba.ClassContext.dumpLiveLocalStoreDataflow(descriptor,cfg,dataflow);
        }
        return dataflow;
    }
}
