/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * Class to maintain a snapshot of a processes's time and memory usage.
 * 
 * This uses some JDK 1.5 APIs so must be careful that it doesn't cause any harm
 * when run from 1.4.
 * 
 * @see FindBugs
 * @author Brian Cole
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.util.List;
public class Footprint extends java.lang.Object {
/**
     *
     */
// in nanoseconds
// in milliseconds
// in bytes
// in milliseconds
/** uses deltas from base for cpuTime and clockTime (but not peakMemory) */
// leave peakMem alone
// catch possible Error thrown when complied by the Eclipse compiler
// catch possible Error thrown when complied by the Eclipse compiler
// or new
// java.util.Date().getTime() ;
// -------- begin static inner classes --------
/**
     * Wrapper so that possible NoClassDefFoundError can be caught.
     * Instantiating this class will throw a NoClassDefFoundError on JDK 1.4 and
     * earlier.
     */
// java.lang.management.MemoryMXBean memBean =
// ManagementFactory.getMemoryMXBean();
// java.lang.management.MemoryUsage memUsage =
// memBean.getHeapMemoryUsage();
// problem: sum of the peaks is not necessarily the peak of the sum.
// For example, objects migrate from the 'eden' to the 'survivor'
// area.
// or getCommitted()
// System.out.println(mpBean.getType()+", "+mpBean.getName()+", "+memUsage.getUsed());
// System.out.println("Memory type="+mpBean.getType()+", Pool name="+mpBean.getName()+", Memory usage="+mpBean.getPeakUsage());
// AnalysisContext.logError("Error getting peak usage", e);
// System.out.println();
/**
     * Wrapper so that possbile NoClassDefFoundError can be caught.
     * Instantiating this class will throw a NoClassDefFoundError on JDK 1.4 and
     * earlier, or will throw a ClassCastException on a 1.5-compliant non-sun
     * JRE where the osBean is not a sunBean. (If compiled by Eclipse,
     * instantiating it will throw an unsubclassed java.lang.Error.)
     */
// next line compiles fine with sun JDK 1.5 but the eclipse compiler may
// complain "The type
// OperatingSystemMXBean is not accessible due to restriction on
// required library classes.jar"
// depending on the contents of the .classpath file.
/**
     * Wrapper so that possible NoClassDefFoundError can be caught.
     * Instantiating this class will throw a NoClassDefFoundError on JDK 1.4 and
     * earlier.
     */
    public static class CollectionBeanWrapper extends java.lang.Object {
        public CollectionBeanWrapper() {
        }
        java.util.List<java.lang.management.GarbageCollectorMXBean> clist = java.lang.management.ManagementFactory.getGarbageCollectorMXBeans();
        public long getCollectionTime() {
            long sum = 0;
            for (java.lang.management.GarbageCollectorMXBean gcBean : clist){
                sum += gcBean.getCollectionTime();
            }
;
            return sum;
        }
    }
    public static class OperatingSystemBeanWrapper extends java.lang.Object {
        public OperatingSystemBeanWrapper() {
        }
        java.lang.management.OperatingSystemMXBean osBean = java.lang.management.ManagementFactory.getOperatingSystemMXBean();
        com.sun.management.OperatingSystemMXBean sunBean = (com.sun.management.OperatingSystemMXBean) (osBean) ;
        public long getProcessCpuTime() {
            return sunBean.getProcessCpuTime();
        }
    }
    public static class MemoryBeanWrapper extends java.lang.Object {
        public MemoryBeanWrapper() {
        }
        java.util.List<java.lang.management.MemoryPoolMXBean> mlist = java.lang.management.ManagementFactory.getMemoryPoolMXBeans();
        public long getPeakUsage() {
            long sum = 0;
            for (java.lang.management.MemoryPoolMXBean mpBean : mlist){
                try {
                    java.lang.management.MemoryUsage memUsage = mpBean.getPeakUsage();
                    if (memUsage != null) sum += memUsage.getUsed();
                }
                catch (java.lang.RuntimeException e){
                    assert true;
                }
            }
;
            return sum;
        }
    }
    final private static int NOCLASSDEF_ERROR =  -9;
    final private static int CLASSCAST_ERROR =  -8;
    final private static int ERROR_ERROR =  -7;
    final private static int RUNTIME_EXCEPTION =  -6;
    private long cpuTime =  -1;
    private long clockTime =  -1;
    private long peakMem =  -1;
    private long collectionTime =  -1;
    public Footprint() {
        super();
        this.pullData();
    }
    public Footprint(edu.umd.cs.findbugs.Footprint base) {
        super();
        this.pullData();
        if (cpuTime >= 0) {
            cpuTime = (base.cpuTime >= 0) ? cpuTime - base.cpuTime : base.cpuTime;
        }
        if (clockTime >= 0) {
            clockTime = (base.clockTime >= 0) ? clockTime - base.clockTime : base.clockTime;
        }
        if (collectionTime >= 0) {
            collectionTime = (base.collectionTime >= 0) ? collectionTime - base.collectionTime : base.collectionTime;
        }
    }
    private void pullData() {
        try {
            cpuTime = new edu.umd.cs.findbugs.Footprint.OperatingSystemBeanWrapper().getProcessCpuTime();
        }
        catch (java.lang.NoClassDefFoundError ncdfe){
            cpuTime = NOCLASSDEF_ERROR;
        }
        catch (java.lang.ClassCastException cce){
            cpuTime = CLASSCAST_ERROR;
        }
        catch (java.lang.Error error){
            cpuTime = ERROR_ERROR;
        }
        catch (java.lang.RuntimeException error){
            cpuTime = RUNTIME_EXCEPTION;
        }
        clockTime = java.lang.System.currentTimeMillis();
        try {
            peakMem = new edu.umd.cs.findbugs.Footprint.MemoryBeanWrapper().getPeakUsage();
        }
        catch (java.lang.NoClassDefFoundError ncdfe){
            peakMem = NOCLASSDEF_ERROR;
        }
        catch (java.lang.Error ncdfe){
            peakMem = CLASSCAST_ERROR;
        }
        catch (java.lang.RuntimeException ncdfe){
            peakMem = RUNTIME_EXCEPTION;
        }
        try {
            collectionTime = new edu.umd.cs.findbugs.Footprint.CollectionBeanWrapper().getCollectionTime();
        }
        catch (java.lang.NoClassDefFoundError ncdfe){
            collectionTime = NOCLASSDEF_ERROR;
        }
        catch (java.lang.Error ncdfe){
            peakMem = ERROR_ERROR;
        }
        catch (java.lang.RuntimeException ncdfe){
            collectionTime = RUNTIME_EXCEPTION;
        }
    }
    public long getCpuTime() {
        return cpuTime;
    }
    public long getClockTime() {
        return clockTime;
    }
    public long getPeakMemory() {
        return peakMem;
    }
    public long getCollectionTime() {
        return collectionTime;
    }
    public java.lang.String toString() {
        return "cpuTime=" + cpuTime + ", clockTime=" + clockTime + ", peakMemory=" + peakMem;
    }
    public static void main(java.lang.String[] argv) {
        java.lang.System.out.println(new edu.umd.cs.findbugs.Footprint());
    }
}
