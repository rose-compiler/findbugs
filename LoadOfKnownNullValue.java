package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.BitSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import org.apache.bcel.classfile.LineNumberTable;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ARETURN;
import org.apache.bcel.generic.BranchInstruction;
import org.apache.bcel.generic.GOTO;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.BugAccumulator;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.SystemProperties;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.ba.npe.IsNullValue;
import edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow;
import edu.umd.cs.findbugs.ba.npe.IsNullValueFrame;
public class LoadOfKnownNullValue extends java.lang.Object implements edu.umd.cs.findbugs.Detector {
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    private edu.umd.cs.findbugs.BugAccumulator bugAccumulator;
    public LoadOfKnownNullValue(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
        this.bugAccumulator = new edu.umd.cs.findbugs.BugAccumulator(bugReporter);
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.Method[] methodList = classContext.getJavaClass().getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.MethodUnprofitableException mue){
// otherwise
                if (edu.umd.cs.findbugs.SystemProperties.getBoolean("unprofitable.debug")) bugReporter.logError("skipping unprofitable method in " + this.getClass().getName());
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("Detector " + this.getClass().getName() + " caught exception",e);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("Detector " + this.getClass().getName() + " caught exception",e);
            }
            bugAccumulator.reportAccumulatedBugs();
        }
;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
        java.util.BitSet lineMentionedMultipleTimes = classContext.linesMentionedMultipleTimes(method);
        java.util.BitSet linesWithLoadsOfNotDefinitelyNullValues = null;
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow nullValueDataflow = classContext.getIsNullValueDataflow(method);
        org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
        java.lang.String sourceFile = classContext.getJavaClass().getSourceFileName();
        if (lineMentionedMultipleTimes.cardinality() > 0) {
            linesWithLoadsOfNotDefinitelyNullValues = new java.util.BitSet();
            org.apache.bcel.classfile.LineNumberTable lineNumbers = method.getLineNumberTable();
            for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
                edu.umd.cs.findbugs.ba.Location location = i.next();
                org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
                org.apache.bcel.generic.Instruction ins = handle.getInstruction();
                if ( !(ins instanceof org.apache.bcel.generic.ALOAD)) continue;
                edu.umd.cs.findbugs.ba.npe.IsNullValueFrame frame = nullValueDataflow.getFactAtLocation(location);
                if ( !frame.isValid()) {
                    continue;
                }
                org.apache.bcel.generic.ALOAD load = (org.apache.bcel.generic.ALOAD) (ins) ;
                int index = load.getIndex();
                edu.umd.cs.findbugs.ba.npe.IsNullValue v = frame.getValue(index);
                if ( !v.isDefinitelyNull()) {
                    int sourceLine = lineNumbers.getSourceLine(handle.getPosition());
                    if (sourceLine > 0) linesWithLoadsOfNotDefinitelyNullValues.set(sourceLine);
                }
            }
        }
        java.util.IdentityHashMap<org.apache.bcel.generic.InstructionHandle, java.lang.Object> sometimesGood = new java.util.IdentityHashMap<org.apache.bcel.generic.InstructionHandle, java.lang.Object>();
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.ALOAD)) continue;
            edu.umd.cs.findbugs.ba.npe.IsNullValueFrame frame = nullValueDataflow.getFactAtLocation(location);
            if ( !frame.isValid()) {
                continue;
            }
            org.apache.bcel.generic.ALOAD load = (org.apache.bcel.generic.ALOAD) (ins) ;
            int index = load.getIndex();
            edu.umd.cs.findbugs.ba.npe.IsNullValue v = frame.getValue(index);
            if ( !v.isDefinitelyNull()) sometimesGood.put(handle,null);
        }
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.ALOAD)) continue;
            if (sometimesGood.containsKey(handle)) continue;
            edu.umd.cs.findbugs.ba.npe.IsNullValueFrame frame = nullValueDataflow.getFactAtLocation(location);
            if ( !frame.isValid()) {
                continue;
            }
            org.apache.bcel.generic.ALOAD load = (org.apache.bcel.generic.ALOAD) (ins) ;
            int index = load.getIndex();
            edu.umd.cs.findbugs.ba.npe.IsNullValue v = frame.getValue(index);
            if (v.isDefinitelyNull()) {
                org.apache.bcel.generic.Instruction next = handle.getNext().getInstruction();
                org.apache.bcel.generic.InstructionHandle prevHandle = handle.getPrev();
                edu.umd.cs.findbugs.SourceLineAnnotation sourceLineAnnotation = edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,methodGen,sourceFile,handle);
                edu.umd.cs.findbugs.SourceLineAnnotation prevSourceLineAnnotation = edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,methodGen,sourceFile,prevHandle);
                if (next instanceof org.apache.bcel.generic.ARETURN) {
                    continue;
                }
                if (next instanceof org.apache.bcel.generic.GOTO) {
                    org.apache.bcel.generic.InstructionHandle targ = ((org.apache.bcel.generic.BranchInstruction) (next) ).getTarget();
                    if (targ.getInstruction() instanceof org.apache.bcel.generic.ARETURN) {
                        continue;
                    }
                }
                int startLine = sourceLineAnnotation.getStartLine();
                if (startLine > 0 && lineMentionedMultipleTimes.get(startLine) && linesWithLoadsOfNotDefinitelyNullValues.get(startLine)) continue;
                int previousLine = prevSourceLineAnnotation.getEndLine();
                if (startLine < previousLine) {
                    continue;
                }
                int priority = NORMAL_PRIORITY;
                if ( !v.isChecked()) priority++;
                bugAccumulator.accumulateBug(new edu.umd.cs.findbugs.BugInstance(this, "NP_LOAD_OF_KNOWN_NULL_VALUE", priority).addClassAndMethod(methodGen,sourceFile),sourceLineAnnotation);
            }
        }
    }
// This basic block is probably dead
// System.out.println(handle.getPosition() + "\t" + ins.getName() +
// "\t" + frame);
// System.out.println(nullValueDataflow);
// This basic block is probably dead
// System.out.println(handle.getPosition() + "\t" + ins.getName() +
// "\t" + frame);
// probably stored for duration of finally block
// Skip for the same reason we would skip if
// (next instanceof ARETURN) were true. This
// is necessary because the bytecode compiler
// compiles the ternary ? operator with a GOTO
// to an ARETURN instead of just an ARETURN.
// probably stored for duration of finally block
// System.out.println("Inverted line");
// System.out.println("lineMentionedMultipleTimes: " +
// lineMentionedMultipleTimes);
// System.out.println("linesWithLoadsOfNonNullValues: " +
// linesWithLoadsOfNotDefinitelyNullValues);
    public void report() {
    }
}
