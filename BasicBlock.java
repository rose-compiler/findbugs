package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Simple basic block abstraction for BCEL.
 * 
 * @author David Hovemeyer
 * @see CFG
 */
/*
     * ----------------------------------------------------------------------
     * Static data
     * ----------------------------------------------------------------------
     */
/**
     * Set of instruction opcodes that have an implicit null check.
     */
// nullCheckInstructionSet.set(Constants.MONITOREXIT);
// Any others?
/*
     * ----------------------------------------------------------------------
     * Fields
     * ----------------------------------------------------------------------
     */
// instruction for which this
// block is the ETB
// set if this block is the entry
// point of an exception handler
/*
     * ----------------------------------------------------------------------
     * Public methods
     * ----------------------------------------------------------------------
     */
/**
     * Constructor.
     */
/**
     * Get the basic block's integer label.
     * 
     * @deprecated call getLabel() instead
     * @return the BasicBlock's integer label
     */
/**
     * Set the instruction for which this block is the ETB.
     * 
     * @param exceptionThrower
     *            the instruction
     */
/**
     * Return whether or not this block is an exception thrower.
     */
/**
     * Get the instruction for which this block is an exception thrower.
     * 
     * @return the instruction, or null if this block is not an exception
     *         thrower
     */
/**
     * Return whether or not this block is a null check.
     */
// Null check blocks must be exception throwers,
// and are always empty. (The only kind of non-empty
// exception throwing block is one terminated by an ATHROW).
/**
     * Get the first instruction in the basic block.
     */
/**
     * Get the last instruction in the basic block.
     */
/**
     * Get the successor of given instruction within the basic block.
     * 
     * @param handle
     *            the instruction
     * @return the instruction's successor, or null if the instruction is the
     *         last in the basic block
     */
/**
     * Get the predecessor of given instruction within the basic block.
     * 
     * @param handle
     *            the instruction
     * @return the instruction's predecessor, or null if the instruction is the
     *         first in the basic block
     */
/**
     * Add an InstructionHandle to the basic block.
     * 
     * @param handle
     *            the InstructionHandle
     */
/**
     * A forward Iterator over the instructions of a basic block. The
     * duplicate() method can be used to make an exact copy of this iterator.
     * Calling next() on the duplicate will not affect the original, and vice
     * versa.
     */
/**
     * Get an Iterator over the instructions in the basic block.
     */
/**
     * A reverse Iterator over the instructions in a basic block.
     */
/**
     * Get an Iterator over the instructions in the basic block in reverse
     * order. This is useful for backwards dataflow analyses.
     */
/**
     * Return true if there are no Instructions in this basic block.
     */
/**
     * Is this block an exception handler?
     */
/**
     * Get CodeExceptionGen object; returns null if this basic block is not the
     * entry point of an exception handler.
     * 
     * @return the CodeExceptionGen object, or null
     */
/**
     * Set the CodeExceptionGen object. Marks this basic block as the entry
     * point of an exception handler.
     * 
     * @param exceptionGen
     *            the CodeExceptionGen object for the block
     */
/**
     * Return whether or not the basic block contains the given instruction.
     * 
     * @param handle
     *            the instruction
     * @return true if the block contains the instruction, false otherwise
     */
/**
     * Return whether or not the basic block contains the instruction with the
     * given bytecode offset.
     * 
     * @param offset
     *            the bytecode offset
     * @return true if the block contains an instruction with the given offset,
     *         false if it does not
     */
/**
     * @return Returns the numNonExceptionSuccessors.
     */
/**
     * @param numNonExceptionSuccessors
     *            The numNonExceptionSuccessors to set.
     */
// vim:ts=4
abstract interface package-info {
}
