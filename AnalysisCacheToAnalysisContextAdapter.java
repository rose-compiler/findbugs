package edu.umd.cs.findbugs.ba;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * An AnalysisContext implementation that uses the IAnalysisCache. This class
 * must only be used by FindBugs2, not the original FindBugs driver.
 *
 * @author David Hovemeyer
 */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.classfile.IErrorLogger#logError(java.lang.String)
         */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.classfile.IErrorLogger#logError(java.lang.String,
         * java.lang.Throwable)
         */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.classfile.IErrorLogger#reportMissingClass(java
         * .lang.ClassNotFoundException)
         */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.classfile.IErrorLogger#reportMissingClass(edu
         * .umd.cs.findbugs.classfile.ClassDescriptor)
         */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.classfile.IErrorLogger#reportSkippedAnalysis(
         * edu.umd.cs.findbugs.classfile.MethodDescriptor)
         */
/**
     * Constructor.
     */
// /* (non-Javadoc)
// * @see
// edu.umd.cs.findbugs.ba.AnalysisContext#addApplicationClassToRepository(org.apache.bcel.classfile.JavaClass)
// */
// @Override
// public void addApplicationClassToRepository(JavaClass appClass) {
// throw new UnsupportedOperationException();
// }
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.AnalysisContext#addClasspathEntry(java.lang.String
     * )
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#clearClassContextCache()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#clearRepository()
     */
// Set the backing store for the BCEL Repository to
// be the AnalysisCache.
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.AnalysisContext#getAnnotationRetentionDatabase()
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.AnalysisContext#getCheckReturnAnnotationDatabase()
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.AnalysisContext#getClassContext(org.apache.bcel
     * .classfile.JavaClass)
     */
// This is a bit silly since we're doing an unnecessary
// ClassDescriptor->JavaClass lookup.
// However, we can be assured that it will succeed.
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#getClassContextStats()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#getFieldStoreTypeDatabase()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#getJCIPAnnotationDatabase()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#getLookupFailureCallback()
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.AnalysisContext#getNullnessAnnotationDatabase()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#getSourceFinder()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#getSourceInfoMap()
     */
// /* (non-Javadoc)
// * @see edu.umd.cs.findbugs.ba.AnalysisContext#getSubtypes()
// */
// @Override
// public Subtypes getSubtypes() {
// if (Subtypes.DO_NOT_USE) {
// throw new IllegalArgumentException();
// }
// return getDatabase(Subtypes.class);
// }
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.AnalysisContext#getUnconditionalDerefParamDatabase
     * ()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#initDatabases()
     */
// Databases are created on-demand - don't need to explicitly create
// them
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#lookupClass(java.lang.String)
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#getInnerClassAccessMap()
     */
/**
     * Helper method to get a database without having to worry about a
     * CheckedAnalysisException.
     *
     * @param cls
     *            Class of the database to get
     * @return the database
     */
/**
     * Set the collection of class descriptors identifying all application
     * classes.
     *
     * @param appClassCollection
     *            List of ClassDescriptors identifying application classes
     */
// FIXME: we really should drive the progress callback here
// Add the application class to the database
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#updateDatabases(int)
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.AnalysisContext#getReturnValueNullnessPropertyDatabase
     * ()
     */
// private Subtypes2 subtypes2;
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#getSubtypes2()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.ba.AnalysisContext#
     * getDirectlyRelevantTypeQualifiersDatabase()
     */
abstract interface package-info {
}
