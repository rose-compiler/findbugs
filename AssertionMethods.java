package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Mark methodref constant pool entries of methods that are likely to implement
 * assertions. This is useful for pruning likely false paths.
 * 
 * @author David Hovemeyer
 */
/**
     * Bitset of methodref constant pool indexes referring to likely assertion
     * methods.
     */
/**
     * Constructor.
     * 
     * @param jclass
     *            the JavaClass containing the methodrefs
     */
// || methodNameLC.indexOf("check") >= 0 
// FIXME: should report
/**
     * Does the given instruction refer to a likely assertion method?
     * 
     * @param ins
     *            the instruction
     * @return true if the instruction likely refers to an assertion, false if
     *         not
     */
// vim:ts=4
abstract interface package-info {
}
