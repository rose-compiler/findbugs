/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * These are the branches in our tree, each branch forms a complete query that
 * could be sent to the main bugset to return all the bugs it contains For
 * example, a single bugAspects could be <priority,high> or it could be
 * <priority,high>, <designation,must fix>,<class,fishpond>,<package,default>
 * 
 * In this implementation, <priority,high>,<designation,unclassified> is
 * different from <designation,unclassified>,<priority,high>. (I'm not talking
 * about the fact we use the .equals from ArrayList, I'm talking about what a
 * query would return, though both are true) For a speed boost, this class could
 * be rewritten to make these equal, BugSet could be rewritten to cache full
 * queries off the main BugSet, (instead of caching each part of the query
 * separately in the BugSets created) and resetData could be rewritten to work
 * more like Swing's validate, only clearing data if the data is wrong. This
 * would save time after changing certain aspects of the tree. Just an idea, I
 * wouldn't suggest it unless its absolutely necessary. -Dan
 * 
 * 
 * @author All of us
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.util.ArrayList;
import java.util.Iterator;
import edu.umd.cs.findbugs.filter.Matcher;
public class BugAspects extends java.lang.Object implements java.lang.Iterable<edu.umd.cs.findbugs.gui2.BugAspects.SortableValue> {
/**
     * This is how the numbers after the branches contain the number of bugs in
     * them, even if they aren't the final branch
     * 
     * @param count
     */
    static class SortableValue extends java.lang.Object {
        final public edu.umd.cs.findbugs.gui2.Sortables key;
        final public java.lang.String value;
        public SortableValue(edu.umd.cs.findbugs.gui2.Sortables key, java.lang.String value) {
            super();
            this.key = key;
            this.value = value;
        }
        public int hashCode() {
            return key.hashCode() + value.hashCode();
        }
        public boolean equals(java.lang.Object that) {
            if ( !(that instanceof edu.umd.cs.findbugs.gui2.BugAspects.SortableValue)) return false;
            edu.umd.cs.findbugs.gui2.BugAspects.SortableValue thatStringPair = ((edu.umd.cs.findbugs.gui2.BugAspects.SortableValue) (that) );
            return this.key.equals(thatStringPair.key) && this.value.equals(thatStringPair.value);
        }
        public java.lang.String toString() {
            return key + ":" + value;
        }
    }
    final private static long serialVersionUID =  -5503915081879996968L;
    private int count =  -1;
    private java.util.ArrayList<edu.umd.cs.findbugs.gui2.BugAspects.SortableValue> lst = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.BugAspects.SortableValue>();
    public edu.umd.cs.findbugs.gui2.BugAspects.SortableValue last() {
        return lst.get(lst.size() - 1);
    }
    public int size() {
        return lst.size();
    }
    public edu.umd.cs.findbugs.gui2.BugAspects.SortableValue get(int i) {
        return lst.get(i);
    }
    public java.lang.String toString() {
        if (lst.isEmpty()) return edu.umd.cs.findbugs.L10N.getLocalString("tree.bugs","Bugs") + " (" + count + ")";
        else {
            if (count ==  -1) return this.last().value;
            else return this.last().key.formatValue(this.last().value) + " (" + count + ")";
        }
    }
    public void setCount(int count) {
        this.count = count;
    }
    public int getCount() {
        return count;
    }
    public BugAspects() {
        super();
    }
    public BugAspects(edu.umd.cs.findbugs.gui2.BugAspects a) {
        super();
        lst = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.BugAspects.SortableValue>(a.lst);
        count = a.count;
    }
    public void add(edu.umd.cs.findbugs.gui2.BugAspects.SortableValue sp) {
        lst.add(sp);
    }
    public edu.umd.cs.findbugs.gui2.BugAspects addToNew(edu.umd.cs.findbugs.gui2.BugAspects.SortableValue sp) {
        edu.umd.cs.findbugs.gui2.BugAspects result = new edu.umd.cs.findbugs.gui2.BugAspects(this);
        result.lst.add(sp);
        return result;
    }
    public edu.umd.cs.findbugs.filter.Matcher getMatcher() {
        return edu.umd.cs.findbugs.gui2.FilterFactory.makeAndMatcher(lst);
    }
    public edu.umd.cs.findbugs.gui2.StackedFilterMatcher getStackedFilterMatcher() {
        edu.umd.cs.findbugs.gui2.FilterMatcher[] filters = new edu.umd.cs.findbugs.gui2.FilterMatcher[lst.size()];
        for (int i = 0; i < filters.length; i++) filters[i] = new edu.umd.cs.findbugs.gui2.FilterMatcher(lst.get(i));
        edu.umd.cs.findbugs.gui2.StackedFilterMatcher sfm = new edu.umd.cs.findbugs.gui2.StackedFilterMatcher(filters);
        return sfm;
    }
    public edu.umd.cs.findbugs.gui2.BugSet getMatchingBugs(edu.umd.cs.findbugs.gui2.BugSet theSet) {
        return theSet.getBugsMatchingFilter(this.getStackedFilterMatcher());
    }
    public java.util.Iterator<edu.umd.cs.findbugs.gui2.BugAspects.SortableValue> iterator() {
        return lst.iterator();
    }
}
