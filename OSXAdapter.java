/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/*
 * Based on sample code from Apple.
 *
 * This is the only class that uses the Apple specific EAWT classes.
 * This class should only ever be referenced via reflection after
 * checking that we are running on Mac OS X.
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import com.apple.eawt.ApplicationAdapter;
import com.apple.eawt.ApplicationEvent;
public class OSXAdapter extends com.apple.eawt.ApplicationAdapter {
// pseudo-singleton model; no point in making multiple instances
// of the EAWT application or our adapter
    private static edu.umd.cs.findbugs.gui2.OSXAdapter theAdapter = new edu.umd.cs.findbugs.gui2.OSXAdapter();
    final private static com.apple.eawt.Application theApplication = new com.apple.eawt.Application();
// reference to the app where the existing quit, about, prefs code is
    private static edu.umd.cs.findbugs.gui2.MainFrame mainApp;
    public OSXAdapter() {
        super();
    }
// implemented handler methods. These are basically hooks into
// existing functionality from the main app, as if it came
// over from another platform.
    public void handleAbout(com.apple.eawt.ApplicationEvent ae) {
        if (mainApp != null) {
            ae.setHandled(true);
            javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
                public void run() {
                    mainApp.about();
                }
            });
        }
        else {
            throw new java.lang.IllegalStateException("handleAbout: MyApp instance detached from listener");
        }
    }
    public void handlePreferences(com.apple.eawt.ApplicationEvent ae) {
        if (mainApp != null) {
            mainApp.preferences();
            ae.setHandled(true);
        }
        else {
            throw new java.lang.IllegalStateException("handlePreferences: MyApp instance detached from listener");
        }
    }
    public void handleQuit(com.apple.eawt.ApplicationEvent ae) {
        if (mainApp != null) {
            ae.setHandled(false);
            mainApp.callOnClose();
        }
        else {
            throw new java.lang.IllegalStateException("handleQuit: MyApp instance detached from listener");
        }
    }
// The main entry-point for this functionality. This is the only method
// that needs to be called at runtime, and it can easily be done using
// reflection (see MyApp.java)
    public static void registerMacOSXApplication(edu.umd.cs.findbugs.gui2.MainFrame inApp) {
        if (mainApp != null) throw new java.lang.IllegalStateException("application already set");
        mainApp = inApp;
        theApplication.addApplicationListener(theAdapter);
        theApplication.addPreferencesMenuItem();
    }
// Another static entry point for EAWT functionality. Enables the
// "Preferences..." menu item in the application menu.
    public static void enablePrefs(boolean enabled) {
        theApplication.setEnabledPreferencesMenu(enabled);
    }
}
