/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A Binding binds a name to a Variable.
 * 
 * @author David Hovemeyer
 * @see Variable
 */
package edu.umd.cs.findbugs.ba.bcp;
import edu.umd.cs.findbugs.ba.bcp.*;
public class Binding extends java.lang.Object {
    final private java.lang.String varName;
    final private edu.umd.cs.findbugs.ba.bcp.Variable variable;
/**
     * Constructor.
     * 
     * @param varName
     *            the name of the variable
     * @param variable
     *            the variable
     */
    public Binding(java.lang.String varName, edu.umd.cs.findbugs.ba.bcp.Variable variable) {
        super();
        if (variable == null) throw new java.lang.IllegalArgumentException("No variable!");
        this.varName = varName;
        this.variable = variable;
    }
/**
     * Get the variable name.
     */
    public java.lang.String getVarName() {
        return varName;
    }
/**
     * Get the variable of the variable.
     */
    public edu.umd.cs.findbugs.ba.bcp.Variable getVariable() {
        return variable;
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append(varName);
        buf.append('\u003d');
        buf.append(variable.toString());
        return buf.toString();
    }
}
// vim:ts=4
