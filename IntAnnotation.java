/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Bug annotation class for integer values.
 * 
 * @author David Hovemeyer
 * @see BugAnnotation
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class IntAnnotation extends java.lang.Object implements edu.umd.cs.findbugs.BugAnnotation {
    final private static long serialVersionUID = 1L;
    final private static java.lang.String DEFAULT_ROLE = "INT_DEFAULT";
    private int value;
    private java.lang.String description;
/**
     *
     */
    final public static java.lang.String INT_SYNC_PERCENT = "INT_SYNC_PERCENT";
    final public static java.lang.String INT_OCCURRENCES = "INT_OCCURRENCES";
    final public static java.lang.String INT_VALUE = "INT_VALUE";
    final public static java.lang.String INT_SHIFT = "INT_SHIFT";
    final public static java.lang.String INT_EXPECTED_ARGUMENTS = "INT_EXPECTED_ARGUMENTS";
    final public static java.lang.String INT_ACTUAL_ARGUMENTS = "INT_ACTUAL_ARGUMENTS";
    final public static java.lang.String INT_OBLIGATIONS_REMAINING = "INT_OBLIGATIONS_REMAINING";
/**
     * Constructor.
     * 
     * @param value
     *            the integer value
     */
    public IntAnnotation(int value) {
        super();
        this.value = value;
        this.description = DEFAULT_ROLE;
    }
    public java.lang.Object clone() {
        try {
            return super.clone();
        }
        catch (java.lang.CloneNotSupportedException e){
            throw new java.lang.AssertionError(e);
        }
    }
/**
     * Get the integer value.
     * 
     * @return the integer value
     */
    public int getValue() {
        return value;
    }
    public void accept(edu.umd.cs.findbugs.BugAnnotationVisitor visitor) {
        visitor.visitIntAnnotation(this);
    }
    public java.lang.String format(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        if (key.equals("hash")) {
            if (this.isSignificant()) return java.lang.Integer.toString(value);
            else return "";
        }
        return getShortInteger(value);
    }
    public static java.lang.String getShortInteger(int value) {
        java.lang.String base16 = java.lang.Integer.toHexString(value);
        int unique = uniqueDigits(base16);
        java.lang.String base10 = java.lang.Integer.toString(value);
        if (unique <= 3 && base16.length() - unique >= 3 && base10.length() > base16.length()) return "0x" + base16;
        return base10;
    }
    public static java.lang.String getShortInteger(long value) {
        java.lang.String base16 = java.lang.Long.toHexString(value);
        int unique = uniqueDigits(base16);
        java.lang.String base10 = java.lang.Long.toString(value);
        if (unique <= 3 && base16.length() - unique >= 3 && base10.length() > base16.length()) return "0x" + base16;
        return base10;
    }
    private static int uniqueDigits(java.lang.String value) {
        java.util.Set<java.lang.Character> used = new java.util.HashSet<java.lang.Character>();
        for (int i = 0; i < value.length(); i++) used.add(value.charAt(i));
        return used.size();
    }
    public void setDescription(java.lang.String description) {
        this.description = description;
    }
    public java.lang.String getDescription() {
        return description;
    }
    public int hashCode() {
        return value;
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.IntAnnotation)) return false;
        return value == ((edu.umd.cs.findbugs.IntAnnotation) (o) ).value;
    }
    public int compareTo(edu.umd.cs.findbugs.BugAnnotation o) {
// BugAnnotations must be Comparable
        if ( !(o instanceof edu.umd.cs.findbugs.IntAnnotation)) 
// with any type of BugAnnotation
        return this.getClass().getName().compareTo(o.getClass().getName());
        return value - ((edu.umd.cs.findbugs.IntAnnotation) (o) ).value;
    }
    public java.lang.String toString() {
        java.lang.String pattern = edu.umd.cs.findbugs.I18N.instance().getAnnotationDescription(description);
        edu.umd.cs.findbugs.FindBugsMessageFormat format = new edu.umd.cs.findbugs.FindBugsMessageFormat(pattern);
        return format.format(new edu.umd.cs.findbugs.BugAnnotation[]{this},null);
    }
/*
     * ----------------------------------------------------------------------
     * XML Conversion support
     * ----------------------------------------------------------------------
     */
    final private static java.lang.String ELEMENT_NAME = "Int";
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        this.writeXML(xmlOutput,false,false);
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean addMessages, boolean isPrimary) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributeList = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("value",java.lang.String.valueOf(value));
        java.lang.String role = this.getDescription();
        if ( !role.equals(DEFAULT_ROLE)) attributeList.addAttribute("role",role);
        edu.umd.cs.findbugs.BugAnnotationUtil.writeXML(xmlOutput,ELEMENT_NAME,this,attributeList,addMessages);
    }
    public boolean isSignificant() {
        return  !description.equals(INT_SYNC_PERCENT) &&  !description.equals(INT_OCCURRENCES);
    }
    public java.lang.String toString(edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        return this.toString();
    }
}
// vim:ts=4
