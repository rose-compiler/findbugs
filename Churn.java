/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.config.CommandLine;
public class Churn extends java.lang.Object {
// return b.getPriorityAbbreviation() + "-" + b.getType();
    class ChurnCommandLine extends edu.umd.cs.findbugs.config.CommandLine {
        public ChurnCommandLine() {
            super();
            this.addOption("-fixRate","percentage","expected fix rate for chi test");
        }
        public void handleOption(java.lang.String option, java.lang.String optionalExtraPart) {
            throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
        public void handleOptionWithArgument(java.lang.String option, java.lang.String argument) {
            if (option.equals("-fixRate")) fixRate = java.lang.Integer.parseInt(argument);
            else throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
    }
    static class Data extends java.lang.Object {
        public Data() {
        }
        int persist;
        int fixed;
        int maxRemovedAtOnce() {
            int count = 0;
            for (int c : lastCount.values())if (count < c) count = c;
;
            return count;
        }
        java.util.Map<java.lang.Long, java.lang.Integer> lastCount = new java.util.HashMap<java.lang.Long, java.lang.Integer>();
        void update(edu.umd.cs.findbugs.BugInstance bug) {
            if (bug.isDead()) fixed++;
            else persist++;
            final long lastVersion = bug.getLastVersion();
            if (lastVersion !=  -1) {
                java.lang.Integer v = lastCount.get(lastVersion);
                if (v == null) lastCount.put(lastVersion,0);
                else lastCount.put(lastVersion,v + 1);
            }
        }
    }
    edu.umd.cs.findbugs.BugCollection bugCollection;
    int fixRate =  -1;
    public Churn() {
        super();
    }
    public Churn(edu.umd.cs.findbugs.BugCollection bugCollection) {
        super();
        this.bugCollection = bugCollection;
    }
    public void setBugCollection(edu.umd.cs.findbugs.BugCollection bugCollection) {
        this.bugCollection = bugCollection;
    }
    java.lang.String getKey(edu.umd.cs.findbugs.BugInstance b) {
        if (false) return b.getType();
        java.lang.String result = b.getCategoryAbbrev();
        if (result.equals("C") || result.equals("N")) return result;
        return "O";
    }
    java.util.Map<java.lang.String, edu.umd.cs.findbugs.workflow.Churn.Data> data = new java.util.TreeMap<java.lang.String, edu.umd.cs.findbugs.workflow.Churn.Data>();
    edu.umd.cs.findbugs.workflow.Churn.Data all = new edu.umd.cs.findbugs.workflow.Churn.Data();
    int[] aliveAt;
    int[] diedAfter;
    public edu.umd.cs.findbugs.workflow.Churn execute() {
        data.put("all",all);
        aliveAt = new int[(int) (bugCollection.getSequenceNumber())  + 1];
        diedAfter = new int[(int) (bugCollection.getSequenceNumber())  + 1];
        for (java.util.Iterator<edu.umd.cs.findbugs.BugInstance> j = bugCollection.iterator(); j.hasNext(); ) {
            edu.umd.cs.findbugs.BugInstance bugInstance = j.next();
            java.lang.String key = this.getKey(bugInstance);
            edu.umd.cs.findbugs.workflow.Churn.Data d = data.get(key);
            if (d == null) data.put(key,d = new edu.umd.cs.findbugs.workflow.Churn.Data());
            d.update(bugInstance);
            all.update(bugInstance);
            long first = bugInstance.getFirstVersion();
            long last = bugInstance.getLastVersion();
            if (last !=  -1) {
                java.lang.System.out.printf("%3d #fixed %s%n",last,key);
            }
            if (first != 0 && last !=  -1) {
                int lifespan = (int) ((last - first + 1)) ;
                java.lang.System.out.printf("%3d #age %s%n",lifespan,key);
                java.lang.System.out.printf("%3d %3d #spread %s%n",first,last,key);
                diedAfter[lifespan]++;
                for (int t = 1; t < lifespan; t++) aliveAt[t]++;
            }
            else if (first != 0) {
                int lifespan = (int) ((bugCollection.getSequenceNumber() - first + 1)) ;
                for (int t = 1; t < lifespan; t++) aliveAt[t]++;
            }
        }
        return this;
    }
    public void dump(java.io.PrintStream out) {
        for (int t = 1; t < aliveAt.length; t++) {
            if (aliveAt[t] != 0) java.lang.System.out.printf("%3d%% %4d %5d %3d #decay%n",diedAfter[t] * 100 / aliveAt[t],diedAfter[t],aliveAt[t],t);
        }
        java.lang.System.out.printf("%7s %3s %5s %5s %5s  %s%n","chi","%","const","fix","max","kind");
        double fixRate;
        if (this.fixRate ==  -1) fixRate = ((double) (all.fixed) ) / (all.fixed + all.persist);
        else fixRate = this.fixRate / 100.0;
        double highFixRate = fixRate + 0.05;
        double lowFixRate = fixRate - 0.05;
        for (java.util.Map.Entry<java.lang.String, edu.umd.cs.findbugs.workflow.Churn.Data> e : data.entrySet()){
            edu.umd.cs.findbugs.workflow.Churn.Data d = e.getValue();
            int total = d.persist + d.fixed;
            if (total < 2) continue;
            double rawFixRate = ((double) (d.fixed) ) / total;
            double chiValue;
            if (lowFixRate <= rawFixRate && rawFixRate <= highFixRate) {
                chiValue = 0;
            }
            else {
                double baseFixRate;
                if (rawFixRate < lowFixRate) baseFixRate = lowFixRate;
                else baseFixRate = highFixRate;
                double expectedFixed = baseFixRate * total;
                double expectedPersist = (1 - baseFixRate) * total;
                chiValue = (d.fixed - expectedFixed) * (d.fixed - expectedFixed) / expectedFixed + (d.persist - expectedPersist) * (d.persist - expectedPersist) / expectedPersist;
                if (rawFixRate < lowFixRate) chiValue =  -chiValue;
            }
            java.lang.System.out.printf("%7d %3d %5d %5d %5d %s%n",(int) (chiValue) ,d.fixed * 100 / total,d.persist,d.fixed,d.maxRemovedAtOnce(),e.getKey());
        }
;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
// load plugins
        edu.umd.cs.findbugs.workflow.Churn churn = new edu.umd.cs.findbugs.workflow.Churn();
        edu.umd.cs.findbugs.workflow.Churn.ChurnCommandLine commandLine = churn.new ChurnCommandLine();
        int argCount = commandLine.parse(args,0,2,"Usage: " + edu.umd.cs.findbugs.workflow.Churn.class.getName() + " [options] [<xml results> [<history]] ");
        edu.umd.cs.findbugs.SortedBugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (argCount < args.length) bugCollection.readXML(args[argCount++]);
        else bugCollection.readXML(java.lang.System.in);
        churn.setBugCollection(bugCollection);
        churn.execute();
        java.io.PrintStream out = java.lang.System.out;
        try {
            if (argCount < args.length) {
                out = edu.umd.cs.findbugs.charsets.UTF8.printStream(new java.io.FileOutputStream(args[argCount++]),true);
            }
            churn.dump(out);
        }
        finally {
            out.close();
        }
    }
}
