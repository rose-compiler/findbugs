/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * Filter out bugs which fail (match) all filters. This is what happens when you
 * filter out a branch.
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.event.TreeModelEvent;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.gui2.BugTreeModel.BranchOperationException;
public class StackedFilterMatcher extends edu.umd.cs.findbugs.gui2.FilterMatcher {
    final private static long serialVersionUID = 3958267780332359162L;
    private edu.umd.cs.findbugs.gui2.FilterMatcher[] filters;
    edu.umd.cs.findbugs.gui2.Sortables getFilterBy() {
        throw new java.lang.UnsupportedOperationException("Stacked filter matchers do not filter out a single Sortables, use getFilters()");
    }
    java.lang.String getValue() {
        throw new java.lang.UnsupportedOperationException("Stacked filter matchers do not filter out a single Sortables's value, use getFilters and getValue individually on returned filters.");
    }
    public StackedFilterMatcher(edu.umd.cs.findbugs.gui2.FilterMatcher[] filters) {
        super(null,null);
        this.filters = filters;
    }
// If only FilterMatcher's setActive were as simple as this one... not.
// See BugTreeModel's long ranting comment about filtering to see the reason
// for all this
// All this does is not force the tree to rebuild when you turn filters for
// branches on and off
    public void setActive(boolean active) {
        javax.swing.event.TreeModelEvent event = null;
        edu.umd.cs.findbugs.gui2.BugTreeModel.TreeModification whatToDo;
        if (active != this.active) {
            if (active == false) this.active = active;
            edu.umd.cs.findbugs.gui2.StackedFilterMatcher theSame = this;
            edu.umd.cs.findbugs.gui2.FilterMatcher[] filtersInStack = theSame.getFilters();
            java.util.ArrayList<edu.umd.cs.findbugs.gui2.Sortables> order = edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSorter().getOrder();
            int sizeToCheck = filtersInStack.length;
            if (order.contains(edu.umd.cs.findbugs.gui2.Sortables.DIVIDER)) if (order.indexOf(edu.umd.cs.findbugs.gui2.Sortables.DIVIDER) < filtersInStack.length) {
                sizeToCheck++;
            }
            java.util.List<edu.umd.cs.findbugs.gui2.Sortables> sortablesToCheck = order.subList(0,java.lang.Math.min(sizeToCheck,order.size()));
            edu.umd.cs.findbugs.gui2.Debug.println("Size to check" + sizeToCheck + " checking list" + sortablesToCheck);
            edu.umd.cs.findbugs.gui2.Debug.println("checking filters");
            java.util.ArrayList<java.lang.String> almostPath = new java.util.ArrayList<java.lang.String>();
            java.util.ArrayList<edu.umd.cs.findbugs.gui2.Sortables> almostPathSortables = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.Sortables>();
            for (int x = 0; x < sortablesToCheck.size(); x++) {
                edu.umd.cs.findbugs.gui2.Sortables s = sortablesToCheck.get(x);
                for (edu.umd.cs.findbugs.gui2.FilterMatcher fm : filtersInStack){
                    if (s.equals(fm.getFilterBy())) {
                        almostPath.add(fm.getValue());
                        almostPathSortables.add(fm.getFilterBy());
                    }
                }
;
            }
            java.util.ArrayList<java.lang.String> finalPath = new java.util.ArrayList<java.lang.String>();
            for (int x = 0; x < almostPath.size(); x++) {
                edu.umd.cs.findbugs.gui2.Sortables s = almostPathSortables.get(x);
                if (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSorter().getOrderBeforeDivider().contains(s)) finalPath.add(almostPath.get(x));
            }
            try {
                if (finalPath.size() == filtersInStack.length) {
                    if (active == true) {
                        event = (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getBugTreeModel()).removeBranch(finalPath);
                        whatToDo = edu.umd.cs.findbugs.gui2.BugTreeModel.TreeModification.REMOVE;
                    }
                    else {
                        event = (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getBugTreeModel()).insertBranch(finalPath);
                        whatToDo = edu.umd.cs.findbugs.gui2.BugTreeModel.TreeModification.INSERT;
                    }
                }
                else {
                    event = (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getBugTreeModel()).restructureBranch(finalPath,active);
// if
// active
// is
// true,
// this
// removes,
// if
// active
// if
// false,
// it
// inserts
                    if (active) whatToDo = edu.umd.cs.findbugs.gui2.BugTreeModel.TreeModification.REMOVERESTRUCTURE;
                    else whatToDo = edu.umd.cs.findbugs.gui2.BugTreeModel.TreeModification.INSERTRESTRUCTURE;
                }
                if (active == true) this.active = active;
                (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getBugTreeModel()).sendEvent(event,whatToDo);
            }
            catch (edu.umd.cs.findbugs.gui2.BugTreeModel.BranchOperationException e){
                this.active = active;
            }
        }
    }
    public edu.umd.cs.findbugs.gui2.FilterMatcher[] getFilters() {
        return filters;
    }
    public boolean match(edu.umd.cs.findbugs.BugInstance bugInstance) {
        if ( !this.isActive()) return true;
        for (edu.umd.cs.findbugs.gui2.FilterMatcher i : filters)if (i.match(bugInstance)) return true;
;
        return false;
    }
    public java.lang.String toString() {
// return "StackedFilterMatcher: " + Arrays.toString(filters);
        java.lang.StringBuilder result = new java.lang.StringBuilder();
        for (int i = 0; i < filters.length - 1; i++) result.append(filters[i].toString() + (i == filters.length - 2 ? " " : ", "));
        if (filters.length > 1) result.append("and ");
        if (filters.length > 0) result.append(filters[filters.length - 1]);
        return result.toString();
    }
    public boolean equals(java.lang.Object o) {
        if (o == null ||  !(o instanceof edu.umd.cs.findbugs.gui2.StackedFilterMatcher)) return false;
        edu.umd.cs.findbugs.gui2.FilterMatcher[] mine = new edu.umd.cs.findbugs.gui2.FilterMatcher[filters.length];
        java.lang.System.arraycopy(this.filters,0,mine,0,mine.length);
        java.util.Arrays.sort(mine);
        edu.umd.cs.findbugs.gui2.FilterMatcher[] others = new edu.umd.cs.findbugs.gui2.FilterMatcher[((edu.umd.cs.findbugs.gui2.StackedFilterMatcher) (o) ).filters.length];
        java.lang.System.arraycopy(((edu.umd.cs.findbugs.gui2.StackedFilterMatcher) (o) ).filters,0,others,0,others.length);
        java.util.Arrays.sort(others);
        return (java.util.Arrays.equals(mine,others));
    }
    public int hashCode() {
        int hash = 0;
        for (edu.umd.cs.findbugs.gui2.FilterMatcher f : filters)hash += f.hashCode();
;
        return hash;
    }
    public static void main(java.lang.String[] args) {
        java.lang.System.out.println(new edu.umd.cs.findbugs.gui2.StackedFilterMatcher(new edu.umd.cs.findbugs.gui2.FilterMatcher[0]).equals(new edu.umd.cs.findbugs.gui2.StackedFilterMatcher(new edu.umd.cs.findbugs.gui2.FilterMatcher[0])));
    }
}
