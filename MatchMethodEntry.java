/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2008, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An ObligationPolicyDatabaseEntry which creates or deletes an obligation based
 * on a call to a specified method.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.obl;
import edu.umd.cs.findbugs.ba.obl.*;
import java.util.Arrays;
import java.util.Collection;
import org.apache.bcel.generic.ReferenceType;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
import edu.umd.cs.findbugs.util.ExactStringMatcher;
import edu.umd.cs.findbugs.util.StringMatcher;
import edu.umd.cs.findbugs.util.SubtypeTypeMatcher;
import edu.umd.cs.findbugs.util.TypeMatcher;
public class MatchMethodEntry extends java.lang.Object implements edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseEntry {
    final private edu.umd.cs.findbugs.util.TypeMatcher receiverType;
    final private edu.umd.cs.findbugs.util.StringMatcher methodName;
    final private edu.umd.cs.findbugs.util.StringMatcher signature;
    final private boolean isStatic;
    final private edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseActionType action;
    final private edu.umd.cs.findbugs.ba.obl.Obligation[] obligations;
    final private edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseEntryType entryType;
    public java.util.Collection<edu.umd.cs.findbugs.ba.obl.Obligation> getAllObligations() {
        return java.util.Arrays.asList(obligations);
    }
/**
     * Constructor. Creates an entry which matches the given XMethod.
     * 
     * @param xmethod
     *            an XMethod
     * @param action
     *            ActionType (ADD or DEL, depending on whether obligation is
     *            added or deleted)
     * @param entryType
     *            entry type
     * @param obligations
     *            Obligation to be added or deleted
     */
    public MatchMethodEntry(edu.umd.cs.findbugs.ba.XMethod xmethod, edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseActionType action, edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseEntryType entryType, edu.umd.cs.findbugs.ba.obl.Obligation[] obligations) {
        this(new edu.umd.cs.findbugs.util.SubtypeTypeMatcher(xmethod.getClassDescriptor()),new edu.umd.cs.findbugs.util.ExactStringMatcher(xmethod.getName()),new edu.umd.cs.findbugs.util.ExactStringMatcher(xmethod.getSignature()),xmethod.isStatic(),action,entryType,obligations);
    }
    public MatchMethodEntry(edu.umd.cs.findbugs.classfile.MethodDescriptor method, edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseActionType action, edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseEntryType entryType, edu.umd.cs.findbugs.ba.obl.Obligation[] obligations) {
        this(new edu.umd.cs.findbugs.util.SubtypeTypeMatcher(method.getClassDescriptor()),new edu.umd.cs.findbugs.util.ExactStringMatcher(method.getName()),new edu.umd.cs.findbugs.util.ExactStringMatcher(method.getSignature()),method.isStatic(),action,entryType,obligations);
    }
/**
     * Constructor.
     * 
     * @param receiverType
     *            TypeMatcher to match the receiver type (or class containing
     *            static method)
     * @param methodName
     *            StringMatcher to match name of called method
     * @param signature
     *            StringMatcher to match signature of called method
     * @param isStatic
     *            true if matched method must be static, false otherwise
     * @param action
     *            ActionType (ADD or DEL, depending on whether obligation is
     *            added or deleted)
     * @param entryType
     *            entry type
     * @param obligations
     *            Obligation to be added or deleted
     */
    public MatchMethodEntry(edu.umd.cs.findbugs.util.TypeMatcher receiverType, edu.umd.cs.findbugs.util.StringMatcher methodName, edu.umd.cs.findbugs.util.StringMatcher signature, boolean isStatic, edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseActionType action, edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseEntryType entryType, edu.umd.cs.findbugs.ba.obl.Obligation[] obligations) {
        super();
        this.receiverType = receiverType;
        this.methodName = methodName;
        this.signature = signature;
        this.isStatic = isStatic;
        this.action = action;
        this.obligations = obligations;
        this.entryType = entryType;
    }
    public edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseEntryType getEntryType() {
        return entryType;
    }
    public boolean getActions(org.apache.bcel.generic.ReferenceType receiverType, java.lang.String methodName, java.lang.String signature, boolean isStatic, java.util.Collection<edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseAction> actionList) {
        if (this.methodName.matches(methodName) && this.signature.matches(signature) && this.isStatic == isStatic && this.receiverType.matches(receiverType)) {
            for (edu.umd.cs.findbugs.ba.obl.Obligation o : obligations)actionList.add(new edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseAction(action, o));
;
            return true;
        }
        return false;
    }
    public java.lang.String toString() {
        return "(" + receiverType + "," + methodName + "," + signature + "," + isStatic + "," + action + "," + java.util.Arrays.asList(obligations) + "," + entryType + ")";
    }
}
