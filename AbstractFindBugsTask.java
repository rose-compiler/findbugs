/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Abstract base class for Ant tasks that run programs (main() methods) in
 * findbugs.jar or findbugsGUI.jar.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Java;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.Reference;
abstract public class AbstractFindBugsTask extends org.apache.tools.ant.Task {
// twenty minutes
// A System property to set when FindBugs is run
    public static class SystemProperty extends java.lang.Object {
        private java.lang.String name;
        private java.lang.String value;
        public SystemProperty() {
            super();
        }
        public void setName(java.lang.String name) {
            this.name = name;
        }
        public void setValue(java.lang.String value) {
            this.value = value;
        }
        public java.lang.String getName() {
            return name;
        }
        public java.lang.String getValue() {
            return value;
        }
    }
    final public static java.lang.String FINDBUGS_JAR = "findbugs.jar";
    final public static long DEFAULT_TIMEOUT = 1200000;
    final public static java.lang.String RESULT_PROPERTY_SUFFIX = "executeReturnCode";
    private java.lang.String mainClass;
    private boolean debug = false;
    private java.io.File homeDir = null;
    private java.lang.String jvm = "";
    private java.lang.String jvmargs = "";
    private long timeout = DEFAULT_TIMEOUT;
    private boolean failOnError = false;
    private java.lang.String errorProperty = null;
    private java.util.List<edu.umd.cs.findbugs.anttask.AbstractFindBugsTask.SystemProperty> systemPropertyList = new java.util.ArrayList<edu.umd.cs.findbugs.anttask.AbstractFindBugsTask.SystemProperty>();
    private org.apache.tools.ant.types.Path classpath = null;
    private org.apache.tools.ant.types.Path pluginList = null;
    private org.apache.tools.ant.taskdefs.Java findbugsEngine = null;
    public java.lang.String execResultProperty = "edu.umd.cs.findbugs.anttask.AbstractFindBugsTask." + RESULT_PROPERTY_SUFFIX;
/**
     * Constructor.
     */
    public AbstractFindBugsTask(java.lang.String mainClass) {
        super();
        this.mainClass = mainClass;
        this.execResultProperty = mainClass + "." + RESULT_PROPERTY_SUFFIX;
    }
/**
     * Set the home directory into which findbugs was installed
     */
    public void setHome(java.io.File homeDir) {
        this.homeDir = homeDir;
    }
/**
     * Set the debug flag
     */
    public void setDebug(boolean flag) {
        this.debug = flag;
    }
/**
     * Get the debug flag.
     */
    protected boolean getDebug() {
        return debug;
    }
/**
     * Set any specific jvm args
     */
    public void setJvmargs(java.lang.String args) {
        this.jvmargs = args;
    }
/**
     * Set the command used to start the VM
     */
    public void setJvm(java.lang.String jvm) {
        this.jvm = jvm;
    }
/**
     * Set timeout in milliseconds.
     * 
     * @param timeout
     *            the timeout
     */
    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
/**
     * Set the failOnError flag
     */
    public void setFailOnError(boolean flag) {
        this.failOnError = flag;
    }
/**
     * Tells this task to set the property with the given name to "true" when
     * there were errors.
     */
    public void setErrorProperty(java.lang.String name) {
        this.errorProperty = name;
    }
/**
     * Create a SystemProperty (to handle &lt;systemProperty&gt; elements).
     */
    public edu.umd.cs.findbugs.anttask.AbstractFindBugsTask.SystemProperty createSystemProperty() {
        edu.umd.cs.findbugs.anttask.AbstractFindBugsTask.SystemProperty systemProperty = new edu.umd.cs.findbugs.anttask.AbstractFindBugsTask.SystemProperty();
        systemPropertyList.add(systemProperty);
        return systemProperty;
    }
/**
     * Set the classpath to use.
     */
    public void setClasspath(org.apache.tools.ant.types.Path src) {
        if (classpath == null) {
            classpath = src;
        }
        else {
            classpath.append(src);
        }
    }
/**
     * Path to use for classpath.
     */
    public org.apache.tools.ant.types.Path createClasspath() {
        if (classpath == null) {
            classpath = new org.apache.tools.ant.types.Path(this.getProject());
        }
        return classpath.createPath();
    }
/**
     * Adds a reference to a classpath defined elsewhere.
     */
    public void setClasspathRef(org.apache.tools.ant.types.Reference r) {
        org.apache.tools.ant.types.Path path = this.createClasspath();
        path.setRefid(r);
        path.toString();
    }
// Evaluated for its side-effects (throwing a
// BuildException)
/**
     * the plugin list to use.
     */
    public void setPluginList(org.apache.tools.ant.types.Path src) {
        if (pluginList == null) {
            pluginList = src;
        }
        else {
            pluginList.append(src);
        }
    }
/**
     * Path to use for plugin list.
     */
    public org.apache.tools.ant.types.Path createPluginList() {
        if (pluginList == null) {
            pluginList = new org.apache.tools.ant.types.Path(this.getProject());
        }
        return pluginList.createPath();
    }
/**
     * Adds a reference to a plugin list defined elsewhere.
     */
    public void setPluginListRef(org.apache.tools.ant.types.Reference r) {
        this.createPluginList().setRefid(r);
    }
    public void execute() throws org.apache.tools.ant.BuildException {
        this.checkParameters();
        try {
            this.execFindbugs();
        }
        catch (org.apache.tools.ant.BuildException e){
// log("Oops: " + e.getMessage());
            if (errorProperty != null) {
                this.getProject().setProperty(errorProperty,"true");
            }
            if (failOnError) {
                throw e;
            }
        }
    }
/**
     * Check that all required attributes have been set.
     */
    protected void checkParameters() {
        if (homeDir == null && classpath == null) {
            throw new org.apache.tools.ant.BuildException("either home attribute or classpath attributes  must be defined for task <" + this.getTaskName() + "/>", this.getLocation());
        }
        if (pluginList != null) {
// Make sure that all plugins are actually Jar files.
            java.lang.String[] pluginFileList = pluginList.list();
            for (java.lang.String pluginFile : pluginFileList){
                if ( !pluginFile.endsWith(".jar")) {
                    throw new org.apache.tools.ant.BuildException("plugin file " + pluginFile + " is not a Jar file " + "in task <" + this.getTaskName() + "/>", this.getLocation());
                }
            }
;
        }
        for (edu.umd.cs.findbugs.anttask.AbstractFindBugsTask.SystemProperty systemProperty : systemPropertyList){
            if (systemProperty.getName() == null || systemProperty.getValue() == null) throw new org.apache.tools.ant.BuildException("systemProperty elements must have name and value attributes");
        }
;
    }
/**
     * Create the FindBugs engine (the Java process that will run whatever
     * FindBugs-related program this task is going to execute).
     */
    protected void createFindbugsEngine() {
        findbugsEngine = new org.apache.tools.ant.taskdefs.Java();
        findbugsEngine.setProject(this.getProject());
        findbugsEngine.setTaskName(this.getTaskName());
        findbugsEngine.setFork(true);
        if (jvm.length() > 0) findbugsEngine.setJvm(jvm);
        findbugsEngine.setTimeout(timeout);
        if (debug) {
            jvmargs = jvmargs + " -Dfindbugs.debug=true";
        }
        jvmargs = jvmargs + " -Dfindbugs.hostApp=FBAntTask";
        findbugsEngine.createJvmarg().setLine(jvmargs);
// Add JVM arguments for system properties
        for (edu.umd.cs.findbugs.anttask.AbstractFindBugsTask.SystemProperty systemProperty : systemPropertyList){
            java.lang.String jvmArg = "-D" + systemProperty.getName() + "=" + systemProperty.getValue();
            findbugsEngine.createJvmarg().setValue(jvmArg);
        }
;
        if (homeDir != null) {
// Use findbugs.home to locate findbugs.jar and the standard
// plugins. This is the usual means of initialization.
            java.io.File findbugsLib = new java.io.File(homeDir, "lib");
            if ( !findbugsLib.exists() && homeDir.getName().equals("lib")) {
                findbugsLib = homeDir;
                homeDir = homeDir.getParentFile();
            }
            java.io.File findbugsLibFindBugs = new java.io.File(findbugsLib, "findbugs.jar");
// log("executing using home dir [" + homeDir + "]");
            if (findbugsLibFindBugs.exists()) findbugsEngine.setClasspath(new org.apache.tools.ant.types.Path(this.getProject(), findbugsLibFindBugs.getPath()));
            else throw new java.lang.IllegalArgumentException("Can't find findbugs.jar in " + homeDir);
            findbugsEngine.createJvmarg().setValue("-Dfindbugs.home=" + homeDir.getPath());
        }
        else {
            findbugsEngine.setClasspath(classpath);
        }
        if (pluginList != null) {
            this.addArg("-pluginList");
            this.addArg(pluginList.toString());
        }
        findbugsEngine.setClassname(mainClass);
    }
// Set the main class to be whatever the subclass's constructor
// specified.
/**
     * Get the Findbugs engine.
     */
    protected org.apache.tools.ant.taskdefs.Java getFindbugsEngine() {
        return findbugsEngine;
    }
/**
     * Add an argument to the JVM used to execute FindBugs.
     * 
     * @param arg
     *            the argument
     */
    protected void addArg(java.lang.String arg) {
        findbugsEngine.createArg().setValue(arg.trim());
    }
/**
     * Sets the given string to be piped to standard input of the FindBugs JVM
     * upon launching.
     */
    protected void setInputString(java.lang.String input) {
        findbugsEngine.setInputString(input);
    }
/**
     * Create a new JVM to do the work.
     * 
     * @since Ant 1.5
     */
    private void execFindbugs() throws org.apache.tools.ant.BuildException {
        java.lang.System.out.println("Executing findbugs from ant task");
        this.createFindbugsEngine();
        this.configureFindbugsEngine();
        this.beforeExecuteJavaProcess();
        if (this.getDebug()) {
            this.log(this.getFindbugsEngine().getCommandLine().describeCommand());
        }
/*
         * set property containing return code of child process using a task
         * identifier and a UUID to ensure exit code corresponds to this
         * execution (the base Ant Task won't overwrite return code once it's
         * been set, so unique identifiers must be used for each execution if we
         * want to get the exit code)
         */
        java.lang.String execReturnCodeIdentifier = execResultProperty + "." + java.util.UUID.randomUUID().toString();
        this.getFindbugsEngine().setResultProperty(execReturnCodeIdentifier);
        this.getFindbugsEngine().setFailonerror(false);
/*
         * if the execution fails, we'll report it ourself -- prevent the
         * underlying Ant Java object from throwing an exception
         */
        try {
            this.getFindbugsEngine().execute();
        }
        catch (org.apache.tools.ant.BuildException be){
            this.log(be.toString());
        }
        java.lang.String returnProperty = this.getFindbugsEngine().getProject().getProperty(execReturnCodeIdentifier);
        int rc = returnProperty == null ? 0 : java.lang.Integer.valueOf(returnProperty).intValue();
        this.afterExecuteJavaProcess(rc);
    }
    abstract protected void configureFindbugsEngine();
    abstract protected void beforeExecuteJavaProcess();
    protected void afterExecuteJavaProcess(int rc) {
        if (rc != 0) throw new org.apache.tools.ant.BuildException("execution of " + this.getTaskName() + " failed");
    }
}
