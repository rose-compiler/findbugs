/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Factory to determine which fields are loaded and stored by the instructions
 * in a method, and the overall method. The main purpose is to support efficient
 * redundant load elimination and forward substitution in ValueNumberAnalysis
 * (there is no need to remember stores of fields that are never read, or loads
 * of fields that are only loaded in one location). However, it might be useful
 * for other kinds of analysis.
 * 
 * <p>
 * The tricky part is that in addition to fields loaded and stored with
 * get/putfield and get/putstatic, we also try to figure out field accessed
 * through calls to inner-class access methods.
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import java.util.BitSet;
import org.apache.bcel.Constants;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FieldInstruction;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.InnerClassAccess;
import edu.umd.cs.findbugs.ba.XField;
import edu.umd.cs.findbugs.ba.vna.LoadedFieldSet;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class LoadedFieldSetFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.vna.LoadedFieldSet> {
    final static java.util.BitSet fieldInstructionOpcodeSet = new java.util.BitSet();
    static {
        fieldInstructionOpcodeSet.set(org.apache.bcel.Constants.GETFIELD);
        fieldInstructionOpcodeSet.set(org.apache.bcel.Constants.PUTFIELD);
        fieldInstructionOpcodeSet.set(org.apache.bcel.Constants.GETSTATIC);
        fieldInstructionOpcodeSet.set(org.apache.bcel.Constants.PUTSTATIC);
    }
/**
     * Constructor.
     */
    public LoadedFieldSetFactory() {
        super("loaded field set factory",edu.umd.cs.findbugs.ba.vna.LoadedFieldSet.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.vna.LoadedFieldSet analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.generic.MethodGen methodGen = this.getMethodGen(analysisCache,descriptor);
        if (methodGen == null) return null;
        org.apache.bcel.generic.InstructionList il = methodGen.getInstructionList();
        edu.umd.cs.findbugs.ba.vna.LoadedFieldSet loadedFieldSet = new edu.umd.cs.findbugs.ba.vna.LoadedFieldSet(methodGen);
        org.apache.bcel.generic.ConstantPoolGen cpg = this.getConstantPoolGen(analysisCache,descriptor.getClassDescriptor());
        for (org.apache.bcel.generic.InstructionHandle handle = il.getStart(); handle != null; handle = handle.getNext()) {
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            short opcode = ins.getOpcode();
            try {
                if (opcode == org.apache.bcel.Constants.INVOKESTATIC) {
                    org.apache.bcel.generic.INVOKESTATIC inv = (org.apache.bcel.generic.INVOKESTATIC) (ins) ;
                    if (edu.umd.cs.findbugs.ba.Hierarchy.isInnerClassAccess(inv,cpg)) {
                        edu.umd.cs.findbugs.ba.InnerClassAccess access = edu.umd.cs.findbugs.ba.Hierarchy.getInnerClassAccess(inv,cpg);
                        if (access != null) {
                            if (access.isLoad()) loadedFieldSet.addLoad(handle,access.getField());
                            else loadedFieldSet.addStore(handle,access.getField());
                        }
                    }
                }
                else if (fieldInstructionOpcodeSet.get(opcode)) {
                    boolean isLoad = (opcode == org.apache.bcel.Constants.GETFIELD || opcode == org.apache.bcel.Constants.GETSTATIC);
                    edu.umd.cs.findbugs.ba.XField field = edu.umd.cs.findbugs.ba.Hierarchy.findXField((org.apache.bcel.generic.FieldInstruction) (ins) ,cpg);
                    if (field != null) {
                        if (isLoad) loadedFieldSet.addLoad(handle,field);
                        else loadedFieldSet.addStore(handle,field);
                    }
                }
            }
            catch (java.lang.ClassNotFoundException e){
                edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getLookupFailureCallback().reportMissingClass(e);
            }
        }
/*
                         * if (access == null) {
                         * System.out.println("Missing inner class access in " +
                         * SignatureConverter.convertMethodSignature(methodGen)
                         * + " at " + inv); }
                         */
        return loadedFieldSet;
    }
}
