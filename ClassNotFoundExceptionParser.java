/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Parse the detail message in a ClassNotFoundException to extract the name of
 * the missing class. Unfortunately, this information is not directly available
 * from the exception object. So, this class parses the detail message in
 * several common formats (such as the format used by BCEL).
 *
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.ResourceNotFoundException;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
import edu.umd.cs.findbugs.util.ClassName;
public class ClassNotFoundExceptionParser extends java.lang.Object {
    public ClassNotFoundExceptionParser() {
    }
// BCEL reports missing classes in this format
    final private static java.util.regex.Pattern BCEL_MISSING_CLASS_PATTERN = java.util.regex.Pattern.compile("^.*while looking for class ([^:]*):.*$");
// edu.umd.cs.findbugs.ba.type.TypeRepository
// and edu.umd.cs.findbugs.ba.ch.Subtypes2 uses this format
    final private static java.util.regex.Pattern TYPE_REPOSITORY_MISSING_CLASS_PATTERN = java.util.regex.Pattern.compile("^Class ([^ ]*) cannot be resolved.*$");
    final private static java.util.regex.Pattern[] patternList;
    static {
        java.util.ArrayList<java.util.regex.Pattern> list = new java.util.ArrayList<java.util.regex.Pattern>();
        list.add(BCEL_MISSING_CLASS_PATTERN);
        list.add(TYPE_REPOSITORY_MISSING_CLASS_PATTERN);
        patternList = list.toArray(new java.util.regex.Pattern[list.size()]);
    }
/**
     * Get the name of the missing class from a ClassNotFoundException.
     *
     * @param ex
     *            the ClassNotFoundException
     * @return the name of the missing class, or null if we couldn't figure out
     *         the class name
     */
    public static java.lang.String getMissingClassName(java.lang.ClassNotFoundException ex) {
// If the exception has a ResourceNotFoundException as the cause,
// then we have an easy answer.
        java.lang.Throwable cause = ex.getCause();
        if (cause instanceof edu.umd.cs.findbugs.classfile.ResourceNotFoundException) {
            java.lang.String resourceName = ((edu.umd.cs.findbugs.classfile.ResourceNotFoundException) (cause) ).getResourceName();
            if (resourceName != null) {
                edu.umd.cs.findbugs.classfile.ClassDescriptor classDesc = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptorFromResourceName(resourceName);
                return classDesc.toDottedClassName();
            }
        }
        if (ex.getMessage() == null) {
            return null;
        }
// Try the regular expression patterns to parse the class name
// from the exception message.
        for (java.util.regex.Pattern pattern : patternList){
            java.util.regex.Matcher matcher = pattern.matcher(ex.getMessage());
            if (matcher.matches()) {
                java.lang.String className = matcher.group(1);
                edu.umd.cs.findbugs.util.ClassName.assertIsDotted(className);
                return className;
            }
        }
;
        return null;
    }
}
// vim:ts=4
