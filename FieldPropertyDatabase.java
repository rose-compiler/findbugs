/*
 * Bytecode analysis framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Interprocedural field property database.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.interproc;
import edu.umd.cs.findbugs.ba.interproc.*;
import java.io.IOException;
import java.io.Writer;
import org.apache.bcel.Constants;
import edu.umd.cs.findbugs.ba.XFactory;
import edu.umd.cs.findbugs.ba.XField;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.FieldDescriptor;
import edu.umd.cs.findbugs.util.ClassName;
abstract public class FieldPropertyDatabase<Property> extends edu.umd.cs.findbugs.ba.interproc.PropertyDatabase<edu.umd.cs.findbugs.classfile.FieldDescriptor, Property> {
    public FieldPropertyDatabase() {
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.interproc.PropertyDatabase#parseKey(java.lang.
     * String)
     */
    protected edu.umd.cs.findbugs.classfile.FieldDescriptor parseKey(java.lang.String s) throws edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException {
        java.lang.String[] tuple = s.split(",");
        if (tuple.length != 4) {
            throw new edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException("Invalid field tuple: " + s);
        }
        java.lang.String className = edu.umd.cs.findbugs.ba.XFactory.canonicalizeString(tuple[0]);
        java.lang.String fieldName = edu.umd.cs.findbugs.ba.XFactory.canonicalizeString(tuple[1]);
        java.lang.String signature = edu.umd.cs.findbugs.ba.XFactory.canonicalizeString(tuple[2]);
        int accessFlags;
        try {
            accessFlags = java.lang.Integer.parseInt(tuple[3]);
        }
        catch (java.lang.NumberFormatException e){
            throw new edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException("Invalid field access flags: " + tuple[3]);
        }
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getFieldDescriptor(edu.umd.cs.findbugs.util.ClassName.toSlashedClassName(className),fieldName,signature,(accessFlags & org.apache.bcel.Constants.ACC_STATIC) != 0);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.interproc.PropertyDatabase#writeKey(java.io.Writer
     * , KeyType)
     */
    protected void writeKey(java.io.Writer writer, edu.umd.cs.findbugs.classfile.FieldDescriptor key) throws java.io.IOException {
        writer.write(key.getClassDescriptor().getDottedClassName());
        writer.write(",");
        writer.write(key.getName());
        writer.write(",");
        writer.write(key.getSignature());
        writer.write(",");
        edu.umd.cs.findbugs.ba.XField xField = edu.umd.cs.findbugs.ba.XFactory.createXField(key);
        int flags = xField.getAccessFlags() & 0xf;
        writer.write(java.lang.String.valueOf(flags));
    }
}
