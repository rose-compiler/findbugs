/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Warning properties for FindRefComparison detector.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import edu.umd.cs.findbugs.props.AbstractWarningProperty;
import edu.umd.cs.findbugs.props.PriorityAdjustment;
public class RefComparisonWarningProperty extends edu.umd.cs.findbugs.props.AbstractWarningProperty {
    public RefComparisonWarningProperty(java.lang.String name, edu.umd.cs.findbugs.props.PriorityAdjustment priorityAdjustment) {
        super(name,priorityAdjustment);
    }
/** There is a call to equals() in the method. */
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty SAW_CALL_TO_EQUALS = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("SAW_CALL_TO_EQUALS", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_LOW);
/** Method is private (or package-protected). */
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty PRIVATE_METHOD = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("PRIVATE_METHOD", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_MEDIUM);
/** Compare inside test case */
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty COMPARE_IN_TEST_CASE = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("COMPARE_IN_TEST_CASE", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_LOW);
/** Comparing static strings using equals operator. */
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty COMPARE_STATIC_STRINGS = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("COMPARE_STATIC_STRINGS", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** Comparing a dynamic string using equals operator. */
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty DYNAMIC_AND_UNKNOWN = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("DYNAMIC_AND_UNKNOWN", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty STRING_PARAMETER_IN_PUBLIC_METHOD = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("STATIC_AND_PARAMETER_IN_PUBLIC_METHOD", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty STRING_PARAMETER = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("STATIC_AND_PARAMETER", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** Comparing static string and an unknown string. */
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty STATIC_AND_UNKNOWN = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("STATIC_AND_UNKNOWN", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
/** Comparing static string and an unknown string. */
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty EMPTY_AND_UNKNOWN = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("EMPTY_AND_UNKNOWN", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** Saw a call to String.intern(). */
    final public static edu.umd.cs.findbugs.detect.RefComparisonWarningProperty SAW_INTERN = new edu.umd.cs.findbugs.detect.RefComparisonWarningProperty("SAW_INTERN", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
}
