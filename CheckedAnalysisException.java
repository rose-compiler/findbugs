/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Common superclass for all checked exceptions that can be thrown while
 * performing some kind of analysis.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile;
import edu.umd.cs.findbugs.classfile.*;
public class CheckedAnalysisException extends java.lang.Exception {
/**
     * Constructor.
     */
    public CheckedAnalysisException() {
        super();
    }
    public CheckedAnalysisException(edu.umd.cs.findbugs.classfile.CheckedAnalysisException e) {
        super(e.getMessage(),e.getCause());
    }
/**
     * Constructor.
     * 
     * @param msg
     *            message
     */
    public CheckedAnalysisException(java.lang.String msg) {
        super(msg);
    }
/**
     * Constructor.
     * 
     * @param msg
     *            message
     * @param cause
     *            root cause of this exception
     */
    public CheckedAnalysisException(java.lang.String msg, java.lang.Throwable cause) {
        super(msg,cause);
    }
}
