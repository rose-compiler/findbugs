/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.charsets;
import edu.umd.cs.findbugs.charsets.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.Charset;
public class SourceCharset extends java.lang.Object {
    public SourceCharset() {
    }
    final public static java.nio.charset.Charset charset;
    static {
        charset = java.nio.charset.Charset.defaultCharset();
    }
    public static java.io.Writer fileWriter(java.io.File fileName) throws java.io.IOException {
        return new java.io.OutputStreamWriter(new java.io.FileOutputStream(fileName), charset);
    }
    public static java.io.PrintWriter printWriter(java.io.File fileName) throws java.io.IOException {
        return new java.io.PrintWriter(new java.io.BufferedWriter(fileWriter(fileName)));
    }
    public static java.io.Writer fileWriter(java.lang.String fileName) throws java.io.IOException {
        return new java.io.OutputStreamWriter(new java.io.FileOutputStream(fileName), charset);
    }
    public static java.io.PrintWriter printWriter(java.lang.String fileName) throws java.io.IOException {
        return new java.io.PrintWriter(new java.io.BufferedWriter(fileWriter(fileName)));
    }
    public static java.io.BufferedReader bufferedReader(java.io.InputStream in) {
        return new java.io.BufferedReader(new java.io.InputStreamReader(in, charset));
    }
}
