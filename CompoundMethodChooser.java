/*
 * Bytecode analysis framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
public class CompoundMethodChooser extends java.lang.Object implements edu.umd.cs.findbugs.ba.JavaClassAndMethodChooser {
    edu.umd.cs.findbugs.ba.JavaClassAndMethodChooser[] conjunctList;
    public CompoundMethodChooser(edu.umd.cs.findbugs.ba.JavaClassAndMethodChooser[] conjunctList) {
        super();
        this.conjunctList = conjunctList;
    }
    public boolean choose(edu.umd.cs.findbugs.ba.JavaClassAndMethod javaClassAndMethod) {
        for (edu.umd.cs.findbugs.ba.JavaClassAndMethodChooser chooser : conjunctList){
            if ( !chooser.choose(javaClassAndMethod)) return false;
        }
;
        return true;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.JavaClassAndMethodChooser#choose(edu.umd.cs.findbugs
     * .ba.XMethod)
     */
    public boolean choose(edu.umd.cs.findbugs.ba.XMethod method) {
        for (edu.umd.cs.findbugs.ba.JavaClassAndMethodChooser chooser : conjunctList){
            if ( !chooser.choose(method)) return false;
        }
;
        return true;
    }
}
