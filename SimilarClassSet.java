/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A set of classes considered similar because of their features.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.model;
import edu.umd.cs.findbugs.model.*;
import java.util.LinkedList;
import java.util.List;
public class SimilarClassSet extends java.lang.Object {
    private java.util.List<edu.umd.cs.findbugs.model.ClassFeatureSet> memberList;
    public SimilarClassSet() {
        super();
        this.memberList = new java.util.LinkedList<edu.umd.cs.findbugs.model.ClassFeatureSet>();
    }
    public boolean shouldContain(edu.umd.cs.findbugs.model.ClassFeatureSet candidate) {
        for (edu.umd.cs.findbugs.model.ClassFeatureSet member : memberList){
            if (candidate.similarTo(member)) return true;
        }
;
        return false;
    }
    public void addMember(edu.umd.cs.findbugs.model.ClassFeatureSet member) {
        memberList.add(member);
    }
    public java.lang.String getRepresentativeClassName() {
        if (memberList.isEmpty()) throw new java.lang.IllegalStateException();
        return memberList.get(0).getClassName();
    }
    public int size() {
        return memberList.size();
    }
}
