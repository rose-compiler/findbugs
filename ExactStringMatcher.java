/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2008, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * Exact String-matching predicate.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.util;
import edu.umd.cs.findbugs.util.*;
public class ExactStringMatcher extends java.lang.Object implements edu.umd.cs.findbugs.util.StringMatcher {
    final private java.lang.String expected;
/**
     * Constructor.
     * 
     * @param expected
     *            the expected string value
     */
    public ExactStringMatcher(java.lang.String expected) {
        super();
        this.expected = expected;
    }
    public boolean matches(java.lang.String s) {
        return this.expected.equals(s);
    }
    public java.lang.String toString() {
        return expected;
    }
}
