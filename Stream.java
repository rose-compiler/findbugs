/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A Stream object marks the location in the code where a stream is created. It
 * also is responsible for determining some aspects of how the stream state is
 * tracked by the ResourceValueAnalysis, such as when the stream is opened or
 * closed, and whether implicit exception edges are significant.
 * <p/>
 * <p>
 * TODO: change streamClass and streamBase to ObjectType
 * <p/>
 * <p>
 * TODO: isStreamOpen() and isStreamClose() should probably be abstract, so we
 * can customize how they work for different kinds of streams
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import org.apache.bcel.Constants;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKEINTERFACE;
import org.apache.bcel.generic.INVOKESPECIAL;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InvokeInstruction;
import edu.umd.cs.findbugs.ResourceCreationPoint;
import edu.umd.cs.findbugs.ba.BasicBlock;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback;
import edu.umd.cs.findbugs.ba.ResourceValue;
import edu.umd.cs.findbugs.ba.ResourceValueFrame;
public class Stream extends edu.umd.cs.findbugs.ResourceCreationPoint implements java.lang.Comparable<edu.umd.cs.findbugs.detect.Stream> {
    private java.lang.String streamBase;
    private boolean isUninteresting;
    private boolean isOpenOnCreation;
    private edu.umd.cs.findbugs.ba.Location openLocation;
    private boolean ignoreImplicitExceptions;
    private java.lang.String bugType;
    private int instanceParam;
    private boolean isClosed;
    public java.lang.String toString() {
        return streamBase + ":" + openLocation;
    }
/**
     * Constructor. By default, Stream objects are marked as uninteresting.
     * setInteresting("BUG_TYPE") must be called explicitly to mark the Stream
     * as interesting.
     * 
     * @param location
     *            where the stream is created
     * @param streamClass
     *            type of Stream
     * @param streamBase
     *            highest class in the class hierarchy through which stream's
     *            close() method could be called
     */
    public Stream(edu.umd.cs.findbugs.ba.Location location, java.lang.String streamClass, java.lang.String streamBase) {
        super(location,streamClass);
        this.streamBase = streamBase;
        isUninteresting = true;
        instanceParam =  -1;
    }
/**
     * Mark this Stream as interesting.
     * 
     * @param bugType
     *            the bug type that should be reported if the stream is not
     *            closed on all paths out of the method
     */
    public edu.umd.cs.findbugs.detect.Stream setInteresting(java.lang.String bugType) {
        this.isUninteresting = false;
        this.bugType = bugType;
        return this;
    }
/**
     * Mark whether or not implicit exception edges should be ignored by
     * ResourceValueAnalysis when determining whether or not stream is closed on
     * all paths out of method.
     */
    public edu.umd.cs.findbugs.detect.Stream setIgnoreImplicitExceptions(boolean enable) {
        ignoreImplicitExceptions = enable;
        return this;
    }
/**
     * Mark whether or not Stream is open as soon as it is created, or whether a
     * later method or constructor must explicitly open it.
     */
    public edu.umd.cs.findbugs.detect.Stream setIsOpenOnCreation(boolean enable) {
        isOpenOnCreation = enable;
        return this;
    }
/**
     * Set the number of the parameter which passes the stream instance.
     * 
     * @param instanceParam
     *            number of the parameter passing the stream instance
     */
    public void setInstanceParam(int instanceParam) {
        this.instanceParam = instanceParam;
    }
/**
     * Set this Stream has having been closed on all paths out of the method.
     */
    public void setClosed() {
        isClosed = true;
    }
    public java.lang.String getStreamBase() {
        return streamBase;
    }
    public boolean isUninteresting() {
        return isUninteresting;
    }
    public boolean isOpenOnCreation() {
        return isOpenOnCreation;
    }
    public void setOpenLocation(edu.umd.cs.findbugs.ba.Location openLocation) {
        this.openLocation = openLocation;
    }
    public edu.umd.cs.findbugs.ba.Location getOpenLocation() {
        return openLocation;
    }
    public boolean ignoreImplicitExceptions() {
        return ignoreImplicitExceptions;
    }
    public int getInstanceParam() {
        return instanceParam;
    }
    public java.lang.String getBugType() {
        return bugType;
    }
/**
     * Return whether or not the Stream is closed on all paths out of the
     * method.
     */
    public boolean isClosed() {
        return isClosed;
    }
    public boolean isStreamOpen(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.ba.ResourceValueFrame frame) {
        if (isOpenOnCreation) return false;
        org.apache.bcel.generic.Instruction ins = handle.getInstruction();
        if ( !(ins instanceof org.apache.bcel.generic.INVOKESPECIAL)) return false;
// Does this instruction open the stream?
        org.apache.bcel.generic.INVOKESPECIAL inv = (org.apache.bcel.generic.INVOKESPECIAL) (ins) ;
        return frame.isValid() && this.getInstanceValue(frame,inv,cpg).isInstance() && this.matchMethod(inv,cpg,this.getResourceClass(),"<init>");
    }
    public static boolean mightCloseStream(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.ConstantPoolGen cpg) {
        org.apache.bcel.generic.Instruction ins = handle.getInstruction();
        if ((ins instanceof org.apache.bcel.generic.INVOKEVIRTUAL) || (ins instanceof org.apache.bcel.generic.INVOKEINTERFACE)) {
// Does this instruction close the stream?
            org.apache.bcel.generic.InvokeInstruction inv = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
// It's a close if the invoked class is any subtype of the stream
// base class.
// (Basically, we may not see the exact original stream class,
// even though it's the same instance.)
            return inv.getName(cpg).equals("close") && inv.getSignature(cpg).equals("()V");
        }
        return false;
    }
    public boolean isStreamClose(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.ba.ResourceValueFrame frame, edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback lookupFailureCallback) {
        if ( !mightCloseStream(basicBlock,handle,cpg)) return false;
        org.apache.bcel.generic.Instruction ins = handle.getInstruction();
        if ((ins instanceof org.apache.bcel.generic.INVOKEVIRTUAL) || (ins instanceof org.apache.bcel.generic.INVOKEINTERFACE)) {
// Does this instruction close the stream?
            org.apache.bcel.generic.InvokeInstruction inv = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
            if ( !frame.isValid() ||  !this.getInstanceValue(frame,inv,cpg).isInstance()) return false;
// It's a close if the invoked class is any subtype of the stream
// base class.
// (Basically, we may not see the exact original stream class,
// even though it's the same instance.)
            try {
                java.lang.String classClosed = inv.getClassName(cpg);
                return edu.umd.cs.findbugs.ba.Hierarchy.isSubtype(classClosed,streamBase) || edu.umd.cs.findbugs.ba.Hierarchy.isSubtype(streamBase,classClosed);
            }
            catch (java.lang.ClassNotFoundException e){
                lookupFailureCallback.reportMissingClass(e);
                return false;
            }
        }
        return false;
    }
    private edu.umd.cs.findbugs.ba.ResourceValue getInstanceValue(edu.umd.cs.findbugs.ba.ResourceValueFrame frame, org.apache.bcel.generic.InvokeInstruction inv, org.apache.bcel.generic.ConstantPoolGen cpg) {
        int numConsumed = inv.consumeStack(cpg);
        if (numConsumed == org.apache.bcel.Constants.UNPREDICTABLE) throw new java.lang.IllegalStateException();
        return frame.getValue(frame.getNumSlots() - numConsumed);
    }
    private boolean matchMethod(org.apache.bcel.generic.InvokeInstruction inv, org.apache.bcel.generic.ConstantPoolGen cpg, java.lang.String className, java.lang.String methodName) {
        return inv.getClassName(cpg).equals(className) && inv.getName(cpg).equals(methodName);
    }
    public int hashCode() {
        return this.getLocation().hashCode() + 3 * streamBase.hashCode() + 7 * this.getResourceClass().hashCode() + 11 * instanceParam;
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.detect.Stream)) return false;
        edu.umd.cs.findbugs.detect.Stream other = (edu.umd.cs.findbugs.detect.Stream) (o) ;
        if ( !this.getLocation().equals(other.getLocation())) return false;
        if ( !streamBase.equals(other.streamBase)) return false;
        if ( !this.getResourceClass().equals(other.getResourceClass())) return false;
        if (instanceParam != other.instanceParam) return false;
        return true;
    }
    public int compareTo(edu.umd.cs.findbugs.detect.Stream other) {
        int cmp;
        cmp = this.getLocation().compareTo(other.getLocation());
// The main idea in comparing streams is that
// if they can't be differentiated by location
// and base/stream class, then we should try
// instanceParam. This allows streams passed in
// different parameters to be distinguished.
        if (cmp != 0) return cmp;
        cmp = streamBase.compareTo(other.streamBase);
        if (cmp != 0) return cmp;
        cmp = this.getResourceClass().compareTo(other.getResourceClass());
        if (cmp != 0) return cmp;
        cmp = instanceParam - other.instanceParam;
        if (cmp != 0) return cmp;
        return 0;
    }
}
// vim:ts=3
