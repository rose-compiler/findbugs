/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Ant task to create/update a bug history database.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.apache.tools.ant.BuildException;
public class ComputeBugHistoryTask extends edu.umd.cs.findbugs.anttask.AbstractFindBugsTask {
    private java.io.File outputFile;
    private boolean overrideRevisionNames;
    private boolean noPackageMoves;
    private boolean preciseMatch;
    private boolean precisePriorityMatch;
    private boolean quiet;
    private boolean withMessages;
    private java.util.List<edu.umd.cs.findbugs.anttask.DataFile> dataFileList;
    public ComputeBugHistoryTask() {
        super("edu.umd.cs.findbugs.workflow.Update");
        dataFileList = new java.util.LinkedList<edu.umd.cs.findbugs.anttask.DataFile>();
        this.setFailOnError(true);
    }
    public void setOutput(java.io.File arg) {
        this.outputFile = arg;
    }
    public void setOverrideRevisionNames(boolean arg) {
        this.overrideRevisionNames = arg;
    }
    public void setNoPackageMoves(boolean arg) {
        this.noPackageMoves = arg;
    }
    public void setPreciseMatch(boolean arg) {
        this.preciseMatch = arg;
    }
    public void setPrecisePriorityMatch(boolean arg) {
        this.precisePriorityMatch = arg;
    }
    public void setQuiet(boolean arg) {
        this.quiet = arg;
    }
    public void setWithMessages(boolean arg) {
        this.withMessages = arg;
    }
/**
     * Called to create DataFile objects in response to nested &lt;DataFile&gt;
     * elements.
     * 
     * @return new DataFile object specifying the location of an input data file
     */
    public edu.umd.cs.findbugs.anttask.DataFile createDataFile() {
        edu.umd.cs.findbugs.anttask.DataFile dataFile = new edu.umd.cs.findbugs.anttask.DataFile();
        dataFileList.add(dataFile);
        return dataFile;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#checkParameters()
     */
    protected void checkParameters() {
        super.checkParameters();
        if (outputFile == null) {
            throw new org.apache.tools.ant.BuildException("outputFile attribute must be set", this.getLocation());
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#configureFindbugsEngine
     * ()
     */
    protected void configureFindbugsEngine() {
        this.addArg("-output");
        this.addArg(outputFile.getPath());
        if (overrideRevisionNames) {
            this.addArg("-overrideRevisionNames");
        }
        if (noPackageMoves) {
            this.addArg("-noPackageMoves");
        }
        if (preciseMatch) {
            this.addArg("-preciseMatch");
        }
        if (precisePriorityMatch) {
            this.addArg("-precisePriorityMatch");
        }
        if (quiet) {
            this.addArg("-quiet");
        }
        if (withMessages) {
            this.addArg("-withMessages");
        }
        for (edu.umd.cs.findbugs.anttask.DataFile dataFile : dataFileList){
            this.addArg(dataFile.getName());
        }
;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#beforeExecuteJavaProcess
     * ()
     */
    protected void beforeExecuteJavaProcess() {
        this.log("Running computeBugHistory...");
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#afterExecuteJavaProcess
     * (int)
     */
    protected void afterExecuteJavaProcess(int rc) {
        if (rc == 0) {
            this.log("History database written to " + outputFile.getPath());
        }
        else {
            throw new org.apache.tools.ant.BuildException("execution of " + this.getTaskName() + " failed");
        }
    }
}
