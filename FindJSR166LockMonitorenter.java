/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004,2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Find places where ordinary (balanced) synchronization is performed on JSR166
 * Lock objects. Suggested by Doug Lea.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.BitSet;
import java.util.Iterator;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.Lookup;
import edu.umd.cs.findbugs.MethodAnnotation;
import edu.umd.cs.findbugs.StatelessDetector;
import edu.umd.cs.findbugs.TypeAnnotation;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.ObjectTypeFactory;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.type.TypeFrame;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
final public class FindJSR166LockMonitorenter extends java.lang.Object implements edu.umd.cs.findbugs.Detector, edu.umd.cs.findbugs.StatelessDetector {
/**
     *
     */
    final private static java.lang.String UTIL_CONCURRRENT_SIG_PREFIX = "Ljava/util/concurrent/";
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    final private static org.apache.bcel.generic.ObjectType LOCK_TYPE = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.util.concurrent.locks.Lock");
    public FindJSR166LockMonitorenter(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public java.lang.Object clone() {
        try {
            return super.clone();
        }
        catch (java.lang.CloneNotSupportedException e){
            throw new java.lang.AssertionError(e);
        }
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.JavaClass jclass = classContext.getJavaClass();
        if (jclass.getClassName().startsWith("java.util.concurrent.")) return;
        org.apache.bcel.classfile.Method[] methodList = jclass.getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
// We can ignore methods that don't contain a monitorenter
            java.util.BitSet bytecodeSet = classContext.getBytecodeSet(method);
            if (bytecodeSet == null) continue;
            if (false &&  !bytecodeSet.get(org.apache.bcel.Constants.MONITORENTER)) continue;
            this.analyzeMethod(classContext,method);
        }
;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) {
        org.apache.bcel.generic.ConstantPoolGen cpg = classContext.getConstantPoolGen();
        edu.umd.cs.findbugs.ba.CFG cfg;
        try {
            cfg = classContext.getCFG(method);
        }
        catch (edu.umd.cs.findbugs.ba.CFGBuilderException e1){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Coult not get CFG",e1);
            return;
        }
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow;
        try {
            typeDataflow = classContext.getTypeDataflow(method);
        }
        catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e1){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Coult not get Type dataflow",e1);
            return;
        }
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if (ins.getOpcode() == org.apache.bcel.Constants.INVOKEVIRTUAL) {
                org.apache.bcel.generic.INVOKEVIRTUAL iv = (org.apache.bcel.generic.INVOKEVIRTUAL) (ins) ;
                java.lang.String methodName = iv.getMethodName(cpg);
                java.lang.String methodSig = iv.getSignature(cpg);
                if (methodName.equals("wait") && (methodSig.equals("()V") || methodSig.equals("(J)V") || methodSig.equals("(JI)V")) || (methodName.equals("notify") || methodName.equals("notifyAll")) && methodSig.equals("()V")) {
                    try {
                        edu.umd.cs.findbugs.ba.type.TypeFrame frame = typeDataflow.getFactAtLocation(location);
                        if ( !frame.isValid()) continue;
                        org.apache.bcel.generic.Type type = frame.getInstance(ins,cpg);
                        if ( !(type instanceof org.apache.bcel.generic.ReferenceType)) {
                            continue;
                        }
                        edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptorFromSignature(type.getSignature());
                        if (classDescriptor.equals(classContext.getClassDescriptor())) continue;
                        if ( !classDescriptor.getClassName().startsWith("java/util/concurrent")) continue;
                        edu.umd.cs.findbugs.ba.XClass c = edu.umd.cs.findbugs.Lookup.getXClass(classDescriptor);
                        edu.umd.cs.findbugs.ba.XMethod m;
                        int priority = NORMAL_PRIORITY;
                        if (methodName.equals("wait")) {
                            m = c.findMethod("await","()V",false);
                            priority = HIGH_PRIORITY;
                        }
                        else if (methodName.equals("notify")) {
                            m = c.findMethod("signal","()V",false);
                            if (m == null) m = c.findMethod("countDown","()V",false);
                        }
                        else if (methodName.equals("notifyAll")) {
                            m = c.findMethod("signalAll","()V",false);
                            if (m == null) m = c.findMethod("countDown","()V",false);
                        }
                        else throw new java.lang.IllegalStateException("Unexpected methodName: " + methodName);
                        if (m != null && m.isPublic() && c.isPublic()) bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "JML_JSR166_CALLING_WAIT_RATHER_THAN_AWAIT", priority).addClassAndMethod(classContext.getJavaClass(),method).addCalledMethod(cpg,iv).addMethod(m).describe(edu.umd.cs.findbugs.MethodAnnotation.METHOD_ALTERNATIVE_TARGET).addType(classDescriptor).describe(edu.umd.cs.findbugs.TypeAnnotation.FOUND_ROLE).addSourceLine(classContext,method,location));
                    }
                    catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
                        edu.umd.cs.findbugs.ba.AnalysisContext.logError("Coult not get Type dataflow",e);
                        continue;
                    }
                }
            }
            if (ins.getOpcode() != org.apache.bcel.Constants.MONITORENTER) continue;
            org.apache.bcel.generic.Type type;
            try {
                edu.umd.cs.findbugs.ba.type.TypeFrame frame = typeDataflow.getFactAtLocation(location);
                if ( !frame.isValid()) continue;
                type = frame.getInstance(ins,cpg);
            }
            catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
                edu.umd.cs.findbugs.ba.AnalysisContext.logError("Coult not get Type dataflow",e);
                continue;
            }
            if ( !(type instanceof org.apache.bcel.generic.ReferenceType)) {
                continue;
            }
            boolean isSubtype = false;
            try {
                isSubtype = edu.umd.cs.findbugs.ba.Hierarchy.isSubtype((org.apache.bcel.generic.ReferenceType) (type) ,LOCK_TYPE);
            }
            catch (java.lang.ClassNotFoundException e){
                bugReporter.reportMissingClass(e);
            }
            java.lang.String sig = type.getSignature();
            boolean isUtilConcurrentSig = sig.startsWith(UTIL_CONCURRRENT_SIG_PREFIX);
            if (isSubtype) {
                bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "JLM_JSR166_LOCK_MONITORENTER", isUtilConcurrentSig ? HIGH_PRIORITY : NORMAL_PRIORITY).addClassAndMethod(classContext.getJavaClass(),method).addType(sig).addSourceForTopStackValue(classContext,method,location).addSourceLine(classContext,method,location));
            }
            else if (isUtilConcurrentSig) {
                int priority = "Ljava/util/concurrent/CopyOnWriteArrayList;".equals(sig) ? HIGH_PRIORITY : NORMAL_PRIORITY;
                bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "JLM_JSR166_UTILCONCURRENT_MONITORENTER", priority).addClassAndMethod(classContext.getJavaClass(),method).addType(sig).addSourceForTopStackValue(classContext,method,location).addSourceLine(classContext,method,location));
            }
        }
    }
// Something is deeply wrong if a non-reference type
// is used for a method invocation. But, that's
// really a
// verification problem.
// Something is deeply wrong if a non-reference type
// is used for a monitorenter. But, that's really a
// verification problem.
    public void report() {
    }
}
// vim:ts=4
