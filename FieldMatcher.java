/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author rafal@caltha.pl
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import java.io.IOException;
import edu.umd.cs.findbugs.BugAnnotation;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.FieldAnnotation;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class FieldMatcher extends edu.umd.cs.findbugs.filter.MemberMatcher implements edu.umd.cs.findbugs.filter.Matcher {
    public FieldMatcher(java.lang.String name) {
        super(name);
    }
    public FieldMatcher(java.lang.String name, java.lang.String type) {
        super(name,edu.umd.cs.findbugs.filter.SignatureUtil.createFieldSignature(type));
    }
    public java.lang.String toString() {
        return "Method(" + super.toString() + ")";
    }
    public boolean match(edu.umd.cs.findbugs.BugInstance bugInstance) {
        edu.umd.cs.findbugs.FieldAnnotation fieldAnnotation = null;
        if (role == null || role.equals("")) fieldAnnotation = bugInstance.getPrimaryField();
        else for (edu.umd.cs.findbugs.BugAnnotation a : bugInstance.getAnnotations())if (a instanceof edu.umd.cs.findbugs.FieldAnnotation && role.equals(a.getDescription())) {
            fieldAnnotation = (edu.umd.cs.findbugs.FieldAnnotation) (a) ;
            break;
        }
;
        if (fieldAnnotation == null) {
            return false;
        }
        if ( !name.match(fieldAnnotation.getFieldName())) {
            return false;
        }
        if (signature != null &&  !signature.match(fieldAnnotation.getFieldSignature())) return false;
        return true;
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean disabled) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributes = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("name",name.getSpec());
        if (signature != null) attributes.addOptionalAttribute("signature",signature.getSpec());
        if (disabled) attributes.addAttribute("disabled","true");
        xmlOutput.openCloseTag("Field",attributes);
    }
}
