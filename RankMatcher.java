/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Matcher to select BugInstances with a particular rank or higher.
 * 
 * @author William Pugh
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import java.io.IOException;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugRanker;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class RankMatcher extends java.lang.Object implements edu.umd.cs.findbugs.filter.Matcher {
    private int rank;
    public java.lang.String toString() {
        return "Rank(rank=" + rank + ")";
    }
/**
     * Constructor.
     * 
     * @param rankAsString
     *            the rank, as a String
     * @throws NumberFormatException
     *             if the rank cannot be parsed
     */
    public RankMatcher(java.lang.String rankAsString) {
        super();
        this.rank = java.lang.Integer.parseInt(rankAsString);
    }
    public int hashCode() {
        return rank;
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.filter.RankMatcher)) return false;
        edu.umd.cs.findbugs.filter.RankMatcher other = (edu.umd.cs.findbugs.filter.RankMatcher) (o) ;
        return rank == other.rank;
    }
    public boolean match(edu.umd.cs.findbugs.BugInstance bugInstance) {
        return edu.umd.cs.findbugs.BugRanker.findRank(bugInstance) >= rank;
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean disabled) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributes = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("value",java.lang.Integer.toString(rank));
        if (disabled) attributes.addAttribute("disabled","true");
        xmlOutput.openCloseTag("Rank",attributes);
    }
}
