/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * @author tuc
 */
package edu.umd.cs.findbugs.sourceViewer;
import edu.umd.cs.findbugs.sourceViewer.*;
import java.awt.Container;
import java.awt.Rectangle;
import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.PriorityQueue;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.StyledDocument;
import edu.umd.cs.findbugs.gui2.MainFrame;
public class NavigableTextPane extends javax.swing.JTextPane {
/**
     * return the height of the parent (which is presumably a JViewport). If
     * there is no parent, return this.getHeight().
     */
// entire pane height, may be huge
/**
     * scroll the specified line into view, with a margin of 'margin' pixels
     * above and below
     */
/** scroll the specified line into the middle third of the view */
/**
     * scroll the specified primary lines into view, along with as many of the
     * other lines as is convenient
     */
// give up
// better than nothing
// give up on this one
    public static class DistanceComparator extends java.lang.Object implements java.util.Comparator<java.lang.Integer>, java.io.Serializable {
        final private int origin;
        public DistanceComparator(int origin) {
            super();
            this.origin = origin;
        }
/*
         * Returns a negative integer, zero, or a positive integer as the first
         * argument is farther from, equadistant, or closer to (respectively)
         * the origin. This sounds backwards, but this way closer values get a
         * higher priority in the priority queue.
         */
        public int compare(java.lang.Integer a, java.lang.Integer b) {
            return java.lang.Math.abs(b - origin) - java.lang.Math.abs(a - origin);
        }
    }
    public NavigableTextPane() {
        super();
    }
    public NavigableTextPane(javax.swing.text.StyledDocument doc) {
        super(doc);
    }
    private int parentHeight() {
        java.awt.Container parent = this.getParent();
        if (parent != null) return parent.getHeight();
        return this.getHeight();
    }
    public int getLineOffset(int line) throws javax.swing.text.BadLocationException {
        return this.lineToOffset(line);
    }
    private int lineToOffset(int line) throws javax.swing.text.BadLocationException {
        javax.swing.text.Document d = this.getDocument();
        try {
            javax.swing.text.Element element = d.getDefaultRootElement().getElement(line - 1);
            if (element == null) throw new javax.swing.text.BadLocationException("line " + line + " does not exist",  -line);
            return element.getStartOffset();
        }
        catch (java.lang.ArrayIndexOutOfBoundsException aioobe){
            javax.swing.text.BadLocationException ble = new javax.swing.text.BadLocationException("line " + line + " does not exist",  -line);
            ble.initCause(aioobe);
            throw ble;
        }
    }
    private int offsetToY(int offset) throws javax.swing.text.BadLocationException {
        java.awt.Rectangle r = this.modelToView(offset);
        return r.y;
    }
    private int lineToY(int line) throws javax.swing.text.BadLocationException {
        return this.offsetToY(this.lineToOffset(line));
    }
    private void scrollYToVisibleImpl(int y, int margin) {
        final java.awt.Rectangle r = new java.awt.Rectangle(0, y - margin, 4, 2 * margin);
        javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
            public void run() {
                edu.umd.cs.findbugs.sourceViewer.NavigableTextPane.this.scrollRectToVisible(r);
            }
        });
    }
    private void scrollLineToVisibleImpl(int line, int margin) {
        try {
            int y = this.lineToY(line);
            this.scrollYToVisibleImpl(y,margin);
        }
        catch (javax.swing.text.BadLocationException ble){
            if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) ble.printStackTrace();
        }
    }
    public void scrollLineToVisible(int line, int margin) {
        int maxMargin = (this.parentHeight() - 20) / 2;
        if (margin > maxMargin) margin = java.lang.Math.max(0,maxMargin);
        this.scrollLineToVisibleImpl(line,margin);
    }
    public void scrollLineToVisible(int line) {
        int margin = this.parentHeight() / 3;
        this.scrollLineToVisibleImpl(line,margin);
    }
    public void scrollLinesToVisible(int startLine, int endLine, java.util.Collection<java.lang.Integer> otherLines) {
        int startY;
        int endY;
        try {
            startY = this.lineToY(startLine);
        }
        catch (javax.swing.text.BadLocationException ble){
            if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) ble.printStackTrace();
            return;
        }
        try {
            endY = this.lineToY(endLine);
        }
        catch (javax.swing.text.BadLocationException ble){
            endY = startY;
        }
        int max = this.parentHeight() - 0;
        if (endY - startY > max) {
            endY = startY + max;
        }
        else if (otherLines != null && otherLines.size() > 0) {
            int origin = startY + endY / 2;
            java.util.PriorityQueue<java.lang.Integer> pq = new java.util.PriorityQueue<java.lang.Integer>(otherLines.size(), new edu.umd.cs.findbugs.sourceViewer.NavigableTextPane.DistanceComparator(origin));
            for (int line : otherLines){
                int otherY;
                try {
                    otherY = this.lineToY(line);
                }
                catch (javax.swing.text.BadLocationException ble){
                    continue;
                }
                pq.add(otherY);
            }
;
            while ( !pq.isEmpty()) {
                int y = pq.remove();
                int lo = java.lang.Math.min(startY,y);
                int hi = java.lang.Math.max(endY,y);
                if (hi - lo > max) break;
                else {
                    startY = lo;
                    endY = hi;
                }
            }
        }
        if (endY - startY > max) {
            endY = startY + max;
        }
        this.scrollYToVisibleImpl((startY + endY) / 2,max / 2);
    }
}
