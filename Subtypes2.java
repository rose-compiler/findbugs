package edu.umd.cs.findbugs.ba.ch;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.ch.*;
/**
 * Class for performing class hierarchy queries. Does <em>not</em> require
 * JavaClass objects to be in memory. Instead, uses XClass objects.
 *
 * @author David Hovemeyer
 */
// SystemProperties.getBoolean("findbugs.subtypes2.superclass");
/**
     * Object to record the results of a supertype search.
     */
// We don't really know which class was missing.
// However, any missing classes will already have been reported.
/**
     * Constructor.
     */
/**
     * @return Returns the graph.
     */
/** A collection, a map, or some other container */
/**
     * Add an application class, and its transitive supertypes, to the
     * inheritance graph.
     *
     * @param appXClass
     *            application XClass to add to the inheritance graph
     */
/**
     * Add a class or interface, and its transitive supertypes, to the
     * inheritance graph.
     *
     * @param xclass
     *            XClass to add to the inheritance graph
     */
/**
     * Add an XClass and all of its supertypes to the InheritanceGraph.
     *
     * @param xclass
     *            an XClass
     * @return the ClassVertex representing the class in the InheritanceGraph
     */
// This class has already been processed.
// There is no need to add additional worklist nodes because
// java/lang/Object has no supertypes.
/**
     * Determine whether or not a given ReferenceType is a subtype of another.
     * Throws ClassNotFoundException if the question cannot be answered
     * definitively due to a missing class.
     *
     * @param type
     *            a ReferenceType
     * @param possibleSupertype
     *            another Reference type
     * @return true if <code>type</code> is a subtype of
     *         <code>possibleSupertype</code>, false if not
     * @throws ClassNotFoundException
     *             if a missing class prevents a definitive answer
     */
// Eliminate some easy cases
// Both types are ordinary object (non-array) types.
// Check superclass/interfaces
// We checked all of the possible class/interface supertypes,
// so if possibleSupertype is not an array type,
// then we can definitively say no
// Check array/array subtype relationship
// Must have same number of dimensions
// If dimensions differ, see if element types are compatible.
// type's base type must be a subtype of possibleSupertype's base
// type.
// Note that neither base type can be a non-ObjectType if we are to
// answer yes.
// OK, we've exhausted the possibilities now
/**
     * Determine whether or not a given ObjectType is a subtype of another.
     * Throws ClassNotFoundException if the question cannot be answered
     * definitively due to a missing class.
     *
     * @param type
     *            a ReferenceType
     * @param possibleSupertype
     *            another Reference type
     * @return true if <code>type</code> is a subtype of
     *         <code>possibleSupertype</code>, false if not
     * @throws ClassNotFoundException
     *             if a missing class prevents a definitive answer
     */
/**
     * Get the first common superclass of the given reference types. Note that
     * an interface type is never returned unless <code>a</code> and
     * <code>b</code> are the same type. Otherwise, we try to return as accurate
     * a type as possible. This method is used as the meet operator in
     * TypeDataflowAnalysis, and is intended to follow (more or less) the JVM
     * bytecode verifier semantics.
     *
     * <p>
     * This method should be used in preference to the
     * getFirstCommonSuperclass() method in {@link ReferenceType}.
     * </p>
     *
     * @param a
     *            a ReferenceType
     * @param b
     *            another ReferenceType
     * @return the first common superclass of <code>a</code> and <code>b</code>
     * @throws ClassNotFoundException
     */
// Easy case: same types
// Merging array types - kind of a pain.
// One of a and b is an array type, but not both.
// Common supertype is Object.
// Neither a nor b is an array type.
// Find first common supertypes of ObjectTypes.
/**
     * Get first common supertype of arrays with the same number of dimensions.
     *
     * @param aArrType
     *            an ArrayType
     * @param bArrType
     *            another ArrayType with the same number of dimensions
     * @return first common supertype
     * @throws ClassNotFoundException
     */
// E.g.: first common supertype of int[][] and WHATEVER[][] is
// Object[]
// E.g.: first common supertype type of int[] and WHATEVER[] is
// Object
// Base types are both ObjectTypes, and number of dimensions is
// same.
// We just need to find the first common supertype of base types
// and return a new ArrayType using that base type.
/**
     * Get the first common superclass of arrays with different numbers of
     * dimensions.
     *
     * @param aArrType
     *            an ArrayType
     * @param bArrType
     *            another ArrayType
     * @return ReferenceType representing first common superclass
     */
// One of the types was something like int[].
// The only possible common supertype is Object.
// Weird case: e.g.,
// - first common supertype of int[][] and char[][][] is
// Object[]
// because f.c.s. of int[] and char[][] is Object
// - first common supertype of int[][][] and char[][][][][] is
// Object[][]
// because f.c.s. of int[] and char[][][] is Object
// Both a and b have base types which are ObjectTypes.
// Since the arrays have different numbers of dimensions, the
// f.c.s. will have Object as its base type.
// E.g., f.c.s. of Cat[] and Dog[][] is Object[]
/**
     * Get the first common superclass of the given object types. Note that an
     * interface type is never returned unless <code>a</code> and <code>b</code>
     * are the same type. Otherwise, we try to return as accurate a type as
     * possible. This method is used as the meet operator in
     * TypeDataflowAnalysis, and is intended to follow (more or less) the JVM
     * bytecode verifier semantics.
     *
     * <p>
     * This method should be used in preference to the
     * getFirstCommonSuperclass() method in {@link ReferenceType}.
     * </p>
     *
     * @param a
     *            an ObjectType
     * @param b
     *            another ObjectType
     * @return the first common superclass of <code>a</code> and <code>b</code>
     * @throws ClassNotFoundException
     */
// Easy case
// Work backwards until the lists diverge.
// The last element common to both lists is the first
// common superclass.
// see if we can't do better
/**
     * Get list of all superclasses of class represented by given class vertex,
     * in order, including the class itself (which is trivially its own
     * superclass as far as "first common superclass" queries are concerned.)
     *
     * @param vertex
     *            a ClassVertex
     * @return list of all superclass vertices in order
     */
/**
     * Get known subtypes of given class. The set returned <em>DOES</em> include
     * the class itself.
     *
     * @param classDescriptor
     *            ClassDescriptor naming a class
     * @return Set of ClassDescriptors which are the known subtypes of the class
     * @throws ClassNotFoundException
     */
/**
     * Determine whether or not the given class has any known subtypes.
     *
     * @param classDescriptor
     *            ClassDescriptor naming a class
     * @return true if the class has subtypes, false if it has no subtypes
     * @throws ClassNotFoundException
     */
/**
     * Get known subtypes of given class.
     *
     * @param classDescriptor
     *            ClassDescriptor naming a class
     * @return Set of ClassDescriptors which are the known subtypes of the class
     * @throws ClassNotFoundException
     */
/**
     * Get the set of common subtypes of the two given classes.
     *
     * @param classDescriptor1
     *            a ClassDescriptor naming a class
     * @param classDescriptor2
     *            a ClassDescriptor naming another class
     * @return Set containing all common transitive subtypes of the two classes
     * @throws ClassNotFoundException
     */
/**
     * Get Collection of all XClass objects (resolved classes) seen so far.
     *
     * @return Collection of all XClass objects
     */
/**
     * An in-progress traversal of one path from a class or interface to
     * java.lang.Object.
     */
/**
     * Starting at the class or interface named by the given ClassDescriptor,
     * traverse the inheritance graph, exploring all paths from the class or
     * interface to java.lang.Object.
     *
     * @param start
     *            ClassDescriptor naming the class where the traversal should
     *            start
     * @param visitor
     *            an InheritanceGraphVisitor
     * @throws ClassNotFoundException
     *             if the start vertex cannot be resolved
     */
// Visitor doesn't want to continue on this path
// Unknown class - so, we don't know its immediate supertypes
// Advance to direct superclass
// Advance to directly-implemented interfaces
// The vertex should already have been added to the graph
// This can only happen when the inheritance graph has a cycle
// We reached java.lang.Object
/**
     * Compute set of known subtypes of class named by given ClassDescriptor.
     *
     * @param classDescriptor
     *            a ClassDescriptor
     * @throws ClassNotFoundException
     */
// Already added this class
// Add class to the result
// Add all known subtype vertices to the work list
// Already added this class
// Add class to the result
// Add all known subtype vertices to the work list
/**
     * Look up or compute the SupertypeQueryResults for class named by given
     * ClassDescriptor.
     *
     * @param classDescriptor
     *            a ClassDescriptor
     * @return SupertypeQueryResults for the class named by the ClassDescriptor
     * @throws ClassNotFoundException
     */
/**
     * Compute supertypes for class named by given ClassDescriptor.
     *
     * @param classDescriptor
     *            a ClassDescriptor
     * @return SupertypeQueryResults containing known supertypes of the class
     * @throws ClassNotFoundException
     *             if the class can't be found
     */
// throws
// ClassNotFoundException
// Try to fully resolve the class and its superclasses/superinterfaces.
// Create new empty SupertypeQueryResults.
// Add all known superclasses/superinterfaces.
// The ClassVertexes for all of them should be in the
// InheritanceGraph by now.
/**
     * Resolve a class named by given ClassDescriptor and return its resolved
     * ClassVertex.
     *
     * @param classDescriptor
     *            a ClassDescriptor
     * @return resolved ClassVertex representing the class in the
     *         InheritanceGraph
     * @throws ClassNotFoundException
     *             if the class named by the ClassDescriptor does not exist
     */
/**
     * @param classDescriptor
     * @return
     */
// We have never tried to resolve this ClassVertex before.
// Try to find the XClass for this class.
// Class we're trying to resolve doesn't exist.
// XXX: unfortunately, we don't know if the missing class is a
// class or interface
// Add the class and all its superclasses/superinterfaces to the
// inheritance graph.
// This will result in a resolved ClassVertex.
/**
     * Add supertype edges to the InheritanceGraph for given ClassVertex. If any
     * direct supertypes have not been processed, add them to the worklist.
     *
     * @param vertex
     *            a ClassVertex whose supertype edges need to be added
     * @param workList
     *            work list of ClassVertexes that need to have their supertype
     *            edges added
     */
// Direct superclass
// Directly implemented interfaces
/**
     * Add supertype edge to the InheritanceGraph.
     *
     * @param vertex
     *            source ClassVertex (subtype)
     * @param superclassDescriptor
     *            ClassDescriptor of a direct supertype
     * @param isInterfaceEdge
     *            true if supertype is (as far as we know) an interface
     * @param workList
     *            work list of ClassVertexes that need to have their supertype
     *            edges added (null if no further work will be generated)
     */
// Haven't encountered this class previously.
// Inheritance graph will be incomplete.
// Add a dummy node to inheritance graph and report missing
// class.
// Haven't seen this class before.
// We'll want to recursively process the superclass.
/**
     * Add a ClassVertex representing a missing class.
     *
     * @param missingClassDescriptor
     *            ClassDescriptor naming a missing class
     * @param isInterfaceEdge
     * @return the ClassVertex representing the missing class
     */
abstract interface package-info {
}
