/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Factory for producing ForwardTypeQualifierDataflow objects for various kinds
 * of type qualifiers.
 *
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.jsr305;
import edu.umd.cs.findbugs.ba.jsr305.*;
import java.util.Iterator;
import javax.annotation.meta.When;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.RETURN;
import org.apache.bcel.generic.ReturnInstruction;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.DepthFirstSearch;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.ba.vna.ValueNumberFrame;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class ForwardTypeQualifierDataflowFactory extends edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDataflowFactory {
/**
     * Constructor.
     *
     * @param methodDescriptor
     *            MethodDescriptor of method being analyzed
     */
    public ForwardTypeQualifierDataflowFactory(edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor) {
        super(methodDescriptor);
    }
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDataflowFactory#getDataflow
     * (edu.umd.cs.findbugs.ba.DepthFirstSearch, edu.umd.cs.findbugs.ba.XMethod,
     * edu.umd.cs.findbugs.ba.CFG,
     * edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow,
     * org.apache.bcel.generic.ConstantPoolGen,
     * edu.umd.cs.findbugs.classfile.IAnalysisCache,
     * edu.umd.cs.findbugs.classfile.MethodDescriptor)
     */
    protected edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflow getDataflow(edu.umd.cs.findbugs.ba.DepthFirstSearch dfs, edu.umd.cs.findbugs.ba.XMethod xmethod, edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue typeQualifierValue) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflowAnalysis analysis = new edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflowAnalysis(dfs, xmethod, cfg, vnaDataflow, cpg, typeQualifierValue);
        analysis.registerSourceSinkLocations();
        edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflow dataflow = new edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflow(cfg, analysis);
        dataflow.execute();
        if (edu.umd.cs.findbugs.ba.ClassContext.DUMP_DATAFLOW_ANALYSIS) {
            dataflow.dumpDataflow(analysis);
        }
        return dataflow;
    }
    protected void populateDatabase(edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflow dataflow, edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow, edu.umd.cs.findbugs.ba.XMethod xmethod, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue tqv) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        assert edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDatabase.USE_DATABASE;
        if (xmethod.getSignature().endsWith(")V")) {
            return;
        }
// If there is no effective type qualifier annotation
// on the method's return value, and we computed an
// "interesting" (ALWAYS or NEVER) value for the return
// value, add it to the database.
        edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation tqa = edu.umd.cs.findbugs.ba.jsr305.TypeQualifierApplications.getEffectiveTypeQualifierAnnotation(xmethod,tqv);
        if (tqa == null) {
// Find the meet of the flow values at all instructions
// which return a value.
            edu.umd.cs.findbugs.ba.jsr305.FlowValue effectiveFlowValue = null;
            edu.umd.cs.findbugs.ba.CFG cfg = dataflow.getCFG();
            java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator();
            while (i.hasNext()) {
                edu.umd.cs.findbugs.ba.Location loc = i.next();
                org.apache.bcel.generic.InstructionHandle handle = loc.getHandle();
                org.apache.bcel.generic.Instruction ins = handle.getInstruction();
                if (ins instanceof org.apache.bcel.generic.ReturnInstruction &&  !(ins instanceof org.apache.bcel.generic.RETURN)) {
                    edu.umd.cs.findbugs.ba.vna.ValueNumberFrame vnaFrame = vnaDataflow.getFactAtLocation(loc);
                    edu.umd.cs.findbugs.ba.vna.ValueNumber topVN = vnaFrame.getTopValue();
                    edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValueSet flowSet = dataflow.getFactAtLocation(loc);
                    edu.umd.cs.findbugs.ba.jsr305.FlowValue topFlowValue = flowSet.getValue(topVN);
                    if (topFlowValue != null) {
                        if (edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDatabase.DEBUG) {
                            java.lang.System.out.println("at pc " + handle.getPosition() + " of " + xmethod + ", return value for " + tqv + " is " + topFlowValue);
                        }
                        if (effectiveFlowValue == null) {
                            effectiveFlowValue = topFlowValue;
                        }
                        else {
                            effectiveFlowValue = edu.umd.cs.findbugs.ba.jsr305.FlowValue.meet(effectiveFlowValue,topFlowValue);
                        }
                    }
                }
            }
            if (effectiveFlowValue == edu.umd.cs.findbugs.ba.jsr305.FlowValue.ALWAYS || effectiveFlowValue == edu.umd.cs.findbugs.ba.jsr305.FlowValue.NEVER) {
                edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDatabase tqdb = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getDatabase(edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDatabase.class);
                if (edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDatabase.DEBUG) {
                    java.lang.System.out.println("inferring return value for " + xmethod + " of" + tqv + " : " + effectiveFlowValue);
                }
                tqa = edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation.getValue(tqv,effectiveFlowValue == edu.umd.cs.findbugs.ba.jsr305.FlowValue.ALWAYS ? javax.annotation.meta.When.ALWAYS : javax.annotation.meta.When.NEVER);
                tqdb.setReturnValue(xmethod.getMethodDescriptor(),tqv,tqa);
            }
        }
    }
}
