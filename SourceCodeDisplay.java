/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Color;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.annotation.Nonnull;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import edu.umd.cs.findbugs.BugAnnotation;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.ba.SourceFile;
import edu.umd.cs.findbugs.charsets.SourceCharset;
import edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument;
final public class SourceCodeDisplay extends java.lang.Object implements java.lang.Runnable {
// for find
// e.printStackTrace();
// e.printStackTrace();
// Display myBug and mySourceLine
// e.printStackTrace();
    final private class DisplayBug extends java.lang.Object implements java.lang.Runnable {
        final private edu.umd.cs.findbugs.SourceLineAnnotation mySourceLine;
        final private edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument src;
        final private edu.umd.cs.findbugs.BugInstance myBug;
        public DisplayBug(edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument src, edu.umd.cs.findbugs.BugInstance myBug, edu.umd.cs.findbugs.SourceLineAnnotation mySourceLine) {
            super();
            this.mySourceLine = mySourceLine;
            this.src = src;
            this.myBug = myBug;
        }
        public void run() {
            frame.getSourceCodeTextPane().setEditorKit(src.getEditorKit());
            javax.swing.text.StyledDocument document = src.getDocument();
            frame.getSourceCodeTextPane().setDocument(document);
            java.lang.String sourceFile = mySourceLine.getSourceFile();
            if (sourceFile == null || sourceFile.equals("<Unknown>")) {
                sourceFile = mySourceLine.getSimpleClassName();
            }
            int startLine = mySourceLine.getStartLine();
            int endLine = mySourceLine.getEndLine();
            frame.setSourceTab(sourceFile + " in " + mySourceLine.getPackageName(),myBug);
            int originLine = (startLine + endLine) / 2;
            java.util.LinkedList<java.lang.Integer> otherLines = new java.util.LinkedList<java.lang.Integer>();
            for (java.util.Iterator<edu.umd.cs.findbugs.BugAnnotation> i = myBug.annotationIterator(); i.hasNext(); ) {
                edu.umd.cs.findbugs.BugAnnotation annotation = i.next();
                if (annotation instanceof edu.umd.cs.findbugs.SourceLineAnnotation) {
                    edu.umd.cs.findbugs.SourceLineAnnotation sourceAnnotation = (edu.umd.cs.findbugs.SourceLineAnnotation) (annotation) ;
                    if (sourceAnnotation != mySourceLine) {
                        int otherLine = sourceAnnotation.getStartLine();
                        if (otherLine > originLine) otherLine = sourceAnnotation.getEndLine();
                        otherLines.add(otherLine);
                    }
                }
            }
// show(frame.getSourceCodeTextPane(), document,
// thisSource);
// show(frame.getSourceCodeTextPane(),
// document, sourceAnnotation);
            if (startLine >= 0 && endLine >= 0) frame.getSourceCodeTextPane().scrollLinesToVisible(startLine,endLine,otherLines);
        }
    }
    static class DisplayMe extends java.lang.Object {
        public DisplayMe(edu.umd.cs.findbugs.BugInstance bug, edu.umd.cs.findbugs.SourceLineAnnotation source) {
            super();
            this.bug = bug;
            this.source = source;
        }
        final edu.umd.cs.findbugs.BugInstance bug;
        final edu.umd.cs.findbugs.SourceLineAnnotation source;
    }
    final edu.umd.cs.findbugs.gui2.MainFrame frame;
    final private static java.awt.Color MAIN_HIGHLIGHT = new java.awt.Color(1f, 1f, 0.5f);
    final private static java.awt.Color MAIN_HIGHLIGHT_MORE = MAIN_HIGHLIGHT.brighter();
    final private static java.awt.Color ALTERNATIVE_HIGHLIGHT = new java.awt.Color(0.86f, 0.90f, 1.0f);
    final private static java.awt.Color FOUND_HIGHLIGHT = new java.awt.Color(0.75f, 0.75f, 1f);
    final static javax.swing.text.Document SOURCE_NOT_RELEVANT = new javax.swing.text.DefaultStyledDocument();
    public edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument myDocument;
    private int currentChar =  -1;
    final private java.util.Map<java.lang.String, java.lang.ref.SoftReference<edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument>> map = new java.util.HashMap<java.lang.String, java.lang.ref.SoftReference<edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument>>();
    public SourceCodeDisplay(edu.umd.cs.findbugs.gui2.MainFrame frame) {
        super();
        this.frame = frame;
        java.lang.Thread t = new java.lang.Thread(this, "Source code display thread");
        t.setDaemon(true);
        t.start();
    }
    final java.util.concurrent.BlockingQueue<edu.umd.cs.findbugs.gui2.SourceCodeDisplay.DisplayMe> queue = new java.util.concurrent.LinkedBlockingQueue<edu.umd.cs.findbugs.gui2.SourceCodeDisplay.DisplayMe>();
    public void displaySource(edu.umd.cs.findbugs.BugInstance bug, edu.umd.cs.findbugs.SourceLineAnnotation source) {
        queue.add(new edu.umd.cs.findbugs.gui2.SourceCodeDisplay.DisplayMe(bug, source));
    }
    public void clearCache() {
        map.clear();
    }
    private edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument getDocument(edu.umd.cs.findbugs.SourceLineAnnotation source) {
        try {
            edu.umd.cs.findbugs.ba.SourceFile sourceFile = frame.getProject().getSourceFinder().findSourceFile(source);
            java.lang.String fullFileName = sourceFile.getFullFileName();
            java.lang.ref.SoftReference<edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument> resultReference = map.get(fullFileName);
            edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument result = null;
            if (resultReference != null) result = resultReference.get();
            if (result != null) return result;
            try {
                java.io.InputStream in = sourceFile.getInputStream();
                result = new edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument(source.getClassName(), edu.umd.cs.findbugs.charsets.SourceCharset.bufferedReader(in), sourceFile);
            }
            catch (java.lang.Exception e){
                result = edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument.UNKNOWNSOURCE;
                edu.umd.cs.findbugs.gui2.Debug.println(e);
            }
            map.put(fullFileName,new java.lang.ref.SoftReference<edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument>(result));
            return result;
        }
        catch (java.lang.Exception e){
            edu.umd.cs.findbugs.gui2.Debug.println(e);
            return edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument.UNKNOWNSOURCE;
        }
    }
    public void run() {
        while (true) {
            edu.umd.cs.findbugs.gui2.SourceCodeDisplay.DisplayMe display;
            try {
                display = queue.take();
            }
            catch (java.lang.InterruptedException e1){
                assert false;
                edu.umd.cs.findbugs.gui2.Debug.println(e1);
                continue;
            }
            edu.umd.cs.findbugs.BugInstance myBug = display.bug;
            edu.umd.cs.findbugs.SourceLineAnnotation mySourceLine = display.source;
            if (myBug == null || mySourceLine == null) {
                frame.clearSourcePane();
                continue;
            }
            try {
                edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument src = this.getDocument(mySourceLine);
                this.myDocument = src;
                src.getHighlightInformation().clear();
                java.lang.String primaryKind = mySourceLine.getDescription();
                for (java.util.Iterator<edu.umd.cs.findbugs.BugAnnotation> i = myBug.annotationIterator(); i.hasNext(); ) {
                    edu.umd.cs.findbugs.BugAnnotation annotation = i.next();
                    if (annotation instanceof edu.umd.cs.findbugs.SourceLineAnnotation) {
                        edu.umd.cs.findbugs.SourceLineAnnotation sourceAnnotation = (edu.umd.cs.findbugs.SourceLineAnnotation) (annotation) ;
                        if (sourceAnnotation == mySourceLine) continue;
                        if (sourceAnnotation.getDescription().equals(primaryKind)) this.highlight(src,sourceAnnotation,MAIN_HIGHLIGHT_MORE);
                        else this.highlight(src,sourceAnnotation,ALTERNATIVE_HIGHLIGHT);
                    }
                }
                this.highlight(src,mySourceLine,MAIN_HIGHLIGHT);
                javax.swing.SwingUtilities.invokeLater(new edu.umd.cs.findbugs.gui2.SourceCodeDisplay.DisplayBug(src, myBug, mySourceLine));
            }
            catch (java.lang.Exception e){
                edu.umd.cs.findbugs.gui2.Debug.println(e);
            }
        }
    }
/**
     * @param src
     * @param sourceAnnotation
     */
    private void highlight(edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument src, edu.umd.cs.findbugs.SourceLineAnnotation sourceAnnotation, java.awt.Color color) {
        int startLine = sourceAnnotation.getStartLine();
        if (startLine ==  -1) return;
        java.lang.String sourceFile = sourceAnnotation.getSourcePath();
        java.lang.String sourceFile2 = src.getSourceFile().getFullFileName();
        if ( !java.io.File.separator.equals(java.lang.String.valueOf(edu.umd.cs.findbugs.SourceLineAnnotation.CANONICAL_PACKAGE_SEPARATOR))) {
            sourceFile2 = sourceFile2.replace(java.io.File.separatorChar,edu.umd.cs.findbugs.SourceLineAnnotation.CANONICAL_PACKAGE_SEPARATOR);
        }
        if ( !sourceFile2.endsWith(sourceFile)) return;
        src.getHighlightInformation().setHighlight(startLine,sourceAnnotation.getEndLine(),color);
    }
    public void foundItem(int lineNum) {
        myDocument.getHighlightInformation().updateFoundLineNum(lineNum);
        myDocument.getHighlightInformation().setHighlight(lineNum,FOUND_HIGHLIGHT);
        frame.getSourceCodeTextPane().scrollLineToVisible(lineNum);
        frame.getSourceCodeTextPane().updateUI();
    }
    private int search(edu.umd.cs.findbugs.sourceViewer.JavaSourceDocument document, java.lang.String target, int start, java.lang.Boolean backwards) {
        if (document == null) return  -1;
        java.lang.String docContent = null;
        try {
            javax.swing.text.StyledDocument document2 = document.getDocument();
            if (document2 == null) return  -1;
            docContent = document2.getText(0,document2.getLength());
        }
        catch (javax.swing.text.BadLocationException ble){
            java.lang.System.out.println("Bad location exception");
        }
        catch (java.lang.NullPointerException npe){
            return  -1;
        }
        if (docContent == null) return  -1;
        int targetLen = target.length();
        int sourceLen = docContent.length();
        if (targetLen > sourceLen) return  -1;
        else if (backwards) {
            for (int i = start; i >= 0; i--) if (docContent.substring(i,i + targetLen).equals(target)) return i;
            for (int i = (sourceLen - targetLen); i > start; i--) if (docContent.substring(i,i + targetLen).equals(target)) return i;
            return  -1;
        }
        else {
            for (int i = start; i <= (sourceLen - targetLen); i++) if (docContent.substring(i,i + targetLen).equals(target)) return i;
            for (int i = 0; i < start; i++) if (docContent.substring(i,i + targetLen).equals(target)) return i;
            return  -1;
        }
    }
    private int charToLineNum(int charNum) {
        if (charNum ==  -1) return  -1;
        try {
            for (int i = 1; true; i++) {
                if (frame.getSourceCodeTextPane().getLineOffset(i) > charNum) return i - 1;
                else if (frame.getSourceCodeTextPane().getLineOffset(i) ==  -1) return  -1;
            }
        }
        catch (javax.swing.text.BadLocationException ble){
            return  -1;
        }
    }
    public int find(java.lang.String target) {
        currentChar = this.search(myDocument,target,0,false);
// System.out.println(currentChar);
// System.out.println(charToLineNum(currentChar));
        return this.charToLineNum(currentChar);
    }
    public int findNext(java.lang.String target) {
        currentChar = this.search(myDocument,target,currentChar + 1,false);
// System.out.println(currentChar);
// System.out.println(charToLineNum(currentChar));
        return this.charToLineNum(currentChar);
    }
    public int findPrevious(java.lang.String target) {
        currentChar = this.search(myDocument,target,currentChar - 1,true);
// System.out.println(currentChar);
// System.out.println(charToLineNum(currentChar));
        return this.charToLineNum(currentChar);
    }
    public void showLine(int line) {
        frame.getSourceCodeTextPane().scrollLineToVisible(line);
    }
}
