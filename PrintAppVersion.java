/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Print the AppVersion information from a BugCollection.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.SortedBugCollection;
public class PrintAppVersion extends java.lang.Object {
    public PrintAppVersion() {
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        if (args.length != 1) {
            java.lang.System.out.println("Usage: " + edu.umd.cs.findbugs.workflow.PrintAppVersion.class.getName() + " <bug collection>");
            java.lang.System.exit(1);
        }
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.SortedBugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        bugCollection.readXML(args[0]);
        java.lang.System.out.println(bugCollection.getCurrentAppVersion());
    }
}
