package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Boolean analysis properties for use in the AnalysisContext. These can be used
 * to enable or disable various analysis features in the bytecode analysis
 * framework.
 * 
 * @author David Hovemeyer
 */
/**
     * Determine (1) what exceptions can be thrown on exception edges, (2) which
     * catch blocks are reachable, and (3) which exception edges carry only
     * "implicit" runtime exceptions.
     */
/**
     * A boolean flag which if set means that analyses should try to conserve
     * space at the expense of precision.
     */
/**
     * If true, model the effect of instanceof checks in type analysis.
     */
/**
     * Skip generating CFG's and methodGen's for huge methods
     */
/**
     * Perform interative opcode stack analysis: always enabled.
     */
/**
     * In the null pointer analysis, track null values that are guaranteed to be
     * dereferenced on some (non-implicit-exception) path.
     */
/**
     * In the null pointer analysis, track value numbers that are known to be
     * null. This allows us to not lose track of null values that are not
     * currently in the stack frame but might be in a heap location where the
     * value is recoverable by redundant load elimination or forward
     * substitution.
     */
/**
     * Merge similar warnings. If we are tracking warnings across versions, it
     * is useful to merge all similar issues together. Otherwise, when we
     * compare the warnings in two different versions, we will not be able to
     * match them up correctly.
     */
/**
     * Number of boolean analysis properties reserved for the bytecode analysis
     * framework. Clients of the framework may use property values &gt;= this
     * value.
     */
// vim:ts=4
abstract interface package-info {
}
