/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An XMethod represents symbolic information about a particular method.
 * 
 * <p>
 * If the resolved() method returns true, then any information queried from this
 * object can be assumed to be accurate. If the resolved() method returns false,
 * then FindBugs can't find the method and any information other than
 * name/signature/etc. cannot be trusted.
 * </p>
 * 
 * @author David Hovemeyer
 * @author Bill Pugh
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.Collection;
import javax.annotation.CheckForNull;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
import edu.umd.cs.findbugs.classfile.analysis.AnnotatedObject;
import edu.umd.cs.findbugs.classfile.analysis.AnnotationValue;
abstract public interface XMethod extends edu.umd.cs.findbugs.ba.ClassMember, edu.umd.cs.findbugs.classfile.analysis.AnnotatedObject, edu.umd.cs.findbugs.ba.ComparableMethod {
    abstract public boolean isNative();
    abstract public boolean isAbstract();
    abstract public boolean isSynchronized();
    abstract public int getNumParams();
/**
     * @return the MethodDescriptor identifying this object
     */
    abstract public edu.umd.cs.findbugs.classfile.MethodDescriptor getMethodDescriptor();
/**
     * 
     * @return the exceptions this method is declared to throw
     */
    abstract public java.lang.String[] getThrownExceptions();
/**
     * @return does this method unconditionally throw an exception?
     */
    abstract public boolean isUnconditionalThrower();
/**
     * @return does the method directly make use of
     *         concurrency/threads/synchronization?
     */
    abstract public boolean usesConcurrency();
/**
     * @return is the method a synthetic stub method?
     */
    abstract public boolean isStub();
/** Is this an identity method */
    abstract public boolean isIdentity();
/**
     * If nonnull, then this method is a synthetic method that overrides a
     * method in a superclass. This method simply forwards the call to the
     * method it bridges to, which is a method with an identical name and set of
     * arguments, but a different return value.
     * 
     * @return
     */
    abstract public edu.umd.cs.findbugs.ba.XMethod bridgeTo();
    abstract public edu.umd.cs.findbugs.ba.XMethod bridgeFrom();
    abstract public edu.umd.cs.findbugs.classfile.MethodDescriptor getAccessMethodForMethod();
    abstract public edu.umd.cs.findbugs.ba.XMethod resolveAccessMethodForMethod();
/**
     * @return does this method unconditionally throw an
     *         UnsupportedOperationException?
     */
    abstract public boolean isUnsupported();
/**
     * @return is this a var args method?
     */
    abstract public boolean isVarArgs();
/**
     * @return true if method's return type is a reference type, false otherwise
     */
    abstract public boolean isReturnTypeReferenceType();
/**
     * Get ClassDescriptors (annotation classes) of annotations applied directly
     * to this method's parameters.
     * 
     * @param param
     *            parameter number (0 for first parameter)
     * @return ClassDescriptors of annotations applied directly to this method's
     *         parameters
     */
    abstract public java.util.Collection<edu.umd.cs.findbugs.classfile.ClassDescriptor> getParameterAnnotationDescriptors(int param);
/**
     * Get the AnnotationValue of annotation applied directly to given
     * parameter.
     * 
     * @param param
     *            parameter number (0 for first parameter)
     * @param desc
     *            ClassDescriptor of the annotation class
     * @return AnnotationValue annotating the parameter, or null if parameter is
     *         not annotated with this kind of annotation
     */
    abstract public edu.umd.cs.findbugs.classfile.analysis.AnnotationValue getParameterAnnotation(int param, edu.umd.cs.findbugs.classfile.ClassDescriptor desc);
/**
     * Get collection of all AnnotationValues applied directly to given
     * parameter.
     * 
     * @param param
     *            parameter number (0 for first parameter)
     * @return Collection of all AnnotationValues applied directly to given
     *         parameter
     */
    abstract public java.util.Collection<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> getParameterAnnotations(int param);
/**
     * Get ClassDescriptors (annotation classes) of annotations applied directly
     * to this method.
     * 
     * @return ClassDescriptors of annotations applied directly to this method
     */
    abstract public java.util.Collection<edu.umd.cs.findbugs.classfile.ClassDescriptor> getAnnotationDescriptors();
/**
     * Get the AnnotationValue of annotation applied directly to the method.
     * 
     * @param desc
     *            ClassDescriptor of the annotation class
     * @return AnnotationValue annotating the method, or null if method is not
     *         annotated with this kind of annotation
     */
    abstract public edu.umd.cs.findbugs.classfile.analysis.AnnotationValue getAnnotation(edu.umd.cs.findbugs.classfile.ClassDescriptor desc);
/**
     * Get collection of all AnnotationValues applied directly to the method.
     * 
     * @return Collection of all AnnotationValues applied directly to the method
     */
    abstract public java.util.Collection<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> getAnnotations();
/**
     * Destructively add a parameter annotation.
     * 
     * @param param
     *            parameter (0 == first parameter)
     * @param annotationValue
     *            an AnnotationValue representing a parameter annotation
     */
    abstract public void addParameterAnnotation(int param, edu.umd.cs.findbugs.classfile.analysis.AnnotationValue annotationValue);
/** Is the variable synthetic? */
    abstract public boolean isVariableSynthetic(int param);
/**
     * Destructively add an annotation. We do this for "built-in" annotations
     * that might not be directly evident in the code. It's not a great idea in
     * general, but we can get away with it as long as it's done early enough
     * (i.e., before anyone asks what annotations this method has.)
     * 
     * @param annotationValue
     *            an AnnotationValue representing a method annotation
     */
    abstract public void addAnnotation(edu.umd.cs.findbugs.classfile.analysis.AnnotationValue annotationValue);
}
