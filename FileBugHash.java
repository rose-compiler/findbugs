/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 William Pugh
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * For each source file that has reported bugs, compute a hash of all the issues
 * reported for that file. These hashes use line numbers, so a change that only
 * changes the line number of an issue will cause the hash to be different.
 * 
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.CheckForNull;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.PackageStats;
import edu.umd.cs.findbugs.PackageStats.ClassStats;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.util.Util;
public class FileBugHash extends java.lang.Object {
    java.util.Map<java.lang.String, java.lang.StringBuilder> hashes = new java.util.LinkedHashMap<java.lang.String, java.lang.StringBuilder>();
    java.util.Map<java.lang.String, java.lang.Integer> counts = new java.util.HashMap<java.lang.String, java.lang.Integer>();
    java.util.Map<java.lang.String, java.lang.Integer> sizes = new java.util.HashMap<java.lang.String, java.lang.Integer>();
    java.security.MessageDigest digest = edu.umd.cs.findbugs.util.Util.getMD5Digest();
    public FileBugHash(edu.umd.cs.findbugs.BugCollection bugs) {
        super();
        for (edu.umd.cs.findbugs.PackageStats pStat : bugs.getProjectStats().getPackageStats())for (edu.umd.cs.findbugs.PackageStats.ClassStats cStat : pStat.getSortedClassStats()){
            java.lang.String path = cStat.getName();
            if (path.indexOf('\u002e') ==  -1) path = cStat.getSourceFile();
            else path = path.substring(0,path.lastIndexOf('\u002e') + 1).replace('\u002e','\u002f') + cStat.getSourceFile();
            counts.put(path,0);
            java.lang.Integer size = sizes.get(path);
            if (size == null) size = 0;
            sizes.put(path,size + cStat.size());
        }
;
;
        for (edu.umd.cs.findbugs.BugInstance bug : bugs.getCollection()){
            edu.umd.cs.findbugs.SourceLineAnnotation source = bug.getPrimarySourceLineAnnotation();
            java.lang.String packagePath = source.getPackageName().replace('\u002e','\u002f');
            java.lang.String key;
            if (packagePath.length() == 0) key = source.getSourceFile();
            else key = packagePath + "/" + source.getSourceFile();
            java.lang.StringBuilder buf = hashes.get(key);
            if (buf == null) {
                buf = new java.lang.StringBuilder();
                hashes.put(key,buf);
            }
            buf.append(bug.getInstanceKey()).append("-").append(source.getStartLine()).append(".").append(source.getStartBytecode()).append(" ");
            java.lang.Integer count = counts.get(key);
            if (count == null) counts.put(key,1);
            else counts.put(key,1 + count);
        }
;
    }
    public java.util.Collection<java.lang.String> getSourceFiles() {
        return counts.keySet();
    }
    public java.lang.String getHash(java.lang.String sourceFile) {
        java.lang.StringBuilder rawHash = hashes.get(sourceFile);
        if (rawHash == null || digest == null) return null;
        byte[] data = digest.digest(edu.umd.cs.findbugs.charsets.UTF8.getBytes(rawHash.toString()));
        java.lang.String tmp = new java.math.BigInteger(1, data).toString(16);
        if (tmp.length() < 32) tmp = "000000000000000000000000000000000".substring(0,32 - tmp.length()) + tmp;
        return tmp;
    }
    public int getBugCount(java.lang.String sourceFile) {
        java.lang.Integer count = counts.get(sourceFile);
        if (count == null) return 0;
        return count;
    }
    public int getSize(java.lang.String sourceFile) {
        java.lang.Integer size = sizes.get(sourceFile);
        if (size == null) return 0;
        return size;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        if (args.length > 1 || (args.length > 0 && "-help".equals(args[0]))) {
            java.lang.System.err.println("Usage: " + edu.umd.cs.findbugs.workflow.FileBugHash.class.getName() + " [<infile>]");
            java.lang.System.exit(1);
        }
        edu.umd.cs.findbugs.BugCollection origCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        int argCount = 0;
        if (argCount == args.length) origCollection.readXML(java.lang.System.in);
        else origCollection.readXML(args[argCount]);
        edu.umd.cs.findbugs.workflow.FileBugHash result = compute(origCollection);
        for (java.lang.String sourceFile : result.getSourceFiles()){
            java.lang.System.out.println(result.getHash(sourceFile) + "	" + sourceFile);
        }
;
    }
/**
     * @param origCollection
     * @return
     */
    public static edu.umd.cs.findbugs.workflow.FileBugHash compute(edu.umd.cs.findbugs.BugCollection origCollection) {
        return new edu.umd.cs.findbugs.workflow.FileBugHash(origCollection);
    }
}
