/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author Alex Mont
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.util.Comparator;
public class SortableStringComparator extends java.lang.Object implements java.util.Comparator<java.lang.String> {
    final edu.umd.cs.findbugs.gui2.Sortables mySortable;
    public SortableStringComparator(edu.umd.cs.findbugs.gui2.Sortables theSortable) {
        super();
        mySortable = theSortable;
    }
    public int compare(java.lang.String one, java.lang.String two) {
        return mySortable.compare(one,two);
    }
}
