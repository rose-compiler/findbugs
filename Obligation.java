/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An obligation that must be cleaned up by error handling code. Examples
 * include open streams and database connections.
 * 
 * <p>
 * See Weimer and Necula, <a href="http://doi.acm.org/10.1145/1028976.1029011"
 * >Finding and preventing run-time error handling mistakes</a>, OOPSLA 2004.
 * </p>
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.obl;
import edu.umd.cs.findbugs.ba.obl.*;
import org.apache.bcel.generic.ObjectType;
import edu.umd.cs.findbugs.ba.ObjectTypeFactory;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
public class Obligation extends java.lang.Object {
    final private java.lang.String className;
    final private org.apache.bcel.generic.ObjectType type;
    final private int id;
    private boolean userObligationType;
    public Obligation(java.lang.String className, int id) {
        super();
        this.className = className;
        this.type = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance(className);
        this.id = id;
    }
    public java.lang.String getClassName() {
        return className;
    }
    public org.apache.bcel.generic.ObjectType getType() {
        return type;
    }
    public int getId() {
        return id;
    }
    public boolean isUserObligationType() {
        return userObligationType;
    }
    public void setUserObligationType(boolean userObligationType) {
        this.userObligationType = userObligationType;
    }
    public java.lang.String toString() {
// Make dataflow output more compact by dropping package
        int lastDot = className.lastIndexOf('\u002e');
        return lastDot >= 0 ? className.substring(lastDot + 1) : className;
    }
}
// vim:ts=4
