/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Front-end for LockDataflow that can avoid doing unnecessary work (e.g.,
 * actually performing the lock dataflow) if the method analyzed does not
 * contain explicit monitorenter/monitorexit instructions.
 * 
 * <p>
 * Note that because LockSets use value numbers, ValueNumberAnalysis must be
 * performed for all methods that are synchronized or contain explicit
 * monitorenter/monitorexit instructions.
 * </p>
 * 
 * @see LockSet
 * @see LockDataflow
 * @see LockAnalysis
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.BitSet;
import java.util.HashMap;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class LockChecker extends java.lang.Object {
    private edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor;
    private org.apache.bcel.classfile.Method method;
    private edu.umd.cs.findbugs.ba.LockDataflow lockDataflow;
    private edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow;
    private java.util.HashMap<edu.umd.cs.findbugs.ba.Location, edu.umd.cs.findbugs.ba.LockSet> cache;
/**
     * Constructor.
     */
    public LockChecker(edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor) {
        super();
        this.cache = new java.util.HashMap<edu.umd.cs.findbugs.ba.Location, edu.umd.cs.findbugs.ba.LockSet>();
        this.methodDescriptor = methodDescriptor;
    }
/**
     * Execute dataflow analyses (only if required).
     * 
     * @throws CheckedAnalysisException
     */
    public void execute() throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        method = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getMethodAnalysis(org.apache.bcel.classfile.Method.class,methodDescriptor);
        edu.umd.cs.findbugs.ba.ClassContext classContext = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getClassAnalysis(edu.umd.cs.findbugs.ba.ClassContext.class,methodDescriptor.getClassDescriptor());
        java.util.BitSet bytecodeSet = classContext.getBytecodeSet(method);
        if (bytecodeSet == null) return;
        if (bytecodeSet.get(org.apache.bcel.Constants.MONITORENTER) || bytecodeSet.get(org.apache.bcel.Constants.MONITOREXIT)) {
            this.lockDataflow = classContext.getLockDataflow(method);
        }
        else if (method.isSynchronized()) {
            this.vnaDataflow = classContext.getValueNumberDataflow(method);
        }
    }
/**
     * Get LockSet at given Location.
     * 
     * @param location
     *            the Location
     * @return the LockSet at that Location
     * @throws DataflowAnalysisException
     */
    public edu.umd.cs.findbugs.ba.LockSet getFactAtLocation(edu.umd.cs.findbugs.ba.Location location) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        if (lockDataflow != null) return lockDataflow.getFactAtLocation(location);
        else {
            edu.umd.cs.findbugs.ba.LockSet lockSet = cache.get(location);
            if (lockSet == null) {
                lockSet = new edu.umd.cs.findbugs.ba.LockSet();
                lockSet.setDefaultLockCount(0);
                if (method.isSynchronized() &&  !method.isStatic()) {
// LockSet contains just the "this" reference
                    edu.umd.cs.findbugs.ba.vna.ValueNumber instance = vnaDataflow.getAnalysis().getThisValue();
                    lockSet.setLockCount(instance.getNumber(),1);
                }
                else {
                }
                cache.put(location,lockSet);
            }
            return lockSet;
        }
    }
}
