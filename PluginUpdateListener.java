package edu.umd.cs.findbugs.updates;
import edu.umd.cs.findbugs.updates.*;
import java.util.Collection;
abstract public interface PluginUpdateListener {
    abstract void pluginUpdateCheckComplete(java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates, boolean force);
}
