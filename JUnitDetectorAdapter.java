/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A special Detector2 class designed to run some JUnit test code. Only used by
 * FindBugsTestCase.
 * 
 * @author David Hovemeyer
 * @see FindBugsTestCase
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
public class JUnitDetectorAdapter extends java.lang.Object implements edu.umd.cs.findbugs.Detector2 {
    private java.lang.Throwable throwable;
    private boolean testExecuted;
    private static java.lang.InheritableThreadLocal<edu.umd.cs.findbugs.JUnitDetectorAdapter> instance = new java.lang.InheritableThreadLocal<edu.umd.cs.findbugs.JUnitDetectorAdapter>();
    private static java.lang.InheritableThreadLocal<edu.umd.cs.findbugs.RunnableWithExceptions> runnableInstance = new java.lang.InheritableThreadLocal<edu.umd.cs.findbugs.RunnableWithExceptions>();
    public JUnitDetectorAdapter(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        instance.set(this);
    }
    public static edu.umd.cs.findbugs.JUnitDetectorAdapter instance() {
        return instance.get();
    }
/**
     * @param runnable
     *            The runnable to set.
     */
    public static void setRunnable(edu.umd.cs.findbugs.RunnableWithExceptions runnable) {
        runnableInstance.set(runnable);
    }
    public void finishTest() throws java.lang.Exception {
        if (throwable instanceof java.lang.Exception) throw (java.lang.Exception) (throwable) ;
        if (throwable instanceof java.lang.Error) throw (java.lang.Error) (throwable) ;
        if (throwable != null) throw new java.lang.Error(throwable);
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.Detector2#finishPass()
     */
    public void finishPass() {
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.Detector2#getDetectorClassName()
     */
    public java.lang.String getDetectorClassName() {
        return this.getClass().getName();
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.Detector2#visitClass(edu.umd.cs.findbugs.classfile
     * .ClassDescriptor)
     */
    public void visitClass(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
// Only execute the test once
        if (testExecuted) {
            return;
        }
        testExecuted = true;
        try {
            runnableInstance.get().run();
        }
        catch (java.lang.Throwable e){
            throwable = e;
        }
    }
}
