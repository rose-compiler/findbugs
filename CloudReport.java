/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 William Pugh
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Java main application to compute update a historical bug collection with
 * results from another build/analysis.
 * 
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.IOException;
import java.io.PrintWriter;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.charsets.UTF8;
public class CloudReport extends java.lang.Object {
    public CloudReport() {
    }
/**
     *
     */
    final private static java.lang.String USAGE = "Usage: <cmd>   [<bugs.xml>]";
    public static void main(java.lang.String[] args) throws org.dom4j.DocumentException, java.io.IOException {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        if (args.length > 1) {
            java.lang.System.out.println(USAGE);
            return;
        }
        edu.umd.cs.findbugs.BugCollection bugs = new edu.umd.cs.findbugs.SortedBugCollection();
        if (args.length == 0) bugs.readXML(java.lang.System.in);
        else bugs.readXML(args[0]);
        bugs.getCloud().waitUntilIssueDataDownloaded();
        java.io.PrintWriter out = edu.umd.cs.findbugs.charsets.UTF8.printWriter(java.lang.System.out);
        bugs.getCloud().printCloudSummary(out,bugs,new java.lang.String[0]);
        out.close();
    }
}
