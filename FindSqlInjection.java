/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004,2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Find potential SQL injection vulnerabilities.
 *
 * @author David Hovemeyer
 * @author Bill Pugh
 * @author Matt Hargett
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.Iterator;
import java.util.regex.Pattern;
import javax.annotation.CheckForNull;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.AALOAD;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.INVOKEINTERFACE;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.NOP;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.BugAccumulator;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.ba.BasicBlock;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.EdgeTypes;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.constant.Constant;
import edu.umd.cs.findbugs.ba.constant.ConstantDataflow;
import edu.umd.cs.findbugs.ba.constant.ConstantFrame;
import edu.umd.cs.findbugs.ba.type.TopType;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.type.TypeFrame;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
public class FindSqlInjection extends java.lang.Object implements edu.umd.cs.findbugs.Detector {
    private static class StringAppendState extends java.lang.Object {
        public StringAppendState() {
        }
// remember the smallest position at which we saw something that
// concerns us
        int sawOpenQuote = java.lang.Integer.MAX_VALUE;
        int sawCloseQuote = java.lang.Integer.MAX_VALUE;
        int sawComma = java.lang.Integer.MAX_VALUE;
        int sawAppend = java.lang.Integer.MAX_VALUE;
        int sawUnsafeAppend = java.lang.Integer.MAX_VALUE;
        int sawTaint = java.lang.Integer.MAX_VALUE;
        int sawSeriousTaint = java.lang.Integer.MAX_VALUE;
        public boolean getSawOpenQuote(org.apache.bcel.generic.InstructionHandle handle) {
            return sawOpenQuote <= handle.getPosition();
        }
        public boolean getSawCloseQuote(org.apache.bcel.generic.InstructionHandle handle) {
            return sawCloseQuote <= handle.getPosition();
        }
        public boolean getSawComma(org.apache.bcel.generic.InstructionHandle handle) {
            return sawComma <= handle.getPosition();
        }
        public boolean getSawAppend(org.apache.bcel.generic.InstructionHandle handle) {
            return sawAppend <= handle.getPosition();
        }
        public boolean getSawUnsafeAppend(org.apache.bcel.generic.InstructionHandle handle) {
            return sawUnsafeAppend <= handle.getPosition();
        }
        public boolean getSawTaint(org.apache.bcel.generic.InstructionHandle handle) {
            return sawTaint <= handle.getPosition();
        }
        public boolean getSawSeriousTaint(org.apache.bcel.generic.InstructionHandle handle) {
            return sawSeriousTaint <= handle.getPosition();
        }
        public void setSawOpenQuote(org.apache.bcel.generic.InstructionHandle handle) {
            sawOpenQuote = java.lang.Math.min(sawOpenQuote,handle.getPosition());
        }
        public void setSawCloseQuote(org.apache.bcel.generic.InstructionHandle handle) {
            sawCloseQuote = java.lang.Math.min(sawCloseQuote,handle.getPosition());
        }
        public void setSawComma(org.apache.bcel.generic.InstructionHandle handle) {
            sawComma = java.lang.Math.min(sawComma,handle.getPosition());
        }
        public void setSawAppend(org.apache.bcel.generic.InstructionHandle handle) {
            sawAppend = java.lang.Math.min(sawAppend,handle.getPosition());
        }
        public void setSawUnsafeAppend(org.apache.bcel.generic.InstructionHandle handle) {
            sawUnsafeAppend = java.lang.Math.min(sawUnsafeAppend,handle.getPosition());
        }
        public void setSawSeriousTaint(org.apache.bcel.generic.InstructionHandle handle) {
            sawSeriousTaint = java.lang.Math.min(sawSeriousTaint,handle.getPosition());
        }
        public void setSawTaint(org.apache.bcel.generic.InstructionHandle handle) {
            sawTaint = java.lang.Math.min(sawTaint,handle.getPosition());
        }
        public void setSawInitialTaint() {
            sawTaint = 0;
        }
    }
    edu.umd.cs.findbugs.BugReporter bugReporter;
    edu.umd.cs.findbugs.BugAccumulator bugAccumulator;
    public FindSqlInjection(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
        this.bugAccumulator = new edu.umd.cs.findbugs.BugAccumulator(bugReporter);
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.JavaClass javaClass = classContext.getJavaClass();
        org.apache.bcel.classfile.Method[] methodList = javaClass.getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
            if (methodGen == null) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("FindSqlInjection caught exception while analyzing " + classContext.getFullyQualifiedMethodName(method),e);
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("FindSqlInjection caught exception while analyzing " + classContext.getFullyQualifiedMethodName(method),e);
            }
            catch (java.lang.RuntimeException e){
                bugReporter.logError("FindSqlInjection caught exception while analyzing " + classContext.getFullyQualifiedMethodName(method),e);
            }
        }
;
    }
    private boolean isStringAppend(org.apache.bcel.generic.Instruction ins, org.apache.bcel.generic.ConstantPoolGen cpg) {
        if (ins instanceof org.apache.bcel.generic.INVOKEVIRTUAL) {
            org.apache.bcel.generic.INVOKEVIRTUAL invoke = (org.apache.bcel.generic.INVOKEVIRTUAL) (ins) ;
            if (invoke.getMethodName(cpg).equals("append") && invoke.getClassName(cpg).startsWith("java.lang.StringB")) {
                java.lang.String sig = invoke.getSignature(cpg);
                char firstChar = sig.charAt(1);
                return firstChar == '\u005b' || firstChar == '\u004c';
            }
        }
        return false;
    }
    private boolean isConstantStringLoad(edu.umd.cs.findbugs.ba.Location location, org.apache.bcel.generic.ConstantPoolGen cpg) {
        org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
        if (ins instanceof org.apache.bcel.generic.LDC) {
            org.apache.bcel.generic.LDC load = (org.apache.bcel.generic.LDC) (ins) ;
            java.lang.Object value = load.getValue(cpg);
            if (value instanceof java.lang.String) {
                return true;
            }
        }
        return false;
    }
    final static java.util.regex.Pattern openQuotePattern = java.util.regex.Pattern.compile("((^')|[^\\p{Alnum}]')$");
    public static boolean isOpenQuote(java.lang.String s) {
        return openQuotePattern.matcher(s).find();
    }
    final static java.util.regex.Pattern closeQuotePattern = java.util.regex.Pattern.compile("^'($|[^\\p{Alnum}])");
    public static boolean isCloseQuote(java.lang.String s) {
        return closeQuotePattern.matcher(s).find();
    }
    private edu.umd.cs.findbugs.detect.FindSqlInjection.StringAppendState updateStringAppendState(edu.umd.cs.findbugs.ba.Location location, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.detect.FindSqlInjection.StringAppendState stringAppendState) {
        org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
        org.apache.bcel.generic.Instruction ins = handle.getInstruction();
        if ( !this.isConstantStringLoad(location,cpg)) {
            throw new java.lang.IllegalArgumentException("instruction must be LDC");
        }
        org.apache.bcel.generic.LDC load = (org.apache.bcel.generic.LDC) (ins) ;
        java.lang.Object value = load.getValue(cpg);
        java.lang.String stringValue = ((java.lang.String) (value) ).trim();
        if (stringValue.startsWith(",") || stringValue.endsWith(",")) stringAppendState.setSawComma(handle);
        if (isCloseQuote(stringValue) && stringAppendState.getSawOpenQuote(handle)) stringAppendState.setSawCloseQuote(handle);
        if (isOpenQuote(stringValue)) stringAppendState.setSawOpenQuote(handle);
        return stringAppendState;
    }
    private boolean isPreparedStatementDatabaseSink(org.apache.bcel.generic.Instruction ins, org.apache.bcel.generic.ConstantPoolGen cpg) {
        if ( !(ins instanceof org.apache.bcel.generic.INVOKEINTERFACE)) {
            return false;
        }
        org.apache.bcel.generic.INVOKEINTERFACE invoke = (org.apache.bcel.generic.INVOKEINTERFACE) (ins) ;
        java.lang.String methodName = invoke.getMethodName(cpg);
        java.lang.String methodSignature = invoke.getSignature(cpg);
        java.lang.String interfaceName = invoke.getClassName(cpg);
        if (methodName.equals("prepareStatement") && interfaceName.equals("java.sql.Connection") && methodSignature.startsWith("(Ljava/lang/String;")) {
            return true;
        }
        return false;
    }
    private boolean isExecuteDatabaseSink(org.apache.bcel.generic.InvokeInstruction ins, org.apache.bcel.generic.ConstantPoolGen cpg) {
        if ( !(ins instanceof org.apache.bcel.generic.INVOKEINTERFACE)) {
            return false;
        }
        org.apache.bcel.generic.INVOKEINTERFACE invoke = (org.apache.bcel.generic.INVOKEINTERFACE) (ins) ;
        java.lang.String methodName = invoke.getMethodName(cpg);
        java.lang.String methodSignature = invoke.getSignature(cpg);
        java.lang.String interfaceName = invoke.getClassName(cpg);
        if (methodName.startsWith("execute") && interfaceName.equals("java.sql.Statement") && methodSignature.startsWith("(Ljava/lang/String;")) {
            return true;
        }
        return false;
    }
    private boolean isDatabaseSink(org.apache.bcel.generic.InvokeInstruction ins, org.apache.bcel.generic.ConstantPoolGen cpg) {
        return this.isPreparedStatementDatabaseSink(ins,cpg) || this.isExecuteDatabaseSink(ins,cpg);
    }
    private edu.umd.cs.findbugs.detect.FindSqlInjection.StringAppendState getStringAppendState(edu.umd.cs.findbugs.ba.CFG cfg, org.apache.bcel.generic.ConstantPoolGen cpg) throws edu.umd.cs.findbugs.ba.CFGBuilderException {
        edu.umd.cs.findbugs.detect.FindSqlInjection.StringAppendState stringAppendState = new edu.umd.cs.findbugs.detect.FindSqlInjection.StringAppendState();
        java.lang.String sig = method.getSignature();
        sig = sig.substring(0,sig.indexOf('\u0029'));
        if (sig.indexOf("java/lang/String") >= 0) stringAppendState.setSawInitialTaint();
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if (this.isConstantStringLoad(location,cpg)) {
                stringAppendState = this.updateStringAppendState(location,cpg,stringAppendState);
            }
            else if (this.isStringAppend(ins,cpg)) {
                stringAppendState.setSawAppend(handle);
                edu.umd.cs.findbugs.ba.Location prevLocation = this.getPreviousLocation(cfg,location,true);
                if (prevLocation != null &&  !this.isSafeValue(prevLocation,cpg)) stringAppendState.setSawUnsafeAppend(handle);
            }
            else if (ins instanceof org.apache.bcel.generic.InvokeInstruction) {
                org.apache.bcel.generic.InvokeInstruction inv = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
                java.lang.String sig1 = inv.getSignature(cpg);
                java.lang.String sig2 = sig1.substring(sig1.indexOf('\u0029'));
                if (sig2.indexOf("java/lang/String") >= 0) {
                    java.lang.String methodName = inv.getMethodName(cpg);
                    java.lang.String className = inv.getClassName(cpg);
                    if (methodName.equals("valueOf") && className.equals("java.lang.String") && sig1.equals("(Ljava/lang/Object;)Ljava/lang/String;")) {
                        try {
                            edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = classContext.getTypeDataflow(method);
                            edu.umd.cs.findbugs.ba.type.TypeFrame frame = typeDataflow.getFactAtLocation(location);
                            if ( !frame.isValid()) {
                                continue;
                            }
                            org.apache.bcel.generic.Type operandType = frame.getTopValue();
                            if (operandType.equals(edu.umd.cs.findbugs.ba.type.TopType.instance())) {
                                continue;
                            }
                            java.lang.String sig3 = operandType.getSignature();
                            if ( !sig3.equals("Ljava/lang/String;")) stringAppendState.setSawTaint(handle);
                        }
                        catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
                            stringAppendState.setSawTaint(handle);
                        }
                    }
                    else if (className.startsWith("java.lang.String") || className.equals("java.lang.Long") || className.equals("java.lang.Integer") || className.equals("java.lang.Float") || className.equals("java.lang.Double") || className.equals("java.lang.Short") || className.equals("java.lang.Byte") || className.equals("java.lang.Character")) {
                        assert true;
                    }
                    else if (methodName.startsWith("to") && methodName.endsWith("String") && methodName.length() > 8) {
                        assert true;
                    }
                    else if (className.startsWith("javax.servlet") && methodName.startsWith("get")) {
                        stringAppendState.setSawTaint(handle);
                        stringAppendState.setSawSeriousTaint(handle);
                    }
                    else stringAppendState.setSawTaint(handle);
                }
            }
            else if (ins instanceof org.apache.bcel.generic.GETFIELD) {
                org.apache.bcel.generic.GETFIELD getfield = (org.apache.bcel.generic.GETFIELD) (ins) ;
                java.lang.String sig2 = getfield.getSignature(cpg);
                if (sig2.indexOf("java/lang/String") >= 0) stringAppendState.setSawTaint(handle);
            }
        }
// This basic block is probably dead
// unreachable
// ignore it
// ignore it
        return stringAppendState;
    }
    private boolean isSafeValue(edu.umd.cs.findbugs.ba.Location location, org.apache.bcel.generic.ConstantPoolGen cpg) throws edu.umd.cs.findbugs.ba.CFGBuilderException {
        org.apache.bcel.generic.Instruction prevIns = location.getHandle().getInstruction();
        if (prevIns instanceof org.apache.bcel.generic.LDC || prevIns instanceof org.apache.bcel.generic.GETSTATIC) return true;
        if (prevIns instanceof org.apache.bcel.generic.InvokeInstruction) {
            java.lang.String methodName = ((org.apache.bcel.generic.InvokeInstruction) (prevIns) ).getMethodName(cpg);
            if (methodName.startsWith("to") && methodName.endsWith("String") && methodName.length() > 8) return true;
        }
        if (prevIns instanceof org.apache.bcel.generic.AALOAD) {
            edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
            edu.umd.cs.findbugs.ba.Location prev = this.getPreviousLocation(cfg,location,true);
            if (prev != null) {
                edu.umd.cs.findbugs.ba.Location prev2 = this.getPreviousLocation(cfg,prev,true);
                if (prev2 != null && prev2.getHandle().getInstruction() instanceof org.apache.bcel.generic.GETSTATIC) {
                    org.apache.bcel.generic.GETSTATIC getStatic = (org.apache.bcel.generic.GETSTATIC) (prev2.getHandle().getInstruction()) ;
                    if (getStatic.getSignature(cpg).equals("[Ljava/lang/String;")) return true;
                }
            }
        }
        return false;
    }
    private org.apache.bcel.generic.InstructionHandle getPreviousInstruction(org.apache.bcel.generic.InstructionHandle handle, boolean skipNops) {
        while (handle.getPrev() != null) {
            handle = handle.getPrev();
            org.apache.bcel.generic.Instruction prevIns = handle.getInstruction();
            if ( !(prevIns instanceof org.apache.bcel.generic.NOP && skipNops)) {
                return handle;
            }
        }
        return null;
    }
    private edu.umd.cs.findbugs.ba.Location getPreviousLocation(edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.Location startLocation, boolean skipNops) {
        edu.umd.cs.findbugs.ba.Location loc = startLocation;
        org.apache.bcel.generic.InstructionHandle prev = this.getPreviousInstruction(loc.getHandle(),skipNops);
        if (prev != null) return new edu.umd.cs.findbugs.ba.Location(prev, loc.getBasicBlock());
        edu.umd.cs.findbugs.ba.BasicBlock block = loc.getBasicBlock();
        while (true) {
            block = cfg.getPredecessorWithEdgeType(block,edu.umd.cs.findbugs.ba.EdgeTypes.FALL_THROUGH_EDGE);
            if (block == null) return null;
            org.apache.bcel.generic.InstructionHandle lastInstruction = block.getLastInstruction();
            if (lastInstruction != null) return new edu.umd.cs.findbugs.ba.Location(lastInstruction, block);
        }
    }
    private edu.umd.cs.findbugs.BugInstance generateBugInstance(org.apache.bcel.classfile.JavaClass javaClass, org.apache.bcel.generic.MethodGen methodGen, org.apache.bcel.generic.InstructionHandle handle, edu.umd.cs.findbugs.detect.FindSqlInjection.StringAppendState stringAppendState) {
        org.apache.bcel.generic.Instruction instruction = handle.getInstruction();
        org.apache.bcel.generic.ConstantPoolGen cpg = methodGen.getConstantPool();
        int priority = LOW_PRIORITY;
        boolean sawSeriousTaint = false;
        if (stringAppendState.getSawAppend(handle)) {
            if (stringAppendState.getSawOpenQuote(handle) && stringAppendState.getSawCloseQuote(handle)) {
                priority = HIGH_PRIORITY;
            }
            else if (stringAppendState.getSawComma(handle)) {
                priority = NORMAL_PRIORITY;
            }
            if ( !stringAppendState.getSawUnsafeAppend(handle)) {
                priority += 2;
            }
            else if (stringAppendState.getSawSeriousTaint(handle)) {
                priority--;
                sawSeriousTaint = true;
            }
            else if ( !stringAppendState.getSawTaint(handle)) {
                priority++;
            }
        }
        java.lang.String description = "TESTING";
        if (instruction instanceof org.apache.bcel.generic.InvokeInstruction && this.isExecuteDatabaseSink((org.apache.bcel.generic.InvokeInstruction) (instruction) ,cpg)) {
            description = "SQL_NONCONSTANT_STRING_PASSED_TO_EXECUTE";
        }
        else if (this.isPreparedStatementDatabaseSink(instruction,cpg)) {
            description = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING";
        }
        edu.umd.cs.findbugs.BugInstance bug = new edu.umd.cs.findbugs.BugInstance(this, description, priority);
        bug.addClassAndMethod(methodGen,javaClass.getSourceFileName());
        if (description.equals("TESTING")) bug.addString("Incomplete report invoking non-constant SQL string");
        if (sawSeriousTaint) bug.addString("non-constant SQL string involving HTTP taint");
        return bug;
    }
    org.apache.bcel.classfile.Method method;
    edu.umd.cs.findbugs.ba.ClassContext classContext;
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.CFGBuilderException, edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        org.apache.bcel.classfile.JavaClass javaClass = classContext.getJavaClass();
        this.method = method;
        this.classContext = classContext;
        org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
        if (methodGen == null) return;
        org.apache.bcel.generic.ConstantPoolGen cpg = methodGen.getConstantPool();
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.detect.FindSqlInjection.StringAppendState stringAppendState = this.getStringAppendState(cfg,cpg);
        edu.umd.cs.findbugs.ba.constant.ConstantDataflow dataflow = classContext.getConstantDataflow(method);
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.InvokeInstruction)) continue;
            org.apache.bcel.generic.InvokeInstruction invoke = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
            if (this.isDatabaseSink(invoke,cpg)) {
                edu.umd.cs.findbugs.ba.constant.ConstantFrame frame = dataflow.getFactAtLocation(location);
                int numArguments = frame.getNumArguments(invoke,cpg);
                edu.umd.cs.findbugs.ba.constant.Constant value = frame.getStackValue(numArguments - 1);
                if ( !value.isConstantString()) {
                    edu.umd.cs.findbugs.ba.Location prev = this.getPreviousLocation(cfg,location,true);
                    if (prev == null ||  !this.isSafeValue(prev,cpg)) {
                        edu.umd.cs.findbugs.BugInstance bug = this.generateBugInstance(javaClass,methodGen,location.getHandle(),stringAppendState);
                        bugAccumulator.accumulateBug(bug,edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,methodGen,javaClass.getSourceFileName(),location.getHandle()));
                    }
                }
            }
        }
        bugAccumulator.reportAccumulatedBugs();
    }
// TODO: verify it's the same string represented by
// stringAppendState
// FIXME: will false positive on const/static strings
// returns by methods
    public void report() {
    }
}
// vim:ts=4
