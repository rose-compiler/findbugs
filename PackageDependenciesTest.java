/*
 * Contributions to FindBugs
 * Copyright (C) 2009, Tom\u00e1s Pollak
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Verifies the package dependencies.
 * 
 * @author Tom\u00e1s Pollak
 * @author Andrei Loskutov
 */
package edu.umd.cs.findbugs.architecture;
import edu.umd.cs.findbugs.architecture.*;
import jdepend.framework.JDepend;
import jdepend.framework.JavaPackage;
import junit.framework.TestCase;
public class PackageDependenciesTest extends junit.framework.TestCase {
    public PackageDependenciesTest() {
    }
    private jdepend.framework.JDepend engine;
    public void testGui2Dependencies() {
        java.lang.String expectedNotEfferent = "edu.umd.cs.findbugs.gui2";
        this.assertPackageConstraint("edu.umd.cs.findbugs",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.asm",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.ba",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.bcel",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.classfile",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.cloud",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.detect",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.graph",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.io",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.log",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.model",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.plan",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.util",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.visitclass",expectedNotEfferent);
        this.assertPackageConstraint("edu.umd.cs.findbugs.xml",expectedNotEfferent);
    }
    protected void setUp() throws java.lang.Exception {
        super.setUp();
// Get the classes root directory
        java.lang.String rootDirectory = this.getClass().getResource("/").getFile();
        engine = new jdepend.framework.JDepend();
        engine.addDirectory(rootDirectory);
        engine.analyze();
    }
// Setup the JDepend analysis
    protected void tearDown() throws java.lang.Exception {
        engine = null;
        super.tearDown();
    }
    private void assertPackageConstraint(java.lang.String afferent, java.lang.String expectedNotEfferent) {
        jdepend.framework.JavaPackage afferentPackage = engine.getPackage(afferent);
        assertNotNull("Afferent package not found: " + afferent,afferentPackage);
        jdepend.framework.JavaPackage efferentPackage = engine.getPackage(expectedNotEfferent);
        assertNotNull("Efferent package not found: " + efferentPackage,efferentPackage);
        assertFalse(afferentPackage.getName() + " shouldn't depend on " + efferentPackage.getName(),afferentPackage.getEfferents().contains(efferentPackage));
    }
}
