/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Convert part or all of a Java type signature into something closer to what
 * types look like in the source code. Both field and method signatures may be
 * processed by this class. For a field signature, just call parseNext() once.
 * For a method signature, parseNext() must be called multiple times, and the
 * parens around the arguments must be skipped manually (by calling the skip()
 * method).
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
public class SignatureConverter extends java.lang.Object {
    private java.lang.String signature;
/**
     * Constructor.
     * 
     * @param signature
     *            the field or method signature to convert
     */
    public SignatureConverter(java.lang.String signature) {
        super();
        this.signature = signature;
    }
/**
     * Get the first character of the remaining part of the signature.
     */
    public char getFirst() {
        return signature.charAt(0);
    }
/**
     * Skip the first character of the remaining part of the signature.
     */
    public void skip() {
        signature = signature.substring(1);
    }
/**
     * Parse a single type out of the signature, starting at the beginning of
     * the remaining part of the signature. For example, if the first character
     * of the remaining part is "I", then this method will return "int", and the
     * "I" will be consumed. Arrays, reference types, and basic types are all
     * handled.
     * 
     * @return the parsed type string
     */
    public java.lang.String parseNext() {
        java.lang.StringBuilder result = new java.lang.StringBuilder();
        if (signature.startsWith("[")) {
            int dimensions = 0;
            do {
                ++dimensions;
                signature = signature.substring(1);
            }
            while (signature.charAt(0) == '\u005b');
            result.append(this.parseNext());
            while (dimensions-- > 0) {
                result.append("[]");
            }
        }
        else if (signature.startsWith("L")) {
            int semi = signature.indexOf('\u003b');
            if (semi < 0) throw new java.lang.IllegalStateException("missing semicolon in signature " + signature);
            result.append(signature.substring(1,semi).replace('\u002f','\u002e'));
            signature = signature.substring(semi + 1);
        }
        else {
            switch(signature.charAt(0)){
                case '\u0042':{
                    result.append("byte");
                    break;
                }
                case '\u0043':{
                    result.append("char");
                    break;
                }
                case '\u0044':{
                    result.append("double");
                    break;
                }
                case '\u0046':{
                    result.append("float");
                    break;
                }
                case '\u0049':{
                    result.append("int");
                    break;
                }
                case '\u004a':{
                    result.append("long");
                    break;
                }
                case '\u0053':{
                    result.append("short");
                    break;
                }
                case '\u005a':{
                    result.append("boolean");
                    break;
                }
                case '\u0056':{
                    result.append("void");
                    break;
                }
                default:{
                    throw new java.lang.IllegalArgumentException("bad signature " + signature);
                }
            }
            this.skip();
        }
        return result.toString();
    }
/**
     * Convenience method for generating a method signature in human readable
     * form.
     * 
     * @param javaClass
     *            the class
     * @param method
     *            the method
     */
    public static java.lang.String convertMethodSignature(org.apache.bcel.classfile.JavaClass javaClass, org.apache.bcel.classfile.Method method) {
        return convertMethodSignature(javaClass.getClassName(),method.getName(),method.getSignature());
    }
/**
     * Convenience method for generating a method signature in human readable
     * form.
     * 
     * @param methodGen
     *            the method to produce a method signature for
     */
    public static java.lang.String convertMethodSignature(org.apache.bcel.generic.MethodGen methodGen) {
        return convertMethodSignature(methodGen.getClassName(),methodGen.getName(),methodGen.getSignature());
    }
/**
     * Convenience method for generating a method signature in human readable
     * form.
     * 
     * @param inv
     *            an InvokeInstruction
     * @param cpg
     *            the ConstantPoolGen for the class the instruction belongs to
     */
    public static java.lang.String convertMethodSignature(org.apache.bcel.generic.InvokeInstruction inv, org.apache.bcel.generic.ConstantPoolGen cpg) {
        return convertMethodSignature(inv.getClassName(cpg),inv.getName(cpg),inv.getSignature(cpg));
    }
/**
     * Convenience method for generating a method signature in human readable
     * form.
     * 
     * @param className
     *            name of the class containing the method
     * @param methodName
     *            the name of the method
     * @param methodSig
     *            the signature of the method
     */
    public static java.lang.String convertMethodSignature(java.lang.String className, java.lang.String methodName, java.lang.String methodSig) {
        return convertMethodSignature(className,methodName,methodSig,"");
    }
/**
     * Convenience method for generating a method signature in human readable
     * form.
     * 
     * @param xmethod
     *            an XMethod
     * @return the formatted version of that signature
     */
    public static java.lang.String convertMethodSignature(edu.umd.cs.findbugs.ba.XMethod xmethod) {
        java.lang.String className = xmethod.getClassName();
        assert className.indexOf('\u002f') ==  -1;
        return convertMethodSignature(className,xmethod.getName(),xmethod.getSignature());
    }
/**
     * Convenience method for generating a method signature in human readable
     * form.
     * 
     * @param methodDescriptor
     *            a MethodDescriptor
     * @return the formatted version of that signature
     */
    public static java.lang.String convertMethodSignature(edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor) {
        return convertMethodSignature(methodDescriptor.getClassDescriptor().toDottedClassName(),methodDescriptor.getName(),methodDescriptor.getSignature());
    }
/**
     * Convenience method for generating a method signature in human readable
     * form.
     * 
     * @param className
     *            name of the class containing the method
     * @param methodName
     *            the name of the method
     * @param methodSig
     *            the signature of the method
     * @param pkgName
     *            the name of the package the method is in (used to shorten
     *            class names)
     */
    public static java.lang.String convertMethodSignature(java.lang.String className, java.lang.String methodName, java.lang.String methodSig, java.lang.String pkgName) {
        java.lang.StringBuilder args = new java.lang.StringBuilder();
        edu.umd.cs.findbugs.ba.SignatureConverter converter = new edu.umd.cs.findbugs.ba.SignatureConverter(methodSig);
        converter.skip();
        args.append('\u0028');
        while (converter.getFirst() != '\u0029') {
            if (args.length() > 1) args.append(", ");
            args.append(shorten(pkgName,converter.parseNext()));
        }
        converter.skip();
        args.append('\u0029');
// Ignore return type
        java.lang.StringBuilder result = new java.lang.StringBuilder();
        result.append(className);
        result.append('\u002e');
        result.append(methodName);
        result.append(args.toString());
        return result.toString();
    }
/**
     * Convenience method for converting a single signature component to
     * human-readable form.
     * 
     * @param signature
     *            the signature
     */
    public static java.lang.String convert(java.lang.String signature) {
        return new edu.umd.cs.findbugs.ba.SignatureConverter(signature).parseNext();
    }
    public static java.lang.String shorten(java.lang.String pkgName, java.lang.String typeName) {
        int index = typeName.lastIndexOf('\u002e');
        if (index >= 0) {
            java.lang.String otherPkg = typeName.substring(0,index);
            if (otherPkg.equals(pkgName) || otherPkg.equals("java.lang")) typeName = typeName.substring(index + 1);
        }
        return typeName;
    }
}
// vim:ts=4
