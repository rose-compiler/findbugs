/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.ba.npe;
import edu.umd.cs.findbugs.ba.npe.*;
import javax.annotation.CheckForNull;
import edu.umd.cs.findbugs.ba.XField;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.ba.XMethodParameter;
abstract public class PointerUsageRequiringNonNullValue extends java.lang.Object {
    public PointerUsageRequiringNonNullValue() {
    }
    abstract public java.lang.String getDescription();
    public java.lang.String toString() {
        return this.getDescription();
    }
    public boolean isDirect() {
        return false;
    }
    public boolean isReturnFromNonNullMethod() {
        return false;
    }
    public edu.umd.cs.findbugs.ba.XMethodParameter getNonNullParameter() {
        return null;
    }
    public edu.umd.cs.findbugs.ba.XField getNonNullField() {
        return null;
    }
    final private static edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue instance = new edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue() {
        public boolean isDirect() {
            return true;
        }
        public java.lang.String getDescription() {
            return "SOURCE_LINE_DEREF";
        }
    };
    final private static edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue nonNullReturnInstance = new edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue() {
        public boolean isReturnFromNonNullMethod() {
            return true;
        }
        public java.lang.String getDescription() {
            return "SOURCE_LINE_RETURNED";
        }
    };
    public static edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue getPointerDereference() {
        return instance;
    }
    final private static edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue nullCheckInstance = new edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue() {
        public java.lang.String getDescription() {
            return "SOURCE_LINE_NULL_CHECKED";
        }
    };
    public static edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue getPointerNullChecked() {
        return nullCheckInstance;
    }
    public static edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue getReturnFromNonNullMethod(edu.umd.cs.findbugs.ba.XMethod m) {
        return nonNullReturnInstance;
    }
    public static edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue getPassedAsNonNullParameter(final edu.umd.cs.findbugs.ba.XMethod m, final int param) {
        return new edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue() {
            public edu.umd.cs.findbugs.ba.XMethodParameter getNonNullParameter() {
                return new edu.umd.cs.findbugs.ba.XMethodParameter(m, param);
            }
            public java.lang.String getDescription() {
                return "SOURCE_LINE_INVOKED";
            }
        };
    }
    public static edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue getStoredIntoNonNullField(final edu.umd.cs.findbugs.ba.XField f) {
        return new edu.umd.cs.findbugs.ba.npe.PointerUsageRequiringNonNullValue() {
            public edu.umd.cs.findbugs.ba.XField getNonNullField() {
                return f;
            }
            public java.lang.String getDescription() {
                return "SOURCE_LINE_STORED";
            }
        };
    }
}
