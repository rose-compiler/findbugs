/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import junit.framework.TestCase;
public class ObfuscateTest extends junit.framework.TestCase {
    public ObfuscateTest() {
    }
    public void testMethodSignature() {
        java.lang.String sig = "([Lcom.google.Search;I)Lcom.google.Money;";
        java.lang.String result = edu.umd.cs.findbugs.Obfuscate.hashMethodSignature(sig);
        assertTrue("hash of " + sig + " gives " + result,result.indexOf("google") ==  -1);
        java.lang.System.out.println(result);
    }
    public void testMethodSignatureDoesntChangeForCoreTypes() {
        java.lang.String sig = "([Ljava/lang/String;I)Ljava/util/Map;";
        java.lang.String result = edu.umd.cs.findbugs.Obfuscate.hashMethodSignature(sig);
        assertEquals(sig,result);
    }
}
