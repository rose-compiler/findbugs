package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * An edge of a control flow graph.
 * 
 * @author David Hovemeyer
 * @see BasicBlock
 * @see CFG
 */
/*
     * ----------------------------------------------------------------------
     * Fields
     * ----------------------------------------------------------------------
     */
/*
     * ----------------------------------------------------------------------
     * Public methods
     * ----------------------------------------------------------------------
     */
/**
     * Constructor.
     * 
     * @param source
     *            source basic block
     * @param dest
     *            destination basic block
     */
/**
     * Get the type of edge.
     */
/**
     * Set the type of edge.
     */
/**
     * Get the edge flags.
     */
/**
     * Set the edge flags.
     */
/**
     * Return if given edge flag is set.
     * 
     * @param flag
     *            the edge flag
     * @return true if the flag is set, false otherwise
     */
/**
     * Is the edge an exception edge?
     */
/**
     * Compare with other edge.
     */
/**
     * Return a string representation of the edge.
     */
/**
     * Get string representing given edge type.
     */
/**
     * Get numeric edge type from string representation.
     */
// vim:ts=4
abstract interface package-info {
}
