/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nonnull;
import org.dom4j.DocumentException;
public class ExcludingHashesBugReporter extends edu.umd.cs.findbugs.DelegatingBugReporter {
    java.util.Set<java.lang.String> excludedHashes = new java.util.HashSet<java.lang.String>();
/**
     * @param delegate
     * @throws DocumentException
     * @throws IOException
     */
    public ExcludingHashesBugReporter(edu.umd.cs.findbugs.BugReporter delegate, java.lang.String baseline) throws org.dom4j.DocumentException, java.io.IOException {
        super(delegate);
        addToExcludedInstanceHashes(excludedHashes,baseline);
    }
/**
     * @param baseline
     * @throws IOException
     * @throws DocumentException
     */
    public static void addToExcludedInstanceHashes(java.util.Set<java.lang.String> instanceHashesToExclude, java.lang.String baseline) throws org.dom4j.DocumentException, java.io.IOException {
        edu.umd.cs.findbugs.Project project = new edu.umd.cs.findbugs.Project();
        edu.umd.cs.findbugs.BugCollection origCollection;
        origCollection = new edu.umd.cs.findbugs.SortedBugCollection(project);
        origCollection.readXML(baseline);
        for (edu.umd.cs.findbugs.BugInstance b : origCollection.getCollection())instanceHashesToExclude.add(b.getInstanceHash());
;
    }
    public void reportBug(edu.umd.cs.findbugs.BugInstance bugInstance) {
        java.lang.String instanceHash = bugInstance.getInstanceHash();
        if ( !excludedHashes.contains(instanceHash)) this.getDelegate().reportBug(bugInstance);
    }
}
