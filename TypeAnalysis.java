package edu.umd.cs.findbugs.ba.type;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.type.*;
/**
 * A forward dataflow analysis to determine the types of all values in the Java
 * stack frame at all points in a Java method. The values include local
 * variables and values on the Java operand stack.
 * <p/>
 * <p>
 * As a side effect, the analysis computes the exception set throwable on each
 * exception edge in the CFG. This information can be used to prune infeasible
 * exception edges, and mark exception edges which propagate only implicit
 * exceptions.
 * 
 * @author David Hovemeyer
 * @see Dataflow
 * @see edu.umd.cs.findbugs.ba.DataflowAnalysis
 * @see TypeFrame
 */
/**
     * Force computation of accurate exceptions.
     */
/**
     * Repository of information about thrown exceptions computed for a basic
     * block and its outgoing exception edges. It contains a result TypeFrame,
     * which is used to detect when the exception information needs to be
     * recomputed for the block.
     */
/**
     * Cached information about an instanceof check.
     */
/**
         * @return Returns the valueNumber.
         */
/**
         * @return Returns the type.
         */
/**
     * Constructor.
     * 
     * @param method
     *            TODO
     * @param methodGen
     *            the MethodGen whose CFG we'll be analyzing
     * @param cfg
     *            the control flow graph
     * @param dfs
     *            DepthFirstSearch of the method
     * @param typeMerger
     *            object to merge types
     * @param visitor
     *            a TypeFrameModelingVisitor to use to model the effect of
     *            instructions
     * @param lookupFailureCallback
     *            lookup failure callback
     * @param exceptionSetFactory
     *            factory for creating ExceptionSet objects
     */
/**
     * Constructor.
     * 
     * @param method
     *            TODO
     * @param methodGen
     *            the MethodGen whose CFG we'll be analyzing
     * @param cfg
     *            the control flow graph
     * @param dfs
     *            DepthFirstSearch of the method
     * @param typeMerger
     *            object to merge types
     * @param lookupFailureCallback
     *            lookup failure callback
     * @param exceptionSetFactory
     *            factory for creating ExceptionSet objects
     */
/**
     * Constructor which uses StandardTypeMerger.
     * 
     * @param method
     *            TODO
     * @param methodGen
     *            the MethodGen whose CFG we'll be analyzing
     * @param cfg
     *            the control flow graph
     * @param dfs
     *            DepthFirstSearch of the method
     * @param lookupFailureCallback
     *            callback for Repository lookup failures
     * @param exceptionSetFactory
     *            factory for creating ExceptionSet objects
     */
/**
     * Set the ValueNumberDataflow for the method being analyzed. This is
     * optional; if set, it will be used to make instanceof instructions more
     * precise.
     * 
     * @param valueNumberDataflow
     *            the ValueNumberDataflow
     */
/**
     * Set the FieldStoreTypeDatabase. This can be used to get more accurate
     * types for values loaded from fields.
     * 
     * @param database
     *            the FieldStoreTypeDatabase
     */
/**
     * Get the set of exceptions that can be thrown on given edge. This should
     * only be called after the analysis completes.
     * 
     * @param edge
     *            the Edge
     * @return the ExceptionSet
     */
// Make the frame valid
// Clear the stack slots in the frame
// Add local for "this" pointer, if present
// [Added: Support for Generics]
// Get a parser that reads the generic signature of the method and
// can be used to get the correct GenericObjectType if an argument
// has a class type
// Add locals for parameters.
// Note that long and double parameters need to be handled
// specially because they occupy two locals.
// Add special "extra" type for long or double params.
// These occupy the slot before the "plain" type.
// [Added: Support for Generics]
// replace with a generic version of the type
// degrade gracefully
// Add the plain parameter type.
// Set remaining locals to BOTTOM; this will cause any
// uses of them to be flagged
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.AbstractDataflowAnalysis#transfer(edu.umd.cs.findbugs
     * .ba.BasicBlock, org.apache.bcel.generic.InstructionHandle,
     * java.lang.Object, java.lang.Object)
     */
// Compute thrown exception types
// If this block ends with an instanceof check,
// update the cached information about it.
// Do nothing if we're not computing propagated exceptions
// Also, nothing to do if the block is not an exception thrower
// If cached results are up to date, don't recompute.
// Figure out what exceptions can be thrown out
// of the basic block, and mark each exception edge
// with the set of exceptions which can be propagated
// along the edge.
// System.out.println("Shouldn't all blocks have an exception edge");
// Compute exceptions that can be thrown by the
// basic block.
// For each outgoing exception edge, compute exceptions
// that can be thrown. This assumes that the exception
// edges are enumerated in decreasing order of priority.
// In the process, this will remove exceptions from
// the thrown exception set.
// Handling an exception?
// Special case: when merging predecessor facts for entry to
// an exception handler, we clear the stack and push a
// single entry for the exception object. That way, the locals
// can still be merged.
// Determine the type of exception(s) caught.
// Ideally, the exceptions that can be propagated
// on this edge has already been computed.
// System.out.println("Using computed edge exception set!");
// No information about propagated exceptions, so
// pick a type conservatively using the handler catch type.
// handle catches anything
// throwable
// See if we can make some types more precise due to
// a successful instanceof check in the source block.
// System.out.println("no instanceof check for block " +
// edge.getSource().getId());
// System.out.println("instanceof check for block " +
// edge.getSource().getId() + " has no value number");
// System.out.println("Successful check on edge " + edge);
// Successful instanceof check.
// Only refine the type if the cast is feasible: i.e., a
// downcast.
// Otherwise, just set it to TOP.
// Result type is exact IFF types are identical and both are exact
/**
     * Get the cached set of exceptions that can be thrown from given basic
     * block. If this information hasn't been computed yet, then an empty
     * exception set is returned.
     * 
     * @param basicBlock
     *            the block to get the cached exception set for
     * @return the CachedExceptionSet for the block
     */
// When creating the cached exception type set for the first time:
// - the block result is set to TOP, so it won't match
// any block result that has actually been computed
// using the analysis transfer function
// - the exception set is created as empty (which makes it
// return TOP as its common superclass)
/**
     * Compute the set of exceptions that can be thrown from the given basic
     * block. This should only be called if the existing cached exception set is
     * out of date.
     * 
     * @param basicBlock
     *            the basic block
     * @param result
     *            the result fact for the block; this is used to determine
     *            whether or not the cached exception set is up to date
     * @return the cached exception set for the block
     */
/**
     * Based on the set of exceptions that can be thrown from the source basic
     * block, compute the set of exceptions that can propagate along given
     * exception edge. This method should be called for each outgoing exception
     * edge in sequence, so the caught exceptions can be removed from the thrown
     * exception set as needed.
     * 
     * @param edge
     *            the exception edge
     * @param thrownExceptionSet
     *            current set of exceptions that can be thrown, taking earlier
     *            (higher priority) exception edges into account
     * @return the set of exceptions that can propagate along this edge
     */
// The unhandled exception edge always comes
// after all of the handled exception edges.
// Go through the set of thrown exceptions.
// Any that will DEFINITELY be caught be this handler, remove.
// Any that MIGHT be caught, but won't definitely be caught,
// remain.
// ThrownException thrownException = i.next();
// Exception can be thrown along this edge
// And it will definitely be caught
// Exception possibly thrown along this edge
// As a special case, if a class hierarchy lookup
// fails, then we will conservatively assume that the
// exception in question CAN, but WON'T NECESSARILY
// be caught by the handler.
/**
     * Compute the set of exception types that can be thrown by given basic
     * block.
     * 
     * @param basicBlock
     *            the basic block
     * @return the set of exceptions that can be thrown by the block
     */
// Get the exceptions that BCEL knows about.
// Note that all of these are unchecked.
// Assume that an Error may be thrown by any instruction.
// For ATHROW instructions, we generate *two* blocks
// for which the ATHROW is an exception thrower.
//
// - The first, empty basic block, does the null check
// - The second block, which actually contains the ATHROW,
// throws the object on the top of the operand stack
//
// We make a special case of the block containing the ATHROW,
// by removing all of the implicit exceptions,
// and using type information to figure out what is thrown.
// This is the actual ATHROW, not the null check
// and implicit exceptions.
// The frame containing the thrown value is the start fact
// for the block, because ATHROW is guaranteed to be
// the only instruction in the block.
// Check whether or not the frame is valid.
// Sun's javac sometimes emits unreachable code.
// For example, it will emit code that follows a JSR
// subroutine call that never returns.
// If the frame is invalid, then we can just make
// a conservative assumption that anything could be
// thrown at this ATHROW.
// Not sure what is being thrown here.
// Be conservative.
// If it's an InvokeInstruction, add declared exceptions and
// RuntimeException
// Couldn't find declared exceptions,
// so conservatively assume it could thrown any checked
// exception.
// public static void main(String[] argv) throws Exception {
// if (argv.length != 1) {
// System.err.println("Usage: " + TypeAnalysis.class.getName() +
// " <class file>");
// System.exit(1);
// }
//
// DataflowTestDriver<TypeFrame, TypeAnalysis> driver = new
// DataflowTestDriver<TypeFrame, TypeAnalysis>() {
// @Override
// public Dataflow<TypeFrame, TypeAnalysis> createDataflow(ClassContext
// classContext, Method method)
// throws CFGBuilderException, DataflowAnalysisException {
// return classContext.getTypeDataflow(method);
// }
// };
//
// driver.execute(argv[0]);
// }
// vim:ts=3
abstract interface package-info {
}
