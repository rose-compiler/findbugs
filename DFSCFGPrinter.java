/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
public class DFSCFGPrinter extends edu.umd.cs.findbugs.ba.CFGPrinter implements edu.umd.cs.findbugs.ba.DFSEdgeTypes {
    private edu.umd.cs.findbugs.ba.DepthFirstSearch dfs;
    public DFSCFGPrinter(edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.DepthFirstSearch dfs) {
        super(cfg);
        this.dfs = dfs;
    }
    public java.lang.String edgeAnnotate(edu.umd.cs.findbugs.ba.Edge edge) {
        int dfsEdgeType = dfs.getDFSEdgeType(edge);
        switch(dfsEdgeType){
            case UNKNOWN_EDGE:{
                return "UNKNOWN_EDGE";
            }
            case TREE_EDGE:{
                return "TREE_EDGE";
            }
            case BACK_EDGE:{
                return "BACK_EDGE";
            }
            case CROSS_EDGE:{
                return "CROSS_EDGE";
            }
            case FORWARD_EDGE:{
                return "FORWARD_EDGE";
            }
            default:{
                throw new java.lang.IllegalStateException("no DFS edge type?");
            }
        }
    }
}
