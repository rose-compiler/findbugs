/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 William Pugh
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Java main application to compute update a historical bug collection with
 * results from another build/analysis.
 *
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.annotation.CheckForNull;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.AppVersion;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.Priorities;
import edu.umd.cs.findbugs.ProjectStats;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.config.CommandLine;
public class ListBugDatabaseInfo extends java.lang.Object {
    static class ListBugDatabaseInfoCommandLine extends edu.umd.cs.findbugs.config.CommandLine {
        boolean formatDates = false;
        public ListBugDatabaseInfoCommandLine() {
            super();
            this.addSwitch("-formatDates","render dates in textual form");
        }
        public void handleOption(java.lang.String option, java.lang.String optionalExtraPart) {
            if (option.equals("-formatDates")) formatDates = true;
            else throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
        public void handleOptionWithArgument(java.lang.String option, java.lang.String argument) {
            throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
    }
    public ListBugDatabaseInfo() {
    }
    final private static java.lang.String USAGE = "Usage: " + edu.umd.cs.findbugs.workflow.ListBugDatabaseInfo.class.getName() + " [options] data1File data2File data3File ... ";
    public static void main(java.lang.String[] args) throws org.dom4j.DocumentException, java.io.IOException {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        edu.umd.cs.findbugs.workflow.ListBugDatabaseInfo.ListBugDatabaseInfoCommandLine commandLine = new edu.umd.cs.findbugs.workflow.ListBugDatabaseInfo.ListBugDatabaseInfoCommandLine();
        int argCount = commandLine.parse(args,0,java.lang.Integer.MAX_VALUE,USAGE);
        java.io.PrintWriter out = edu.umd.cs.findbugs.charsets.UTF8.printWriter(java.lang.System.out,true);
        if (argCount == args.length) listVersion(out,null,commandLine.formatDates);
        else {
            out.println("version	time	classes	NCSS	errors	total	high	medium	low	file");
            while (argCount < args.length) {
                java.lang.String fileName = args[argCount++];
                listVersion(out,fileName,commandLine.formatDates);
            }
        }
        out.close();
    }
    private static void listVersion(java.io.PrintWriter out, java.lang.String fileName, boolean formatDates) throws org.dom4j.DocumentException, java.io.IOException {
        edu.umd.cs.findbugs.SortedBugCollection origCollection;
        origCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (fileName == null) origCollection.readXML(java.lang.System.in);
        else origCollection.readXML(fileName);
        edu.umd.cs.findbugs.AppVersion appVersion = origCollection.getCurrentAppVersion();
        edu.umd.cs.findbugs.ProjectStats stats = origCollection.getProjectStats();
        out.print(appVersion.getReleaseName());
        out.print('\t');
        if (formatDates) out.print("\"" + new java.util.Date(appVersion.getTimestamp()) + "\"");
        else out.print(appVersion.getTimestamp());
        out.print('\t');
        out.print(appVersion.getNumClasses());
        out.print('\t');
        out.print(appVersion.getCodeSize());
        out.print('\t');
        out.print(origCollection.getErrors().size());
        out.print('\t');
        out.print(stats.getTotalBugs());
        out.print('\t');
        out.print(stats.getBugsOfPriority(edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY));
        out.print('\t');
        out.print(stats.getBugsOfPriority(edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY));
        out.print('\t');
        out.print(stats.getBugsOfPriority(edu.umd.cs.findbugs.Priorities.LOW_PRIORITY));
        if (fileName != null) {
            out.print('\t');
            out.print(fileName);
        }
        out.println();
    }
}
