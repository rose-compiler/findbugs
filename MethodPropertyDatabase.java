/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A MethodPropertyDatabase keeps track of properties of methods. This is useful
 * for implementing interprocedural analyses.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.interproc;
import edu.umd.cs.findbugs.ba.interproc.*;
import java.io.IOException;
import java.io.Writer;
import org.apache.bcel.Constants;
import edu.umd.cs.findbugs.ba.XFactory;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
import edu.umd.cs.findbugs.util.ClassName;
abstract public class MethodPropertyDatabase<Property> extends edu.umd.cs.findbugs.ba.interproc.PropertyDatabase<edu.umd.cs.findbugs.classfile.MethodDescriptor, Property> {
    public MethodPropertyDatabase() {
    }
    protected edu.umd.cs.findbugs.classfile.MethodDescriptor parseKey(java.lang.String methodStr) throws edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException {
        java.lang.String[] tuple = methodStr.split(",");
        if (tuple.length != 4) throw new edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException("Invalid method tuple: " + methodStr);
        try {
            int accessFlags = java.lang.Integer.parseInt(tuple[3]);
// return
// XFactory.createMethodDescriptor(XFactory.canonicalizeString(tuple[0]),
// XFactory.canonicalizeString( tuple[1]),
// XFactory.canonicalizeString(tuple[2]), accessFlags);
            java.lang.String className = edu.umd.cs.findbugs.ba.XFactory.canonicalizeString(tuple[0]);
            java.lang.String methodName = edu.umd.cs.findbugs.ba.XFactory.canonicalizeString(tuple[1]);
            java.lang.String methodSig = edu.umd.cs.findbugs.ba.XFactory.canonicalizeString(tuple[2]);
            return edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getMethodDescriptor(edu.umd.cs.findbugs.util.ClassName.toSlashedClassName(className),methodName,methodSig,(accessFlags & org.apache.bcel.Constants.ACC_STATIC) != 0);
        }
        catch (java.lang.NumberFormatException e){
            return null;
        }
    }
    protected void writeKey(java.io.Writer writer, edu.umd.cs.findbugs.classfile.MethodDescriptor method) throws java.io.IOException {
        writer.write(method.getClassDescriptor().toDottedClassName());
        writer.write(",");
        writer.write(method.getName());
        writer.write(",");
        writer.write(method.getSignature());
        writer.write(",");
        edu.umd.cs.findbugs.ba.XMethod xMethod = edu.umd.cs.findbugs.ba.XFactory.createXMethod(method);
        writer.write(java.lang.Integer.toString(xMethod.getAccessFlags() & 0xf));
    }
}
