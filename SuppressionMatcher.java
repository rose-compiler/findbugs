/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import edu.umd.cs.findbugs.filter.Matcher;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class SuppressionMatcher extends java.lang.Object implements edu.umd.cs.findbugs.filter.Matcher {
    public SuppressionMatcher() {
    }
    private java.util.Map<edu.umd.cs.findbugs.ClassAnnotation, java.util.Collection<edu.umd.cs.findbugs.WarningSuppressor>> suppressedWarnings = new java.util.HashMap<edu.umd.cs.findbugs.ClassAnnotation, java.util.Collection<edu.umd.cs.findbugs.WarningSuppressor>>();
    private java.util.Map<java.lang.String, java.util.Collection<edu.umd.cs.findbugs.WarningSuppressor>> suppressedPackageWarnings = new java.util.HashMap<java.lang.String, java.util.Collection<edu.umd.cs.findbugs.WarningSuppressor>>();
    int count = 0;
    public void addPackageSuppressor(edu.umd.cs.findbugs.PackageWarningSuppressor suppressor) {
        java.lang.String packageName = suppressor.getPackageName();
        java.util.Collection<edu.umd.cs.findbugs.WarningSuppressor> c = suppressedPackageWarnings.get(packageName);
        if (c == null) {
            c = new java.util.LinkedList<edu.umd.cs.findbugs.WarningSuppressor>();
            suppressedPackageWarnings.put(packageName,c);
        }
        c.add(suppressor);
    }
    public void addSuppressor(edu.umd.cs.findbugs.ClassWarningSuppressor suppressor) {
        edu.umd.cs.findbugs.ClassAnnotation clazz = suppressor.getClassAnnotation().getTopLevelClass();
        java.util.Collection<edu.umd.cs.findbugs.WarningSuppressor> c = suppressedWarnings.get(clazz);
        if (c == null) {
            c = new java.util.LinkedList<edu.umd.cs.findbugs.WarningSuppressor>();
            suppressedWarnings.put(clazz,c);
        }
        c.add(suppressor);
    }
    public int count() {
        return count;
    }
    public boolean match(edu.umd.cs.findbugs.BugInstance b) {
        edu.umd.cs.findbugs.ClassAnnotation clazz = b.getPrimaryClass().getTopLevelClass();
        java.util.Collection<edu.umd.cs.findbugs.WarningSuppressor> c = suppressedWarnings.get(clazz);
        if (c != null) for (edu.umd.cs.findbugs.WarningSuppressor w : c)if (w.match(b)) {
            count++;
            return true;
        }
;
        for (java.util.Collection<edu.umd.cs.findbugs.WarningSuppressor> c2 : suppressedPackageWarnings.values())for (edu.umd.cs.findbugs.WarningSuppressor w : c2){
            if (w.match(b)) {
                count++;
                return true;
            }
        }
;
;
        return false;
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean disabled) throws java.io.IOException {
    }
}
// no-op; these aren't saved to XML
// vim:ts=4
