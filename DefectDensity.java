/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 William Pugh
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Java main application to compute defect density for a bug collection (stored
 * as an XML collection)
 * 
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.PackageStats;
import edu.umd.cs.findbugs.PackageStats.ClassStats;
import edu.umd.cs.findbugs.ProjectStats;
import edu.umd.cs.findbugs.SortedBugCollection;
public class DefectDensity extends java.lang.Object {
    public DefectDensity() {
    }
    private static void printRow(java.lang.Object[] values) {
        for (java.lang.Object s : values){
            java.lang.System.out.print(s);
            java.lang.System.out.print("	");
        }
;
        java.lang.System.out.println();
    }
    public static double density(int bugs, int ncss) {
        if (ncss == 0) return java.lang.Double.NaN;
        long bugsPer10KNCSS = java.lang.Math.round(10000.0 * bugs / ncss);
        return bugsPer10KNCSS / 10.0;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        if (args.length > 1 || (args.length > 0 && "-help".equals(args[0]))) {
            java.lang.System.err.println("Usage: " + edu.umd.cs.findbugs.workflow.DefectDensity.class.getName() + " [<infile>]");
            java.lang.System.exit(1);
        }
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.BugCollection origCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        int argCount = 0;
        if (argCount == args.length) origCollection.readXML(java.lang.System.in);
        else origCollection.readXML(args[argCount]);
        edu.umd.cs.findbugs.ProjectStats stats = origCollection.getProjectStats();
        printRow("kind","name","density/KNCSS","bugs","NCSS");
        double projectDensity = density(stats.getTotalBugs(),stats.getCodeSize());
        printRow("project",origCollection.getCurrentAppVersion().getReleaseName(),projectDensity,stats.getTotalBugs(),stats.getCodeSize());
        for (edu.umd.cs.findbugs.PackageStats p : stats.getPackageStats())if (p.getTotalBugs() > 4) {
            double packageDensity = density(p.getTotalBugs(),p.size());
            if (java.lang.Double.isNaN(packageDensity) || packageDensity < projectDensity) continue;
            printRow("package",p.getPackageName(),packageDensity,p.getTotalBugs(),p.size());
            for (edu.umd.cs.findbugs.PackageStats.ClassStats c : p.getSortedClassStats())if (c.getTotalBugs() > 4) {
                double density = density(c.getTotalBugs(),c.size());
                if (java.lang.Double.isNaN(density) || density < packageDensity) continue;
                printRow("class",c.getName(),density,c.getTotalBugs(),c.size());
            }
;
        }
;
    }
}
