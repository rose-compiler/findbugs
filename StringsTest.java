/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006,2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.util;
import edu.umd.cs.findbugs.util.*;
import junit.framework.TestCase;
public class StringsTest extends junit.framework.TestCase {
    public StringsTest() {
    }
    public static java.lang.String[] escapedStrings = {"a b c 1 2 3 &amp; &lt; &gt; &quot; &apos; \\u0005 \\u0013 &#955; \\\\u0007", "a b c \\\\\\u0005", "a b c \\\\\\\\u0005", "a b c \\\\\\u0005 \\\\\\\\u0013", "\\\\\\", "a b c 1 2 3 &amp; &lt; &gt; &quot; &apos; \\u0005 \\u0013 &#955; \\\\u0007 a b c 1 2 3", null, ""};
// mixed entities/unicode escape sequences
// *even* series of prefixed slashes + \\u -> do escape
// *odd* series of prefixed slashes + \\u -> don't escape
// mixed even/odd prefixed slashes
// make sure slashes work on their own (no double un/escaping)
// make sure that normal characters are handled correctly if they
// appear after escapes
// escaping a null string should be safe
// an empty string should be safe too
    public static java.lang.String[] unescapedStrings = {"a b c 1 2 3 & < > \" '   λ \\\\u0007", "a b c \\\\", "a b c \\\\\\\\u0005", "a b c \\\\ \\\\\\\\u0013", "\\\\\\", "a b c 1 2 3 & < > \" '   λ \\\\u0007 a b c 1 2 3", null, ""};
    public void testEscapeXml() {
        assert (escapedStrings.length == unescapedStrings.length);
        for (int i = 0; i < unescapedStrings.length; i++) {
            if (unescapedStrings[i] == null) {
                assert (edu.umd.cs.findbugs.util.Strings.escapeXml(unescapedStrings[i]) == null);
            }
            else {
                assert (edu.umd.cs.findbugs.util.Strings.escapeXml(unescapedStrings[i]).compareTo(escapedStrings[i]) == 0);
            }
        }
    }
    public void testUnescapeXml() {
        assert (escapedStrings.length == unescapedStrings.length);
        for (int i = 0; i < escapedStrings.length; i++) {
            if (escapedStrings[i] == null) {
                assert (edu.umd.cs.findbugs.util.Strings.unescapeXml(escapedStrings[i]) == null);
            }
            else {
                assert (edu.umd.cs.findbugs.util.Strings.unescapeXml(escapedStrings[i]).compareTo(unescapedStrings[i]) == 0);
            }
        }
    }
    public void checkEscapeLFCRBackSlash(java.lang.String expected, java.lang.String argument) {
        assertEquals(argument,expected,edu.umd.cs.findbugs.util.Strings.escapeLFCRBackSlash(argument));
    }
    public void testEscapeLFCRBackSlash() {
        this.checkEscapeLFCRBackSlash("abc","abc");
        this.checkEscapeLFCRBackSlash("\\n","\n");
        this.checkEscapeLFCRBackSlash("\\r","\r");
        this.checkEscapeLFCRBackSlash("\\\\a\\r","\\a\r");
    }
}
