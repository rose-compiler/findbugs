/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce ValueNumberDataflow objects for analyzed methods.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import java.util.Iterator;
import java.util.TreeSet;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DepthFirstSearch;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.ba.vna.LoadedFieldSet;
import edu.umd.cs.findbugs.ba.vna.MergeTree;
import edu.umd.cs.findbugs.ba.vna.ValueNumberAnalysis;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class ValueNumberDataflowFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow> {
/**
     * Constructor.
     */
    public ValueNumberDataflowFactory() {
        super("value number analysis",edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.generic.MethodGen methodGen = this.getMethodGen(analysisCache,descriptor);
        if (methodGen == null) {
            throw new edu.umd.cs.findbugs.ba.MethodUnprofitableException(descriptor);
        }
        edu.umd.cs.findbugs.ba.DepthFirstSearch dfs = this.getDepthFirstSearch(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.vna.LoadedFieldSet loadedFieldSet = this.getLoadedFieldSet(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.vna.ValueNumberAnalysis analysis = new edu.umd.cs.findbugs.ba.vna.ValueNumberAnalysis(methodGen, dfs, loadedFieldSet, edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getLookupFailureCallback());
        analysis.setMergeTree(new edu.umd.cs.findbugs.ba.vna.MergeTree(analysis.getFactory()));
        edu.umd.cs.findbugs.ba.CFG cfg = this.getCFG(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow = new edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow(cfg, analysis);
        vnaDataflow.execute();
        if (edu.umd.cs.findbugs.ba.ClassContext.DUMP_DATAFLOW_ANALYSIS) {
            java.util.TreeSet<edu.umd.cs.findbugs.ba.Location> tree = new java.util.TreeSet<edu.umd.cs.findbugs.ba.Location>();
            for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> locs = cfg.locationIterator(); locs.hasNext(); ) {
                edu.umd.cs.findbugs.ba.Location loc = locs.next();
                tree.add(loc);
            }
            java.lang.System.out.println("\n\nValue number analysis for " + descriptor.getName() + descriptor.getSignature() + " {");
            for (edu.umd.cs.findbugs.ba.Location loc : tree){
                java.lang.System.out.println("\nBefore: " + vnaDataflow.getFactAtLocation(loc));
                java.lang.System.out.println("Location: " + loc);
                java.lang.System.out.println("After: " + vnaDataflow.getFactAfterLocation(loc));
            }
;
            java.lang.System.out.println("}\n");
        }
        return vnaDataflow;
    }
}
