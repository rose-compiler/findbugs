/*
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2000-2002 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "Ant" and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 */
/**
 * FindBugs in Java class files. This task can take the following arguments:
 * <ul>
 * <li>adjustExperimental (boolean default false)
 * <li>adjustPriority (passed to -adjustPriority)
 * <li>applySuppression (exclude any warnings that match a suppression filter
 * supplied in a project file)
 * <li>auxAnalyzepath (class, jar, zip files or directories containing classes
 * to analyze)
 * <li>auxClasspath (classpath or classpathRef)
 * <li>baselineBugs (xml file containing baseline bugs)
 * <li>class (class, jar, zip or directory containing classes to analyze)
 * <li>classpath (classpath for running FindBugs)
 * <li>cloud (cloud id)
 * <li>conserveSpace (boolean - default false)</li>
 * <li>debug (boolean default false)
 * <li>effort (enum min|default|max)</li>
 * <li>excludeFilter (filter filename)
 * <li>failOnError (boolean - default false)
 * <li>home (findbugs install dir)
 * <li>includeFilter (filter filename)
 * <li>maxRank (maximum rank issue to be reported)
 * <li>jvm (Set the command used to start the VM)
 * <li>jvmargs (any additional jvm arguments)
 * <li>omitVisitors (collection - comma seperated)
 * <li>onlyAnalyze (restrict analysis to find bugs to given comma-separated list
 * of classes and packages - See the textui argument description for details)
 * <li>output (enum text|xml|xml:withMessages|html - default xml)
 * <li>outputFile (name of output file to create)
 * <li>noClassOk (boolean default false)
 * <li>pluginList (list of plugin Jar files to load)
 * <li>projectFile (project filename)
 * <li>projectName (project name, for display in generated HTML)
 * <li>quietErrors (boolean - default false)
 * <li>relaxed (boolean - default false)
 * <li>reportLevel (enum experimental|low|medium|high)
 * <li>sort (boolean default true)
 * <li>stylesheet (name of stylesheet to generate HTML: default is
 * "default.xsl")
 * <li>systemProperty (a system property to set)
 * <li>timestampNow (boolean - default false)
 * <li>visitors (collection - comma seperated)
 * <li>chooseVisitors (selectively enable/disable visitors)
 * <li>workHard (boolean default false)
 * </ul>
 * Of these arguments, the <b>home</b> is required. <b>projectFile</b> is
 * required if nested &lt;class&gt; or &lt;auxAnalyzepath&gt elements are not
 * specified. the &lt;class&gt; tag defines the location of either a class, jar
 * file, zip file, or directory containing classes.
 * <p>
 * 
 * @author Mike Fagan <a href="mailto:mfagan@tde.com">mfagan@tde.com</a>
 * @author Michael Tamm <a
 *         href="mailto:mail@michaeltamm.de">mail@michaeltamm.de</a>
 * @author Scott Wolk
 * @version $Revision: 1.56 $
 * 
 * @since Ant 1.5
 * 
 * @ant.task category="utility"
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.Reference;
import edu.umd.cs.findbugs.ExitCodes;
public class FindBugsTask extends edu.umd.cs.findbugs.anttask.AbstractFindBugsTask {
// define the inner class to store class locations
    public static class ClassLocation extends java.lang.Object {
        public ClassLocation() {
        }
        java.io.File classLocation = null;
        public void setLocation(java.io.File location) {
            classLocation = location;
        }
        public java.io.File getLocation() {
            return classLocation;
        }
        public java.lang.String toString() {
            return classLocation != null ? classLocation.toString() : "";
        }
    }
    private java.lang.String effort;
    private boolean conserveSpace;
    private boolean sorted = true;
    private boolean timestampNow = true;
    private boolean quietErrors;
    private java.lang.String warningsProperty;
    private java.lang.String cloudId;
    private int maxRank;
    private java.lang.String projectName;
    private boolean workHard;
    private boolean relaxed;
    private boolean adjustExperimental;
    private java.lang.String adjustPriority;
    private java.io.File projectFile;
    private java.io.File baselineBugs;
    private boolean applySuppression;
    private java.io.File excludeFile;
    private java.io.File includeFile;
    private org.apache.tools.ant.types.Path auxClasspath;
    private org.apache.tools.ant.types.Path auxAnalyzepath;
    private org.apache.tools.ant.types.Path sourcePath;
    private java.lang.String outputFormat = "xml";
    private java.lang.String reportLevel;
    private java.lang.String visitors;
    private java.lang.String chooseVisitors;
    private java.lang.String omitVisitors;
    private java.lang.String outputFileName;
    private java.lang.String stylesheet;
    private java.util.List<edu.umd.cs.findbugs.anttask.FindBugsTask.ClassLocation> classLocations = new java.util.ArrayList<edu.umd.cs.findbugs.anttask.FindBugsTask.ClassLocation>();
    private java.lang.String onlyAnalyze;
    private boolean noClassOk;
    private java.util.List<org.apache.tools.ant.types.FileSet> filesets = new java.util.ArrayList<org.apache.tools.ant.types.FileSet>();
    public FindBugsTask() {
        super("edu.umd.cs.findbugs.FindBugs2");
    }
/**
     * Set the workHard flag.
     * 
     * @param workHard
     *            true if we want findbugs to run with workHard option enabled
     */
    public void setWorkHard(boolean workHard) {
        this.workHard = workHard;
    }
/**
     * Set the noClassOk flag.
     * 
     * @param noClassOk
     *            true if we should generate no-error output if no classfiles
     *            are specified
     */
    public void setNoClassOk(boolean noClassOk) {
        this.noClassOk = noClassOk;
    }
/**
     * Set the relaxed flag.
     * 
     * @param relaxed
     *            true if we want findbugs to run with relaxed option enabled
     */
    public void setRelaxed(boolean relaxed) {
        this.relaxed = relaxed;
    }
/**
     * Set the adjustExperimental flag
     * 
     * @param adjustExperimental
     *            true if we want experimental bug patterns to have lower
     *            priority
     */
    public void setAdjustExperimental(boolean adjustExperimental) {
        this.adjustExperimental = adjustExperimental;
    }
    public void setAdjustPriority(java.lang.String adjustPriorityString) {
        this.adjustPriority = adjustPriorityString;
    }
/**
     * Set the specific visitors to use
     */
    public void setVisitors(java.lang.String commaSeperatedString) {
        this.visitors = commaSeperatedString;
    }
/**
     * Set the specific visitors to use
     */
    public void setChooseVisitors(java.lang.String commaSeperatedString) {
        this.chooseVisitors = commaSeperatedString;
    }
/**
     * Set the specific visitors to use
     */
    public void setOmitVisitors(java.lang.String commaSeperatedString) {
        this.omitVisitors = commaSeperatedString;
    }
/**
     * Set the output format
     */
    public void setOutput(java.lang.String format) {
        this.outputFormat = format;
    }
/**
     * Set the stylesheet filename for HTML generation.
     */
    public void setStylesheet(java.lang.String stylesheet) {
        this.stylesheet = stylesheet;
    }
/**
     * Set the report level
     */
    public void setReportLevel(java.lang.String level) {
        this.reportLevel = level;
    }
/**
     * Set the sorted flag
     */
    public void setSort(boolean flag) {
        this.sorted = flag;
    }
/**
     * Set the timestampNow flag
     */
    public void setTimestampNow(boolean flag) {
        this.timestampNow = flag;
    }
/**
     * Set the quietErrors flag
     */
    public void setQuietErrors(boolean flag) {
        this.quietErrors = flag;
    }
/**
     * Set the quietErrors flag
     */
    public void setApplySuppression(boolean flag) {
        this.applySuppression = flag;
    }
/**
     * Tells this task to set the property with the given name to "true" when
     * bugs were found.
     */
    public void setWarningsProperty(java.lang.String name) {
        this.warningsProperty = name;
    }
/**
     * Set effort level.
     * 
     * @param effort
     *            the effort level
     */
    public void setEffort(java.lang.String effort) {
        this.effort = effort;
    }
    public void setCloud(java.lang.String cloudId) {
        this.cloudId = cloudId.trim();
    }
    public void setMaxRank(int maxRank) {
        this.maxRank = maxRank;
    }
/**
     * Set project name
     * 
     * @param projectName
     *            the project name
     */
    public void setProjectName(java.lang.String projectName) {
        this.projectName = projectName;
    }
/**
     * Set the conserveSpace flag.
     */
    public void setConserveSpace(boolean flag) {
        this.conserveSpace = flag;
    }
/**
     * Set the exclude filter file
     */
    public void setExcludeFilter(java.io.File filterFile) {
        if (filterFile != null && filterFile.length() > 0) this.excludeFile = filterFile;
        else this.excludeFile = null;
    }
/**
     * Set the exclude filter file
     */
    public void setIncludeFilter(java.io.File filterFile) {
        if (filterFile != null && filterFile.length() > 0) this.includeFile = filterFile;
        else this.includeFile = null;
    }
/**
     * Set the exclude filter file
     */
    public void setBaselineBugs(java.io.File baselineBugs) {
        if (baselineBugs != null && baselineBugs.length() > 0) this.baselineBugs = baselineBugs;
        else this.baselineBugs = null;
    }
/**
     * Set the project file
     */
    public void setProjectFile(java.io.File projectFile) {
        this.projectFile = projectFile;
    }
/**
     * the auxclasspath to use.
     */
    public void setAuxClasspath(org.apache.tools.ant.types.Path src) {
        boolean nonEmpty = false;
        java.lang.String[] elementList = src.list();
        for (java.lang.String anElementList : elementList){
            if ( !anElementList.equals("")) {
                nonEmpty = true;
                break;
            }
        }
;
        if (nonEmpty) {
            if (auxClasspath == null) {
                auxClasspath = src;
            }
            else {
                auxClasspath.append(src);
            }
        }
    }
/**
     * Path to use for auxclasspath.
     */
    public org.apache.tools.ant.types.Path createAuxClasspath() {
        if (auxClasspath == null) {
            auxClasspath = new org.apache.tools.ant.types.Path(this.getProject());
        }
        return auxClasspath.createPath();
    }
/**
     * Adds a reference to a sourcepath defined elsewhere.
     */
    public void setAuxClasspathRef(org.apache.tools.ant.types.Reference r) {
        org.apache.tools.ant.types.Path path = this.createAuxClasspath();
        path.setRefid(r);
        path.toString();
    }
// Evaluated for its side-effects (throwing a
// BuildException)
/**
     * the auxAnalyzepath to use.
     */
    public void setAuxAnalyzepath(org.apache.tools.ant.types.Path src) {
        boolean nonEmpty = false;
        java.lang.String[] elementList = src.list();
        for (java.lang.String anElementList : elementList){
            if ( !anElementList.equals("")) {
                nonEmpty = true;
                break;
            }
        }
;
        if (nonEmpty) {
            if (auxAnalyzepath == null) {
                auxAnalyzepath = src;
            }
            else {
                auxAnalyzepath.append(src);
            }
        }
    }
/**
     * Path to use for auxAnalyzepath.
     */
    public org.apache.tools.ant.types.Path createAuxAnalyzepath() {
        if (auxAnalyzepath == null) {
            auxAnalyzepath = new org.apache.tools.ant.types.Path(this.getProject());
        }
        return auxAnalyzepath.createPath();
    }
/**
     * Adds a reference to a sourcepath defined elsewhere.
     */
    public void setAuxAnalyzepathRef(org.apache.tools.ant.types.Reference r) {
        this.createAuxAnalyzepath().setRefid(r);
    }
/**
     * the sourcepath to use.
     */
    public void setSourcePath(org.apache.tools.ant.types.Path src) {
        if (sourcePath == null) {
            sourcePath = src;
        }
        else {
            sourcePath.append(src);
        }
    }
/**
     * Path to use for sourcepath.
     */
    public org.apache.tools.ant.types.Path createSourcePath() {
        if (sourcePath == null) {
            sourcePath = new org.apache.tools.ant.types.Path(this.getProject());
        }
        return sourcePath.createPath();
    }
/**
     * Adds a reference to a source path defined elsewhere.
     */
    public void setSourcePathRef(org.apache.tools.ant.types.Reference r) {
        this.createSourcePath().setRefid(r);
    }
/**
     * Add a class location
     */
    public edu.umd.cs.findbugs.anttask.FindBugsTask.ClassLocation createClass() {
        edu.umd.cs.findbugs.anttask.FindBugsTask.ClassLocation cl = new edu.umd.cs.findbugs.anttask.FindBugsTask.ClassLocation();
        classLocations.add(cl);
        return cl;
    }
/**
     * Set name of output file.
     */
    public void setOutputFile(java.lang.String outputFileName) {
        if (outputFileName != null && outputFileName.length() > 0) this.outputFileName = outputFileName;
    }
/**
     * Set the packages or classes to analyze
     */
    public void setOnlyAnalyze(java.lang.String filter) {
        this.onlyAnalyze = filter;
    }
/**
     * Add a nested fileset of classes or jar files.
     */
    public void addFileset(org.apache.tools.ant.types.FileSet fs) {
        filesets.add(fs);
    }
/**
     * Check that all required attributes have been set
     */
    protected void checkParameters() {
        super.checkParameters();
        if (projectFile == null && classLocations.size() == 0 && filesets.size() == 0 && auxAnalyzepath == null) {
            throw new org.apache.tools.ant.BuildException("either projectfile, <class/>, <fileset/> or <auxAnalyzepath/> child elements must be defined for task <" + this.getTaskName() + "/>", this.getLocation());
        }
        if (cloudId != null && cloudId.contains(" ")) throw new org.apache.tools.ant.BuildException("cloudId must not contain spaces: '" + cloudId + "'");
        if (outputFormat != null &&  !(outputFormat.trim().equalsIgnoreCase("xml") || outputFormat.trim().equalsIgnoreCase("xml:withMessages") || outputFormat.trim().equalsIgnoreCase("html") || outputFormat.trim().equalsIgnoreCase("text") || outputFormat.trim().equalsIgnoreCase("xdocs") || outputFormat.trim().equalsIgnoreCase("emacs"))) {
            throw new org.apache.tools.ant.BuildException("output attribute must be either 'text', 'xml', 'html', 'xdocs' or 'emacs' for task <" + this.getTaskName() + "/>", this.getLocation());
        }
        if (reportLevel != null &&  !(reportLevel.trim().equalsIgnoreCase("experimental") || reportLevel.trim().equalsIgnoreCase("low") || reportLevel.trim().equalsIgnoreCase("medium") || reportLevel.trim().equalsIgnoreCase("high"))) {
            throw new org.apache.tools.ant.BuildException("reportlevel attribute must be either 'experimental' or 'low' or 'medium' or 'high' for task <" + this.getTaskName() + "/>", this.getLocation());
        }
// FindBugs allows both, so there's no apparent reason for this check
// if ( excludeFile != null && includeFile != null ) {
// throw new BuildException("only one of excludeFile and includeFile " +
// " attributes may be used in task <" + getTaskName() + "/>",
// getLocation());
// }
        java.util.List<java.lang.String> efforts = java.util.Arrays.asList("min","less","default","more","max");
        if (effort != null &&  !efforts.contains(effort)) {
            throw new org.apache.tools.ant.BuildException("effort attribute must be one of " + efforts);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#beforeExecuteJavaProcess
     * ()
     */
    protected void beforeExecuteJavaProcess() {
        this.log("Running FindBugs...");
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#afterExecuteJavaProcess
     * (int)
     */
    protected void afterExecuteJavaProcess(int rc) {
        if ((rc & edu.umd.cs.findbugs.ExitCodes.ERROR_FLAG) != 0) {
            throw new org.apache.tools.ant.BuildException("Execution of findbugs failed.");
        }
        if ((rc & edu.umd.cs.findbugs.ExitCodes.MISSING_CLASS_FLAG) != 0) {
            this.log("Classes needed for analysis were missing");
        }
        if (warningsProperty != null && (rc & edu.umd.cs.findbugs.ExitCodes.BUGS_FOUND_FLAG) != 0) {
            this.getProject().setProperty(warningsProperty,"true");
        }
        if (outputFileName != null) {
            this.log("Output saved to " + outputFileName);
        }
    }
    protected void configureFindbugsEngine() {
        if (projectName != null) {
            this.addArg("-projectName");
            this.addArg(projectName);
        }
        if (adjustExperimental) {
            this.addArg("-adjustExperimental");
        }
        if (cloudId != null) {
            this.addArg("-cloud");
            this.addArg(cloudId);
        }
        if (conserveSpace) {
            this.addArg("-conserveSpace");
        }
        if (workHard) {
            this.addArg("-workHard");
        }
        if (effort != null) {
            this.addArg("-effort:" + effort);
        }
        if (maxRank > 0 && maxRank < 20) {
            this.addArg("-maxRank ");
            this.addArg(java.lang.Integer.toString(maxRank));
        }
        if (adjustPriority != null) {
            this.addArg("-adjustPriority");
            this.addArg(adjustPriority);
        }
        if (sorted) this.addArg("-sortByClass");
        if (timestampNow) this.addArg("-timestampNow");
        if (outputFormat != null &&  !outputFormat.trim().equalsIgnoreCase("text")) {
            outputFormat = outputFormat.trim();
            java.lang.String outputArg = "-";
            int colon = outputFormat.indexOf('\u003a');
            if (colon >= 0) {
                outputArg += outputFormat.substring(0,colon).toLowerCase();
                outputArg += ":";
                outputArg += outputFormat.substring(colon + 1);
            }
            else {
                outputArg += outputFormat.toLowerCase();
                if (stylesheet != null) {
                    outputArg += ":";
                    outputArg += stylesheet.trim();
                }
            }
            this.addArg(outputArg);
        }
        if (quietErrors) this.addArg("-quiet");
        if (reportLevel != null) this.addArg("-" + reportLevel.trim().toLowerCase());
        if (projectFile != null) {
            this.addArg("-project");
            this.addArg(projectFile.getPath());
        }
        if (applySuppression) {
            this.addArg("-applySuppression");
        }
        if (baselineBugs != null) {
            this.addArg("-excludeBugs");
            this.addArg(baselineBugs.getPath());
        }
        if (excludeFile != null) {
            this.addArg("-exclude");
            this.addArg(excludeFile.getPath());
        }
        if (includeFile != null) {
            this.addArg("-include");
            this.addArg(includeFile.getPath());
        }
        if (visitors != null) {
            this.addArg("-visitors");
            this.addArg(visitors);
        }
        if (omitVisitors != null) {
            this.addArg("-omitVisitors");
            this.addArg(omitVisitors);
        }
        if (chooseVisitors != null) {
            this.addArg("-chooseVisitors");
            this.addArg(chooseVisitors);
        }
        if (auxClasspath != null) {
            try {
// Try to dereference the auxClasspath.
// If it throws an exception, we know it
// has an invalid path entry, so we complain
// and tolerate it.
                java.lang.String unreadReference = auxClasspath.toString();
                java.lang.String auxClasspathString = auxClasspath.toString();
                if (auxClasspathString.length() > 100) {
                    this.addArg("-auxclasspathFromInput");
                    this.setInputString(auxClasspathString);
                }
                else {
                    this.addArg("-auxclasspath");
                    this.addArg(auxClasspathString);
                }
            }
            catch (java.lang.Throwable t){
                this.log("Warning: auxClasspath " + t + " not found.");
            }
        }
        if (sourcePath != null) {
            this.addArg("-sourcepath");
            this.addArg(sourcePath.toString());
        }
        if (outputFileName != null) {
            this.addArg("-outputFile");
            this.addArg(outputFileName);
        }
        if (relaxed) {
            this.addArg("-relaxed");
        }
        if (noClassOk) {
            this.addArg("-noClassOk");
        }
        if (onlyAnalyze != null) {
            this.addArg("-onlyAnalyze");
            this.addArg(onlyAnalyze);
        }
        this.addArg("-exitcode");
        for (edu.umd.cs.findbugs.anttask.FindBugsTask.ClassLocation classLocation : classLocations){
            this.addArg(classLocation.toString());
        }
;
        for (org.apache.tools.ant.types.FileSet fs : filesets){
            org.apache.tools.ant.DirectoryScanner ds = fs.getDirectoryScanner();
            for (java.lang.String fileName : ds.getIncludedFiles()){
                java.io.File file = new java.io.File(ds.getBasedir(), fileName);
                this.addArg(file.toString());
            }
;
        }
;
        if (auxAnalyzepath != null) {
            java.lang.String[] result = auxAnalyzepath.toString().split(java.io.File.pathSeparator);
            for (int x = 0; x < result.length; x++) {
                this.addArg(result[x]);
            }
        }
    }
}
// vim:ts=4
