/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author Keith
 */
package edu.umd.cs.findbugs.cloud;
import edu.umd.cs.findbugs.cloud.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import edu.umd.cs.findbugs.BugAnnotation;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugRanker;
import edu.umd.cs.findbugs.ClassAnnotation;
import edu.umd.cs.findbugs.Project;
import edu.umd.cs.findbugs.PropertyBundle;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.ba.SourceFile;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.cloud.Cloud.UserDesignation;
import edu.umd.cs.findbugs.util.Util;
public class BugFilingCommentHelper extends java.lang.Object {
// ================================= end of public methods
// ====================================
// ==================================== inner classes
// =========================================
    public static class SourceLine extends java.lang.Object {
        final public int line;
        final public java.lang.String text;
        public SourceLine(int line, java.lang.String text) {
            super();
            this.line = line;
            this.text = text;
        }
    }
    final private edu.umd.cs.findbugs.cloud.Cloud cloud;
    final private java.lang.String BUG_NOTE;
    final private java.lang.String POSTMORTEM_NOTE;
    final private int POSTMORTEM_RANK;
    public BugFilingCommentHelper(edu.umd.cs.findbugs.cloud.Cloud cloud) {
        super();
        this.cloud = cloud;
        edu.umd.cs.findbugs.PropertyBundle properties = cloud.getPlugin().getProperties();
        BUG_NOTE = properties.getProperty("findbugs.bugnote");
        POSTMORTEM_NOTE = properties.getProperty("findbugs.postmortem.note");
        POSTMORTEM_RANK = properties.getInt("findbugs.postmortem.maxRank",4);
    }
    public java.lang.String getBugReportSummary(edu.umd.cs.findbugs.BugInstance b) {
        return b.getMessageWithoutPrefix() + " in " + b.getPrimaryClass().getSourceFileName();
    }
    public java.lang.String getBugReportText(edu.umd.cs.findbugs.BugInstance b) {
        return this.getBugReportHead(b) + this.getBugReportSourceCode(b) + this.getLineTerminatedUserEvaluation(b) + this.getBugPatternExplanation(b) + this.getBugReportTail(b);
    }
    public java.lang.String getBugReportSourceCode(edu.umd.cs.findbugs.BugInstance b) {
        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter out = new java.io.PrintWriter(stringWriter);
        edu.umd.cs.findbugs.ClassAnnotation primaryClass = b.getPrimaryClass();
        int firstLine = java.lang.Integer.MAX_VALUE;
        int lastLine = java.lang.Integer.MIN_VALUE;
        for (edu.umd.cs.findbugs.BugAnnotation a : b.getAnnotations())if (a instanceof edu.umd.cs.findbugs.SourceLineAnnotation) {
            edu.umd.cs.findbugs.SourceLineAnnotation s = (edu.umd.cs.findbugs.SourceLineAnnotation) (a) ;
            if (s.getClassName().equals(primaryClass.getClassName()) && s.getStartLine() > 0) {
                firstLine = java.lang.Math.min(firstLine,s.getStartLine());
                lastLine = java.lang.Math.max(lastLine,s.getEndLine());
            }
        }
;
        edu.umd.cs.findbugs.SourceLineAnnotation primarySource = primaryClass.getSourceLines();
        if (primarySource.isSourceFileKnown() && firstLine >= 1 && firstLine <= lastLine && lastLine - firstLine < 50) {
            java.io.BufferedReader in = null;
            try {
                edu.umd.cs.findbugs.Project project = cloud.getBugCollection().getProject();
                edu.umd.cs.findbugs.ba.SourceFile sourceFile = project.getSourceFinder().findSourceFile(primarySource);
                in = edu.umd.cs.findbugs.charsets.UTF8.bufferedReader(sourceFile.getInputStream());
                int lineNumber = 1;
                java.lang.String commonWhiteSpace = null;
                java.util.List<edu.umd.cs.findbugs.cloud.BugFilingCommentHelper.SourceLine> source = new java.util.ArrayList<edu.umd.cs.findbugs.cloud.BugFilingCommentHelper.SourceLine>();
                while (lineNumber <= lastLine + 4) {
                    java.lang.String txt = in.readLine();
                    if (txt == null) break;
                    if (lineNumber >= firstLine - 4) {
                        java.lang.String trimmed = txt.trim();
                        if (trimmed.length() == 0) {
                            if (lineNumber > lastLine) break;
                            txt = trimmed;
                        }
                        source.add(new edu.umd.cs.findbugs.cloud.BugFilingCommentHelper.SourceLine(lineNumber, txt));
                        commonWhiteSpace = this.commonLeadingWhitespace(commonWhiteSpace,txt);
                    }
                    lineNumber++;
                }
                if (commonWhiteSpace == null) commonWhiteSpace = "";
                out.println("\nRelevant source code:");
                for (edu.umd.cs.findbugs.cloud.BugFilingCommentHelper.SourceLine s : source){
                    if (s.text.length() == 0) out.printf("%5d: %n",s.line);
                    else out.printf("%5d:   %s%n",s.line,s.text.substring(commonWhiteSpace.length()));
                }
;
                out.println();
            }
            catch (java.io.IOException e){
                assert true;
            }
            finally {
                edu.umd.cs.findbugs.util.Util.closeSilently(in);
            }
            out.close();
            return stringWriter.toString();
        }
        return "";
    }
    public java.lang.String getBugReportHead(edu.umd.cs.findbugs.BugInstance b) {
        java.io.StringWriter stringWriter = new java.io.StringWriter();
        java.io.PrintWriter out = new java.io.PrintWriter(stringWriter);
        out.println("Bug report generated from FindBugs");
        out.println(b.getMessageWithoutPrefix());
        out.println();
        edu.umd.cs.findbugs.ClassAnnotation primaryClass = b.getPrimaryClass();
        for (edu.umd.cs.findbugs.BugAnnotation a : b.getAnnotations()){
            if (a == primaryClass) out.println(a);
            else out.println("  " + a.toString(primaryClass));
        }
;
        if (cloud.supportsSourceLinks()) {
            java.net.URL link = cloud.getSourceLink(b);
            if (link != null) {
                out.println();
                out.println(cloud.getSourceLinkToolTip(b) + ": " + link);
                out.println();
            }
        }
        if (BUG_NOTE != null) {
            out.println(BUG_NOTE);
            if (POSTMORTEM_NOTE != null && edu.umd.cs.findbugs.BugRanker.findRank(b) <= POSTMORTEM_RANK && cloud.getConsensusDesignation(b).score() >= 0) {
                out.println(POSTMORTEM_NOTE);
            }
            out.println();
        }
        java.util.Collection<java.lang.String> projects = cloud.getProjects(primaryClass.getClassName());
        if (projects != null &&  !projects.isEmpty()) {
            java.lang.String projectList = projects.toString();
            projectList = projectList.substring(1,projectList.length() - 1);
            out.println("Possibly part of: " + projectList);
            out.println();
        }
        out.close();
        return stringWriter.toString();
    }
    public java.lang.String getBugPatternExplanation(edu.umd.cs.findbugs.BugInstance b) {
        java.lang.String detailPlainText = b.getBugPattern().getDetailPlainText();
        return "Bug pattern explanation:\n" + detailPlainText + "\n\n";
    }
    public java.lang.String getLineTerminatedUserEvaluation(edu.umd.cs.findbugs.BugInstance b) {
        edu.umd.cs.findbugs.cloud.Cloud.UserDesignation designation = cloud.getUserDesignation(b);
        java.lang.String result;
        if (designation != edu.umd.cs.findbugs.cloud.Cloud.UserDesignation.UNCLASSIFIED) result = "Classified as: " + designation.toString() + "\n";
        else result = "";
        java.lang.String eval = cloud.getUserEvaluation(b).trim();
        if (eval.length() > 0) result = result + eval + "\n";
        return result;
    }
    public java.lang.String getBugReportTail(edu.umd.cs.findbugs.BugInstance b) {
        return "\nFindBugs issue identifier (do not modify or remove): " + b.getInstanceHash();
    }
    private java.lang.String commonLeadingWhitespace(java.lang.String soFar, java.lang.String txt) {
        if (txt.length() == 0) return soFar;
        if (soFar == null) return txt;
        soFar = edu.umd.cs.findbugs.util.Util.commonPrefix(soFar,txt);
        for (int i = 0; i < soFar.length(); i++) {
            if ( !java.lang.Character.isWhitespace(soFar.charAt(i))) return soFar.substring(0,i);
        }
        return soFar;
    }
}
