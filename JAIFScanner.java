/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Lexical scanner for external annotation files.
 * 
 * @author David Hovemeyer
 * @see <a
 *      href="http://groups.csail.mit.edu/pag/jsr308/annotation-file-utilities/">Annotation
 *      File Utilities/</a>
 */
package edu.umd.cs.findbugs.jaif;
import edu.umd.cs.findbugs.jaif.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class JAIFScanner extends java.lang.Object {
    static class TokenPattern extends java.lang.Object {
        private java.util.regex.Pattern pattern;
        private edu.umd.cs.findbugs.jaif.JAIFTokenKind kind;
        public TokenPattern(java.lang.String regex, edu.umd.cs.findbugs.jaif.JAIFTokenKind kind) {
            super();
            this.pattern = java.util.regex.Pattern.compile("^" + regex);
            this.kind = kind;
        }
        public edu.umd.cs.findbugs.jaif.JAIFTokenKind getKind(java.lang.String lexeme) {
            return kind;
        }
        public java.util.regex.Pattern getPattern() {
            return pattern;
        }
    }
// See http://java.sun.com/docs/books/jls/third_edition/html/lexical.html
// Hexidecimal floating-point literals are not implemented.
// Unicode escapes are not implemented (but could be implemented in the
// fillLineBuf() method).
    final private static java.lang.String ID_START = "[@A-Za-z_\\$]";
    final private static java.lang.String ID_REST = "[A-Za-z0-9_\\$]";
    final private static java.lang.String DIGIT = "[0-9]";
    final private static java.lang.String DIGITS = DIGIT + "+";
    final private static java.lang.String DIGITS_OPT = DIGIT + "*";
    final private static java.lang.String SIGN_OPT = "[+-]?";
    final private static java.lang.String DOT = "\\.";
    final private static java.lang.String EXP_PART = "([Ee]" + SIGN_OPT + DIGITS + ")";
    final private static java.lang.String EXP_PART_OPT = EXP_PART + "?";
    final private static java.lang.String FLOAT_TYPE_SUFFIX = "[FfDd]";
    final private static java.lang.String FLOAT_TYPE_SUFFIX_OPT = FLOAT_TYPE_SUFFIX + "?";
    final private static java.lang.String OCTAL_DIGITS = "[0-7]+";
    final private static java.lang.String HEX_SIGNIFIER = "0[Xx]";
    final private static java.lang.String HEX_DIGITS = "[0-9A-Fa-f]+";
    final private static java.lang.String INT_TYPE_SUFFIX_OPT = "[Ll]?";
// anything other
    final private static java.lang.String INPUT_CHAR = "[^\\\\\\\"]";
// than backslash or
// double-quote
// character
    final private static java.lang.String OCT_ESCAPE = "([0-7]|[0-3]?[0-7][0-7])";
    final private static java.lang.String ESCAPE_SEQ = "(\\\\[btnfr\"'\\\\]|\\\\" + OCT_ESCAPE + ")";
    final private static java.lang.String STRING_CHARS_OPT = "(" + INPUT_CHAR + "|" + ESCAPE_SEQ + ")*";
    final private static edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern[] TOKEN_PATTERNS = {new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(":", edu.umd.cs.findbugs.jaif.JAIFTokenKind.COLON), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern("\\(", edu.umd.cs.findbugs.jaif.JAIFTokenKind.LPAREN), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern("\\)", edu.umd.cs.findbugs.jaif.JAIFTokenKind.RPAREN), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(",", edu.umd.cs.findbugs.jaif.JAIFTokenKind.COMMA), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern("=", edu.umd.cs.findbugs.jaif.JAIFTokenKind.EQUALS), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(ID_START + "(" + ID_REST + ")*", edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(DIGITS + DOT + DIGITS_OPT + EXP_PART_OPT + FLOAT_TYPE_SUFFIX_OPT, edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(DOT + DIGITS + EXP_PART_OPT + FLOAT_TYPE_SUFFIX_OPT, edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(DIGITS + EXP_PART + FLOAT_TYPE_SUFFIX_OPT, edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(DIGITS + EXP_PART_OPT + FLOAT_TYPE_SUFFIX, edu.umd.cs.findbugs.jaif.JAIFTokenKind.FLOATING_POINT_LITERAL), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(DOT, edu.umd.cs.findbugs.jaif.JAIFTokenKind.DOT), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern("0" + OCTAL_DIGITS + INT_TYPE_SUFFIX_OPT, edu.umd.cs.findbugs.jaif.JAIFTokenKind.OCTAL_LITERAL), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(HEX_SIGNIFIER + HEX_DIGITS + INT_TYPE_SUFFIX_OPT, edu.umd.cs.findbugs.jaif.JAIFTokenKind.HEX_LITERAL), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern(DIGITS + INT_TYPE_SUFFIX_OPT, edu.umd.cs.findbugs.jaif.JAIFTokenKind.DECIMAL_LITERAL), new edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern("\"" + STRING_CHARS_OPT + "\"", edu.umd.cs.findbugs.jaif.JAIFTokenKind.STRING_LITERAL)};
// Misc. syntax
// Identifiers and keywords
// FP literals
// This must come after the FP literal patterns
// Integer literals
// String literals
    private java.io.BufferedReader reader;
    private edu.umd.cs.findbugs.jaif.JAIFToken next;
    private java.lang.String lineBuf;
    private int lineNum;
/**
     * @param reader
     */
    public JAIFScanner(java.io.Reader reader) {
        super();
        this.reader = new java.io.BufferedReader(reader);
        this.lineNum = 0;
    }
    public int getLineNumber() {
        return lineNum;
    }
    public edu.umd.cs.findbugs.jaif.JAIFToken nextToken() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        if (next == null) {
            this.fetchToken();
        }
        edu.umd.cs.findbugs.jaif.JAIFToken result = next;
        next = null;
        return result;
    }
    public edu.umd.cs.findbugs.jaif.JAIFToken peekToken() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        if (next == null) {
            this.fetchToken();
        }
        return next;
    }
    public boolean atEOF() throws java.io.IOException {
        this.fillLineBuf();
        return lineBuf == null;
    }
    private void fillLineBuf() throws java.io.IOException {
        if (lineBuf == null) {
            lineBuf = reader.readLine();
            if (lineBuf != null) {
                ++lineNum;
            }
        }
    }
    private boolean isHorizWhitespace(char c) {
        return c == '\u0020' || c == '\t';
    }
    private void fetchToken() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        assert next == null;
        this.fillLineBuf();
        if (lineBuf == null) {
            throw new edu.umd.cs.findbugs.jaif.JAIFSyntaxException(this, "Unexpected end of file");
        }
// Strip leading whitespace, if any
        int wsCount = 0;
        while (wsCount < lineBuf.length() && this.isHorizWhitespace(lineBuf.charAt(wsCount))) {
            wsCount++;
        }
        if (wsCount > 0) {
            lineBuf = lineBuf.substring(wsCount);
        }
// System.out.println("Consumed " + wsCount +
// " characters of horizontal whitespace");
        if (lineBuf.equals("")) {
            next = new edu.umd.cs.findbugs.jaif.JAIFToken(edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE, "\n", lineNum);
            lineBuf = null;
// Reached end of line.
            return;
        }
// Try matching line buffer against all known patterns
// until we fine one that matches.
        for (edu.umd.cs.findbugs.jaif.JAIFScanner.TokenPattern tokenPattern : TOKEN_PATTERNS){
            java.util.regex.Matcher m = tokenPattern.getPattern().matcher(lineBuf);
            if (m.find()) {
                java.lang.String lexeme = m.group();
                lineBuf = lineBuf.substring(lexeme.length());
                next = new edu.umd.cs.findbugs.jaif.JAIFToken(tokenPattern.getKind(lexeme), lexeme, lineNum);
                return;
            }
        }
;
        throw new edu.umd.cs.findbugs.jaif.JAIFSyntaxException(this, "Unrecognized token (trying to match text `" + lineBuf + "')");
    }
}
