/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce the ClassInfo for a loaded class. We parse just
 * enough information from the classfile to get the needed information.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine;
import edu.umd.cs.findbugs.classfile.engine.*;
import edu.umd.cs.findbugs.asm.FBClassReader;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.ClassNameMismatchException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.IClassAnalysisEngine;
import edu.umd.cs.findbugs.classfile.analysis.ClassData;
import edu.umd.cs.findbugs.classfile.analysis.ClassInfo;
public class ClassInfoAnalysisEngine extends java.lang.Object implements edu.umd.cs.findbugs.classfile.IClassAnalysisEngine<edu.umd.cs.findbugs.ba.XClass> {
    public ClassInfoAnalysisEngine() {
    }
/*
     * private static final boolean USE_ASM_CLASS_PARSER =
     * SystemProperties.getBoolean("findbugs.classparser.asm"); static { if
     * (USE_ASM_CLASS_PARSER) { System.out.println("Using ClassParserUsingASM");
     * } }
     */
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.classfile.analysis.ClassInfo analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.ClassDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        if (descriptor instanceof edu.umd.cs.findbugs.classfile.analysis.ClassInfo) return (edu.umd.cs.findbugs.classfile.analysis.ClassInfo) (descriptor) ;
        edu.umd.cs.findbugs.classfile.analysis.ClassData classData;
        try {
            classData = analysisCache.getClassAnalysis(edu.umd.cs.findbugs.classfile.analysis.ClassData.class,descriptor);
        }
        catch (edu.umd.cs.findbugs.classfile.MissingClassException e){
            if ( !descriptor.getSimpleName().equals("packageinfovc8")) throw e;
            edu.umd.cs.findbugs.classfile.analysis.ClassInfo.Builder builder = new edu.umd.cs.findbugs.classfile.analysis.ClassInfo.Builder();
            builder.setClassDescriptor(descriptor);
            builder.setAccessFlags(1536);
            return builder.build();
        }
// Read the class info
        edu.umd.cs.findbugs.asm.FBClassReader reader = analysisCache.getClassAnalysis(edu.umd.cs.findbugs.asm.FBClassReader.class,descriptor);
        edu.umd.cs.findbugs.classfile.engine.ClassParserInterface parser = new edu.umd.cs.findbugs.classfile.engine.ClassParserUsingASM(reader, descriptor, classData.getCodeBaseEntry());
        edu.umd.cs.findbugs.classfile.analysis.ClassInfo.Builder classInfoBuilder = new edu.umd.cs.findbugs.classfile.analysis.ClassInfo.Builder();
        parser.parse(classInfoBuilder);
        edu.umd.cs.findbugs.classfile.analysis.ClassInfo classInfo = classInfoBuilder.build();
        if ( !classInfo.getClassDescriptor().equals(descriptor)) {
            throw new edu.umd.cs.findbugs.classfile.ClassNameMismatchException(descriptor, classInfo.getClassDescriptor(), classData.getCodeBaseEntry());
        }
        return classInfo;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#registerWith(edu.umd.cs
     * .findbugs.classfile.IAnalysisCache)
     */
    public void registerWith(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache) {
        analysisCache.registerClassAnalysisEngine(edu.umd.cs.findbugs.ba.XClass.class,this);
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.IAnalysisEngine#canRecompute()
     */
    public boolean canRecompute() {
// ClassInfo objects serve as XClass objects,
// which we want interned. So, they are never purged from the cache.
        return false;
    }
}
