/** A synthetic instruction that converts a reference to a boolean value,
 * translating null to 1 (true), and any nonnull value to 0 (false).
 * 
 */
package edu.umd.cs.findbugs.bcel.generic;
import edu.umd.cs.findbugs.bcel.generic.*;
public class NULL2Z extends edu.umd.cs.findbugs.bcel.generic.NullnessConversationInstruction {
    public NULL2Z() {
        super(org.apache.bcel.Constants.IMPDEP1);
    }
}
