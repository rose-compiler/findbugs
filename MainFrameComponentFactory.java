package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolTip;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import edu.umd.cs.findbugs.BugAnnotation;
import edu.umd.cs.findbugs.BugAnnotationWithSourceLines;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.ClassAnnotation;
import edu.umd.cs.findbugs.L10N;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.SystemProperties;
import edu.umd.cs.findbugs.cloud.Cloud;
import edu.umd.cs.findbugs.util.LaunchBrowser;
public class MainFrameComponentFactory extends java.lang.Object implements java.io.Serializable {
// statusBar.setBackground(Color.WHITE);
//        mainFrame.getSummaryTopPanel().setMinimumSize(new Dimension(fontSize * 50, fontSize * 5));
// JPanel temp = new JPanel(new BorderLayout());
// temp.add(summaryTopPanel, BorderLayout.CENTER);
/**
     * Creates the source code panel, but does not put anything in it.
     */
// add the buttons
/**
     * Sets the title of the source tabs for either docking or non-docking
     * versions.
     */
/**
     * Creates bug summary component. If obj is a string will create a JLabel
     * with that string as it's text and return it. If obj is an annotation will
     * return a JLabel with the annotation's toString(). If that annotation is a
     * SourceLineAnnotation or has a SourceLineAnnotation connected to it and
     * the source file is available will attach a listener to the label.
     *
     * @return
     */
// noinspection ConstantConditions
// If an exception was encountered while initializing, this may
// be because of a bug in the particular look-and-feel selected
// (as in sourceforge bug 1899648). In an attempt to recover
// gracefully, this code reverts to the cross-platform look-
// and-feel and attempts again to initialize the layout.
// Sets the size of the tooltip to match the rest of the GUI. -
// Kristin
// This will be thrown first if the OSXAdapter is loaded on
// a system without the EAWT
// because OSXAdapter extends ApplicationAdapter in its def
// This shouldn't be reached; if there's a problem with the
// OSXAdapter we should get the
// above NoClassDefFoundError first.
/**
     * Listens for when cursor is over the label and when it is clicked. When
     * the cursor is over the label will make the label text blue and the cursor
     * the hand cursor. When clicked will take the user to the source code tab
     * and to the lines of code connected to the SourceLineAnnotation.
     *
     * @author Kristin Stephens
     *
     */
    private class BugSummaryMouseListener extends java.awt.event.MouseAdapter {
        final private edu.umd.cs.findbugs.BugInstance bugInstance;
        final private javax.swing.JLabel label;
        final private edu.umd.cs.findbugs.SourceLineAnnotation note;
        public BugSummaryMouseListener(edu.umd.cs.findbugs.BugInstance bugInstance, javax.swing.JLabel label, edu.umd.cs.findbugs.SourceLineAnnotation link) {
            super();
            this.bugInstance = bugInstance;
            this.label = label;
            this.note = link;
        }
        public void mouseClicked(java.awt.event.MouseEvent e) {
            mainFrame.getSourceCodeDisplayer().displaySource(bugInstance,note);
        }
        public void mouseEntered(java.awt.event.MouseEvent e) {
            label.setForeground(java.awt.Color.blue);
            mainFrame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        }
        public void mouseExited(java.awt.event.MouseEvent e) {
            label.setForeground(java.awt.Color.black);
            mainFrame.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        }
    }
    private static class InitializeGUI extends java.lang.Object implements java.lang.Runnable {
        private edu.umd.cs.findbugs.gui2.MainFrame mainFrame;
        public InitializeGUI(final edu.umd.cs.findbugs.gui2.MainFrame mainFrame) {
            super();
            this.mainFrame = mainFrame;
        }
        public void run() {
            mainFrame.setTitle("FindBugs");
            if (edu.umd.cs.findbugs.gui2.MainFrame.USE_WINDOWS_LAF && java.lang.System.getProperty("os.name").toLowerCase().contains("windows")) {
                try {
                    javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
                }
                catch (java.lang.Exception e){
                    LOGGER.log(java.util.logging.Level.SEVERE,"Could not load Windows Look&Feel",e);
                }
            }
            try {
                mainFrame.getGuiLayout().initialize();
            }
            catch (java.lang.Exception e){
                if ( !javax.swing.UIManager.getLookAndFeel().getName().equals("Metal")) {
                    java.lang.System.err.println("Exception caught initializing GUI; reverting to CrossPlatformLookAndFeel");
                    try {
                        javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
                    }
                    catch (java.lang.Exception e2){
                        java.lang.System.err.println("Exception while setting CrossPlatformLookAndFeel: " + e2);
                        throw new java.lang.Error(e2);
                    }
                    mainFrame.getGuiLayout().initialize();
                }
                else {
                    throw new java.lang.Error(e);
                }
            }
            mainFrame.mainFrameTree.setBugPopupMenu(mainFrame.mainFrameTree.createBugPopupMenu());
            mainFrame.mainFrameTree.setBranchPopupMenu(mainFrame.mainFrameTree.createBranchPopUpMenu());
            mainFrame.updateStatusBar();
            java.awt.Rectangle bounds = edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getFrameBounds();
            if (bounds != null) mainFrame.setBounds(bounds);
            mainFrame.setExtendedState(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getExtendedWindowState());
            java.awt.Toolkit.getDefaultToolkit().setDynamicLayout(true);
            mainFrame.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
            mainFrame.setJMenuBar(mainFrame.mainFrameMenu.createMainMenuBar());
            mainFrame.setVisible(true);
            mainFrame.getMainFrameLoadSaveHelper().initialize();
            javax.swing.JToolTip tempToolTip = mainFrame.mainFrameTree.getTableheader().createToolTip();
            javax.swing.UIManager.put("ToolTip.font",new javax.swing.plaf.FontUIResource(tempToolTip.getFont().deriveFont(edu.umd.cs.findbugs.gui2.Driver.getFontSize())));
            this.setupOSX();
            java.lang.String loadFromURL = edu.umd.cs.findbugs.SystemProperties.getOSDependentProperty("findbugs.loadBugsFromURL");
            if (loadFromURL != null) {
                try {
                    loadFromURL = edu.umd.cs.findbugs.SystemProperties.rewriteURLAccordingToProperties(loadFromURL);
                    java.net.URL url = new java.net.URL(loadFromURL);
                    mainFrame.getMainFrameLoadSaveHelper().loadAnalysis(url);
                }
                catch (java.net.MalformedURLException e1){
                    javax.swing.JOptionPane.showMessageDialog(mainFrame,"Error loading " + loadFromURL);
                }
            }
            mainFrame.addWindowListener(new java.awt.event.WindowAdapter() {
                public void windowClosing(java.awt.event.WindowEvent e) {
                    mainFrame.callOnClose();
                }
            });
            edu.umd.cs.findbugs.gui2.Driver.removeSplashScreen();
            mainFrame.waitForMainFrameInitialized();
        }
        private void setupOSX() {
            if (edu.umd.cs.findbugs.gui2.MainFrame.MAC_OS_X) {
                try {
                    mainFrame.mainFrameMenu.initOSX();
                    mainFrame.mainFrameMenu.enablePreferencesMenuItem(true);
                }
                catch (java.lang.NoClassDefFoundError e){
                    java.lang.System.err.println("This version of Mac OS X does not support the Apple EAWT. Application Menu handling has been disabled (" + e + ")");
                }
                catch (java.lang.ClassNotFoundException e){
                    java.lang.System.err.println("This version of Mac OS X does not support the Apple EAWT. Application Menu handling has been disabled (" + e + ")");
                }
                catch (java.lang.Exception e){
                    java.lang.System.err.println("Exception while loading the OSXAdapter: " + e);
                    e.printStackTrace();
                    if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    final private static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.gui2.MainFrameComponentFactory.class.getName());
    final private edu.umd.cs.findbugs.gui2.MainFrame mainFrame;
    private java.net.URL sourceLink;
    private boolean listenerAdded = false;
    public MainFrameComponentFactory(edu.umd.cs.findbugs.gui2.MainFrame mainFrame) {
        super();
        this.mainFrame = mainFrame;
    }
    javax.swing.JPanel statusBar() {
        javax.swing.JPanel statusBar = new javax.swing.JPanel();
        statusBar.setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.LOWERED));
        statusBar.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints constraints = new java.awt.GridBagConstraints();
        constraints.anchor = java.awt.GridBagConstraints.WEST;
        constraints.fill = java.awt.GridBagConstraints.BOTH;
        constraints.gridy = 0;
        constraints.weightx = 1;
        statusBar.add(mainFrame.getStatusBarLabel(),constraints.clone());
        constraints.weightx = 0;
        constraints.fill = java.awt.GridBagConstraints.NONE;
        constraints.anchor = java.awt.GridBagConstraints.EAST;
        constraints.insets = new java.awt.Insets(0, 5, 0, 5);
        javax.swing.JLabel logoLabel = new javax.swing.JLabel();
        constraints.insets = new java.awt.Insets(0, 0, 0, 0);
        javax.swing.ImageIcon logoIcon = new javax.swing.ImageIcon(edu.umd.cs.findbugs.gui2.MainFrame.class.getResource("logo_umd.png"));
        logoLabel.setIcon(logoIcon);
        logoLabel.setPreferredSize(new java.awt.Dimension(logoIcon.getIconWidth(), logoIcon.getIconHeight()));
        constraints.anchor = java.awt.GridBagConstraints.WEST;
        statusBar.add(logoLabel,constraints.clone());
        return statusBar;
    }
    javax.swing.JSplitPane summaryTab() {
        mainFrame.setSummaryTopPanel(new javax.swing.JPanel());
        mainFrame.getSummaryTopPanel().setLayout(new java.awt.GridLayout(0, 1));
        mainFrame.getSummaryTopPanel().setBorder(javax.swing.BorderFactory.createEmptyBorder(2,4,2,4));
        javax.swing.JPanel summaryTopOuter = new javax.swing.JPanel(new java.awt.BorderLayout());
        summaryTopOuter.add(mainFrame.getSummaryTopPanel(),java.awt.BorderLayout.NORTH);
        mainFrame.getSummaryHtmlArea().setContentType("text/html");
        mainFrame.getSummaryHtmlArea().setEditable(false);
        mainFrame.getSummaryHtmlArea().addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
            public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {
                edu.umd.cs.findbugs.gui2.AboutDialog.editorPaneHyperlinkUpdate(evt);
            }
        });
        this.setStyleSheets();
        javax.swing.JScrollPane summaryScrollPane = new javax.swing.JScrollPane(summaryTopOuter);
        summaryScrollPane.getVerticalScrollBar().setUnitIncrement((int) (edu.umd.cs.findbugs.gui2.Driver.getFontSize()) );
        javax.swing.JSplitPane splitP = new javax.swing.JSplitPane(javax.swing.JSplitPane.HORIZONTAL_SPLIT, false, summaryScrollPane, mainFrame.getSummaryHtmlScrollPane());
        splitP.setContinuousLayout(true);
        splitP.setDividerLocation(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getSplitSummary());
        splitP.setOneTouchExpandable(true);
        splitP.setUI(new javax.swing.plaf.basic.BasicSplitPaneUI() {
            public javax.swing.plaf.basic.BasicSplitPaneDivider createDefaultDivider() {
                return new javax.swing.plaf.basic.BasicSplitPaneDivider(this) {
                    public void setBorder(javax.swing.border.Border b) {
                    }
                };
            }
        });
        splitP.setBorder(null);
        return splitP;
    }
    private void setStyleSheets() {
        javax.swing.text.html.StyleSheet styleSheet = new javax.swing.text.html.StyleSheet();
        styleSheet.addRule("body {font-size: " + edu.umd.cs.findbugs.gui2.Driver.getFontSize() + "pt}");
        styleSheet.addRule("H1 {color: red;  font-size: 120%; font-weight: bold;}");
        styleSheet.addRule("code {font-family: courier; font-size: " + edu.umd.cs.findbugs.gui2.Driver.getFontSize() + "pt}");
        styleSheet.addRule(" a:link { color: #0000FF; } ");
        styleSheet.addRule(" a:visited { color: #800080; } ");
        styleSheet.addRule(" a:active { color: #FF0000; text-decoration: underline; } ");
        javax.swing.text.html.HTMLEditorKit htmlEditorKit = new javax.swing.text.html.HTMLEditorKit();
        htmlEditorKit.setStyleSheet(styleSheet);
        mainFrame.summaryHtmlArea.setEditorKit(htmlEditorKit);
    }
    javax.swing.JPanel createCommentsInputPanel() {
        return mainFrame.getComments().createCommentsInputPanel();
    }
    javax.swing.JPanel createSourceCodePanel() {
        java.awt.Font sourceFont = new java.awt.Font("Monospaced", java.awt.Font.PLAIN, (int) (edu.umd.cs.findbugs.gui2.Driver.getFontSize()) );
        mainFrame.getSourceCodeTextPane().setFont(sourceFont);
        mainFrame.getSourceCodeTextPane().setEditable(false);
        mainFrame.getSourceCodeTextPane().getCaret().setSelectionVisible(true);
        mainFrame.getSourceCodeTextPane().setDocument(edu.umd.cs.findbugs.gui2.SourceCodeDisplay.SOURCE_NOT_RELEVANT);
        javax.swing.JScrollPane sourceCodeScrollPane = new javax.swing.JScrollPane(mainFrame.getSourceCodeTextPane());
        sourceCodeScrollPane.getVerticalScrollBar().setUnitIncrement(20);
        javax.swing.JPanel panel = new javax.swing.JPanel();
        panel.setLayout(new java.awt.BorderLayout());
        panel.add(sourceCodeScrollPane,java.awt.BorderLayout.CENTER);
        panel.revalidate();
        if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.out.println("Created source code panel");
        return panel;
    }
    javax.swing.JPanel createSourceSearchPanel() {
        java.awt.GridBagLayout gridbag = new java.awt.GridBagLayout();
        java.awt.GridBagConstraints c = new java.awt.GridBagConstraints();
        javax.swing.JPanel thePanel = new javax.swing.JPanel();
        thePanel.setLayout(gridbag);
        mainFrame.getFindButton().setToolTipText("Find first occurrence");
        mainFrame.getFindNextButton().setToolTipText("Find next occurrence");
        mainFrame.getFindPreviousButton().setToolTipText("Find previous occurrence");
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.insets = new java.awt.Insets(0, 5, 0, 5);
        c.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridbag.setConstraints(mainFrame.getSourceSearchTextField(),c);
        thePanel.add(mainFrame.getSourceSearchTextField());
        mainFrame.getFindButton().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mainFrame.searchSource(0);
            }
        });
        c.gridx = 1;
        c.weightx = 0.0;
        c.fill = java.awt.GridBagConstraints.NONE;
        gridbag.setConstraints(mainFrame.getFindButton(),c);
        thePanel.add(mainFrame.getFindButton());
        mainFrame.getFindNextButton().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mainFrame.searchSource(1);
            }
        });
        c.gridx = 2;
        c.weightx = 0.0;
        c.fill = java.awt.GridBagConstraints.NONE;
        gridbag.setConstraints(mainFrame.getFindNextButton(),c);
        thePanel.add(mainFrame.getFindNextButton());
        mainFrame.getFindPreviousButton().addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mainFrame.searchSource(2);
            }
        });
        c.gridx = 3;
        c.weightx = 0.0;
        c.fill = java.awt.GridBagConstraints.NONE;
        gridbag.setConstraints(mainFrame.getFindPreviousButton(),c);
        thePanel.add(mainFrame.getFindPreviousButton());
        return thePanel;
    }
    void setSourceTab(java.lang.String title, edu.umd.cs.findbugs.BugInstance bug) {
        javax.swing.JComponent label = mainFrame.getGuiLayout().getSourceViewComponent();
        if (label != null) {
            java.net.URL u = null;
            if (bug != null) {
                edu.umd.cs.findbugs.cloud.Cloud plugin = mainFrame.getBugCollection().getCloud();
                if (plugin.supportsSourceLinks()) u = plugin.getSourceLink(bug);
            }
            if (u != null) this.addLink(label,u);
            else this.removeLink(label);
        }
        mainFrame.getGuiLayout().setSourceTitle(title);
    }
    private void addLink(javax.swing.JComponent component, java.net.URL source) {
        this.sourceLink = source;
        component.setEnabled(true);
        if ( !listenerAdded) {
            listenerAdded = true;
            component.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent e) {
                    java.net.URL u = sourceLink;
                    if (u != null) edu.umd.cs.findbugs.util.LaunchBrowser.showDocument(u);
                }
            });
        }
        component.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        edu.umd.cs.findbugs.cloud.Cloud plugin = mainFrame.getBugCollection().getCloud();
        if (plugin != null) component.setToolTipText(plugin.getSourceLinkToolTip(null));
    }
    private void removeLink(javax.swing.JComponent component) {
        this.sourceLink = null;
        component.setEnabled(false);
        component.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        component.setToolTipText("");
    }
    void initializeGUI() {
        javax.swing.SwingUtilities.invokeLater(new edu.umd.cs.findbugs.gui2.MainFrameComponentFactory.InitializeGUI(mainFrame));
    }
    java.awt.Component bugSummaryComponent(edu.umd.cs.findbugs.BugAnnotation value, edu.umd.cs.findbugs.BugInstance bug) {
        javax.swing.JLabel label = new javax.swing.JLabel();
        label.setFont(label.getFont().deriveFont(edu.umd.cs.findbugs.gui2.Driver.getFontSize()));
        label.setFont(label.getFont().deriveFont(java.awt.Font.PLAIN));
        label.setForeground(java.awt.Color.BLACK);
        edu.umd.cs.findbugs.ClassAnnotation primaryClass = bug.getPrimaryClass();
        java.lang.String sourceCodeLabel = edu.umd.cs.findbugs.L10N.getLocalString("summary.source_code","source code.");
        java.lang.String summaryLine = edu.umd.cs.findbugs.L10N.getLocalString("summary.line","Line");
        java.lang.String summaryLines = edu.umd.cs.findbugs.L10N.getLocalString("summary.lines","Lines");
        java.lang.String clickToGoToText = edu.umd.cs.findbugs.L10N.getLocalString("tooltip.click_to_go_to","Click to go to");
        if (value instanceof edu.umd.cs.findbugs.SourceLineAnnotation) {
            final edu.umd.cs.findbugs.SourceLineAnnotation link = (edu.umd.cs.findbugs.SourceLineAnnotation) (value) ;
            if (this.sourceCodeExists(link)) {
                java.lang.String srcStr = "";
                int start = link.getStartLine();
                int end = link.getEndLine();
                if (start < 0 && end < 0) srcStr = sourceCodeLabel;
                else if (start == end) srcStr = " [" + summaryLine + " " + start + "]";
                else if (start < end) srcStr = " [" + summaryLines + " " + start + " - " + end + "]";
                label.setToolTipText(clickToGoToText + " " + srcStr);
                label.addMouseListener(new edu.umd.cs.findbugs.gui2.MainFrameComponentFactory.BugSummaryMouseListener(bug, label, link));
            }
            label.setText(link.toString());
        }
        else if (value instanceof edu.umd.cs.findbugs.BugAnnotationWithSourceLines) {
            edu.umd.cs.findbugs.BugAnnotationWithSourceLines note = (edu.umd.cs.findbugs.BugAnnotationWithSourceLines) (value) ;
            final edu.umd.cs.findbugs.SourceLineAnnotation link = note.getSourceLines();
            java.lang.String srcStr = "";
            if (link != null && this.sourceCodeExists(link)) {
                int start = link.getStartLine();
                int end = link.getEndLine();
                if (start < 0 && end < 0) srcStr = sourceCodeLabel;
                else if (start == end) srcStr = " [" + summaryLine + " " + start + "]";
                else if (start < end) srcStr = " [" + summaryLines + " " + start + " - " + end + "]";
                if ( !srcStr.equals("")) {
                    label.setToolTipText(clickToGoToText + " " + srcStr);
                    label.addMouseListener(new edu.umd.cs.findbugs.gui2.MainFrameComponentFactory.BugSummaryMouseListener(bug, label, link));
                }
            }
            java.lang.String noteText;
            if (note == bug.getPrimaryMethod() || note == bug.getPrimaryField()) noteText = note.toString();
            else noteText = note.toString(primaryClass);
            if ( !srcStr.equals(sourceCodeLabel)) label.setText(noteText + srcStr);
            else label.setText(noteText);
        }
        else {
            label.setText(value.toString(primaryClass));
        }
        return label;
    }
    public java.awt.Component bugSummaryComponent(java.lang.String str, edu.umd.cs.findbugs.BugInstance bug) {
        javax.swing.JLabel label = new javax.swing.JLabel();
        label.setFont(label.getFont().deriveFont(edu.umd.cs.findbugs.gui2.Driver.getFontSize()));
        label.setFont(label.getFont().deriveFont(java.awt.Font.PLAIN));
        label.setForeground(java.awt.Color.BLACK);
        label.setText(str);
        edu.umd.cs.findbugs.SourceLineAnnotation link = bug.getPrimarySourceLineAnnotation();
        if (link != null) label.addMouseListener(new edu.umd.cs.findbugs.gui2.MainFrameComponentFactory.BugSummaryMouseListener(bug, label, link));
        return label;
    }
    private boolean sourceCodeExists(edu.umd.cs.findbugs.SourceLineAnnotation note) {
        try {
            mainFrame.getProject().getSourceFinder().findSourceFile(note);
        }
        catch (java.io.FileNotFoundException e){
            return false;
        }
        catch (java.io.IOException e){
            return false;
        }
        return true;
    }
}
