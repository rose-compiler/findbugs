/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An object providing context information about a particular BugInstance.
 * 
 * @author David Hovemeyer
 * @see BugInstance
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.Serializable;
abstract public interface BugAnnotation extends java.lang.Comparable<edu.umd.cs.findbugs.BugAnnotation>, edu.umd.cs.findbugs.XMLWriteableWithMessages, java.io.Serializable, java.lang.Cloneable {
/**
     * XML tag for a formatted text message describing the annotation.
     */
    final public static java.lang.String MESSAGE_TAG = "Message";
    abstract public java.lang.Object clone();
/**
     * Accept a BugAnnotationVisitor.
     * 
     * @param visitor
     *            the visitor to accept
     */
    abstract public void accept(edu.umd.cs.findbugs.BugAnnotationVisitor visitor);
/**
     * Format the annotation as a String. The given key specifies additional
     * information about how the annotation should be formatted. If the key is
     * empty, then the "default" format will be used.
     * 
     * @param key
     *            how the annotation should be formatted
     * @param primaryClass
     *            The primary class for the bug; some bug annotation format msgs
     *            are simplified in relation to that class.
     */
    abstract public java.lang.String format(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass);
/**
     * Get a description of this bug annotation. The description is a key for
     * the FindBugsAnnotationDescriptions resource bundle.
     */
    abstract public java.lang.String getDescription();
/**
     * Set a description of this bug annotation. The description is a key for
     * the FindBugsAnnotationDescriptions resource bundle.
     */
    abstract public void setDescription(java.lang.String description);
/**
     * Is this annotation used to compute instance hashes or match bug instances
     * across versions
     * 
     * @return true if significant
     */
    abstract public boolean isSignificant();
    abstract public java.lang.String toString(edu.umd.cs.findbugs.ClassAnnotation primaryClass);
}
// vim:ts=4
