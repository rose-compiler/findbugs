/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Vertex class - represents a class or interface in the InheritanceGraph. Edges
 * connect subtypes to supertypes.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.ch;
import edu.umd.cs.findbugs.ba.ch.*;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.graph.AbstractVertex;
class ClassVertex extends edu.umd.cs.findbugs.graph.AbstractVertex<edu.umd.cs.findbugs.ba.ch.InheritanceEdge, edu.umd.cs.findbugs.ba.ch.ClassVertex> {
    final private static int FINISHED = 1;
    final private static int APPLICATION_CLASS = 2;
    final private static int INTERFACE = 4;
    final private edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor;
    final private edu.umd.cs.findbugs.ba.XClass xclass;
    private int flags;
    private edu.umd.cs.findbugs.ba.ch.ClassVertex directSuperclass;
    public java.lang.String toString() {
        return classDescriptor.toString();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.ba.ch.ClassVertex)) return false;
        return classDescriptor.equals(((edu.umd.cs.findbugs.ba.ch.ClassVertex) (o) ).classDescriptor);
    }
    public int hashCode() {
        return classDescriptor.hashCode();
    }
    public ClassVertex(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor, edu.umd.cs.findbugs.ba.XClass xclass) {
        super();
        this.classDescriptor = classDescriptor;
        this.xclass = xclass;
        this.flags = 0;
        if (xclass.isInterface()) {
            this.setInterface();
        }
    }
    public ClassVertex(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor, boolean isInterfaceEdge) {
        super();
        this.classDescriptor = classDescriptor;
        this.xclass = null;
        this.flags = 0;
        if (isInterfaceEdge) {
            this.setInterface();
        }
    }
/**
     * Factory method for resolved ClassVertex objects.
     * 
     * @param classDescriptor
     *            ClassDescriptor naming the class or interface
     * @param xclass
     *            object containing information about a class or interface
     * @return ClassVertex
     */
    public static edu.umd.cs.findbugs.ba.ch.ClassVertex createResolvedClassVertex(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor, edu.umd.cs.findbugs.ba.XClass xclass) {
        return new edu.umd.cs.findbugs.ba.ch.ClassVertex(classDescriptor, xclass);
    }
/**
     * Factory method for ClassVertex objects representing missing classes.
     * 
     * @param classDescriptor
     *            ClassDescriptor naming the missing class or interface
     * @param isInterface
     *            true if missing class is an interface, false otherwise
     * @return ClassVertex
     */
    public static edu.umd.cs.findbugs.ba.ch.ClassVertex createMissingClassVertex(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor, boolean isInterface) {
        return new edu.umd.cs.findbugs.ba.ch.ClassVertex(classDescriptor, isInterface);
    }
/**
     * @return Returns the classDescriptor.
     */
    public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() {
        return classDescriptor;
    }
/**
     * @return Returns the xClass.
     */
    public edu.umd.cs.findbugs.ba.XClass getXClass() {
        return xclass;
    }
/**
     * Return true if this ClassVertex corresponds to a resolved class, or false
     * if the class could not be found.
     */
    public boolean isResolved() {
        return xclass != null;
    }
/**
     * @param finished
     *            The finished to set.
     */
    public void setFinished(boolean finished) {
        this.setFlag(FINISHED,finished);
    }
/**
     * @return Returns the finished.
     */
    public boolean isFinished() {
        return this.isFlagSet(FINISHED);
    }
/**
     * Mark this ClassVertex as representing an application class.
     */
    public void markAsApplicationClass() {
        this.setFlag(APPLICATION_CLASS,true);
    }
/**
     * @return true if this ClassVertex represents an application class, false
     *         otherwise
     */
    public boolean isApplicationClass() {
        return this.isFlagSet(APPLICATION_CLASS);
    }
/**
     * Mark this ClassVertex as representing an interface.
     */
    private void setInterface() {
        this.setFlag(INTERFACE,true);
    }
/**
     * @return true if this ClassVertex represents an interface, false otherwise
     */
    public boolean isInterface() {
        return this.isFlagSet(INTERFACE);
    }
/**
     * Set the ClassVertex representing the direct superclass.
     * 
     * @param target
     *            ClassVertex representing the direct superclass.
     */
    public void setDirectSuperclass(edu.umd.cs.findbugs.ba.ch.ClassVertex target) {
        this.directSuperclass = target;
    }
/**
     * @return Returns the directSuperclass.
     */
    public edu.umd.cs.findbugs.ba.ch.ClassVertex getDirectSuperclass() {
        return directSuperclass;
    }
    private void setFlag(int flag, boolean enable) {
        if (enable) {
            flags |= flag;
        }
        else {
            flags &=  ~flag;
        }
    }
    private boolean isFlagSet(int flag) {
        return (flags & flag) != 0;
    }
}
