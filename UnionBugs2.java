/*
 * UnionBugs - Ant Task to Merge Findbugs Bug Reports
 * Copyright (C) 2008 peterfranza.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An ant task that is wraps the behavior of the UnionResults executable into an
 * ant task.
 * 
 * <taskdef name="UnionBugs2" classname="edu.umd.cs.findbugs.anttask.UnionBugs2"
 * classpath="...">
 * 
 * <UnionBugs2 to="${basedir}/findbugs.xml" > <fileset dir="plugins"> <include
 * name="*_findbugs_partial.xml" /> </fileset> </UnionBugs>
 * 
 * 
 * @ant.task category="utility"
 * 
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import java.io.File;
import java.util.ArrayList;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.types.FileSet;
public class UnionBugs2 extends edu.umd.cs.findbugs.anttask.AbstractFindBugsTask {
    private java.lang.String to;
    private java.util.ArrayList<org.apache.tools.ant.types.FileSet> fileSets = new java.util.ArrayList<org.apache.tools.ant.types.FileSet>();
    public UnionBugs2() {
        super("edu.umd.cs.findbugs.workflow.UnionResults");
        this.setFailOnError(true);
    }
/**
     * The fileset containing all the findbugs xml files that need to be merged
     * 
     * @param arg
     */
    public void addFileset(org.apache.tools.ant.types.FileSet arg) {
        this.fileSets.add(arg);
    }
/**
     * The File everything should get merged into
     * 
     * @param file
     */
    public void setTo(java.lang.String arg) {
        this.to = arg;
    }
    protected void checkParameters() {
        super.checkParameters();
        if (to == null) throw new org.apache.tools.ant.BuildException("to attribute is required", this.getLocation());
        if (fileSets.size() < 1) throw new org.apache.tools.ant.BuildException("fileset is required");
    }
    protected void beforeExecuteJavaProcess() {
        this.log("unioning bugs...");
    }
    protected void configureFindbugsEngine() {
        this.addArg("-withMessages");
        this.addArg("-output");
        this.addArg(to);
        for (org.apache.tools.ant.types.FileSet s : fileSets){
            java.io.File fromDir = s.getDir(this.getProject());
            for (java.lang.String file : s.getDirectoryScanner(this.getProject()).getIncludedFiles()){
                this.addArg(new java.io.File(fromDir, file).toString());
            }
;
        }
;
    }
}
