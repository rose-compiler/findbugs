/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 William Pugh
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Java main application to filter/transform an XML bug collection or bug
 * history collection.
 *
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.AppVersion;
import edu.umd.cs.findbugs.BugCategory;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugPattern;
import edu.umd.cs.findbugs.BugRanker;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.ExcludingHashesBugReporter;
import edu.umd.cs.findbugs.FieldAnnotation;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.I18N;
import edu.umd.cs.findbugs.MethodAnnotation;
import edu.umd.cs.findbugs.PackageStats;
import edu.umd.cs.findbugs.PackageStats.ClassStats;
import edu.umd.cs.findbugs.Project;
import edu.umd.cs.findbugs.ProjectStats;
import edu.umd.cs.findbugs.SloppyBugComparator;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.cloud.Cloud;
import edu.umd.cs.findbugs.config.CommandLine;
import edu.umd.cs.findbugs.filter.FilterException;
import edu.umd.cs.findbugs.filter.Matcher;
import edu.umd.cs.findbugs.util.Util;
public class Filter extends java.lang.Object {
    static class FilterCommandLine extends edu.umd.cs.findbugs.config.CommandLine {
/**
         *
         */
        final public static long MILLISECONDS_PER_DAY = 24 * 60 * 60 * 1000L;
        java.util.regex.Pattern classPattern;
        java.util.regex.Pattern bugPattern;
        java.util.regex.Pattern callsPattern;
        public boolean notSpecified = false;
        public boolean not = false;
        int duration;
        long first;
        java.lang.String firstAsString;
        long after;
        java.lang.String afterAsString;
        long before;
        java.lang.String beforeAsString;
        int maxRank = java.lang.Integer.MAX_VALUE;
        long maybeMutated;
        java.lang.String maybeMutatedAsString;
        long last;
        java.lang.String lastAsString;
        java.lang.String trimToVersionAsString;
// alternate way to specify 'last'
        java.lang.String fixedAsString;
        long present;
        java.lang.String presentAsString;
        long absent;
        java.lang.String absentAsString;
        java.lang.String annotation;
        java.util.HashSet<java.lang.String> hashesFromFile;
        public boolean sloppyUniqueSpecified = false;
        public boolean sloppyUnique = false;
        public boolean purgeHistorySpecified = false;
        public boolean purgeHistory = false;
        public boolean activeSpecified = false;
        public boolean active = false;
        public boolean notAProblem = false;
        public boolean notAProblemSpecified = false;
        public boolean shouldFix = false;
        public boolean shouldFixSpecified = false;
        public boolean hasField = false;
        public boolean hasFieldSpecified = false;
        public boolean hasLocal = false;
        public boolean hasLocalSpecified = false;
        public boolean applySuppression = false;
        public boolean applySuppressionSpecified = false;
        public boolean withSource = false;
        public boolean withSourceSpecified = false;
        public boolean knownSource = false;
        public boolean knownSourceSpecified = false;
        public boolean introducedByChange = false;
        public boolean introducedByChangeSpecified = false;
        public boolean removedByChange = false;
        public boolean removedByChangeSpecified = false;
        public boolean newCode = false;
        public boolean newCodeSpecified = false;
        public boolean hashChanged = false;
        public boolean hashChangedSpecified = false;
        public boolean removedCode = false;
        public boolean removedCodeSpecified = false;
        public boolean dontUpdateStats = false;
        public boolean dontUpdateStatsSpecified = false;
        public int maxAge = 0;
        public boolean maxAgeSpecified = false;
        public boolean withMessagesSpecified = false;
        public boolean withMessages = false;
        final private java.util.List<edu.umd.cs.findbugs.filter.Matcher> includeFilter = new java.util.LinkedList<edu.umd.cs.findbugs.filter.Matcher>();
        final private java.util.List<edu.umd.cs.findbugs.filter.Matcher> excludeFilter = new java.util.LinkedList<edu.umd.cs.findbugs.filter.Matcher>();
        java.util.HashSet<java.lang.String> excludedInstanceHashes = new java.util.HashSet<java.lang.String>();
        java.util.Set<java.lang.String> designationKey = new java.util.HashSet<java.lang.String>();
        java.util.Set<java.lang.String> categoryKey = new java.util.HashSet<java.lang.String>();
        java.util.SortedSet<edu.umd.cs.findbugs.BugInstance> uniqueSloppy;
        int priority = 3;
        public FilterCommandLine() {
            super();
            this.addSwitch("-not","reverse (all) switches for the filter");
            this.addSwitchWithOptionalExtraPart("-knownSource","trurh","Only issues that have known source locations");
            this.addSwitchWithOptionalExtraPart("-withSource","truth","only warnings for which source is available");
            this.addSwitchWithOptionalExtraPart("-hashChanged","truth","only warnings for which the stored hash is not the same as the calculated hash");
            this.addOption("-excludeBugs","baseline bug collection","exclude bugs already contained in the baseline bug collection");
            this.addOption("-exclude","filter file","exclude bugs matching given filter");
            this.addOption("-include","filter file","include only bugs matching given filter");
            this.addOption("-annotation","text","allow only warnings containing this text in a user annotation");
            this.addSwitchWithOptionalExtraPart("-withMessages","truth","generated XML should contain textual messages");
            this.addOption("-maxDuration","# versions","only issues present in at most this many versions");
            this.addOption("-after","when","allow only warnings that first occurred after this version");
            this.addOption("-before","when","allow only warnings that first occurred before this version");
            this.addOption("-first","when","allow only warnings that first occurred in this version");
            this.addOption("-last","when","allow only warnings that last occurred in this version");
            this.addOption("-trimToVersion","when","trim bug collection to exclude information about versions after this one");
            this.addOption("-fixed","when","allow only warnings that last occurred in the previous version (clobbers last)");
            this.addOption("-present","when","allow only warnings present in this version");
            this.addOption("-absent","when","allow only warnings absent in this version");
            this.addOption("-maybeMutated","when","allow only warnings that might have mutated/fixed/born in this version");
            this.addSwitchWithOptionalExtraPart("-hasField","truth","allow only warnings that are annotated with a field");
            this.addSwitchWithOptionalExtraPart("-hasLocal","truth","allow only warnings that are annotated with a local variable");
            this.addSwitchWithOptionalExtraPart("-active","truth","allow only warnings alive in the last sequence number");
            this.addSwitch("-applySuppression","exclude warnings that match the suppression filter");
            this.addSwitch("-purgeHistory","remove all version history");
            this.addSwitchWithOptionalExtraPart("-sloppyUnique","truth","select only issues thought to be unique by the sloppy bug comparator ");
            this.makeOptionUnlisted("-sloppyUnique");
            this.addSwitchWithOptionalExtraPart("-introducedByChange","truth","allow only warnings introduced by a change of an existing class");
            this.addSwitchWithOptionalExtraPart("-removedByChange","truth","allow only warnings removed by a change of a persisting class");
            this.addSwitchWithOptionalExtraPart("-newCode","truth","allow only warnings introduced by the addition of a new class");
            this.addSwitchWithOptionalExtraPart("-removedCode","truth","allow only warnings removed by removal of a class");
            this.addOption("-priority","level","allow only warnings with this priority or higher");
            this.makeOptionUnlisted("-priority");
            this.addOption("-confidence","level","allow only warnings with this confidence or higher");
            this.addOption("-maxRank","rank","allow only warnings with this rank or lower");
            this.addOption("-maxAge","days","Only issues that and in the cloud and weren't first seen more than this many days ago");
            this.addSwitchWithOptionalExtraPart("-notAProblem","truth","Only issues with a consensus view that they are not a problem");
            this.addSwitchWithOptionalExtraPart("-shouldFix","truth","Only issues with a consensus view that they should be fixed");
            this.addOption("-class","pattern","allow only bugs whose primary class name matches this pattern");
            this.addOption("-calls","pattern","allow only bugs that involve a call to a method that matches this pattern (matches with method class or name)");
            this.addOption("-bugPattern","pattern","allow only bugs whose type matches this pattern");
            this.addOption("-category","category","allow only warnings with a category that starts with this string");
            this.addOption("-designation","designation","allow only warnings with this designation (e.g., -designation SHOULD_FIX,MUST_FIX)");
            this.addSwitch("-dontUpdateStats","used when withSource is specified to only update bugs, not the class and package stats");
            this.addOption("-hashes","hash file","only bugs with instance hashes contained in the hash file");
        }
        public static long getVersionNum(edu.umd.cs.findbugs.BugCollection collection, java.lang.String val, boolean roundToLaterVersion) {
            if (val == null) return  -1;
            java.util.Map<java.lang.String, edu.umd.cs.findbugs.AppVersion> versions = new java.util.HashMap<java.lang.String, edu.umd.cs.findbugs.AppVersion>();
            java.util.SortedMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion> timeStamps = new java.util.TreeMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion>();
            for (java.util.Iterator<edu.umd.cs.findbugs.AppVersion> i = collection.appVersionIterator(); i.hasNext(); ) {
                edu.umd.cs.findbugs.AppVersion v = i.next();
                versions.put(v.getReleaseName(),v);
                timeStamps.put(v.getTimestamp(),v);
            }
// add current version to the maps
            edu.umd.cs.findbugs.AppVersion v = collection.getCurrentAppVersion();
            versions.put(v.getReleaseName(),v);
            timeStamps.put(v.getTimestamp(),v);
            return getVersionNum(versions,timeStamps,val,roundToLaterVersion,v.getSequenceNumber());
        }
        public static long getVersionNum(java.util.Map<java.lang.String, edu.umd.cs.findbugs.AppVersion> versions, java.util.SortedMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion> timeStamps, java.lang.String val, boolean roundToLaterVersion, long currentSeqNum) {
            if (val == null) return  -1;
            long numVersions = currentSeqNum + 1;
            if (val.equals("last") || val.equals("lastVersion")) return numVersions - 1;
            edu.umd.cs.findbugs.AppVersion v = versions.get(val);
            if (v != null) return v.getSequenceNumber();
            try {
                long time = 0;
                if (val.endsWith("daysAgo")) time = java.lang.System.currentTimeMillis() - MILLISECONDS_PER_DAY * java.lang.Integer.parseInt(val.substring(0,val.length() - 7));
                else time = java.util.Date.parse(val);
                return getAppropriateSeq(timeStamps,time,roundToLaterVersion);
            }
            catch (java.lang.Exception e){
                try {
                    long version = java.lang.Long.parseLong(val);
                    if (version < 0) {
                        version = numVersions + version;
                    }
                    return version;
                }
                catch (java.lang.NumberFormatException e1){
                    throw new java.lang.IllegalArgumentException("Could not interpret version specification of '" + val + "'");
                }
            }
        }
// timeStamps contains 0 10 20 30
// if roundToLater == true, ..0 = 0, 1..10 = 1, 11..20 = 2, 21..30 = 3,
// 31.. = Long.MAX
// if roundToLater == false, ..-1 = Long.MIN, 0..9 = 0, 10..19 = 1,
// 20..29 = 2, 30..39 = 3, 40 .. = 4
        private static long getAppropriateSeq(java.util.SortedMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion> timeStamps, long when, boolean roundToLaterVersion) {
            if (roundToLaterVersion) {
                java.util.SortedMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion> geq = timeStamps.tailMap(when);
                if (geq.isEmpty()) return java.lang.Long.MAX_VALUE;
                return geq.get(geq.firstKey()).getSequenceNumber();
            }
            else {
                java.util.SortedMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion> leq = timeStamps.headMap(when);
                if (leq.isEmpty()) return java.lang.Long.MIN_VALUE;
                return leq.get(leq.lastKey()).getSequenceNumber();
            }
        }
        private long minFirstSeen;
        edu.umd.cs.findbugs.filter.Filter suppressionFilter;
        void adjustFilter(edu.umd.cs.findbugs.Project project, edu.umd.cs.findbugs.BugCollection collection) {
            suppressionFilter = project.getSuppressionFilter();
            if (maxAgeSpecified) {
                minFirstSeen = collection.getAnalysisTimestamp() - maxAge * MILLISECONDS_PER_DAY;
            }
            first = getVersionNum(collection,firstAsString,true);
            maybeMutated = getVersionNum(collection,maybeMutatedAsString,true);
            last = getVersionNum(collection,lastAsString,true);
            before = getVersionNum(collection,beforeAsString,true);
            after = getVersionNum(collection,afterAsString,false);
            present = getVersionNum(collection,presentAsString,true);
            absent = getVersionNum(collection,absentAsString,true);
            if (sloppyUniqueSpecified) uniqueSloppy = new java.util.TreeSet<edu.umd.cs.findbugs.BugInstance>(new edu.umd.cs.findbugs.SloppyBugComparator());
            long fixed = getVersionNum(collection,fixedAsString,true);
            if (fixed >= 0) last = fixed - 1;
        }
// fixed means last on previous sequence (ok
// if -1)
        boolean accept(edu.umd.cs.findbugs.BugCollection collection, edu.umd.cs.findbugs.BugInstance bug) {
            boolean result = this.evaluate(collection,bug);
            if (not) return  !result;
            return result;
        }
        boolean evaluate(edu.umd.cs.findbugs.BugCollection collection, edu.umd.cs.findbugs.BugInstance bug) {
            for (edu.umd.cs.findbugs.filter.Matcher m : includeFilter)if ( !m.match(bug)) return false;
;
            for (edu.umd.cs.findbugs.filter.Matcher m : excludeFilter)if (m.match(bug)) return false;
;
            if (excludedInstanceHashes.contains(bug.getInstanceHash())) return false;
            if (annotation != null && bug.getAnnotationText().indexOf(annotation) ==  -1) return false;
            if (bug.getPriority() > priority) return false;
            if (firstAsString != null && bug.getFirstVersion() != first) return false;
            if (afterAsString != null && bug.getFirstVersion() <= after) return false;
            if (beforeAsString != null && bug.getFirstVersion() >= before) return false;
            if (hashesFromFile != null &&  !hashesFromFile.contains(bug.getInstanceHash())) return false;
            long lastSeen = bug.getLastVersion();
            if (lastSeen < 0) lastSeen = collection.getSequenceNumber();
            long thisDuration = lastSeen - bug.getFirstVersion();
            if (duration > 0 && thisDuration > duration) return false;
            if ((lastAsString != null || fixedAsString != null) && (last < 0 || bug.getLastVersion() != last)) return false;
            if (presentAsString != null &&  !this.bugLiveAt(bug,present)) return false;
            if (absentAsString != null && this.bugLiveAt(bug,absent)) return false;
            if (hasFieldSpecified && (hasField != (bug.getPrimaryField() != null))) return false;
            if (hasLocalSpecified && (hasLocal != (bug.getPrimaryLocalVariableAnnotation() != null))) return false;
            if (maxRank < java.lang.Integer.MAX_VALUE && edu.umd.cs.findbugs.BugRanker.findRank(bug) > maxRank) return false;
            if (activeSpecified && active == bug.isDead()) return false;
            if (removedByChangeSpecified && bug.isRemovedByChangeOfPersistingClass() != removedByChange) return false;
            if (introducedByChangeSpecified && bug.isIntroducedByChangeOfExistingClass() != introducedByChange) return false;
            if (newCodeSpecified && newCode != ( !bug.isIntroducedByChangeOfExistingClass() && bug.getFirstVersion() != 0)) return false;
            if (removedCodeSpecified && removedCode != ( !bug.isRemovedByChangeOfPersistingClass() && bug.isDead())) return false;
            if (bugPattern != null &&  !bugPattern.matcher(bug.getType()).find()) return false;
            if (classPattern != null &&  !classPattern.matcher(bug.getPrimaryClass().getClassName()).find()) return false;
            if (callsPattern != null) {
                edu.umd.cs.findbugs.MethodAnnotation m = bug.getAnnotationWithRole(edu.umd.cs.findbugs.MethodAnnotation.class,edu.umd.cs.findbugs.MethodAnnotation.METHOD_CALLED);
                if (m == null) return false;
                if ( !callsPattern.matcher(m.getClassName()).find() &&  !callsPattern.matcher(m.getMethodName()).find()) return false;
            }
            if (maybeMutatedAsString != null &&  !(this.atMutationPoint(bug) && mutationPoints.contains(this.getBugLocation(bug)))) return false;
            edu.umd.cs.findbugs.BugPattern thisBugPattern = bug.getBugPattern();
            if ( !categoryKey.isEmpty() && thisBugPattern != null &&  !categoryKey.contains(thisBugPattern.getCategory())) return false;
            if ( !designationKey.isEmpty() &&  !designationKey.contains(bug.getUserDesignationKey())) return false;
            if (hashChangedSpecified) {
                if (bug.isInstanceHashConsistent() == hashChanged) return false;
            }
            if (applySuppressionSpecified && applySuppression && suppressionFilter.match(bug)) return false;
            edu.umd.cs.findbugs.SourceLineAnnotation primarySourceLineAnnotation = bug.getPrimarySourceLineAnnotation();
            if (knownSourceSpecified) {
                if (primarySourceLineAnnotation.isUnknown() == knownSource) return false;
            }
            if (withSourceSpecified) {
                if (sourceSearcher.findSource(primarySourceLineAnnotation) != withSource) return false;
            }
            edu.umd.cs.findbugs.cloud.Cloud cloud = collection.getCloud();
            if (maxAgeSpecified) {
                long firstSeen = cloud.getFirstSeen(bug);
                if ( !cloud.isInCloud(bug)) return false;
                if (firstSeen < minFirstSeen) return false;
            }
            if (notAProblemSpecified && notAProblem != (cloud.getConsensusDesignation(bug).score() < 0)) return false;
            if (shouldFixSpecified && shouldFix != (cloud.getConsensusDesignation(bug).score() > 0)) return false;
            if (sloppyUniqueSpecified) {
                boolean unique = uniqueSloppy.add(bug);
                if (unique != sloppyUnique) return false;
            }
            return true;
        }
        private void addDesignationKey(java.lang.String argument) {
            edu.umd.cs.findbugs.I18N i18n = edu.umd.cs.findbugs.I18N.instance();
            for (java.lang.String x : argument.split("[,|]")){
                for (java.lang.String designationKey : i18n.getUserDesignationKeys()){
                    if (designationKey.equals(x) || i18n.getUserDesignation(designationKey).equals(x)) {
                        this.designationKey.add(designationKey);
                        break;
                    }
                }
;
            }
;
        }
        private void addCategoryKey(java.lang.String argument) {
            edu.umd.cs.findbugs.DetectorFactoryCollection i18n = edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
            for (java.lang.String x : argument.split("[,|]")){
                for (edu.umd.cs.findbugs.BugCategory category : i18n.getBugCategoryObjects()){
                    if (category.getAbbrev().equals(x) || category.getCategory().equals(x)) {
                        this.categoryKey.add(category.getCategory());
                        break;
                    }
                }
;
            }
;
        }
        private boolean bugLiveAt(edu.umd.cs.findbugs.BugInstance bug, long now) {
            if (now < bug.getFirstVersion()) return false;
            if (bug.isDead() && bug.getLastVersion() < now) return false;
            return true;
        }
        protected void handleOption(java.lang.String option, java.lang.String optionExtraPart) throws java.io.IOException {
            option = option.substring(1);
            if (optionExtraPart.length() == 0) this.setField(option,true);
            else this.setField(option,java.lang.Boolean.parseBoolean(optionExtraPart));
            this.setField(option + "Specified",true);
        }
        private void setField(java.lang.String fieldName, boolean value) {
            try {
                java.lang.reflect.Field f = edu.umd.cs.findbugs.workflow.Filter.FilterCommandLine.class.getField(fieldName);
                f.setBoolean(this,value);
            }
            catch (java.lang.RuntimeException e){
                throw e;
            }
            catch (java.lang.Exception e){
                throw new java.lang.RuntimeException(e);
            }
        }
        protected void handleOptionWithArgument(java.lang.String option, java.lang.String argument) throws java.io.IOException {
            if (option.equals("-priority") || option.equals("-confidence")) {
                priority = parsePriority(argument);
            }
            else if (option.equals("-maxRank")) maxRank = java.lang.Integer.parseInt(argument);
            else if (option.equals("-first")) firstAsString = argument;
            else if (option.equals("-maybeMutated")) maybeMutatedAsString = argument;
            else if (option.equals("-last")) lastAsString = argument;
            else if (option.equals("-trimToVersion")) trimToVersionAsString = argument;
            else if (option.equals("-maxDuration")) duration = java.lang.Integer.parseInt(argument);
            else if (option.equals("-fixed")) fixedAsString = argument;
            else if (option.equals("-after")) afterAsString = argument;
            else if (option.equals("-before")) beforeAsString = argument;
            else if (option.equals("-present")) presentAsString = argument;
            else if (option.equals("-absent")) absentAsString = argument;
            else if (option.equals("-category")) this.addCategoryKey(argument);
            else if (option.equals("-designation")) this.addDesignationKey(argument);
            else if (option.equals("-class")) classPattern = java.util.regex.Pattern.compile(argument.replace('\u002c','\u007c'));
            else if (option.equals("-calls")) callsPattern = java.util.regex.Pattern.compile(argument.replace('\u002c','\u007c'));
            else if (option.equals("-bugPattern")) bugPattern = java.util.regex.Pattern.compile(argument);
            else if (option.equals("-annotation")) annotation = argument;
            else if (option.equals("-excludeBugs")) {
                try {
                    edu.umd.cs.findbugs.ExcludingHashesBugReporter.addToExcludedInstanceHashes(excludedInstanceHashes,argument);
                }
                catch (org.dom4j.DocumentException e){
                    throw new java.lang.IllegalArgumentException("Error processing include file: " + argument, e);
                }
            }
            else if (option.equals("-include")) {
                try {
                    includeFilter.add(new edu.umd.cs.findbugs.filter.Filter(argument));
                }
                catch (edu.umd.cs.findbugs.filter.FilterException e){
                    throw new java.lang.IllegalArgumentException("Error processing include file: " + argument, e);
                }
            }
            else if (option.equals("-exclude")) {
                try {
                    excludeFilter.add(new edu.umd.cs.findbugs.filter.Filter(argument));
                }
                catch (edu.umd.cs.findbugs.filter.FilterException e){
                    throw new java.lang.IllegalArgumentException("Error processing include file: " + argument, e);
                }
            }
            else if (option.equals("-maxAge")) {
                maxAge = java.lang.Integer.parseInt(argument);
                maxAgeSpecified = true;
            }
            else if (option.equals("-hashes")) {
                hashesFromFile = new java.util.HashSet<java.lang.String>();
                java.io.BufferedReader in = null;
                try {
                    in = new java.io.BufferedReader(edu.umd.cs.findbugs.charsets.UTF8.fileReader(argument));
                    while (true) {
                        java.lang.String h = in.readLine();
                        if (h == null) break;
                        hashesFromFile.add(h);
                    }
                }
                catch (java.io.IOException e){
                    throw new java.lang.RuntimeException("Error reading hashes from " + argument, e);
                }
                finally {
                    edu.umd.cs.findbugs.util.Util.closeSilently(in);
                }
            }
            else throw new java.lang.IllegalArgumentException("can't handle command line argument of " + option);
        }
        java.util.HashSet<java.lang.String> mutationPoints;
/**
         * Do any prep work needed to perform bug filtering
         *
         * @param origCollection
         */
        public void getReady(edu.umd.cs.findbugs.SortedBugCollection origCollection) {
            if (maybeMutatedAsString != null) {
                java.util.HashSet<java.lang.String> addedIssues = new java.util.HashSet<java.lang.String>();
                java.util.HashSet<java.lang.String> removedIssues = new java.util.HashSet<java.lang.String>();
                for (edu.umd.cs.findbugs.BugInstance b : origCollection)if (b.getFirstVersion() == maybeMutated) addedIssues.add(this.getBugLocation(b));
                else if (b.getLastVersion() == maybeMutated - 1) removedIssues.add(this.getBugLocation(b));
;
                addedIssues.remove(null);
                addedIssues.retainAll(removedIssues);
                mutationPoints = addedIssues;
            }
        }
/**
         * @param b
         * @return
         */
        private boolean atMutationPoint(edu.umd.cs.findbugs.BugInstance b) {
            return b.getFirstVersion() == maybeMutated || b.getLastVersion() == maybeMutated - 1;
        }
/**
         * @param b
         * @return
         */
        private java.lang.String getBugLocation(edu.umd.cs.findbugs.BugInstance b) {
            java.lang.String point;
            edu.umd.cs.findbugs.MethodAnnotation m = b.getPrimaryMethod();
            edu.umd.cs.findbugs.FieldAnnotation f = b.getPrimaryField();
            if (m != null) point = m.toString();
            else if (f != null) point = f.toString();
            else point = null;
            return point;
        }
    }
    public Filter() {
    }
    public static int parsePriority(java.lang.String argument) {
        int i = " HMLE".indexOf(argument);
        if (i ==  -1) i = " 1234".indexOf(argument);
        if (i ==  -1) throw new java.lang.IllegalArgumentException("Bad priority: " + argument);
        return i;
    }
    static edu.umd.cs.findbugs.workflow.SourceSearcher sourceSearcher;
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        final edu.umd.cs.findbugs.workflow.Filter.FilterCommandLine commandLine = new edu.umd.cs.findbugs.workflow.Filter.FilterCommandLine();
        int argCount = commandLine.parse(args,0,2,"Usage: " + edu.umd.cs.findbugs.workflow.Filter.class.getName() + " [options] [<orig results> [<new results]] ");
        edu.umd.cs.findbugs.SortedBugCollection origCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (argCount == args.length) origCollection.readXML(java.lang.System.in);
        else origCollection.readXML(args[argCount++]);
        boolean verbose = argCount < args.length;
        edu.umd.cs.findbugs.SortedBugCollection resultCollection = origCollection.createEmptyCollectionWithMetadata();
        edu.umd.cs.findbugs.Project project = resultCollection.getProject();
        int passed = 0;
        int dropped = 0;
        resultCollection.setWithMessages(commandLine.withMessages);
        if (commandLine.hashChangedSpecified) origCollection.computeBugHashes();
        commandLine.adjustFilter(project,resultCollection);
        edu.umd.cs.findbugs.ProjectStats projectStats = resultCollection.getProjectStats();
        projectStats.clearBugCounts();
        if (commandLine.classPattern != null) {
            projectStats.purgeClassesThatDontMatch(commandLine.classPattern);
        }
        sourceSearcher = new edu.umd.cs.findbugs.workflow.SourceSearcher(project);
        long trimToVersion =  -1;
        if (commandLine.trimToVersionAsString != null) {
            java.util.Map<java.lang.String, edu.umd.cs.findbugs.AppVersion> versions = new java.util.HashMap<java.lang.String, edu.umd.cs.findbugs.AppVersion>();
            java.util.SortedMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion> timeStamps = new java.util.TreeMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion>();
            for (java.util.Iterator<edu.umd.cs.findbugs.AppVersion> i = origCollection.appVersionIterator(); i.hasNext(); ) {
                edu.umd.cs.findbugs.AppVersion v = i.next();
                versions.put(v.getReleaseName(),v);
                timeStamps.put(v.getTimestamp(),v);
            }
// add current version to the maps
            edu.umd.cs.findbugs.AppVersion v = resultCollection.getCurrentAppVersion();
            versions.put(v.getReleaseName(),v);
            timeStamps.put(v.getTimestamp(),v);
            trimToVersion = edu.umd.cs.findbugs.workflow.Filter.FilterCommandLine.getVersionNum(versions,timeStamps,commandLine.trimToVersionAsString,true,v.getSequenceNumber());
            if (trimToVersion < origCollection.getSequenceNumber()) {
                java.lang.String name = resultCollection.getAppVersionFromSequenceNumber(trimToVersion).getReleaseName();
                long timestamp = resultCollection.getAppVersionFromSequenceNumber(trimToVersion).getTimestamp();
                resultCollection.setReleaseName(name);
                resultCollection.setTimestamp(timestamp);
                resultCollection.trimAppVersions(trimToVersion);
            }
        }
        if (commandLine.maxAgeSpecified || commandLine.notAProblemSpecified || commandLine.shouldFixSpecified) origCollection.getCloud().waitUntilIssueDataDownloaded();
        commandLine.getReady(origCollection);
        for (edu.umd.cs.findbugs.BugInstance bug : origCollection.getCollection())if (commandLine.accept(origCollection,bug)) {
            if (trimToVersion >= 0) {
                if (bug.getFirstVersion() > trimToVersion) {
                    dropped++;
                    continue;
                }
                else if (bug.getLastVersion() >= trimToVersion) {
                    bug.setLastVersion( -1);
                    bug.setRemovedByChangeOfPersistingClass(false);
                }
            }
            resultCollection.add(bug,false);
            passed++;
        }
        else dropped++;
;
        if (commandLine.purgeHistorySpecified && commandLine.purgeHistory) {
            resultCollection.clearAppVersions();
            for (edu.umd.cs.findbugs.BugInstance bug : resultCollection.getCollection()){
                bug.clearHistory();
            }
;
        }
        if (verbose) java.lang.System.out.println(passed + " warnings passed through, " + dropped + " warnings dropped");
        if (commandLine.withSourceSpecified && commandLine.withSource &&  !commandLine.dontUpdateStats && projectStats.hasClassStats()) {
            for (edu.umd.cs.findbugs.PackageStats stats : projectStats.getPackageStats()){
                java.util.Iterator<edu.umd.cs.findbugs.PackageStats.ClassStats> i = stats.getClassStats().iterator();
                while (i.hasNext()) {
                    java.lang.String className = i.next().getName();
                    if (sourceSearcher.sourceNotFound.contains(className) ||  !sourceSearcher.sourceFound.contains(className) &&  !sourceSearcher.findSource(edu.umd.cs.findbugs.SourceLineAnnotation.createReallyUnknown(className))) i.remove();
                }
            }
;
        }
        projectStats.recomputeFromComponents();
        if (argCount == args.length) {
            assert  !verbose;
            resultCollection.writeXML(java.lang.System.out);
        }
        else {
            resultCollection.writeXML(args[argCount++]);
        }
    }
}
// vim:ts=4
