package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantCP;
import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantDouble;
import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantLong;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.ba.ch.Subtypes2;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MissingClassException;
import edu.umd.cs.findbugs.util.ClassName;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
public class ResolveAllReferences extends edu.umd.cs.findbugs.visitclass.PreorderVisitor implements edu.umd.cs.findbugs.Detector {
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    public ResolveAllReferences(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    java.util.Set<java.lang.String> defined;
    private void compute() {
        if (defined == null) {
            defined = new java.util.HashSet<java.lang.String>();
// System.out.println("Computing");
            edu.umd.cs.findbugs.ba.ch.Subtypes2 subtypes2 = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getSubtypes2();
            java.util.Collection<edu.umd.cs.findbugs.ba.XClass> allClasses = subtypes2.getXClassCollection();
            edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache();
            for (edu.umd.cs.findbugs.ba.XClass c : allClasses){
                try {
                    org.apache.bcel.classfile.JavaClass jclass = analysisCache.getClassAnalysis(org.apache.bcel.classfile.JavaClass.class,c.getClassDescriptor());
                    this.addAllDefinitions(jclass);
                }
                catch (edu.umd.cs.findbugs.classfile.MissingClassException e){
                    bugReporter.reportMissingClass(e.getClassDescriptor());
                }
                catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
                    bugReporter.logError("Could not find class " + c.getClassDescriptor().toDottedClassName(),e);
                }
            }
;
        }
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        classContext.getJavaClass().accept(this);
    }
    public void report() {
    }
    public void addAllDefinitions(org.apache.bcel.classfile.JavaClass obj) {
        java.lang.String className2 = obj.getClassName();
        defined.add(className2);
        for (org.apache.bcel.classfile.Method m : obj.getMethods())if ( !m.isPrivate()) {
            java.lang.String name = this.getMemberName(obj,className2,m.getNameIndex(),m.getSignatureIndex());
            defined.add(name);
        }
;
        for (org.apache.bcel.classfile.Field f : obj.getFields())if ( !f.isPrivate()) {
            java.lang.String name = this.getMemberName(obj,className2,f.getNameIndex(),f.getSignatureIndex());
            defined.add(name);
        }
;
    }
    private java.lang.String getClassName(org.apache.bcel.classfile.JavaClass c, int classIndex) {
        java.lang.String name = c.getConstantPool().getConstantString(classIndex,CONSTANT_Class);
        return edu.umd.cs.findbugs.util.ClassName.extractClassName(name).replace('\u002f','\u002e');
    }
    private java.lang.String getMemberName(org.apache.bcel.classfile.JavaClass c, java.lang.String className, int memberNameIndex, int signatureIndex) {
        return className + "." + ((org.apache.bcel.classfile.ConstantUtf8) (c.getConstantPool().getConstant(memberNameIndex,CONSTANT_Utf8)) ).getBytes() + " : " + ((org.apache.bcel.classfile.ConstantUtf8) (c.getConstantPool().getConstant(signatureIndex,CONSTANT_Utf8)) ).getBytes();
    }
    private java.lang.String getMemberName(java.lang.String className, java.lang.String memberName, java.lang.String signature) {
        return className.replace('\u002f','\u002e') + "." + memberName + " : " + signature;
    }
    private boolean find(org.apache.bcel.classfile.JavaClass target, java.lang.String name, java.lang.String signature) throws java.lang.ClassNotFoundException {
        if (target == null) return false;
        java.lang.String ref = this.getMemberName(target.getClassName(),name,signature);
        if (defined.contains(ref)) return true;
        if (this.find(target.getSuperClass(),name,signature)) return true;
        for (org.apache.bcel.classfile.JavaClass i : target.getInterfaces())if (this.find(i,name,signature)) return true;
;
        return false;
    }
    public void visit(org.apache.bcel.classfile.JavaClass obj) {
        this.compute();
        org.apache.bcel.classfile.ConstantPool cp = obj.getConstantPool();
        org.apache.bcel.classfile.Constant[] constants = cp.getConstantPool();
        checkConstant:for (int i = 0; i < constants.length; i++) {
            org.apache.bcel.classfile.Constant co = constants[i];
            if (co instanceof org.apache.bcel.classfile.ConstantDouble || co instanceof org.apache.bcel.classfile.ConstantLong) i++;
            if (co instanceof org.apache.bcel.classfile.ConstantClass) {
                java.lang.String ref = this.getClassName(obj,i);
                if ((ref.startsWith("java") || ref.startsWith("org.w3c.dom")) &&  !defined.contains(ref)) bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "VR_UNRESOLVABLE_REFERENCE", NORMAL_PRIORITY).addClass(obj).addString(ref));
            }
            else if (co instanceof org.apache.bcel.classfile.ConstantFieldref) {
            }
            else if (co instanceof org.apache.bcel.classfile.ConstantCP) {
                org.apache.bcel.classfile.ConstantCP co2 = (org.apache.bcel.classfile.ConstantCP) (co) ;
                java.lang.String className = this.getClassName(obj,co2.getClassIndex());
                if (className.equals(obj.getClassName()) ||  !defined.contains(className)) {
                    continue checkConstant;
                }
                org.apache.bcel.classfile.ConstantNameAndType nt = (org.apache.bcel.classfile.ConstantNameAndType) (cp.getConstant(co2.getNameAndTypeIndex())) ;
                java.lang.String name = ((org.apache.bcel.classfile.ConstantUtf8) (obj.getConstantPool().getConstant(nt.getNameIndex(),CONSTANT_Utf8)) ).getBytes();
                java.lang.String signature = ((org.apache.bcel.classfile.ConstantUtf8) (obj.getConstantPool().getConstant(nt.getSignatureIndex(),CONSTANT_Utf8)) ).getBytes();
                try {
                    org.apache.bcel.classfile.JavaClass target = org.apache.bcel.Repository.lookupClass(className);
                    if ( !this.find(target,name,signature)) bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "VR_UNRESOLVABLE_REFERENCE", NORMAL_PRIORITY).addClass(obj).addString(this.getMemberName(target.getClassName(),name,signature)));
                }
                catch (java.lang.ClassNotFoundException e){
                    bugReporter.reportMissingClass(e);
                }
            }
        }
    }
}
// do nothing until we handle static fields defined in
// interfaces
// System.out.println("checking " + ref);
// System.out.println("Skipping check of " + ref);
