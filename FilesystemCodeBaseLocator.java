/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Codebase locator for files and directories in the filesystem.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.impl;
import edu.umd.cs.findbugs.classfile.impl.*;
import java.io.File;
import java.io.IOException;
import edu.umd.cs.findbugs.classfile.ICodeBase;
import edu.umd.cs.findbugs.classfile.ICodeBaseLocator;
public class FilesystemCodeBaseLocator extends java.lang.Object implements edu.umd.cs.findbugs.classfile.ICodeBaseLocator {
    final private java.lang.String pathName;
    public FilesystemCodeBaseLocator(java.lang.String pathName) {
        super();
        java.io.File file = new java.io.File(pathName);
        try {
            pathName = file.getCanonicalPath();
        }
        catch (java.io.IOException e){
            assert true;
        }
        this.pathName = pathName;
    }
/**
     * @return Returns the pathName.
     */
    public java.lang.String getPathName() {
        return pathName;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.ICodeBaseLocator#createRelativeCodeBaseLocator
     * (java.lang.String)
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseLocator createRelativeCodeBaseLocator(java.lang.String relativePath) {
        java.io.File path = new java.io.File(pathName);
        if ( !path.isDirectory()) {
            path = path.getParentFile();
        }
        java.io.File relativeFile = new java.io.File(path, relativePath);
        return new edu.umd.cs.findbugs.classfile.impl.FilesystemCodeBaseLocator(relativeFile.getPath());
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseLocator#openCodeBase()
     */
    public edu.umd.cs.findbugs.classfile.ICodeBase openCodeBase() throws java.io.IOException {
        return edu.umd.cs.findbugs.classfile.impl.ClassFactory.createFilesystemCodeBase(this);
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public java.lang.String toString() {
        return "filesystem:" + pathName;
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(java.lang.Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        edu.umd.cs.findbugs.classfile.impl.FilesystemCodeBaseLocator other = (edu.umd.cs.findbugs.classfile.impl.FilesystemCodeBaseLocator) (obj) ;
        return this.pathName.equals(other.pathName);
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return pathName.hashCode();
    }
}
