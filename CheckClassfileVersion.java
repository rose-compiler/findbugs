/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Verify that a set of jar files are compiled for Java 5.0, the release
 * standard for FindBugs
 * 
 */
package edu.umd.cs.findbugs.tools;
import edu.umd.cs.findbugs.tools.*;
import java.io.DataInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
public class CheckClassfileVersion extends java.lang.Object {
    public CheckClassfileVersion() {
    }
    private static boolean isJarFile(java.io.File f) {
        java.lang.String name = f.getName();
        return name.endsWith(".jar") || name.endsWith(".zip") || name.endsWith("war") || name.endsWith(".ear");
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        boolean fail = false;
        java.util.ArrayList<java.io.File> s = new java.util.ArrayList<java.io.File>(args.length);
        for (java.lang.String f : args){
            java.io.File file = new java.io.File(f);
            if ( !file.canRead()) java.lang.System.out.println("Can't read " + f);
            if (file.isDirectory()) {
                for (java.io.File f2 : file.listFiles())if (isJarFile(f2)) s.add(f2);
;
            }
            else if (isJarFile(file)) s.add(file);
        }
;
        for (java.io.File jarFile : s){
            java.lang.String jarFileName = jarFile.getName();
            java.lang.System.out.println("Checking " + jarFileName);
            java.util.jar.JarFile z = new java.util.jar.JarFile(jarFile);
            for (java.util.Enumeration<java.util.jar.JarEntry> e = z.entries(); e.hasMoreElements(); ) {
                java.util.jar.JarEntry ze = e.nextElement();
                if (ze.isDirectory()) continue;
                java.lang.String name = ze.getName();
                boolean isClassFile = name.endsWith(".class");
                if ( !isClassFile) continue;
                java.io.DataInputStream zipIn = new java.io.DataInputStream(z.getInputStream(ze));
                int magic = zipIn.readInt();
                int minorVersion = zipIn.readUnsignedShort();
                int majorVersion = zipIn.readUnsignedShort();
                if (magic != 0xCAFEBABE) {
                    java.lang.System.out.printf("bad magic %x: %s %s%n",magic,jarFileName,name);
                    fail = true;
                }
                else if (minorVersion >= 60) {
                    java.lang.System.out.printf("bad version %d:%s %s%n",minorVersion,jarFileName,name);
                    fail = true;
                }
                zipIn.close();
            }
            z.close();
        }
;
        if (fail) java.lang.System.exit(1);
    }
}
