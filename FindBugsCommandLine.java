/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Base class for FindBugs command line classes. Handles all shared
 * switches/options.
 *
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.StringTokenizer;
import javax.annotation.Nonnull;
import edu.umd.cs.findbugs.config.AnalysisFeatureSetting;
import edu.umd.cs.findbugs.config.CommandLine;
abstract public class FindBugsCommandLine extends edu.umd.cs.findbugs.config.CommandLine {
/**
     * Analysis settings to configure the analysis effort.
     */
    protected edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] settingList = edu.umd.cs.findbugs.FindBugs.DEFAULT_EFFORT;
/**
     * Project to analyze.
     */
    protected edu.umd.cs.findbugs.Project project;
/**
     * True if project was initialized by loading a project file.
     */
    protected boolean projectLoadedFromFile;
/**
     * Constructor. Adds shared options/switches.
     */
    public FindBugsCommandLine() {
        super();
        project = new edu.umd.cs.findbugs.Project();
        this.startOptionGroup("General FindBugs options:");
        this.addOption("-project","project","analyze given project");
        this.addOption("-home","home directory","specify FindBugs home directory");
        this.addOption("-pluginList","jar1[" + java.io.File.pathSeparator + "jar2...]","specify list of plugin Jar files to load");
        this.addSwitchWithOptionalExtraPart("-effort","min|less|default|more|max","set analysis effort level");
        this.addSwitch("-adjustExperimental","lower priority of experimental Bug Patterns");
        this.addSwitch("-workHard","ensure analysis effort is at least 'default'");
        this.addSwitch("-conserveSpace","same as -effort:min (for backward compatibility)");
    }
/**
     * Additional constuctor just as hack for decoupling the core package from
     * gui2 package
     *
     * @param modernGui
     *            ignored. In any case, gui2 options are added here.
     */
    public FindBugsCommandLine(boolean modernGui) {
        this();
        this.addOption("-f","font size","set font size");
        this.addSwitch("-clear","clear saved GUI settings and exit");
        this.addOption("-priority","thread priority","set analysis thread priority");
        this.addOption("-loadbugs","saved analysis results","load bugs from saved analysis results");
        this.makeOptionUnlisted("-loadbugs");
        this.addOption("-loadBugs","saved analysis results","load bugs from saved analysis results");
        this.addSwitch("-d","disable docking");
        this.addSwitch("--nodock","disable docking");
        this.addSwitchWithOptionalExtraPart("-look","plastic|gtk|native","set UI look and feel");
    }
    public edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] getSettingList() {
        return settingList;
    }
    public edu.umd.cs.findbugs.Project getProject() {
        return project;
    }
    public boolean isProjectLoadedFromFile() {
        return projectLoadedFromFile;
    }
    protected void handleOption(java.lang.String option, java.lang.String optionExtraPart) {
        if (option.equals("-effort")) {
            if (optionExtraPart.equals("min")) {
                settingList = edu.umd.cs.findbugs.FindBugs.MIN_EFFORT;
            }
            else if (optionExtraPart.equals("less")) {
                settingList = edu.umd.cs.findbugs.FindBugs.LESS_EFFORT;
            }
            else if (optionExtraPart.equals("default")) {
                settingList = edu.umd.cs.findbugs.FindBugs.DEFAULT_EFFORT;
            }
            else if (optionExtraPart.equals("more")) {
                settingList = edu.umd.cs.findbugs.FindBugs.MORE_EFFORT;
            }
            else if (optionExtraPart.equals("max")) {
                settingList = edu.umd.cs.findbugs.FindBugs.MAX_EFFORT;
            }
            else {
                throw new java.lang.IllegalArgumentException("-effort:<value> must be one of min,default,more,max");
            }
        }
        else if (option.equals("-workHard")) {
            if (settingList != edu.umd.cs.findbugs.FindBugs.MAX_EFFORT) settingList = edu.umd.cs.findbugs.FindBugs.MORE_EFFORT;
        }
        else if (option.equals("-conserveSpace")) {
            settingList = edu.umd.cs.findbugs.FindBugs.MIN_EFFORT;
        }
        else if (option.equals("-adjustExperimental")) {
            edu.umd.cs.findbugs.BugInstance.setAdjustExperimental(true);
        }
        else {
            throw new java.lang.IllegalArgumentException("Don't understand option " + option);
        }
    }
    protected void handleOptionWithArgument(java.lang.String option, java.lang.String argument) throws java.io.IOException {
        if (option.equals("-home")) {
            edu.umd.cs.findbugs.FindBugs.setHome(argument);
        }
        else if (option.equals("-pluginList")) {
            java.lang.String pluginListStr = argument;
            java.util.Map<java.lang.String, java.lang.Boolean> customPlugins = this.getProject().getConfiguration().getCustomPlugins();
            java.util.StringTokenizer tok = new java.util.StringTokenizer(pluginListStr, java.io.File.pathSeparator);
            while (tok.hasMoreTokens()) {
                java.io.File file = new java.io.File(tok.nextToken());
                java.lang.Boolean enabled = java.lang.Boolean.valueOf(file.isFile());
                customPlugins.put(file.getAbsolutePath(),enabled);
                if (enabled.booleanValue()) {
                    try {
                        edu.umd.cs.findbugs.Plugin.loadCustomPlugin(file,this.getProject());
                    }
                    catch (edu.umd.cs.findbugs.PluginException e){
                        throw new java.lang.IllegalStateException("Failed to load plugin specified by the '-pluginList', file: " + file, e);
                    }
                }
            }
        }
        else if (option.equals("-project")) {
            this.loadProject(argument);
        }
        else {
            throw new java.lang.IllegalStateException();
        }
    }
/**
     * Load given project file.
     *
     * @param arg
     *            name of project file
     * @throws java.io.IOException
     */
    public void loadProject(java.lang.String arg) throws java.io.IOException {
        edu.umd.cs.findbugs.Project newProject = edu.umd.cs.findbugs.Project.readProject(arg);
        newProject.setConfiguration(project.getConfiguration());
        project = newProject;
        projectLoadedFromFile = true;
    }
}
