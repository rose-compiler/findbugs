/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import junit.framework.TestCase;
public class IncompatMaskTest extends junit.framework.TestCase {
    public IncompatMaskTest() {
    }
    void check(long value) {
        assertEquals(1,edu.umd.cs.findbugs.detect.IncompatMask.populationCount(value));
        boolean isLong = (value >>> 32) != 0;
        assertEquals(value,edu.umd.cs.findbugs.detect.IncompatMask.getFlagBits(true,value));
        assertEquals(value,edu.umd.cs.findbugs.detect.IncompatMask.getFlagBits(true, ~value));
        if ( !isLong) {
            assertEquals(value,edu.umd.cs.findbugs.detect.IncompatMask.getFlagBits(false,value));
            assertEquals(value,edu.umd.cs.findbugs.detect.IncompatMask.getFlagBits(false, ~value));
        }
    }
    public void testGetFlagBits() {
        this.check(1);
        this.check(4);
        this.check(0x10000000L);
        this.check(0x80000000L);
        this.check(0x100000000L);
        this.check(0x10000000000L);
        this.check(java.lang.Long.MIN_VALUE >>> 1);
        this.check(java.lang.Long.MIN_VALUE);
    }
}
