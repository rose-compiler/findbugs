package edu.umd.cs.findbugs.ba.npe;
import edu.umd.cs.findbugs.ba.npe.*;
import junit.framework.Assert;
import junit.framework.TestCase;
import edu.umd.cs.findbugs.ba.interproc.ParameterProperty;
public class NonNullParamPropertyTest extends junit.framework.TestCase {
    public NonNullParamPropertyTest() {
    }
    edu.umd.cs.findbugs.ba.interproc.ParameterProperty empty;
    edu.umd.cs.findbugs.ba.interproc.ParameterProperty nonEmpty;
    edu.umd.cs.findbugs.ba.interproc.ParameterProperty extremes;
    protected void setUp() throws java.lang.Exception {
        empty = new edu.umd.cs.findbugs.ba.interproc.ParameterProperty();
        nonEmpty = new edu.umd.cs.findbugs.ba.interproc.ParameterProperty();
        nonEmpty.setParamWithProperty(11,true);
        nonEmpty.setParamWithProperty(25,true);
        extremes = new edu.umd.cs.findbugs.ba.interproc.ParameterProperty();
        extremes.setParamWithProperty(0,true);
        extremes.setParamWithProperty(31,true);
    }
    public void testEmpty() {
        for (int i = 0; i < 32; ++i) {
            junit.framework.Assert.assertFalse(empty.hasProperty(i));
        }
    }
    public void testIsEmpty() {
        junit.framework.Assert.assertTrue(empty.isEmpty());
        junit.framework.Assert.assertFalse(nonEmpty.isEmpty());
        junit.framework.Assert.assertFalse(extremes.isEmpty());
    }
    public void testNonEmpty() {
        junit.framework.Assert.assertTrue(nonEmpty.hasProperty(11));
        junit.framework.Assert.assertTrue(nonEmpty.hasProperty(25));
        junit.framework.Assert.assertFalse(nonEmpty.hasProperty(5));
    }
    public void testExtremes() {
        junit.framework.Assert.assertTrue(extremes.hasProperty(0));
        junit.framework.Assert.assertTrue(extremes.hasProperty(31));
        junit.framework.Assert.assertFalse(extremes.hasProperty(10));
    }
    public void testOutOfBounds() {
        junit.framework.Assert.assertFalse(nonEmpty.hasProperty( -1));
        junit.framework.Assert.assertFalse(nonEmpty.hasProperty(32));
    }
}
