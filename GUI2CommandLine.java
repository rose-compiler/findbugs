/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Command line switches/options for GUI2.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.io.File;
import java.io.IOException;
import javax.swing.UIManager;
import edu.umd.cs.findbugs.FindBugsCommandLine;
public class GUI2CommandLine extends edu.umd.cs.findbugs.FindBugsCommandLine {
    private float fontSize = 12;
    private boolean fontSizeSpecified = false;
    private boolean docking = true;
    private int priority = java.lang.Thread.NORM_PRIORITY - 1;
    private java.io.File saveFile;
    public GUI2CommandLine() {
// Additional constuctor just as hack for decoupling the core package
// from gui2 package
// please add all options in the super class
        super(true);
    }
    protected void handleOption(java.lang.String option, java.lang.String optionExtraPart) {
        if (option.equals("-clear")) {
            edu.umd.cs.findbugs.gui2.GUISaveState.clear();
            java.lang.System.exit(0);
        }
        else if (option.equals("-d") || option.equals("--nodock")) {
            docking = false;
        }
        else if (option.equals("-look")) {
            java.lang.String arg = optionExtraPart;
            java.lang.String theme = null;
            if (arg.equals("plastic")) {
                theme = "com.jgoodies.plaf.plastic.PlasticXPLookAndFeel";
            }
            else if (arg.equals("gtk")) {
                theme = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
            }
            else if (arg.equals("native")) {
                theme = javax.swing.UIManager.getSystemLookAndFeelClassName();
            }
            else {
                java.lang.System.err.println("Style '" + arg + "' not supported");
            }
            if (theme != null) {
                try {
                    javax.swing.UIManager.setLookAndFeel(theme);
                }
                catch (java.lang.Exception e){
                    java.lang.System.err.println("Couldn't load " + arg + " look and feel: " + e.toString());
                }
            }
        }
        else {
            super.handleOption(option,optionExtraPart);
        }
    }
    protected void handleOptionWithArgument(java.lang.String option, java.lang.String argument) throws java.io.IOException {
        if (option.equals("-f")) {
            try {
                fontSize = java.lang.Float.parseFloat(argument);
                fontSizeSpecified = true;
            }
            catch (java.lang.NumberFormatException e){
            }
        }
        else if (option.equals("-priority")) {
            try {
                priority = java.lang.Integer.parseInt(argument);
            }
            catch (java.lang.NumberFormatException e){
            }
        }
        else if (option.equals("-loadBugs") || option.equals("-loadbugs")) {
            saveFile = new java.io.File(argument);
            if ( !saveFile.exists()) {
                java.lang.System.err.println("Bugs file \"" + argument + "\" could not be found");
                java.lang.System.exit(1);
            }
        }
        else {
            super.handleOptionWithArgument(option,argument);
        }
    }
    public float getFontSize() {
        return fontSize;
    }
    public boolean isFontSizeSpecified() {
        return fontSizeSpecified;
    }
    public boolean getDocking() {
        return docking;
    }
    public void setDocking(boolean docking) {
        this.docking = docking;
    }
    public int getPriority() {
        return priority;
    }
    public java.io.File getSaveFile() {
        return saveFile;
    }
    public void setSaveFile(java.io.File saveFile) {
        this.saveFile = saveFile;
    }
}
