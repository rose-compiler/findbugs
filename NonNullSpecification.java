/*
 * Bytecode analysis framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba.npe;
import edu.umd.cs.findbugs.ba.npe.*;
import edu.umd.cs.findbugs.ba.JavaClassAndMethod;
import edu.umd.cs.findbugs.ba.interproc.ParameterProperty;
public class NonNullSpecification extends java.lang.Object {
    final private edu.umd.cs.findbugs.ba.JavaClassAndMethod classAndMethod;
    final private edu.umd.cs.findbugs.ba.interproc.ParameterProperty nonNullProperty;
    final private edu.umd.cs.findbugs.ba.interproc.ParameterProperty possiblyNullProperty;
    public NonNullSpecification(edu.umd.cs.findbugs.ba.JavaClassAndMethod classAndMethod, edu.umd.cs.findbugs.ba.interproc.ParameterProperty nonParamProperty, edu.umd.cs.findbugs.ba.interproc.ParameterProperty possiblyNullProperty) {
        super();
        this.classAndMethod = classAndMethod;
        this.nonNullProperty = nonParamProperty;
        this.possiblyNullProperty = possiblyNullProperty;
    }
    public edu.umd.cs.findbugs.ba.JavaClassAndMethod getClassAndMethod() {
        return classAndMethod;
    }
    public edu.umd.cs.findbugs.ba.interproc.ParameterProperty getNonNullProperty() {
        return nonNullProperty;
    }
    public edu.umd.cs.findbugs.ba.interproc.ParameterProperty getCheckForNullProperty() {
        return possiblyNullProperty;
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append(classAndMethod);
        buf.append(":");
        if ( !nonNullProperty.isEmpty()) {
            buf.append(" nonull=");
            buf.append(nonNullProperty);
        }
        if ( !possiblyNullProperty.isEmpty()) {
            buf.append(" possiblynull=");
            buf.append(possiblyNullProperty);
        }
        return buf.toString();
    }
}
