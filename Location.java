/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A class representing a location in the CFG for a method. Essentially, it
 * represents a static instruction, <em>with the important caveat</em> that CFGs
 * have inlined JSR subroutines, meaning that a single InstructionHandle in a
 * CFG may represent several static locations. To this end, a Location is
 * comprised of both an InstructionHandle and the BasicBlock that contains it.
 * <p/>
 * <p>
 * Location objects may be compared with each other using the equals() method,
 * and may be used as keys in tree and hash maps and sets. Note that
 * <em>it is only valid to compare Locations produced from the same CFG</em>.
 * 
 * @author David Hovemeyer
 * @see CFG
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import javax.annotation.Nonnull;
import org.apache.bcel.generic.InstructionHandle;
public class Location extends java.lang.Object implements java.lang.Comparable<edu.umd.cs.findbugs.ba.Location> {
    final private org.apache.bcel.generic.InstructionHandle handle;
    final private edu.umd.cs.findbugs.ba.BasicBlock basicBlock;
    private int hash;
/**
     * Constructor.
     * 
     * @param handle
     *            the instruction
     * @param basicBlock
     *            the basic block containing the instruction
     */
    public Location(org.apache.bcel.generic.InstructionHandle handle, edu.umd.cs.findbugs.ba.BasicBlock basicBlock) {
        super();
        if (handle == null) throw new java.lang.NullPointerException("handle cannot be null");
        if (basicBlock == null) throw new java.lang.NullPointerException("basicBlock cannot be null");
        this.handle = handle;
        this.basicBlock = basicBlock;
    }
    public static edu.umd.cs.findbugs.ba.Location getFirstLocation(edu.umd.cs.findbugs.ba.BasicBlock basicBlock) {
        org.apache.bcel.generic.InstructionHandle location = basicBlock.getFirstInstruction();
        if (location == null) return null;
        return new edu.umd.cs.findbugs.ba.Location(location, basicBlock);
    }
    public static edu.umd.cs.findbugs.ba.Location getLastLocation(edu.umd.cs.findbugs.ba.BasicBlock basicBlock) {
        org.apache.bcel.generic.InstructionHandle lastInstruction = basicBlock.getLastInstruction();
/*
         * if (lastInstruction == null) lastInstruction =
         * basicBlock.getExceptionThrower(); if (lastInstruction == null)
         * lastInstruction = basicBlock.getFirstInstruction();
         */
        if (lastInstruction == null) return null;
        return new edu.umd.cs.findbugs.ba.Location(lastInstruction, basicBlock);
    }
/**
     * Get the instruction handle.
     */
    public org.apache.bcel.generic.InstructionHandle getHandle() {
        return handle;
    }
/**
     * Get the basic block.
     */
    public edu.umd.cs.findbugs.ba.BasicBlock getBasicBlock() {
        return basicBlock;
    }
/**
     * Return whether or not the Location is positioned at the first instruction
     * in the basic block.
     */
    public boolean isFirstInstructionInBasicBlock() {
        return  !basicBlock.isEmpty() && handle == basicBlock.getFirstInstruction();
    }
/**
     * Return whether or not the Location is positioned at the last instruction
     * in the basic block.
     */
    public boolean isLastInstructionInBasicBlock() {
        return  !basicBlock.isEmpty() && handle == basicBlock.getLastInstruction();
    }
    public int compareTo(edu.umd.cs.findbugs.ba.Location other) {
        int pos = handle.getPosition() - other.handle.getPosition();
        return pos;
    }
    public int hashCode() {
        if (hash == 0) {
            return hash = java.lang.System.identityHashCode(basicBlock) + handle.getPosition();
        }
        return hash;
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.ba.Location)) return false;
        edu.umd.cs.findbugs.ba.Location other = (edu.umd.cs.findbugs.ba.Location) (o) ;
        return basicBlock == other.basicBlock && handle == other.handle;
    }
    public java.lang.String toString() {
        return handle.toString() + " in basic block " + basicBlock.getLabel();
    }
/**
     * @return a compact string of the form "bb:xx", where "bb" is the basic
     *         block number and "xx" is the bytecode offset
     */
    public java.lang.String toCompactString() {
        return basicBlock.getLabel() + ":" + handle.getPosition();
    }
}
// vim:ts=4
