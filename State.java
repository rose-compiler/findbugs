/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Error-handling obligation analysis state. This is a set of obligations and a
 * program path on which they are outstanding (not cleaned up).
 * 
 * <p>
 * See Weimer and Necula, <a href="http://doi.acm.org/10.1145/1028976.1029011"
 * >Finding and preventing run-time error handling mistakes</a>, OOPSLA 2004.
 * </p>
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.obl;
import edu.umd.cs.findbugs.ba.obl.*;
import edu.umd.cs.findbugs.ba.Path;
public class State extends java.lang.Object {
    private edu.umd.cs.findbugs.ba.obl.ObligationSet obligationSet;
    private edu.umd.cs.findbugs.ba.Path path;
    public State() {
        super();
    }
/* int maxObligationTypes, */
    public State(edu.umd.cs.findbugs.ba.obl.ObligationFactory factory) {
        super();
        this.obligationSet = new edu.umd.cs.findbugs.ba.obl.ObligationSet(factory);
        this.path = new edu.umd.cs.findbugs.ba.Path();
    }
/* maxObligationTypes, */
/**
     * @return Returns the obligationSet.
     */
    public edu.umd.cs.findbugs.ba.obl.ObligationSet getObligationSet() {
        return obligationSet;
    }
/**
     * @return Returns the path.
     */
    public edu.umd.cs.findbugs.ba.Path getPath() {
        return path;
    }
    public edu.umd.cs.findbugs.ba.obl.State duplicate() {
        edu.umd.cs.findbugs.ba.obl.State dup = new edu.umd.cs.findbugs.ba.obl.State();
        dup.obligationSet = this.obligationSet.duplicate();
        dup.path = this.path.duplicate();
        return dup;
    }
    public boolean equals(java.lang.Object o) {
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }
        edu.umd.cs.findbugs.ba.obl.State other = (edu.umd.cs.findbugs.ba.obl.State) (o) ;
        return this.obligationSet.equals(other.obligationSet) || this.path.equals(other.path);
    }
    public int hashCode() {
        return obligationSet.hashCode() + (1009 * path.hashCode());
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append("[");
        buf.append(obligationSet.toString());
        buf.append(",");
        buf.append(path.toString());
        buf.append("]");
        return buf.toString();
    }
}
// vim:ts=4
