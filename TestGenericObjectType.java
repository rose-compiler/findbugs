/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author Nat Ayewah
 */
package edu.umd.cs.findbugs.ba.generic;
import edu.umd.cs.findbugs.ba.generic.*;
import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory;
public class TestGenericObjectType extends junit.framework.TestCase {
    public TestGenericObjectType() {
    }
    edu.umd.cs.findbugs.ba.generic.GenericObjectType obj;
    java.lang.String javaSignature;
    java.lang.String underlyingClass;
    edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory typeCategory;
    java.lang.String variable;
    org.apache.bcel.generic.Type extension;
    java.util.List<org.apache.bcel.generic.ReferenceType> parameters;
    public void initTest(java.lang.String bytecodeSignature, java.lang.String javaSignature, java.lang.String underlyingClass, edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory typeCategory, java.lang.String variable, org.apache.bcel.generic.Type extension, java.util.List<org.apache.bcel.generic.ReferenceType> parameters) {
        this.obj = (edu.umd.cs.findbugs.ba.generic.GenericObjectType) (edu.umd.cs.findbugs.ba.generic.GenericUtilities.getType(bytecodeSignature)) ;
        this.javaSignature = javaSignature;
        this.underlyingClass = underlyingClass;
        this.typeCategory = typeCategory;
        this.variable = variable;
        this.extension = extension;
        this.parameters = parameters;
    }
    public void processTest() {
        assertEquals(obj.toString(true),javaSignature);
        assertEquals(obj.getClassName(),underlyingClass);
        assertEquals(obj.getTypeCategory(),typeCategory);
        if (typeCategory == edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.PARAMETERIZED) {
            assertTrue(obj.hasParameters());
            assertTrue(obj.getNumParameters() == parameters.size());
            for (int i = 0; i < obj.getNumParameters(); i++) this.compareTypes(obj.getParameterAt(i),parameters.get(i));
            assertNull(obj.getVariable());
            assertNull(obj.getExtension());
        }
        else if (typeCategory == edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.TYPE_VARIABLE) {
            assertFalse(obj.hasParameters());
            assertNull(obj.getExtension());
            assertNotNull(obj.getVariable());
            assertEquals(obj.getVariable(),variable);
        }
        else if (typeCategory == edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.WILDCARD) {
            assertFalse(obj.hasParameters());
            assertNull(obj.getExtension());
            assertNotNull(obj.getVariable());
            assertEquals(obj.getVariable(),"*");
        }
        else if (typeCategory == edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.WILDCARD_EXTENDS || typeCategory == edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.WILDCARD_SUPER) {
            assertFalse(obj.hasParameters());
            assertNotNull(obj.getExtension());
            assertNotNull(obj.getVariable());
            assertEquals(obj.getVariable(),variable);
            this.compareTypes(obj.getExtension(),extension);
        }
    }
    private void compareTypes(org.apache.bcel.generic.Type a, org.apache.bcel.generic.Type b) {
        assertEquals(a,b);
        if (a instanceof edu.umd.cs.findbugs.ba.generic.GenericObjectType || b instanceof edu.umd.cs.findbugs.ba.generic.GenericObjectType) {
            assertTrue(a instanceof edu.umd.cs.findbugs.ba.generic.GenericObjectType && b instanceof edu.umd.cs.findbugs.ba.generic.GenericObjectType);
            assertEquals(((edu.umd.cs.findbugs.ba.generic.GenericObjectType) (a) ).toString(true),((edu.umd.cs.findbugs.ba.generic.GenericObjectType) (b) ).toString(true));
        }
    }
    public void testParameterizedList() {
        this.initTest("Ljava/util/List<Ljava/lang/Comparable;>;","java.util.List<java.lang.Comparable>","java.util.List",edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.PARAMETERIZED,null,null,edu.umd.cs.findbugs.ba.generic.GenericUtilities.getTypeParameters("Ljava/lang/Comparable;"));
        this.processTest();
    }
    public void notestCreateTypes() {
        this.initTest("LDummyClass<Ljava/lang/Comparable;TE;>;","DummyClass<java.lang.Comparable,E>","DummyClass",edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.PARAMETERIZED,null,null,java.util.Arrays.asList((org.apache.bcel.generic.ReferenceType) (edu.umd.cs.findbugs.ba.generic.GenericUtilities.getType("Ljava/lang/Comparable;")) ,(org.apache.bcel.generic.ReferenceType) (edu.umd.cs.findbugs.ba.generic.GenericUtilities.getType("TE;")) ));
        this.processTest();
    }
    public void notestTypeVariables() {
        this.initTest("TE;","E","java.lang.Object",edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.TYPE_VARIABLE,"E",null,null);
        this.processTest();
        this.initTest("*","?","java.lang.Object",edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.WILDCARD,"*",null,null);
        this.processTest();
        this.initTest("+TE;","? extends E","java.lang.Object",edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.WILDCARD_EXTENDS,"+",edu.umd.cs.findbugs.ba.generic.GenericUtilities.getType("TE;"),null);
        this.processTest();
        this.initTest("-TE;","? super E","java.lang.Object",edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.WILDCARD_SUPER,"-",edu.umd.cs.findbugs.ba.generic.GenericUtilities.getType("TE;"),null);
        this.processTest();
        this.initTest("-[TE;","? super E[]","java.lang.Object",edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.WILDCARD_SUPER,"-",edu.umd.cs.findbugs.ba.generic.GenericUtilities.getType("[TE;"),null);
        this.processTest();
    }
}
