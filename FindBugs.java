/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Static methods and fields useful for working with instances of
 * IFindBugsEngine.
 *
 * This class was previously the main driver for FindBugs analyses, but has been
 * replaced by {@link FindBugs2 FindBugs2}.
 *
 * @author Bill Pugh
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.annotations.SuppressWarnings;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.AnalysisFeatures;
import edu.umd.cs.findbugs.config.AnalysisFeatureSetting;
import edu.umd.cs.findbugs.config.CommandLine.HelpRequestedException;
import edu.umd.cs.findbugs.filter.Filter;
import edu.umd.cs.findbugs.filter.FilterException;
import edu.umd.cs.findbugs.updates.UpdateChecker;
import edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate;
import edu.umd.cs.findbugs.util.FutureValue;
abstract public class FindBugs extends java.lang.Object {
    public FindBugs() {
    }
/**
     * Analysis settings for -effort:min.
     */
    final public static edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] MIN_EFFORT = new edu.umd.cs.findbugs.config.AnalysisFeatureSetting[]{new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.CONSERVE_SPACE, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.ACCURATE_EXCEPTIONS, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MERGE_SIMILAR_WARNINGS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MODEL_INSTANCEOF, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.SKIP_HUGE_METHODS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.INTERATIVE_OPCODE_STACK_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_GUARANTEED_VALUE_DEREFS_IN_NULL_POINTER_ANALYSIS, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_VALUE_NUMBERS_IN_NULL_POINTER_ANALYSIS, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS_OF_REFERENCED_CLASSES, false)};
/**
     * Analysis settings for -effort:less.
     */
    final public static edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] LESS_EFFORT = new edu.umd.cs.findbugs.config.AnalysisFeatureSetting[]{new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.CONSERVE_SPACE, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.ACCURATE_EXCEPTIONS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MERGE_SIMILAR_WARNINGS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MODEL_INSTANCEOF, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.SKIP_HUGE_METHODS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.INTERATIVE_OPCODE_STACK_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_GUARANTEED_VALUE_DEREFS_IN_NULL_POINTER_ANALYSIS, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_VALUE_NUMBERS_IN_NULL_POINTER_ANALYSIS, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS_OF_REFERENCED_CLASSES, false)};
/**
     * Analysis settings for -effort:default.
     */
    final public static edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] DEFAULT_EFFORT = new edu.umd.cs.findbugs.config.AnalysisFeatureSetting[]{new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.CONSERVE_SPACE, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.ACCURATE_EXCEPTIONS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MERGE_SIMILAR_WARNINGS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MODEL_INSTANCEOF, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.SKIP_HUGE_METHODS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.INTERATIVE_OPCODE_STACK_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_GUARANTEED_VALUE_DEREFS_IN_NULL_POINTER_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_VALUE_NUMBERS_IN_NULL_POINTER_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS_OF_REFERENCED_CLASSES, false)};
/**
     * Analysis settings for -effort:more.
     */
    final public static edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] MORE_EFFORT = new edu.umd.cs.findbugs.config.AnalysisFeatureSetting[]{new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.CONSERVE_SPACE, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.ACCURATE_EXCEPTIONS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MERGE_SIMILAR_WARNINGS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MODEL_INSTANCEOF, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.SKIP_HUGE_METHODS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.INTERATIVE_OPCODE_STACK_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_GUARANTEED_VALUE_DEREFS_IN_NULL_POINTER_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_VALUE_NUMBERS_IN_NULL_POINTER_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS_OF_REFERENCED_CLASSES, false)};
/**
     * Analysis settings for -effort:max.
     */
    final public static edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] MAX_EFFORT = new edu.umd.cs.findbugs.config.AnalysisFeatureSetting[]{new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.CONSERVE_SPACE, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.ACCURATE_EXCEPTIONS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MERGE_SIMILAR_WARNINGS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.MODEL_INSTANCEOF, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.SKIP_HUGE_METHODS, false), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.INTERATIVE_OPCODE_STACK_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_GUARANTEED_VALUE_DEREFS_IN_NULL_POINTER_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.ba.AnalysisFeatures.TRACK_VALUE_NUMBERS_IN_NULL_POINTER_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS, true), new edu.umd.cs.findbugs.config.AnalysisFeatureSetting(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS_OF_REFERENCED_CLASSES, true)};
/**
     * Debug tracing.
     */
    final public static boolean DEBUG = java.lang.Boolean.getBoolean("findbugs.debug");
/**
     * FindBugs home directory.
     */
    private static java.lang.String home = java.lang.System.getProperty("findbugs.home");
    private static boolean noAnalysis = java.lang.Boolean.getBoolean("findbugs.noAnalysis");
/**
     * Disable analysis within FindBugs. Turns off loading of bug detectors.
     */
    public static void setNoAnalysis() {
        noAnalysis = true;
    }
/**
     * @return Returns the noAnalysis.
     */
    public static boolean isNoAnalysis() {
        return noAnalysis;
    }
    private static boolean noMains = java.lang.Boolean.getBoolean("findbugs.noMains");
/**
     * Disable loading of FindBugsMain classes.
     */
    public static void setNoMains() {
        noMains = true;
    }
/**
     * @return Returns the noMains.
     */
    public static boolean isNoMains() {
        return noMains;
    }
    final public static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.FindBugs.class.getPackage().getName());
    static {
        LOGGER.setLevel(java.util.logging.Level.WARNING);
    }
/**
     * Known URL protocols. Filename URLs that do not have an explicit protocol
     * are assumed to be files.
     */
    final public static java.util.Set<java.lang.String> knownURLProtocolSet = new java.util.HashSet<java.lang.String>();
    static {
        knownURLProtocolSet.add("file");
        knownURLProtocolSet.add("http");
        knownURLProtocolSet.add("https");
        knownURLProtocolSet.add("jar");
    }
/**
     * Set the FindBugs home directory.
     */
    public static void setHome(java.lang.String home) {
        edu.umd.cs.findbugs.FindBugs.home = home;
    }
/**
     * Get the FindBugs home directory.
     */
    public static java.lang.String getHome() {
        return home;
    }
/**
     * Configure training databases.
     *
     * @param findBugs
     *            the IFindBugsEngine to configure
     * @throws IOException
     */
    public static void configureTrainingDatabases(edu.umd.cs.findbugs.IFindBugsEngine findBugs) throws java.io.IOException {
        if (findBugs.emitTrainingOutput()) {
            java.lang.String trainingOutputDir = findBugs.getTrainingOutputDir();
            if ( !new java.io.File(trainingOutputDir).isDirectory()) throw new java.io.IOException("Training output directory " + trainingOutputDir + " does not exist");
            edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().setDatabaseOutputDir(trainingOutputDir);
            java.lang.System.setProperty("findbugs.checkreturn.savetraining",new java.io.File(trainingOutputDir, "checkReturn.db").getPath());
        }
        if (findBugs.useTrainingInput()) {
            java.lang.String trainingInputDir = findBugs.getTrainingInputDir();
            if ( !new java.io.File(trainingInputDir).isDirectory()) throw new java.io.IOException("Training input directory " + trainingInputDir + " does not exist");
            edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().setDatabaseInputDir(trainingInputDir);
            edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().loadInterproceduralDatabases();
            java.lang.System.setProperty("findbugs.checkreturn.loadtraining",new java.io.File(trainingInputDir, "checkReturn.db").getPath());
        }
        else {
            edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().loadDefaultInterproceduralDatabases();
        }
    }
/**
     * Determines whether or not given DetectorFactory should be enabled.
     *
     * @param findBugs
     *            the IFindBugsEngine
     * @param factory
     *            the DetectorFactory
     * @param rankThreshold
     *            TODO
     * @return true if the DetectorFactory should be enabled, false otherwise
     */
    public static boolean isDetectorEnabled(edu.umd.cs.findbugs.IFindBugsEngine findBugs, edu.umd.cs.findbugs.DetectorFactory factory, int rankThreshold) {
        if ( !findBugs.getUserPreferences().isDetectorEnabled(factory)) return false;
        if ( !factory.isEnabledForCurrentJRE()) return false;
// Slow first pass detectors are usually disabled, but may be explicitly
// enabled
        if ( !edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getBoolProperty(edu.umd.cs.findbugs.FindBugsAnalysisFeatures.INTERPROCEDURAL_ANALYSIS) && factory.isDetectorClassSubtypeOf(edu.umd.cs.findbugs.InterproceduralFirstPassDetector.class)) return false;
        int maxRank = java.lang.Integer.MAX_VALUE;
        java.util.Set<edu.umd.cs.findbugs.BugPattern> reportedBugPatterns = factory.getReportedBugPatterns();
        boolean isNonReportingDetector = factory.isDetectorClassSubtypeOf(edu.umd.cs.findbugs.FirstPassDetector.class);
        if ( !isNonReportingDetector &&  !reportedBugPatterns.isEmpty()) {
            for (edu.umd.cs.findbugs.BugPattern b : reportedBugPatterns){
                int rank = edu.umd.cs.findbugs.BugRanker.findRank(b,factory);
                if (maxRank > rank) maxRank = rank;
            }
;
            if (maxRank > rankThreshold) {
                if (false) {
                    java.lang.System.out.println("Detector " + factory.getShortName() + " has max rank " + maxRank + ", disabling");
                    java.lang.System.out.println("Reports : " + reportedBugPatterns);
                }
                return false;
            }
        }
// Training detectors are enabled if, and only if, we are emitting
// training output
        boolean isTrainingDetector = factory.isDetectorClassSubtypeOf(edu.umd.cs.findbugs.TrainingDetector.class);
        if (findBugs.emitTrainingOutput()) {
            return isTrainingDetector || isNonReportingDetector;
        }
        if (isTrainingDetector) return false;
        return true;
    }
/**
     * Process -bugCategories option.
     *
     * @param categories
     *            comma-separated list of bug categories
     *
     * @return Set of categories to be used
     */
    public static java.util.Set<java.lang.String> handleBugCategories(java.lang.String categories) {
// Parse list of bug categories
        java.util.Set<java.lang.String> categorySet = new java.util.HashSet<java.lang.String>();
        java.util.StringTokenizer tok = new java.util.StringTokenizer(categories, ",");
        while (tok.hasMoreTokens()) {
            categorySet.add(tok.nextToken());
        }
        return categorySet;
    }
/**
     * Process the command line.
     *
     * @param commandLine
     *            the TextUICommandLine object which will parse the command line
     * @param argv
     *            the command line arguments
     * @param findBugs
     *            the IFindBugsEngine to configure
     * @throws IOException
     * @throws FilterException
     */
    public static void processCommandLine(edu.umd.cs.findbugs.TextUICommandLine commandLine, java.lang.String[] argv, edu.umd.cs.findbugs.IFindBugsEngine findBugs) throws edu.umd.cs.findbugs.filter.FilterException, java.io.IOException {
// Expand option files in command line.
// An argument beginning with "@" is treated as specifying
// the name of an option file.
// Each line of option files are treated as a single argument.
// Blank lines and comment lines (beginning with "#")
// are ignored.
        try {
            argv = commandLine.expandOptionFiles(argv,true,true);
        }
        catch (edu.umd.cs.findbugs.config.CommandLine.HelpRequestedException e){
            showHelp(commandLine);
        }
        int argCount = 0;
        try {
            argCount = commandLine.parse(argv);
        }
        catch (java.lang.IllegalArgumentException e){
            java.lang.System.out.println(e.getMessage());
            showHelp(commandLine);
        }
        catch (edu.umd.cs.findbugs.config.CommandLine.HelpRequestedException e){
            showHelp(commandLine);
        }
        edu.umd.cs.findbugs.Project project = commandLine.getProject();
        for (int i = argCount; i < argv.length; ++i) project.addFile(argv[i]);
        commandLine.handleXArgs();
        commandLine.configureEngine(findBugs);
        if (commandLine.getProject().getFileCount() == 0 &&  !commandLine.justPrintConfiguration() &&  !commandLine.justPrintVersion()) {
            java.lang.System.out.println("No files to be analyzed");
            showHelp(commandLine);
        }
    }
/**
     * Show -help message.
     *
     * @param commandLine
     */
    public static void showHelp(edu.umd.cs.findbugs.TextUICommandLine commandLine) {
        showSynopsis();
        edu.umd.cs.findbugs.ShowHelp.showGeneralOptions();
        edu.umd.cs.findbugs.FindBugs.showCommandLineOptions(commandLine);
        java.lang.System.exit(1);
    }
/**
     * Given a fully-configured IFindBugsEngine and the TextUICommandLine used
     * to configure it, execute the analysis.
     *
     * @param findBugs
     *            a fully-configured IFindBugsEngine
     * @param commandLine
     *            the TextUICommandLine used to configure the IFindBugsEngine
     */
    public static void runMain(edu.umd.cs.findbugs.IFindBugsEngine findBugs, edu.umd.cs.findbugs.TextUICommandLine commandLine) throws java.io.IOException {
        boolean verbose =  !commandLine.quiet() || commandLine.setExitCode();
        edu.umd.cs.findbugs.util.FutureValue<java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate>> updateHolder = null;
        if (verbose) updateHolder = edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getUpdates();
        try {
            findBugs.execute();
        }
        catch (java.lang.InterruptedException e){
// should not occur
            assert false;
            checkExitCodeFail(commandLine,e);
            throw new java.lang.RuntimeException(e);
        }
        catch (java.lang.RuntimeException e){
            checkExitCodeFail(commandLine,e);
            throw e;
        }
        catch (java.io.IOException e){
            checkExitCodeFail(commandLine,e);
            throw e;
        }
        int bugCount = findBugs.getBugCount();
        int missingClassCount = findBugs.getMissingClassCount();
        int errorCount = findBugs.getErrorCount();
        if (verbose) {
            if (bugCount > 0) java.lang.System.err.println("Warnings generated: " + bugCount);
            if (missingClassCount > 0) java.lang.System.err.println("Missing classes: " + missingClassCount);
            if (errorCount > 0) java.lang.System.err.println("Analysis errors: " + errorCount);
            if (updateHolder.isDone()) {
                try {
                    java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates = updateHolder.get();
                    if ( !edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getUpdateChecker().updatesHaveBeenSeenBefore(updates)) for (edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate u : updates){
                        java.lang.System.err.println(u);
                    }
;
                }
                catch (java.lang.InterruptedException e){
                    assert true;
                }
            }
        }
        if (commandLine.setExitCode()) {
            int exitCode = 0;
            java.lang.System.err.println("Calculating exit code...");
            if (errorCount > 0) {
                exitCode |= edu.umd.cs.findbugs.ExitCodes.ERROR_FLAG;
                java.lang.System.err.println("Setting 'errors encountered' flag (" + edu.umd.cs.findbugs.ExitCodes.ERROR_FLAG + ")");
            }
            if (missingClassCount > 0) {
                exitCode |= edu.umd.cs.findbugs.ExitCodes.MISSING_CLASS_FLAG;
                java.lang.System.err.println("Setting 'missing class' flag (" + edu.umd.cs.findbugs.ExitCodes.MISSING_CLASS_FLAG + ")");
            }
            if (bugCount > 0) {
                exitCode |= edu.umd.cs.findbugs.ExitCodes.BUGS_FOUND_FLAG;
                java.lang.System.err.println("Setting 'bugs found' flag (" + edu.umd.cs.findbugs.ExitCodes.BUGS_FOUND_FLAG + ")");
            }
            java.lang.System.err.println("Exit code set to: " + exitCode);
            java.lang.System.exit(exitCode);
        }
    }
/**
     * @param commandLine
     * @param e
     */
    private static void checkExitCodeFail(edu.umd.cs.findbugs.TextUICommandLine commandLine, java.lang.Exception e) {
        if (commandLine.setExitCode()) {
            e.printStackTrace(java.lang.System.err);
            java.lang.System.exit(edu.umd.cs.findbugs.ExitCodes.ERROR_FLAG);
        }
    }
/**
     * Print command line options synopses to stdout.
     */
    public static void showCommandLineOptions() {
        showCommandLineOptions(new edu.umd.cs.findbugs.TextUICommandLine());
    }
/**
     * Print command line options synopses to stdout.
     *
     * @param commandLine
     *            the TextUICommandLine whose options should be printed
     */
    public static void showCommandLineOptions(edu.umd.cs.findbugs.TextUICommandLine commandLine) {
        commandLine.printUsage(java.lang.System.out);
    }
/**
     * Show the overall FindBugs command synopsis.
     */
    public static void showSynopsis() {
        java.lang.System.out.println("Usage: findbugs [general options] -textui [command line options...] [jar/zip/class files, directories...]");
    }
/**
     * Configure the (bug instance) Filter for the given DelegatingBugReporter.
     *
     * @param bugReporter
     *            a DelegatingBugReporter
     * @param filterFileName
     *            filter file name
     * @param include
     *            true if the filter is an include filter, false if it's an
     *            exclude filter
     * @throws java.io.IOException
     * @throws edu.umd.cs.findbugs.filter.FilterException
     */
    public static edu.umd.cs.findbugs.BugReporter configureFilter(edu.umd.cs.findbugs.BugReporter bugReporter, java.lang.String filterFileName, boolean include) throws edu.umd.cs.findbugs.filter.FilterException, java.io.IOException {
        edu.umd.cs.findbugs.filter.Filter filter = new edu.umd.cs.findbugs.filter.Filter(filterFileName);
        return new edu.umd.cs.findbugs.FilterBugReporter(bugReporter, filter, include);
    }
/**
     * Configure a baseline bug instance filter.
     *
     * @param bugReporter
     *            a DelegatingBugReporter
     * @param baselineFileName
     *            filename of baseline Filter
     * @throws java.io.IOException
     * @throws org.dom4j.DocumentException
     */
    public static edu.umd.cs.findbugs.BugReporter configureBaselineFilter(edu.umd.cs.findbugs.BugReporter bugReporter, java.lang.String baselineFileName) throws org.dom4j.DocumentException, java.io.IOException {
        return new edu.umd.cs.findbugs.ExcludingHashesBugReporter(bugReporter, baselineFileName);
    }
/**
     * Configure the BugCollection (if the BugReporter being used is
     * constructing one).
     *
     * @param findBugs
     *            the IFindBugsEngine
     */
    public static void configureBugCollection(edu.umd.cs.findbugs.IFindBugsEngine findBugs) {
        edu.umd.cs.findbugs.BugCollection bugs = findBugs.getBugReporter().getBugCollection();
        if (bugs != null) {
            bugs.setReleaseName(findBugs.getReleaseName());
            edu.umd.cs.findbugs.Project project = findBugs.getProject();
            java.lang.String projectName = project.getProjectName();
            if (projectName == null) {
                projectName = findBugs.getProjectName();
                project.setProjectName(projectName);
            }
            long timestamp = project.getTimestamp();
            if (edu.umd.cs.findbugs.FindBugs.validTimestamp(timestamp)) {
                bugs.setTimestamp(timestamp);
                bugs.getProjectStats().setTimestamp(timestamp);
            }
        }
    }
/** Date of release of Java 1.0 */
    final public static long MINIMUM_TIMESTAMP = new java.util.GregorianCalendar(1996, 0, 23).getTime().getTime();
/**
     * @param timestamp
     * @return
     */
    public static boolean validTimestamp(long timestamp) {
        return timestamp > MINIMUM_TIMESTAMP;
    }
}
// vim:ts=4
