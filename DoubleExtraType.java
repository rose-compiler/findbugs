/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Special type used to represent the "extra" part of a double value. We say
 * that when a double is stored, local <i>n</i> will have type double, and local
 * <i>n+1</i> will have this type.
 * 
 * @author David Hovemeyer
 * @see TypeAnalysis
 * @see TypeFrame
 * @see TypeMerger
 */
package edu.umd.cs.findbugs.ba.type;
import edu.umd.cs.findbugs.ba.type.*;
import org.apache.bcel.generic.Type;
public class DoubleExtraType extends org.apache.bcel.generic.Type implements edu.umd.cs.findbugs.ba.type.ExtendedTypes {
/**
     *
     */
    final private static long serialVersionUID = 1L;
    final private static org.apache.bcel.generic.Type theInstance = new edu.umd.cs.findbugs.ba.type.DoubleExtraType();
    public DoubleExtraType() {
        super(T_DOUBLE_EXTRA,"<double extra>");
    }
    public int hashCode() {
        return java.lang.System.identityHashCode(this);
    }
    public boolean equals(java.lang.Object o) {
        return o == this;
    }
    public static org.apache.bcel.generic.Type instance() {
        return theInstance;
    }
}
// vim:ts=4
