/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pwilliam
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
public class ClearGuiSaveState extends java.lang.Object {
    public ClearGuiSaveState() {
    }
    public static void main(java.lang.String[] args) throws java.util.prefs.BackingStoreException {
        java.util.prefs.Preferences p = java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.gui2.GUISaveState.class);
        p.clear();
    }
}
