/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.util.ArrayList;
import java.util.Map;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.ProjectPackagePrefixes;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.util.Bag;
public class TestingGround extends java.lang.Object {
    static class CommandLine extends edu.umd.cs.findbugs.config.CommandLine {
        public CommandLine() {
        }
        public void handleOption(java.lang.String option, java.lang.String optionalExtraPart) {
            throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
        public void handleOptionWithArgument(java.lang.String option, java.lang.String argument) {
            throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
    }
    edu.umd.cs.findbugs.BugCollection bugCollection;
    public TestingGround() {
        super();
    }
    public TestingGround(edu.umd.cs.findbugs.BugCollection bugCollection) {
        super();
        this.bugCollection = bugCollection;
    }
    public void setBugCollection(edu.umd.cs.findbugs.BugCollection bugCollection) {
        this.bugCollection = bugCollection;
    }
    public edu.umd.cs.findbugs.workflow.TestingGround execute() {
        edu.umd.cs.findbugs.ProjectPackagePrefixes foo = new edu.umd.cs.findbugs.ProjectPackagePrefixes();
        for (edu.umd.cs.findbugs.BugInstance b : bugCollection.getCollection())foo.countBug(b);
;
        foo.report();
        return this;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
// load plugins
        edu.umd.cs.findbugs.workflow.TestingGround.CommandLine commandLine = new edu.umd.cs.findbugs.workflow.TestingGround.CommandLine();
        int argCount = commandLine.parse(args,0,2,"Usage: " + edu.umd.cs.findbugs.workflow.TestingGround.class.getName() + " [options] [<xml results>] ");
        edu.umd.cs.findbugs.SortedBugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (argCount < args.length) bugCollection.readXML(args[argCount++]);
        else bugCollection.readXML(java.lang.System.in);
        java.util.ArrayList<edu.umd.cs.findbugs.util.Bag<java.lang.String>> live = new java.util.ArrayList<edu.umd.cs.findbugs.util.Bag<java.lang.String>>();
        java.util.ArrayList<edu.umd.cs.findbugs.util.Bag<java.lang.String>> died = new java.util.ArrayList<edu.umd.cs.findbugs.util.Bag<java.lang.String>>();
        edu.umd.cs.findbugs.util.Bag<java.lang.String> allBugs = new edu.umd.cs.findbugs.util.Bag<java.lang.String>();
        for (int i = 0; i <= bugCollection.getSequenceNumber(); i++) {
            live.add(new edu.umd.cs.findbugs.util.Bag<java.lang.String>());
            died.add(new edu.umd.cs.findbugs.util.Bag<java.lang.String>());
        }
        for (edu.umd.cs.findbugs.BugInstance b : bugCollection){
            int first = (int) (b.getFirstVersion()) ;
            int buried = (int) (b.getLastVersion())  + 1;
            int finish = buried;
            if (finish == 0) finish = (int) (bugCollection.getSequenceNumber()) ;
            java.lang.String bugPattern = b.getBugPattern().getType();
            allBugs.add(bugPattern);
            for (int i = first; i <= finish; i++) live.get(i).add(bugPattern);
            if (buried > 0) died.get(buried).add(bugPattern);
        }
;
        for (int i = 0; i < bugCollection.getSequenceNumber(); i++) {
            for (java.util.Map.Entry<java.lang.String, java.lang.Integer> e : died.get(i).entrySet()){
                java.lang.Integer buried = e.getValue();
                int total = live.get(i).getCount(e.getKey());
                if (buried > 30 && buried * 3 > total) {
                    java.lang.System.out.printf("%d/%d died at %d for %s%n",buried,total,i,e.getKey());
                }
            }
;
        }
        edu.umd.cs.findbugs.SortedBugCollection results = bugCollection.createEmptyCollectionWithMetadata();
        for (edu.umd.cs.findbugs.BugInstance b : bugCollection){
            int buried = (int) (b.getLastVersion())  + 1;
            java.lang.String bugPattern = b.getBugPattern().getType();
            if (buried > 0) {
                int buriedCount = died.get(buried).getCount(bugPattern);
                int total = live.get(buried).getCount(bugPattern);
                if (buriedCount > 30 && buriedCount * 3 > total) continue;
            }
            int survied = live.get((int) (bugCollection.getSequenceNumber()) ).getCount(bugPattern);
            if (survied == 0 && allBugs.getCount(bugPattern) > 100) continue;
            results.add(b,false);
        }
;
        if (argCount == args.length) {
            results.writeXML(java.lang.System.out);
        }
        else {
            results.writeXML(args[argCount++]);
        }
    }
}
