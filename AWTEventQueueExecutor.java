package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.SwingUtilities;
public class AWTEventQueueExecutor extends java.util.concurrent.AbstractExecutorService {
    public AWTEventQueueExecutor() {
    }
    public void shutdown() {
    }
    public java.util.List<java.lang.Runnable> shutdownNow() {
        return java.util.Collections.emptyList();
    }
    public boolean isShutdown() {
        return true;
    }
    public boolean isTerminated() {
        return true;
    }
    public boolean awaitTermination(long timeout, java.util.concurrent.TimeUnit unit) throws java.lang.InterruptedException {
        return true;
    }
    public void execute(java.lang.Runnable command) {
        if (javax.swing.SwingUtilities.isEventDispatchThread()) {
            command.run();
            return;
        }
        try {
            javax.swing.SwingUtilities.invokeAndWait(command);
        }
        catch (java.lang.InterruptedException e){
            throw new java.lang.IllegalStateException(e);
        }
        catch (java.lang.reflect.InvocationTargetException e){
            throw new java.lang.IllegalStateException(e);
        }
    }
}
