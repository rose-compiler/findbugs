package edu.umd.cs.findbugs.ba.npe;
import edu.umd.cs.findbugs.ba.npe.*;
import junit.framework.TestCase;
public class IsNullValueTest extends junit.framework.TestCase {
    public IsNullValueTest() {
    }
    public void testMerge1() {
        edu.umd.cs.findbugs.ba.npe.IsNullValue nullValue = edu.umd.cs.findbugs.ba.npe.IsNullValue.nullValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue nullExceptionValue = edu.umd.cs.findbugs.ba.npe.IsNullValue.nullValue().toExceptionValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue result = edu.umd.cs.findbugs.ba.npe.IsNullValue.merge(nullValue,nullExceptionValue);
        assertTrue(result.isDefinitelyNull());
        assertFalse(result.isException());
    }
    public void testMerge2() {
        edu.umd.cs.findbugs.ba.npe.IsNullValue nullExceptionValue = edu.umd.cs.findbugs.ba.npe.IsNullValue.nullValue().toExceptionValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue nonNullValue = edu.umd.cs.findbugs.ba.npe.IsNullValue.nonNullValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue nsp_e = edu.umd.cs.findbugs.ba.npe.IsNullValue.merge(nonNullValue,nullExceptionValue);
        assertTrue(nsp_e.isNullOnSomePath());
        assertTrue(nsp_e.isException());
        assertEquals(nsp_e,edu.umd.cs.findbugs.ba.npe.IsNullValue.nullOnSimplePathValue().toExceptionValue());
    }
    public void testMerge3() {
        edu.umd.cs.findbugs.ba.npe.IsNullValue nullValue = edu.umd.cs.findbugs.ba.npe.IsNullValue.nullValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue nsp_e = edu.umd.cs.findbugs.ba.npe.IsNullValue.nullOnSimplePathValue().toExceptionValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue nsp = edu.umd.cs.findbugs.ba.npe.IsNullValue.merge(nullValue,nsp_e);
        assertTrue(nsp.isNullOnSomePath());
        assertFalse(nsp.isException());
    }
// public void testMerge4() {
// IsNullValue noKaboom = IsNullValue.noKaboomNonNullValue(null);
// IsNullValue nsp_e =
// IsNullValue.nullOnSimplePathValue().toExceptionValue();
// IsNullValue nsp_e2 = IsNullValue.merge(noKaboom, nsp_e);
// assertTrue(nsp_e2.isNullOnSomePath());
// assertTrue(nsp_e2.isException());
// }
    public void testMerge5() {
        edu.umd.cs.findbugs.ba.npe.IsNullValue checkedNonNull = edu.umd.cs.findbugs.ba.npe.IsNullValue.checkedNonNullValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue nsp_e = edu.umd.cs.findbugs.ba.npe.IsNullValue.nullOnSimplePathValue().toExceptionValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue nsp_e2 = edu.umd.cs.findbugs.ba.npe.IsNullValue.merge(checkedNonNull,nsp_e);
        assertTrue(nsp_e2.isNullOnSomePath());
        assertTrue(nsp_e2.isException());
    }
    public void testMerge6() {
        edu.umd.cs.findbugs.ba.npe.IsNullValue checkedNull_e = edu.umd.cs.findbugs.ba.npe.IsNullValue.checkedNullValue().toExceptionValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue unknown = edu.umd.cs.findbugs.ba.npe.IsNullValue.nonReportingNotNullValue();
        edu.umd.cs.findbugs.ba.npe.IsNullValue nsp_e = edu.umd.cs.findbugs.ba.npe.IsNullValue.merge(checkedNull_e,unknown);
        assertTrue(nsp_e.isNullOnSomePath());
        assertTrue(nsp_e.isException());
    }
}
