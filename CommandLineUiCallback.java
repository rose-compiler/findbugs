/*
 * Contributions to FindBugs
 * Copyright (C) 2009, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Implementation of the UI callback for command line sessions.
 *
 * @author andy.st
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import edu.umd.cs.findbugs.charsets.UserTextFile;
import edu.umd.cs.findbugs.cloud.Cloud;
public class CommandLineUiCallback extends java.lang.Object implements edu.umd.cs.findbugs.IGuiCallback {
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.IGuiCallback#showQuestionDialog(java.lang.String,
     * java.lang.String, java.lang.String)
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.IGuiCallback#showDocument(java.net.URL)
     */
    private static class CurrentThreadExecutorService extends java.util.concurrent.AbstractExecutorService {
        public CurrentThreadExecutorService() {
        }
        public void shutdown() {
        }
        public java.util.List<java.lang.Runnable> shutdownNow() {
            return null;
        }
        public boolean isShutdown() {
            return false;
        }
        public boolean isTerminated() {
            return false;
        }
        public boolean awaitTermination(long timeout, java.util.concurrent.TimeUnit unit) throws java.lang.InterruptedException {
            return false;
        }
        public void execute(java.lang.Runnable command) {
            command.run();
        }
    }
    final private edu.umd.cs.findbugs.CommandLineUiCallback.CurrentThreadExecutorService bugUpdateExecutor = new edu.umd.cs.findbugs.CommandLineUiCallback.CurrentThreadExecutorService();
    public CommandLineUiCallback() {
        super();
    }
    java.io.BufferedReader br = edu.umd.cs.findbugs.charsets.UserTextFile.bufferedReader(java.lang.System.in);
    public void showMessageDialogAndWait(java.lang.String message) throws java.lang.InterruptedException {
        java.lang.System.out.println(message);
    }
    public void showMessageDialog(java.lang.String message) {
        java.lang.System.out.println(message);
    }
    public int showConfirmDialog(java.lang.String message, java.lang.String title, java.lang.String ok, java.lang.String cancel) {
        java.lang.String confirmStr = "Yes (Y) or No (N)?";
        java.lang.System.out.println(java.lang.String.format("Confirmation required: %s%n	%s%n	%s",title,message,confirmStr));
        java.lang.String answer = null;
        while (true) {
            try {
                answer = br.readLine();
            }
            catch (java.io.IOException ioe){
                throw new java.lang.IllegalArgumentException("IO error trying to read System.in!");
            }
            int response = this.parseAnswer(answer);
            if (response < 0) {
                java.lang.System.out.println(java.lang.String.format("	%s",confirmStr));
            }
            else {
                return response;
            }
        }
    }
    private int parseAnswer(java.lang.String answer) {
        if (null == answer || answer.length() == 0) {
            java.lang.System.out.println("You entered an empty string");
            return  -1;
        }
        char option = answer.toLowerCase(java.util.Locale.ENGLISH).charAt(0);
        switch(option){
            case '\u006f':{
                return javax.swing.JOptionPane.OK_OPTION;
            }
            case '\u0079':{
                return javax.swing.JOptionPane.YES_OPTION;
            }
            case '\u006e':{
                return javax.swing.JOptionPane.NO_OPTION;
            }
            case '\u0063':{
                return javax.swing.JOptionPane.CANCEL_OPTION;
            }
            default:{
                java.lang.System.out.println("You entered '" + option + "'");
                return  -1;
            }
        }
    }
    public java.io.InputStream getProgressMonitorInputStream(java.io.InputStream in, int length, java.lang.String msg) {
        return in;
    }
    public void setErrorMessage(java.lang.String errorMsg) {
        java.lang.System.err.println(errorMsg);
    }
    public void displayNonmodelMessage(java.lang.String title, java.lang.String message) {
        java.lang.System.out.println(java.lang.String.format("Message: %s%n%s",title,message));
    }
    public java.lang.String showQuestionDialog(java.lang.String message, java.lang.String title, java.lang.String defaultValue) {
        throw new java.lang.UnsupportedOperationException();
    }
    public java.util.List<java.lang.String> showForm(java.lang.String message, java.lang.String title, java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> labels) {
        throw new java.lang.UnsupportedOperationException();
    }
    public boolean showDocument(java.net.URL u) {
        return false;
    }
    public void registerCloud(edu.umd.cs.findbugs.Project project, edu.umd.cs.findbugs.BugCollection collection, edu.umd.cs.findbugs.cloud.Cloud cloud) {
    }
    public java.util.concurrent.ExecutorService getBugUpdateExecutor() {
        return bugUpdateExecutor;
    }
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.IGuiCallback#isHeadless()
     */
    public boolean isHeadless() {
        return true;
    }
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.IGuiCallback#unregisterCloud(edu.umd.cs.findbugs.
     * Project, edu.umd.cs.findbugs.BugCollection,
     * edu.umd.cs.findbugs.cloud.Cloud)
     */
    public void unregisterCloud(edu.umd.cs.findbugs.Project project, edu.umd.cs.findbugs.BugCollection collection, edu.umd.cs.findbugs.cloud.Cloud cloud) {
    }
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.IGuiCallback#invokeInGUIThread(java.lang.Runnable)
     */
    public void invokeInGUIThread(java.lang.Runnable r) {
        throw new java.lang.UnsupportedOperationException();
    }
}
