/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005 Dave Brosius <dbrosius@users.sourceforge.net>
 * Copyright (C) 2005-2006 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author Dave Brousius 4/2005 original author
 * @author Brian Cole 7/2006 serious reworking
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.BranchInstruction;
import org.apache.bcel.generic.GotoInstruction;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.LOOKUPSWITCH;
import org.apache.bcel.generic.ReturnInstruction;
import org.apache.bcel.generic.TABLESWITCH;
import org.apache.bcel.util.ByteSequence;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.SystemProperties;
import edu.umd.cs.findbugs.ba.BasicBlock;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.Edge;
import edu.umd.cs.findbugs.ba.EdgeTypes;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
public class DuplicateBranches extends edu.umd.cs.findbugs.visitclass.PreorderVisitor implements edu.umd.cs.findbugs.Detector {
    private edu.umd.cs.findbugs.ba.ClassContext classContext;
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    private java.util.Collection<edu.umd.cs.findbugs.BugInstance> pendingBugs = new java.util.LinkedList<edu.umd.cs.findbugs.BugInstance>();
    public DuplicateBranches(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        this.classContext = classContext;
        classContext.getJavaClass().accept(this);
    }
    public void visitMethod(org.apache.bcel.classfile.Method method) {
        try {
            if (method.getCode() == null) return;
            edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
            java.util.Iterator<edu.umd.cs.findbugs.ba.BasicBlock> bbi = cfg.blockIterator();
            while (bbi.hasNext()) {
                edu.umd.cs.findbugs.ba.BasicBlock bb = bbi.next();
                int numOutgoing = cfg.getNumOutgoingEdges(bb);
                if (numOutgoing == 2) this.findIfElseDuplicates(cfg,method,bb);
                else if (numOutgoing > 2) this.findSwitchDuplicates(cfg,method,bb);
            }
        }
        catch (edu.umd.cs.findbugs.ba.MethodUnprofitableException mue){
// otherwise
            if (edu.umd.cs.findbugs.SystemProperties.getBoolean("unprofitable.debug")) bugReporter.logError("skipping unprofitable method in " + this.getClass().getName());
        }
        catch (java.lang.Exception e){
            bugReporter.logError("Failure examining basic blocks in Duplicate Branches detector",e);
        }
        if (pendingBugs.size() <= 2) for (edu.umd.cs.findbugs.BugInstance b : pendingBugs)bugReporter.reportBug(b);
;
        pendingBugs.clear();
    }
    private void findIfElseDuplicates(edu.umd.cs.findbugs.ba.CFG cfg, org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.BasicBlock bb) {
        edu.umd.cs.findbugs.ba.BasicBlock thenBB = null;
        edu.umd.cs.findbugs.ba.BasicBlock elseBB = null;
        java.util.Iterator<edu.umd.cs.findbugs.ba.Edge> iei = cfg.outgoingEdgeIterator(bb);
        while (iei.hasNext()) {
            edu.umd.cs.findbugs.ba.Edge e = iei.next();
            if (e.getType() == edu.umd.cs.findbugs.ba.EdgeTypes.IFCMP_EDGE) {
                elseBB = e.getTarget();
            }
            else if (e.getType() == edu.umd.cs.findbugs.ba.EdgeTypes.FALL_THROUGH_EDGE) {
                thenBB = e.getTarget();
            }
        }
        if ((thenBB == null) || (elseBB == null)) return;
        org.apache.bcel.generic.InstructionHandle thenStartHandle = getDeepFirstInstruction(cfg,thenBB);
        org.apache.bcel.generic.InstructionHandle elseStartHandle = getDeepFirstInstruction(cfg,elseBB);
        if ((thenStartHandle == null) || (elseStartHandle == null)) return;
        int thenStartPos = thenStartHandle.getPosition();
        int elseStartPos = elseStartHandle.getPosition();
        org.apache.bcel.generic.InstructionHandle thenFinishIns = this.findThenFinish(cfg,thenBB,elseStartPos);
        int thenFinishPos = thenFinishIns.getPosition();
        if ( !(thenFinishIns.getInstruction() instanceof org.apache.bcel.generic.GotoInstruction)) return;
        org.apache.bcel.generic.InstructionHandle elseFinishHandle = ((org.apache.bcel.generic.GotoInstruction) (thenFinishIns.getInstruction()) ).getTarget();
        int elseFinishPos = elseFinishHandle.getPosition();
        if (thenFinishPos >= elseStartPos) return;
        if ((thenFinishPos - thenStartPos) != (elseFinishPos - elseStartPos)) return;
        if (thenFinishPos <= thenStartPos) return;
        byte[] thenBytes = this.getCodeBytes(method,thenStartPos,thenFinishPos);
        byte[] elseBytes = this.getCodeBytes(method,elseStartPos,elseFinishPos);
        if ( !java.util.Arrays.equals(thenBytes,elseBytes)) return;
// adjust elseFinishPos to be inclusive (for source line attribution)
        org.apache.bcel.generic.InstructionHandle elseLastIns = elseFinishHandle.getPrev();
        if (elseLastIns != null) elseFinishPos = elseLastIns.getPosition();
        pendingBugs.add(new edu.umd.cs.findbugs.BugInstance(this, "DB_DUPLICATE_BRANCHES", NORMAL_PRIORITY).addClassAndMethod(classContext.getJavaClass(),method).addSourceLineRange(classContext,this,thenStartPos,thenFinishPos).addSourceLineRange(classContext,this,elseStartPos,elseFinishPos));
    }
/**
     * Like bb.getFirstInstruction() except that if null is returned it will
     * follow the FALL_THROUGH_EDGE (if any)
     */
    private static org.apache.bcel.generic.InstructionHandle getDeepFirstInstruction(edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.BasicBlock bb) {
        org.apache.bcel.generic.InstructionHandle ih = bb.getFirstInstruction();
        if (ih != null) return ih;
        java.util.Iterator<edu.umd.cs.findbugs.ba.Edge> iei = cfg.outgoingEdgeIterator(bb);
        while (iei.hasNext()) {
            edu.umd.cs.findbugs.ba.Edge e = iei.next();
            java.lang.String edgeString = e.toString();
            if (edu.umd.cs.findbugs.ba.EdgeTypes.FALL_THROUGH_EDGE == e.getType()) return getDeepFirstInstruction(cfg,e.getTarget());
        }
        return null;
    }
    private void findSwitchDuplicates(edu.umd.cs.findbugs.ba.CFG cfg, org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.BasicBlock bb) {
        int[] switchPos = new int[cfg.getNumOutgoingEdges(bb) + 1];
        java.util.HashMap<java.lang.Integer, org.apache.bcel.generic.InstructionHandle> prevHandle = new java.util.HashMap<java.lang.Integer, org.apache.bcel.generic.InstructionHandle>();
        java.util.Iterator<edu.umd.cs.findbugs.ba.Edge> iei = cfg.outgoingEdgeIterator(bb);
        int idx = 0;
        while (iei.hasNext()) {
            edu.umd.cs.findbugs.ba.Edge e = iei.next();
            int eType = e.getType();
            if (eType == edu.umd.cs.findbugs.ba.EdgeTypes.SWITCH_EDGE || eType == edu.umd.cs.findbugs.ba.EdgeTypes.SWITCH_DEFAULT_EDGE) {
                edu.umd.cs.findbugs.ba.BasicBlock target = e.getTarget();
                org.apache.bcel.generic.InstructionHandle firstIns = getDeepFirstInstruction(cfg,target);
                if (firstIns == null) 
// give up on this edge
                continue;
                int firstInsPosition = firstIns.getPosition();
                switchPos[idx++] = firstInsPosition;
// prev in
                org.apache.bcel.generic.InstructionHandle prevIns = firstIns.getPrev();
// bytecode, not
// flow
                if (prevIns != null) prevHandle.put(firstInsPosition,prevIns);
            }
            else {
// hmm, this must not be a switch statement, so give up
                return;
            }
        }
// need at least two edges to tango
        if (idx < 2) return;
        java.util.Arrays.sort(switchPos,0,idx);
        switchPos[idx] = getFinalTarget(cfg,switchPos[idx - 1],prevHandle.values());
// sort the 'idx' switch positions
// compute end position of final case (ok if set to 0 or <=
// switchPos[idx-1])
        java.util.HashMap<java.math.BigInteger, java.util.Collection<java.lang.Integer>> map = new java.util.HashMap<java.math.BigInteger, java.util.Collection<java.lang.Integer>>();
        for (int i = 0; i < idx; i++) {
            if (switchPos[i] + 7 >= switchPos[i + 1]) continue;
            int endPos = switchPos[i + 1];
            org.apache.bcel.generic.InstructionHandle last = prevHandle.get(switchPos[i + 1]);
            if (last == null) {
            }
            else if (last.getInstruction() instanceof org.apache.bcel.generic.GotoInstruction) {
                endPos = last.getPosition();
            }
            else if (last.getInstruction() instanceof org.apache.bcel.generic.ReturnInstruction) {
            }
            else {
                if (i + 2 < idx) continue;
                if (i + 1 < idx && switchPos[idx] != switchPos[idx - 1]) continue;
            }
            java.math.BigInteger clauseAsInt = this.getCodeBytesAsBigInt(method,switchPos,i,endPos);
            this.updateMap(map,i,clauseAsInt);
        }
// ignore small switch clauses
// should be default case -- leave endPos as is
// don't store the goto
// leave endPos as is (store the return instruction)
// } else if (last.getInstruction() instanceof ATHROW) {
// // leave endPos as is (store the throw instruction)
// Don't do this since many cases may throw "not implemented".
// falls through to next case, so don't store it
// at all
// also falls through unless switch has no default
// case
        for (java.util.Collection<java.lang.Integer> clauses : map.values()){
            if (clauses.size() > 1) {
                edu.umd.cs.findbugs.BugInstance bug = new edu.umd.cs.findbugs.BugInstance(this, "DB_DUPLICATE_SWITCH_CLAUSES", LOW_PRIORITY).addClassAndMethod(classContext.getJavaClass(),method);
                for (int i : clauses)bug.addSourceLineRange(this.classContext,this,switchPos[i],switchPos[i + 1] - 1);
;
                pendingBugs.add(bug);
            }
        }
;
    }
    private void updateMap(java.util.HashMap<java.math.BigInteger, java.util.Collection<java.lang.Integer>> map, int i, java.math.BigInteger clauseAsInt) {
        java.util.Collection<java.lang.Integer> values = map.get(clauseAsInt);
        if (values == null) {
            values = new java.util.LinkedList<java.lang.Integer>();
            map.put(clauseAsInt,values);
        }
        values.add(i);
    }
// index into the sorted array
    private java.math.BigInteger getCodeBytesAsBigInt(org.apache.bcel.classfile.Method method, int[] switchPos, int i, int endPos) {
        byte[] clause = this.getCodeBytes(method,switchPos[i],endPos);
        java.math.BigInteger clauseAsInt;
        if (clause.length == 0) clauseAsInt = java.math.BigInteger.ZERO;
        else clauseAsInt = new java.math.BigInteger(clause);
        return clauseAsInt;
    }
/**
     * determine the end position (exclusive) of the final case by looking at
     * the gotos at the ends of the other cases
     */
    private static int getFinalTarget(edu.umd.cs.findbugs.ba.CFG cfg, int myPos, java.util.Collection<org.apache.bcel.generic.InstructionHandle> prevs) {
        int maxGoto = 0;
        edu.umd.cs.findbugs.ba.BasicBlock myBB = null;
// note: InstructionHandle doesn't override equals(), so use
// prevs.contains() with caution.
        java.util.Iterator<edu.umd.cs.findbugs.ba.BasicBlock> bbi = cfg.blockIterator();
        while (bbi.hasNext()) {
            edu.umd.cs.findbugs.ba.BasicBlock bb = bbi.next();
// may be null
            org.apache.bcel.generic.InstructionHandle last = bb.getLastInstruction();
// danger will robinson
            if (prevs.contains(last)) {
                java.util.Iterator<edu.umd.cs.findbugs.ba.Edge> iei = cfg.outgoingEdgeIterator(bb);
                while (iei.hasNext()) {
                    edu.umd.cs.findbugs.ba.Edge e = iei.next();
                    int eType = e.getType();
                    java.lang.String aab = e.toString();
                    if (eType == edu.umd.cs.findbugs.ba.EdgeTypes.GOTO_EDGE) {
                        edu.umd.cs.findbugs.ba.BasicBlock target = e.getTarget();
                        org.apache.bcel.generic.InstructionHandle targetFirst = getDeepFirstInstruction(cfg,target);
                        if (targetFirst != null) {
                            int targetPos = targetFirst.getPosition();
                            if (targetPos > maxGoto) maxGoto = targetPos;
                        }
                    }
                }
            }
            else if (last != null && myPos == bb.getFirstInstruction().getPosition()) {
                myBB = bb;
            }
        }
/*
         * ok, there are three cases: a) maxGoto == myPos: There is no default
         * case within the switch. b) maxGoto > myPos: maxGoto is the end
         * (exclusive) of the default case c) maxGoto < myPos: all the cases do
         * something like return or throw, so who knows if there is a default
         * case (and it's length)?
         */
        if (maxGoto < myPos && myBB != null) {
/*
             * We're in case (c), so let's guess that the end of the basic block
             * is the end of the default case (if it exists). This may give
             * false negatives (if the default case has branches, for example)
             * but shouldn't give false negatives (because if it matches one of
             * the cases, then it has also matched that case's return/throw).
             */
            org.apache.bcel.generic.InstructionHandle last = myBB.getLastInstruction();
            if (last != null) {
// note: last.getNext() may return null, so do it this way
                return last.getPosition() + last.getInstruction().getLength();
            }
        }
        return maxGoto;
    }
    private byte[] getCodeBytes(org.apache.bcel.classfile.Method m, int start, int end) {
        byte[] code = m.getCode().getCode();
        byte[] bytes = new byte[end - start];
        java.lang.System.arraycopy(code,start,bytes,0,end - start);
        try {
            org.apache.bcel.util.ByteSequence sequence = new org.apache.bcel.util.ByteSequence(code);
            while ((sequence.available() > 0) && (sequence.getIndex() < start)) {
                org.apache.bcel.generic.Instruction.readInstruction(sequence);
            }
            int pos;
            while (sequence.available() > 0 && ((pos = sequence.getIndex()) < end)) {
                org.apache.bcel.generic.Instruction ins = org.apache.bcel.generic.Instruction.readInstruction(sequence);
                if ((ins instanceof org.apache.bcel.generic.BranchInstruction) &&  !(ins instanceof org.apache.bcel.generic.TABLESWITCH) &&  !(ins instanceof org.apache.bcel.generic.LOOKUPSWITCH)) {
                    org.apache.bcel.generic.BranchInstruction bi = (org.apache.bcel.generic.BranchInstruction) (ins) ;
                    int offset = bi.getIndex();
                    int target = offset + pos;
// or target < start ??
                    if (target >= end) {
                        byte hiByte = (byte) (((target >> 8) & 0x000000FF)) ;
                        byte loByte = (byte) ((target & 0x000000FF)) ;
                        bytes[pos + bi.getLength() - 2 - start] = hiByte;
                        bytes[pos + bi.getLength() - 1 - start] = loByte;
                    }
                }
            }
        }
        catch (java.io.IOException ioe){
        }
        return bytes;
    }
    private org.apache.bcel.generic.InstructionHandle findThenFinish(edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.BasicBlock thenBB, int elsePos) {
        org.apache.bcel.generic.InstructionHandle inst = thenBB.getFirstInstruction();
        while (inst == null) {
            java.util.Iterator<edu.umd.cs.findbugs.ba.Edge> ie = cfg.outgoingEdgeIterator(thenBB);
            while (ie.hasNext()) {
                edu.umd.cs.findbugs.ba.Edge e = ie.next();
                if (e.getType() == edu.umd.cs.findbugs.ba.EdgeTypes.FALL_THROUGH_EDGE) {
                    thenBB = e.getTarget();
                    break;
                }
            }
            inst = thenBB.getFirstInstruction();
        }
        org.apache.bcel.generic.InstructionHandle lastIns = inst;
        while (inst.getPosition() < elsePos) {
            lastIns = inst;
            inst = inst.getNext();
        }
        return lastIns;
    }
    public void report() {
    }
}
