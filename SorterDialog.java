/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * This is the window that pops up when the user double clicks on the sorting
 * table Its also available from the menu if they remove all Sortables.
 * 
 * The user can choose what Sortables he wants to sort by, sort them into the
 * order he wants to see and then click apply to move his choices onto the
 * sorting table
 * 
 * @author Dan
 * 
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
public class SorterDialog extends edu.umd.cs.findbugs.gui2.FBDialog {
    class SortableCheckBox extends javax.swing.JCheckBox {
        final edu.umd.cs.findbugs.gui2.Sortables sortable;
        public SortableCheckBox(edu.umd.cs.findbugs.gui2.Sortables s) {
            super(s == edu.umd.cs.findbugs.gui2.Sortables.DIVIDER ? edu.umd.cs.findbugs.L10N.getLocalString("sort.divider","[divider]") : s.toString());
            this.sortable = s;
            this.addChangeListener(new javax.swing.event.ChangeListener() {
                public void stateChanged(javax.swing.event.ChangeEvent e) {
                    ((edu.umd.cs.findbugs.gui2.SorterTableColumnModel) (preview.getColumnModel()) ).setVisible(sortable,edu.umd.cs.findbugs.gui2.SorterDialog.SortableCheckBox.this.isSelected());
                }
            });
        }
    }
    private javax.swing.table.JTableHeader preview;
    private java.util.ArrayList<edu.umd.cs.findbugs.gui2.SorterDialog.SortableCheckBox> checkBoxSortList = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.SorterDialog.SortableCheckBox>();
    javax.swing.JButton sortApply;
    public static edu.umd.cs.findbugs.gui2.SorterDialog getInstance() {
        return new edu.umd.cs.findbugs.gui2.SorterDialog();
    }
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible) {
            ((edu.umd.cs.findbugs.gui2.SorterTableColumnModel) ((preview.getColumnModel())) ).createFrom(edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSorter());
            this.setSorterCheckBoxes();
        }
    }
    public SorterDialog() {
        super();
        this.setTitle("Group Bugs By...");
        this.add(this.createSorterPane());
        this.pack();
        this.setLocationByPlatform(true);
        this.setResizable(false);
        preview.setColumnModel(new edu.umd.cs.findbugs.gui2.SorterTableColumnModel(edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSorter().getOrder()));
    }
/**
     * Creates JPanel with checkboxes of different things to sort by. List is:
     * priority, class, package, category, bugcode, status, and type.
     * 
     * @return
     */
    private javax.swing.JPanel createSorterPane() {
        javax.swing.JPanel insidePanel = new javax.swing.JPanel();
        insidePanel.setLayout(new java.awt.GridBagLayout());
        preview = new javax.swing.table.JTableHeader();
        edu.umd.cs.findbugs.gui2.Sortables[] sortables = edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getAvailableSortables();
        preview.setColumnModel(new edu.umd.cs.findbugs.gui2.SorterTableColumnModel(sortables));
        for (edu.umd.cs.findbugs.gui2.Sortables s : sortables)if (s != edu.umd.cs.findbugs.gui2.Sortables.DIVIDER) {
            checkBoxSortList.add(new edu.umd.cs.findbugs.gui2.SorterDialog.SortableCheckBox(s));
        }
;
        this.setSorterCheckBoxes();
        java.awt.GridBagConstraints gbc = new java.awt.GridBagConstraints();
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.gridx = 1;
        gbc.insets = new java.awt.Insets(2, 5, 2, 5);
        insidePanel.add(new javax.swing.JLabel("<html><h2>1. Choose bug properties"),gbc);
        insidePanel.add(new edu.umd.cs.findbugs.gui2.CheckBoxList(checkBoxSortList.toArray(new javax.swing.JCheckBox[checkBoxSortList.size()])),gbc);
        javax.swing.JTable t = new javax.swing.JTable(new javax.swing.table.DefaultTableModel(0, sortables.length));
        t.setTableHeader(preview);
        preview.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.W_RESIZE_CURSOR));
        insidePanel.add(new javax.swing.JLabel("<html><h2>2. Drag and drop to change grouping priority"),gbc);
        insidePanel.add(this.createAppropriatelySizedScrollPane(t),gbc);
        sortApply = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.apply_btn","Apply"));
        sortApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSorter().createFrom((edu.umd.cs.findbugs.gui2.SorterTableColumnModel) (preview.getColumnModel()) );
                ((edu.umd.cs.findbugs.gui2.BugTreeModel) (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getTree().getModel()) ).checkSorter();
                edu.umd.cs.findbugs.gui2.SorterDialog.this.dispose();
            }
        });
        gbc.fill = java.awt.GridBagConstraints.NONE;
        insidePanel.add(sortApply,gbc);
        javax.swing.JPanel sorter = new javax.swing.JPanel();
        sorter.setLayout(new java.awt.BorderLayout());
        sorter.add(new javax.swing.JScrollPane(insidePanel),java.awt.BorderLayout.CENTER);
        return sorter;
    }
    private javax.swing.JScrollPane createAppropriatelySizedScrollPane(javax.swing.JTable t) {
        javax.swing.JScrollPane sp = new javax.swing.JScrollPane(t);
// This sets the height of the scrollpane so it is dependent on the
// fontsize.
        int num = (int) ((edu.umd.cs.findbugs.gui2.Driver.getFontSize() * 1.2)) ;
        sp.setPreferredSize(new java.awt.Dimension(670, 10 + num));
        return sp;
    }
/**
     * Sets the checkboxes in the sorter panel to what is shown in the
     * MainFrame. This assumes that sorterTableColumnModel will return the list
     * of which box is checked in the same order as the order that sorter panel
     * has.
     */
    private void setSorterCheckBoxes() {
        edu.umd.cs.findbugs.gui2.SorterTableColumnModel sorter = edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSorter();
        for (edu.umd.cs.findbugs.gui2.SorterDialog.SortableCheckBox c : checkBoxSortList)c.setSelected(sorter.isShown(c.sortable));
;
    }
    void freeze() {
        sortApply.setEnabled(false);
    }
    void thaw() {
        sortApply.setEnabled(true);
    }
}
