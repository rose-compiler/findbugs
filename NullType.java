/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Special type representing the null value. This is a type which is higher in
 * the lattice than any object type, but lower than the overall Top type. It
 * represents the type of the null value, which may logically be merged with any
 * object type without loss of information.
 * 
 * @author David Hovemeyer
 * @see TypeAnalysis
 * @see TypeFrame
 * @see TypeMerger
 */
package edu.umd.cs.findbugs.ba.type;
import edu.umd.cs.findbugs.ba.type.*;
import org.apache.bcel.generic.ReferenceType;
public class NullType extends org.apache.bcel.generic.ReferenceType implements edu.umd.cs.findbugs.ba.type.ExtendedTypes {
    final private static long serialVersionUID = 1L;
    final private static edu.umd.cs.findbugs.ba.type.NullType theInstance = new edu.umd.cs.findbugs.ba.type.NullType();
    public NullType() {
        super(T_NULL,"<null type>");
    }
    public int hashCode() {
        return java.lang.System.identityHashCode(this);
    }
    public boolean equals(java.lang.Object o) {
        return o == this;
    }
    public static edu.umd.cs.findbugs.ba.type.NullType instance() {
        return theInstance;
    }
}
// vim:ts=4
