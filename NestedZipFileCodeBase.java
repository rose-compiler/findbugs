/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A scannable code base class for a zip (or Jar) file nested inside some other
 * codebase. These are handled by extracting the nested zip/jar file to a
 * temporary file, and delegating to an internal ZipFileCodeBase that reads from
 * the temporary file.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.impl;
import edu.umd.cs.findbugs.classfile.impl.*;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.classfile.ICodeBase;
import edu.umd.cs.findbugs.classfile.ICodeBaseEntry;
import edu.umd.cs.findbugs.classfile.ICodeBaseIterator;
import edu.umd.cs.findbugs.classfile.ResourceNotFoundException;
import edu.umd.cs.findbugs.io.IO;
public class NestedZipFileCodeBase extends edu.umd.cs.findbugs.classfile.impl.AbstractScannableCodeBase {
    private edu.umd.cs.findbugs.classfile.ICodeBase parentCodeBase;
    private java.lang.String resourceName;
    private java.io.File tempFile;
    private edu.umd.cs.findbugs.classfile.impl.AbstractScannableCodeBase delegateCodeBase;
/**
     * Constructor.
     * 
     * @param codeBaseLocator
     *            the codebase locator for this codebase
     */
    public NestedZipFileCodeBase(edu.umd.cs.findbugs.classfile.impl.NestedZipFileCodeBaseLocator codeBaseLocator) throws java.io.IOException, edu.umd.cs.findbugs.classfile.ResourceNotFoundException {
        super(codeBaseLocator);
        this.parentCodeBase = codeBaseLocator.getParentCodeBase();
        this.resourceName = codeBaseLocator.getResourceName();
        java.io.InputStream inputStream = null;
        java.io.OutputStream outputStream = null;
        try {
            this.tempFile = java.io.File.createTempFile("findbugs",".zip");
            tempFile.deleteOnExit();
// Create a temp file
// just in case we crash before the
// codebase is closed
// Copy nested zipfile to the temporary file
// FIXME: potentially long blocking operation - should be
// interruptible
            edu.umd.cs.findbugs.classfile.ICodeBaseEntry resource = parentCodeBase.lookupResource(resourceName);
            if (resource == null) {
                throw new edu.umd.cs.findbugs.classfile.ResourceNotFoundException(resourceName);
            }
            inputStream = resource.openResource();
            outputStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(tempFile));
            edu.umd.cs.findbugs.io.IO.copy(inputStream,outputStream);
            outputStream.flush();
            delegateCodeBase = edu.umd.cs.findbugs.classfile.impl.ZipCodeBaseFactory.makeZipCodeBase(codeBaseLocator,tempFile);
        }
        finally {
            if (inputStream != null) {
                edu.umd.cs.findbugs.io.IO.close(inputStream);
            }
            if (outputStream != null) {
                edu.umd.cs.findbugs.io.IO.close(outputStream);
            }
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.IScannableCodeBase#iterator()
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseIterator iterator() throws java.lang.InterruptedException {
        return new edu.umd.cs.findbugs.classfile.impl.DelegatingCodeBaseIterator(this, delegateCodeBase);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.ICodeBase#lookupResource(java.lang.String)
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseEntry lookupResource(java.lang.String resourceName) {
        edu.umd.cs.findbugs.classfile.ICodeBaseEntry delegateCodeBaseEntry = delegateCodeBase.lookupResource(resourceName);
        if (delegateCodeBaseEntry == null) {
            return null;
        }
        return new edu.umd.cs.findbugs.classfile.impl.DelegatingCodeBaseEntry(this, delegateCodeBaseEntry);
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getPathName()
     */
    public java.lang.String getPathName() {
        return null;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#close()
     */
    public void close() {
        delegateCodeBase.close();
        if ( !tempFile.delete()) {
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Could not delete " + tempFile);
        }
    }
}
