/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce the ClassInfo for a loaded class. We parse just
 * enough information from the classfile to get the needed information.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine;
import edu.umd.cs.findbugs.classfile.engine.*;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.ClassNameMismatchException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.IClassAnalysisEngine;
import edu.umd.cs.findbugs.classfile.analysis.ClassData;
import edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo;
public class ClassNameAndSuperclassInfoAnalysisEngine extends java.lang.Object implements edu.umd.cs.findbugs.classfile.IClassAnalysisEngine<edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo> {
    public ClassNameAndSuperclassInfoAnalysisEngine() {
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.ClassDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
// Get InputStream reading from class data
        edu.umd.cs.findbugs.classfile.analysis.ClassData classData = analysisCache.getClassAnalysis(edu.umd.cs.findbugs.classfile.analysis.ClassData.class,descriptor);
        java.io.DataInputStream classDataIn = new java.io.DataInputStream(new java.io.ByteArrayInputStream(classData.getData()));
// Read the class info
        edu.umd.cs.findbugs.classfile.engine.ClassParserInterface parser = new edu.umd.cs.findbugs.classfile.engine.ClassParser(classDataIn, descriptor, classData.getCodeBaseEntry());
        edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo.Builder classInfoBuilder = new edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo.Builder();
        parser.parse(classInfoBuilder);
        edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo classInfo = classInfoBuilder.build();
        if ( !classInfo.getClassDescriptor().equals(descriptor)) {
            throw new edu.umd.cs.findbugs.classfile.ClassNameMismatchException(descriptor, classInfo.getClassDescriptor(), classData.getCodeBaseEntry());
        }
        return classInfo;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#registerWith(edu.umd.cs
     * .findbugs.classfile.IAnalysisCache)
     */
    public void registerWith(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache) {
        analysisCache.registerClassAnalysisEngine(edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo.class,this);
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.IAnalysisEngine#canRecompute()
     */
    public boolean canRecompute() {
// ClassInfo objects serve as XClass objects,
// which we want interned. So, they are never purged from the cache.
        return false;
    }
}
