/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.io.IO;
public class PropertyBundle extends java.lang.Object {
    class Rewriter extends java.lang.Object {
        final private java.lang.String urlRewritePatternString = edu.umd.cs.findbugs.PropertyBundle.this.getOSDependentProperty("findbugs.urlRewritePattern");
        final private java.util.regex.Pattern urlRewritePattern;
        final private java.lang.String urlRewriteFormat = edu.umd.cs.findbugs.PropertyBundle.this.getOSDependentProperty("findbugs.urlRewriteFormat");
        public Rewriter() {
            super();
            java.util.regex.Pattern p = null;
            if (urlRewritePatternString != null && urlRewriteFormat != null) try {
                p = java.util.regex.Pattern.compile(urlRewritePatternString);
            }
            catch (java.lang.Exception e){
                assert true;
            }
            urlRewritePattern = p;
        }
    }
    final private java.util.Properties properties;
    volatile edu.umd.cs.findbugs.PropertyBundle.Rewriter rewriter;
    edu.umd.cs.findbugs.PropertyBundle.Rewriter getRewriter() {
        if (rewriter == null) {
            synchronized (this){
                if (rewriter == null) rewriter = new edu.umd.cs.findbugs.PropertyBundle.Rewriter();
            }
        }
        return rewriter;
    }
    public PropertyBundle() {
        super();
        properties = new java.util.Properties();
    }
    public PropertyBundle(java.util.Properties properties) {
        super();
        this.properties = (java.util.Properties) (properties.clone()) ;
    }
    public edu.umd.cs.findbugs.PropertyBundle copy() {
        return new edu.umd.cs.findbugs.PropertyBundle(properties);
    }
    public java.util.Properties getProperties() {
        return properties;
    }
    public void loadPropertiesFromString(java.lang.String contents) {
        if (contents == null) {
            return;
        }
        java.io.InputStream in = null;
        try {
            in = new java.io.ByteArrayInputStream(contents.getBytes("ISO-8859-1"));
            properties.load(in);
        }
        catch (java.io.IOException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Unable to load properties from " + contents,e);
        }
        finally {
            edu.umd.cs.findbugs.io.IO.close(in);
        }
    }
    public void loadPropertiesFromURL(java.net.URL url) {
        if (url == null) {
            return;
        }
        java.io.InputStream in = null;
        try {
            in = url.openStream();
            properties.load(in);
        }
        catch (java.io.IOException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Unable to load properties from " + url,e);
        }
        finally {
            edu.umd.cs.findbugs.io.IO.close(in);
        }
    }
    public void loadProperties(java.util.Properties properties) {
        this.properties.putAll(properties);
    }
/**
     * Get boolean property, returning false if a security manager prevents us
     * from accessing system properties
     * 
     * @return true if the property exists and is set to true
     */
    public boolean getBoolean(java.lang.String name) {
        return this.getBoolean(name,false);
    }
    public boolean getBoolean(java.lang.String name, boolean defaultValue) {
        boolean result = defaultValue;
        try {
            java.lang.String value = this.getProperty(name);
            if (value == null) return defaultValue;
            result = this.toBoolean(value);
        }
        catch (java.lang.IllegalArgumentException e){
        }
        catch (java.lang.NullPointerException e){
        }
        return result;
    }
    private boolean toBoolean(java.lang.String name) {
        return ((name != null) && name.equalsIgnoreCase("true"));
    }
/**
     * @param name
     *            property name
     * @param defaultValue
     *            default value
     * @return the int value (or defaultValue if the property does not exist)
     */
    public int getInt(java.lang.String name, int defaultValue) {
        try {
            java.lang.String value = this.getProperty(name);
            if (value != null) return java.lang.Integer.decode(value);
        }
        catch (java.lang.Exception e){
            assert true;
        }
        return defaultValue;
    }
/**
     * @param name
     *            property name
     * @return string value (or null if the property does not exist)
     */
    public java.lang.String getOSDependentProperty(java.lang.String name) {
        java.lang.String osDependentName = name + edu.umd.cs.findbugs.SystemProperties.OS_NAME;
        java.lang.String value = this.getProperty(osDependentName);
        if (value != null) return value;
        return this.getProperty(name);
    }
/**
     * @param name
     *            property name
     * @return string value (or null if the property does not exist)
     */
    public java.lang.String getProperty(java.lang.String name) {
        try {
            java.lang.String value = edu.umd.cs.findbugs.SystemProperties.getProperty(name);
            if (value != null) return value;
            return properties.getProperty(name);
        }
        catch (java.lang.Exception e){
            return null;
        }
    }
/**
     * @param name
     *            property name
     * @return string value (or null if the property does not exist)
     */
    public void setProperty(java.lang.String name, java.lang.String value) {
        properties.setProperty(name,value);
    }
    public java.lang.String toString() {
        return properties.toString();
    }
/**
     * @param name
     *            property name
     * @param defaultValue
     *            default value
     * @return string value (or defaultValue if the property does not exist)
     */
    public java.lang.String getProperty(java.lang.String name, java.lang.String defaultValue) {
        java.lang.String value = this.getProperty(name);
        if (value != null) return value;
        return defaultValue;
    }
    public java.lang.String rewriteURLAccordingToProperties(java.lang.String u) {
        if (this.getRewriter().urlRewritePattern == null || this.getRewriter().urlRewriteFormat == null) return u;
        java.util.regex.Matcher m = this.getRewriter().urlRewritePattern.matcher(u);
        if ( !m.matches()) return u;
        java.lang.String result = java.lang.String.format(this.getRewriter().urlRewriteFormat,m.group(1));
        return result;
    }
}
