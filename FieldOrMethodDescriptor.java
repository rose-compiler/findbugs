/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Common superclass for FieldDescriptor and MethodDescriptor.
 *
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile;
import edu.umd.cs.findbugs.classfile.*;
import edu.umd.cs.findbugs.internalAnnotations.SlashedClassName;
abstract public class FieldOrMethodDescriptor extends java.lang.Object implements edu.umd.cs.findbugs.classfile.FieldOrMethodName {
    final private java.lang.String slashedClassName;
    final private java.lang.String name;
    final private java.lang.String signature;
    final private boolean isStatic;
    private int cachedHashCode;
    final private int nameSigHashCode;
    public FieldOrMethodDescriptor(java.lang.String slashedClassName, java.lang.String name, java.lang.String signature, boolean isStatic) {
        super();
        assert slashedClassName.indexOf('\u002e') ==  -1 : "class name not in VM format: " + slashedClassName;
        this.slashedClassName = edu.umd.cs.findbugs.classfile.DescriptorFactory.canonicalizeString(slashedClassName);
        this.name = edu.umd.cs.findbugs.classfile.DescriptorFactory.canonicalizeString(name);
        this.signature = edu.umd.cs.findbugs.classfile.DescriptorFactory.canonicalizeString(signature);
        this.isStatic = isStatic;
        this.nameSigHashCode = getNameSigHashCode(this.name,this.signature);
    }
    public static int getNameSigHashCode(java.lang.String name, java.lang.String signature) {
        return name.hashCode() * 3119 + signature.hashCode() * 131;
    }
    public int getNameSigHashCode() {
        return nameSigHashCode;
    }
/**
     *
     *
     * @return Returns the class name
     */
    public java.lang.String getSlashedClassName() {
        return slashedClassName;
    }
/**
     * @return a ClassDescriptor for the method's class
     */
    public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() {
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(slashedClassName);
    }
/**
     * @return Returns the method name
     */
    public java.lang.String getName() {
        return name;
    }
/**
     * @return Returns the method signature
     */
    public java.lang.String getSignature() {
        return signature;
    }
/**
     * @return Returns true if method is static, false if not
     */
    public boolean isStatic() {
        return isStatic;
    }
    protected int compareTo(edu.umd.cs.findbugs.classfile.FieldOrMethodName o) {
        int cmp;
        cmp = this.getClassDescriptor().compareTo(o.getClassDescriptor());
        if (cmp != 0) {
            return cmp;
        }
        cmp = this.name.compareTo(o.getName());
        if (cmp != 0) {
            return cmp;
        }
        cmp = this.signature.compareTo(o.getSignature());
        if (cmp != 0) {
            return cmp;
        }
        return (this.isStatic ? 1 : 0) - (o.isStatic() ? 1 : 0);
    }
    protected boolean haveEqualFields(edu.umd.cs.findbugs.classfile.FieldOrMethodDescriptor other) {
        return this.isStatic == other.isStatic && this.slashedClassName.equals(other.slashedClassName) && this.name.equals(other.name) && this.signature.equals(other.signature);
    }
/*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    final public int hashCode() {
        if (cachedHashCode == 0) {
            cachedHashCode = slashedClassName.hashCode() * 7919 + nameSigHashCode + (isStatic ? 1 : 0);
        }
        return cachedHashCode;
    }
/*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    public java.lang.String toString() {
        return (isStatic ? "static " : "") + this.getClassDescriptor().getDottedClassName() + "." + name + signature;
    }
    public static int compareTo(edu.umd.cs.findbugs.classfile.FieldOrMethodDescriptor thas, edu.umd.cs.findbugs.classfile.FieldOrMethodDescriptor that) {
        int result = thas.slashedClassName.compareTo(that.slashedClassName);
        if (result != 0) return result;
        result = thas.name.compareTo(that.name);
        if (result != 0) return result;
        result = thas.signature.compareTo(that.signature);
        if (result != 0) return result;
        result = (thas.isStatic ? 1 : 0) - (that.isStatic ? 1 : 0);
        return result;
    }
}
