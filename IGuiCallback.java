/*
 * Contributions to FindBugs
 * Copyright (C) 2009, Andrei Loskutov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Interface for any kind of GUI attached to the current FindBug analysis
 * 
 * @author Andrei
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.swing.JComponent;
import edu.umd.cs.findbugs.cloud.Cloud;
abstract public interface IGuiCallback {
/** If true, do not open windows or browsers */
/**
     * Called as soon as the cloud object is created, before it is initialized.
     * Useful for adding status msg listener.
     */
/**
     * Use this executor to queue bug collection updates without interfering
     * with the GUI. Runs on the AWT event thread.
     */
    public static class FormItem extends java.lang.Object {
        private java.lang.String label;
        private java.lang.String defaultValue;
        private boolean password = false;
        private java.util.List<java.lang.String> possibleValues;
        private javax.swing.JComponent field;
        private java.lang.String currentValue;
        private java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> items;
        public FormItem(java.lang.String label) {
            this(label,null,null);
        }
        public FormItem(java.lang.String label, java.lang.String defaultValue) {
            this(label,defaultValue,null);
        }
        public FormItem(java.lang.String label, java.lang.String defaultValue, java.util.List<java.lang.String> possibleValues) {
            super();
            this.label = label;
            this.defaultValue = defaultValue;
            this.possibleValues = possibleValues;
        }
        public edu.umd.cs.findbugs.IGuiCallback.FormItem password() {
            password = true;
            return this;
        }
        public boolean isPassword() {
            return password;
        }
        public java.lang.String getLabel() {
            return label;
        }
        public java.lang.String getDefaultValue() {
            return defaultValue;
        }
        public java.util.List<java.lang.String> getPossibleValues() {
            return possibleValues;
        }
        public javax.swing.JComponent getField() {
            return field;
        }
        public void setField(javax.swing.JComponent field) {
            this.field = field;
        }
        public void setItems(java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> items) {
            this.items = items;
        }
        public java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> getItems() {
            return items;
        }
        public void setCurrentValue(java.lang.String currentValue) {
            this.currentValue = currentValue;
        }
        public java.lang.String getCurrentValue() {
            return currentValue;
        }
        public void updated() {
        }
    }
    abstract boolean isHeadless();
    abstract void showMessageDialog(java.lang.String message);
    final public static int YES_OPTION = 0;
    final public static int NO_OPTION = 1;
    final public static int CANCEL_OPTION = 2;
    final public static int YES_NO_OPTION = 0;
    final public static int YES_NO_CANCEL_OPTION = 1;
    final public static int OK_CANCEL_OPTION = 2;
    abstract void invokeInGUIThread(java.lang.Runnable r);
    abstract int showConfirmDialog(java.lang.String message, java.lang.String title, java.lang.String ok, java.lang.String cancel);
    abstract java.lang.String showQuestionDialog(java.lang.String message, java.lang.String title, java.lang.String defaultValue);
    abstract java.util.List<java.lang.String> showForm(java.lang.String message, java.lang.String title, java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> labels);
    abstract java.io.InputStream getProgressMonitorInputStream(java.io.InputStream in, int length, java.lang.String msg);
    abstract void setErrorMessage(java.lang.String errorMsg);
    abstract void displayNonmodelMessage(java.lang.String title, java.lang.String message);
    abstract boolean showDocument(java.net.URL u);
    abstract void registerCloud(edu.umd.cs.findbugs.Project project, edu.umd.cs.findbugs.BugCollection collection, edu.umd.cs.findbugs.cloud.Cloud cloud);
    abstract void unregisterCloud(edu.umd.cs.findbugs.Project project, edu.umd.cs.findbugs.BugCollection collection, edu.umd.cs.findbugs.cloud.Cloud cloud);
    abstract java.util.concurrent.ExecutorService getBugUpdateExecutor();
    abstract void showMessageDialogAndWait(java.lang.String message) throws java.lang.InterruptedException;
}
