package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import junit.framework.Assert;
import junit.framework.TestCase;
import edu.umd.cs.findbugs.xml.OutputStreamXMLOutput;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class BugInstanceTest extends junit.framework.TestCase {
    public BugInstanceTest() {
    }
    edu.umd.cs.findbugs.BugInstance b;
    protected void setUp() throws java.lang.Exception {
        b = new edu.umd.cs.findbugs.BugInstance("NP_NULL_ON_SOME_PATH", edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY);
        b.setProperty("A","a");
        b.setProperty("B","b");
        b.setProperty("C","c");
    }
    public void testPropertyIterator() {
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[]{"A", "B", "C"},new java.lang.String[]{"a", "b", "c"});
    }
    public void testRemoveThroughIterator1() {
        this.removeThroughIterator(b.propertyIterator(),"A");
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[]{"B", "C"},new java.lang.String[]{"b", "c"});
    }
    public void testRemoveThroughIterator2() {
        this.removeThroughIterator(b.propertyIterator(),"B");
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[]{"A", "C"},new java.lang.String[]{"a", "c"});
    }
    public void testRemoveThroughIterator3() {
        this.removeThroughIterator(b.propertyIterator(),"C");
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[]{"A", "B"},new java.lang.String[]{"a", "b"});
    }
    public void testIterateTooFar() {
        java.util.Iterator<edu.umd.cs.findbugs.BugProperty> iter = b.propertyIterator();
        this.get(iter);
        this.get(iter);
        this.get(iter);
        this.noMore(iter);
    }
    public void testMultipleRemove() {
        java.util.Iterator<edu.umd.cs.findbugs.BugProperty> iter = b.propertyIterator();
        iter.next();
        iter.remove();
        try {
            iter.remove();
            fail();
        }
        catch (java.lang.IllegalStateException e){
            assert true;
        }
    }
    public void testRemoveBeforeNext() {
        java.util.Iterator<edu.umd.cs.findbugs.BugProperty> iter = b.propertyIterator();
        try {
            iter.remove();
            junit.framework.Assert.fail();
        }
        catch (java.lang.IllegalStateException e){
            assert true;
        }
    }
    public void testRemoveAndAdd() {
        this.removeThroughIterator(b.propertyIterator(),"C");
        b.setProperty("D","d");
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[]{"A", "B", "D"},new java.lang.String[]{"a", "b", "d"});
        b.setProperty("E","e");
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[]{"A", "B", "D", "E"},new java.lang.String[]{"a", "b", "d", "e"});
    }
    public void testRemoveAll1() {
        this.removeThroughIterator(b.propertyIterator(),"A");
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[]{"B", "C"},new java.lang.String[]{"b", "c"});
        this.removeThroughIterator(b.propertyIterator(),"B");
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[]{"C"},new java.lang.String[]{"c"});
        this.removeThroughIterator(b.propertyIterator(),"C");
        this.checkPropertyIterator(b.propertyIterator(),new java.lang.String[0],new java.lang.String[0]);
    }
    public void testWriteCloudPropertiesWithoutMessagesEnabled() throws java.lang.Exception {
        edu.umd.cs.findbugs.BugInstance inst = new edu.umd.cs.findbugs.BugInstance("ABC", 2);
        inst.getXmlProps().setConsensus("NOT_A_BUG");
        inst.getXmlProps().setFirstSeen(edu.umd.cs.findbugs.BugInstance.firstSeenXMLFormat().parse("4/11/10 2:00 PM"));
        inst.getXmlProps().setReviewCount(3);
        edu.umd.cs.findbugs.SortedBugCollection bc = new edu.umd.cs.findbugs.SortedBugCollection();
        bc.setWithMessages(false);
        java.lang.String output = this.writeXML(inst,bc);
        java.lang.System.err.println(output);
        assertTrue("firstSeen",output.contains("firstSeen=\"4/11/10 2:00 PM\""));
        assertTrue("consensus",output.contains("consensus=\"NOT_A_BUG\""));
        assertTrue("reviews",output.contains("reviews=\"3\""));
        assertFalse("notAProblem",output.contains("notAProblem"));
        assertFalse("ageInDays",output.contains("ageInDays"));
    }
    public void testWriteCloudPropertiesWithMessagesEnabled() throws java.lang.Exception {
        edu.umd.cs.findbugs.BugInstance inst = new edu.umd.cs.findbugs.BugInstance("ABC", 2);
        inst.addClass("my.class");
        inst.getXmlProps().setConsensus("NOT_A_BUG");
        inst.getXmlProps().setFirstSeen(edu.umd.cs.findbugs.BugInstance.firstSeenXMLFormat().parse("4/11/10 2:00 PM"));
        inst.getXmlProps().setReviewCount(3);
        edu.umd.cs.findbugs.SortedBugCollection bc = new edu.umd.cs.findbugs.SortedBugCollection();
        bc.setWithMessages(true);
        java.lang.String output = this.writeXML(inst,bc);
        java.lang.System.err.println(output);
        assertTrue("firstSeen",output.contains("firstSeen=\"4/11/10 2:00 PM\""));
        assertTrue("consensus",output.contains("consensus=\"NOT_A_BUG\""));
        assertTrue("reviews",output.contains("reviews=\"3\""));
        assertTrue("notAProblem",output.contains("notAProblem=\"true\""));
        assertTrue("ageInDays",output.contains("ageInDays="));
    }
    private java.lang.String writeXML(edu.umd.cs.findbugs.BugInstance inst, edu.umd.cs.findbugs.BugCollection bc) throws java.io.IOException {
        java.io.ByteArrayOutputStream bout = new java.io.ByteArrayOutputStream();
        edu.umd.cs.findbugs.xml.XMLOutput out = new edu.umd.cs.findbugs.xml.OutputStreamXMLOutput(bout);
        inst.writeXML(out,bc,bc.getWithMessages());
        out.finish();
        return new java.lang.String(bout.toByteArray(), "UTF-8");
    }
    private void get(java.util.Iterator<edu.umd.cs.findbugs.BugProperty> iter) {
        try {
            iter.next();
        }
        catch (java.util.NoSuchElementException e){
            junit.framework.Assert.assertTrue(false);
        }
    }
    private void noMore(java.util.Iterator<edu.umd.cs.findbugs.BugProperty> iter) {
        try {
            iter.next();
            junit.framework.Assert.fail();
        }
        catch (java.util.NoSuchElementException e){
            assert true;
        }
    }
    private void checkPropertyIterator(java.util.Iterator<edu.umd.cs.findbugs.BugProperty> iter, java.lang.String[] names, java.lang.String[] values) {
        if (names.length != values.length) throw new java.lang.IllegalArgumentException();
        for (int i = 0; i < names.length; ++i) {
            junit.framework.Assert.assertTrue(iter.hasNext());
            java.lang.String name = names[i];
            java.lang.String value = values[i];
            this.checkProperty(iter.next(),name,value);
        }
        junit.framework.Assert.assertFalse(iter.hasNext());
    }
    private void checkProperty(edu.umd.cs.findbugs.BugProperty property, java.lang.String name, java.lang.String value) {
        junit.framework.Assert.assertEquals(property.getName(),name);
        junit.framework.Assert.assertEquals(property.getValue(),value);
    }
    private void removeThroughIterator(java.util.Iterator<edu.umd.cs.findbugs.BugProperty> iter, java.lang.String name) {
        while (iter.hasNext()) {
            edu.umd.cs.findbugs.BugProperty prop = iter.next();
            if (prop.getName().equals(name)) iter.remove();
        }
    }
}
