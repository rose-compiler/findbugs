/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005 Dave Brosius <dbrosius@users.sourceforge.net>
 * Copyright (C) 2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.StatelessDetector;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
public class RedundantInterfaces extends edu.umd.cs.findbugs.visitclass.PreorderVisitor implements edu.umd.cs.findbugs.Detector, edu.umd.cs.findbugs.StatelessDetector {
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    public RedundantInterfaces(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.JavaClass obj = classContext.getJavaClass();
        java.lang.String superClassName = obj.getSuperclassName();
        if (superClassName.equals("java.lang.Object")) return;
        java.lang.String[] interfaceNames = obj.getInterfaceNames();
        if ((interfaceNames == null) || (interfaceNames.length == 0)) return;
        try {
            org.apache.bcel.classfile.JavaClass superObj = obj.getSuperClass();
            java.util.SortedSet<java.lang.String> redundantInfNames = new java.util.TreeSet<java.lang.String>();
            for (java.lang.String interfaceName : interfaceNames){
                if ( !"java/io/Serializable".equals(interfaceName)) {
                    org.apache.bcel.classfile.JavaClass inf = org.apache.bcel.Repository.lookupClass(interfaceName.replace('\u002f','\u002e'));
                    if (superObj.instanceOf(inf)) redundantInfNames.add(inf.getClassName());
                }
            }
;
            if (redundantInfNames.size() > 0) {
                edu.umd.cs.findbugs.BugInstance bug = new edu.umd.cs.findbugs.BugInstance(this, "RI_REDUNDANT_INTERFACES", LOW_PRIORITY).addClass(obj);
                for (java.lang.String redundantInfName : redundantInfNames)bug.addClass(redundantInfName).describe("INTERFACE_TYPE");
;
                bugReporter.reportBug(bug);
            }
        }
        catch (java.lang.ClassNotFoundException cnfe){
            bugReporter.reportMissingClass(cnfe);
        }
    }
    public void report() {
    }
}
