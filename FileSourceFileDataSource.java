/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Data source for source files which are stored in the filesystem.
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
public class FileSourceFileDataSource extends java.lang.Object implements edu.umd.cs.findbugs.ba.SourceFileDataSource {
    private java.lang.String fileName;
    public FileSourceFileDataSource(java.lang.String fileName) {
        super();
        this.fileName = fileName;
    }
    public java.io.InputStream open() throws java.io.IOException {
        return new java.io.BufferedInputStream(new java.io.FileInputStream(fileName));
    }
    public java.lang.String getFullFileName() {
        return fileName;
    }
    public long getLastModified() {
        return new java.io.File(fileName).lastModified();
    }
}
// vim:ts=4
