/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Abstract dataflow value representing a value which may or may not be a
 * constant.
 * 
 * @see edu.umd.cs.findbugs.ba.constant.ConstantAnalysis
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.constant;
import edu.umd.cs.findbugs.ba.constant.*;
import javax.annotation.Nullable;
public class Constant extends java.lang.Object {
    final private java.lang.Object value;
/**
     * Single instance representing all non-constant values.
     */
    final public static edu.umd.cs.findbugs.ba.constant.Constant NOT_CONSTANT = new edu.umd.cs.findbugs.ba.constant.Constant(null);
/**
     * Constructor for a constant value.
     * 
     * @param value
     *            the constant value; must be a String, Integer, etc.
     */
    public Constant(java.lang.Object value) {
        super();
        this.value = value;
    }
/**
     * Return whether or not this value is a constant.
     * 
     * @return true if the value is a constant, false if not
     */
    public boolean isConstant() {
        return value != null;
    }
/**
     * Return whether or not this value is a constant String.
     * 
     * @return true if the value is a constant String, false if not
     */
    public boolean isConstantString() {
        return this.isConstant() && (value instanceof java.lang.String);
    }
/**
     * Get the constant String value of this value.
     * 
     * @return the constant String value
     */
    public java.lang.String getConstantString() {
        return (java.lang.String) (value) ;
    }
/**
     * Return whether or not this value is a constant int/Integer.
     * 
     * @return true if the value is a constant int/Integer, false if not
     */
    public boolean isConstantInteger() {
        return this.isConstant() && (value instanceof java.lang.Integer);
    }
/**
     * Get the constant int value of this value.
     * 
     * @return the constant int value
     */
    public int getConstantInt() {
        return ((java.lang.Integer) (value) ).intValue();
    }
/**
     * Merge two Constants.
     * 
     * @param a
     *            a Constant
     * @param b
     *            another Constant
     * @return the merge (dataflow meet) of the two Constants
     */
    public static edu.umd.cs.findbugs.ba.constant.Constant merge(edu.umd.cs.findbugs.ba.constant.Constant a, edu.umd.cs.findbugs.ba.constant.Constant b) {
        if ( !a.isConstant() ||  !b.isConstant()) return NOT_CONSTANT;
        if (a.value.getClass() != b.value.getClass() ||  !a.value.equals(b.value)) return NOT_CONSTANT;
        return a;
    }
    public boolean equals(java.lang.Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) return false;
        edu.umd.cs.findbugs.ba.constant.Constant other = (edu.umd.cs.findbugs.ba.constant.Constant) (obj) ;
        if (other.value == this.value) return true;
        else if (other.value == null || this.value == null) return false;
        else return this.value.equals(other.value);
    }
    public int hashCode() {
        return (value == null) ? 123 : value.hashCode();
    }
    public java.lang.String toString() {
        if ( !this.isConstant()) {
            return "-";
        }
        else {
            return "<" + value.toString() + ">";
        }
    }
}
