/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Factory for BackwardTypeQualifierDataflow objects for given type qualifier
 * values.
 *
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.jsr305;
import edu.umd.cs.findbugs.ba.jsr305.*;
import javax.annotation.meta.When;
import org.apache.bcel.generic.ConstantPoolGen;
import edu.umd.cs.findbugs.ba.BasicBlock;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DepthFirstSearch;
import edu.umd.cs.findbugs.ba.ReverseDepthFirstSearch;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class BackwardTypeQualifierDataflowFactory extends edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDataflowFactory {
/**
     * Constructor.
     *
     * @param methodDescriptor
     *            MethodDescriptor of the method for which we want to create
     *            BackwardTypeQualifierDataflow objects
     */
    public BackwardTypeQualifierDataflowFactory(edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor) {
        super(methodDescriptor);
    }
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDataflowFactory#getDataflow
     * (edu.umd.cs.findbugs.ba.DepthFirstSearch, edu.umd.cs.findbugs.ba.XMethod,
     * edu.umd.cs.findbugs.ba.CFG,
     * edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow,
     * org.apache.bcel.generic.ConstantPoolGen,
     * edu.umd.cs.findbugs.classfile.IAnalysisCache,
     * edu.umd.cs.findbugs.classfile.MethodDescriptor,
     * edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue)
     */
    protected edu.umd.cs.findbugs.ba.jsr305.BackwardTypeQualifierDataflow getDataflow(edu.umd.cs.findbugs.ba.DepthFirstSearch dfs, edu.umd.cs.findbugs.ba.XMethod xmethod, edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue typeQualifierValue) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        edu.umd.cs.findbugs.ba.ReverseDepthFirstSearch rdfs = analysisCache.getMethodAnalysis(edu.umd.cs.findbugs.ba.ReverseDepthFirstSearch.class,methodDescriptor);
        edu.umd.cs.findbugs.ba.jsr305.BackwardTypeQualifierDataflowAnalysis analysis = new edu.umd.cs.findbugs.ba.jsr305.BackwardTypeQualifierDataflowAnalysis(dfs, rdfs, xmethod, cfg, vnaDataflow, cpg, typeQualifierValue);
// Get the corresponding forward dataflow.
// We use it to halt tracking of backwards values once we know
// that they encounter a conflicting forward value.
        edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflowFactory forwardFactory = analysisCache.getMethodAnalysis(edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflowFactory.class,methodDescriptor);
        edu.umd.cs.findbugs.ba.jsr305.ForwardTypeQualifierDataflow forwardDataflow = forwardFactory.getDataflow(typeQualifierValue);
        analysis.setForwardTypeQualifierDataflow(forwardDataflow);
        analysis.registerSourceSinkLocations();
        edu.umd.cs.findbugs.ba.jsr305.BackwardTypeQualifierDataflow dataflow = new edu.umd.cs.findbugs.ba.jsr305.BackwardTypeQualifierDataflow(cfg, analysis);
        dataflow.execute();
        if (edu.umd.cs.findbugs.ba.ClassContext.DUMP_DATAFLOW_ANALYSIS) {
            dataflow.dumpDataflow(analysis);
        }
        return dataflow;
    }
    protected void populateDatabase(edu.umd.cs.findbugs.ba.jsr305.BackwardTypeQualifierDataflow dataflow, edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow, edu.umd.cs.findbugs.ba.XMethod xmethod, edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValue tqv) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        assert edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDatabase.USE_DATABASE;
// Get the dataflow fact that propagated
// back to the entry of the method.
// This will contain the effective type qualifier
// annotations on the method parameters.
        if (xmethod.isIdentity()) return;
        edu.umd.cs.findbugs.ba.BasicBlock entry = dataflow.getCFG().getEntry();
        edu.umd.cs.findbugs.ba.jsr305.TypeQualifierValueSet entryFact = dataflow.getAnalysis().getResultFact(entry);
        for (int i = 0; i < xmethod.getNumParams(); i++) {
            if (edu.umd.cs.findbugs.ba.jsr305.TypeQualifierApplications.getEffectiveTypeQualifierAnnotation(xmethod,i,tqv) != null) {
                continue;
            }
            edu.umd.cs.findbugs.ba.vna.ValueNumber paramVN = vnaDataflow.getAnalysis().getEntryValueForParameter(i);
            edu.umd.cs.findbugs.ba.jsr305.FlowValue paramFlowValue = entryFact.getValue(paramVN);
            if (paramFlowValue == edu.umd.cs.findbugs.ba.jsr305.FlowValue.ALWAYS || paramFlowValue == edu.umd.cs.findbugs.ba.jsr305.FlowValue.NEVER) {
                edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDatabase tqdb = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getDatabase(edu.umd.cs.findbugs.ba.jsr305.TypeQualifierDatabase.class);
                edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation tqa = edu.umd.cs.findbugs.ba.jsr305.TypeQualifierAnnotation.getValue(tqv,paramFlowValue == edu.umd.cs.findbugs.ba.jsr305.FlowValue.ALWAYS ? javax.annotation.meta.When.ALWAYS : javax.annotation.meta.When.NEVER);
                tqdb.setParameter(xmethod.getMethodDescriptor(),i,tqv,tqa);
            }
        }
    }
}
// this parameter already has an explicit annotation
// Get the value number for this parameter
