/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba.bcp;
import edu.umd.cs.findbugs.ba.bcp.*;
import javax.annotation.Nullable;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
public class FieldVariable extends java.lang.Object implements edu.umd.cs.findbugs.ba.bcp.Variable {
    final private edu.umd.cs.findbugs.ba.vna.ValueNumber ref;
    final private java.lang.String className;
    final private java.lang.String fieldName;
    final private java.lang.String fieldSig;
/**
     * Constructor for static fields.
     * 
     * @param className
     *            the class name
     * @param fieldName
     *            the field name
     * @param fieldSig
     *            the field signature
     */
    public FieldVariable(java.lang.String className, java.lang.String fieldName, java.lang.String fieldSig) {
        this(null,className,fieldName,fieldSig);
    }
/**
     * Constructor for instance fields.
     * 
     * @param ref
     *            ValueNumber of the object reference
     * @param className
     *            the class name
     * @param fieldName
     *            the field name
     * @param fieldSig
     *            the field signature
     */
    public FieldVariable(edu.umd.cs.findbugs.ba.vna.ValueNumber ref, java.lang.String className, java.lang.String fieldName, java.lang.String fieldSig) {
        super();
        this.ref = ref;
        this.className = className;
        this.fieldName = fieldName;
        this.fieldSig = fieldSig;
    }
/**
     * Return whether or not this is a static field.
     */
    public boolean isStatic() {
        return ref == null;
    }
/**
     * Get the class name.
     */
    public java.lang.String getClassName() {
        return className;
    }
/**
     * Get the field name.
     */
    public java.lang.String getFieldName() {
        return fieldName;
    }
/**
     * Get the field signature.
     */
    public java.lang.String getFieldSig() {
        return fieldSig;
    }
    public boolean sameAs(edu.umd.cs.findbugs.ba.bcp.Variable other) {
        if ( !(other instanceof edu.umd.cs.findbugs.ba.bcp.FieldVariable)) return false;
        edu.umd.cs.findbugs.ba.bcp.FieldVariable otherField = (edu.umd.cs.findbugs.ba.bcp.FieldVariable) (other) ;
        if (this.isStatic() != otherField.isStatic()) return false;
        return (ref == null || ref.equals(otherField.ref)) && className.equals(otherField.className) && fieldName.equals(otherField.fieldName) && fieldSig.equals(otherField.fieldSig);
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append(className);
        buf.append('\u002e');
        buf.append(fieldName);
        return buf.toString();
    }
}
// vim:ts=4
