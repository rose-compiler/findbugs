/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import java.io.IOException;
import edu.umd.cs.findbugs.BugAnnotation;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.MethodAnnotation;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class MethodMatcher extends edu.umd.cs.findbugs.filter.MemberMatcher implements edu.umd.cs.findbugs.filter.Matcher {
    public MethodMatcher(java.lang.String name) {
        super(name);
    }
    public MethodMatcher(java.lang.String name, java.lang.String role) {
        super(name,null,role);
    }
    public MethodMatcher(java.lang.String name, java.lang.String params, java.lang.String returns) {
        super(name,edu.umd.cs.findbugs.filter.SignatureUtil.createMethodSignature(params,returns));
    }
    public MethodMatcher(java.lang.String name, java.lang.String params, java.lang.String returns, java.lang.String role) {
        super(name,edu.umd.cs.findbugs.filter.SignatureUtil.createMethodSignature(params,returns),role);
    }
    public boolean match(edu.umd.cs.findbugs.BugInstance bugInstance) {
        edu.umd.cs.findbugs.MethodAnnotation methodAnnotation = null;
        if (role == null || role.equals("")) methodAnnotation = bugInstance.getPrimaryMethod();
        else for (edu.umd.cs.findbugs.BugAnnotation a : bugInstance.getAnnotations())if (a instanceof edu.umd.cs.findbugs.MethodAnnotation && role.equals(a.getDescription())) {
            methodAnnotation = (edu.umd.cs.findbugs.MethodAnnotation) (a) ;
            break;
        }
;
        if (methodAnnotation == null) return false;
        if ( !name.match(methodAnnotation.getMethodName())) return false;
        if (signature != null &&  !signature.match(methodAnnotation.getMethodSignature())) return false;
        return true;
    }
    public java.lang.String toString() {
        return "Method(" + super.toString() + ")";
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean disabled) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributes = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("name",name.getSpec());
        if (signature != null) attributes.addOptionalAttribute("signature",signature.getSpec()).addOptionalAttribute("role",role);
        if (disabled) attributes.addAttribute("disabled","true");
        xmlOutput.openCloseTag("Method",attributes);
    }
}
// vim:ts=4
