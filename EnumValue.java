/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Represents an enumeration value used with an application of an annotation.
 * 
 * @author William Pugh
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.analysis;
import edu.umd.cs.findbugs.classfile.analysis.*;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
public class EnumValue extends java.lang.Object {
    final public edu.umd.cs.findbugs.classfile.ClassDescriptor desc;
    final public java.lang.String value;
    public EnumValue(java.lang.String desc, java.lang.String value) {
        super();
        this.desc = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptorFromSignature(desc);
        this.value = value;
    }
    public java.lang.String toString() {
        return desc.getDottedClassName() + "." + value;
    }
    public int hashCode() {
        return desc.hashCode() + 37 * value.hashCode();
    }
    public boolean equals(java.lang.Object obj) {
        if (obj == null ||  !(obj instanceof edu.umd.cs.findbugs.classfile.analysis.EnumValue)) {
            return false;
        }
        edu.umd.cs.findbugs.classfile.analysis.EnumValue other = (edu.umd.cs.findbugs.classfile.analysis.EnumValue) (obj) ;
        return this.desc.equals(other.desc) && this.value.equals(other.value);
    }
}
