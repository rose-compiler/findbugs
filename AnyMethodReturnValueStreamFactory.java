/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Factory for stream objects of a particular base class type returned by any
 * method. This factory helps us keep track of streams returned by methods; we
 * don't want to report them, but we do want to keep track of whether or not
 * they are closed, to avoid reporting unclosed streams in the same equivalence
 * class.
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.ObjectType;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.ObjectTypeFactory;
import edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback;
public class AnyMethodReturnValueStreamFactory extends java.lang.Object implements edu.umd.cs.findbugs.detect.StreamFactory {
    private org.apache.bcel.generic.ObjectType baseClassType;
    private java.lang.String bugType;
    public AnyMethodReturnValueStreamFactory(java.lang.String streamBase) {
        super();
        this.baseClassType = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance(streamBase);
        this.bugType = null;
    }
    public edu.umd.cs.findbugs.detect.AnyMethodReturnValueStreamFactory setBugType(java.lang.String bugType) {
        this.bugType = bugType;
        return this;
    }
    public edu.umd.cs.findbugs.detect.Stream createStream(edu.umd.cs.findbugs.ba.Location location, org.apache.bcel.generic.ObjectType type, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback lookupFailureCallback) {
        org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
        try {
            if (ins instanceof org.apache.bcel.generic.InvokeInstruction) {
                if ( !edu.umd.cs.findbugs.ba.Hierarchy.isSubtype(type,baseClassType)) return null;
                edu.umd.cs.findbugs.detect.Stream stream = new edu.umd.cs.findbugs.detect.Stream(location, type.getClassName(), baseClassType.getClassName()).setIsOpenOnCreation(true).setIgnoreImplicitExceptions(true);
                if (bugType != null) stream.setInteresting(bugType);
                return stream;
            }
        }
        catch (java.lang.ClassNotFoundException e){
            lookupFailureCallback.reportMissingClass(e);
        }
        return null;
    }
}
// vim:ts=4
