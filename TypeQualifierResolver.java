/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Resolve annotations into type qualifiers.
 * 
 * @author William Pugh
 */
package edu.umd.cs.findbugs.ba.jsr305;
import edu.umd.cs.findbugs.ba.jsr305.*;
import java.lang.annotation.ElementType;
import java.util.Collection;
import java.util.LinkedList;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.MissingClassException;
import edu.umd.cs.findbugs.classfile.analysis.AnnotationValue;
import edu.umd.cs.findbugs.classfile.analysis.EnumValue;
public class TypeQualifierResolver extends java.lang.Object {
    public TypeQualifierResolver() {
    }
    static edu.umd.cs.findbugs.classfile.ClassDescriptor typeQualifier = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(javax.annotation.meta.TypeQualifier.class);
    static edu.umd.cs.findbugs.classfile.ClassDescriptor typeQualifierNickname = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(javax.annotation.meta.TypeQualifierNickname.class);
    static edu.umd.cs.findbugs.classfile.ClassDescriptor typeQualifierDefault = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(javax.annotation.meta.TypeQualifierDefault.class);
    static edu.umd.cs.findbugs.classfile.ClassDescriptor elementTypeDescriptor = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(java.lang.annotation.ElementType.class);
    static edu.umd.cs.findbugs.classfile.ClassDescriptor googleNullable = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor("com/google/common/base/Nullable");
    static edu.umd.cs.findbugs.classfile.ClassDescriptor intellijNullable = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor("org/jetbrains/annotations/Nullable");
/**
     * Resolve an AnnotationValue into a list of AnnotationValues representing
     * type qualifier annotations.
     * 
     * @param value
     *            AnnotationValue representing the use of an annotation
     * @return Collection of AnnotationValues representing resolved
     *         TypeQualifier annotations
     */
    public static java.util.Collection<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> resolveTypeQualifiers(edu.umd.cs.findbugs.classfile.analysis.AnnotationValue value) {
        java.util.LinkedList<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> result = new java.util.LinkedList<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue>();
        resolveTypeQualifierNicknames(value,result,new java.util.LinkedList<edu.umd.cs.findbugs.classfile.ClassDescriptor>());
        return result;
    }
/**
     * Resolve collection of AnnotationValues (which have been used to annotate
     * an AnnotatedObject or method parameter) into collection of resolved type
     * qualifier AnnotationValues.
     * 
     * @param values
     *            Collection of AnnotationValues used to annotate an
     *            AnnotatedObject or method parameter
     * @return Collection of resolved type qualifier AnnotationValues
     */
    public static java.util.Collection<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> resolveTypeQualifierDefaults(java.util.Collection<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> values, java.lang.annotation.ElementType elementType) {
        java.util.LinkedList<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> result = new java.util.LinkedList<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue>();
        for (edu.umd.cs.findbugs.classfile.analysis.AnnotationValue value : values)resolveTypeQualifierDefaults(value,elementType,result);
;
        return result;
    }
/**
     * Resolve an annotation into AnnotationValues representing any type
     * qualifier(s) the annotation resolves to. Detects annotations which are
     * directly marked as TypeQualifier annotations, and also resolves the use
     * of TypeQualifierNickname annotations.
     * 
     * @param value
     *            AnnotationValue representing the use of an annotation
     * @param result
     *            LinkedList containing resolved type qualifier AnnotationValues
     * @param onStack
     *            stack of annotations being processed; used to detect cycles in
     *            type qualifier nicknames
     */
    private static void resolveTypeQualifierNicknames(edu.umd.cs.findbugs.classfile.analysis.AnnotationValue value, java.util.LinkedList<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> result, java.util.LinkedList<edu.umd.cs.findbugs.classfile.ClassDescriptor> onStack) {
        edu.umd.cs.findbugs.classfile.ClassDescriptor annotationClass = value.getAnnotationClass();
        if (onStack.contains(annotationClass)) {
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Cycle found in type nicknames: " + onStack);
            return;
        }
        try {
            onStack.add(annotationClass);
            try {
                if (annotationClass.equals(googleNullable) || annotationClass.equals(intellijNullable)) {
                    resolveTypeQualifierNicknames(new edu.umd.cs.findbugs.classfile.analysis.AnnotationValue(edu.umd.cs.findbugs.ba.jsr305.JSR305NullnessAnnotations.CHECK_FOR_NULL),result,onStack);
                    return;
                }
                edu.umd.cs.findbugs.ba.XClass c = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getClassAnalysis(edu.umd.cs.findbugs.ba.XClass.class,annotationClass);
                if (c.getAnnotationDescriptors().contains(typeQualifierNickname)) {
                    for (edu.umd.cs.findbugs.classfile.ClassDescriptor d : c.getAnnotationDescriptors())if ( !c.equals(typeQualifierNickname)) resolveTypeQualifierNicknames(c.getAnnotation(d),result,onStack);
;
                }
                else if (c.getAnnotationDescriptors().contains(typeQualifier)) {
                    result.add(value);
                }
            }
            catch (edu.umd.cs.findbugs.classfile.MissingClassException e){
                logMissingAnnotationClass(e);
                return;
            }
            catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
                edu.umd.cs.findbugs.ba.AnalysisContext.logError("Error resolving " + annotationClass,e);
                return;
            }
        }
        finally {
            onStack.removeLast();
        }
    }
    public static void logMissingAnnotationClass(edu.umd.cs.findbugs.classfile.MissingClassException e) {
        edu.umd.cs.findbugs.classfile.ClassDescriptor c = e.getClassDescriptor();
        if (c.getClassName().startsWith("javax.annotation")) edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getLookupFailureCallback().reportMissingClass(c);
    }
/**
     * Resolve collection of AnnotationValues (which have been used to annotate
     * an AnnotatedObject or method parameter) into collection of resolved type
     * qualifier AnnotationValues.
     * 
     * @param values
     *            Collection of AnnotationValues used to annotate an
     *            AnnotatedObject or method parameter
     * @return Collection of resolved type qualifier AnnotationValues
     */
    public static java.util.Collection<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> resolveTypeQualifiers(java.util.Collection<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> values) {
        java.util.LinkedList<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> result = new java.util.LinkedList<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue>();
        java.util.LinkedList<edu.umd.cs.findbugs.classfile.ClassDescriptor> onStack = new java.util.LinkedList<edu.umd.cs.findbugs.classfile.ClassDescriptor>();
        for (edu.umd.cs.findbugs.classfile.analysis.AnnotationValue value : values)resolveTypeQualifierNicknames(value,result,onStack);
;
        return result;
    }
/**
     * Resolve an annotation into AnnotationValues representing any type
     * qualifier(s) the annotation resolves to. Detects annotations which are
     * directly marked as TypeQualifier annotations, and also resolves the use
     * of TypeQualifierNickname annotations.
     * 
     * @param value
     *            AnnotationValue representing the use of an annotation
     * @param result
     *            LinkedList containing resolved type qualifier AnnotationValues
     * @param onStack
     *            stack of annotations being processed; used to detect cycles in
     *            type qualifier nicknames
     */
    private static void resolveTypeQualifierDefaults(edu.umd.cs.findbugs.classfile.analysis.AnnotationValue value, java.lang.annotation.ElementType defaultFor, java.util.LinkedList<edu.umd.cs.findbugs.classfile.analysis.AnnotationValue> result) {
        try {
            edu.umd.cs.findbugs.ba.XClass c = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getClassAnalysis(edu.umd.cs.findbugs.ba.XClass.class,value.getAnnotationClass());
            edu.umd.cs.findbugs.classfile.analysis.AnnotationValue defaultAnnotation = c.getAnnotation(typeQualifierDefault);
            if (defaultAnnotation == null) return;
            for (java.lang.Object o : (java.lang.Object[]) (defaultAnnotation.getValue("value")) )if (o instanceof edu.umd.cs.findbugs.classfile.analysis.EnumValue) {
                edu.umd.cs.findbugs.classfile.analysis.EnumValue e = (edu.umd.cs.findbugs.classfile.analysis.EnumValue) (o) ;
                if (e.desc.equals(elementTypeDescriptor) && e.value.equals(defaultFor.name())) {
                    for (edu.umd.cs.findbugs.classfile.ClassDescriptor d : c.getAnnotationDescriptors())if ( !d.equals(typeQualifierDefault)) resolveTypeQualifierNicknames(c.getAnnotation(d),result,new java.util.LinkedList<edu.umd.cs.findbugs.classfile.ClassDescriptor>());
;
                    break;
                }
            }
;
        }
        catch (edu.umd.cs.findbugs.classfile.MissingClassException e){
            logMissingAnnotationClass(e);
        }
        catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Error resolving " + value.getAnnotationClass(),e);
        }
        catch (java.lang.ClassCastException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("ClassCastException " + value.getAnnotationClass(),e);
        }
    }
}
