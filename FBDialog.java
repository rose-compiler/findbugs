/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * All Dialogs are FBDialogs so font size will work.
 * 
 * @author Kristin
 * 
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.JDialog;
public class FBDialog extends javax.swing.JDialog {
    public FBDialog() {
        super(edu.umd.cs.findbugs.gui2.MainFrame.getInstance());
    }
    public FBDialog(java.awt.Frame f) {
        super(f);
    }
    public FBDialog(java.awt.Dialog d) {
        super(d);
    }
/**
     * Sets size of font
     * 
     * @param size
     */
    protected void setFontSize(float size) {
        this.setFont(this.getFont().deriveFont(size));
        this.setFontSizeHelper(this.getComponents(),size);
    }
/*
     * Helps above method, runs through all components recursively.
     */
    protected void setFontSizeHelper(java.awt.Component[] comps, float size) {
        if (comps.length <= 0) return;
        for (java.awt.Component comp : comps){
            comp.setFont(comp.getFont().deriveFont(size));
            if (comp instanceof java.awt.Container) this.setFontSizeHelper(((java.awt.Container) (comp) ).getComponents(),size);
        }
;
    }
    public void addNotify() {
        super.addNotify();
        this.setFontSize(edu.umd.cs.findbugs.gui2.Driver.getFontSize());
    }
}
