/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.HashMap;
public class AnnotationRetentionDatabase extends java.lang.Object {
    public AnnotationRetentionDatabase() {
    }
    private java.util.HashMap<java.lang.String, java.lang.Boolean> annotationRetention = new java.util.HashMap<java.lang.String, java.lang.Boolean>();
    public boolean hasRuntimeRetention(java.lang.String dottedClassName) {
        java.lang.Boolean result = annotationRetention.get(dottedClassName);
        if (result == null) return false;
        return result;
    }
/** return false if it has class retention *or* if the retention is unknown */
    public boolean lacksRuntimeRetention(java.lang.String dottedClassName) {
        java.lang.Boolean result = annotationRetention.get(dottedClassName);
        if (result == null) return false;
        return  !result;
    }
    public void setRuntimeRetention(java.lang.String dottedClassName, boolean value) {
        annotationRetention.put(dottedClassName,java.lang.Boolean.valueOf(value));
    }
}
