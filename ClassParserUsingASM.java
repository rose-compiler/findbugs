package edu.umd.cs.findbugs.classfile.engine;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.engine.*;
/**
 * @author William Pugh
 */
// static final boolean NO_SHIFT_INNER_CLASS_CTOR =
// SystemProperties.getBoolean("classparser.noshift");
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.engine.ClassParserInterface#parse(edu.umd
     * .cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo.Builder)
     */
// TODO Auto-generated method stub
// TODO Auto-generated method stub
// primitive array
// System.out.println("Call from " +
// ClassParserUsingASM.this.slashedClassName +
// " to " + owner + " : " + desc);
// else
// System.out.println(slashedClassName+"."+methodName+methodDesc
// + " is thrower");
// collect class references
// System.out.println("constant pool count: " + constantPoolCount);
// case ClassWriter.CLASS:
// case ClassWriter.STR:
// System.out.println(count + "@" + offset + " : [" + tag
// +"] size="+size);
abstract interface package-info {
}
