/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import edu.umd.cs.findbugs.ba.XMethod;
public class ProgramPoint extends java.lang.Object {
    public ProgramPoint(edu.umd.cs.findbugs.BytecodeScanningDetector v) {
        super();
        method = v.getXMethod();
        pc = v.getPC();
    }
    final public edu.umd.cs.findbugs.ba.XMethod method;
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((method == null) ? 0 : method.hashCode());
        result = prime * result + pc;
        return result;
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(java.lang.Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (this.getClass() != obj.getClass()) return false;
        edu.umd.cs.findbugs.ProgramPoint other = (edu.umd.cs.findbugs.ProgramPoint) (obj) ;
        if (method == null) {
            if (other.method != null) return false;
        }
        else if ( !method.equals(other.method)) return false;
        if (pc != other.pc) return false;
        return true;
    }
    final public int pc;
    public edu.umd.cs.findbugs.MethodAnnotation getMethodAnnotation() {
        return edu.umd.cs.findbugs.MethodAnnotation.fromXMethod(method);
    }
    public edu.umd.cs.findbugs.SourceLineAnnotation getSourceLineAnnotation() {
        return edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(method.getMethodDescriptor(),pc);
    }
    public java.lang.String toString() {
        return method.toString() + ":" + pc;
    }
}
