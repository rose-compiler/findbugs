/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce MethodGen objects for analyzed methods.
 * 
 * @author David Hovemeyer
 * @author Bill Pugh
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.AnalysisFeatures;
import edu.umd.cs.findbugs.ba.JavaClassAndMethod;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class MethodGenFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<org.apache.bcel.generic.MethodGen> {
/**
     * Constructor.
     */
    public MethodGenFactory() {
        super("MethodGen construction",org.apache.bcel.generic.MethodGen.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public org.apache.bcel.generic.MethodGen analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.classfile.Method method = this.getMethod(analysisCache,descriptor);
        if (method.getCode() == null) return null;
        try {
            edu.umd.cs.findbugs.ba.AnalysisContext analysisContext = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext();
            org.apache.bcel.classfile.JavaClass jclass = this.getJavaClass(analysisCache,descriptor.getClassDescriptor());
            org.apache.bcel.generic.ConstantPoolGen cpg = this.getConstantPoolGen(analysisCache,descriptor.getClassDescriptor());
            java.lang.String methodName = method.getName();
            int codeLength = method.getCode().getLength();
            java.lang.String superclassName = jclass.getSuperclassName();
            if (codeLength > 6000 && methodName.equals("<clinit>") && superclassName.equals("java.lang.Enum")) {
                analysisContext.getLookupFailureCallback().reportSkippedAnalysis(new edu.umd.cs.findbugs.ba.JavaClassAndMethod(jclass, method).toMethodDescriptor());
                return null;
            }
            if (analysisContext.getBoolProperty(edu.umd.cs.findbugs.ba.AnalysisFeatures.SKIP_HUGE_METHODS)) {
                if (codeLength > 6000 || (methodName.equals("<clinit>") || methodName.equals("getContents")) && codeLength > 2000) {
                    analysisContext.getLookupFailureCallback().reportSkippedAnalysis(new edu.umd.cs.findbugs.ba.JavaClassAndMethod(jclass, method).toMethodDescriptor());
                    return null;
                }
            }
            return new org.apache.bcel.generic.MethodGen(method, jclass.getClassName(), cpg);
        }
        catch (java.lang.Exception e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Error constructing methodGen",e);
            return null;
        }
    }
}
