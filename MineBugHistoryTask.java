/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Ant task to invoke the MineBugHistory program in the workflow package
 * 
 * @author David Hovemeyer
 * @author Ben Langmead
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import java.io.File;
import org.apache.tools.ant.BuildException;
public class MineBugHistoryTask extends edu.umd.cs.findbugs.anttask.AbstractFindBugsTask {
    private java.io.File outputFile;
    private java.lang.String formatDates;
    private java.lang.String noTabs;
    private java.lang.String summary;
    private edu.umd.cs.findbugs.anttask.DataFile inputFile;
    public MineBugHistoryTask() {
        super("edu.umd.cs.findbugs.workflow.MineBugHistory");
        this.setFailOnError(true);
    }
    public edu.umd.cs.findbugs.anttask.DataFile createDataFile() {
        if (inputFile != null) {
            throw new org.apache.tools.ant.BuildException("only one dataFile element is allowed", this.getLocation());
        }
        inputFile = new edu.umd.cs.findbugs.anttask.DataFile();
        return inputFile;
    }
    public void setOutput(java.io.File output) {
        this.outputFile = output;
    }
    public void setInput(java.lang.String input) {
        this.inputFile = new edu.umd.cs.findbugs.anttask.DataFile();
        this.inputFile.name = input;
    }
    public void setFormatDates(java.lang.String arg) {
        this.formatDates = arg;
    }
    public void setNoTabs(java.lang.String arg) {
        this.noTabs = arg;
    }
    public void setSummary(java.lang.String arg) {
        this.summary = arg;
    }
    private void checkBoolean(java.lang.String attrVal, java.lang.String attrName) {
        if (attrVal == null) {
            return;
        }
        attrVal = attrVal.toLowerCase();
        if ( !attrVal.equals("true") &&  !attrVal.equals("false")) {
            throw new org.apache.tools.ant.BuildException("attribute " + attrName + " requires boolean value", this.getLocation());
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#checkParameters()
     */
    protected void checkParameters() {
        super.checkParameters();
        if (inputFile == null) {
            throw new org.apache.tools.ant.BuildException("inputFile element is required");
        }
        this.checkBoolean(formatDates,"formatDates");
        this.checkBoolean(noTabs,"noTabs");
        this.checkBoolean(summary,"summary");
    }
    public void addBoolOption(java.lang.String option, java.lang.String value) {
        if (value != null) {
            this.addArg(option + ":" + value);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#configureFindbugsEngine
     * ()
     */
    protected void configureFindbugsEngine() {
        this.addBoolOption("-formatDates",formatDates);
        this.addBoolOption("-noTabs",noTabs);
        this.addBoolOption("-summary",summary);
        this.addArg(inputFile.getName());
        if (outputFile != null) {
            this.addArg(outputFile.getAbsolutePath());
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#beforeExecuteJavaProcess
     * ()
     */
    protected void beforeExecuteJavaProcess() {
        this.log("running mineBugHistory...");
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#afterExecuteJavaProcess
     * (int)
     */
    protected void afterExecuteJavaProcess(int rc) {
        if (rc != 0) {
            throw new org.apache.tools.ant.BuildException("execution of " + this.getTaskName() + " failed");
        }
    }
}
