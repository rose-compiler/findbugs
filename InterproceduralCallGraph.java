/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Class representing the interprocedural call graph. Vertices represent
 * methods. Edges represent method calls.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.ch;
import edu.umd.cs.findbugs.ba.ch.*;
import java.util.HashMap;
import java.util.Map;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
import edu.umd.cs.findbugs.graph.AbstractGraph;
public class InterproceduralCallGraph extends edu.umd.cs.findbugs.graph.AbstractGraph<edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphEdge, edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphVertex> {
    private java.util.Map<edu.umd.cs.findbugs.classfile.MethodDescriptor, edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphVertex> methodDescToVertexMap;
/**
     * Constructor.
     */
    public InterproceduralCallGraph() {
        super();
        this.methodDescToVertexMap = new java.util.HashMap<edu.umd.cs.findbugs.classfile.MethodDescriptor, edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphVertex>();
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.graph.AbstractGraph#addVertex(edu.umd.cs.findbugs
     * .graph.AbstractVertex)
     */
    public void addVertex(edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphVertex v) {
        super.addVertex(v);
        methodDescToVertexMap.put(v.getXmethod().getMethodDescriptor(),v);
    }
/**
     * Look up vertex corresponding to given method.
     * 
     * @param methodDesc
     *            a MethodDescriptor specifying a method
     * @return the InterproceduralCallGraphVertex representing that method, or
     *         null if no such vertex exists
     */
    public edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphVertex lookupVertex(edu.umd.cs.findbugs.classfile.MethodDescriptor methodDesc) {
        return methodDescToVertexMap.get(methodDesc);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.graph.AbstractGraph#allocateEdge(edu.umd.cs.findbugs
     * .graph.AbstractVertex, edu.umd.cs.findbugs.graph.AbstractVertex)
     */
    protected edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphEdge allocateEdge(edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphVertex source, edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphVertex target) {
        return new edu.umd.cs.findbugs.ba.ch.InterproceduralCallGraphEdge(source, target);
    }
}
