/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Sample detector, using ASM
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import static org.apache.bcel.Constants.I2D;
import static org.apache.bcel.Constants.INVOKESTATIC;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.MethodAnnotation;
import edu.umd.cs.findbugs.Priorities;
import edu.umd.cs.findbugs.asm.AbstractFBMethodVisitor;
import edu.umd.cs.findbugs.asm.ClassNodeDetector;
public class TestASM extends edu.umd.cs.findbugs.asm.ClassNodeDetector {
    public TestASM(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super(bugReporter);
    }
    public org.objectweb.asm.MethodVisitor visitMethod(final int access, final java.lang.String name, final java.lang.String desc, final java.lang.String signature, final java.lang.String[] exceptions) {
        if (java.lang.Character.isUpperCase(name.charAt(0))) {
            edu.umd.cs.findbugs.BugInstance bug0 = new edu.umd.cs.findbugs.BugInstance(this, "NM_METHOD_NAMING_CONVENTION", NORMAL_PRIORITY).addClass(this).addMethod(this.name,name,desc,access);
            bugReporter.reportBug(bug0);
        }
        return new edu.umd.cs.findbugs.asm.AbstractFBMethodVisitor() {
            int prevOpcode;
            int prevPC;
            public void visitInsn(int opcode) {
                prevOpcode = opcode;
                prevPC = this.getPC();
            }
            public void visitMethodInsn(int opcode, java.lang.String owner, java.lang.String invokedName, java.lang.String invokedDesc) {
                if (prevPC + 1 == this.getPC() && prevOpcode == I2D && opcode == INVOKESTATIC && owner.equals("java/lang/Math") && invokedName.equals("ceil") && invokedDesc.equals("(D)D")) {
                    edu.umd.cs.findbugs.BugInstance bug0 = new edu.umd.cs.findbugs.BugInstance(edu.umd.cs.findbugs.detect.TestASM.this, "ICAST_INT_CAST_TO_DOUBLE_PASSED_TO_CEIL", NORMAL_PRIORITY);
                    edu.umd.cs.findbugs.MethodAnnotation methodAnnotation = edu.umd.cs.findbugs.MethodAnnotation.fromForeignMethod(edu.umd.cs.findbugs.detect.TestASM.this.name,name,desc,access);
                    bug0.addClass(edu.umd.cs.findbugs.detect.TestASM.this).addMethod(methodAnnotation);
                    bugReporter.reportBug(bug0);
                }
            }
        };
    }
    public org.objectweb.asm.FieldVisitor visitField(int access, java.lang.String name, java.lang.String desc, java.lang.String signature, java.lang.Object value) {
        if ((access & org.objectweb.asm.Opcodes.ACC_STATIC) != 0 && (access & org.objectweb.asm.Opcodes.ACC_FINAL) != 0 && (access & org.objectweb.asm.Opcodes.ACC_PUBLIC) != 0 &&  !name.equals(name.toUpperCase())) bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_FIELD_NAMING_CONVENTION", edu.umd.cs.findbugs.Priorities.LOW_PRIORITY).addClass(this).addField(this.name,name,desc,access));
        return null;
    }
}
