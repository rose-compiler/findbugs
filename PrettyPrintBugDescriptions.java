/*
 * Generate HTML file containing bug descriptions
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.tools.html;
import edu.umd.cs.findbugs.tools.html.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import edu.umd.cs.findbugs.BugPattern;
import edu.umd.cs.findbugs.DetectorFactory;
import edu.umd.cs.findbugs.I18N;
public class PrettyPrintBugDescriptions extends edu.umd.cs.findbugs.tools.html.PlainPrintBugDescriptions {
    private static class BugPatternComparator extends java.lang.Object implements java.util.Comparator<edu.umd.cs.findbugs.BugPattern>, java.io.Serializable {
        public BugPatternComparator() {
        }
        public int compare(edu.umd.cs.findbugs.BugPattern a, edu.umd.cs.findbugs.BugPattern b) {
            int cmp = a.getCategory().compareTo(b.getCategory());
            if (cmp != 0) return cmp;
            cmp = a.getAbbrev().compareTo(b.getAbbrev());
            if (cmp != 0) return cmp;
            return a.getType().compareTo(b.getType());
        }
    }
    private java.util.Set<edu.umd.cs.findbugs.BugPattern> bugPatternSet;
    private java.lang.String headerText;
    private java.lang.String beginBodyText;
    private java.lang.String prologueText;
    private java.lang.String endBodyText;
    private boolean unabridged;
    final private static java.lang.String[] TABLE_COLORS = new java.lang.String[]{"#eeeeee", "#ffffff"};
    public PrettyPrintBugDescriptions(java.lang.String docTitle, java.io.OutputStream out) {
        super(docTitle,out);
        this.bugPatternSet = new java.util.TreeSet<edu.umd.cs.findbugs.BugPattern>(new edu.umd.cs.findbugs.tools.html.PrettyPrintBugDescriptions.BugPatternComparator());
        this.headerText = this.beginBodyText = this.prologueText = this.endBodyText = "";
    }
    public void setHeaderText(java.lang.String headerText) {
        this.headerText = headerText;
    }
    public void setBeginBodyText(java.lang.String beginBodyText) {
        this.beginBodyText = beginBodyText;
    }
    public void setPrologueText(java.lang.String prologueText) {
        this.prologueText = prologueText;
    }
    public void setEndBodyText(java.lang.String endBodyText) {
        this.endBodyText = endBodyText;
    }
    protected void prologue() throws java.io.IOException {
        super.prologue();
        java.io.PrintStream out = this.getPrintStream();
        out.println(prologueText);
    }
    protected void emit(edu.umd.cs.findbugs.BugPattern bugPattern) throws java.io.IOException {
        bugPatternSet.add(bugPattern);
    }
    protected void epilogue() throws java.io.IOException {
        this.emitSummaryTable();
        this.emitBugDescriptions();
        super.epilogue();
    }
    protected void header() throws java.io.IOException {
        java.io.PrintStream out = this.getPrintStream();
        out.println(headerText);
    }
/** Extra stuff printed at the beginning of the &lt;body&gt; element. */
    protected void beginBody() throws java.io.IOException {
        java.io.PrintStream out = this.getPrintStream();
        out.println(beginBodyText);
    }
/** Extra stuff printed at the end of the &lt;body&gt; element. */
    protected void endBody() throws java.io.IOException {
        java.io.PrintStream out = this.getPrintStream();
        out.println(endBodyText);
    }
    private void emitSummaryTable() {
        java.io.PrintStream out = this.getPrintStream();
        out.println("<h2>Summary</h2>");
        out.println("<table width=\"100%\">");
        out.println("<tr bgcolor=\"#b9b9fe\"><th>Description</th><th>Category</th></tr>");
        edu.umd.cs.findbugs.tools.html.ColorAlternator colorAlternator = new edu.umd.cs.findbugs.tools.html.ColorAlternator(TABLE_COLORS);
        for (edu.umd.cs.findbugs.BugPattern bugPattern : bugPatternSet){
            out.print("<tr bgcolor=\"" + colorAlternator.nextColor() + "\">");
            out.print("<td><a href=\"#" + bugPattern.getType() + "\">" + bugPattern.getAbbrev() + ": " + bugPattern.getShortDescription() + "</a></td>");
            out.println("<td>" + edu.umd.cs.findbugs.I18N.instance().getBugCategoryDescription(bugPattern.getCategory()) + "</td></tr>");
        }
;
        out.println("</table>");
    }
    private void emitBugDescriptions() {
        java.io.PrintStream out = this.getPrintStream();
        out.println("<h2>Descriptions</h2>");
        for (edu.umd.cs.findbugs.BugPattern bugPattern : bugPatternSet){
            out.println("<h3><a name=\"" + bugPattern.getType() + "\">" + bugPattern.getAbbrev() + ": " + bugPattern.getShortDescription() + " (" + bugPattern.getType() + ")" + "</a></h3>");
            out.println(bugPattern.getDetailText());
        }
;
    }
    protected boolean isEnabled(edu.umd.cs.findbugs.DetectorFactory factory) {
        return unabridged || super.isEnabled(factory);
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        int argCount = 0;
        boolean unabridged = false;
        if (argCount < args.length && args[argCount].equals("-unabridged")) {
            ++argCount;
            unabridged = true;
        }
        if (java.lang.Boolean.getBoolean("findbugs.bugdesc.unabridged")) {
            unabridged = true;
        }
        java.lang.String docTitle = "FindBugs Bug Descriptions";
        if (argCount < args.length) {
            docTitle = args[argCount++];
        }
        edu.umd.cs.findbugs.tools.html.PrettyPrintBugDescriptions pp = new edu.umd.cs.findbugs.tools.html.PrettyPrintBugDescriptions(docTitle, java.lang.System.out);
        if (argCount < args.length) {
            pp.setHeaderText(args[argCount++]);
        }
        if (argCount < args.length) {
            pp.setBeginBodyText(args[argCount++]);
        }
        if (argCount < args.length) {
            pp.setPrologueText(args[argCount++]);
        }
        if (argCount < args.length) {
            pp.setEndBodyText(args[argCount++]);
        }
        if (unabridged) pp.unabridged = true;
        pp.print();
    }
}
// vim:ts=3
