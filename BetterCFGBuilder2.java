package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * A CFGBuilder that really tries to construct accurate control flow graphs. The
 * CFGs it creates have accurate exception edges, and have accurately inlined
 * JSR subroutines.
 * 
 * @author David Hovemeyer
 * @see CFG
 */
/*
     * ----------------------------------------------------------------------
     * Helper classes
     * ----------------------------------------------------------------------
     */
/**
     * A work list item for creating the CFG for a subroutine.
     */
/**
         * Constructor.
         * 
         * @param start
         *            first instruction in the basic block
         * @param basicBlock
         *            the basic block to build
         */
/**
         * Get the start instruction.
         */
/**
         * Get the basic block.
         */
/**
     * A placeholder for a control edge that escapes its subroutine to return
     * control back to an outer (calling) subroutine. It will turn into a real
     * edge during inlining.
     */
/**
         * Constructor.
         * 
         * @param target
         *            the target instruction in a calling subroutine
         * @param edgeType
         *            the type of edge that should be created when the
         *            subroutine is inlined into its calling context
         */
/**
         * Get the target instruction.
         */
/**
         * Get the edge type.
         */
/**
     * JSR subroutine. The top level subroutine is where execution starts. Each
     * subroutine has its own CFG. Eventually, all JSR subroutines will be
     * inlined into the top level subroutine, resulting in an accurate CFG for
     * the overall method.
     */
/**
         * Constructor.
         * 
         * @param start
         *            the start instruction for the subroutine
         */
/**
         * Get the start instruction.
         */
/**
         * Allocate a new basic block in the subroutine.
         */
/**
         * Add a work list item for a basic block to be constructed.
         */
/**
         * Are there more work list items?
         */
/**
         * Get the next work list item.
         */
/**
         * Get the entry block for the subroutine's CFG.
         */
/**
         * Get the exit block for the subroutine's CFG.
         */
/**
         * Get the start block for the subroutine's CFG. (I.e., the block
         * containing the start instruction.)
         */
/**
         * Get the subroutine's CFG.
         */
/**
         * Add an instruction to the subroutine. We keep track of which
         * instructions are part of which subroutines. No instruction may be
         * part of more than one subroutine.
         * 
         * @param handle
         *            the instruction to be added to the subroutine
         */
/**
         * Is the given instruction part of this subroutine?
         */
/**
         * Get the basic block in the subroutine for the given instruction. If
         * the block doesn't exist yet, it is created, and a work list item is
         * added which will populate it. Note that if start is an exception
         * thrower, the block returned will be its ETB.
         * 
         * @param start
         *            the start instruction for the block
         * @return the basic block for the instruction
         */
// Block is an exception handler?
/**
         * Indicate that the method returns at the end of the given block.
         * 
         * @param block
         *            the returning block
         */
/**
         * Does the method return at the end of this block?
         */
/**
         * Indicate that System.exit() is called at the end of the given block.
         * 
         * @param block
         *            the exiting block
         */
/**
         * Is System.exit() called at the end of this block?
         */
/**
         * Indicate that an unhandled exception may be thrown by the given
         * block.
         * 
         * @param block
         *            the block throwing an unhandled exception
         */
/**
         * Does this block throw an unhandled exception?
         */
/**
         * Add a control flow edge to the subroutine. If the control target has
         * not yet been added to the subroutine, a new work list item is added.
         * If the control target is in another subroutine, an EscapeTarget is
         * added.
         * 
         * @param sourceBlock
         *            the source basic block
         * @param target
         *            the control target
         * @param edgeType
         *            the type of control edge
         */
// Control escapes this subroutine
// Edge within the current subroutine
/**
         * Add an edge to the subroutine's CFG.
         * 
         * @param sourceBlock
         *            the source basic block
         * @param destBlock
         *            the destination basic block
         * @param edgeType
         *            the type of edge
         */
/**
         * Get an Iterator over the EscapeTargets of given basic block.
         * 
         * @param sourceBlock
         *            the basic block
         * @return an Iterator over the EscapeTargets
         */
/**
     * Inlining context. This essentially consists of a inlining site and a
     * subroutine to be inlined. A stack of calling contexts is maintained in
     * order to resolve EscapeTargets.
     */
/**
         * Constructor.
         * 
         * @param caller
         *            the calling context
         * @param subroutine
         *            the subroutine being inlined
         * @param result
         *            the result CFG
         */
/**
         * Get the calling context.
         */
/**
         * Get the subroutine being inlined.
         */
/**
         * Get the result CFG.
         */
/**
         * Add a basic block to the inlining work list.
         */
/**
         * Are there more work list items?
         */
/**
         * Get the next work list item (basic block to be inlined).
         */
/**
         * Map a basic block in a subroutine to the corresponding block in the
         * resulting CFG.
         * 
         * @param subBlock
         *            the subroutine block
         * @param resultBlock
         *            the result CFG block
         */
/**
         * Get the block in the result CFG corresponding to the given subroutine
         * block.
         * 
         * @param subBlock
         *            the subroutine block
         * @return the result CFG block
         */
/**
         * Check to ensure that this context is not the result of recursion.
         */
/*
     * ----------------------------------------------------------------------
     * Instance data
     * ----------------------------------------------------------------------
     */
/*
     * ----------------------------------------------------------------------
     * Public methods
     * ----------------------------------------------------------------------
     */
/**
     * Constructor.
     * 
     * @param methodGen
     *            the method to build a CFG for
     */
// ICONST
// GOTO
// ICONST
// System.out.println("Found NULL2Z instruction");
// System.out.println("Found NONNULL2Z instruction");
// need to update
// Build top level subroutine and all JSR subroutines
// Inline everything into the top level subroutine
// Add a NOP instruction to the entry block.
// This allows analyses to construct a Location
// representing the entry to the method.
/*
     * ----------------------------------------------------------------------
     * Implementation
     * ----------------------------------------------------------------------
     */
/**
     * Build a subroutine. We iteratively add basic blocks to the subroutine
     * until there are no more blocks reachable from the calling context. As JSR
     * instructions are encountered, new Subroutines are added to the subroutine
     * work list.
     * 
     * @param subroutine
     *            the subroutine
     */
// Prime the work list
// Keep going until all basic blocks in the subroutine have been added
// Add exception handler block (ETB) for exception-throwing
// instructions
// Add instructions until we get to the end of the block
// Add the instruction to the block
// TODO: should check instruction to ensure that in a JSR
// subroutine
// no assignments are made to the local containing the return
// address.
// if (ins instanceof ASTORE) ...
// Find JSR subroutine, add it to subroutine work list if
// we haven't built a CFG for it yet
// This ends the basic block.
// Add a JSR_EDGE to the successor.
// It will be replaced later by the inlined JSR subroutine.
// End of JSR subroutine
// Add control edges as appropriate
// Is the next instruction a control merge or a PEI?
// Basic block continues
/**
     * Add exception edges for given instruction.
     * 
     * @param subroutine
     *            the subroutine containing the instruction
     * @param pei
     *            the instruction which throws an exception
     * @param etb
     *            the exception thrower block (ETB) for the instruction
     */
// Remember whether or not a universal exception handler
// is reachable. If so, then we know that exceptions raised
// at this instruction cannot propagate out of the method.
// If required, mark this block as throwing an unhandled exception.
// For now, we assume that if there is no reachable handler that handles
// ANY exception type, then the exception can be thrown out of the
// method.
/**
     * Return whether or not the given instruction can throw exceptions.
     * 
     * @param handle
     *            the instruction
     * @return true if the instruction can throw an exception, false otherwise
     */
// if (ins instanceof ATHROW) return false;
/**
     * Determine whether or not the given instruction is a control flow merge.
     * 
     * @param handle
     *            the instruction
     * @return true if the instruction is a control merge, false otherwise
     */
// Check all targeters of this handle to see if any
// of them are branches. If so, the instruction is a merge.
/**
     * Inline all JSR subroutines into the top-level subroutine. This produces a
     * complete CFG for the entire method, in which all JSR subroutines are
     * inlined.
     * 
     * @return the CFG for the method
     */
/**
     * Inline a subroutine into a calling context.
     * 
     * @param context
     *            the Context
     */
// Check to ensure we're not trying to inline something that is
// recursive
// Mark blocks which are in JSR subroutines
// Copy instructions into the result block
// Set exception thrower status
// Set exception handler status
// Add control edges (including inlining JSR subroutines)
// Inline a JSR subroutine...
// Create a new Context
// The start block in the JSR subroutine maps to the first
// inlined block in the result CFG.
// The exit block in the JSR subroutine maps to the result
// block
// corresponding to the instruction following the JSR.
// (I.e., that is where control returns after the execution
// of
// the JSR subroutine.)
// Inline the JSR subroutine
// Ordinary control edge
// Add control edges for escape targets
// Look for the calling context which has the target instruction
// Find result block in caller
// Add an edge to caller context
// If the block returns from the method, add a return edge
// If the block calls System.exit(), add an exit edge
// If the block throws an unhandled exception, add an unhandled
// exception edge
/*
         * while (blocks are left) {
         * 
         * get block from subroutine get corresponding block from result copy
         * instructions into result block
         * 
         * if (block terminated by JSR) { get JSR subroutine create new context
         * create GOTO edge from current result block to start block of new
         * inlined context map subroutine exit block to result JSR successor
         * block inline (new context, result) } else { for each outgoing edge {
         * map each target to result blocks (add block to to work list if
         * needed) add edges to result }
         * 
         * for each outgoing escape target { add edges into blocks in outer
         * contexts (adding those blocks to outer work list if needed) }
         * 
         * if (block returns) { add return edge from result block to result CFG
         * exit block }
         * 
         * if (block calls System.exit()) { add exit edge from result block to
         * result CFG exit block }
         * 
         * if (block throws unhandled exception) { add unhandled exception edge
         * from result block to result CFG exit block } }
         * 
         * }
         */
/**
     * Test driver.
     */
// vim:ts=4
abstract interface package-info {
}
