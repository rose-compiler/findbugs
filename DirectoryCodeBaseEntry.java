/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Codebase entry class for directory codebases.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.impl;
import edu.umd.cs.findbugs.classfile.impl.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.InvalidClassFileFormatException;
public class DirectoryCodeBaseEntry extends edu.umd.cs.findbugs.classfile.impl.AbstractScannableCodeBaseEntry {
    final private edu.umd.cs.findbugs.classfile.impl.DirectoryCodeBase codeBase;
    final private java.lang.String realResourceName;
    public DirectoryCodeBaseEntry(edu.umd.cs.findbugs.classfile.impl.DirectoryCodeBase codeBase, java.lang.String realResourceName) {
        super();
        this.codeBase = codeBase;
        this.realResourceName = realResourceName;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseEntry#getNumBytes()
     */
    public int getNumBytes() {
        java.io.File fullPath = codeBase.getFullPathOfResource(realResourceName);
// this is not needed but causes slowdown on a slow file system IO
// file.length() returns zero if not found, and matches the contract of
// this method
// if (!fullPath.exists()) {
// return -1;
// }
        return (int) (fullPath.length()) ;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseEntry#openResource()
     */
    public java.io.InputStream openResource() throws java.io.IOException {
        return codeBase.openFile(realResourceName);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.impl.AbstractScannableCodeBaseEntry#getCodeBase
     * ()
     */
    public edu.umd.cs.findbugs.classfile.impl.AbstractScannableCodeBase getCodeBase() {
        return codeBase;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.impl.AbstractScannableCodeBaseEntry#
     * getRealResourceName()
     */
    public java.lang.String getRealResourceName() {
        return realResourceName;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseEntry#getClassDescriptor()
     */
    public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() throws edu.umd.cs.findbugs.classfile.InvalidClassFileFormatException {
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptorFromResourceName(this.getResourceName());
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(java.lang.Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        edu.umd.cs.findbugs.classfile.impl.DirectoryCodeBaseEntry other = (edu.umd.cs.findbugs.classfile.impl.DirectoryCodeBaseEntry) (obj) ;
        return this.codeBase.equals(other.codeBase) && this.realResourceName.equals(other.realResourceName);
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return 7919 * codeBase.hashCode() + realResourceName.hashCode();
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public java.lang.String toString() {
        return this.getCodeBase() + ":" + this.getResourceName();
    }
}
