/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Fixedup of from org.apache.bcel.classfile.Visitor
 * 
 * @author <A HREF="http://www.cs.umd.edu/~pugh">William Pugh</A>
 * @version 980818
 */
package edu.umd.cs.findbugs.visitclass;
import edu.umd.cs.findbugs.visitclass.*;
import java.io.PrintStream;
import org.apache.bcel.classfile.Attribute;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.CodeException;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantCP;
import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantDouble;
import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantFloat;
import org.apache.bcel.classfile.ConstantInteger;
import org.apache.bcel.classfile.ConstantInterfaceMethodref;
import org.apache.bcel.classfile.ConstantLong;
import org.apache.bcel.classfile.ConstantMethodref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantString;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.ConstantValue;
import org.apache.bcel.classfile.ExceptionTable;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.InnerClass;
import org.apache.bcel.classfile.InnerClasses;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LineNumber;
import org.apache.bcel.classfile.LineNumberTable;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.LocalVariableTable;
import org.apache.bcel.classfile.LocalVariableTypeTable;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.classfile.Signature;
import org.apache.bcel.classfile.SourceFile;
import org.apache.bcel.classfile.StackMap;
import org.apache.bcel.classfile.StackMapEntry;
import org.apache.bcel.classfile.Synthetic;
import org.apache.bcel.classfile.Unknown;
import org.apache.bcel.classfile.Visitor;
abstract public class BetterVisitor extends java.lang.Object implements org.apache.bcel.classfile.Visitor {
    public BetterVisitor() {
    }
/**
     * clone() is overridden to change access control from protected to public.
     * Originally we compelled subclasses not to throw
     * CloneNotSupportedException, but that was unfriendly to some third-parties
     * with existing code.
     */
    public java.lang.Object clone() throws java.lang.CloneNotSupportedException {
        return super.clone();
    }
// //////////////// In short form //////////////////////
// General classes
    public void visit(org.apache.bcel.classfile.JavaClass obj) {
    }
    public void visit(org.apache.bcel.classfile.ConstantPool obj) {
    }
    public void visit(org.apache.bcel.classfile.Field obj) {
    }
    public void visit(org.apache.bcel.classfile.Method obj) {
    }
// Constants
    public void visit(org.apache.bcel.classfile.Constant obj) {
    }
    public void visit(org.apache.bcel.classfile.ConstantCP obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantMethodref obj) {
        this.visit((org.apache.bcel.classfile.ConstantCP) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantFieldref obj) {
        this.visit((org.apache.bcel.classfile.ConstantCP) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantInterfaceMethodref obj) {
        this.visit((org.apache.bcel.classfile.ConstantCP) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantClass obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantDouble obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantFloat obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantInteger obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantLong obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantNameAndType obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantString obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantUtf8 obj) {
        this.visit((org.apache.bcel.classfile.Constant) (obj) );
    }
// Attributes
    public void visit(org.apache.bcel.classfile.Attribute obj) {
    }
    public void visit(org.apache.bcel.classfile.Code obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ConstantValue obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.ExceptionTable obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.InnerClasses obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.LineNumberTable obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.LocalVariableTable obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.LocalVariableTypeTable obj) {
    }
// must explicitly override to get functionality
    public void visit(org.apache.bcel.classfile.SourceFile obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.Synthetic obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.Deprecated obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.Unknown obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
    public void visit(org.apache.bcel.classfile.Signature obj) {
        this.visit((org.apache.bcel.classfile.Attribute) (obj) );
    }
// Extra classes (i.e. leaves in this context)
    public void visit(org.apache.bcel.classfile.InnerClass obj) {
    }
    public void visit(org.apache.bcel.classfile.LocalVariable obj) {
    }
    public void visit(org.apache.bcel.classfile.LineNumber obj) {
    }
    public void visit(org.apache.bcel.classfile.CodeException obj) {
    }
    public void visit(org.apache.bcel.classfile.StackMapEntry obj) {
    }
// Attributes
    public void visitCode(org.apache.bcel.classfile.Code obj) {
        this.visit(obj);
    }
    public void visitCodeException(org.apache.bcel.classfile.CodeException obj) {
        this.visit(obj);
    }
// Constants
    public void visitConstantClass(org.apache.bcel.classfile.ConstantClass obj) {
        this.visit(obj);
    }
    public void visitConstantDouble(org.apache.bcel.classfile.ConstantDouble obj) {
        this.visit(obj);
    }
    public void visitConstantFieldref(org.apache.bcel.classfile.ConstantFieldref obj) {
        this.visit(obj);
    }
    public void visitConstantFloat(org.apache.bcel.classfile.ConstantFloat obj) {
        this.visit(obj);
    }
    public void visitConstantInteger(org.apache.bcel.classfile.ConstantInteger obj) {
        this.visit(obj);
    }
    public void visitConstantInterfaceMethodref(org.apache.bcel.classfile.ConstantInterfaceMethodref obj) {
        this.visit(obj);
    }
    public void visitConstantLong(org.apache.bcel.classfile.ConstantLong obj) {
        this.visit(obj);
    }
    public void visitConstantMethodref(org.apache.bcel.classfile.ConstantMethodref obj) {
        this.visit(obj);
    }
    public void visitConstantNameAndType(org.apache.bcel.classfile.ConstantNameAndType obj) {
        this.visit(obj);
    }
    public void visitConstantPool(org.apache.bcel.classfile.ConstantPool obj) {
        this.visit(obj);
    }
    public void visitConstantString(org.apache.bcel.classfile.ConstantString obj) {
        this.visit(obj);
    }
    public void visitConstantUtf8(org.apache.bcel.classfile.ConstantUtf8 obj) {
        this.visit(obj);
    }
    public void visitConstantValue(org.apache.bcel.classfile.ConstantValue obj) {
        this.visit(obj);
    }
    public void visitDeprecated(org.apache.bcel.classfile.Deprecated obj) {
        this.visit(obj);
    }
    public void visitExceptionTable(org.apache.bcel.classfile.ExceptionTable obj) {
        this.visit(obj);
    }
    public void visitField(org.apache.bcel.classfile.Field obj) {
        this.visit(obj);
    }
// Extra classes (i.e. leaves in this context)
    public void visitInnerClass(org.apache.bcel.classfile.InnerClass obj) {
        this.visit(obj);
    }
    public void visitInnerClasses(org.apache.bcel.classfile.InnerClasses obj) {
        this.visit(obj);
    }
// General classes
    public void visitJavaClass(org.apache.bcel.classfile.JavaClass obj) {
        this.visit(obj);
    }
    public void visitLineNumber(org.apache.bcel.classfile.LineNumber obj) {
        this.visit(obj);
    }
    public void visitLineNumberTable(org.apache.bcel.classfile.LineNumberTable obj) {
        this.visit(obj);
    }
    public void visitLocalVariable(org.apache.bcel.classfile.LocalVariable obj) {
        this.visit(obj);
    }
    public void visitLocalVariableTable(org.apache.bcel.classfile.LocalVariableTable obj) {
        this.visit(obj);
    }
    public void visitLocalVariableTypeTable(org.apache.bcel.classfile.LocalVariableTypeTable obj) {
        this.visit(obj);
    }
    public void visitMethod(org.apache.bcel.classfile.Method obj) {
        this.visit(obj);
    }
    public void visitSignature(org.apache.bcel.classfile.Signature obj) {
        this.visit(obj);
    }
    public void visitSourceFile(org.apache.bcel.classfile.SourceFile obj) {
        this.visit(obj);
    }
    public void visitSynthetic(org.apache.bcel.classfile.Synthetic obj) {
        this.visit(obj);
    }
    public void visitUnknown(org.apache.bcel.classfile.Unknown obj) {
        this.visit(obj);
    }
    public void visitStackMapEntry(org.apache.bcel.classfile.StackMapEntry obj) {
        this.visit(obj);
    }
    public void visitStackMap(org.apache.bcel.classfile.StackMap obj) {
        this.visit(obj);
    }
    public void report(java.io.PrintStream out) {
    }
}
