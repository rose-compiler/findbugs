/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import javax.annotation.CheckForNull;
import edu.umd.cs.findbugs.util.Util;
public class MemberMatcher extends java.lang.Object {
    final protected edu.umd.cs.findbugs.filter.NameMatch name;
    final protected java.lang.String role;
    final protected edu.umd.cs.findbugs.filter.NameMatch signature;
    public MemberMatcher(java.lang.String name) {
        this(name,null,null);
    }
    public MemberMatcher(java.lang.String name, java.lang.String signature) {
        this(name,signature,null);
    }
    public MemberMatcher(java.lang.String name, java.lang.String signature, java.lang.String role) {
        super();
        if (name == null) {
            if (signature == null) throw new edu.umd.cs.findbugs.filter.FilterException(this.getClass().getName() + " must have eiter name or signature attributes");
            else name = "~.*";
        }
        this.name = new edu.umd.cs.findbugs.filter.NameMatch(name);
        this.signature = new edu.umd.cs.findbugs.filter.NameMatch(signature);
        this.role = role;
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        if ( !name.isUniversal()) {
            buf.append("name=\"");
            buf.append(name.getSpec());
            buf.append("\"");
        }
        if (signature != null) {
            if (buf.length() > 0) buf.append(" ");
            buf.append("signature=\"");
            buf.append(signature);
            buf.append("\"");
        }
        return buf.toString();
    }
    public int hashCode() {
        return name.hashCode() + edu.umd.cs.findbugs.util.Util.nullSafeHashcode(signature);
    }
    public boolean equals(java.lang.Object o) {
        if (o == null || this.getClass() != o.getClass()) return false;
        edu.umd.cs.findbugs.filter.MemberMatcher other = (edu.umd.cs.findbugs.filter.MemberMatcher) (o) ;
        return name.equals(other.name) && edu.umd.cs.findbugs.util.Util.nullSafeEquals(signature,other.signature);
    }
}
