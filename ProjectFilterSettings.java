/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Settings for user filtering of warnings for a project. This includes
 * selecting particular bug categories to view, as well as a minimum warning
 * priority. Includes support for encoding these settings as a String, which can
 * easily be stored as a persistent project property in Eclipse.
 *
 * @see BugInstance
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.config;
import edu.umd.cs.findbugs.config.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugPattern;
import edu.umd.cs.findbugs.BugProperty;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.Priorities;
public class ProjectFilterSettings extends java.lang.Object implements java.lang.Cloneable {
/** Text string for high priority. */
    final public static java.lang.String HIGH_PRIORITY = "High";
/** Text string for medium priority. */
    final public static java.lang.String MEDIUM_PRIORITY = "Medium";
/** Text string for low priority. */
    final public static java.lang.String LOW_PRIORITY = "Low";
/** Text string for experimental priority. */
    final public static java.lang.String EXPERIMENTAL_PRIORITY = "Experimental";
/** Default warning threshold priority. */
    final public static java.lang.String DEFAULT_PRIORITY = MEDIUM_PRIORITY;
/** Map of priority level names to their numeric values. */
    private static java.util.Map<java.lang.String, java.lang.Integer> priorityNameToValueMap = new java.util.HashMap<java.lang.String, java.lang.Integer>();
    static {
        priorityNameToValueMap.put(HIGH_PRIORITY,(edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY));
        priorityNameToValueMap.put(MEDIUM_PRIORITY,(edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY));
        priorityNameToValueMap.put(LOW_PRIORITY,(edu.umd.cs.findbugs.Priorities.LOW_PRIORITY));
        priorityNameToValueMap.put(EXPERIMENTAL_PRIORITY,(edu.umd.cs.findbugs.Priorities.EXP_PRIORITY));
    }
/**
     * The character used for delimiting whole fields in filter settings encoded
     * as strings
     */
    private static java.lang.String FIELD_DELIMITER = "|";
/**
     * The character used for delimiting list items in filter settings encoded
     * as strings
     */
    private static java.lang.String LISTITEM_DELIMITER = ",";
    private static int DEFAULT_MIN_RANK = 15;
// Fields
// not used for much:
    private java.util.Set<java.lang.String> activeBugCategorySet;
// hiddenBugCategorySet has
// priority.
    private java.util.Set<java.lang.String> hiddenBugCategorySet;
    private java.lang.String minPriority;
    private int minPriorityAsInt;
    private int minRank = DEFAULT_MIN_RANK;
    private boolean displayFalseWarnings;
/**
     * Constructor. This is not meant to be called directly; use one of the
     * factory methods instead.
     */
    public ProjectFilterSettings() {
        super();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        this.activeBugCategorySet = new java.util.TreeSet<java.lang.String>(edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getBugCategories());
        this.hiddenBugCategorySet = new java.util.HashSet<java.lang.String>();
        activeBugCategorySet.remove("NOISE");
        hiddenBugCategorySet.add("NOISE");
        this.setMinRank(DEFAULT_MIN_RANK);
        this.setMinPriority(DEFAULT_PRIORITY);
        this.displayFalseWarnings = false;
    }
// ensure detectors loaded
// initially all known bug categories are active
// using SortedSet to allow better revision control on saved sorted
// properties
    public void setMinRank(int minRank) {
        this.minRank = minRank;
    }
    public int getMinRank() {
        return minRank;
    }
/**
     * Factory method to create a default ProjectFilterSettings object. Uses the
     * default warning priority threshold, and enables all bug categories.
     *
     * @return a default ProjectFilterSettings object
     */
    public static edu.umd.cs.findbugs.config.ProjectFilterSettings createDefault() {
        edu.umd.cs.findbugs.config.ProjectFilterSettings result = new edu.umd.cs.findbugs.config.ProjectFilterSettings();
        result.setMinPriority(DEFAULT_PRIORITY);
// Set default priority threshold
        return result;
    }
/**
     * Create ProjectFilterSettings from an encoded string.
     *
     * @param s
     *            the encoded string
     * @return the ProjectFilterSettings
     */
    public static edu.umd.cs.findbugs.config.ProjectFilterSettings fromEncodedString(java.lang.String s) {
        edu.umd.cs.findbugs.config.ProjectFilterSettings result = new edu.umd.cs.findbugs.config.ProjectFilterSettings();
        if (s.length() > 0) {
            int bar = s.indexOf(FIELD_DELIMITER);
            java.lang.String minPriority;
            if (bar >= 0) {
                minPriority = s.substring(0,bar);
                s = s.substring(bar + 1);
            }
            else {
                minPriority = s;
                s = "";
            }
            if (priorityNameToValueMap.get(minPriority) == null) {
                minPriority = DEFAULT_PRIORITY;
            }
            result.setMinPriority(minPriority);
        }
        if (s.length() > 0) {
            int bar = s.indexOf(FIELD_DELIMITER);
            java.lang.String categories;
            if (bar >= 0) {
                categories = s.substring(0,bar);
                s = s.substring(bar + 1);
            }
            else {
                categories = s;
                s = "";
            }
            java.util.StringTokenizer t = new java.util.StringTokenizer(categories, LISTITEM_DELIMITER);
            while (t.hasMoreTokens()) {
                java.lang.String category = t.nextToken();
                result.addCategory(category);
            }
        }
        if (s.length() > 0) {
            int bar = s.indexOf(FIELD_DELIMITER);
            java.lang.String displayFalseWarnings;
            if (bar >= 0) {
                displayFalseWarnings = s.substring(0,bar);
                s = s.substring(bar + 1);
            }
            else {
                displayFalseWarnings = s;
                s = "";
            }
            result.setDisplayFalseWarnings(java.lang.Boolean.valueOf(displayFalseWarnings).booleanValue());
        }
        if (s.length() > 0) {
            int bar = s.indexOf(FIELD_DELIMITER);
            java.lang.String minRankStr;
            if (bar >= 0) {
                minRankStr = s.substring(0,bar);
                s = s.substring(bar + 1);
            }
            else {
                minRankStr = s;
                s = "";
            }
            result.setMinRank(java.lang.Integer.parseInt(minRankStr));
        }
        if (s.length() > 0) {
// Can add other fields here...
            assert true;
        }
        return result;
    }
/**
     * set the hidden bug categories on the specifed ProjectFilterSettings from
     * an encoded string
     *
     * @param result
     *            the ProjectFilterSettings from which to remove bug categories
     * @param s
     *            the encoded string
     * @see ProjectFilterSettings#hiddenFromEncodedString(ProjectFilterSettings,
     *      String)
     */
    public static void hiddenFromEncodedString(edu.umd.cs.findbugs.config.ProjectFilterSettings result, java.lang.String s) {
        if (s.length() > 0) {
            int bar = s.indexOf(FIELD_DELIMITER);
            java.lang.String categories;
            if (bar >= 0) {
                categories = s.substring(0,bar);
            }
            else {
                categories = s;
            }
            java.util.StringTokenizer t = new java.util.StringTokenizer(categories, LISTITEM_DELIMITER);
            while (t.hasMoreTokens()) {
                java.lang.String category = t.nextToken();
                result.removeCategory(category);
            }
        }
    }
/**
     * Return whether or not a warning should be displayed, according to the
     * project filter settings.
     *
     * @param bugInstance
     *            the warning
     * @return true if the warning should be displayed, false if not
     */
    public boolean displayWarning(edu.umd.cs.findbugs.BugInstance bugInstance) {
        int priority = bugInstance.getPriority();
        if (priority > this.getMinPriorityAsInt()) {
            return false;
        }
        int rank = bugInstance.getBugRank();
        if (rank > this.getMinRank()) {
            return false;
        }
        edu.umd.cs.findbugs.BugPattern bugPattern = bugInstance.getBugPattern();
// HACK: it is conceivable that the detector plugin which generated
// this warning is not available any more, in which case we can't
// find out the category. Let the warning be visible in this case.
        if (bugPattern != null &&  !this.containsCategory(bugPattern.getCategory())) {
            return false;
        }
        if ( !displayFalseWarnings) {
            boolean isFalseWarning =  !java.lang.Boolean.valueOf(bugInstance.getProperty(edu.umd.cs.findbugs.BugProperty.IS_BUG,"true")).booleanValue();
            if (isFalseWarning) {
                return false;
            }
        }
        return true;
    }
/**
     * Set minimum warning priority threshold.
     *
     * @param minPriority
     *            the priority threshold: one of "High", "Medium", or "Low"
     */
    public void setMinPriority(java.lang.String minPriority) {
        this.minPriority = minPriority;
        java.lang.Integer value = priorityNameToValueMap.get(minPriority);
        if (value == null) {
            value = priorityNameToValueMap.get(DEFAULT_PRIORITY);
            if (value == null) {
                throw new java.lang.IllegalStateException();
            }
        }
        this.minPriorityAsInt = value.intValue();
    }
/**
     * Get the minimum warning priority threshold.
     *
     * @return minimum warning priority threshold: one of "High", "Medium", or
     *         "Low"
     */
    public java.lang.String getMinPriority() {
        return this.minPriority;
    }
/**
     * Return the minimum warning priority threshold as an integer.
     *
     * @return the minimum warning priority threshold as an integer
     */
    public int getMinPriorityAsInt() {
        return minPriorityAsInt;
    }
/**
     * Add a bug category to the set of categories to be displayed.
     *
     * @param category
     *            the bug category: e.g., "CORRECTNESS"
     */
    public void addCategory(java.lang.String category) {
        this.hiddenBugCategorySet.remove(category);
        this.activeBugCategorySet.add(category);
    }
/**
     * Remove a bug category from the set of categories to be displayed.
     *
     * @param category
     *            the bug category: e.g., "CORRECTNESS"
     */
    public void removeCategory(java.lang.String category) {
        this.hiddenBugCategorySet.add(category);
        this.activeBugCategorySet.remove(category);
    }
/**
     * Clear all bug categories from the hidden list. So the effect is to enable
     * all bug categories.
     */
    public void clearAllCategories() {
        this.activeBugCategorySet.addAll(hiddenBugCategorySet);
        this.hiddenBugCategorySet.clear();
    }
/**
     * Returns false if the given category is hidden in the project filter
     * settings.
     *
     * @param category
     *            the category
     * @return false if the category is hidden, true if not
     */
    public boolean containsCategory(java.lang.String category) {
// do _not_ consult the activeBugCategorySet: if not hidden return true.
        return  !hiddenBugCategorySet.contains(category);
    }
/**
     * Return set of active (enabled) bug categories.
     *
     * Note that bug categories that are not explicity hidden will appear active
     * even if they are not members of this set.
     *
     * @return the set of active categories
     */
    public java.util.Set<java.lang.String> getActiveCategorySet() {
        java.util.Set<java.lang.String> result = new java.util.TreeSet<java.lang.String>();
        result.addAll(this.activeBugCategorySet);
        return result;
    }
/**
     * Set whether or not false warnings should be displayed.
     *
     * @param displayFalseWarnings
     *            true if false warnings should be displayed, false if not
     */
    public void setDisplayFalseWarnings(boolean displayFalseWarnings) {
        this.displayFalseWarnings = displayFalseWarnings;
    }
/**
     * Get whether or not false warnings should be displayed.
     *
     * @return true if false warnings should be displayed, false if not
     */
    public boolean displayFalseWarnings() {
        return displayFalseWarnings;
    }
/**
     * Create a string containing the encoded form of the hidden bug categories
     *
     * @return an encoded string
     */
    public java.lang.String hiddenToEncodedString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        for (java.util.Iterator<java.lang.String> i = hiddenBugCategorySet.iterator(); i.hasNext(); ) {
            buf.append(i.next());
            if (i.hasNext()) {
                buf.append(LISTITEM_DELIMITER);
            }
        }
        buf.append(FIELD_DELIMITER);
// Encode hidden bug categories
        return buf.toString();
    }
/**
     * Create a string containing the encoded form of the ProjectFilterSettings.
     *
     * @return an encoded string
     */
    public java.lang.String toEncodedString() {
// Priority threshold
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append(this.getMinPriority());
        buf.append(FIELD_DELIMITER);
        for (java.util.Iterator<java.lang.String> i = activeBugCategorySet.iterator(); i.hasNext(); ) {
            buf.append(i.next());
            if (i.hasNext()) {
                buf.append(LISTITEM_DELIMITER);
            }
        }
        buf.append(FIELD_DELIMITER);
        buf.append(displayFalseWarnings ? "true" : "false");
        buf.append(FIELD_DELIMITER);
        buf.append(this.getMinRank());
// Encode enabled bug categories. Note that these aren't really used for
// much.
// They only come in to play when parsed by a version of FindBugs older
// than 1.1.
// Whether to display false warnings
        return buf.toString();
    }
    public java.lang.String toString() {
        return this.toEncodedString();
    }
/*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(java.lang.Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        edu.umd.cs.findbugs.config.ProjectFilterSettings other = (edu.umd.cs.findbugs.config.ProjectFilterSettings) (obj) ;
        if ( !this.getMinPriority().equals(other.getMinPriority())) {
            return false;
        }
        if (this.getMinRank() != other.getMinRank()) {
            return false;
        }
// don't compare the activeBugCategorySet. compare the
// hiddenBugCategorySet only
        if ( !this.hiddenBugCategorySet.equals(other.hiddenBugCategorySet)) {
            return false;
        }
        if (this.displayFalseWarnings != other.displayFalseWarnings) {
            return false;
        }
        return true;
    }
/*
     * (non-Javadoc)
     *
     * @see java.lang.Object#clone()
     */
    public java.lang.Object clone() {
        try {
// Create shallow copy
            edu.umd.cs.findbugs.config.ProjectFilterSettings clone = (edu.umd.cs.findbugs.config.ProjectFilterSettings) (super.clone()) ;
            clone.hiddenBugCategorySet = new java.util.HashSet<java.lang.String>();
            clone.hiddenBugCategorySet.addAll(this.hiddenBugCategorySet);
            clone.activeBugCategorySet = new java.util.TreeSet<java.lang.String>();
            clone.activeBugCategorySet.addAll(this.activeBugCategorySet);
            clone.setMinPriority(this.getMinPriority());
            clone.setMinRank(this.getMinRank());
            clone.displayFalseWarnings = this.displayFalseWarnings;
// Copy field contents
            return clone;
        }
        catch (java.lang.CloneNotSupportedException e){
// Should not happen!
            throw new java.lang.AssertionError(e);
        }
    }
/*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return minPriority.hashCode() + minRank + 1009 * hiddenBugCategorySet.hashCode() + (displayFalseWarnings ? 7919 : 0);
    }
/**
     * Convert an integer warning priority threshold value to a String.
     */
    public static java.lang.String getIntPriorityAsString(int prio) {
        java.lang.String minPriority;
        switch(prio){
            case edu.umd.cs.findbugs.Priorities.EXP_PRIORITY:{
                minPriority = edu.umd.cs.findbugs.config.ProjectFilterSettings.EXPERIMENTAL_PRIORITY;
                break;
            }
            case edu.umd.cs.findbugs.Priorities.LOW_PRIORITY:{
                minPriority = edu.umd.cs.findbugs.config.ProjectFilterSettings.LOW_PRIORITY;
                break;
            }
            case edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY:{
                minPriority = edu.umd.cs.findbugs.config.ProjectFilterSettings.MEDIUM_PRIORITY;
                break;
            }
            case edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY:{
                minPriority = edu.umd.cs.findbugs.config.ProjectFilterSettings.HIGH_PRIORITY;
                break;
            }
            default:{
                minPriority = edu.umd.cs.findbugs.config.ProjectFilterSettings.DEFAULT_PRIORITY;
                break;
            }
        }
        return minPriority;
    }
}
// vim:ts=4
