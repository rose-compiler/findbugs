/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Implementation of ICodeBase for a single classfile.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.impl;
import edu.umd.cs.findbugs.classfile.impl.*;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.ICodeBaseEntry;
import edu.umd.cs.findbugs.classfile.ICodeBaseIterator;
import edu.umd.cs.findbugs.classfile.ICodeBaseLocator;
import edu.umd.cs.findbugs.classfile.IScannableCodeBase;
import edu.umd.cs.findbugs.classfile.InvalidClassFileFormatException;
import edu.umd.cs.findbugs.classfile.ResourceNotFoundException;
import edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo;
import edu.umd.cs.findbugs.classfile.engine.ClassParser;
import edu.umd.cs.findbugs.classfile.engine.ClassParserInterface;
import edu.umd.cs.findbugs.io.IO;
public class SingleFileCodeBase extends java.lang.Object implements edu.umd.cs.findbugs.classfile.IScannableCodeBase {
    private edu.umd.cs.findbugs.classfile.ICodeBaseLocator codeBaseLocator;
    private java.lang.String fileName;
    private boolean isAppCodeBase;
    private int howDiscovered;
    private long lastModifiedTime;
    private boolean resourceNameKnown;
    private java.lang.String resourceName;
    public SingleFileCodeBase(edu.umd.cs.findbugs.classfile.ICodeBaseLocator codeBaseLocator, java.lang.String fileName) {
        super();
        this.codeBaseLocator = codeBaseLocator;
        this.fileName = fileName;
        this.lastModifiedTime = new java.io.File(fileName).lastModified();
    }
    public java.lang.String toString() {
        return fileName;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getCodeBaseLocator()
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseLocator getCodeBaseLocator() {
        return codeBaseLocator;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IScannableCodeBase#containsSourceFiles()
     */
    public boolean containsSourceFiles() throws java.lang.InterruptedException {
        return false;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.IScannableCodeBase#iterator()
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseIterator iterator() throws java.lang.InterruptedException {
        return new edu.umd.cs.findbugs.classfile.ICodeBaseIterator() {
            boolean done = false;
            public boolean hasNext() throws java.lang.InterruptedException {
                return  !done;
            }
            public edu.umd.cs.findbugs.classfile.ICodeBaseEntry next() throws java.lang.InterruptedException {
                if (done) {
                    throw new java.util.NoSuchElementException();
                }
                done = true;
                return new edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBaseEntry(edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBase.this);
            }
        };
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.classfile.ICodeBaseIterator#hasNext()
             */
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.classfile.ICodeBaseIterator#next()
             */
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.ICodeBase#lookupResource(java.lang.String)
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseEntry lookupResource(java.lang.String resourceName) {
        if ( !resourceName.equals(this.getResourceName())) {
            return null;
        }
        return new edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBaseEntry(this);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.ICodeBase#setApplicationCodeBase(boolean)
     */
    public void setApplicationCodeBase(boolean isAppCodeBase) {
        this.isAppCodeBase = isAppCodeBase;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#isApplicationCodeBase()
     */
    public boolean isApplicationCodeBase() {
        return isAppCodeBase;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#setHowDiscovered(int)
     */
    public void setHowDiscovered(int howDiscovered) {
        this.howDiscovered = howDiscovered;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getHowDiscovered()
     */
    public int getHowDiscovered() {
        return howDiscovered;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#setLastModifiedTime(long)
     */
    public void setLastModifiedTime(long lastModifiedTime) {
        if (lastModifiedTime > 0 && edu.umd.cs.findbugs.FindBugs.validTimestamp(lastModifiedTime)) {
            this.lastModifiedTime = lastModifiedTime;
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getLastModifiedTime()
     */
    public long getLastModifiedTime() {
        return lastModifiedTime;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getPathName()
     */
    public java.lang.String getPathName() {
        return fileName;
    }
    java.io.InputStream openFile() throws java.io.IOException {
        return new java.io.BufferedInputStream(new java.io.FileInputStream(fileName));
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#close()
     */
    public void close() {
    }
// Nothing to do
/**
     * Get the resource name of the single file. We have to open the file and
     * parse the constant pool in order to find this out.
     * 
     * @return the resource name (e.g., "java/lang/String.class" if the class is
     *         java.lang.String)
     */
    java.lang.String getResourceName() {
        if ( !resourceNameKnown) {
// The resource name of a classfile can only be determined by
// reading
// the file and parsing the constant pool.
// If we can't do this for some reason, then we just
// make the resource name equal to the filename.
            try {
                resourceName = this.getClassDescriptor().toResourceName();
            }
            catch (java.lang.Exception e){
                resourceName = fileName;
            }
            resourceNameKnown = true;
        }
        return resourceName;
    }
    edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() throws edu.umd.cs.findbugs.classfile.InvalidClassFileFormatException, edu.umd.cs.findbugs.classfile.ResourceNotFoundException {
        java.io.DataInputStream in = null;
        try {
            try {
                in = new java.io.DataInputStream(new java.io.BufferedInputStream(new java.io.FileInputStream(fileName)));
                edu.umd.cs.findbugs.classfile.engine.ClassParserInterface classParser = new edu.umd.cs.findbugs.classfile.engine.ClassParser(in, null, new edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBaseEntry(this));
                edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo.Builder builder = new edu.umd.cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo.Builder();
                classParser.parse(builder);
                return builder.build().getClassDescriptor();
            }
            finally {
                if (in != null) {
                    edu.umd.cs.findbugs.io.IO.close(in);
                }
            }
        }
        catch (java.io.IOException e){
// XXX: file name isn't really the resource name, but whatever
            throw new edu.umd.cs.findbugs.classfile.ResourceNotFoundException(fileName);
        }
    }
/**
     * Return the number of bytes in the file.
     * 
     * @return the number of bytes in the file, or -1 if the file's length can't
     *         be determined
     */
    int getNumBytes() {
        java.io.File file = new java.io.File(fileName);
// this is not needed but causes slowdown on a slow file system IO
// file.length() returns zero if not found, and matches the contract of
// this method
// if (!file.exists()) {
// return -1;
// }
        return (int) (file.length()) ;
    }
}
