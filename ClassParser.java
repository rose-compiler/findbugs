package edu.umd.cs.findbugs.classfile.engine;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.engine.*;
/**
 * Parse a class to extract symbolic information. see <a
 * href=http://java.sun.com
 * /docs/books/vmspec/2nd-edition/html/ClassFile.doc.html">
 * http://java.sun.com/docs/books/vmspec/2nd-edition/html/ClassFile.doc.html
 * </a>
 * 
 * @author David Hovemeyer
 */
/**
     * Constructor.
     * 
     * @param in
     *            the DataInputStream to read class data from
     * @param expectedClassDescriptor
     *            ClassDescriptor expected: null if unknown
     * @param codeBaseEntry
     *            codebase entry class is loaded from
     */
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.engine.ClassParserInterface#parse(edu.umd
     * .cs.findbugs.classfile.analysis.ClassNameAndSuperclassInfo.Builder)
     */
// Double and Long constants take up two constant pool
// entries
// Extract all references to other classes,
// both CONSTANT_Class entries and also referenced method
// signatures.
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.engine.ClassParserInterface#parse(edu.umd
     * .cs.findbugs.classfile.analysis.ClassInfo.Builder)
     */
/**
     * Extract references to other classes.
     * 
     * @return array of ClassDescriptors of referenced classes
     * @throws InvalidClassFileFormatException
     */
// Get the target class name
// Parse signature to extract class names
/**
     * @param referencedClassSet
     * @param signature
     */
// 8: UTF-8 string
// I: int
// F: float
// L: long
// D: double
// i: 2-byte constant pool index
// 1:
// CONSTANT_Utf8
// 3: CONSTANT_Integer
// 4: CONSTANT_Float
// 5: CONSTANT_Long
// 6: CONSTANT_Double
// 7: CONSTANT_Class
// 8: CONSTANT_String
// 9: CONSTANT_Fieldref
// 10: CONSTANT_Methodref
// 11: CONSTANT_InterfaceMethodref
// 12: CONSTANT_NameAndType
/**
     * Read a constant from the constant pool.
     * 
     * @return a Constant
     * @throws InvalidClassFileFormatException
     * @throws IOException
     */
/**
     * Get a class name from a CONSTANT_Class. Note that this may be an array
     * (e.g., "[Ljava/lang/String;").
     * 
     * @param index
     *            index of the constant
     * @return the class name
     * @throws InvalidClassFileFormatException
     */
/**
     * Get the ClassDescriptor of a class referenced in the constant pool.
     * 
     * @param index
     *            index of the referenced class in the constant pool
     * @return the ClassDescriptor of the referenced class
     * @throws InvalidClassFileFormatException
     */
/**
     * Get the UTF-8 string constant at given constant pool index.
     * 
     * @param refIndex
     *            the constant pool index
     * @return the String at that index
     * @throws InvalidClassFileFormatException
     */
/**
     * Check that a constant pool index is valid.
     * 
     * @param expectedClassDescriptor
     *            class descriptor
     * @param constantPool
     *            the constant pool
     * @param index
     *            the index to check
     * @throws InvalidClassFileFormatException
     *             if the index is not valid
     */
/**
     * Check that a constant has the expected tag.
     * 
     * @param constant
     *            the constant to check
     * @param expectedTag
     *            the expected constant tag
     * @throws InvalidClassFileFormatException
     *             if the constant's tag does not match the expected tag
     */
/**
     * Read field_info, return FieldDescriptor.
     * 
     * @param thisClassDescriptor
     *            the ClassDescriptor of this class (being parsed)
     * @return the FieldDescriptor
     * @throws IOException
     * @throws InvalidClassFileFormatException
     */
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.classfile.engine.ClassParser.
             * FieldOrMethodDescriptorCreator#create(java.lang.String,
             * java.lang.String, java.lang.String, int)
             */
/**
     * Read method_info, read method descriptor.
     * 
     * @param thisClassDescriptor
     * @return
     * @throws IOException
     * @throws InvalidClassFileFormatException
     */
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.classfile.engine.ClassParser.
             * FieldOrMethodDescriptorCreator#create(java.lang.String,
             * java.lang.String, java.lang.String, int)
             */
/**
     * Read field_info or method_info. They have the same format.
     * 
     * @param <E>
     *            descriptor type to return
     * @param thisClassDescriptor
     *            class descriptor of class being parsed
     * @param creator
     *            callback to create the FieldDescriptor or MethodDescriptor
     * @return the parsed descriptor
     * @throws IOException
     * @throws InvalidClassFileFormatException
     */
/**
     * Read an attribute.
     * 
     * @throws IOException
     * @throws InvalidClassFileFormatException
     */
/**
     * Read an InnerClasses attribute.
     * 
     * @param attribute_length
     *            length of attribute (excluding first 6 bytes)
     * @throws InvalidClassFileFormatException
     * @throws IOException
     */
// Record which class this class is a member of.
/**
     * Get the signature from a CONSTANT_NameAndType.
     * 
     * @param index
     *            the index of the CONSTANT_NameAndType
     * @return the signature
     * @throws InvalidClassFileFormatException
     */
abstract interface package-info {
}
