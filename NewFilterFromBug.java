/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * Allows you to make a new Filter by right clicking (control clicking) on a bug
 * in the tree
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import edu.umd.cs.findbugs.filter.Matcher;
public class NewFilterFromBug extends edu.umd.cs.findbugs.gui2.FBDialog {
    final private static java.util.List<edu.umd.cs.findbugs.gui2.NewFilterFromBug> listOfAllFrames = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.NewFilterFromBug>();
    public NewFilterFromBug(final edu.umd.cs.findbugs.gui2.FilterFromBugPicker filterFromBugPicker, final edu.umd.cs.findbugs.gui2.ApplyNewFilter applyNewFilter) {
        super();
        this.setModal(true);
        listOfAllFrames.add(this);
        this.setLayout(new java.awt.BorderLayout());
        this.add(new javax.swing.JLabel("Filter out all bugs whose..."),java.awt.BorderLayout.NORTH);
        javax.swing.JPanel center = filterFromBugPicker.pickerPanel();
        this.add(center,java.awt.BorderLayout.CENTER);
        javax.swing.JPanel south = new javax.swing.JPanel();
        javax.swing.JButton okButton = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.ok_btn","OK"));
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edu.umd.cs.findbugs.filter.Matcher matcherFromSelection = filterFromBugPicker.makeMatcherFromSelection();
                applyNewFilter.fromMatcher(matcherFromSelection);
                edu.umd.cs.findbugs.gui2.NewFilterFromBug.this.closeDialog();
            }
        });
        javax.swing.JButton cancelButton = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.cancel_btn","Cancel"));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edu.umd.cs.findbugs.gui2.NewFilterFromBug.this.closeDialog();
            }
        });
        edu.umd.cs.findbugs.gui2.GuiUtil.addOkAndCancelButtons(south,okButton,cancelButton);
        this.add(south,java.awt.BorderLayout.SOUTH);
        this.pack();
        this.setVisible(true);
    }
    final private void closeDialog() {
        edu.umd.cs.findbugs.gui2.NewFilterFromBug.this.dispose();
    }
    static void closeAll() {
        for (edu.umd.cs.findbugs.gui2.NewFilterFromBug frame : listOfAllFrames){
            frame.dispose();
        }
;
        listOfAllFrames.clear();
    }
}
