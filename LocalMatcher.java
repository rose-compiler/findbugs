/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import java.io.IOException;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.LocalVariableAnnotation;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class LocalMatcher extends java.lang.Object implements edu.umd.cs.findbugs.filter.Matcher {
    private edu.umd.cs.findbugs.filter.NameMatch name;
    public LocalMatcher(java.lang.String name) {
        super();
        this.name = new edu.umd.cs.findbugs.filter.NameMatch(name);
    }
    public LocalMatcher(java.lang.String name, java.lang.String type) {
        super();
        this.name = new edu.umd.cs.findbugs.filter.NameMatch(name);
    }
    public java.lang.String toString() {
        return "Local(name=" + name + ")";
    }
    public boolean match(edu.umd.cs.findbugs.BugInstance bugInstance) {
        edu.umd.cs.findbugs.LocalVariableAnnotation localAnnotation = bugInstance.getPrimaryLocalVariableAnnotation();
        if (localAnnotation == null) {
            return false;
        }
        if ( !name.match(localAnnotation.getName())) {
            return false;
        }
        return true;
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean disabled) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributes = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("name",name.getSpec());
        if (disabled) attributes.addAttribute("disabled","true");
        xmlOutput.openCloseTag("Local",attributes);
    }
}
