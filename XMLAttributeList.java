/*
 * XML input/output support for FindBugs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Helper class to format attributes in an XML tag.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.xml;
import edu.umd.cs.findbugs.xml.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import edu.umd.cs.findbugs.util.Strings;
public class XMLAttributeList extends java.lang.Object {
    public static class NameValuePair extends java.lang.Object {
        private java.lang.String name;
        private java.lang.String value;
        public NameValuePair(java.lang.String name, java.lang.String value) {
            super();
            this.name = name;
            this.value = value;
        }
        public java.lang.String getName() {
            return name;
        }
        public java.lang.String getValue() {
            return value;
        }
    }
// Fields
    private java.util.List<edu.umd.cs.findbugs.xml.XMLAttributeList.NameValuePair> nameValuePairList;
/**
     * Constructor. Creates an empty object.
     */
    public XMLAttributeList() {
        super();
        this.nameValuePairList = new java.util.LinkedList<edu.umd.cs.findbugs.xml.XMLAttributeList.NameValuePair>();
    }
/**
     * Add a single attribute name and value.
     * 
     * @param name
     *            the attribute name
     * @param value
     *            the attribute value
     * @return this object (so calls to addAttribute() can be chained)
     */
    public edu.umd.cs.findbugs.xml.XMLAttributeList addAttribute(java.lang.String name, java.lang.String value) {
        if (name == null) throw new java.lang.NullPointerException("name must be nonnull");
        if (value == null) throw new java.lang.NullPointerException("value must be nonnull");
        nameValuePairList.add(new edu.umd.cs.findbugs.xml.XMLAttributeList.NameValuePair(name, value));
        return this;
    }
/**
     * Add a single attribute name and value.
     * 
     * @param name
     *            the attribute name
     * @param value
     *            the attribute value
     * @return this object (so calls to addAttribute() can be chained)
     */
    public edu.umd.cs.findbugs.xml.XMLAttributeList addOptionalAttribute(java.lang.String name, java.lang.String value) {
        if (value == null) return this;
        return this.addAttribute(name,value);
    }
/**
     * Return the attribute list as a String which can be directly output as
     * part of an XML tag.
     */
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        for (edu.umd.cs.findbugs.xml.XMLAttributeList.NameValuePair pair : nameValuePairList){
            buf.append('\u0020');
            buf.append(pair.getName());
            buf.append('\u003d');
            buf.append('"');
            buf.append(getQuotedAttributeValue(pair.getValue()));
            buf.append('"');
        }
;
        return buf.toString();
    }
/**
     * Return an Iterator over NameValuePairs.
     */
    public java.util.Iterator<edu.umd.cs.findbugs.xml.XMLAttributeList.NameValuePair> iterator() {
        return nameValuePairList.iterator();
    }
/**
     * Return a properly quoted form for an attribute value.
     * 
     * @param rawValue
     *            the raw value of the attribute
     * @return a properly quoted representation of the value
     */
    public static java.lang.String getQuotedAttributeValue(java.lang.String rawValue) {
        return edu.umd.cs.findbugs.util.Strings.escapeXml(rawValue);
    }
}
// vim:ts=4
