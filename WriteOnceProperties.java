package edu.umd.cs.findbugs.util;
import edu.umd.cs.findbugs.util.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
public class WriteOnceProperties extends java.util.Properties {
    static class PropertyReadAt extends java.lang.Exception {
        public PropertyReadAt() {
        }
        final private static long serialVersionUID = 1L;
    }
    final private static long serialVersionUID = 1L;
    private java.util.Map<java.lang.String, edu.umd.cs.findbugs.util.WriteOnceProperties.PropertyReadAt> propertReadAt = new java.util.HashMap<java.lang.String, edu.umd.cs.findbugs.util.WriteOnceProperties.PropertyReadAt>();
    public WriteOnceProperties(java.util.Properties initialValue) {
        super();
        super.putAll(initialValue);
    }
    public boolean equals(java.lang.Object o) {
        return super.equals(o);
    }
    public int hashCode() {
        return super.hashCode();
    }
    public java.lang.String getProperty(java.lang.String key) {
        java.lang.String result = super.getProperty(key);
        if (result != null && result.length() > 0 &&  !propertReadAt.containsKey(key)) propertReadAt.put(key,new edu.umd.cs.findbugs.util.WriteOnceProperties.PropertyReadAt());
        return result;
    }
    public java.lang.String getProperty(java.lang.String key, java.lang.String defaultValue) {
        java.lang.String result = super.getProperty(key,defaultValue);
        if (result != null && result.length() > 0 &&  !propertReadAt.containsKey(key)) propertReadAt.put(key,new edu.umd.cs.findbugs.util.WriteOnceProperties.PropertyReadAt());
        return result;
    }
    public java.lang.Object setProperty(java.lang.String key, java.lang.String value) {
        if (propertReadAt.containsKey(key) &&  !value.equals(super.getProperty(key))) {
            java.lang.IllegalStateException e = new java.lang.IllegalStateException("Changing property '" + key + "' to '" + value + "' after it has already been read as '" + super.getProperty(key) + "'");
            e.initCause(propertReadAt.get(key));
            throw e;
        }
        return super.setProperty(key,value);
    }
    public static void makeSystemPropertiesWriteOnce() {
        java.util.Properties properties = java.lang.System.getProperties();
        if (properties instanceof edu.umd.cs.findbugs.util.WriteOnceProperties) return;
        java.lang.System.setProperties(new edu.umd.cs.findbugs.util.WriteOnceProperties(properties));
    }
    public static void main(java.lang.String[] args) {
        dumpProperties();
        java.lang.System.out.println("-----");
        makeSystemPropertiesWriteOnce();
        dumpProperties();
        java.lang.System.setProperty("x","1");
        java.lang.System.setProperty("y","1");
        java.lang.System.getProperty("y");
        java.lang.System.setProperty("x","2");
        java.lang.System.setProperty("y","2");
    }
/**
     *
     */
    private static void dumpProperties() {
        java.util.Properties properties = java.lang.System.getProperties();
        java.lang.System.out.println("Total properties: " + properties.size());
        for (java.lang.Object k : properties.keySet()){
            java.lang.System.out.println(k + " : " + java.lang.System.getProperty((java.lang.String) (k) ));
        }
;
    }
}
