/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * Bug annotation class for java types. This is of lighter weight than
 * ClassAnnotation, and can be used for things like array types.
 * 
 * @see ClassAnnotation
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.SignatureConverter;
import edu.umd.cs.findbugs.ba.generic.GenericObjectType;
import edu.umd.cs.findbugs.ba.generic.GenericUtilities;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class TypeAnnotation extends edu.umd.cs.findbugs.BugAnnotationWithSourceLines {
    final private static long serialVersionUID = 1L;
    final public static java.lang.String DEFAULT_ROLE = "TYPE_DEFAULT";
    final public static java.lang.String EXPECTED_ROLE = "TYPE_EXPECTED";
    final public static java.lang.String FOUND_ROLE = "TYPE_FOUND";
    final public static java.lang.String CLOSEIT_ROLE = "TYPE_CLOSEIT";
    final public static java.lang.String UNHASHABLE_ROLE = "TYPE_UNHASHABLE";
// jvm type descriptor, such as "[I"
    final private java.lang.String descriptor;
    private java.lang.String roleDescription;
    private java.lang.String typeParameters;
/**
     * constructor.
     * 
     * <p>
     * For information on type descriptors, <br>
     * see http://java.sun.com/docs/books/vmspec/2nd-edition/html/ClassFile.doc.
     * html#14152 <br>
     * or http://www.murrayc.com/learning/java/java_classfileformat.shtml#
     * TypeDescriptors
     * 
     * @param typeDescriptor
     *            a jvm type descriptor, such as "[I"
     */
    public TypeAnnotation(java.lang.String typeDescriptor) {
        this(typeDescriptor,DEFAULT_ROLE);
    }
    public TypeAnnotation(org.apache.bcel.generic.Type objectType) {
        this(objectType,DEFAULT_ROLE);
    }
    public TypeAnnotation(org.apache.bcel.generic.Type objectType, java.lang.String roleDescription) {
        this(objectType.getSignature(),roleDescription);
        if (objectType instanceof edu.umd.cs.findbugs.ba.generic.GenericObjectType) {
            edu.umd.cs.findbugs.ba.generic.GenericObjectType genericObjectType = (edu.umd.cs.findbugs.ba.generic.GenericObjectType) (objectType) ;
            if (genericObjectType.getTypeCategory() == edu.umd.cs.findbugs.ba.generic.GenericUtilities.TypeCategory.PARAMETERIZED) typeParameters = genericObjectType.getGenericParametersAsString();
        }
    }
    public TypeAnnotation(java.lang.String typeDescriptor, java.lang.String roleDescription) {
        super();
        descriptor = typeDescriptor;
        this.roleDescription = roleDescription;
        if (descriptor.startsWith("L")) {
            java.lang.String className = typeDescriptor.substring(1,typeDescriptor.length() - 1).replace('\u002f','\u002e');
            edu.umd.cs.findbugs.ba.AnalysisContext context = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext();
            if (context != null) {
                this.sourceFileName = context.lookupSourceFile(className);
                this.sourceLines = edu.umd.cs.findbugs.ClassAnnotation.getSourceLinesForClass(className,sourceFileName);
            }
            else this.sourceFileName = edu.umd.cs.findbugs.SourceLineAnnotation.UNKNOWN_SOURCE_FILE;
        }
    }
/**
     * Get the type descriptor.
     * 
     * @return the jvm type descriptor, such as "[I"
     */
    public java.lang.String getTypeDescriptor() {
        return descriptor;
    }
    public void accept(edu.umd.cs.findbugs.BugAnnotationVisitor visitor) {
        visitor.visitTypeAnnotation(this);
    }
    public java.lang.String format(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        java.lang.String name = new edu.umd.cs.findbugs.ba.SignatureConverter(descriptor).parseNext().replace("java.lang.","");
        if (key.equals("givenClass")) name = edu.umd.cs.findbugs.PackageMemberAnnotation.shorten(primaryClass.getPackageName(),name);
        else if (key.equals("excludingPackage")) name = edu.umd.cs.findbugs.PackageMemberAnnotation.removePackage(name);
        if (typeParameters != null &&  !key.equals("hash")) name = name + typeParameters;
        return name;
    }
    public void setDescription(java.lang.String roleDescription) {
        this.roleDescription = roleDescription.intern();
    }
    public java.lang.String getDescription() {
        return roleDescription;
    }
    public void setTypeParameters(java.lang.String typeParameters) {
        this.typeParameters = typeParameters;
    }
    public int hashCode() {
        return descriptor.hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.TypeAnnotation)) return false;
        return descriptor.equals(((edu.umd.cs.findbugs.TypeAnnotation) (o) ).descriptor);
    }
    public int compareTo(edu.umd.cs.findbugs.BugAnnotation o) {
// BugAnnotations must be Comparable
        if ( !(o instanceof edu.umd.cs.findbugs.TypeAnnotation)) 
// with any type of BugAnnotation
        return this.getClass().getName().compareTo(o.getClass().getName());
        return descriptor.compareTo(((edu.umd.cs.findbugs.TypeAnnotation) (o) ).descriptor);
    }
// could try to determine equivalence with ClassAnnotation, but don't
// see how this would be useful
    public java.lang.String toString() {
        java.lang.String pattern = edu.umd.cs.findbugs.I18N.instance().getAnnotationDescription(roleDescription);
        edu.umd.cs.findbugs.FindBugsMessageFormat format = new edu.umd.cs.findbugs.FindBugsMessageFormat(pattern);
        return format.format(new edu.umd.cs.findbugs.BugAnnotation[]{this},null);
    }
/*
     * ----------------------------------------------------------------------
     * XML Conversion support
     * ----------------------------------------------------------------------
     */
    final private static java.lang.String ELEMENT_NAME = "Type";
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        this.writeXML(xmlOutput,false,false);
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean addMessages, boolean isPrimary) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributeList = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("descriptor",descriptor);
        java.lang.String role = this.getDescription();
        if ( !role.equals(DEFAULT_ROLE)) attributeList.addAttribute("role",role);
        if (typeParameters != null) attributeList.addAttribute("typeParameters",typeParameters);
        edu.umd.cs.findbugs.BugAnnotationUtil.writeXML(xmlOutput,ELEMENT_NAME,this,attributeList,addMessages);
    }
    public boolean isSignificant() {
        return true;
    }
}
