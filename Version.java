/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Version number and release date information.
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.annotation.CheckForNull;
import edu.umd.cs.findbugs.cloud.CloudPlugin;
import edu.umd.cs.findbugs.updates.UpdateChecker;
import edu.umd.cs.findbugs.util.FutureValue;
import edu.umd.cs.findbugs.util.Util;
public class Version extends java.lang.Object {
    public Version() {
    }
/**
     * Major version number.
     */
    final public static int MAJOR = 2;
/**
     * Minor version number.
     */
    final public static int MINOR = 0;
/**
     * Patch level.
     */
    final public static int PATCHLEVEL = 1;
/**
     * Development version or release candidate?
     */
    final public static boolean IS_DEVELOPMENT = false;
/**
     * Release candidate number. "0" indicates that the version is not a release
     * candidate.
     */
    final public static int RELEASE_CANDIDATE = 0;
    final public static java.lang.String SVN_REVISION = java.lang.System.getProperty("svn.revision","Unknown");
/**
     * Release date.
     */
    final private static java.lang.String COMPUTED_DATE;
    final public static java.lang.String DATE;
    final public static java.lang.String CORE_PLUGIN_RELEASE_DATE;
    final private static java.lang.String COMPUTED_ECLIPSE_DATE;
    final private static java.lang.String COMPUTED_PLUGIN_RELEASE_DATE;
    private static java.lang.String applicationName = "";
    private static java.lang.String applicationVersion = "";
    static {
        java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("HH:mm:ss z, dd MMMM, yyyy", java.util.Locale.ENGLISH);
        java.text.SimpleDateFormat eclipseDateFormat = new java.text.SimpleDateFormat("yyyyMMdd", java.util.Locale.ENGLISH);
        java.text.SimpleDateFormat releaseDateFormat = new java.text.SimpleDateFormat(edu.umd.cs.findbugs.updates.UpdateChecker.PLUGIN_RELEASE_DATE_FMT, java.util.Locale.ENGLISH);
        java.util.Date now = new java.util.Date();
        COMPUTED_DATE = dateFormat.format(now);
        COMPUTED_ECLIPSE_DATE = eclipseDateFormat.format(now);
        java.lang.String tmp = releaseDateFormat.format(now);
        COMPUTED_PLUGIN_RELEASE_DATE = tmp;
    }
/**
     * Preview release number. "0" indicates that the version is not a preview
     * release.
     */
    final public static int PREVIEW = 0;
    final private static java.lang.String RELEASE_SUFFIX_WORD;
    static {
        java.lang.String suffix;
        if (RELEASE_CANDIDATE > 0) suffix = "rc" + RELEASE_CANDIDATE;
        else if (PREVIEW > 0) suffix = "preview" + PREVIEW;
        else {
            suffix = "dev-" + COMPUTED_ECLIPSE_DATE;
            if ( !SVN_REVISION.equals("Unknown")) suffix += "-r" + SVN_REVISION;
        }
        RELEASE_SUFFIX_WORD = suffix;
    }
    final public static java.lang.String RELEASE_BASE = MAJOR + "." + MINOR + "." + PATCHLEVEL;
/**
     * Release version string.
     */
    final public static java.lang.String COMPUTED_RELEASE = RELEASE_BASE + (IS_DEVELOPMENT ? "-" + RELEASE_SUFFIX_WORD : "");
/**
     * Release version string.
     */
    final public static java.lang.String RELEASE;
/**
     * Version of Eclipse plugin.
     */
    final private static java.lang.String COMPUTED_ECLIPSE_UI_VERSION = RELEASE_BASE + "." + COMPUTED_ECLIPSE_DATE;
    static {
        java.lang.Class<edu.umd.cs.findbugs.Version> c = edu.umd.cs.findbugs.Version.class;
        java.net.URL u = c.getResource(c.getSimpleName() + ".class");
        boolean fromFile = u.getProtocol().equals("file");
        java.io.InputStream in = null;
        java.lang.String release = null;
        java.lang.String date = null;
        java.lang.String plugin_release_date = null;
        if ( !fromFile) try {
            java.util.Properties versionProperties = new java.util.Properties();
            in = edu.umd.cs.findbugs.Version.class.getResourceAsStream("version.properties");
            if (in != null) {
                versionProperties.load(in);
                release = (java.lang.String) (versionProperties.get("release.number")) ;
                date = (java.lang.String) (versionProperties.get("release.date")) ;
                plugin_release_date = (java.lang.String) (versionProperties.get("plugin.release.date")) ;
            }
        }
        catch (java.lang.Exception e){
// ignore
            assert true;
        }
        finally {
            edu.umd.cs.findbugs.util.Util.closeSilently(in);
        }
        else {
            edu.umd.cs.findbugs.util.Util.closeSilently(in);
        }
        if (release == null) release = COMPUTED_RELEASE;
        if (date == null) date = COMPUTED_DATE;
        if (plugin_release_date == null) plugin_release_date = COMPUTED_PLUGIN_RELEASE_DATE;
        RELEASE = release;
        DATE = date;
        CORE_PLUGIN_RELEASE_DATE = plugin_release_date;
        java.util.Date parsedDate;
        try {
            java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat(edu.umd.cs.findbugs.updates.UpdateChecker.PLUGIN_RELEASE_DATE_FMT, java.util.Locale.ENGLISH);
            parsedDate = fmt.parse(CORE_PLUGIN_RELEASE_DATE);
        }
        catch (java.text.ParseException e){
            if (edu.umd.cs.findbugs.SystemProperties.ASSERTIONS_ENABLED) e.printStackTrace();
            parsedDate = null;
        }
        releaseDate = parsedDate;
    }
/**
     * FindBugs website.
     */
    final public static java.lang.String WEBSITE = "http://findbugs.sourceforge.net";
/**
     * Downloads website.
     */
    final public static java.lang.String DOWNLOADS_WEBSITE = "http://prdownloads.sourceforge.net/findbugs";
/**
     * Support email.
     */
    final public static java.lang.String SUPPORT_EMAIL = "http://findbugs.sourceforge.net/reportingBugs.html";
    private static java.util.Date releaseDate;
    public static void registerApplication(java.lang.String name, java.lang.String version) {
        applicationName = name;
        applicationVersion = version;
    }
    public static java.lang.String getApplicationName() {
        return applicationName;
    }
    public static java.lang.String getApplicationVersion() {
        return applicationVersion;
    }
    public static void main(java.lang.String[] argv) throws java.lang.InterruptedException {
        if ( !IS_DEVELOPMENT && RELEASE_CANDIDATE != 0) {
            throw new java.lang.IllegalStateException("Non developmental version, but is release candidate " + RELEASE_CANDIDATE);
        }
        if (argv.length == 0) {
            printVersion(false);
            return;
        }
        java.lang.String arg = argv[0];
        if (arg.equals("-release")) java.lang.System.out.println(RELEASE);
        else if (arg.equals("-date")) java.lang.System.out.println(DATE);
        else if (arg.equals("-props")) {
            java.lang.System.out.println("release.base=" + RELEASE_BASE);
            java.lang.System.out.println("release.number=" + COMPUTED_RELEASE);
            java.lang.System.out.println("release.date=" + COMPUTED_DATE);
            java.lang.System.out.println("plugin.release.date=" + COMPUTED_PLUGIN_RELEASE_DATE);
            java.lang.System.out.println("eclipse.ui.version=" + COMPUTED_ECLIPSE_UI_VERSION);
            java.lang.System.out.println("findbugs.website=" + WEBSITE);
            java.lang.System.out.println("findbugs.downloads.website=" + DOWNLOADS_WEBSITE);
            java.lang.System.out.println("findbugs.svn.revision=" + SVN_REVISION);
        }
        else if (arg.equals("-plugins")) {
            edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
            for (edu.umd.cs.findbugs.Plugin p : edu.umd.cs.findbugs.Plugin.getAllPlugins()){
                java.lang.System.out.println("Plugin: " + p.getPluginId());
                java.lang.System.out.println("  description: " + p.getShortDescription());
                java.lang.System.out.println("     provider: " + p.getProvider());
                java.lang.String version = p.getVersion();
                if (version != null && version.length() > 0) java.lang.System.out.println("      version: " + version);
                java.lang.String website = p.getWebsite();
                if (website != null && website.length() > 0) java.lang.System.out.println("      website: " + website);
                java.lang.System.out.println();
            }
;
        }
        else if (arg.equals("-configuration")) {
            printVersion(true);
        }
        else {
            usage();
            java.lang.System.exit(1);
        }
    }
    private static void usage() {
        java.lang.System.err.println("Usage: " + edu.umd.cs.findbugs.Version.class.getName() + "  [(-release|-date|-props|-configuration)]");
    }
    public static java.lang.String getReleaseWithDateIfDev() {
        if (IS_DEVELOPMENT) return RELEASE + " (" + DATE + ")";
        return RELEASE;
    }
    public static java.util.Date getReleaseDate() {
        return releaseDate;
    }
/**
     * @param justPrintConfiguration
     * @throws InterruptedException
     */
    public static void printVersion(boolean justPrintConfiguration) throws java.lang.InterruptedException {
        java.lang.System.out.println("FindBugs " + edu.umd.cs.findbugs.Version.COMPUTED_RELEASE);
        if (justPrintConfiguration) {
            for (edu.umd.cs.findbugs.Plugin plugin : edu.umd.cs.findbugs.Plugin.getAllPlugins()){
                java.lang.System.out.printf("Plugin %s, version %s, loaded from %s%n",plugin.getPluginId(),plugin.getVersion(),plugin.getPluginLoader().getURL());
                if (plugin.isCorePlugin()) java.lang.System.out.println("  is core plugin");
                if (plugin.isInitialPlugin()) java.lang.System.out.println("  is initial plugin");
                if (plugin.isEnabledByDefault()) java.lang.System.out.println("  is enabled by default");
                if (plugin.isGloballyEnabled()) java.lang.System.out.println("  is globally enabled");
                edu.umd.cs.findbugs.Plugin parent = plugin.getParentPlugin();
                if (parent != null) {
                    java.lang.System.out.println("  has parent plugin " + parent.getPluginId());
                }
                for (edu.umd.cs.findbugs.cloud.CloudPlugin cloudPlugin : plugin.getCloudPlugins()){
                    java.lang.System.out.printf("  cloud %s%n",cloudPlugin.getId());
                    java.lang.System.out.printf("     %s%n",cloudPlugin.getDescription());
                }
;
                for (edu.umd.cs.findbugs.DetectorFactory factory : plugin.getDetectorFactories()){
                    java.lang.System.out.printf("  detector %s%n",factory.getShortName());
                }
;
                java.lang.System.out.println();
            }
;
            printPluginUpdates(true,10);
        }
        else printPluginUpdates(false,3);
    }
    private static void printPluginUpdates(boolean verbose, int secondsToWait) throws java.lang.InterruptedException {
        edu.umd.cs.findbugs.DetectorFactoryCollection dfc = edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        if (dfc.getUpdateChecker().updateChecksGloballyDisabled()) {
            if (verbose) {
                java.lang.System.out.println();
                java.lang.System.out.print("Update checking globally disabled");
            }
            return;
        }
        if (verbose) {
            java.lang.System.out.println();
            java.lang.System.out.print("Checking for plugin updates...");
        }
        edu.umd.cs.findbugs.util.FutureValue<java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate>> updateHolder = dfc.getUpdates();
        try {
            java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates = updateHolder.get(secondsToWait,java.util.concurrent.TimeUnit.SECONDS);
            if (updates.isEmpty()) {
                if (verbose) java.lang.System.out.println("none!");
            }
            else {
                java.lang.System.out.println();
                for (edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate update : updates){
                    java.lang.System.out.println(update);
                    java.lang.System.out.println();
                }
;
            }
        }
        catch (java.util.concurrent.TimeoutException e){
            if (verbose) java.lang.System.out.println("Timeout while trying to get updates");
        }
    }
}
// vim:ts=4
