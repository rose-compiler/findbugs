package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Simple control flow graph abstraction for BCEL.
 * 
 * @see BasicBlock
 * @see Edge
 */
/*
     * ----------------------------------------------------------------------
     * CFG flags
     * ----------------------------------------------------------------------
     */
/** Flag set if infeasible exception edges have been pruned from the CFG. */
/**
     * Flag set if normal return edges from calls to methods which
     * unconditionally throw an exception have been removed.
     */
/**
     * Flag set if CFG has been "refined"; i.e., to the extent possible, all
     * infeasible edges have been removed.
     */
/**
     * Flag set if CFG edges corresponding to failed assertions have been
     * removed.
     */
/** Flag set if CFG is busy (in the process of being refined. */
/*
     * ----------------------------------------------------------------------
     * Helper classes
     * ----------------------------------------------------------------------
     */
/**
     * An Iterator over the Locations in the CFG. Because of JSR subroutines,
     * the same instruction may actually be part of multiple basic blocks (with
     * different facts true in each, due to calling context). Locations specify
     * both the instruction and the basic block.
     */
// Make sure we have an instruction iterator
// At end
// Go to next block
/*
     * ----------------------------------------------------------------------
     * Fields
     * ----------------------------------------------------------------------
     */
// for informational purposes
/*
     * ----------------------------------------------------------------------
     * Public methods
     * ----------------------------------------------------------------------
     */
/**
     * Constructor. Creates empty control flow graph (with just entry and exit
     * nodes).
     */
/**
     * @param methodName
     *            The methodName to set.
     */
/**
     * @return Returns the methodName.
     */
/**
     * Get the entry node.
     */
/**
     * Get the exit node.
     */
/**
     * Add a unique edge to the graph. There must be no other edge already in
     * the CFG with the same source and destination blocks.
     * 
     * @param source
     *            the source basic block
     * @param dest
     *            the destination basic block
     * @param type
     *            the type of edge; see constants in EdgeTypes interface
     * @return the newly created Edge
     * @throws IllegalStateException
     *             if there is already an edge in the CFG with the same source
     *             and destination block
     */
/**
     * Look up an Edge by its id.
     * 
     * @param id
     *            the id of the edge to look up
     * @return the Edge, or null if no matching Edge was found
     */
/**
     * Look up a BasicBlock by its unique label.
     * 
     * @param blockLabel
     *            the label of a BasicBlock
     * @return the BasicBlock with the given label, or null if there is no such
     *         BasicBlock
     */
/**
     * Get an Iterator over the nodes (BasicBlocks) of the control flow graph.
     */
/*
     * * Get an Iteratable over the nodes (BasicBlocks) of the control flow
     * graph.
     */
/**
     * Get an Iterator over the Locations in the control flow graph.
     */
/**
     * Get an Iterator over the Locations in the control flow graph.
     */
/**
     * Returns a collection of locations, ordered according to the compareTo
     * ordering over locations. If you want to list all the locations in a CFG
     * for debugging purposes, this is a good order to do so in.
     * 
     * @return collection of locations
     */
/**
     * Get Collection of basic blocks whose IDs are specified by given BitSet.
     * 
     * @param labelSet
     *            BitSet of block labels
     * @return a Collection containing the blocks whose IDs are given
     */
/**
     * Get a Collection of basic blocks which contain the bytecode instruction
     * with given offset.
     * 
     * @param offset
     *            the bytecode offset of an instruction
     * @return Collection of BasicBlock objects which contain the instruction
     *         with that offset
     */
/**
     * Get a Collection of Locations which specify the instruction at given
     * bytecode offset.
     * 
     * @param offset
     *            the bytecode offset
     * @return all Locations referring to the instruction at that offset
     */
/**
     * Get the first predecessor reachable from given edge type.
     * 
     * @param target
     *            the target block
     * @param edgeType
     *            the edge type leading from the predecessor
     * @return the predecessor, or null if there is no incoming edge with the
     *         specified edge type
     */
/**
     * Get the first successor reachable from given edge type.
     * 
     * @param source
     *            the source block
     * @param edgeType
     *            the edge type leading to the successor
     * @return the successor, or null if there is no outgoing edge with the
     *         specified edge type
     */
/**
     * Get the Location where exception(s) thrown on given exception edge are
     * thrown.
     * 
     * @param exceptionEdge
     *            the exception Edge
     * @return Location where exception(s) are thrown from
     */
// The fall-through edge might have been removed during
// CFG pruning. Look for it in the removed edge list.
/**
     * Get an Iterator over Edges removed from this CFG.
     * 
     * @return Iterator over Edges removed from this CFG
     */
/**
     * Get the first incoming edge in basic block with given type.
     * 
     * @param basicBlock
     *            the basic block
     * @param edgeType
     *            the edge type
     * @return the Edge, or null if there is no edge with that edge type
     */
/**
     * Get the first outgoing edge in basic block with given type.
     * 
     * @param basicBlock
     *            the basic block
     * @param edgeType
     *            the edge type
     * @return the Edge, or null if there is no edge with that edge type
     */
/**
     * Allocate a new BasicBlock. The block won't be connected to any node in
     * the graph.
     */
/**
     * Get number of basic blocks. This is just here for compatibility with the
     * old CFG method names.
     */
/**
     * Get the number of edge labels allocated. This is just here for
     * compatibility with the old CFG method names.
     */
// Ensure that basic blocks have only consecutive instructions
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.graph.AbstractGraph#removeEdge(edu.umd.cs.findbugs
     * .graph.AbstractEdge)
     */
// Keep track of removed edges.
/**
     * Get number of non-exception control successors of given basic block.
     * 
     * @param block
     *            a BasicBlock
     * @return number of non-exception control successors of the basic block
     */
/**
     * Get the Location representing the entry to the CFG. Note that this is a
     * "fake" Location, and shouldn't be relied on to yield source line
     * information.
     * 
     * @return Location at entry to CFG
     */
// vim:ts=4
abstract interface package-info {
}
