package edu.umd.cs.findbugs.ba.npe;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.npe.*;
/**
 * A dataflow analysis to detect potential null pointer dereferences.
 * 
 * @author David Hovemeyer
 * @see IsNullValue
 * @see IsNullValueFrame
 * @see IsNullValueFrameModelingVisitor
 */
// Parameter declared @CheckForNull
// Parameter declared @NonNull
// TODO: label this so we don't report defensive programming
// Don't know; use default value, normally non-reporting
// nonnull
// purge stale information
// Determine if this basic block ends in a redundant branch.
// If this is the last instruction in the block,
// save the result immediately before the instruction.
// Model the instruction
// Special case:
// The instruction may have produced previously seen values
// about which new is-null information is known.
// If any other instances of the produced values exist,
// update their is-null information.
// Also, make a note of any newly-produced null values.
// Same value is in both slots.
// Update the is-null information to match
// the new information.
// ,
// handle
// Downgrade NSP to DNR on non-exception control splits
// Exception handler - clear stack and push a non-null value
// to represent the exception.
// Downgrade NULL and NSP to DNR if the handler is for
// CloneNotSupportedException or InterruptedException
// Mark all values as having occurred on an exception path
// Push the exception value
// Determine if the edge conveys any information about the
// null/non-null status of operands in the incoming frame.
// The incoming edge is infeasible; just use TOP
// as the start fact for this block.
// A value has been determined for this edge.
// Use the value to update the is-null
// information in
// the start fact for this block.
// Make a note of the value that has
// become null
// due to the if comparison.
// if (edgeType == IFCMP_EDGE || edgeType ==
// FALL_THROUGH_EDGE)
// If this is a fall-through edge from a null check,
// then we know the value checked is not null.
// If we know the variable is null, this edge is
// infeasible
// If we're not sure that the instance is definitely
// non-null,
// update the is-null information for the dereferenced
// value.
// if (sourceBlock.isNullCheck() && edgeType ==
// FALL_THROUGH_EDGE)
// if (fact.isValid())
// Normal dataflow merge
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.FrameDataflowAnalysis#mergeInto(edu.umd.cs.findbugs
     * .ba.Frame, edu.umd.cs.findbugs.ba.Frame)
     */
// FIXME: update decision?
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AbstractDataflowAnalysis#startIteration()
     */
// At the beginning of each iteration, clear the set of locations
// where values become null. That way, after the final iteration
// of dataflow analysis the set should be as accurate as possible.
// System.out.println("Location becomes null: " +
// locationWhereValueBecomesNull );
/**
     * Determine if the given basic block ends in a redundant null comparison.
     * 
     * @param basicBlock
     *            the basic block
     * @param lastFrame
     *            the IsNullValueFrame representing values at the final
     *            instruction of the block
     * @return an IsNullConditionDecision object representing the is-null
     *         information gained about the compared value, or null if no
     *         information is gained
     */
// doesn't end in null comparison
// check for instanceof check
// System.out.println("Second last opcode: " +
// Constants.OPCODE_NAMES[secondToLastOpcode]);
// Initially, assume neither branch is feasible.
// Predetermined comparison - one branch is infeasible
// ifnonnull
// As far as we know, both branches feasible
// doesn't end in null comparison
// Initially, assume neither branch is feasible.
// Redundant comparison: both values are null, only one branch
// is feasible
// no value will be replaced - just want to
// indicate that one of the branches is infeasible
// cmpne
// learn that nextToTos is definitely non null on one branch
// learn that tos is definitely non null on one branch
// No information gained
// Initially, assume neither branch is feasible.
// Predetermined comparison - one branch is infeasible
// ifnonnull
// Predetermined comparison - one branch is infeasible
// ifnonnull
// As far as we know, both branches feasible
/**
     * Update is-null information at a branch target based on information gained
     * at a null comparison branch.
     * 
     * @param origFrame
     *            the original is-null frame at entry to basic block
     * @param frame
     *            the modified version of the is-null entry frame; null if the
     *            entry frame has not been modified yet
     * @param replaceMe
     *            the ValueNumber in the value number frame at the if comparison
     *            whose is-null information will be updated
     * @param prevVnaFrame
     *            the ValueNumberFrame at the if comparison
     * @param targetVnaFrame
     *            the ValueNumberFrame at entry to the basic block
     * @param replacementValue
     *            the IsNullValue representing the updated is-null information
     * @return a modified IsNullValueFrame with updated is-null information
     */
// If required, make a copy of the frame
// The VNA frame may have more slots than the IsNullValueFrame
// if it was produced by an IF comparison (whose operand or operands
// are subsequently popped off the stack).
// Here's the deal:
// - "replaceMe" is the value number from the previous frame (at the if
// branch)
// which indicates a value that we have updated is-null information
// about
// - in the target value number frame (at entry to the target block),
// we find the value number in the stack slot corresponding to the
// "replaceMe"
// value; this is the "corresponding" value
// - all instances of the "corresponding" value in the target frame have
// their is-null information updated to "replacementValue"
// This should thoroughly make use of the updated information.
// vim:ts=4
abstract interface package-info {
}
