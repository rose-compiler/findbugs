/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Build a classpath. Takes a list of project codebases and
 * <ul>
 * <li>Scans them for nested and referenced codebases</li>
 * <li>Builds a list of application class descriptors</li>
 * <li>Adds system codebases</li>
 * </ul>
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile;
import edu.umd.cs.findbugs.classfile.*;
import java.io.IOException;
import java.util.List;
abstract public interface IClassPathBuilder {
/**
     * Add a project codebase.
     * 
     * @param locator
     *            locator for project codebase
     * @param isApplication
     *            true if the codebase is an application codebase, false
     *            otherwise
     */
    abstract public void addCodeBase(edu.umd.cs.findbugs.classfile.ICodeBaseLocator locator, boolean isApplication);
/**
     * Set whether or not nested archives should be scanned. This should be
     * called before the build() method is called.
     * 
     * @param scanNestedArchives
     *            true if nested archives should be scanned, false otherwise
     */
    abstract public void scanNestedArchives(boolean scanNestedArchives);
/**
     * Build the classpath.
     * 
     * @param classPath
     *            IClassPath object to build
     * @param progress
     *            IClassPathBuilderProgress callback
     * @throws ResourceNotFoundException
     * @throws IOException
     * @throws InterruptedException
     */
    abstract public void build(edu.umd.cs.findbugs.classfile.IClassPath classPath, edu.umd.cs.findbugs.classfile.IClassPathBuilderProgress progress) throws java.lang.InterruptedException, java.io.IOException, edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
/**
     * Get the list of application classes discovered while scanning the
     * classpath.
     * 
     * @return list of application classes
     */
    abstract public java.util.List<edu.umd.cs.findbugs.classfile.ClassDescriptor> getAppClassList();
}
