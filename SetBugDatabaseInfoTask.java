/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Ant task to invoke the SetBugDatabaseInfo program in the workflow package
 * 
 * @author David Hovemeyer
 * @author Ben Langmead
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import org.apache.tools.ant.BuildException;
public class SetBugDatabaseInfoTask extends edu.umd.cs.findbugs.anttask.AbstractFindBugsTask {
    private java.lang.String outputFile;
    private java.lang.String name;
    private java.lang.String timestamp;
    private java.lang.String source;
    private java.lang.String findSource;
    private java.lang.String suppress;
    private java.lang.String withMessages;
    private java.lang.String resetSource;
    private java.lang.String inputFile;
    public SetBugDatabaseInfoTask() {
        super("edu.umd.cs.findbugs.workflow.SetBugDatabaseInfo");
        this.setFailOnError(true);
    }
    public void setName(java.lang.String arg) {
        this.name = arg;
    }
    public void setTimestamp(java.lang.String arg) {
        this.timestamp = arg;
    }
    public void setOutput(java.lang.String output) {
        this.outputFile = output;
    }
    public void setInput(java.lang.String input) {
        this.inputFile = input;
    }
    public void setSuppress(java.lang.String arg) {
        this.suppress = arg;
    }
    public void setSource(java.lang.String arg) {
        this.source = arg;
    }
    public void setFindSource(java.lang.String arg) {
        this.findSource = arg;
    }
    public void setWithMessages(java.lang.String arg) {
        this.withMessages = arg;
    }
    public void setResetSource(java.lang.String arg) {
        this.resetSource = arg;
    }
    private void checkBoolean(java.lang.String attrVal, java.lang.String attrName) {
        if (attrVal == null) {
            return;
        }
        attrVal = attrVal.toLowerCase();
        if ( !attrVal.equals("true") &&  !attrVal.equals("false")) {
            throw new org.apache.tools.ant.BuildException("attribute " + attrName + " requires boolean value", this.getLocation());
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#checkParameters()
     */
    protected void checkParameters() {
        super.checkParameters();
        if (outputFile == null) {
            throw new org.apache.tools.ant.BuildException("output attribute is required", this.getLocation());
        }
        if (inputFile == null) {
            throw new org.apache.tools.ant.BuildException("inputFile element is required");
        }
        this.checkBoolean(withMessages,"withMessages");
        this.checkBoolean(resetSource,"resetSource");
    }
    private void addOption(java.lang.String name, java.lang.String value) {
        if (value != null) {
            this.addArg(name);
            this.addArg(value);
        }
    }
    public void addBoolOption(java.lang.String option, java.lang.String value) {
        if (value != null) {
            this.addArg(option + ":" + value);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#configureFindbugsEngine
     * ()
     */
    protected void configureFindbugsEngine() {
        this.addOption("-name",name);
        this.addOption("-timestamp",timestamp);
        this.addOption("-source",source);
        this.addOption("-findSource",findSource);
        this.addOption("-suppress",suppress);
        this.addBoolOption("-withMessages",withMessages);
        if (resetSource != null && resetSource.equals("true")) {
            this.addArg("-resetSource");
        }
        this.addArg(inputFile);
        if (outputFile != null) {
            this.addArg(outputFile);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#beforeExecuteJavaProcess
     * ()
     */
    protected void beforeExecuteJavaProcess() {
        this.log("running setBugDatabaseInfo...");
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#afterExecuteJavaProcess
     * (int)
     */
    protected void afterExecuteJavaProcess(int rc) {
        if (rc != 0) {
            throw new org.apache.tools.ant.BuildException("execution of " + this.getTaskName() + " failed");
        }
    }
}
