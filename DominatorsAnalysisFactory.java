/**
 * Analysis engine to produce DominatorsAnalysis objects for analyzed methods.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.Dataflow;
import edu.umd.cs.findbugs.ba.DepthFirstSearch;
import edu.umd.cs.findbugs.ba.DominatorsAnalysis;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class DominatorsAnalysisFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.DominatorsAnalysis> {
/**
     * Constructor.
     */
    public DominatorsAnalysisFactory() {
        super("non-exception dominators analysis",edu.umd.cs.findbugs.ba.DominatorsAnalysis.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.DominatorsAnalysis analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        edu.umd.cs.findbugs.ba.CFG cfg = this.getCFG(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.DepthFirstSearch dfs = this.getDepthFirstSearch(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.DominatorsAnalysis analysis = new edu.umd.cs.findbugs.ba.DominatorsAnalysis(cfg, dfs, true);
        edu.umd.cs.findbugs.ba.Dataflow<java.util.BitSet, edu.umd.cs.findbugs.ba.DominatorsAnalysis> dataflow = new edu.umd.cs.findbugs.ba.Dataflow<java.util.BitSet, edu.umd.cs.findbugs.ba.DominatorsAnalysis>(cfg, analysis);
        dataflow.execute();
        return analysis;
    }
}
