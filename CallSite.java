/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * The site of a method call.
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.InstructionHandle;
import edu.umd.cs.findbugs.ba.BasicBlock;
import edu.umd.cs.findbugs.ba.Location;
public class CallSite extends java.lang.Object {
    final private org.apache.bcel.classfile.Method method;
    final private edu.umd.cs.findbugs.ba.Location location;
/**
     * Constructor.
     * 
     * @param method
     *            the method containing the call site
     * @param basicBlock
     *            the basic block where the call site is located
     * @param handle
     *            the instruction which performs the call
     */
    public CallSite(org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle handle) {
        super();
        this.method = method;
        this.location = new edu.umd.cs.findbugs.ba.Location(handle, basicBlock);
    }
/**
     * Get the method containing the call site.
     */
    public org.apache.bcel.classfile.Method getMethod() {
        return method;
    }
/**
     * Get the Location (basic block and instruction) where the call site is
     * located.
     */
    public edu.umd.cs.findbugs.ba.Location getLocation() {
        return location;
    }
/**
     * Get the basic block where the call site is located.
     */
    public edu.umd.cs.findbugs.ba.BasicBlock getBasicBlock() {
        return location.getBasicBlock();
    }
/**
     * Get the instruction which performs the call.
     */
    public org.apache.bcel.generic.InstructionHandle getHandle() {
        return location.getHandle();
    }
    public int hashCode() {
        return java.lang.System.identityHashCode(method) ^ this.getBasicBlock().getLabel() ^ java.lang.System.identityHashCode(location.getHandle());
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.CallSite)) return false;
        edu.umd.cs.findbugs.CallSite other = (edu.umd.cs.findbugs.CallSite) (o) ;
        return method == other.method && this.getBasicBlock() == other.getBasicBlock() && this.getHandle() == other.getHandle();
    }
}
// vim:ts=4
