/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Updates filters in the current running FindBugs.
 * 
 * @author Graham Allan
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import edu.umd.cs.findbugs.filter.Filter;
import edu.umd.cs.findbugs.filter.Matcher;
import edu.umd.cs.findbugs.gui2.FilterActivity.FilterActivityNotifier;
public class ApplyNewFilter extends java.lang.Object {
    final private edu.umd.cs.findbugs.filter.Filter suppressionFilter;
    final private edu.umd.cs.findbugs.gui2.PreferencesFrame preferencesFrame;
    final private edu.umd.cs.findbugs.gui2.FilterActivity.FilterActivityNotifier filterActivityNotifier;
    public ApplyNewFilter(edu.umd.cs.findbugs.filter.Filter suppressionFilter, edu.umd.cs.findbugs.gui2.PreferencesFrame preferencesFrame, edu.umd.cs.findbugs.gui2.FilterActivity.FilterActivityNotifier filterActivityNotifier) {
        super();
        this.suppressionFilter = suppressionFilter;
        this.preferencesFrame = preferencesFrame;
        this.filterActivityNotifier = filterActivityNotifier;
    }
    public void fromMatcher(edu.umd.cs.findbugs.filter.Matcher matcher) {
        if (matcher != null) {
            suppressionFilter.addChild(matcher);
            preferencesFrame.updateFilterPanel();
            filterActivityNotifier.notifyListeners(edu.umd.cs.findbugs.gui2.FilterListener.Action.FILTERING,null);
        }
    }
}
