/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A simple class to parse method signatures that include generic information.
 * <p>
 * 
 * Modified from edu.umd.cs.findbugs.ba.SignatureParser
 * 
 * @author Nat Ayewah
 */
package edu.umd.cs.findbugs.ba.generic;
import edu.umd.cs.findbugs.ba.generic.*;
import java.util.Iterator;
import java.util.NoSuchElementException;
import javax.annotation.CheckForNull;
import org.apache.bcel.classfile.Attribute;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.classfile.Signature;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;
public class GenericSignatureParser extends java.lang.Object {
    private class ParameterSignatureIterator extends java.lang.Object implements java.util.Iterator<java.lang.String> {
        public ParameterSignatureIterator() {
        }
        private int index = 1;
        public boolean hasNext() {
            return index < signature.length() && signature.charAt(index) != '\u0029' && signature.charAt(index) != '\u005e';
        }
        public java.lang.String next() {
            if ( !this.hasNext()) throw new java.util.NoSuchElementException();
            java.lang.StringBuilder result = new java.lang.StringBuilder();
            boolean done;
            do {
                done = true;
                char ch = signature.charAt(index);
                switch(ch){
                    case '\u0042':{
                    }
                    case '\u0043':{
                    }
                    case '\u0044':{
                    }
                    case '\u0046':{
                    }
                    case '\u0049':{
                    }
                    case '\u004a':{
                    }
                    case '\u0053':{
                    }
                    case '\u005a':{
                    }
                    case '\u002a':{
                        result.append(signature.charAt(index));
                        ++index;
                        break;
                    }
                    case '\u004c':{
                    }
                    case '\u0054':{
                        java.lang.String tmp = "";
                        int startsemi = index;
                        int leftCount = 0;
                        int i = startsemi + 1;
                        loop:while (true) {
                            char c = signature.charAt(i);
                            switch(c){
                                case '\u003b':{
                                    if (leftCount == 0) break loop;
                                    break;
                                }
                                case '\u003c':{
                                    leftCount++;
                                    break;
                                }
                                case '\u003e':{
                                    leftCount--;
                                    break;
                                }
                            }
                            i++;
                        }
                        java.lang.String foo = signature.substring(startsemi,i + 1);
                        result.append(foo);
                        index = i + 1;
                        break;
                    }
                    case '\u005b':{
                    }
                    case '\u002b':{
                    }
                    case '\u002d':{
                        result.append(signature.charAt(index));
                        ++index;
                        done = false;
                        break;
                    }
                    case '\u0029':{
                    }
                    case '\u005e':{
                        throw new java.util.NoSuchElementException("Should have already thrown NoSuchElementException");
                    }
                    case '\u0056':{
                    }
                    default:{
                        throw new java.lang.IllegalStateException("Invalid method signature: '" + signature + "' : " + signature.substring(index) + " " + result);
                    }
                }
            }
            while ( !done);
            return result.toString();
        }
        public void remove() {
            throw new java.lang.UnsupportedOperationException();
        }
    }
    final private java.lang.String signature;
/**
     * Parses a generic method signature of the form:
     * <code>(argument_signature)return_type_signature</code>
     * 
     * @param signature
     *            the method signature to be parsed
     */
    public GenericSignatureParser(java.lang.String signature) {
        super();
// XXX not currently handling Type parameters for class, interface or
// method definitions
        int s = signature.indexOf('\u0028');
        java.lang.String sig = signature;
        if (s > 0) sig = sig.substring(s);
        else if (s < 0 || sig.indexOf('\u003a') >= 0 || sig.startsWith("(V)")) throw new java.lang.IllegalArgumentException("Bad method signature: " + signature);
        this.signature = sig;
    }
/**
     * Get an Iterator over signatures of the method parameters.
     * 
     * @return Iterator which returns the parameter type signatures in order
     */
    public java.util.Iterator<java.lang.String> parameterSignatureIterator() {
        return new edu.umd.cs.findbugs.ba.generic.GenericSignatureParser.ParameterSignatureIterator();
    }
/**
     * Get the method return type signature.
     * 
     * @return the method return type signature
     */
    public java.lang.String getReturnTypeSignature() {
        int endOfParams = signature.lastIndexOf('\u0029');
        if (endOfParams < 0) throw new java.lang.IllegalArgumentException("Bad method signature: " + signature);
        return signature.substring(endOfParams + 1);
    }
/**
     * Get the number of parameters in the signature.
     * 
     * @return the number of parameters
     */
    public int getNumParameters() {
        int count = 0;
        for (java.util.Iterator<java.lang.String> i = this.parameterSignatureIterator(); i.hasNext(); ) {
            i.next();
            ++count;
        }
        return count;
    }
/**
     * Get the number of parameters passed to method invocation.
     * 
     * @param inv
     * @param cpg
     * @return int number of parameters
     */
    public static int getNumParametersForInvocation(org.apache.bcel.generic.InvokeInstruction inv, org.apache.bcel.generic.ConstantPoolGen cpg) {
        edu.umd.cs.findbugs.ba.generic.GenericSignatureParser sigParser = new edu.umd.cs.findbugs.ba.generic.GenericSignatureParser(inv.getSignature(cpg));
        return sigParser.getNumParameters();
    }
/**
     * @param target
     *            the method whose signature is to be parsed
     * @return an iterator over the parameters of the generic signature of
     *         method. Returns null if the generic signature cannot be parsed
     */
    public static java.util.Iterator<java.lang.String> getGenericSignatureIterator(org.apache.bcel.classfile.Method target) {
        try {
            edu.umd.cs.findbugs.ba.generic.GenericSignatureParser parser = null;
            java.lang.String genericSignature = null;
            for (org.apache.bcel.classfile.Attribute a : target.getAttributes()){
                if (a instanceof org.apache.bcel.classfile.Signature) {
                    org.apache.bcel.classfile.Signature sig = (org.apache.bcel.classfile.Signature) (a) ;
                    if (genericSignature != null) {
                        if ( !genericSignature.equals(sig.getSignature())) {
                            if (false) {
                                java.lang.System.out.println("Inconsistent signatures: ");
                                java.lang.System.out.println(genericSignature);
                                java.lang.System.out.println(sig.getSignature());
                            }
// we've seen two inconsistent
                            return null;
                        }
                        continue;
                    }
                    genericSignature = sig.getSignature();
                    if (compareSignatures(target.getSignature(),genericSignature)) parser = new edu.umd.cs.findbugs.ba.generic.GenericSignatureParser(genericSignature);
                }
            }
;
            java.util.Iterator<java.lang.String> iter = parser == null ? null : parser.parameterSignatureIterator();
            return iter;
        }
        catch (java.lang.RuntimeException e){
        }
        return null;
    }
/**
     * Compare a plain method signature to the a generic method Signature and
     * return true if they match
     */
    public static boolean compareSignatures(java.lang.String plainSignature, java.lang.String genericSignature) {
        edu.umd.cs.findbugs.ba.generic.GenericSignatureParser plainParser = new edu.umd.cs.findbugs.ba.generic.GenericSignatureParser(plainSignature);
        edu.umd.cs.findbugs.ba.generic.GenericSignatureParser genericParser = new edu.umd.cs.findbugs.ba.generic.GenericSignatureParser(genericSignature);
        if (plainParser.getNumParameters() != genericParser.getNumParameters()) return false;
        return true;
    }
    public static void main(java.lang.String[] args) {
        if (args.length != 1) {
            java.lang.System.err.println("Usage: " + edu.umd.cs.findbugs.ba.generic.GenericSignatureParser.class.getName() + " '<method signature>'");
            java.lang.System.exit(1);
        }
        edu.umd.cs.findbugs.ba.generic.GenericSignatureParser parser = new edu.umd.cs.findbugs.ba.generic.GenericSignatureParser(args[0]);
        for (java.util.Iterator<java.lang.String> i = parser.parameterSignatureIterator(); i.hasNext(); ) {
            java.lang.String s = i.next();
            java.lang.System.out.println(s);
            org.apache.bcel.generic.Type t = edu.umd.cs.findbugs.ba.generic.GenericUtilities.getType(s);
            java.lang.System.out.println("-~- " + t);
            if (t instanceof org.apache.bcel.generic.ObjectType) java.lang.System.out.println("-~- " + ((org.apache.bcel.generic.ObjectType) (t) ).toString());
            if (t != null) java.lang.System.out.println("-~- " + t.getClass());
        }
        java.lang.System.out.println(parser.getNumParameters() + " parameter(s)");
    }
}
