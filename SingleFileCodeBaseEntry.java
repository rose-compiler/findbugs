/**
 * Codebase entry for a single-file codebase.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.impl;
import edu.umd.cs.findbugs.classfile.impl.*;
import java.io.IOException;
import java.io.InputStream;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.ICodeBase;
import edu.umd.cs.findbugs.classfile.ICodeBaseEntry;
import edu.umd.cs.findbugs.classfile.InvalidClassFileFormatException;
import edu.umd.cs.findbugs.classfile.ResourceNotFoundException;
public class SingleFileCodeBaseEntry extends java.lang.Object implements edu.umd.cs.findbugs.classfile.ICodeBaseEntry {
    final private edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBase codeBase;
    private java.lang.String overriddenResourceName;
/**
     * Constructor.
     * 
     * @param codeBase
     *            parent codebase
     */
    public SingleFileCodeBaseEntry(edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBase codeBase) {
        super();
        this.codeBase = codeBase;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseEntry#getNumBytes()
     */
    public int getNumBytes() {
        return codeBase.getNumBytes();
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseEntry#getResourceName()
     */
    public java.lang.String getResourceName() {
        if (overriddenResourceName != null) return overriddenResourceName;
        return codeBase.getResourceName();
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseEntry#openResource()
     */
    public java.io.InputStream openResource() throws java.io.IOException {
        return codeBase.openFile();
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseEntry#getCodeBase()
     */
    public edu.umd.cs.findbugs.classfile.ICodeBase getCodeBase() {
        return codeBase;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBaseEntry#getClassDescriptor()
     */
    public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() throws edu.umd.cs.findbugs.classfile.InvalidClassFileFormatException, edu.umd.cs.findbugs.classfile.ResourceNotFoundException {
        return codeBase.getClassDescriptor();
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.ICodeBaseEntry#overrideResourceName(java
     * .lang.String)
     */
    public void overrideResourceName(java.lang.String resourceName) {
        overriddenResourceName = resourceName;
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(java.lang.Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBaseEntry other = (edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBaseEntry) (obj) ;
        return other.codeBase.equals(this.codeBase);
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return codeBase.hashCode();
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public java.lang.String toString() {
        return codeBase.getPathName();
    }
}
