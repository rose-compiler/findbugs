package edu.umd.cs.findbugs.updates;
import edu.umd.cs.findbugs.updates.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.regex.Pattern;
import junit.framework.TestCase;
import edu.umd.cs.findbugs.Plugin;
import edu.umd.cs.findbugs.PluginException;
import edu.umd.cs.findbugs.PluginLoader;
import edu.umd.cs.findbugs.Version;
public class UpdateCheckerTest extends junit.framework.TestCase {
// setup
// execute
// verify
// setup
// execute
// verify
// setup
// execute
// verify
// setup
// execute
// verify
// setup
// execute
// verify
// setup
// execute
// verify
// setup
// execute
// verify
// setup
// execute
// verify
// setup
// execute
// verify
// setup
// execute
// verify
// ================ end of tests =============
    private class TestingUpdateCheckCallback extends java.lang.Object implements edu.umd.cs.findbugs.updates.UpdateCheckCallback {
        final private java.util.concurrent.CountDownLatch latch;
        public TestingUpdateCheckCallback(java.util.concurrent.CountDownLatch latch) {
            super();
            this.latch = latch;
        }
        public void pluginUpdateCheckComplete(java.util.List<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates, boolean force) {
            updateCollector.addAll(updates);
            latch.countDown();
        }
        public java.lang.String getGlobalOption(java.lang.String key) {
            return globalOptions.get(key);
        }
        public edu.umd.cs.findbugs.Plugin getGlobalOptionSetter(java.lang.String key) {
            return null;
        }
    }
    public UpdateCheckerTest() {
    }
    final private static java.util.Date KEITHS_BIRTHDAY_2011;
    static {
        try {
            KEITHS_BIRTHDAY_2011 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", java.util.Locale.ENGLISH).parse("2011-03-20 02:00:00 EST");
        }
        catch (java.text.ParseException e){
            throw new java.lang.IllegalStateException(e);
        }
    }
    private java.util.List<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updateCollector;
    private java.lang.StringBuilder errors;
    private java.lang.String responseXml;
    private java.util.concurrent.CountDownLatch latch;
    private edu.umd.cs.findbugs.updates.UpdateChecker checker;
    private java.util.Map<java.lang.String, java.util.Collection<edu.umd.cs.findbugs.Plugin>> checked;
    private java.util.Map<java.lang.String, java.lang.String> globalOptions;
    private java.lang.String uploadedXml;
    protected void setUp() throws java.lang.Exception {
        updateCollector = new java.util.ArrayList<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate>();
        errors = new java.lang.StringBuilder();
        latch = new java.util.concurrent.CountDownLatch(1);
        checked = new java.util.HashMap<java.lang.String, java.util.Collection<edu.umd.cs.findbugs.Plugin>>();
        globalOptions = new java.util.HashMap<java.lang.String, java.lang.String>();
        uploadedXml = null;
        checker = new edu.umd.cs.findbugs.updates.UpdateChecker(new edu.umd.cs.findbugs.updates.UpdateCheckerTest.TestingUpdateCheckCallback(latch)) {
            protected void actuallyCheckforUpdates(java.net.URI url, java.util.Collection<edu.umd.cs.findbugs.Plugin> plugins, java.lang.String entryPoint) throws java.io.IOException {
                java.lang.String urlStr = url.toString();
                assertFalse(checked.containsKey(urlStr));
                checked.put(urlStr,plugins);
                java.io.ByteArrayInputStream stream = new java.io.ByteArrayInputStream(responseXml.getBytes("UTF-8"));
                java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
                this.writeXml(out,plugins,"x.y.z");
                uploadedXml = new java.lang.String(out.toByteArray(), "UTF-8");
                this.parseUpdateXml(url,plugins,stream);
            }
            protected void logError(java.lang.Exception e, java.lang.String msg) {
                errors.append(msg).append("\n");
                java.lang.System.err.println(msg);
                e.printStackTrace();
            }
            protected void logError(java.util.logging.Level level, java.lang.String msg) {
                errors.append(msg).append("\n");
                java.lang.System.err.println(msg);
            }
        };
    }
    public void testSimplePluginUpdate() throws java.lang.Exception {
        this.setResponseXml("my.id","09/01/2011 02:00 PM EST","2.1");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        assertEquals(1,updateCollector.size());
        edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate update = updateCollector.get(0);
        assertEquals("UPDATE ME",update.getMessage());
        assertEquals("http://example.com/update",update.getUrl());
        assertEquals("2.1",update.getVersion());
        assertEquals("my.id",update.getPlugin().getPluginId());
    }
    public void testPluginSameVersionDifferentDate() throws java.lang.Exception {
        this.setResponseXml("my.id","09/01/2011 02:00 PM EST","2.0");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        assertEquals(0,updateCollector.size());
    }
    public void testPluginSameVersionSameDate() throws java.lang.Exception {
        this.setResponseXml("my.id","2011-03-20 02:00:00 EST","2.0");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        assertEquals(0,updateCollector.size());
    }
    public void testPluginDifferentVersionSameDate() throws java.lang.Exception {
        this.setResponseXml("my.id","09/01/2011 02:00 PM EST","2.0");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        assertEquals(0,updateCollector.size());
    }
    public void testPluginNotPresent() throws java.lang.Exception {
        this.setResponseXml("SOME.OTHER.PLUGIN","09/01/2011 02:00 PM EST","2.0");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        assertEquals(0,updateCollector.size());
    }
    public void testRedirectUpdateChecks() throws java.lang.Exception {
        this.setResponseXml("SOME.OTHER.PLUGIN","09/01/2011 02:00 PM EST","2.0");
        globalOptions.put("redirectUpdateChecks","http://redirect.com");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        assertEquals(1,checked.size());
        assertTrue(checked.containsKey("http://redirect.com"));
        assertEquals(0,updateCollector.size());
    }
    public void testDisableUpdateChecks() throws java.lang.Exception {
        this.setResponseXml("my.id","09/01/2011 02:00 PM EST","2.1");
        globalOptions.put("noUpdateChecks","true");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        assertEquals(0,checked.size());
        assertEquals(0,updateCollector.size());
    }
    public void testDisableUpdateChecksFalse() throws java.lang.Exception {
        this.setResponseXml("my.id","09/01/2011 02:00 PM EST","2.1");
        globalOptions.put("noUpdateChecks","false");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        assertEquals(1,checked.size());
        assertEquals(1,updateCollector.size());
    }
    public void testDisableUpdateChecksInvalid() throws java.lang.Exception {
        this.setResponseXml("my.id","09/01/2011 02:00 PM EST","2.1");
        globalOptions.put("noUpdateChecks","BLAH");
        try {
            this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
            fail();
        }
        catch (java.lang.Throwable e){
        }
        assertEquals(0,checked.size());
        assertEquals(0,updateCollector.size());
    }
    public void testSubmittedXml() throws java.lang.Exception {
        this.setResponseXml("my.id","09/01/2011 02:00 PM EST","2.1");
        edu.umd.cs.findbugs.Version.registerApplication("MyApp","2.x");
        this.checkForUpdates(this.createPlugin("my.id",KEITHS_BIRTHDAY_2011,"2.0"));
        java.lang.String pattern = "<?xml version='1.0' encoding='UTF-8'?>\n\n<findbugs-invocation version='*' app-name='MyApp' app-version='2.x' entry-point='x.y.z' os='*' java-version='*' language='*' country='*' uuid='*'>\n  <plugin id='my.id' name='My Plugin' version='2.0' release-date='1300604400000'/>\n</findbugs-invocation>\n";
        java.lang.String patternRE = this.convertGlobToRE(pattern);
        assertTrue(uploadedXml + " did not match " + patternRE,uploadedXml.matches(patternRE));
    }
    private void checkForUpdates(edu.umd.cs.findbugs.Plugin plugin) throws java.lang.InterruptedException {
        checker.checkForUpdates(java.util.Arrays.asList(plugin),true);
        latch.await();
    }
    private java.lang.String convertGlobToRE(java.lang.String pattern) {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        boolean first = true;
        for (java.lang.String blah : pattern.split("\\*")){
            if (first) first = false;
            else builder.append(".*");
            builder.append(java.util.regex.Pattern.quote(blah.replaceAll("'","\"")));
        }
;
        return builder.toString();
    }
    private void setResponseXml(java.lang.String pluginid, java.lang.String releaseDate, java.lang.String v) {
        responseXml = "<fb-plugin-updates>  <plugin id='" + pluginid + "'>" + "    <release date='" + releaseDate + "' version='" + v + "' url='http://example.com/update'>" + "      <message>UPDATE ME</message>" + "    </release>" + "  </plugin>" + "</fb-plugin-updates>";
    }
    private edu.umd.cs.findbugs.Plugin createPlugin(java.lang.String pluginId, java.util.Date releaseDate, java.lang.String version) throws edu.umd.cs.findbugs.PluginException, java.net.URISyntaxException {
        edu.umd.cs.findbugs.PluginLoader fakeLoader;
        try {
            fakeLoader = new edu.umd.cs.findbugs.PluginLoader(true, new java.net.URL("http://" + pluginId + ".findbugs.cs.umd.edu"));
        }
        catch (java.net.MalformedURLException e){
            throw new java.lang.RuntimeException(e);
        }
        edu.umd.cs.findbugs.Plugin plugin = new edu.umd.cs.findbugs.Plugin(pluginId, version, releaseDate, fakeLoader, true, false);
        plugin.setShortDescription("My Plugin");
        plugin.setUpdateUrl("http://example.com/update");
        return plugin;
    }
}
