/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Ant task to invoke the FilterBugs program in the workflow package (a.k.a. the
 * filterBugs script.)
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import java.io.File;
import org.apache.tools.ant.BuildException;
public class FilterBugsTask extends edu.umd.cs.findbugs.anttask.AbstractFindBugsTask {
    private java.io.File outputFile;
    private java.lang.String applySuppression;
    private java.lang.String notAProblem;
    private java.lang.String not;
    private java.lang.String withSource;
    private java.lang.String exclude;
    private java.lang.String include;
    private java.lang.String annotation;
    private java.lang.String after;
    private java.lang.String before;
    private java.lang.String first;
    private java.lang.String last;
    private java.lang.String fixed;
    private java.lang.String present;
    private java.lang.String absent;
    private java.lang.String active;
    private java.lang.String introducedByChange;
    private java.lang.String removedByChange;
    private java.lang.String newCode;
    private java.lang.String removedCode;
    private java.lang.String priority;
    private java.lang.String maxRank;
    private java.lang.String clazz;
    private java.lang.String bugPattern;
    private java.lang.String category;
    private java.lang.String designation;
    private java.lang.String withMessages;
    private java.lang.String excludeBugs;
    private edu.umd.cs.findbugs.anttask.DataFile inputFile;
    public FilterBugsTask() {
        super("edu.umd.cs.findbugs.workflow.Filter");
        this.setFailOnError(true);
    }
    public edu.umd.cs.findbugs.anttask.DataFile createDataFile() {
        if (inputFile != null) {
            throw new org.apache.tools.ant.BuildException("only one dataFile element is allowed", this.getLocation());
        }
        inputFile = new edu.umd.cs.findbugs.anttask.DataFile();
        return inputFile;
    }
    public void setOutput(java.io.File output) {
        this.outputFile = output;
    }
    public void setOutputFile(java.io.File output) {
        this.outputFile = output;
    }
    public void setInput(java.lang.String input) {
        this.inputFile = new edu.umd.cs.findbugs.anttask.DataFile();
        this.inputFile.name = input;
    }
    public void setInputFile(java.lang.String input) {
        this.inputFile = new edu.umd.cs.findbugs.anttask.DataFile();
        this.inputFile.name = input;
    }
    public void setNot(java.lang.String arg) {
        this.not = arg;
    }
    public void setNotAProblem(java.lang.String arg) {
        this.notAProblem = arg;
    }
    public void setWithSource(java.lang.String arg) {
        this.withSource = arg;
    }
    public void setExclude(java.lang.String arg) {
        this.exclude = arg;
    }
    public void setApplySuppression(java.lang.String arg) {
        this.applySuppression = arg;
    }
    public void setInclude(java.lang.String arg) {
        this.include = arg;
    }
    public void setAnnotation(java.lang.String arg) {
        this.annotation = arg;
    }
    public void setAfter(java.lang.String arg) {
        this.after = arg;
    }
    public void setBefore(java.lang.String arg) {
        this.before = arg;
    }
    public void setFirst(java.lang.String arg) {
        this.first = arg;
    }
    public void setLast(java.lang.String arg) {
        this.last = arg;
    }
    public void setFixed(java.lang.String arg) {
        this.fixed = arg;
    }
    public void setPresent(java.lang.String arg) {
        this.present = arg;
    }
    public void setAbsent(java.lang.String arg) {
        this.absent = arg;
    }
    public void setActive(java.lang.String arg) {
        this.active = arg;
    }
    public void setIntroducedByChange(java.lang.String arg) {
        this.introducedByChange = arg;
    }
    public void setRemovedByChange(java.lang.String arg) {
        this.removedByChange = arg;
    }
    public void setNewCode(java.lang.String arg) {
        this.newCode = arg;
    }
    public void setRemovedCode(java.lang.String arg) {
        this.removedCode = arg;
    }
    public void setPriority(java.lang.String arg) {
        this.priority = arg;
    }
    public void setMaxRank(java.lang.String arg) {
        this.maxRank = arg;
    }
    public void setClass(java.lang.String arg) {
        this.clazz = arg;
    }
    public void setBugPattern(java.lang.String arg) {
        this.bugPattern = arg;
    }
    public void setCategory(java.lang.String arg) {
        this.category = arg;
    }
    public void setDesignation(java.lang.String arg) {
        this.designation = arg;
    }
    public void setWithMessages(java.lang.String arg) {
        this.withMessages = arg;
    }
    public void setExcludeBugs(java.lang.String arg) {
        this.excludeBugs = arg;
    }
    private void checkBoolean(java.lang.String attrVal, java.lang.String attrName) {
        if (attrVal == null) {
            return;
        }
        attrVal = attrVal.toLowerCase();
        if ( !attrVal.equals("true") &&  !attrVal.equals("false")) {
            throw new org.apache.tools.ant.BuildException("attribute " + attrName + " requires boolean value", this.getLocation());
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#checkParameters()
     */
    protected void checkParameters() {
        super.checkParameters();
        if (outputFile == null) {
            throw new org.apache.tools.ant.BuildException("output attribute is required", this.getLocation());
        }
        if (inputFile == null) {
            throw new org.apache.tools.ant.BuildException("inputFile element is required");
        }
        this.checkBoolean(notAProblem,"notAProblem");
        this.checkBoolean(withSource,"withSource");
        this.checkBoolean(applySuppression,"applySuppression");
        this.checkBoolean(active,"active");
        this.checkBoolean(introducedByChange,"introducedByChange");
        this.checkBoolean(removedByChange,"removedByChange");
        this.checkBoolean(newCode,"newCode");
        this.checkBoolean(removedCode,"removedCode");
        this.checkBoolean(withMessages,"withMessages");
    }
    private void addOption(java.lang.String name, java.lang.String value) {
        if (value != null) {
            this.addArg(name);
            this.addArg(value);
        }
    }
    public void addBoolOption(java.lang.String option, java.lang.String value) {
        if (value != null) {
            this.addArg(option + ":" + value);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#configureFindbugsEngine
     * ()
     */
    protected void configureFindbugsEngine() {
        if (not != null) {
            this.addArg("-not");
        }
        this.addBoolOption("-notAProblem",notAProblem);
        this.addBoolOption("-withSource",withSource);
        this.addOption("-exclude",exclude);
        this.addOption("-include",include);
        this.addOption("-annotation",annotation);
        this.addOption("-after",after);
        this.addOption("-before",before);
        this.addOption("-first",first);
        this.addOption("-last",last);
        this.addOption("-fixed",fixed);
        this.addOption("-present",present);
        this.addOption("-absent",absent);
        this.addBoolOption("-active",active);
        this.addBoolOption("-introducedByChange",introducedByChange);
        this.addBoolOption("-removedByChange",removedByChange);
        this.addBoolOption("-newCode",newCode);
        this.addBoolOption("-removedCode",removedCode);
        this.addOption("-priority",priority);
        this.addOption("-maxRank",maxRank);
        this.addOption("-class",clazz);
        this.addOption("-bugPattern",bugPattern);
        this.addOption("-category",category);
        this.addOption("-designation",designation);
        this.addBoolOption("-withMessages",withMessages);
        this.addBoolOption("-applySuppression",applySuppression);
        if (excludeBugs != null) {
            this.addArg("-excludeBugs");
            this.addArg(excludeBugs);
        }
        this.addArg(inputFile.getName());
        this.addArg(outputFile.getPath());
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#beforeExecuteJavaProcess
     * ()
     */
    protected void beforeExecuteJavaProcess() {
        this.log("running filterBugs...");
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#afterExecuteJavaProcess
     * (int)
     */
    protected void afterExecuteJavaProcess(int rc) {
        if (rc != 0) {
            throw new org.apache.tools.ant.BuildException("execution of " + this.getTaskName() + " failed");
        }
    }
}
