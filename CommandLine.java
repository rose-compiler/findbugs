/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Helper class for parsing command line arguments.
 */
package edu.umd.cs.findbugs.config;
import edu.umd.cs.findbugs.config.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.annotations.SuppressWarnings;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.util.Util;
abstract public class CommandLine extends java.lang.Object {
/**
     * Start a new group of related command-line options.
     *
     * @param description
     *            description of the group
     */
/**
     * Add a command line switch. This method is for adding options that do not
     * require an argument.
     *
     * @param option
     *            the option, must start with "-"
     * @param description
     *            single line description of the option
     */
/**
     * Add a command line switch that allows optional extra information to be
     * specified as part of it.
     *
     * @param option
     *            the option, must start with "-"
     * @param optionExtraPartSynopsis
     *            synopsis of the optional extra information
     * @param description
     *            single-line description of the option
     */
// Option will display as -foo[:extraPartSynopsis]
/**
     * Add an option requiring an argument.
     *
     * @param option
     *            the option, must start with "-"
     * @param argumentDesc
     *            brief (one or two word) description of the argument
     * @param description
     *            single line description of the option
     */
/**
     * Don't list this option when printing Usage information
     *
     * @param option
     */
/**
     * Expand option files in given command line. Any token beginning with "@"
     * is assumed to be an option file. Option files contain one command line
     * option per line.
     *
     * @param argv
     *            the original command line
     * @param ignoreComments
     *            ignore comments (lines starting with "#")
     * @param ignoreBlankLines
     *            ignore blank lines
     * @return the expanded command line
     */
// Add all expanded options at the end of the options list, before the
// list of
// jar/zip/class files and directories.
// At the end of the options to preserve the order of the options (e.g.
// -adjustPriority
// must always come after -pluginList).
    public static class HelpRequestedException extends java.lang.Exception {
        public HelpRequestedException() {
        }
    }
    final private static java.lang.String SPACES = "                    ";
    private java.util.List<java.lang.String> optionList;
    private java.util.Set<java.lang.String> unlistedOptions;
    private java.util.Map<java.lang.Integer, java.lang.String> optionGroups;
    private java.util.Set<java.lang.String> requiresArgumentSet;
    private java.util.Map<java.lang.String, java.lang.String> optionDescriptionMap;
    private java.util.Map<java.lang.String, java.lang.String> optionExtraPartSynopsisMap;
    private java.util.Map<java.lang.String, java.lang.String> argumentDescriptionMap;
    int maxWidth;
    public CommandLine() {
        super();
        this.unlistedOptions = new java.util.HashSet<java.lang.String>();
        this.optionList = new java.util.LinkedList<java.lang.String>();
        this.optionGroups = new java.util.HashMap<java.lang.Integer, java.lang.String>();
        this.requiresArgumentSet = new java.util.HashSet<java.lang.String>();
        this.optionDescriptionMap = new java.util.HashMap<java.lang.String, java.lang.String>();
        this.optionExtraPartSynopsisMap = new java.util.HashMap<java.lang.String, java.lang.String>();
        this.argumentDescriptionMap = new java.util.HashMap<java.lang.String, java.lang.String>();
        this.maxWidth = 0;
    }
    public void startOptionGroup(java.lang.String description) {
        optionGroups.put(optionList.size(),description);
    }
    public void addSwitch(java.lang.String option, java.lang.String description) {
        optionList.add(option);
        optionDescriptionMap.put(option,description);
        if (option.length() > maxWidth) maxWidth = option.length();
    }
    public void addSwitchWithOptionalExtraPart(java.lang.String option, java.lang.String optionExtraPartSynopsis, java.lang.String description) {
        optionList.add(option);
        optionExtraPartSynopsisMap.put(option,optionExtraPartSynopsis);
        optionDescriptionMap.put(option,description);
        int length = option.length() + optionExtraPartSynopsis.length() + 3;
        if (length > maxWidth) maxWidth = length;
    }
    public void addOption(java.lang.String option, java.lang.String argumentDesc, java.lang.String description) {
        optionList.add(option);
        optionDescriptionMap.put(option,description);
        requiresArgumentSet.add(option);
        argumentDescriptionMap.put(option,argumentDesc);
        int width = option.length() + 3 + argumentDesc.length();
        if (width > maxWidth) maxWidth = width;
    }
    public void makeOptionUnlisted(java.lang.String option) {
        unlistedOptions.add(option);
    }
    public java.lang.String[] expandOptionFiles(java.lang.String[] argv, boolean ignoreComments, boolean ignoreBlankLines) throws edu.umd.cs.findbugs.config.CommandLine.HelpRequestedException, java.io.IOException {
        int lastOptionIndex = this.parse(argv,true);
        java.util.ArrayList<java.lang.String> resultList = new java.util.ArrayList<java.lang.String>();
        java.util.ArrayList<java.lang.String> expandedOptionsList = getAnalysisOptionProperties(ignoreComments,ignoreBlankLines);
        for (int i = 0; i < lastOptionIndex; i++) {
            java.lang.String arg = argv[i];
            if ( !arg.startsWith("@")) {
                resultList.add(arg);
                continue;
            }
            java.io.BufferedReader reader = null;
            try {
                reader = edu.umd.cs.findbugs.charsets.UTF8.bufferedReader(new java.io.FileInputStream(arg.substring(1)));
                addCommandLineOptions(expandedOptionsList,reader,ignoreComments,ignoreBlankLines);
            }
            finally {
                edu.umd.cs.findbugs.util.Util.closeSilently(reader);
            }
        }
        resultList.addAll(expandedOptionsList);
        for (int i = lastOptionIndex; i < argv.length; i++) {
            resultList.add(argv[i]);
        }
        return resultList.toArray(new java.lang.String[resultList.size()]);
    }
    public static java.util.ArrayList<java.lang.String> getAnalysisOptionProperties(boolean ignoreComments, boolean ignoreBlankLines) {
        java.util.ArrayList<java.lang.String> resultList = new java.util.ArrayList<java.lang.String>();
        java.net.URL u = edu.umd.cs.findbugs.DetectorFactoryCollection.getCoreResource("analysisOptions.properties");
        if (u != null) {
            java.io.BufferedReader reader = null;
            try {
                reader = edu.umd.cs.findbugs.charsets.UTF8.bufferedReader(u.openStream());
                addCommandLineOptions(resultList,reader,ignoreComments,ignoreBlankLines);
            }
            catch (java.io.IOException e){
                edu.umd.cs.findbugs.ba.AnalysisContext.logError("unable to load analysisOptions.properties",e);
            }
            finally {
                edu.umd.cs.findbugs.util.Util.closeSilently(reader);
            }
        }
        return resultList;
    }
    private static void addCommandLineOptions(java.util.ArrayList<java.lang.String> resultList, java.io.BufferedReader reader, boolean ignoreComments, boolean ignoreBlankLines) throws java.io.IOException {
        java.lang.String line;
        while ((line = reader.readLine()) != null) {
            line = line.trim();
            if (ignoreComments && line.startsWith("#")) continue;
            if (ignoreBlankLines && line.equals("")) continue;
            if (line.length() >= 2 && line.charAt(0) == '"' && line.charAt(line.length() - 1) == '"') resultList.add(line.substring(0,line.length() - 1));
            else for (java.lang.String segment : line.split(" "))resultList.add(segment);
;
        }
    }
/**
     * Parse switches/options, showing usage information if they can't be
     * parsed, or if we have the wrong number of remaining arguments after
     * parsing. Calls parse(String[]).
     *
     * @param argv
     *            command line arguments
     * @param minArgs
     *            allowed minimum number of arguments remaining after
     *            switches/options are parsed
     * @param maxArgs
     *            allowed maximum number of arguments remaining after
     *            switches/options are parsed
     * @param usage
     *            usage synopsis
     * @return number of arguments parsed
     */
    public int parse(java.lang.String[] argv, int minArgs, int maxArgs, java.lang.String usage) {
        try {
            int count = this.parse(argv);
            int remaining = argv.length - count;
            if (remaining < minArgs || remaining > maxArgs) {
                java.lang.System.out.println(usage);
                java.lang.System.out.println("Expected " + minArgs + "..." + maxArgs + " file arguments, found " + remaining);
                java.lang.System.out.println("Options:");
                this.printUsage(java.lang.System.out);
                java.lang.System.exit(1);
            }
            return count;
        }
        catch (edu.umd.cs.findbugs.config.CommandLine.HelpRequestedException e){
        }
        catch (java.lang.RuntimeException e){
            e.printStackTrace();
        }
        catch (java.io.IOException e){
            e.printStackTrace();
        }
        java.lang.System.out.println(usage);
        java.lang.System.out.println("Options:");
        this.printUsage(java.lang.System.out);
        java.lang.System.exit(1);
        return  -1;
    }
/**
     * Parse a command line. Calls down to handleOption() and
     * handleOptionWithArgument() methods. Stops parsing when it reaches the end
     * of the command line, or when a command line argument not starting with
     * "-" is seen.
     *
     * @param argv
     *            the arguments
     * @return the number of arguments parsed; if equal to argv.length, then the
     *         entire command line was parsed
     * @throws HelpRequestedException
     */
    public int parse(java.lang.String[] argv) throws edu.umd.cs.findbugs.config.CommandLine.HelpRequestedException, java.io.IOException {
        return this.parse(argv,false);
    }
    private int parse(java.lang.String[] argv, boolean dryRun) throws edu.umd.cs.findbugs.config.CommandLine.HelpRequestedException, java.io.IOException {
        int arg = 0;
        while (arg < argv.length) {
            java.lang.String option = argv[arg];
            if (option.equals("-help")) throw new edu.umd.cs.findbugs.config.CommandLine.HelpRequestedException();
            if ( !option.startsWith("-")) break;
            java.lang.String optionExtraPart = "";
            int colon = option.indexOf('\u003a');
            if (colon >= 0) {
                optionExtraPart = option.substring(colon + 1);
                option = option.substring(0,colon);
            }
            if (optionDescriptionMap.get(option) == null) throw new java.lang.IllegalArgumentException("Unknown option: " + option);
            if (requiresArgumentSet.contains(option)) {
                ++arg;
                if (arg >= argv.length) throw new java.lang.IllegalArgumentException("Option " + option + " requires an argument");
                java.lang.String argument = argv[arg];
                if ( !dryRun) this.handleOptionWithArgument(option,argument);
                ++arg;
            }
            else {
                if ( !dryRun) this.handleOption(option,optionExtraPart);
                ++arg;
            }
        }
        return arg;
    }
/**
     * Callback method for handling an option.
     *
     * @param option
     *            the option
     * @param optionExtraPart
     *            the "extra" part of the option (everything after the colon:
     *            e.g., "withMessages" in "-xml:withMessages"); the empty string
     *            if there was no extra part
     */
    abstract protected void handleOption(java.lang.String option, java.lang.String optionExtraPart) throws java.io.IOException;
/**
     * Callback method for handling an option with an argument.
     *
     * @param option
     *            the option
     * @param argument
     *            the argument
     */
    abstract protected void handleOptionWithArgument(java.lang.String option, java.lang.String argument) throws java.io.IOException;
/**
     * Print command line usage information to given stream.
     *
     * @param os
     *            the output stream
     */
    public void printUsage(java.io.OutputStream os) {
        int count = 0;
        java.io.PrintStream out = edu.umd.cs.findbugs.charsets.UTF8.printStream(os);
        for (java.lang.String option : optionList){
            if (optionGroups.containsKey(count)) {
                out.println("  " + optionGroups.get(count));
            }
            count++;
            if (unlistedOptions.contains(option)) continue;
            out.print("    ");
            java.lang.StringBuilder buf = new java.lang.StringBuilder();
            buf.append(option);
            if (optionExtraPartSynopsisMap.get(option) != null) {
                java.lang.String optionExtraPartSynopsis = optionExtraPartSynopsisMap.get(option);
                buf.append("[:");
                buf.append(optionExtraPartSynopsis);
                buf.append("]");
            }
            if (requiresArgumentSet.contains(option)) {
                buf.append(" <");
                buf.append(argumentDescriptionMap.get(option));
                buf.append(">");
            }
            printField(out,buf.toString(),maxWidth + 1);
            out.println(optionDescriptionMap.get(option));
        }
;
        out.flush();
    }
    private static void printField(java.io.PrintStream out, java.lang.String s, int width) {
        if (s.length() > width) throw new java.lang.IllegalArgumentException();
        int nSpaces = width - s.length();
        out.print(s);
        while (nSpaces > 0) {
            int n = java.lang.Math.min(SPACES.length(),nSpaces);
            out.print(SPACES.substring(0,n));
            nSpaces -= n;
        }
    }
}
