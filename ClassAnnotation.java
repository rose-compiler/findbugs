/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A BugAnnotation object specifying a Java class involved in the bug.
 *
 * @author David Hovemeyer
 * @see BugAnnotation
 * @see BugInstance
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.SourceInfoMap;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
import edu.umd.cs.findbugs.util.ClassName;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class ClassAnnotation extends edu.umd.cs.findbugs.PackageMemberAnnotation {
    final private static long serialVersionUID = 1L;
    final private static java.lang.String DEFAULT_ROLE = "CLASS_DEFAULT";
    final public static java.lang.String SUBCLASS_ROLE = "CLASS_SUBCLASS";
    final public static java.lang.String SUPERCLASS_ROLE = "CLASS_SUPERCLASS";
    final public static java.lang.String RECOMMENDED_SUPERCLASS_ROLE = "CLASS_RECOMMENDED_SUPERCLASS";
    final public static java.lang.String IMPLEMENTED_INTERFACE_ROLE = "CLASS_IMPLEMENTED_INTERFACE";
    final public static java.lang.String INTERFACE_ROLE = "INTERFACE_TYPE";
    final public static java.lang.String ANNOTATION_ROLE = "CLASS_ANNOTATION";
    final public static java.lang.String TYPE_QUALIFIER_ROLE = "CLASS_TYPE_QUALIFIER";
/**
     * Constructor.
     *
     * @param className
     *            the name of the class
     */
    public ClassAnnotation(java.lang.String className) {
        super(className,DEFAULT_ROLE);
    }
    public ClassAnnotation(java.lang.String className, java.lang.String sourceFileName) {
        super(className,DEFAULT_ROLE,sourceFileName);
    }
    public boolean isSignificant() {
        return  !SUBCLASS_ROLE.equals(description);
    }
/**
     * Factory method to create a ClassAnnotation from a ClassDescriptor.
     *
     * @param classDescriptor
     *            the ClassDescriptor
     * @return the ClassAnnotation
     */
    public static edu.umd.cs.findbugs.ClassAnnotation fromClassDescriptor(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor) {
        return new edu.umd.cs.findbugs.ClassAnnotation(classDescriptor.toDottedClassName());
    }
    public void accept(edu.umd.cs.findbugs.BugAnnotationVisitor visitor) {
        visitor.visitClassAnnotation(this);
    }
    protected java.lang.String formatPackageMember(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        if (key.equals("") || key.equals("hash")) return className;
        else if (key.equals("givenClass")) return shorten(primaryClass.getPackageName(),className);
        else if (key.equals("excludingPackage")) return shorten(this.getPackageName(),className);
        else if (key.equals("simpleClass") || key.equals("simpleName")) return edu.umd.cs.findbugs.util.ClassName.extractSimpleName(className);
        else throw new java.lang.IllegalArgumentException("unknown key " + key);
    }
    public int hashCode() {
        return className.hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.ClassAnnotation)) return false;
        edu.umd.cs.findbugs.ClassAnnotation other = (edu.umd.cs.findbugs.ClassAnnotation) (o) ;
        return className.equals(other.className);
    }
    public boolean contains(edu.umd.cs.findbugs.ClassAnnotation other) {
        return other.className.startsWith(className);
    }
    public edu.umd.cs.findbugs.ClassAnnotation getTopLevelClass() {
        int firstDollar = className.indexOf('\u0024');
        if (firstDollar <= 0) return this;
        return new edu.umd.cs.findbugs.ClassAnnotation(className.substring(0,firstDollar));
    }
    public int compareTo(edu.umd.cs.findbugs.BugAnnotation o) {
// BugAnnotations must be
        if ( !(o instanceof edu.umd.cs.findbugs.ClassAnnotation)) 
// Comparable with any type of
// BugAnnotation
        return this.getClass().getName().compareTo(o.getClass().getName());
        edu.umd.cs.findbugs.ClassAnnotation other = (edu.umd.cs.findbugs.ClassAnnotation) (o) ;
        return className.compareTo(other.className);
    }
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.PackageMemberAnnotation#getSourceLines()
     */
    public edu.umd.cs.findbugs.SourceLineAnnotation getSourceLines() {
        if (sourceLines == null) this.sourceLines = getSourceLinesForClass(className,sourceFileName);
        return sourceLines;
    }
    public static edu.umd.cs.findbugs.SourceLineAnnotation getSourceLinesForClass(java.lang.String className, java.lang.String sourceFileName) {
// Create source line annotation for class on demand
        edu.umd.cs.findbugs.ba.AnalysisContext currentAnalysisContext = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext();
        if (currentAnalysisContext == null) return new edu.umd.cs.findbugs.SourceLineAnnotation(className, sourceFileName,  -1,  -1,  -1,  -1);
        edu.umd.cs.findbugs.ba.SourceInfoMap.SourceLineRange classLine = currentAnalysisContext.getSourceInfoMap().getClassLine(className);
        if (classLine == null) return edu.umd.cs.findbugs.SourceLineAnnotation.getSourceAnnotationForClass(className,sourceFileName);
        else return new edu.umd.cs.findbugs.SourceLineAnnotation(className, sourceFileName, classLine.getStart(), classLine.getEnd(),  -1,  -1);
    }
/*
     * ----------------------------------------------------------------------
     * XML Conversion support
     * ----------------------------------------------------------------------
     */
    final private static java.lang.String ELEMENT_NAME = "Class";
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        this.writeXML(xmlOutput,false,false);
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean addMessages, boolean isPrimary) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributeList = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("classname",this.getClassName());
        if (isPrimary) attributeList.addAttribute("primary","true");
        java.lang.String role = this.getDescription();
        if ( !role.equals(DEFAULT_ROLE)) attributeList.addAttribute("role",role);
        xmlOutput.openTag(ELEMENT_NAME,attributeList);
        this.getSourceLines().writeXML(xmlOutput,addMessages,false);
        if (addMessages) {
            xmlOutput.openTag(edu.umd.cs.findbugs.BugAnnotation.MESSAGE_TAG);
            xmlOutput.writeText(this.toString());
            xmlOutput.closeTag(edu.umd.cs.findbugs.BugAnnotation.MESSAGE_TAG);
        }
        xmlOutput.closeTag(ELEMENT_NAME);
    }
}
// vim:ts=4
