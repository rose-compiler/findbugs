/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * The User Interface for creating a Project and editing it after the fact.
 *
 * @author Reuven
 *
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.Plugin;
import edu.umd.cs.findbugs.Project;
import edu.umd.cs.findbugs.cloud.CloudPlugin;
import edu.umd.cs.findbugs.util.LaunchBrowser;
import edu.umd.cs.findbugs.util.Util;
public class NewProjectWizard extends edu.umd.cs.findbugs.gui2.FBDialog {
    static class CloudComboBoxRenderer extends javax.swing.plaf.basic.BasicComboBoxRenderer {
        public CloudComboBoxRenderer() {
        }
        public java.awt.Component getListCellRendererComponent(javax.swing.JList list, java.lang.Object value, int index, boolean isSelected, boolean cellHasFocus) {
            edu.umd.cs.findbugs.cloud.CloudPlugin plugin = (edu.umd.cs.findbugs.cloud.CloudPlugin) (value) ;
            if (isSelected) {
                this.setBackground(list.getSelectionBackground());
                this.setForeground(list.getSelectionForeground());
                if ( -1 < index) {
                    if (plugin == null) list.setToolTipText("No cloud plugin specified by project");
                    else list.setToolTipText(plugin.getDetails());
                }
            }
            else {
                this.setBackground(list.getBackground());
                this.setForeground(list.getForeground());
            }
            this.setFont(list.getFont());
            this.setText((value == null) ? "<default>" : plugin.getDescription());
            return this;
        }
    }
    final private javax.swing.border.EmptyBorder border = new javax.swing.border.EmptyBorder(5, 5, 5, 5);
    private edu.umd.cs.findbugs.Project project;
    private boolean projectChanged = false;
    private boolean projectNameChanged = false;
    final private edu.umd.cs.findbugs.gui2.FBFileChooser chooser = new edu.umd.cs.findbugs.gui2.FBFileChooser();
    final private javax.swing.filechooser.FileFilter directoryOrArchive = new javax.swing.filechooser.FileFilter() {
        public boolean accept(java.io.File f) {
            java.lang.String fileName = f.getName().toLowerCase();
            return f.isDirectory() || fileName.endsWith(".jar") || fileName.endsWith(".ear") || fileName.endsWith(".war") || fileName.endsWith(".zip") || fileName.endsWith(".sar") || fileName.endsWith(".class");
        }
        public java.lang.String getDescription() {
            return edu.umd.cs.findbugs.L10N.getLocalString("file.accepted_extensions","Class archive files (*.class, *.[jwes]ar, *.zip)");
        }
    };
    final private javax.swing.JList analyzeList = new javax.swing.JList();
    final private javax.swing.DefaultListModel analyzeModel = new javax.swing.DefaultListModel();
    final private javax.swing.JTextField projectName = new javax.swing.JTextField();
    final private javax.swing.JList auxList = new javax.swing.JList();
    final private javax.swing.DefaultListModel auxModel = new javax.swing.DefaultListModel();
    final private javax.swing.JList sourceList = new javax.swing.JList();
    final private javax.swing.DefaultListModel sourceModel = new javax.swing.DefaultListModel();
    final private javax.swing.JButton finishButton = new javax.swing.JButton();
    final private javax.swing.JButton cancelButton = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.cancel_btn","Cancel"));
    final private javax.swing.JComboBox cloudSelector = new javax.swing.JComboBox();
    final private javax.swing.JComponent[] wizardComponents = new javax.swing.JComponent[4];
    private int currentPanel;
    private boolean isNewProject;
    public NewProjectWizard() {
        this(null);
        finishButton.setEnabled(false);
    }
/**
     * @param curProject
     *            the project to populate from, or null to start a new one
     */
    public NewProjectWizard(edu.umd.cs.findbugs.Project curProject) {
        super();
        project = curProject;
        if (project == null) {
            edu.umd.cs.findbugs.gui2.ProjectSettings.newInstance();
            project = new edu.umd.cs.findbugs.Project();
            isNewProject = true;
        }
        boolean temp = false;
        if (curProject == null) this.setTitle(edu.umd.cs.findbugs.L10N.getLocalString("dlg.new_item","New Project"));
        else {
            this.setTitle(edu.umd.cs.findbugs.L10N.getLocalString("dlg.reconfig","Reconfigure"));
            temp = true;
        }
        final boolean reconfig = temp;
        javax.swing.JPanel mainPanel = new javax.swing.JPanel();
        mainPanel.setBorder(new javax.swing.border.EmptyBorder(5, 5, 5, 5));
        mainPanel.setLayout(new javax.swing.BoxLayout(mainPanel, javax.swing.BoxLayout.Y_AXIS));
        wizardComponents[0] = this.createFilePanel(edu.umd.cs.findbugs.L10N.getLocalString("dlg.class_jars_dirs_lbl","Class archives and directories to analyze:"),analyzeList,analyzeModel,javax.swing.JFileChooser.FILES_AND_DIRECTORIES,directoryOrArchive,"Choose Class Archives and Directories to Analyze",false,"http://findbugs.sourceforge.net/manual/gui.html#d0e1087");
        wizardComponents[1] = this.createFilePanel(edu.umd.cs.findbugs.L10N.getLocalString("dlg.aux_class_lbl","Auxiliary class locations:"),auxList,auxModel,javax.swing.JFileChooser.FILES_AND_DIRECTORIES,directoryOrArchive,"Choose Auxilliary Class Archives and Directories",false,"http://findbugs.sourceforge.net/FAQ.html#q4");
        wizardComponents[2] = this.createFilePanel(edu.umd.cs.findbugs.L10N.getLocalString("dlg.source_dirs_lbl","Source directories:"),sourceList,sourceModel,javax.swing.JFileChooser.FILES_AND_DIRECTORIES,null,"Choose Source Directories",true,"http://findbugs.sourceforge.net/manual/gui.html#d0e1087");
        javax.swing.JPanel cloudPanel = new javax.swing.JPanel(new java.awt.BorderLayout());
        cloudPanel.add(new javax.swing.JLabel("Store bug reviews in:"),java.awt.BorderLayout.NORTH);
        cloudPanel.add(cloudSelector,java.awt.BorderLayout.CENTER);
        wizardComponents[3] = cloudPanel;
        cloudSelector.setRenderer(new edu.umd.cs.findbugs.gui2.NewProjectWizard.CloudComboBoxRenderer());
        cloudSelector.addItem(null);
        java.lang.String cloudId = project.getCloudId();
        for (edu.umd.cs.findbugs.cloud.CloudPlugin c : edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getRegisteredClouds().values()){
            java.lang.String fbid = c.getFindbugsPluginId();
            edu.umd.cs.findbugs.Plugin plugin = edu.umd.cs.findbugs.Plugin.getByPluginId(fbid);
            if (plugin == null) continue;
            java.lang.Boolean fbPluginStatus = project.getPluginStatus(plugin);
            if (( !c.isHidden() || c.getId().equals(cloudId)) &&  !java.lang.Boolean.FALSE.equals(fbPluginStatus)) cloudSelector.addItem(c);
        }
;
        if (cloudId != null) {
            edu.umd.cs.findbugs.cloud.CloudPlugin c = edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getRegisteredClouds().get(project.getCloudId());
            cloudSelector.setSelectedItem(c);
        }
        javax.swing.JPanel buttons = new javax.swing.JPanel();
        buttons.setLayout(new javax.swing.BoxLayout(buttons, javax.swing.BoxLayout.X_AXIS));
        if (edu.umd.cs.findbugs.gui2.MainFrameHelper.isMacLookAndFeel()) {
            buttons.add(javax.swing.Box.createHorizontalStrut(5));
            buttons.add(cancelButton);
            buttons.add(javax.swing.Box.createHorizontalStrut(5));
            buttons.add(finishButton);
        }
        else {
            buttons.add(javax.swing.Box.createHorizontalStrut(5));
            buttons.add(finishButton);
            buttons.add(javax.swing.Box.createHorizontalStrut(5));
            buttons.add(cancelButton);
        }
        finishButton.addActionListener(new java.awt.event.ActionListener() {
            boolean keepGoing = false;
            private boolean displayWarningAndAskIfWeShouldContinue(java.lang.String msg, java.lang.String title) {
                if (keepGoing) return true;
                boolean result = javax.swing.JOptionPane.showConfirmDialog(edu.umd.cs.findbugs.gui2.NewProjectWizard.this,msg,title,javax.swing.JOptionPane.OK_CANCEL_OPTION,javax.swing.JOptionPane.WARNING_MESSAGE) == javax.swing.JOptionPane.OK_OPTION;
                if (result) keepGoing = true;
                return result;
            }
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (this.displayWarnings()) return;
                edu.umd.cs.findbugs.Project p;
                java.lang.String oldCloudId = null;
                p = project;
                oldCloudId = project.getCloudId();
                p.setGuiCallback(edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getGuiCallback());
                edu.umd.cs.findbugs.gui2.NewProjectWizard.this.clearProjectSettings(p);
                for (int i = 0; i < analyzeModel.getSize(); i++) p.addFile((java.lang.String) (analyzeModel.get(i)) );
                for (int i = 0; i < auxModel.getSize(); i++) p.addAuxClasspathEntry((java.lang.String) (auxModel.get(i)) );
                for (int i = 0; i < sourceModel.getSize(); i++) p.addSourceDir((java.lang.String) (sourceModel.get(i)) );
                p.setProjectName(projectName.getText());
                edu.umd.cs.findbugs.cloud.CloudPlugin cloudPlugin = (edu.umd.cs.findbugs.cloud.CloudPlugin) (cloudSelector.getSelectedItem()) ;
                java.lang.String newCloudId;
                if (cloudPlugin == null || cloudSelector.getSelectedIndex() == 0) {
                    newCloudId = null;
                }
                else {
                    newCloudId = cloudPlugin.getId();
                }
                p.setCloudId(newCloudId);
                edu.umd.cs.findbugs.gui2.MainFrame mainFrame = edu.umd.cs.findbugs.gui2.MainFrame.getInstance();
                if (keepGoing) {
                    mainFrame.setProject(p);
                }
                if (projectChanged && (isNewProject || javax.swing.JOptionPane.showConfirmDialog(edu.umd.cs.findbugs.gui2.NewProjectWizard.this,edu.umd.cs.findbugs.L10N.getLocalString("dlg.project_settings_changed_lbl","Project settings have been changed.  Perform a new analysis with the changed files?"),edu.umd.cs.findbugs.L10N.getLocalString("dlg.redo_analysis_question_lbl","Redo analysis?"),javax.swing.JOptionPane.YES_NO_OPTION) == javax.swing.JOptionPane.YES_OPTION)) {
                    new edu.umd.cs.findbugs.gui2.AnalyzingDialog(p);
                }
                else if ( !edu.umd.cs.findbugs.util.Util.nullSafeEquals(newCloudId,oldCloudId)) {
                    edu.umd.cs.findbugs.BugCollection bugs = mainFrame.getBugCollection();
                    try {
                        bugs.reinitializeCloud();
                        mainFrame.getComments().updateCloud();
                    }
                    catch (java.lang.Exception e){
                        javax.swing.JOptionPane.showMessageDialog(edu.umd.cs.findbugs.gui2.NewProjectWizard.this,"Error loading " + newCloudId + "\n\n" + e.getClass().getSimpleName() + ": " + e.getMessage(),"FindBugs Cloud Error",javax.swing.JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    mainFrame.getComments().updateCommentsFromLeafInformation(mainFrame.getCurrentSelectedBugLeaf());
                }
                if (reconfig) mainFrame.setProjectChanged(true);
                java.lang.String name = p.getProjectName();
                if (name == null) {
                    name = edu.umd.cs.findbugs.Project.UNNAMED_PROJECT;
                    edu.umd.cs.findbugs.gui2.Debug.println("PROJECT NAME IS NULL!!");
                }
                if (projectNameChanged) {
                    mainFrame.updateTitle();
                }
                edu.umd.cs.findbugs.gui2.NewProjectWizard.this.dispose();
            }
            private boolean displayWarnings() {
                for (int i = 0; i < analyzeModel.getSize(); i++) {
                    java.io.File temp = new java.io.File((java.lang.String) (analyzeModel.get(i)) );
                    if ( !temp.exists() && directoryOrArchive.accept(temp)) {
                        if ( !this.displayWarningAndAskIfWeShouldContinue(temp.getName() + " " + edu.umd.cs.findbugs.L10N.getLocalString("dlg.invalid_txt"," is invalid."),edu.umd.cs.findbugs.L10N.getLocalString("dlg.error_ttl","Can't locate file"))) return true;
                    }
                }
                for (int i = 0; i < sourceModel.getSize(); i++) {
                    java.io.File temp = new java.io.File((java.lang.String) (sourceModel.get(i)) );
                    if ( !temp.exists() && directoryOrArchive.accept(temp)) {
                        if ( !this.displayWarningAndAskIfWeShouldContinue(temp.getName() + " " + edu.umd.cs.findbugs.L10N.getLocalString("dlg.invalid_txt"," is invalid."),edu.umd.cs.findbugs.L10N.getLocalString("dlg.error_ttl","Can't locate file"))) return true;
                    }
                }
                for (int i = 0; i < auxModel.getSize(); i++) {
                    java.io.File temp = new java.io.File((java.lang.String) (auxModel.get(i)) );
                    if ( !temp.exists() && directoryOrArchive.accept(temp)) {
                        if ( !this.displayWarningAndAskIfWeShouldContinue(temp.getName() + " " + edu.umd.cs.findbugs.L10N.getLocalString("dlg.invalid_txt"," is invalid."),edu.umd.cs.findbugs.L10N.getLocalString("dlg.error_ttl","Can't locate file"))) return true;
                    }
                }
                return false;
            }
        });
// Now that p is cleared, we can add in all the correct files.
        if (curProject == null) finishButton.setText(edu.umd.cs.findbugs.L10N.getLocalString("dlg.analyze_btn","Analyze"));
        else finishButton.setText(edu.umd.cs.findbugs.L10N.getLocalString("dlg.ok_btn","OK"));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edu.umd.cs.findbugs.gui2.NewProjectWizard.this.dispose();
            }
        });
        javax.swing.JPanel south = new javax.swing.JPanel(new java.awt.BorderLayout());
        south.setBorder(new javax.swing.border.EmptyBorder(5, 5, 5, 5));
        south.add(new javax.swing.JSeparator(),java.awt.BorderLayout.NORTH);
        south.add(buttons,java.awt.BorderLayout.EAST);
        if (curProject != null) {
            for (java.lang.String i : curProject.getFileList())analyzeModel.addElement(i);
;
// If the project had no classes in it, disable the finish button
// until classes are added.
// if (curProject.getFileList().size()==0)
// this.finishButton.setEnabled(false);
            for (java.lang.String i : curProject.getAuxClasspathEntryList())auxModel.addElement(i);
;
            for (java.lang.String i : curProject.getSourceDirList())sourceModel.addElement(i);
;
            projectName.setText(curProject.getProjectName());
            projectName.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent e) {
                    projectNameChanged = true;
                }
            });
        }
        else {
            finishButton.setEnabled(false);
        }
        this.loadAllPanels(mainPanel);
        this.add(this.createTextFieldPanel("Project name",projectName),java.awt.BorderLayout.NORTH);
        this.add(mainPanel,java.awt.BorderLayout.CENTER);
        this.add(south,java.awt.BorderLayout.SOUTH);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setModal(true);
        this.setVisible(true);
    }
// loadPanel(0);
// pack();
    private void clearProjectSettings(edu.umd.cs.findbugs.Project p) {
// First clear p's old files, otherwise we can't remove a file
// once an analysis has been performed on it
        int numOldFiles = p.getFileCount();
        for (int x = 0; x < numOldFiles; x++) p.removeFile(0);
        int numOldAuxFiles = p.getNumAuxClasspathEntries();
        for (int x = 0; x < numOldAuxFiles; x++) p.removeAuxClasspathEntry(0);
        int numOldSrc = p.getNumSourceDirs();
        for (int x = 0; x < numOldSrc; x++) p.removeSourceDir(0);
    }
    private javax.swing.JComponent createTextFieldPanel(java.lang.String label, javax.swing.JTextField textField) {
        javax.swing.JPanel myPanel = new javax.swing.JPanel(new java.awt.BorderLayout());
        myPanel.add(new javax.swing.JLabel(label),java.awt.BorderLayout.NORTH);
        myPanel.add(textField,java.awt.BorderLayout.CENTER);
        myPanel.setBorder(new javax.swing.border.EmptyBorder(5, 5, 5, 5));
        return myPanel;
    }
    private javax.swing.JPanel createFilePanel(final java.lang.String label, final javax.swing.JList list, final javax.swing.DefaultListModel listModel, final int fileSelectionMode, final javax.swing.filechooser.FileFilter filter, final java.lang.String dialogTitle, boolean wizard, final java.lang.String helpUrl) {
        javax.swing.JPanel myPanel = new javax.swing.JPanel(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gbc = new java.awt.GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 0;
        gbc.anchor = java.awt.GridBagConstraints.WEST;
        myPanel.add(new javax.swing.JLabel(label),gbc);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        gbc.weighty = 0;
        gbc.anchor = java.awt.GridBagConstraints.WEST;
        javax.swing.JButton button = new javax.swing.JButton("<HTML><U>Help");
        button.setFont(button.getFont().deriveFont(java.awt.Font.PLAIN));
        button.setForeground(java.awt.Color.BLUE);
        button.setBorderPainted(false);
        button.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.HAND_CURSOR));
        button.setContentAreaFilled(false);
        button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                try {
                    edu.umd.cs.findbugs.util.LaunchBrowser.showDocument(new java.net.URL(helpUrl));
                }
                catch (java.net.MalformedURLException e1){
                    throw new java.lang.IllegalStateException(e1);
                }
            }
        });
        myPanel.add(button,gbc);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridheight = 3;
        gbc.gridwidth = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        myPanel.add(new javax.swing.JScrollPane(list),gbc);
        list.setModel(listModel);
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        gbc.weighty = 0;
        gbc.fill = java.awt.GridBagConstraints.HORIZONTAL;
        final javax.swing.JButton addButton = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.add_btn","Add"));
        myPanel.add(addButton,gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.insets = new java.awt.Insets(5, 0, 0, 0);
        final javax.swing.JButton removeButton = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.remove_btn","Remove"));
        myPanel.add(removeButton,gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        final javax.swing.JButton wizardButton = new javax.swing.JButton("Wizard");
        if (wizard) {
            final edu.umd.cs.findbugs.gui2.NewProjectWizard thisGUI = this;
            myPanel.add(wizardButton,gbc);
            wizardButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    final edu.umd.cs.findbugs.Project tempProject = new edu.umd.cs.findbugs.Project();
                    for (int i = 0; i < analyzeModel.getSize(); i++) tempProject.addFile((java.lang.String) (analyzeModel.get(i)) );
                    for (int i = 0; i < auxModel.getSize(); i++) tempProject.addAuxClasspathEntry((java.lang.String) (auxModel.get(i)) );
                    java.awt.EventQueue.invokeLater(new java.lang.Runnable() {
                        public void run() {
                            final edu.umd.cs.findbugs.gui2.SourceDirectoryWizard dialog = new edu.umd.cs.findbugs.gui2.SourceDirectoryWizard(new javax.swing.JFrame(), true, tempProject, thisGUI);
                            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                                public void windowClosing(java.awt.event.WindowEvent e) {
                                    if (dialog.discover != null && dialog.discover.isAlive()) dialog.discover.interrupt();
                                }
                            });
                            dialog.setVisible(true);
                        }
                    });
                }
            });
        }
        gbc.insets = new java.awt.Insets(0, 0, 0, 0);
        myPanel.add(javax.swing.Box.createGlue(),gbc);
        myPanel.setBorder(border);
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooser.setFileSelectionMode(fileSelectionMode);
                chooser.setMultiSelectionEnabled(true);
                chooser.setApproveButtonText("Choose");
                chooser.setDialogTitle(dialogTitle);
                for (javax.swing.filechooser.FileFilter ff : chooser.getChoosableFileFilters()){
                    chooser.removeChoosableFileFilter(ff);
                }
;
                chooser.setFileFilter(filter);
                if (chooser.showOpenDialog(edu.umd.cs.findbugs.gui2.NewProjectWizard.this) == javax.swing.JFileChooser.APPROVE_OPTION) {
                    java.io.File[] selectedFiles = chooser.getSelectedFiles();
                    for (java.io.File selectedFile : selectedFiles){
                        listModel.addElement(selectedFile.getAbsolutePath());
                    }
;
                    projectChanged = true;
                    if (label.equals(edu.umd.cs.findbugs.L10N.getLocalString("dlg.class_jars_dirs_lbl","Class archives and directories to analyze:"))) finishButton.setEnabled(true);
                }
            }
        });
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (list.getSelectedValues().length > 0) projectChanged = true;
                for (java.lang.Object i : list.getSelectedValues())listModel.removeElement(i);
;
            }
        });
// Removes all the file filters currently in the chooser.
// If this is the primary class directories add button, set
// it to enable the finish button of the main dialog
// If this is the primary class directories remove button, set
// it to disable finish when there are no class files being
// analyzed
// if (listModel.size()==0 &&
// label.equals(edu.umd.cs.findbugs.L10N.getLocalString("dlg.class_jars_dirs_lbl",
// "Class archives and directories to analyze:")))
// finishButton.setEnabled(false);
        return myPanel;
    }
/*
     * private void loadPanel(final int index) { SwingUtilities.invokeLater(new
     * Runnable() { public void run() { remove(wizardPanels[currentPanel]);
     * currentPanel = index; add(wizardPanels[index], BorderLayout.CENTER);
     * backButton.setEnabled(index > 0); nextButton.setEnabled(index <
     * wizardPanels.length - 1); validate(); repaint(); } }); }
     */
    private void loadAllPanels(final javax.swing.JPanel mainPanel) {
        javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
            public void run() {
                int numPanels = wizardComponents.length;
                for (int i = 0; i < numPanels; i++) mainPanel.remove(wizardComponents[i]);
                for (int i = 0; i < numPanels; i++) mainPanel.add(wizardComponents[i]);
                edu.umd.cs.findbugs.gui2.NewProjectWizard.this.validate();
                edu.umd.cs.findbugs.gui2.NewProjectWizard.this.repaint();
            }
        });
    }
    public void addNotify() {
        super.addNotify();
        for (javax.swing.JComponent component : wizardComponents){
            this.setFontSizeHelper(component.getComponents(),edu.umd.cs.findbugs.gui2.Driver.getFontSize());
        }
;
        this.pack();
        int width = super.getWidth();
        if (width < 600) width = 600;
        this.setSize(new java.awt.Dimension(width, 500));
        this.setLocationRelativeTo(edu.umd.cs.findbugs.gui2.MainFrame.getInstance());
    }
/**
     * @param foundModel
     */
    public void setSourceDirecs(javax.swing.DefaultListModel foundModel) {
        for (int i = 0; i < foundModel.size(); i++) {
            this.sourceModel.addElement(foundModel.getElementAt(i));
        }
    }
}
