/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Graph of inheritance relationships.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.ch;
import edu.umd.cs.findbugs.ba.ch.*;
import edu.umd.cs.findbugs.graph.AbstractGraph;
public class InheritanceGraph extends edu.umd.cs.findbugs.graph.AbstractGraph<edu.umd.cs.findbugs.ba.ch.InheritanceEdge, edu.umd.cs.findbugs.ba.ch.ClassVertex> {
    public InheritanceGraph() {
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.graph.AbstractGraph#allocateEdge(edu.umd.cs.findbugs
     * .graph.AbstractVertex, edu.umd.cs.findbugs.graph.AbstractVertex)
     */
    protected edu.umd.cs.findbugs.ba.ch.InheritanceEdge allocateEdge(edu.umd.cs.findbugs.ba.ch.ClassVertex source, edu.umd.cs.findbugs.ba.ch.ClassVertex target) {
        return new edu.umd.cs.findbugs.ba.ch.InheritanceEdge(source, target);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.graph.AbstractGraph#createEdge(edu.umd.cs.findbugs
     * .graph.AbstractVertex, edu.umd.cs.findbugs.graph.AbstractVertex)
     */
    public edu.umd.cs.findbugs.ba.ch.InheritanceEdge createEdge(edu.umd.cs.findbugs.ba.ch.ClassVertex source, edu.umd.cs.findbugs.ba.ch.ClassVertex target) {
        if ( !target.isInterface()) {
            source.setDirectSuperclass(target);
        }
        return super.createEdge(source,target);
    }
}
