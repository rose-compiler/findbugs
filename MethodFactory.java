/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Method analysis engine to produce BCEL Method objects.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class MethodFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<org.apache.bcel.classfile.Method> {
    public MethodFactory() {
        super("Method factory",org.apache.bcel.classfile.Method.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public org.apache.bcel.classfile.Method analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.classfile.JavaClass jclass = analysisCache.getClassAnalysis(org.apache.bcel.classfile.JavaClass.class,descriptor.getClassDescriptor());
        org.apache.bcel.classfile.Method[] methodList = jclass.getMethods();
        org.apache.bcel.classfile.Method result = null;
// As a side-effect, cache all of the Methods for this JavaClass
        for (org.apache.bcel.classfile.Method method : methodList){
            edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor = edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getMethodDescriptor(descriptor.getSlashedClassName(),method.getName(),method.getSignature(),method.isStatic());
            analysisCache.eagerlyPutMethodAnalysis(org.apache.bcel.classfile.Method.class,methodDescriptor,method);
// Put in cache eagerly
            if (methodDescriptor.equals(descriptor)) {
                result = method;
            }
        }
;
        return result;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#registerWith(edu.umd.cs
     * .findbugs.classfile.IAnalysisCache)
     */
    public void registerWith(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache) {
        analysisCache.registerMethodAnalysisEngine(org.apache.bcel.classfile.Method.class,this);
    }
}
