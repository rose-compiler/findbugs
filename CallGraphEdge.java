/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.graph.AbstractEdge;
public class CallGraphEdge extends edu.umd.cs.findbugs.graph.AbstractEdge<edu.umd.cs.findbugs.CallGraphEdge, edu.umd.cs.findbugs.CallGraphNode> {
    private edu.umd.cs.findbugs.CallSite callSite;
    public CallGraphEdge(edu.umd.cs.findbugs.CallGraphNode source, edu.umd.cs.findbugs.CallGraphNode target) {
        super(source,target);
    }
    void setCallSite(edu.umd.cs.findbugs.CallSite callSite) {
        this.callSite = callSite;
    }
    public edu.umd.cs.findbugs.CallSite getCallSite() {
        return callSite;
    }
    public org.apache.bcel.classfile.Method getCallingMethod() {
        return this.getSource().getMethod();
    }
    public org.apache.bcel.classfile.Method getCalledMethod() {
        return this.getTarget().getMethod();
    }
}
// vim:ts=4
