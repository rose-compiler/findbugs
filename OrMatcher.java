/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import java.io.IOException;
import java.util.Iterator;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class OrMatcher extends edu.umd.cs.findbugs.filter.CompoundMatcher {
    public OrMatcher() {
    }
    public boolean match(edu.umd.cs.findbugs.BugInstance bugInstance) {
        java.util.Iterator<edu.umd.cs.findbugs.filter.Matcher> i = this.childIterator();
        while (i.hasNext()) {
            edu.umd.cs.findbugs.filter.Matcher child = i.next();
            if (child.match(bugInstance)) return true;
        }
        return false;
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean disabled) throws java.io.IOException {
        if (this.numberChildren() == 1) {
            this.childIterator().next().writeXML(xmlOutput,false);
            return;
        }
        xmlOutput.startTag("Or");
        if (disabled) xmlOutput.addAttribute("disabled","true");
        xmlOutput.stopTag(false);
        this.writeChildrenXML(xmlOutput);
        xmlOutput.closeTag("Or");
    }
    public java.lang.String toString() {
        if (this.numberChildren() == 1) return super.toString();
        return "Or(" + super.toString() + ")";
    }
}
// vim:ts=4
