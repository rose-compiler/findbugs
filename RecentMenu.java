/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author Dan
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JMenu;
public class RecentMenu extends java.lang.Object {
    private static class LimitedArrayList<T> extends java.util.ArrayList<T> {
        final public static int MAX_ENTRIES = 5;
        public LimitedArrayList() {
            super(MAX_ENTRIES);
        }
        public boolean add(T element) {
            if ( !this.contains(element)) {
                super.add(0,element);
                if (this.size() > MAX_ENTRIES) {
                    this.remove(MAX_ENTRIES);
                }
            }
            else {
                this.remove(element);
                super.add(0,element);
            }
            return true;
        }
    }
// Originally called recentProjects
    edu.umd.cs.findbugs.gui2.RecentMenu.LimitedArrayList<java.io.File> recentFiles;
// before merge two lists into one.
    javax.swing.JMenu recentMenu;
    public RecentMenu(javax.swing.JMenu menu) {
        super();
        recentFiles = new edu.umd.cs.findbugs.gui2.RecentMenu.LimitedArrayList<java.io.File>();
        recentMenu = menu;
        for (java.io.File f : edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getRecentFiles()){
            recentFiles.add(f);
        }
;
        this.makeRecentMenu();
    }
    public void makeRecentMenu() {
        recentMenu.removeAll();
        for (java.io.File f : recentFiles){
            edu.umd.cs.findbugs.gui2.Debug.println(f);
            if ( !f.exists()) {
                if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.err.println("a recent project was not found, removing it from menu");
                continue;
            }
            recentMenu.add(edu.umd.cs.findbugs.gui2.MainFrame.getInstance().createRecentItem(f,edu.umd.cs.findbugs.gui2.SaveType.forFile(f)));
        }
;
    }
/**
     * Adds a file to the list of recent files used.
     * 
     * @param f
     */
    public void addRecentFile(final java.io.File f) {
        if (f != null) recentFiles.add(f);
        this.makeRecentMenu();
    }
}
