/**
 * Warning property for FindDeadLocalStores.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import edu.umd.cs.findbugs.props.AbstractWarningProperty;
import edu.umd.cs.findbugs.props.PriorityAdjustment;
public class DeadLocalStoreProperty extends edu.umd.cs.findbugs.props.AbstractWarningProperty {
    public DeadLocalStoreProperty(java.lang.String name, edu.umd.cs.findbugs.props.PriorityAdjustment priorityAdjustment) {
        super(name,priorityAdjustment);
    }
/** A store in a JSP page */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty IN_JSP_PAGE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("IN_JSP_PAGE", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** A store in non Java page */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty NOT_JAVA = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("IN_JSP_PAGE", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_LOW);
/**
     * A store that seemed to have been cloned (an inlined finally block or JSR?
     */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty CLONED_STORE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("CLONED_STORE", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** Store is killed by a subsequent store. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty KILLED_BY_SUBSEQUENT_STORE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("KILLED_BY_SUBSEQUENT_STORE", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
/** Store of database operation */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty STORE_OF_DATABASE_VALUE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("STORE_OF_DATABASE_VALUE", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY_TO_AT_LEAST_NORMAL);
/** Dead store is of a defense programming constant value. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty DEFENSIVE_CONSTANT_OPCODE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("DEFENSIVE_CONSTANT_OPCODE", edu.umd.cs.findbugs.props.PriorityAdjustment.A_LITTLE_BIT_LOWER_PRIORITY);
/** Dead store is likely to be the exception object in an exception handler. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty EXCEPTION_HANDLER = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("EXCEPTION_HANDLER", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** The dead store is an increment. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty DEAD_INCREMENT = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("DEAD_INCREMENT", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty DEAD_INCREMENT_IN_MAIN = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("DEAD_INCREMENT", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_LOW);
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty METHOD_RESULT = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("METHOD_RESULT", edu.umd.cs.findbugs.props.PriorityAdjustment.A_LITTLE_BIT_HIGHER_PRIORITY);
/** The dead store is an increment: the only one in the method. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty SINGLE_DEAD_INCREMENT = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("SINGLE_DEAD_INCREMENT", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
/** Dead store is of a newly allocated object. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty DEAD_OBJECT_STORE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("DEAD_OBJECT_STORE", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
/** Method contains two stores and multiple loads of this local. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty TWO_STORES_MULTIPLE_LOADS = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("TWO_STORES_MULTIPLE_LOADS", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** There is only one store of this local. (Maybe it's final?) */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty SINGLE_STORE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("SINGLE_STORE", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** There is a dup immediately before the store; perhaps a ++ operation */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty DUP_THEN_STORE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("DUP_THEN_STORE", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** There are no loads of this local. (Maybe it's final?). */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty NO_LOADS = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("NO_LOADS", edu.umd.cs.findbugs.props.PriorityAdjustment.A_LITTLE_BIT_LOWER_PRIORITY);
/** Variable shadows a fields with the same name */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty SHADOWS_FIELD = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("SHADOWS_FIELD", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty SYNTHETIC_NAME = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("SYNTHETIC_NAME", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_LOW);
/** This local is a parameter which is dead on entry to the method. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty PARAM_DEAD_ON_ENTRY = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("PARAM_DEAD_ON_ENTRY", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY_TO_HIGH);
/** Name of the local variable. */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty LOCAL_NAME = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("LOCAL_NAME", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** Caching value */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty CACHING_VALUE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("CACHING_VALUE", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
/** copy value */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty COPY_VALUE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("COPY_VALUE", edu.umd.cs.findbugs.props.PriorityAdjustment.A_LITTLE_BIT_LOWER_PRIORITY);
/** primitive or string */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty BASE_VALUE = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("BASE_VALUE", edu.umd.cs.findbugs.props.PriorityAdjustment.A_LITTLE_BIT_LOWER_PRIORITY);
/** many stores */
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty MANY_STORES = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("MANY_STORES", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty STORE_OF_NULL = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("STORE_OF_NULL", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_LOW);
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty STORE_OF_CONSTANT = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("STORE_OF_CONSTANT", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
    final public static edu.umd.cs.findbugs.detect.DeadLocalStoreProperty IS_PARAMETER = new edu.umd.cs.findbugs.detect.DeadLocalStoreProperty("IS_PARAMETER", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
}
