/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004-2006 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.IFNONNULL;
import org.apache.bcel.generic.IFNULL;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.POP;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.BugAccumulator;
import edu.umd.cs.findbugs.BugAnnotation;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.Priorities;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.SystemProperties;
import edu.umd.cs.findbugs.TypeAnnotation;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.Dataflow;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.LiveLocalStoreAnalysis;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.ba.XField;
import edu.umd.cs.findbugs.ba.ch.Subtypes2;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.ba.vna.ValueNumberFrame;
import edu.umd.cs.findbugs.ba.vna.ValueNumberSourceInfo;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
import edu.umd.cs.findbugs.internalAnnotations.SlashedClassName;
public class DontIgnoreResultOfPutIfAbsent extends java.lang.Object implements edu.umd.cs.findbugs.Detector {
    final static boolean countOtherCalls = false;
    final edu.umd.cs.findbugs.BugReporter bugReporter;
    final edu.umd.cs.findbugs.BugAccumulator accumulator;
    final edu.umd.cs.findbugs.classfile.ClassDescriptor concurrentMapDescriptor = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(java.util.concurrent.ConcurrentMap.class);
    public DontIgnoreResultOfPutIfAbsent(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
        this.accumulator = new edu.umd.cs.findbugs.BugAccumulator(bugReporter);
    }
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.Detector#report()
     */
    public void report() {
    }
// TODO Auto-generated method stub
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.JavaClass javaClass = classContext.getJavaClass();
        org.apache.bcel.classfile.ConstantPool pool = javaClass.getConstantPool();
        boolean found = false;
        for (org.apache.bcel.classfile.Constant constantEntry : pool.getConstantPool())if (constantEntry instanceof org.apache.bcel.classfile.ConstantNameAndType) {
            org.apache.bcel.classfile.ConstantNameAndType nt = (org.apache.bcel.classfile.ConstantNameAndType) (constantEntry) ;
            if (nt.getName(pool).equals("putIfAbsent")) {
                found = true;
                break;
            }
        }
;
        if ( !found) return;
        org.apache.bcel.classfile.Method[] methodList = javaClass.getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
            if (methodGen == null) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("Error analyzing " + method.toString(),e);
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("Error analyzing " + method.toString(),e);
            }
        }
;
    }
    final static boolean DEBUG = false;
    static java.util.HashSet<java.lang.String> immutableClassNames = new java.util.HashSet<java.lang.String>();
    static {
        immutableClassNames.add("java/lang/Integer");
        immutableClassNames.add("java/lang/Long");
        immutableClassNames.add("java/lang/String");
        immutableClassNames.add("java/util/Comparator");
    }
    private static int getPriorityForBeingMutable(org.apache.bcel.generic.Type type) {
        if (type instanceof org.apache.bcel.generic.ArrayType) {
            return HIGH_PRIORITY;
        }
        else if (type instanceof org.apache.bcel.generic.ObjectType) {
            edu.umd.cs.findbugs.detect.UnreadFieldsData unreadFields = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getUnreadFieldsData();
            edu.umd.cs.findbugs.classfile.ClassDescriptor cd = edu.umd.cs.findbugs.classfile.DescriptorFactory.getClassDescriptor((org.apache.bcel.generic.ObjectType) (type) );
            java.lang.String className = cd.getClassName();
            if (immutableClassNames.contains(className)) return edu.umd.cs.findbugs.Priorities.LOW_PRIORITY;
            edu.umd.cs.findbugs.ba.XClass xClass = edu.umd.cs.findbugs.ba.AnalysisContext.currentXFactory().getXClass(cd);
            if (xClass == null) return edu.umd.cs.findbugs.Priorities.IGNORE_PRIORITY;
            edu.umd.cs.findbugs.classfile.ClassDescriptor superclassDescriptor = xClass.getSuperclassDescriptor();
            if (superclassDescriptor != null) {
                java.lang.String superClassName = superclassDescriptor.getClassName();
                if (superClassName.equals("java/lang/Enum")) return edu.umd.cs.findbugs.Priorities.LOW_PRIORITY;
            }
            boolean hasMutableField = false;
            boolean hasUpdates = false;
            for (edu.umd.cs.findbugs.ba.XField f : xClass.getXFields())if ( !f.isStatic()) {
                if ( !f.isFinal() &&  !f.isSynthetic()) {
                    hasMutableField = true;
                    if (unreadFields.isWrittenOutsideOfInitialization(f)) hasUpdates = true;
                }
                java.lang.String signature = f.getSignature();
                if (signature.startsWith("Ljava/util/concurrent") || signature.startsWith("Ljava/lang/StringB") || signature.charAt(0) == '\u005b' || signature.indexOf("Map") >= 0 || signature.indexOf("List") >= 0 || signature.indexOf("Set") >= 0) hasMutableField = hasUpdates = true;
            }
;
            if ( !hasMutableField &&  !xClass.isInterface() &&  !xClass.isAbstract()) return edu.umd.cs.findbugs.Priorities.LOW_PRIORITY;
            if (hasUpdates || className.startsWith("java/util") || className.indexOf("Map") >= 0 || className.indexOf("List") >= 0) return edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY;
            return edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY;
        }
        else return edu.umd.cs.findbugs.Priorities.IGNORE_PRIORITY;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.CFGBuilderException, edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        if (method.isSynthetic() || (method.getAccessFlags() & org.apache.bcel.Constants.ACC_BRIDGE) == org.apache.bcel.Constants.ACC_BRIDGE) return;
        if (DEBUG) {
            java.lang.System.out.println("    Analyzing method " + classContext.getJavaClass().getClassName() + "." + method.getName());
        }
        org.apache.bcel.classfile.JavaClass javaClass = classContext.getJavaClass();
        org.apache.bcel.generic.ConstantPoolGen cpg = classContext.getConstantPoolGen();
        edu.umd.cs.findbugs.ba.Dataflow<java.util.BitSet, edu.umd.cs.findbugs.ba.LiveLocalStoreAnalysis> llsaDataflow = classContext.getLiveLocalStoreDataflow(method);
        org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow = classContext.getValueNumberDataflow(method);
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = classContext.getTypeDataflow(method);
        java.lang.String sourceFileName = javaClass.getSourceFileName();
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if (ins instanceof org.apache.bcel.generic.InvokeInstruction) {
                org.apache.bcel.generic.InvokeInstruction invoke = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
                java.lang.String className = invoke.getClassName(cpg);
                if (invoke.getMethodName(cpg).equals("putIfAbsent")) {
                    java.lang.String signature = invoke.getSignature(cpg);
                    if (signature.equals("(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;") &&  !(invoke instanceof org.apache.bcel.generic.INVOKESTATIC) && this.extendsConcurrentMap(className)) {
                        org.apache.bcel.generic.InstructionHandle next = handle.getNext();
                        boolean isIgnored = next != null && next.getInstruction() instanceof org.apache.bcel.generic.POP;
                        boolean isImmediateNullTest = next != null && (next.getInstruction() instanceof org.apache.bcel.generic.IFNULL || next.getInstruction() instanceof org.apache.bcel.generic.IFNONNULL);
                        if (countOtherCalls || isIgnored) {
                            java.util.BitSet live = llsaDataflow.getAnalysis().getFactAtLocation(location);
                            edu.umd.cs.findbugs.ba.vna.ValueNumberFrame vna = vnaDataflow.getAnalysis().getFactAtLocation(location);
                            edu.umd.cs.findbugs.ba.vna.ValueNumber vn = vna.getTopValue();
                            int locals = vna.getNumLocals();
                            boolean isRetained = false;
                            for (int pos = 0; pos < locals; pos++) if (vna.getValue(pos).equals(vn) && live.get(pos)) {
                                edu.umd.cs.findbugs.BugAnnotation ba = edu.umd.cs.findbugs.ba.vna.ValueNumberSourceInfo.findAnnotationFromValueNumber(method,location,vn,vnaDataflow.getFactAtLocation(location),"VALUE_OF");
                                if (ba == null) continue;
                                java.lang.String pattern = "RV_RETURN_VALUE_OF_PUTIFABSENT_IGNORED";
                                if ( !isIgnored) pattern = "UNKNOWN";
                                org.apache.bcel.generic.Type type = typeDataflow.getAnalysis().getFactAtLocation(location).getTopValue();
                                int priority = getPriorityForBeingMutable(type);
                                edu.umd.cs.findbugs.BugInstance bugInstance = new edu.umd.cs.findbugs.BugInstance(this, pattern, priority).addClassAndMethod(methodGen,sourceFileName).addCalledMethod(methodGen,invoke).add(new edu.umd.cs.findbugs.TypeAnnotation(type)).add(ba);
                                edu.umd.cs.findbugs.SourceLineAnnotation where = edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,method,location);
                                accumulator.accumulateBug(bugInstance,where);
                                isRetained = true;
                                break;
                            }
                            if (countOtherCalls &&  !isRetained && edu.umd.cs.findbugs.SystemProperties.getBoolean("report_TESTING_pattern_in_standard_detectors")) {
                                int priority = LOW_PRIORITY;
                                if ( !isImmediateNullTest &&  !isIgnored) {
                                    edu.umd.cs.findbugs.ba.type.TypeDataflow typeAnalysis = classContext.getTypeDataflow(method);
                                    org.apache.bcel.generic.Type type = typeAnalysis.getFactAtLocation(location).getTopValue();
                                    java.lang.String valueSignature = type.getSignature();
                                    if ( !valueSignature.startsWith("Ljava/util/concurrent/atomic/Atomic")) priority = edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY;
                                }
                                edu.umd.cs.findbugs.BugInstance bugInstance = new edu.umd.cs.findbugs.BugInstance(this, "TESTING", priority).addClassAndMethod(methodGen,sourceFileName).addString("Counting putIfAbsentCalls").addCalledMethod(methodGen,invoke);
                                edu.umd.cs.findbugs.SourceLineAnnotation where = edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,method,location);
                                accumulator.accumulateBug(bugInstance,where);
                            }
                        }
                    }
                    else if (countOtherCalls) {
                        edu.umd.cs.findbugs.BugInstance bugInstance = new edu.umd.cs.findbugs.BugInstance(this, "TESTING2", edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY).addClassAndMethod(methodGen,sourceFileName).addCalledMethod(methodGen,invoke);
                        edu.umd.cs.findbugs.SourceLineAnnotation where = edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,method,location);
                        accumulator.accumulateBug(bugInstance,where);
                    }
                }
            }
        }
        accumulator.reportAccumulatedBugs();
    }
    private boolean extendsConcurrentMap(java.lang.String className) {
        if (className.equals("java.util.concurrent.ConcurrentHashMap") || className.equals(concurrentMapDescriptor.getDottedClassName())) return true;
        edu.umd.cs.findbugs.classfile.ClassDescriptor c = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptorFromDottedClassName(className);
        edu.umd.cs.findbugs.ba.ch.Subtypes2 subtypes2 = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getSubtypes2();
        try {
            if (subtypes2.isSubtype(c,concurrentMapDescriptor)) return true;
        }
        catch (java.lang.ClassNotFoundException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.reportMissingClass(e);
        }
        return false;
    }
}
