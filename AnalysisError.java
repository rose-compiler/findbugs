/**
 * Object recording a recoverable error that occurred during analysis.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.util.ArrayList;
import java.util.Arrays;
public class AnalysisError extends java.lang.Object {
    private java.lang.String message;
    private java.lang.String exceptionMessage;
    private java.lang.String[] stackTrace;
    private java.lang.String nestedExceptionMessage;
    private java.lang.String[] nestedStackTrace;
    final private java.lang.Throwable exception;
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((exceptionMessage == null) ? 0 : exceptionMessage.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + ((nestedExceptionMessage == null) ? 0 : nestedExceptionMessage.hashCode());
        result = prime * result + java.util.Arrays.hashCode(nestedStackTrace);
        result = prime * result + java.util.Arrays.hashCode(stackTrace);
        return result;
    }
    public boolean equals(java.lang.Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if ( !(obj instanceof edu.umd.cs.findbugs.AnalysisError)) return false;
        edu.umd.cs.findbugs.AnalysisError other = (edu.umd.cs.findbugs.AnalysisError) (obj) ;
        if (exceptionMessage == null) {
            if (other.exceptionMessage != null) return false;
        }
        else if ( !exceptionMessage.equals(other.exceptionMessage)) return false;
        if (message == null) {
            if (other.message != null) return false;
        }
        else if ( !message.equals(other.message)) return false;
        if (nestedExceptionMessage == null) {
            if (other.nestedExceptionMessage != null) return false;
        }
        else if ( !nestedExceptionMessage.equals(other.nestedExceptionMessage)) return false;
        if ( !java.util.Arrays.equals(nestedStackTrace,other.nestedStackTrace)) return false;
        if ( !java.util.Arrays.equals(stackTrace,other.stackTrace)) return false;
        return true;
    }
/**
     * Constructor.
     * 
     * @param message
     *            message describing the error
     */
    public AnalysisError(java.lang.String message) {
        this(message,null);
    }
/**
     * Constructor.
     * 
     * @param message
     *            message describing the error
     * @param exception
     *            exception which is the cause of the error
     */
    public AnalysisError(java.lang.String message, java.lang.Throwable exception) {
        super();
        this.message = message;
        this.exception = exception;
        if (exception != null) {
            exceptionMessage = exception.toString();
            stackTrace = this.getStackTraceAsStringArray(exception);
            java.lang.Throwable initCause = exception.getCause();
            if (initCause != null) {
                nestedExceptionMessage = initCause.toString();
                nestedStackTrace = this.getStackTraceAsStringArray(initCause);
            }
        }
    }
/**
     * @param exception
     * @return
     */
    private java.lang.String[] getStackTraceAsStringArray(java.lang.Throwable exception) {
        java.lang.StackTraceElement[] exceptionStackTrace = exception.getStackTrace();
        java.util.ArrayList<java.lang.String> arr = new java.util.ArrayList<java.lang.String>();
        for (java.lang.StackTraceElement aExceptionStackTrace : exceptionStackTrace){
            arr.add(aExceptionStackTrace.toString());
        }
;
        java.lang.String[] tmp = arr.toArray(new java.lang.String[arr.size()]);
        return tmp;
    }
/**
     * Set the message describing the error.
     * 
     * @param message
     *            message describing the error
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }
/**
     * Get the message describing the error.
     */
    public java.lang.String getMessage() {
        return message;
    }
/**
     * Set the exception message. This is the value returned by calling
     * toString() on the original exception object.
     * 
     * @param exceptionMessage
     *            the exception message
     */
    public void setExceptionMessage(java.lang.String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
/**
     * Get the exception message. This is the value returned by calling
     * toString() on the original exception object.
     */
    public java.lang.String getExceptionMessage() {
        return exceptionMessage;
    }
/**
     * Get the exception message. This is the value returned by calling
     * toString() on the original exception object.
     */
    public java.lang.String getNestedExceptionMessage() {
        return nestedExceptionMessage;
    }
/**
     * Set the stack trace elements. These are the strings returned by calling
     * toString() on each StackTraceElement in the original exception.
     * 
     * @param stackTraceList
     *            the stack trace elements
     */
    public void setStackTrace(java.lang.String[] stackTraceList) {
        stackTrace = stackTraceList;
    }
/**
     * Get the stack trace elements. These are the strings returned by calling
     * toString() on each StackTraceElement in the original exception.
     */
    public java.lang.String[] getStackTrace() {
        return stackTrace;
    }
/**
     * Get the stack trace elements. These are the strings returned by calling
     * toString() on each StackTraceElement in the original exception.
     */
    public java.lang.String[] getNestedStackTrace() {
        return nestedStackTrace;
    }
/**
     * @return original exception object, or null if no exception was thrown
     */
    public java.lang.Throwable getException() {
        return exception;
    }
}
