/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
public class ClassSummary extends java.lang.Object {
    public ClassSummary() {
    }
    private java.util.Map<edu.umd.cs.findbugs.classfile.ClassDescriptor, edu.umd.cs.findbugs.classfile.ClassDescriptor> map = new java.util.HashMap<edu.umd.cs.findbugs.classfile.ClassDescriptor, edu.umd.cs.findbugs.classfile.ClassDescriptor>();
    private java.util.Set<edu.umd.cs.findbugs.classfile.ClassDescriptor> veryFunky = new java.util.HashSet<edu.umd.cs.findbugs.classfile.ClassDescriptor>();
    public boolean mightBeEqualTo(edu.umd.cs.findbugs.classfile.ClassDescriptor checker, edu.umd.cs.findbugs.classfile.ClassDescriptor checkee) {
        return checkee.equals(map.get(checker)) || veryFunky.contains(checker);
    }
    public void checksForEqualTo(edu.umd.cs.findbugs.classfile.ClassDescriptor checker, edu.umd.cs.findbugs.classfile.ClassDescriptor checkee) {
        edu.umd.cs.findbugs.classfile.ClassDescriptor existing = map.get(checker);
        if (checkee.equals(existing)) return;
        else if (existing != null) veryFunky.add(checker);
        else map.put(checker,checkee);
    }
}
