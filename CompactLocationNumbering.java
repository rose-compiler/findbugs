/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Compute a compact numbering of Locations in a CFG. This is useful for
 * analyses that want to use a BitSet to keep track of Locations.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.HashMap;
import java.util.Iterator;
public class CompactLocationNumbering extends java.lang.Object {
    private java.util.HashMap<edu.umd.cs.findbugs.ba.Location, java.lang.Integer> locationToNumberMap;
    private java.util.HashMap<java.lang.Integer, edu.umd.cs.findbugs.ba.Location> numberToLocationMap;
/**
     * Constructor.
     * 
     * @param cfg
     *            the CFG containing the Locations to number
     */
    public CompactLocationNumbering(edu.umd.cs.findbugs.ba.CFG cfg) {
        super();
        this.locationToNumberMap = new java.util.HashMap<edu.umd.cs.findbugs.ba.Location, java.lang.Integer>();
        this.numberToLocationMap = new java.util.HashMap<java.lang.Integer, edu.umd.cs.findbugs.ba.Location>();
        this.build(cfg);
    }
/**
     * Get the size of the numbering, which is the maximum number assigned plus
     * one.
     * 
     * @return the maximum number assigned plus one
     */
    public int getSize() {
        return locationToNumberMap.size();
    }
/**
     * Get the number of given Location, which will be a non-negative integer in
     * the range 0..getSize() - 1.
     * 
     * @param location
     * @return the number of the location
     */
    public int getNumber(edu.umd.cs.findbugs.ba.Location location) {
        return locationToNumberMap.get(location).intValue();
    }
/**
     * Get the Location given its number.
     * 
     * @param number
     *            the number
     * @return Location corresponding to that number
     */
    public edu.umd.cs.findbugs.ba.Location getLocation(int number) {
        return numberToLocationMap.get(number);
    }
    private void build(edu.umd.cs.findbugs.ba.CFG cfg) {
        int count = 0;
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            java.lang.Integer number = count++;
            edu.umd.cs.findbugs.ba.Location location = i.next();
            locationToNumberMap.put(location,number);
            numberToLocationMap.put(number,location);
        }
    }
}
