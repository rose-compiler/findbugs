/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * This is the properties dialog of the GUI. It allows the user to set the size
 * of the tabs and font size. If the user changes the font size they will be
 * told to restart the computer before the new size takes affect.
 * 
 * @author Kristin Stephens
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
public class PropertiesDialog extends edu.umd.cs.findbugs.gui2.FBDialog {
    private static edu.umd.cs.findbugs.gui2.PropertiesDialog instance;
    private javax.swing.JTextField tabTextField;
    private javax.swing.JTextField fontTextField;
    public static edu.umd.cs.findbugs.gui2.PropertiesDialog getInstance() {
        if (instance == null) instance = new edu.umd.cs.findbugs.gui2.PropertiesDialog();
        return instance;
    }
    public PropertiesDialog() {
        super();
        javax.swing.JPanel contentPanel = new javax.swing.JPanel(new java.awt.BorderLayout());
        javax.swing.JPanel mainPanel = new javax.swing.JPanel();
        mainPanel.setLayout(new java.awt.GridLayout(2, 2));
        mainPanel.add(new javax.swing.JLabel("Tab Size"));
        tabTextField = new javax.swing.JTextField(java.lang.Integer.toString(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getTabSize()));
        mainPanel.add(tabTextField);
        mainPanel.add(new javax.swing.JLabel("Font Size"));
        fontTextField = new javax.swing.JTextField(java.lang.Float.toString(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getFontSize()));
        mainPanel.add(fontTextField);
        contentPanel.add(mainPanel,java.awt.BorderLayout.CENTER);
        javax.swing.JPanel bottomPanel = new javax.swing.JPanel();
        bottomPanel.add(new javax.swing.JButton(new javax.swing.AbstractAction("Apply") {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (java.lang.Integer.decode(tabTextField.getText()).intValue() != edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getTabSize()) {
                    edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setTabSize(java.lang.Integer.decode(tabTextField.getText()).intValue());
                    edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSourceCodeDisplayer().clearCache();
                    edu.umd.cs.findbugs.gui2.MainFrame.getInstance().syncBugInformation();
                }
                if (java.lang.Float.parseFloat(fontTextField.getText()) != edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getFontSize()) {
                    edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setFontSize(java.lang.Float.parseFloat(fontTextField.getText()));
                    javax.swing.JOptionPane.showMessageDialog(edu.umd.cs.findbugs.gui2.PropertiesDialog.getInstance(),"To implement the new font size, please restart FindBugs.","Changing Font",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }));
        bottomPanel.add(new javax.swing.JButton(new javax.swing.AbstractAction("Reset") {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tabTextField.setText(java.lang.Integer.toString(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getTabSize()));
                fontTextField.setText(java.lang.Float.toString(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getFontSize()));
            }
        }));
        contentPanel.add(bottomPanel,java.awt.BorderLayout.SOUTH);
        this.setContentPane(contentPanel);
        this.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        this.setModal(true);
        this.pack();
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowDeactivated(java.awt.event.WindowEvent e) {
                if (java.lang.Integer.decode(tabTextField.getText()).intValue() != edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getTabSize()) tabTextField.setText(java.lang.Integer.toString(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getTabSize()));
                if (java.lang.Float.parseFloat(fontTextField.getText()) != edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getFontSize()) {
                    fontTextField.setText(java.lang.Float.toString(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getFontSize()));
                }
            }
        });
    }
}
