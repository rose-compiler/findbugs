/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Enum representing how a particular warning property is expected to affect its
 * likelihood of being serious, benign, or a false positive.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.props;
import edu.umd.cs.findbugs.props.*;
public class PriorityAdjustment extends java.lang.Object {
    private java.lang.String value;
    public PriorityAdjustment(java.lang.String value) {
        super();
        this.value = value;
    }
    public java.lang.String toString() {
        return value;
    }
/** No adjustment to the priority. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment NO_ADJUSTMENT = new edu.umd.cs.findbugs.props.PriorityAdjustment("NO_ADJUSTMENT");
/** Raise the priority. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment RAISE_PRIORITY = new edu.umd.cs.findbugs.props.PriorityAdjustment("RAISE_PRIORITY");
/** Raise the priority. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment RAISE_PRIORITY_TO_AT_LEAST_NORMAL = new edu.umd.cs.findbugs.props.PriorityAdjustment("RAISE_PRIORITY_TO_AT_LEAST_NORMAL");
/** lower the priority. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment LOWER_PRIORITY_TO_AT_MOST_NORMAL = new edu.umd.cs.findbugs.props.PriorityAdjustment("LOWER_PRIORITY_TO_AT_MOST_NORMAL");
/** Raise the priority. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment RAISE_PRIORITY_TO_HIGH = new edu.umd.cs.findbugs.props.PriorityAdjustment("RAISE_PRIORITY_TO_HIGH");
/** Priority is at most low. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment AT_MOST_LOW = new edu.umd.cs.findbugs.props.PriorityAdjustment("AT_MOST_LOW");
/** Priority is at most medium. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment AT_MOST_MEDIUM = new edu.umd.cs.findbugs.props.PriorityAdjustment("AT_MOST_MEDIUM");
/** Pegged high */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment PEGGED_HIGH = new edu.umd.cs.findbugs.props.PriorityAdjustment("PEGGED_HIGH");
/** Lower the priority. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment LOWER_PRIORITY = new edu.umd.cs.findbugs.props.PriorityAdjustment("LOWER_PRIORITY");
/** Lower the priority a little */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment A_LITTLE_BIT_LOWER_PRIORITY = new edu.umd.cs.findbugs.props.PriorityAdjustment("A_LITTLE_BIT_LOWER_PRIORITY");
/** Raise the priority a little */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment A_LITTLE_BIT_HIGHER_PRIORITY = new edu.umd.cs.findbugs.props.PriorityAdjustment("A_LITTLE_BIT_HIGHER_PRIORITY");
/** Warning is likely to be a false positive. */
    final public static edu.umd.cs.findbugs.props.PriorityAdjustment FALSE_POSITIVE = new edu.umd.cs.findbugs.props.PriorityAdjustment("FALSE_POSITIVE");
}
