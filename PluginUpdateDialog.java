package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.updates.PluginUpdateListener;
import edu.umd.cs.findbugs.updates.UpdateChecker;
import edu.umd.cs.findbugs.util.LaunchBrowser;
public class PluginUpdateDialog extends java.lang.Object implements java.io.Serializable {
// sort by name
// place core plugin first, if present
    private class MyPluginUpdateListener extends java.lang.Object implements edu.umd.cs.findbugs.updates.PluginUpdateListener {
        public MyPluginUpdateListener() {
        }
        public void pluginUpdateCheckComplete(final java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates, final boolean force) {
            if (updates.isEmpty() &&  !force) return;
            if (force) this.showUpdateDialogInSwingThread(updates,force);
            else edu.umd.cs.findbugs.util.Util.runInDameonThread(new java.lang.Runnable() {
                public void run() {
                    try {
                        java.lang.Thread.sleep(SOFTWARE_UPDATE_DIALOG_DELAY_MS);
                        edu.umd.cs.findbugs.gui2.PluginUpdateDialog.MyPluginUpdateListener.this.showUpdateDialogInSwingThread(updates,force);
                    }
                    catch (java.lang.InterruptedException e){
                        LOGGER.log(java.util.logging.Level.FINE,"Software update dialog thread interrupted",e);
                    }
                }
            },"Software Update Dialog");
        }
// wait 5 seconds before showing dialog
        private void showUpdateDialogInSwingThread(final java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates, final boolean force) {
            javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
                public void run() {
                    edu.umd.cs.findbugs.gui2.PluginUpdateDialog.this.showUpdateDialog(updates,force);
                }
            });
        }
    }
    public PluginUpdateDialog() {
    }
    final private static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.gui2.PluginUpdateDialog.class.getName());
    final private static int SOFTWARE_UPDATE_DIALOG_DELAY_MS = 5000;
    public void showUpdateDialog(java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates, boolean force) {
        java.util.List<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> sortedUpdates = new java.util.ArrayList<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate>();
        edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate core = this.sortUpdates(updates,sortedUpdates);
        if (edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getUpdateChecker().updatesHaveBeenSeenBefore(sortedUpdates) &&  !force) return;
        java.lang.String headline;
        if (core != null && updates.size() >= 2) headline = "FindBugs and some plugins have updates";
        else if (updates.isEmpty()) headline = "FindBugs and all plugins are up to date!";
        else if (core == null) headline = "Some FindBugs plugins have updates";
        else headline = null;
        final javax.swing.JPanel comp = new javax.swing.JPanel(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gbc = new java.awt.GridBagConstraints();
        gbc.insets = new java.awt.Insets(5, 5, 5, 5);
        gbc.gridwidth = 3;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        if (headline != null) {
            javax.swing.JLabel headlineLabel = new javax.swing.JLabel(headline);
            headlineLabel.setFont(headlineLabel.getFont().deriveFont(java.awt.Font.BOLD,24));
            comp.add(headlineLabel,gbc);
        }
        if ( !updates.isEmpty()) {
            int i = 1;
            for (edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate update : sortedUpdates){
                gbc.gridy = ++i;
                gbc.gridx = 1;
                gbc.fill = java.awt.GridBagConstraints.BOTH;
                gbc.gridwidth = 1;
                gbc.weightx = 1;
                javax.swing.JLabel label = this.createPluginLabel(update);
                comp.add(label,gbc);
                gbc.weightx = 0;
                gbc.gridx = 2;
                java.lang.String url = update.getUrl();
                if (url != null && url.length() > 0) {
                    javax.swing.JButton button = this.createPluginUpdateButton(comp,update);
                    comp.add(button,gbc);
                }
                java.lang.String msg = update.getMessage();
                if (msg != null && msg.length() > 0) {
                    gbc.gridx = 1;
                    gbc.gridwidth = 3;
                    gbc.weightx = 1;
                    gbc.fill = java.awt.GridBagConstraints.BOTH;
                    gbc.gridy = ++i;
                    javax.swing.JTextPane msgpane = this.createMessagePane(msg);
                    comp.add(msgpane,gbc);
                }
            }
;
        }
        javax.swing.JOptionPane.showMessageDialog(null,comp,"Software Updates",javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }
    private javax.swing.JTextPane createMessagePane(java.lang.String msg) {
        javax.swing.JTextPane msgpane = new javax.swing.JTextPane();
        msgpane.setEditable(false);
        msgpane.setFocusable(false);
        msgpane.setText(msg);
        return msgpane;
    }
    private javax.swing.JLabel createPluginLabel(edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate update) {
        java.lang.String name;
        if (update.getPlugin().isCorePlugin()) name = "FindBugs";
        else name = update.getPlugin().getShortDescription();
        javax.swing.JLabel label = new javax.swing.JLabel(java.text.MessageFormat.format("<html><b>{0} {2}</b> is available<br><i><small>(currently installed: {1})",name,update.getPlugin().getVersion(),update.getVersion()));
        label.setFont(label.getFont().deriveFont(java.awt.Font.PLAIN,label.getFont().getSize() + 4));
        return label;
    }
    public edu.umd.cs.findbugs.updates.PluginUpdateListener createListener() {
        return new edu.umd.cs.findbugs.gui2.PluginUpdateDialog.MyPluginUpdateListener();
    }
    private javax.swing.JButton createPluginUpdateButton(final javax.swing.JPanel comp, final edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate update) {
        javax.swing.JButton button = new javax.swing.JButton("<html><u><font color=#0000ff>More info...");
        button.setBorderPainted(false);
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBackground(comp.getBackground());
        button.setToolTipText(update.getUrl());
        button.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.HAND_CURSOR));
        button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                boolean failed;
                java.lang.String url = update.getUrl();
                try {
                    failed = url == null ||  !edu.umd.cs.findbugs.util.LaunchBrowser.showDocument(new java.net.URL(url));
                }
                catch (java.net.MalformedURLException e1){
                    failed = true;
                }
                if (failed) javax.swing.JOptionPane.showMessageDialog(comp,"Could not open URL " + url);
            }
        });
        return button;
    }
    private edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate sortUpdates(java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates, java.util.List<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> sorted) {
        edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate core = null;
        for (edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate update : updates){
            if (update.getPlugin().isCorePlugin()) core = update;
            else sorted.add(update);
        }
;
        java.util.Collections.sort(sorted,new java.util.Comparator<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate>() {
            public int compare(edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate o1, edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate o2) {
                return o1.getPlugin().getShortDescription().compareTo(o2.getPlugin().getShortDescription());
            }
        });
        if (core != null) sorted.add(0,core);
        return core;
    }
}
