/**
 * Analysis engine to produce CallListDataflow objects for a method.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import edu.umd.cs.findbugs.ba.ca.CallListAnalysis;
import edu.umd.cs.findbugs.ba.ca.CallListDataflow;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class CallListDataflowFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.ca.CallListDataflow> {
    public CallListDataflowFactory() {
        super("call list analysis",edu.umd.cs.findbugs.ba.ca.CallListDataflow.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.ca.CallListDataflow analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        edu.umd.cs.findbugs.ba.ca.CallListAnalysis analysis = new edu.umd.cs.findbugs.ba.ca.CallListAnalysis(this.getCFG(analysisCache,descriptor), this.getDepthFirstSearch(analysisCache,descriptor), this.getConstantPoolGen(analysisCache,descriptor.getClassDescriptor()));
        edu.umd.cs.findbugs.ba.ca.CallListDataflow dataflow = new edu.umd.cs.findbugs.ba.ca.CallListDataflow(this.getCFG(analysisCache,descriptor), analysis);
        dataflow.execute();
        return dataflow;
    }
}
