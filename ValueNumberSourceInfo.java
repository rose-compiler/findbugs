/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Helper methods to find out information about the source of the value
 * represented by a given ValueNumber.
 * 
 * @author Bill Pugh
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.vna;
import edu.umd.cs.findbugs.ba.vna.*;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ACONST_NULL;
import org.apache.bcel.generic.InstructionHandle;
import edu.umd.cs.findbugs.BugAnnotation;
import edu.umd.cs.findbugs.FieldAnnotation;
import edu.umd.cs.findbugs.LocalVariableAnnotation;
import edu.umd.cs.findbugs.StringAnnotation;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.XField;
abstract public class ValueNumberSourceInfo extends java.lang.Object {
    public ValueNumberSourceInfo() {
    }
/**
     * @param method
     * @param location
     * @param valueNumber
     * @param vnaFrame
     * @param partialRole
     *            TODO
     * @return the annotation
     */
    public static edu.umd.cs.findbugs.BugAnnotation findAnnotationFromValueNumber(org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.Location location, edu.umd.cs.findbugs.ba.vna.ValueNumber valueNumber, edu.umd.cs.findbugs.ba.vna.ValueNumberFrame vnaFrame, java.lang.String partialRole) {
        if (location.getHandle().getInstruction() instanceof org.apache.bcel.generic.ACONST_NULL) {
            edu.umd.cs.findbugs.StringAnnotation nullConstant = new edu.umd.cs.findbugs.StringAnnotation("null");
            nullConstant.setDescription(edu.umd.cs.findbugs.StringAnnotation.STRING_NONSTRING_CONSTANT_ROLE);
            return nullConstant;
        }
        edu.umd.cs.findbugs.LocalVariableAnnotation ann = edu.umd.cs.findbugs.ba.vna.ValueNumberSourceInfo.findLocalAnnotationFromValueNumber(method,location,valueNumber,vnaFrame);
        if (ann != null && partialRole != null) ann.setDescription("LOCAL_VARIABLE_" + partialRole);
        if (ann != null && ann.isSignificant()) {
            return ann;
        }
        edu.umd.cs.findbugs.FieldAnnotation field = edu.umd.cs.findbugs.ba.vna.ValueNumberSourceInfo.findFieldAnnotationFromValueNumber(method,location,valueNumber,vnaFrame);
        if (field != null) {
            if (partialRole != null) field.setDescription("FIELD_" + partialRole);
            return field;
        }
        if (ann != null) return ann;
        return null;
    }
/**
     * @param method
     * @param location
     * @param valueNumber
     * @param vnaFrame
     * @param partialRole
     *            TODO
     * @return the annotation
     */
    public static edu.umd.cs.findbugs.BugAnnotation findRequiredAnnotationFromValueNumber(org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.Location location, edu.umd.cs.findbugs.ba.vna.ValueNumber valueNumber, edu.umd.cs.findbugs.ba.vna.ValueNumberFrame vnaFrame, java.lang.String partialRole) {
        edu.umd.cs.findbugs.BugAnnotation result = findAnnotationFromValueNumber(method,location,valueNumber,vnaFrame,partialRole);
        if (result != null) return result;
        return new edu.umd.cs.findbugs.LocalVariableAnnotation("?",  -1, location.getHandle().getPosition());
    }
    public static edu.umd.cs.findbugs.LocalVariableAnnotation findLocalAnnotationFromValueNumber(org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.Location location, edu.umd.cs.findbugs.ba.vna.ValueNumber valueNumber, edu.umd.cs.findbugs.ba.vna.ValueNumberFrame vnaFrame) {
        if (vnaFrame == null || vnaFrame.isBottom() || vnaFrame.isTop()) return null;
        edu.umd.cs.findbugs.LocalVariableAnnotation localAnnotation = null;
        for (int i = 0; i < vnaFrame.getNumLocals(); i++) {
            if (valueNumber.equals(vnaFrame.getValue(i))) {
                org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
                org.apache.bcel.generic.InstructionHandle prev = handle.getPrev();
                if (prev == null) continue;
                int position1 = prev.getPosition();
                int position2 = handle.getPosition();
                localAnnotation = edu.umd.cs.findbugs.LocalVariableAnnotation.getLocalVariableAnnotation(method,i,position1,position2);
                if (localAnnotation != null) return localAnnotation;
            }
        }
        return null;
    }
    public static edu.umd.cs.findbugs.FieldAnnotation findFieldAnnotationFromValueNumber(org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.Location location, edu.umd.cs.findbugs.ba.vna.ValueNumber valueNumber, edu.umd.cs.findbugs.ba.vna.ValueNumberFrame vnaFrame) {
        edu.umd.cs.findbugs.ba.XField field = edu.umd.cs.findbugs.ba.vna.ValueNumberSourceInfo.findXFieldFromValueNumber(method,location,valueNumber,vnaFrame);
        if (field == null) return null;
        return edu.umd.cs.findbugs.FieldAnnotation.fromXField(field);
    }
    public static edu.umd.cs.findbugs.ba.XField findXFieldFromValueNumber(org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.Location location, edu.umd.cs.findbugs.ba.vna.ValueNumber valueNumber, edu.umd.cs.findbugs.ba.vna.ValueNumberFrame vnaFrame) {
        if (vnaFrame == null || vnaFrame.isBottom() || vnaFrame.isTop()) return null;
        edu.umd.cs.findbugs.ba.vna.AvailableLoad load = vnaFrame.getLoad(valueNumber);
        if (load != null) {
            return load.getField();
        }
        return null;
    }
/**
     * @param classContext
     * @param method
     * @param location
     * @param stackPos
     * @throws DataflowAnalysisException
     * @throws CFGBuilderException
     */
    public static edu.umd.cs.findbugs.BugAnnotation getFromValueNumber(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.Location location, int stackPos) throws edu.umd.cs.findbugs.ba.CFGBuilderException, edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        edu.umd.cs.findbugs.ba.vna.ValueNumberFrame vnaFrame = classContext.getValueNumberDataflow(method).getFactAtLocation(location);
        if ( !vnaFrame.isValid()) return null;
        edu.umd.cs.findbugs.ba.vna.ValueNumber valueNumber = vnaFrame.getStackValue(stackPos);
        if (valueNumber.hasFlag(edu.umd.cs.findbugs.ba.vna.ValueNumber.CONSTANT_CLASS_OBJECT)) return null;
        edu.umd.cs.findbugs.BugAnnotation variableAnnotation = findAnnotationFromValueNumber(method,location,valueNumber,vnaFrame,"VALUE_OF");
        return variableAnnotation;
    }
}
