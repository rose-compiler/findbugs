/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba.bcp;
import edu.umd.cs.findbugs.ba.bcp.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.bcel.generic.InstructionHandle;
public class ByteCodePatternMatch extends java.lang.Object {
    private edu.umd.cs.findbugs.ba.bcp.BindingSet bindingSet;
    private edu.umd.cs.findbugs.ba.bcp.PatternElementMatch lastElementMatch;
    private java.util.LinkedList<edu.umd.cs.findbugs.ba.bcp.PatternElementMatch> patternElementMatchList;
    public java.lang.String toString() {
        java.util.ArrayList<java.lang.Integer> lst = new java.util.ArrayList<java.lang.Integer>();
        for (edu.umd.cs.findbugs.ba.bcp.PatternElementMatch m : patternElementMatchList){
            lst.add(m.getMatchedInstructionInstructionHandle().getPosition());
        }
;
        return lst.toString();
    }
    public ByteCodePatternMatch(edu.umd.cs.findbugs.ba.bcp.BindingSet bindingSet, edu.umd.cs.findbugs.ba.bcp.PatternElementMatch lastElementMatch) {
        super();
        this.bindingSet = bindingSet;
        this.lastElementMatch = lastElementMatch;
        this.patternElementMatchList = new java.util.LinkedList<edu.umd.cs.findbugs.ba.bcp.PatternElementMatch>();
// The PatternElementMatch objects are stored in reverse order.
// So, put them in a LinkedList to get them in the right order.
        while (lastElementMatch != null) {
            patternElementMatchList.addFirst(lastElementMatch);
            lastElementMatch = lastElementMatch.getPrev();
        }
    }
    public edu.umd.cs.findbugs.ba.bcp.BindingSet getBindingSet() {
        return bindingSet;
    }
    public java.util.Iterator<edu.umd.cs.findbugs.ba.bcp.PatternElementMatch> patternElementMatchIterator() {
        return patternElementMatchList.iterator();
    }
    public org.apache.bcel.generic.InstructionHandle getLabeledInstruction(java.lang.String label) {
        return lastElementMatch != null ? lastElementMatch.getLabeledInstruction(label) : null;
    }
    public edu.umd.cs.findbugs.ba.bcp.PatternElementMatch getFirstLabeledMatch(java.lang.String label) {
        return lastElementMatch != null ? lastElementMatch.getFirstLabeledMatch(label) : null;
    }
    public edu.umd.cs.findbugs.ba.bcp.PatternElementMatch getLastLabeledMatch(java.lang.String label) {
        return lastElementMatch != null ? lastElementMatch.getLastLabeledMatch(label) : null;
    }
}
// vim:ts=4
