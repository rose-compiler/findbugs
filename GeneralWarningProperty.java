/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * General warning properties. These are properties that could be attached to
 * any warning to provide information which might be useful in determining
 * whether or not the bug is a false positive, and/or the severity of the
 * warning.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.props;
import edu.umd.cs.findbugs.props.*;
public class GeneralWarningProperty extends edu.umd.cs.findbugs.props.AbstractWarningProperty {
    public GeneralWarningProperty(java.lang.String name, edu.umd.cs.findbugs.props.PriorityAdjustment priorityAdjustment) {
        super(name,priorityAdjustment);
    }
/**
     * The type of the receiver object in a method call or instance field
     * access.
     */
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty RECEIVER_OBJECT_TYPE = new edu.umd.cs.findbugs.props.GeneralWarningProperty("RECEIVER_OBJECT_TYPE", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** Name of most recently called method. */
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty CALLED_METHOD_1 = new edu.umd.cs.findbugs.props.GeneralWarningProperty("CALLED_METHOD_1", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** Name of second-most recently called method. */
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty CALLED_METHOD_2 = new edu.umd.cs.findbugs.props.GeneralWarningProperty("CALLED_METHOD_2", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** Name of third-most recently called method. */
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty CALLED_METHOD_3 = new edu.umd.cs.findbugs.props.GeneralWarningProperty("CALLED_METHOD_3", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** Name of fourth-most recently called method. */
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty CALLED_METHOD_4 = new edu.umd.cs.findbugs.props.GeneralWarningProperty("CALLED_METHOD_4", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** Warning occurs on an exception control path. */
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty ON_EXCEPTION_PATH = new edu.umd.cs.findbugs.props.GeneralWarningProperty("ON_EXCEPTION_PATH", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
/** issue is in uncallable method */
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty IN_UNCALLABLE_METHOD = new edu.umd.cs.findbugs.props.GeneralWarningProperty("IN_UNCALLABLE_METHOD", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_LOW);
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty FALSE_POSITIVE = new edu.umd.cs.findbugs.props.GeneralWarningProperty("FALSE_POSITIVE", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty NOISY_BUG = new edu.umd.cs.findbugs.props.GeneralWarningProperty("NOISY_BUG", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_MEDIUM);
    final public static edu.umd.cs.findbugs.props.GeneralWarningProperty SILENT_BUG = new edu.umd.cs.findbugs.props.GeneralWarningProperty("SILENT_BUG", edu.umd.cs.findbugs.props.PriorityAdjustment.NO_ADJUSTMENT);
}
