/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.io;
import edu.umd.cs.findbugs.io.*;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
public class SlowInputStream extends java.io.FilterInputStream {
    private long started = java.lang.System.currentTimeMillis();
    private long length = 0;
    private int bytesPerSecond;
    public SlowInputStream(java.io.InputStream in, int baudRate) {
        super(in);
        this.bytesPerSecond = baudRate / 10;
    }
    private void delay() {
        try {
            long beenRunning = java.lang.System.currentTimeMillis() - started;
            long time = length * 1000L / bytesPerSecond - beenRunning;
            if (time > 0) {
                java.lang.Thread.sleep((int) (time) );
            }
        }
        catch (java.lang.InterruptedException e){
        }
    }
    public int read() throws java.io.IOException {
        int b = in.read();
        if (b >= 0) length++;
        this.delay();
        return b;
    }
    public int read(byte[] b) throws java.io.IOException {
        return this.read(b,0,b.length);
    }
    public int read(byte[] b, int off, int len) throws java.io.IOException {
        if (len > bytesPerSecond / 10) len = bytesPerSecond / 10;
        int tmp = in.read(b,off,len);
        if (tmp >= 0) {
            length += tmp;
            this.delay();
        }
        return tmp;
    }
    public long skip(long n) throws java.io.IOException {
        n = in.skip(n);
        if (n >= 0) length += n;
        this.delay();
        return n;
    }
    public void close() throws java.io.IOException {
        in.close();
    }
}
