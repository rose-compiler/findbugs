/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba.type;
import edu.umd.cs.findbugs.ba.type.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.bcel.generic.ObjectType;
public class ExceptionSetFactory extends java.lang.Object implements java.io.Serializable {
/**
     *
     */
    final private static long serialVersionUID = 1L;
    private java.util.HashMap<org.apache.bcel.generic.ObjectType, java.lang.Integer> typeIndexMap;
    private java.util.ArrayList<org.apache.bcel.generic.ObjectType> typeList;
    public ExceptionSetFactory() {
        super();
        this.typeIndexMap = new java.util.HashMap<org.apache.bcel.generic.ObjectType, java.lang.Integer>();
        this.typeList = new java.util.ArrayList<org.apache.bcel.generic.ObjectType>();
    }
    public edu.umd.cs.findbugs.ba.type.ExceptionSet createExceptionSet() {
        return new edu.umd.cs.findbugs.ba.type.ExceptionSet(this);
    }
    int getIndexOfType(org.apache.bcel.generic.ObjectType type) {
        java.lang.Integer index = typeIndexMap.get(type);
        if (index == null) {
            index = this.getNumTypes();
            typeList.add(type);
            typeIndexMap.put(type,index);
        }
        return index.intValue();
    }
    org.apache.bcel.generic.ObjectType getType(int index) {
        return typeList.get(index);
    }
    int getNumTypes() {
        return typeList.size();
    }
}
// vim:ts=3
