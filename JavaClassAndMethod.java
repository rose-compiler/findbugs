/*
 * Bytecode analysis framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A JavaClass and a Method belonging to the class. This is useful for answering
 * a method lookup query which must concretely identify both the class and the
 * method.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import org.apache.bcel.Constants;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class JavaClassAndMethod extends java.lang.Object {
    final private org.apache.bcel.classfile.JavaClass javaClass;
    final private org.apache.bcel.classfile.Method method;
/**
     * Constructor.
     * 
     * @param javaClass
     *            the JavaClass
     * @param method
     *            a Method belonging to the JavaClass
     */
    public JavaClassAndMethod(org.apache.bcel.classfile.JavaClass javaClass, org.apache.bcel.classfile.Method method) {
        super();
        this.javaClass = javaClass;
        this.method = method;
    }
/**
     * Constructor.
     * 
     * @param method
     *            an XMethod specifying a specific method in a specific class
     * @throws ClassNotFoundException
     */
    public JavaClassAndMethod(edu.umd.cs.findbugs.ba.XMethod method) throws java.lang.ClassNotFoundException {
        super();
        this.javaClass = org.apache.bcel.Repository.lookupClass(method.getClassName());
        for (org.apache.bcel.classfile.Method m : javaClass.getMethods())if (m.getName().equals(method.getName()) && m.getSignature().equals(method.getSignature()) && m.isStatic() == method.isStatic()) {
            this.method = m;
            return;
        }
;
        throw new java.lang.IllegalArgumentException("Can't find " + method);
    }
/**
     * Get the JavaClass.
     */
    public org.apache.bcel.classfile.JavaClass getJavaClass() {
        return javaClass;
    }
/**
     * Get the Method.
     */
    public org.apache.bcel.classfile.Method getMethod() {
        return method;
    }
/**
     * Convert to an XMethod.
     */
    public edu.umd.cs.findbugs.ba.XMethod toXMethod() {
        return edu.umd.cs.findbugs.ba.XFactory.createXMethod(javaClass,method);
    }
/**
     * Get the MethodDescriptor that (hopefully) uniqely names this method.
     * 
     * @return the MethodDescriptor uniquely naming this method
     */
    public edu.umd.cs.findbugs.classfile.MethodDescriptor toMethodDescriptor() {
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getMethodDescriptor(this.getSlashedClassName(),method.getName(),method.getSignature(),method.isStatic());
    }
    private java.lang.String getSlashedClassName() {
        return javaClass.getConstantPool().getConstantString(javaClass.getClassNameIndex(),org.apache.bcel.Constants.CONSTANT_Class);
    }
    public int hashCode() {
        return javaClass.hashCode() + method.hashCode();
    }
    public boolean equals(java.lang.Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) return false;
        edu.umd.cs.findbugs.ba.JavaClassAndMethod other = (edu.umd.cs.findbugs.ba.JavaClassAndMethod) (obj) ;
        return javaClass.equals(other.javaClass) && method.equals(other.method);
    }
    public java.lang.String toString() {
        return edu.umd.cs.findbugs.ba.SignatureConverter.convertMethodSignature(javaClass,method);
    }
}
