/*
 * Bytecode analysis framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.type;
import edu.umd.cs.findbugs.ba.type.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.ba.interproc.FieldPropertyDatabase;
import edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException;
import edu.umd.cs.findbugs.classfile.FieldDescriptor;
public class FieldStoreTypeDatabase extends edu.umd.cs.findbugs.ba.interproc.FieldPropertyDatabase<edu.umd.cs.findbugs.ba.type.FieldStoreType> {
    public FieldStoreTypeDatabase() {
    }
    final public static java.lang.String DEFAULT_FILENAME = "fieldStoreTypes.db";
    public void purgeBoringEntries() {
        java.util.Collection<edu.umd.cs.findbugs.classfile.FieldDescriptor> keys = new java.util.ArrayList<edu.umd.cs.findbugs.classfile.FieldDescriptor>(this.getKeys());
        for (edu.umd.cs.findbugs.classfile.FieldDescriptor f : keys){
            java.lang.String s = f.getSignature();
            edu.umd.cs.findbugs.ba.type.FieldStoreType type = this.getProperty(f);
            org.apache.bcel.generic.Type fieldType = org.apache.bcel.generic.Type.getType(f.getSignature());
            if ( !(fieldType instanceof org.apache.bcel.generic.ReferenceType)) {
                this.removeProperty(f);
                continue;
            }
            org.apache.bcel.generic.ReferenceType storeType = type.getLoadType((org.apache.bcel.generic.ReferenceType) (fieldType) );
            if (storeType.equals(fieldType)) this.removeProperty(f);
        }
;
    }
    protected edu.umd.cs.findbugs.ba.type.FieldStoreType decodeProperty(java.lang.String propStr) throws edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException {
        edu.umd.cs.findbugs.ba.type.FieldStoreType property = new edu.umd.cs.findbugs.ba.type.FieldStoreType();
        java.util.StringTokenizer t = new java.util.StringTokenizer(propStr, ",");
        while (t.hasMoreTokens()) {
            java.lang.String signature = t.nextToken();
            property.addTypeSignature(signature);
        }
        return property;
    }
    protected java.lang.String encodeProperty(edu.umd.cs.findbugs.ba.type.FieldStoreType property) {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        for (java.util.Iterator<java.lang.String> i = property.signatureIterator(); i.hasNext(); ) {
            if (buf.length() > 0) {
                buf.append('\u002c');
            }
            buf.append(i.next());
        }
        return buf.toString();
    }
}
