/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 * @author Keith Lea
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.CheckForNull;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.cloud.Cloud;
import edu.umd.cs.findbugs.cloud.Cloud.BugFilingStatus;
import edu.umd.cs.findbugs.util.LaunchBrowser;
public class CommentsArea extends java.lang.Object {
    final private static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.gui2.CommentsArea.class.getName());
    private javax.swing.JButton fileBug;
    final private edu.umd.cs.findbugs.gui2.MainFrame frame;
    private edu.umd.cs.findbugs.cloud.Cloud.BugFilingStatus currentBugStatus;
    private edu.umd.cs.findbugs.gui2.CloudCommentsPaneSwing commentsPane;
    public CommentsArea(edu.umd.cs.findbugs.gui2.MainFrame frame) {
        super();
        this.frame = frame;
    }
    javax.swing.JPanel createCommentsInputPanel() {
        javax.swing.JPanel mainPanel = new javax.swing.JPanel();
        java.awt.GridBagLayout layout = new java.awt.GridBagLayout();
        mainPanel.setLayout(layout);
        fileBug = new javax.swing.JButton(edu.umd.cs.findbugs.cloud.Cloud.BugFilingStatus.FILE_BUG.toString());
        fileBug.setEnabled(false);
        fileBug.setToolTipText("Click to file bug for this issue");
        fileBug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                if (frame.getCurrentSelectedBugLeaf() == null) {
                    return;
                }
                if ( !edu.umd.cs.findbugs.gui2.CommentsArea.this.canNavigateAway()) return;
                edu.umd.cs.findbugs.BugInstance bug = frame.getCurrentSelectedBugLeaf().getBug();
                edu.umd.cs.findbugs.cloud.Cloud cloud1 = edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getBugCollection().getCloud();
                if ( !cloud1.supportsBugLinks()) return;
                try {
                    java.net.URL u = cloud1.getBugLink(bug);
                    if (u != null) {
                        if (edu.umd.cs.findbugs.util.LaunchBrowser.showDocument(u)) {
                            cloud1.bugFiled(bug,null);
                            edu.umd.cs.findbugs.gui2.MainFrame.getInstance().syncBugInformation();
                        }
                    }
                }
                catch (java.lang.Exception e1){
                    LOGGER.log(java.util.logging.Level.SEVERE,"Could not view/file bug",e1);
                    javax.swing.JOptionPane.showMessageDialog(edu.umd.cs.findbugs.gui2.MainFrame.getInstance(),"Could not view/file bug:\n" + e1.getClass().getSimpleName() + ": " + e1.getMessage());
                }
            }
        });
        java.awt.GridBagConstraints c = new java.awt.GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.fill = java.awt.GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        commentsPane = new edu.umd.cs.findbugs.gui2.CloudCommentsPaneSwing();
        mainPanel.add(new javax.swing.JScrollPane(commentsPane),c);
        c.gridy++;
        c.weightx = 0;
        c.weighty = 0;
        c.fill = java.awt.GridBagConstraints.NONE;
        c.anchor = java.awt.GridBagConstraints.EAST;
        mainPanel.add(fileBug,c);
        return mainPanel;
    }
    void updateCommentsFromLeafInformation(final edu.umd.cs.findbugs.gui2.BugLeafNode node) {
        if (node == null) return;
        javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
            public void run() {
                edu.umd.cs.findbugs.BugInstance bug = node.getBug();
                edu.umd.cs.findbugs.cloud.Cloud plugin = edu.umd.cs.findbugs.gui2.CommentsArea.this.getCloud();
                if (plugin.supportsBugLinks()) {
                    currentBugStatus = plugin.getBugLinkStatus(bug);
                    fileBug.setText(currentBugStatus.toString());
                    fileBug.setToolTipText(currentBugStatus == edu.umd.cs.findbugs.cloud.Cloud.BugFilingStatus.FILE_BUG ? "Click to file bug for this issue" : "");
                    fileBug.setEnabled(currentBugStatus.linkEnabled());
                    fileBug.setVisible(true);
                }
                else {
                    fileBug.setVisible(false);
                }
                commentsPane.setBugInstance(node.getBug());
            }
        });
    }
    private edu.umd.cs.findbugs.SortedBugCollection getBugCollection() {
        return (edu.umd.cs.findbugs.SortedBugCollection) (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getBugCollection()) ;
    }
    void updateCommentsFromNonLeafInformation(final edu.umd.cs.findbugs.gui2.BugAspects theAspects) {
        javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
            public void run() {
                edu.umd.cs.findbugs.gui2.CommentsArea.this.updateCommentsFromNonLeafInformationFromSwingThread(theAspects);
            }
        });
    }
    public boolean canNavigateAway() {
        return commentsPane.canNavigateAway();
    }
    protected void updateCommentsFromNonLeafInformationFromSwingThread(edu.umd.cs.findbugs.gui2.BugAspects theAspects) {
        commentsPane.setBugAspects(theAspects);
        fileBug.setEnabled(false);
    }
    public boolean hasFocus() {
        return commentsPane.hasFocus();
    }
    private edu.umd.cs.findbugs.cloud.Cloud getCloud() {
        edu.umd.cs.findbugs.gui2.MainFrame instance = edu.umd.cs.findbugs.gui2.MainFrame.getInstance();
        edu.umd.cs.findbugs.BugCollection bugCollection = instance.getBugCollection();
        if (bugCollection == null) return null;
        return bugCollection.getCloud();
    }
    public void updateBugCollection() {
        commentsPane.setBugCollection(this.getBugCollection());
    }
    public void refresh() {
        commentsPane.refresh();
    }
    public void setDesignation(java.lang.String designationKey) {
        commentsPane.setDesignation(designationKey);
    }
    public void updateCloud() {
        commentsPane.updateCloud();
    }
}
