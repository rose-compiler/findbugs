package edu.umd.cs.findbugs.config;
import edu.umd.cs.findbugs.config.*;
import junit.framework.Assert;
import junit.framework.TestCase;
public class UserPreferencesTest extends junit.framework.TestCase {
    public UserPreferencesTest() {
    }
    edu.umd.cs.findbugs.config.UserPreferences prefs;
    protected void setUp() throws java.lang.Exception {
        prefs = edu.umd.cs.findbugs.config.UserPreferences.createDefaultUserPreferences();
    }
    public void testClone() {
        edu.umd.cs.findbugs.config.UserPreferences clone = (edu.umd.cs.findbugs.config.UserPreferences) (prefs.clone()) ;
        junit.framework.Assert.assertEquals(prefs,clone);
        junit.framework.Assert.assertEquals(prefs.getClass(),clone.getClass());
    }
}
