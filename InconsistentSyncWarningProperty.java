/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Warning properties for inconsistent synchronization detector.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import edu.umd.cs.findbugs.props.AbstractWarningProperty;
import edu.umd.cs.findbugs.props.PriorityAdjustment;
public class InconsistentSyncWarningProperty extends edu.umd.cs.findbugs.props.AbstractWarningProperty {
    public InconsistentSyncWarningProperty(java.lang.String name, edu.umd.cs.findbugs.props.PriorityAdjustment priorityAdjustment) {
        super(name,priorityAdjustment);
    }
// /** Field is never accessed while locked. */
// public static final InconsistentSyncWarningProperty NEVER_LOCKED =
// new InconsistentSyncWarningProperty("NEVER_LOCKED",
// PriorityAdjustment.FALSE_POSITIVE);
// /** Field is never accessed while unlocked. */
// public static final InconsistentSyncWarningProperty NEVER_UNLOCKED =
// new InconsistentSyncWarningProperty("NEVER_UNLOCKED",
// PriorityAdjustment.FALSE_POSITIVE);
/**
     * Field is accessed unlocked most of the time, and therefore is probably
     * not intended to be safely used from multiple threads.
     */
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty MANY_BIASED_UNLOCKED = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("MANY_BIASED_UNLOCKED", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** Field is never written outside constructor. */
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty NEVER_WRITTEN = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("NEVER_WRITTEN", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** Field is never read outside constructor. */
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty NEVER_READ = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("NEVER_READ", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/**
     * Field is never locked in the definition of the class. (I.e., all locked
     * accesses are in methods of other classes.)
     */
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty NO_LOCAL_LOCKS = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("NO_LOCAL_LOCKS", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** Below minimum percentage synchronized accesses. */
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty BELOW_MIN_SYNC_PERCENT = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("BELOW_MIN_SYNC_PERCENT", edu.umd.cs.findbugs.props.PriorityAdjustment.FALSE_POSITIVE);
/** The only unlocked accesses are in getter methods. */
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty ONLY_UNSYNC_IN_GETTERS = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("ONLY_UNSYNC_IN_GETTERS", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty ANNOTATED_AS_GUARDED_BY_THIS = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("ANNOTATED_AS_GUARDED_BY_THIS", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY_TO_AT_LEAST_NORMAL);
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty MUTABLE_SERVLET_FIELD = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("MUTABLE_SERVLET_FIELD", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY_TO_AT_LEAST_NORMAL);
    final public static edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty ANNOTATED_AS_THREAD_SAFE = new edu.umd.cs.findbugs.detect.InconsistentSyncWarningProperty("ANNOTATED_AS_THREAD_SAFE", edu.umd.cs.findbugs.props.PriorityAdjustment.RAISE_PRIORITY);
}
