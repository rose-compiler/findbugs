/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pwilliam
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import edu.umd.cs.findbugs.ba.ch.Subtypes2;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
public class TestCaseDetector extends java.lang.Object {
    public TestCaseDetector() {
    }
    final private static edu.umd.cs.findbugs.classfile.ClassDescriptor JUNIT4TEST = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor("org/junit/Test");
    final private static edu.umd.cs.findbugs.classfile.ClassDescriptor JUNIT3TESTCASE = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor("junit/framework/TestCase");
    public static boolean likelyTestCase(edu.umd.cs.findbugs.ba.XMethod m) {
        if (m.getAnnotation(JUNIT4TEST) != null) return true;
        edu.umd.cs.findbugs.classfile.ClassDescriptor c = m.getClassDescriptor();
        if (m.getName().startsWith("test") || m.getName().startsWith("assert")) {
            edu.umd.cs.findbugs.ba.ch.Subtypes2 subtypes2 = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getSubtypes2();
            try {
                if (subtypes2.isSubtype(c,JUNIT3TESTCASE)) return true;
            }
            catch (java.lang.ClassNotFoundException e){
                edu.umd.cs.findbugs.ba.AnalysisContext.reportMissingClass(e);
            }
        }
        return false;
    }
}
