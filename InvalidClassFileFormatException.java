/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile;
import edu.umd.cs.findbugs.classfile.*;
public class InvalidClassFileFormatException extends edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
    private edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor;
    private edu.umd.cs.findbugs.classfile.ICodeBaseEntry codeBaseEntry;
    public InvalidClassFileFormatException(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor, edu.umd.cs.findbugs.classfile.ICodeBaseEntry codeBaseEntry) {
        super("Invalid classfile format");
        this.classDescriptor = classDescriptor;
        this.codeBaseEntry = codeBaseEntry;
    }
    public InvalidClassFileFormatException(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor, edu.umd.cs.findbugs.classfile.ICodeBaseEntry codeBaseEntry, java.lang.Throwable cause) {
        super("Invalid classfile format",cause);
        this.classDescriptor = classDescriptor;
        this.codeBaseEntry = codeBaseEntry;
    }
    public InvalidClassFileFormatException(java.lang.String msg, edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor, edu.umd.cs.findbugs.classfile.ICodeBaseEntry codeBaseEntry) {
        super(msg);
        this.classDescriptor = classDescriptor;
        this.codeBaseEntry = codeBaseEntry;
    }
/**
     * @return Returns the ClassDescriptor of the class found to have an invalid
     *         format (null if unknown)
     */
    public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() {
        return classDescriptor;
    }
/**
     * @return Returns the codeBaseEntry.
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseEntry getCodeBaseEntry() {
        return codeBaseEntry;
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Throwable#getMessage()
     */
    public java.lang.String getMessage() {
        return super.getMessage() + " in " + codeBaseEntry;
    }
}
