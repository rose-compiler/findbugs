/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A dataflow analysis to be used with the {@link Dataflow} class.
 * 
 * <p>
 * In order to avoid duplicating functionality (such as caching of start and
 * result facts), most analyses should extend the
 * {@link BasicAbstractDataflowAnalysis} or {@link AbstractDataflowAnalysis}
 * classes rather than directly implementing this interface.
 * </p>
 * 
 * @author David Hovemeyer
 * @see Dataflow
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import javax.annotation.CheckForNull;
import org.apache.bcel.generic.InstructionHandle;
abstract public interface DataflowAnalysis<Fact> {
/**
     * Create empty (uninitialized) dataflow facts for one program point. A
     * valid value will be copied into it before it is used.
     */
    abstract public Fact createFact();
/**
     * Get the start fact for given basic block.
     * 
     * @param block
     *            the basic block
     */
    abstract public Fact getStartFact(edu.umd.cs.findbugs.ba.BasicBlock block);
/**
     * Get the result fact for given basic block.
     * 
     * @param block
     *            the basic block
     */
    abstract public Fact getResultFact(edu.umd.cs.findbugs.ba.BasicBlock block);
/**
     * Get dataflow fact at (just before) given Location. Note "before" is meant
     * in the logical sense, so for backward analyses, before means after the
     * location in the control flow sense.
     * 
     * @param location
     *            the Location
     * @return the dataflow value at given Location
     * @throws DataflowAnalysisException
     */
    abstract public Fact getFactAtLocation(edu.umd.cs.findbugs.ba.Location location) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException;
/**
     * Get the dataflow fact representing the point just after given Location.
     * Note "after" is meant in the logical sense, so for backward analyses,
     * after means before the location in the control flow sense.
     * 
     * @param location
     *            the Location
     * @return the dataflow value after given Location
     * @throws DataflowAnalysisException
     */
    abstract public Fact getFactAfterLocation(edu.umd.cs.findbugs.ba.Location location) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException;
/**
     * Get the fact that is true on the given control edge.
     * 
     * @param edge
     *            the edge
     * @return the fact that is true on the edge
     * @throws DataflowAnalysisException
     */
    abstract public Fact getFactOnEdge(edu.umd.cs.findbugs.ba.Edge edge) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException;
/**
     * Copy dataflow facts.
     */
    abstract public void copy(Fact source, Fact dest);
/**
     * Initialize the "entry" fact for the graph.
     */
    abstract public void initEntryFact(Fact result) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException;
/**
     * Make given fact the top value.
     */
    abstract public void makeFactTop(Fact fact);
/**
     * Is the given fact the top value.
     */
    abstract public boolean isTop(Fact fact);
/**
     * Returns true if the analysis is forwards, false if backwards.
     */
    abstract public boolean isForwards();
/**
     * Return the BlockOrder specifying the order in which BasicBlocks should be
     * visited in the main dataflow loop.
     * 
     * @param cfg
     *            the CFG upon which we're performing dataflow analysis
     */
    abstract public edu.umd.cs.findbugs.ba.BlockOrder getBlockOrder(edu.umd.cs.findbugs.ba.CFG cfg);
/**
     * Are given dataflow facts the same?
     */
    abstract public boolean same(Fact fact1, Fact fact2);
/**
     * Transfer function for the analysis. Taking dataflow facts at start (which
     * might be either the entry or exit of the block, depending on whether the
     * analysis is forwards or backwards), modify result to be the facts at the
     * other end of the block.
     * 
     * @param basicBlock
     *            the basic block
     * @param end
     *            if nonnull, stop before considering this instruction;
     *            otherwise, consider all of the instructions in the basic block
     * @param start
     *            dataflow facts at beginning of block (if forward analysis) or
     *            end of block (if backwards analysis)
     * @param result
     *            resulting dataflow facts at other end of block
     */
    abstract public void transfer(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle end, Fact start, Fact result) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException;
/**
     * Edge transfer function. Modify the given fact that is true on the
     * (logical) edge source to modify it so that it is true at the (logical)
     * edge target.
     * 
     * <p>
     * A do-nothing implementation is legal, and appropriate for analyses where
     * branches are not significant.
     * </p>
     * 
     * @param edge
     *            the Edge
     * @param fact
     *            a dataflow fact
     * @throws DataflowAnalysisException
     */
    abstract public void edgeTransfer(edu.umd.cs.findbugs.ba.Edge edge, Fact fact) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException;
/**
     * Meet a dataflow fact associated with an incoming edge into another fact.
     * This is used to determine the start fact for a basic block.
     * 
     * @param fact
     *            the predecessor fact (incoming edge)
     * @param edge
     *            the edge from the predecessor
     * @param result
     *            the result fact
     */
    abstract public void meetInto(Fact fact, edu.umd.cs.findbugs.ba.Edge edge, Fact result) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException;
/**
     * Called before beginning an iteration of analysis. Each iteration visits
     * every basic block in the CFG.
     */
    abstract public void startIteration();
/**
     * Called after finishing an iteration of analysis.
     */
    abstract public void finishIteration();
    abstract public int getLastUpdateTimestamp(Fact fact);
    abstract public void setLastUpdateTimestamp(Fact fact, int timestamp);
/**
     * Return a String representation of given Fact. For debugging purposes.
     * 
     * @param fact
     *            a dataflow fact
     * @return String representation of the fact
     */
    abstract public java.lang.String factToString(Fact fact);
}
// vim:ts=4
