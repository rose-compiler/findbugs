/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.bugReporter;
import edu.umd.cs.findbugs.bugReporter.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.HashSet;
import javax.annotation.Nonnull;
import javax.annotation.WillClose;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.ClassAnnotation;
import edu.umd.cs.findbugs.ComponentPlugin;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.charsets.UserTextFile;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
public class SuppressionDecorator extends edu.umd.cs.findbugs.bugReporter.BugReporterDecorator {
    final java.lang.String category;
    final java.util.HashSet<java.lang.String> check = new java.util.HashSet<java.lang.String>();
    final java.util.HashSet<java.lang.String> dontCheck = new java.util.HashSet<java.lang.String>();
    public SuppressionDecorator(edu.umd.cs.findbugs.ComponentPlugin<edu.umd.cs.findbugs.bugReporter.BugReporterDecorator> plugin, edu.umd.cs.findbugs.BugReporter delegate) {
        super(plugin,delegate);
        category = plugin.getProperties().getProperty("category");
        if (edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getBugCategory(category) == null) {
            throw new java.lang.IllegalArgumentException("Unable to find category " + category);
        }
        final java.lang.String adjustmentSource = plugin.getProperties().getProperty("packageSource");
        java.lang.String packageList = plugin.getProperties().getProperty("packageList");
        try {
            if (packageList != null) {
                this.processPackageList(new java.io.StringReader(packageList));
            }
            if (adjustmentSource != null) {
                java.net.URL u;
                if (adjustmentSource.startsWith("file:") || adjustmentSource.startsWith("http:") || adjustmentSource.startsWith("https:")) u = new java.net.URL(adjustmentSource);
                else {
                    u = plugin.getPlugin().getResource(adjustmentSource);
                    if (u == null) u = edu.umd.cs.findbugs.DetectorFactoryCollection.getCoreResource(adjustmentSource);
                }
                if (u != null) {
                    java.io.Reader rawIn = edu.umd.cs.findbugs.charsets.UserTextFile.bufferedReader(u.openStream());
                    this.processPackageList(rawIn);
                }
            }
        }
        catch (java.io.IOException e){
            throw new java.lang.RuntimeException("Unable to load " + category + " filters from " + adjustmentSource, e);
        }
    }
/**
     * @param rawIn
     * @throws IOException
     */
    private void processPackageList(java.io.Reader rawIn) throws java.io.IOException {
        try {
            java.io.BufferedReader in = new java.io.BufferedReader(rawIn);
            while (true) {
                java.lang.String s = in.readLine();
                if (s == null) break;
                s = s.trim();
                if (s.length() == 0) continue;
                java.lang.String packageName = s.substring(1).trim();
                if (s.charAt(0) == '\u002b') {
                    check.add(packageName);
                    dontCheck.remove(packageName);
                }
                else if (s.charAt(0) == '\u002d') {
                    dontCheck.add(packageName);
                    check.remove(packageName);
                }
                else throw new java.lang.IllegalArgumentException("Can't parse " + category + " filter line: " + s);
            }
        }
        finally {
            rawIn.close();
        }
    }
    public void reportBug(edu.umd.cs.findbugs.BugInstance bugInstance) {
        if ( !category.equals(bugInstance.getBugPattern().getCategory())) {
            this.getDelegate().reportBug(bugInstance);
            return;
        }
        if (check.isEmpty()) return;
        edu.umd.cs.findbugs.ClassAnnotation c = bugInstance.getPrimaryClass();
        java.lang.String packageName = c.getPackageName();
        while (true) {
            if (check.contains(packageName)) {
                this.getDelegate().reportBug(bugInstance);
                return;
            }
            else if (dontCheck.contains(packageName)) {
                return;
            }
            int i = packageName.lastIndexOf('\u002e');
            if (i < 0) return;
            packageName = packageName.substring(0,i);
        }
    }
}
