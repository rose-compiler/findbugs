/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Factory to create codebase/classpath/classfile objects.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.impl;
import edu.umd.cs.findbugs.classfile.impl.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.IClassFactory;
import edu.umd.cs.findbugs.classfile.IClassPath;
import edu.umd.cs.findbugs.classfile.IClassPathBuilder;
import edu.umd.cs.findbugs.classfile.ICodeBase;
import edu.umd.cs.findbugs.classfile.ICodeBaseLocator;
import edu.umd.cs.findbugs.classfile.IErrorLogger;
import edu.umd.cs.findbugs.classfile.IScannableCodeBase;
import edu.umd.cs.findbugs.classfile.ResourceNotFoundException;
public class ClassFactory extends java.lang.Object implements edu.umd.cs.findbugs.classfile.IClassFactory {
    private static edu.umd.cs.findbugs.classfile.IClassFactory theInstance = new edu.umd.cs.findbugs.classfile.impl.ClassFactory();
    public ClassFactory() {
        super();
    }
    public static edu.umd.cs.findbugs.classfile.IClassFactory instance() {
        return theInstance;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.impl.IClassFactory#createClassPath()
     */
    public edu.umd.cs.findbugs.classfile.IClassPath createClassPath() {
        return new edu.umd.cs.findbugs.classfile.impl.ClassPathImpl();
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IClassFactory#createClassPathBuilder(edu
     * .umd.cs.findbugs.classfile.IErrorLogger)
     */
    public edu.umd.cs.findbugs.classfile.IClassPathBuilder createClassPathBuilder(edu.umd.cs.findbugs.classfile.IErrorLogger errorLogger) {
        return new edu.umd.cs.findbugs.classfile.impl.ClassPathBuilder(this, errorLogger);
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.impl.IClassFactory#
     * createFilesystemCodeBaseLocator(java.lang.String)
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseLocator createFilesystemCodeBaseLocator(java.lang.String pathName) {
// Attempt to canonicalize the pathname.
// It's not fatal if we can't.
        try {
            pathName = new java.io.File(pathName).getCanonicalPath();
        }
        catch (java.io.IOException e){
        }
        return new edu.umd.cs.findbugs.classfile.impl.FilesystemCodeBaseLocator(pathName);
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.IClassFactory#
     * createNestedArchiveCodeBaseLocator
     * (edu.umd.cs.findbugs.classfile.ICodeBase, java.lang.String)
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseLocator createNestedArchiveCodeBaseLocator(edu.umd.cs.findbugs.classfile.ICodeBase parentCodeBase, java.lang.String path) {
        return new edu.umd.cs.findbugs.classfile.impl.NestedZipFileCodeBaseLocator(parentCodeBase, path);
    }
    static edu.umd.cs.findbugs.classfile.IScannableCodeBase createFilesystemCodeBase(edu.umd.cs.findbugs.classfile.impl.FilesystemCodeBaseLocator codeBaseLocator) throws java.io.IOException {
        java.lang.String fileName = codeBaseLocator.getPathName();
        java.io.File file = new java.io.File(fileName);
        if ( !file.exists()) {
            throw new java.io.FileNotFoundException("File " + file.getAbsolutePath() + " doesn't exist");
        }
        else if ( !file.canRead()) {
            throw new java.io.IOException("File " + file.getAbsolutePath() + " not readable");
        }
        else if (file.isDirectory()) {
            return new edu.umd.cs.findbugs.classfile.impl.DirectoryCodeBase(codeBaseLocator, file);
        }
        else if ( !file.isFile()) {
            throw new java.io.IOException("File " + file.getAbsolutePath() + " is not a normal file");
        }
        else if (fileName.endsWith(".class")) {
            return new edu.umd.cs.findbugs.classfile.impl.SingleFileCodeBase(codeBaseLocator, fileName);
        }
        else {
            return edu.umd.cs.findbugs.classfile.impl.ZipCodeBaseFactory.makeZipCodeBase(codeBaseLocator,file);
        }
    }
    static edu.umd.cs.findbugs.classfile.IScannableCodeBase createNestedZipFileCodeBase(edu.umd.cs.findbugs.classfile.impl.NestedZipFileCodeBaseLocator codeBaseLocator) throws java.io.IOException, edu.umd.cs.findbugs.classfile.ResourceNotFoundException {
        return new edu.umd.cs.findbugs.classfile.impl.NestedZipFileCodeBase(codeBaseLocator);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IClassFactory#createAnalysisCache(edu.umd
     * .cs.findbugs.classfile.IClassPath)
     */
    public edu.umd.cs.findbugs.classfile.IAnalysisCache createAnalysisCache(edu.umd.cs.findbugs.classfile.IClassPath classPath, edu.umd.cs.findbugs.BugReporter errorLogger) {
        edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache = new edu.umd.cs.findbugs.classfile.impl.AnalysisCache(classPath, errorLogger);
        return analysisCache;
    }
}
