package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Determine which methods are accessors used by inner classes to access fields
 * in their enclosing classes. This has been tested with javac from the Sun JDK
 * 1.4.x, but will probably not work with other source to bytecode compilers.
 *
 * <p>
 * The instance of InnerClassAccessMap should be retrieved from the
 * AnalysisContext.
 * </p>
 *
 * @author David Hovemeyer
 * @see InnerClassAccess
 */
/*
     * ----------------------------------------------------------------------
     * Fields
     * ----------------------------------------------------------------------
     */
/**
     * Map of class names to maps of method names to InnerClassAccess objects
     * representing access methods.
     */
/*
     * ----------------------------------------------------------------------
     * Public interface
     * ----------------------------------------------------------------------
     */
/**
     * Create an instance.
     *
     * @return a new instance of InnerClassAccessMap
     */
/**
     * Get the InnerClassAccess in given class with the given method name.
     *
     * @param className
     *            the name of the class
     * @param methodName
     *            the name of the access method
     * @return the InnerClassAccess object for the method, or null if the method
     *         doesn't seem to be an inner class access
     */
/**
     * Get the inner class access object for given invokestatic instruction.
     * Returns null if the called method is not an inner class access.
     *
     * @param inv
     *            the invokestatic instruction
     * @param cpg
     *            the ConstantPoolGen for the method
     * @return the InnerClassAccess, or null if the call is not an inner class
     *         access
     */
/**
     * Clear the cache.
     */
/*
     * ----------------------------------------------------------------------
     * Implementation
     * ----------------------------------------------------------------------
     */
/**
     * Constructor.
     */
/**
     * Convert byte to unsigned int.
     */
/**
     * Get an unsigned 16 bit constant pool index from a byte array.
     */
/**
         *
         */
/**
     * Callback to scan an access method to determine what field it accesses,
     * and whether the field is loaded or stored.
     */
/**
         * Constructor.
         *
         * @param javaClass
         *            the class containing the access method
         * @param methodName
         *            the name of the access method
         * @param methodSig
         *            the signature of the access method
         * @param instructionList
         *            the bytecode of the method
         */
/**
         * Get the InnerClassAccess object representing the method.
         *
         * @return the InnerClassAccess, or null if the method was not found to
         *         be a simple load or store in the expected form
         */
/**
         * Called to indicate that a field load or store was encountered.
         *
         * @param cpIndex
         *            the constant pool index of the fieldref
         * @param isStatic
         *            true if it is a static field access
         * @param isLoad
         *            true if the access is a load
         */
// We only allow one field access for an accessor method.
/**
         * Determine if the method appears to be an accessor of the expected
         * form. This has only been tested with the Sun JDK 1.4 javac
         * (definitely) and jikes 1.18 (I think).
         *
         * @param methodSig
         *            the method's signature
         * @param field
         *            the field accessed by the method
         * @param isLoad
         *            true if the access is a load
         */
// Get the method parameters and return type
// (as they appear in the method signature).
// Figure out what the expected method parameters should be
// the OuterClass.this reference
// the value being stored
// See if params match
// Return type can be either the type of the field, or void.
/**
     * Return a map of inner-class member access method names to the fields that
     * they access for given class name.
     *
     * @param className
     *            the name of the class
     * @return map of access method names to the fields they access
     */
// vim:ts=4
abstract interface package-info {
}
