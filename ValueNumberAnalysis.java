package edu.umd.cs.findbugs.ba.vna;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.vna.*;
/**
 * A dataflow analysis to track the production and flow of values in the Java
 * stack frame. See the {@link ValueNumber ValueNumber} class for an explanation
 * of what the value numbers mean, and when they can be compared.
 * 
 * <p>
 * This class is still experimental.
 * 
 * @author David Hovemeyer
 * @see ValueNumber
 * @see edu.umd.cs.findbugs.ba.DominatorsAnalysis
 */
// For non-static methods, keep track of which value represents the
// "this" reference
/**
     * Get the value number assigned to the given local variable upon entry to
     * the method.
     * 
     * @param local
     *            local variable number
     * @return ValueNumber assigned to the local variable
     */
/**
     * Get the value number assigned to the given parameter upon entry to the
     * method.
     * 
     * @param param
     *            a parameter (0 == first parameter)
     * @return the ValueNumber assigned to that parameter
     */
// Change the frame from TOP to something valid.
// At entry to the method, each local has (as far as we know) a unique
// value.
// Special case: when merging predecessor facts for entry to
// an exception handler, we clear the stack and push a
// single entry for the exception object. That way, the locals
// can still be merged.
// Get the value number for the exception
// Set up the stack frame
// Merging slot values:
// - Merging identical values results in no change
// - If the values are different, and the value in the result
// frame is not the result of a previous result, a fresh value
// is allocated.
// - If the value in the result frame is the result of a
// previous merge, IT STAYS THE SAME.
//
// The "one merge" rule means that merged values are essentially like
// phi nodes. They combine some number of other values.
// I believe (but haven't proved) that this technique is a dumb way
// of computing SSA.
/**
     * Get an Iterator over all dataflow facts that we've recorded for the
     * Locations in the CFG. Note that this does not include result facts (since
     * there are no Locations corresponding to the end of basic blocks).
     */
// These fields are used by the compactValueNumbers() method.
// The "discovered" array tells us the mapping of old value numbers
// to new (which are based on order of discovery). Negative values
// specify value numbers which are not actually used (and thus can
// be purged.)
/**
     * Compact the value numbers assigned. This should be done only after the
     * dataflow algorithm has executed. This works by modifying the actual
     * ValueNumber objects assigned. After this method is called, the
     * getNumValuesAllocated() method of this object will return a value less
     * than or equal to the value it would have returned before the call to this
     * method.
     * <p/>
     * <p>
     * <em>This method should be called at most once</em>.
     * 
     * @param dataflow
     *            the Dataflow object which executed this analysis (and has all
     *            of the block result values)
     */
// We can get all extant Frames by looking at the values in
// the location to value map, and also the block result values.
// Now the factory can modify the ValueNumbers.
/**
     * Mark value numbers in a value number frame for compaction.
     */
// We don't need to do anything for top and bottom frames.
// /**
// * Test driver.
// */
// public static void main(String[] argv) throws Exception {
//
// if (argv.length != 1) {
// System.out.println("Usage: edu.umd.cs.findbugs.ba.ValueNumberAnalysis <filename>");
// System.exit(1);
// }
//
// DataflowTestDriver<ValueNumberFrame, ValueNumberAnalysis> driver =
// new DataflowTestDriver<ValueNumberFrame, ValueNumberAnalysis>() {
// @Override
// public Dataflow<ValueNumberFrame, ValueNumberAnalysis>
// createDataflow(ClassContext classContext, Method method)
// throws CFGBuilderException, DataflowAnalysisException {
// return classContext.getValueNumberDataflow(method);
// }
// };
//
// driver.execute(argv[0]);
// }
// vim:ts=4
abstract interface package-info {
}
