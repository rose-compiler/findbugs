/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.ba.BytecodeScanner;
import edu.umd.cs.findbugs.ba.MethodBytecodeSet;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class MethodBytecodeSetFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.MethodBytecodeSet> {
/**
     * @param analysisName
     * @param analysisClass
     */
    public MethodBytecodeSetFactory(java.lang.String analysisName, java.lang.Class<edu.umd.cs.findbugs.ba.MethodBytecodeSet> analysisClass) {
        super(analysisName,analysisClass);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.MethodBytecodeSet analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.classfile.Method method = analysisCache.getMethodAnalysis(org.apache.bcel.classfile.Method.class,descriptor);
        org.apache.bcel.classfile.Code code = method.getCode();
        if (code == null) {
            return null;
        }
        byte[] instructionList = code.getCode();
// Create callback
        edu.umd.cs.findbugs.classfile.engine.bcel.UnpackedBytecodeCallback callback = new edu.umd.cs.findbugs.classfile.engine.bcel.UnpackedBytecodeCallback(instructionList.length);
// Scan the method.
        edu.umd.cs.findbugs.ba.BytecodeScanner scanner = new edu.umd.cs.findbugs.ba.BytecodeScanner();
        scanner.scan(instructionList,callback);
        edu.umd.cs.findbugs.classfile.engine.bcel.UnpackedCode unpackedCode = callback.getUnpackedCode();
        edu.umd.cs.findbugs.ba.MethodBytecodeSet result = null;
        if (unpackedCode != null) {
            result = unpackedCode.getBytecodeSet();
        }
        return result;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#registerWith(edu.umd.cs
     * .findbugs.classfile.IAnalysisCache)
     */
    public void registerWith(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache) {
        analysisCache.registerMethodAnalysisEngine(edu.umd.cs.findbugs.ba.MethodBytecodeSet.class,this);
    }
}
