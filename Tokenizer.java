/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A simple tokenizer for Java source text. This is not intended to be a
 * compliant lexer; instead, it is for quick and dirty scanning.
 * 
 * @author David Hovemeyer
 * @see Token
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.BitSet;
public class Tokenizer extends java.lang.Object {
    final private static java.util.BitSet whiteSpace = new java.util.BitSet();
    static {
        whiteSpace.set('\u0020');
        whiteSpace.set('\t');
        whiteSpace.set('\r');
        whiteSpace.set('\f');
    }
    final private static java.util.BitSet single = new java.util.BitSet();
    static {
        single.set('\u0021');
        single.set('\u0025');
        single.set('\u005e');
        single.set('\u0026');
        single.set('\u002a');
        single.set('\u0028');
        single.set('\u0029');
        single.set('\u002d');
        single.set('\u002b');
        single.set('\u003d');
        single.set('\u005b');
        single.set('\u005d');
        single.set('\u007b');
        single.set('\u007d');
        single.set('\u007c');
        single.set('\u003a');
        single.set('\u003b');
        single.set('\u002c');
        single.set('\u002e');
        single.set('\u003c');
        single.set('\u003e');
        single.set('\u003f');
        single.set('\u007e');
    }
    private java.io.PushbackReader reader;
/**
     * Constructor.
     * 
     * @param reader
     *            the Reader for the Java source text
     */
    public Tokenizer(java.io.Reader reader) {
        super();
        this.reader = new java.io.PushbackReader(reader);
    }
/**
     * Get the next Token in the stream.
     * 
     * @return the Token
     */
    public edu.umd.cs.findbugs.Token next() throws java.io.IOException {
        this.skipWhitespace();
        int c = reader.read();
        if (c < 0) return new edu.umd.cs.findbugs.Token(edu.umd.cs.findbugs.Token.EOF);
        else if (c == '\n') return new edu.umd.cs.findbugs.Token(edu.umd.cs.findbugs.Token.EOL);
        else if (c == '\'' || c == '"') return this.munchString(c);
        else if (c == '\u002f') return this.maybeComment();
        else if (single.get(c)) return new edu.umd.cs.findbugs.Token(edu.umd.cs.findbugs.Token.SINGLE, java.lang.String.valueOf((char) (c) ));
        else {
            reader.unread(c);
            return this.parseWord();
        }
    }
    private void skipWhitespace() throws java.io.IOException {
        for (; ; ) {
            int c = reader.read();
            if (c < 0) break;
            if ( !whiteSpace.get(c)) {
                reader.unread(c);
                break;
            }
        }
    }
    private edu.umd.cs.findbugs.Token munchString(int delimiter) throws java.io.IOException {
        final int SCAN = 0;
        final int ESCAPE = 1;
        final int DONE = 2;
        java.lang.StringBuilder result = new java.lang.StringBuilder();
        result.append((char) (delimiter) );
        int state = SCAN;
        while (state != DONE) {
            int c = reader.read();
            if (c < 0) break;
            result.append((char) (c) );
            switch(state){
                case SCAN:{
                    if (c == delimiter) state = DONE;
                    else if (c == '\\') state = ESCAPE;
                    break;
                }
                case ESCAPE:{
                    state = SCAN;
                    break;
                }
            }
        }
        return new edu.umd.cs.findbugs.Token(edu.umd.cs.findbugs.Token.STRING, result.toString());
    }
    private edu.umd.cs.findbugs.Token maybeComment() throws java.io.IOException {
        int c = reader.read();
        if (c == '\u002f') {
// Single line comment
            java.lang.StringBuilder result = new java.lang.StringBuilder();
            result.append("//");
            for (; ; ) {
                c = reader.read();
                if (c < 0) break;
                else if (c == '\n') {
                    reader.unread(c);
                    break;
                }
                result.append((char) (c) );
            }
            return new edu.umd.cs.findbugs.Token(edu.umd.cs.findbugs.Token.COMMENT, result.toString());
        }
        else if (c == '\u002a') {
// C-style multiline comment
            java.lang.StringBuilder result = new java.lang.StringBuilder();
            result.append("/*");
            final int SCAN = 0;
            final int STAR = 1;
            final int DONE = 2;
            int state = SCAN;
            while (state != DONE) {
                c = reader.read();
                if (c < 0) state = DONE;
                else result.append((char) (c) );
                switch(state){
                    case SCAN:{
                        if (c == '\u002a') state = STAR;
                        break;
                    }
                    case STAR:{
                        if (c == '\u002f') state = DONE;
                        else if (c != '\u002a') state = SCAN;
                        break;
                    }
                    case DONE:{
                        break;
                    }
                }
            }
            return new edu.umd.cs.findbugs.Token(edu.umd.cs.findbugs.Token.COMMENT, result.toString());
        }
        else {
            if (c >= 0) reader.unread(c);
            return new edu.umd.cs.findbugs.Token(edu.umd.cs.findbugs.Token.SINGLE, "/");
        }
    }
    private edu.umd.cs.findbugs.Token parseWord() throws java.io.IOException {
        java.lang.StringBuilder result = new java.lang.StringBuilder();
        for (; ; ) {
            int c = reader.read();
            if (c < 0) break;
            if (whiteSpace.get(c) || c == '\n' || single.get(c)) {
                reader.unread(c);
                break;
            }
            result.append((char) (c) );
        }
        return new edu.umd.cs.findbugs.Token(edu.umd.cs.findbugs.Token.WORD, result.toString());
    }
}
// vim:ts=4
