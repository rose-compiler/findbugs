/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import javax.annotation.CheckForNull;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.ba.XFactory;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.ba.ch.Subtypes2;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.MissingClassException;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
import edu.umd.cs.findbugs.internalAnnotations.SlashedClassName;
import edu.umd.cs.findbugs.visitclass.Constants2;
public class Lookup extends java.lang.Object implements edu.umd.cs.findbugs.visitclass.Constants2 {
    public Lookup() {
    }
    private static edu.umd.cs.findbugs.ba.ch.Subtypes2 subtypes2() {
        return edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getSubtypes2();
    }
    public static edu.umd.cs.findbugs.ba.XClass getXClass(edu.umd.cs.findbugs.classfile.ClassDescriptor c) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        return edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getClassAnalysis(edu.umd.cs.findbugs.ba.XClass.class,c);
    }
    public static edu.umd.cs.findbugs.ba.XClass getXClass(java.lang.String className) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        return edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getClassAnalysis(edu.umd.cs.findbugs.ba.XClass.class,edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(className));
    }
    public static edu.umd.cs.findbugs.ba.XClass findSuperImplementor(edu.umd.cs.findbugs.ba.XClass clazz, java.lang.String name, java.lang.String signature, boolean isStatic, edu.umd.cs.findbugs.BugReporter bugReporter) {
        try {
            return findSuperImplementor(clazz,name,signature,isStatic);
        }
        catch (edu.umd.cs.findbugs.classfile.MissingClassException e){
            bugReporter.reportMissingClass(e.getClassDescriptor());
            return clazz;
        }
        catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
            bugReporter.logError("Error finding " + clazz + "." + name + signature,e);
            return clazz;
        }
    }
    public static edu.umd.cs.findbugs.ba.XClass findImplementor(edu.umd.cs.findbugs.ba.XClass clazz, java.lang.String name, java.lang.String signature, boolean isStatic, edu.umd.cs.findbugs.BugReporter bugReporter) {
        try {
            return findImplementor(clazz,name,signature,isStatic);
        }
        catch (edu.umd.cs.findbugs.classfile.MissingClassException e){
            bugReporter.reportMissingClass(e.getClassDescriptor());
            return clazz;
        }
        catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
            bugReporter.logError("Error finding " + clazz + "." + name + signature,e);
            return clazz;
        }
    }
    public static edu.umd.cs.findbugs.ba.XClass findSuperImplementor(edu.umd.cs.findbugs.ba.XClass clazz, java.lang.String name, java.lang.String signature, boolean isStatic) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        edu.umd.cs.findbugs.classfile.ClassDescriptor superclassDescriptor = clazz.getSuperclassDescriptor();
        if (superclassDescriptor == null) return clazz;
        return findImplementor(getXClass(superclassDescriptor),name,signature,isStatic);
    }
    public static edu.umd.cs.findbugs.ba.XClass findImplementor(edu.umd.cs.findbugs.ba.XClass clazz, java.lang.String name, java.lang.String signature, boolean isStatic) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        edu.umd.cs.findbugs.ba.XMethod m = clazz.findMethod(name,signature,isStatic);
        if (m != null) return clazz;
        return findSuperImplementor(clazz,name,signature,isStatic);
    }
    public static org.apache.bcel.classfile.JavaClass findSuperDefiner(org.apache.bcel.classfile.JavaClass clazz, java.lang.String name, java.lang.String signature, edu.umd.cs.findbugs.BugReporter bugReporter) {
        try {
            org.apache.bcel.classfile.JavaClass c = clazz;
            while (true) {
                c = c.getSuperClass();
                if (c == null) return null;
                org.apache.bcel.classfile.Method m = findImplementation(c,name,signature);
                if (m != null) {
                    return c;
                }
            }
        }
        catch (java.lang.ClassNotFoundException e){
            bugReporter.reportMissingClass(e);
            return null;
        }
    }
    public static org.apache.bcel.classfile.JavaClass findSuperImplementor(org.apache.bcel.classfile.JavaClass clazz, java.lang.String name, java.lang.String signature, edu.umd.cs.findbugs.BugReporter bugReporter) {
        try {
            org.apache.bcel.classfile.JavaClass c = clazz;
            while (true) {
                c = c.getSuperClass();
                if (c == null) return null;
                org.apache.bcel.classfile.Method m = findImplementation(c,name,signature);
                if (m != null &&  !m.isAbstract()) return c;
            }
        }
        catch (java.lang.ClassNotFoundException e){
            bugReporter.reportMissingClass(e);
            return null;
        }
    }
    public static edu.umd.cs.findbugs.ba.XMethod findSuperImplementorAsXMethod(org.apache.bcel.classfile.JavaClass clazz, java.lang.String name, java.lang.String signature, edu.umd.cs.findbugs.BugReporter bugReporter) {
        try {
            org.apache.bcel.classfile.JavaClass c = clazz;
            while (true) {
                c = c.getSuperClass();
                if (c == null) return null;
                org.apache.bcel.classfile.Method m = findImplementation(c,name,signature);
                if (m != null &&  !m.isAbstract()) return edu.umd.cs.findbugs.ba.XFactory.createXMethod(c,m);
            }
        }
        catch (java.lang.ClassNotFoundException e){
            bugReporter.reportMissingClass(e);
            return null;
        }
    }
    public static java.lang.String findSuperImplementor(java.lang.String clazz, java.lang.String name, java.lang.String signature, edu.umd.cs.findbugs.BugReporter bugReporter) {
        try {
            org.apache.bcel.classfile.JavaClass c = findImplementor(org.apache.bcel.Repository.getSuperClasses(clazz),name,signature);
            return (c != null) ? c.getClassName() : clazz;
        }
        catch (java.lang.ClassNotFoundException e){
            bugReporter.reportMissingClass(e);
            return clazz;
        }
    }
    public static org.apache.bcel.classfile.JavaClass findImplementor(org.apache.bcel.classfile.JavaClass[] clazz, java.lang.String name, java.lang.String signature) {
        for (org.apache.bcel.classfile.JavaClass aClazz : clazz){
            org.apache.bcel.classfile.Method m = findImplementation(aClazz,name,signature);
            if (m != null &&  !m.isAbstract()) return aClazz;
        }
;
        return null;
    }
    public static org.apache.bcel.classfile.Method findImplementation(org.apache.bcel.classfile.JavaClass clazz, java.lang.String name, java.lang.String signature) {
        org.apache.bcel.classfile.Method[] m = clazz.getMethods();
        for (org.apache.bcel.classfile.Method aM : m)if (aM.getName().equals(name) && aM.getSignature().equals(signature) &&  !aM.isPrivate() &&  !aM.isStatic()) return aM;
;
        return null;
    }
}
