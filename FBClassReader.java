/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.asm;
import edu.umd.cs.findbugs.asm.*;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.MethodVisitor;
public class FBClassReader extends org.objectweb.asm.ClassReader {
// boolean needOffsets; // optional optimization (not thread safe)
// if (!needOffsets) return super.readLabel(offset, labels);
// needOffsets = mv instanceof MyMethodVisitor;
    private static class MyLabel extends org.objectweb.asm.Label {
        final int originalOffset;
        boolean realLabel;
        public MyLabel(int originalOffset) {
            super();
            this.originalOffset = originalOffset;
        }
    }
    private static class MyMethodAdapter extends org.objectweb.asm.MethodAdapter {
        public MyMethodAdapter(edu.umd.cs.findbugs.asm.FBMethodVisitor mv) {
            super(mv);
        }
        public void visitLabel(org.objectweb.asm.Label label) {
            assert label instanceof edu.umd.cs.findbugs.asm.FBClassReader.MyLabel;
            edu.umd.cs.findbugs.asm.FBClassReader.MyLabel l = (edu.umd.cs.findbugs.asm.FBClassReader.MyLabel) (label) ;
            ((edu.umd.cs.findbugs.asm.FBMethodVisitor) (mv) ).visitOffset(l.originalOffset);
            if (l.realLabel) {
                mv.visitLabel(label);
            }
        }
    }
    private static class MyClassAdapter extends org.objectweb.asm.ClassAdapter {
        public MyClassAdapter(org.objectweb.asm.ClassVisitor cv) {
            super(cv);
        }
        public org.objectweb.asm.MethodVisitor visitMethod(int access, java.lang.String name, java.lang.String desc, java.lang.String signature, java.lang.String[] exceptions) {
            org.objectweb.asm.MethodVisitor mv = super.visitMethod(access,name,desc,signature,exceptions);
            if (mv instanceof edu.umd.cs.findbugs.asm.FBMethodVisitor) {
                mv = new edu.umd.cs.findbugs.asm.FBClassReader.MyMethodAdapter((edu.umd.cs.findbugs.asm.FBMethodVisitor) (mv) );
            }
            return mv;
        }
    }
    public FBClassReader(byte[] b) {
        super(b);
    }
    public FBClassReader(byte[] b, int off, int len) {
        super(b,off,len);
    }
    public void accept(org.objectweb.asm.ClassVisitor cv, org.objectweb.asm.Attribute[] attrs, int flags) {
        super.accept(new edu.umd.cs.findbugs.asm.FBClassReader.MyClassAdapter(cv),attrs,flags);
    }
    protected org.objectweb.asm.Label readLabel(int offset, org.objectweb.asm.Label[] labels) {
        if (labels[offset] == null) {
            for (int i = 0; i < labels.length; ++i) {
                labels[i] = new edu.umd.cs.findbugs.asm.FBClassReader.MyLabel(i);
            }
        }
        ((edu.umd.cs.findbugs.asm.FBClassReader.MyLabel) (labels[offset]) ).realLabel = true;
        return labels[offset];
    }
}
