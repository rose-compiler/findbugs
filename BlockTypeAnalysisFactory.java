/**
 * Analysis engine for producing BlockTypeDataflow for an analyzed method.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import edu.umd.cs.findbugs.ba.BlockTypeAnalysis;
import edu.umd.cs.findbugs.ba.BlockTypeDataflow;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.DepthFirstSearch;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class BlockTypeAnalysisFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.BlockTypeDataflow> {
    public BlockTypeAnalysisFactory() {
        super("block type analysis",edu.umd.cs.findbugs.ba.BlockTypeDataflow.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.BlockTypeDataflow analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        edu.umd.cs.findbugs.ba.CFG cfg = this.getCFG(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.DepthFirstSearch dfs = this.getDepthFirstSearch(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.BlockTypeAnalysis analysis = new edu.umd.cs.findbugs.ba.BlockTypeAnalysis(dfs);
        edu.umd.cs.findbugs.ba.BlockTypeDataflow dataflow = new edu.umd.cs.findbugs.ba.BlockTypeDataflow(cfg, analysis);
        dataflow.execute();
        return dataflow;
    }
}
