/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Tests for Subtypes2.
 * 
 * @author Bill Pugh
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.ch;
import edu.umd.cs.findbugs.ba.ch.*;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.FindBugsTestCase;
import edu.umd.cs.findbugs.RunnableWithExceptions;
import edu.umd.cs.findbugs.ba.ObjectTypeFactory;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.detect.FindRefComparison;
public class Subtypes2Test extends edu.umd.cs.findbugs.FindBugsTestCase {
    public Subtypes2Test() {
    }
    org.apache.bcel.generic.ObjectType typeSerializable;
    org.apache.bcel.generic.ObjectType typeClonable;
    org.apache.bcel.generic.ObjectType typeObject;
    org.apache.bcel.generic.ObjectType typeInteger;
    org.apache.bcel.generic.ObjectType typeString;
    org.apache.bcel.generic.ObjectType typeList;
    org.apache.bcel.generic.ObjectType typeCollection;
    org.apache.bcel.generic.ObjectType typeHashSet;
    org.apache.bcel.generic.ArrayType typeArrayClonable;
    org.apache.bcel.generic.ObjectType typeComparable;
    org.apache.bcel.generic.ArrayType typeArrayObject;
    org.apache.bcel.generic.ArrayType typeArrayInteger;
    org.apache.bcel.generic.ArrayType typeArrayString;
    org.apache.bcel.generic.ArrayType typeArrayComparable;
    org.apache.bcel.generic.ArrayType typeArrayArrayObject;
    org.apache.bcel.generic.ArrayType typeArrayArraySerializable;
    org.apache.bcel.generic.ArrayType typeArrayArrayString;
    org.apache.bcel.generic.ArrayType typeArrayInt;
    org.apache.bcel.generic.ArrayType typeArrayArrayInt;
    org.apache.bcel.generic.ArrayType typeArrayArrayArrayInt;
    org.apache.bcel.generic.ArrayType typeArrayChar;
    org.apache.bcel.generic.ArrayType typeArrayArrayChar;
    org.apache.bcel.generic.ArrayType typeArrayArrayArrayChar;
    org.apache.bcel.generic.ObjectType typeDynamicString;
    org.apache.bcel.generic.ObjectType typeStaticString;
    org.apache.bcel.generic.ObjectType typeParameterString;
/*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws java.lang.Exception {
        super.setUp();
        typeSerializable = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.io.Serializable");
        typeClonable = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.lang.Cloneable");
        typeObject = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.lang.Object");
        typeInteger = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.lang.Integer");
        typeString = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.lang.String");
        typeComparable = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.lang.Comparable");
        typeList = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.util.List");
        typeCollection = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.util.Collection");
        typeHashSet = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance("java.util.HashSet");
        typeArrayClonable = new org.apache.bcel.generic.ArrayType(typeClonable, 1);
        typeArrayComparable = new org.apache.bcel.generic.ArrayType(typeComparable, 1);
        typeArrayObject = new org.apache.bcel.generic.ArrayType(typeObject, 1);
        typeArrayInteger = new org.apache.bcel.generic.ArrayType(typeInteger, 1);
        typeArrayString = new org.apache.bcel.generic.ArrayType(typeString, 1);
        typeArrayArrayObject = new org.apache.bcel.generic.ArrayType(typeObject, 2);
        typeArrayArraySerializable = new org.apache.bcel.generic.ArrayType(typeSerializable, 2);
        typeArrayArrayString = new org.apache.bcel.generic.ArrayType(typeString, 2);
        typeArrayInt = new org.apache.bcel.generic.ArrayType(org.apache.bcel.generic.Type.INT, 1);
        typeArrayArrayInt = new org.apache.bcel.generic.ArrayType(org.apache.bcel.generic.Type.INT, 2);
        typeArrayArrayArrayInt = new org.apache.bcel.generic.ArrayType(org.apache.bcel.generic.Type.INT, 3);
        typeArrayChar = new org.apache.bcel.generic.ArrayType(org.apache.bcel.generic.Type.CHAR, 1);
        typeArrayArrayChar = new org.apache.bcel.generic.ArrayType(org.apache.bcel.generic.Type.CHAR, 2);
        typeArrayArrayArrayChar = new org.apache.bcel.generic.ArrayType(org.apache.bcel.generic.Type.CHAR, 3);
        typeDynamicString = new edu.umd.cs.findbugs.detect.FindRefComparison.DynamicStringType();
        typeStaticString = new edu.umd.cs.findbugs.detect.FindRefComparison.StaticStringType();
        typeParameterString = new edu.umd.cs.findbugs.detect.FindRefComparison.ParameterStringType();
    }
    private static edu.umd.cs.findbugs.ba.ch.Subtypes2 getSubtypes2() {
        return edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getDatabase(edu.umd.cs.findbugs.ba.ch.Subtypes2.class);
    }
    public void testStringSubtypeOfObject() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertTrue(test.isSubtype(typeString,typeObject));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testStringSubtypeOfSerializable() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertTrue(test.isSubtype(typeString,typeSerializable));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testIdentitySubtype() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertTrue(test.isSubtype(typeObject,typeObject));
                assertTrue(test.isSubtype(typeSerializable,typeSerializable));
                assertTrue(test.isSubtype(typeArrayClonable,typeArrayClonable));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testInterfaceIsSubtypeOfObject() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.ClassNotFoundException {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertTrue(test.isSubtype(typeClonable,typeObject));
            }
        });
    }
    public void testArrays() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertTrue(test.isSubtype(typeArrayClonable,typeObject));
                assertTrue(test.isSubtype(typeArrayClonable,typeArrayObject));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testUnrelatedTypes() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertFalse(test.isSubtype(typeInteger,typeString));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testArraysWrongDimension() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertFalse(test.isSubtype(typeArrayArrayString,typeArrayString));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testMultidimensionalArrayIsSubtypeOfObjectArray() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertTrue(test.isSubtype(typeArrayArrayString,typeArrayObject));
                assertTrue(test.isSubtype(typeArrayArraySerializable,typeArrayObject));
                assertTrue(test.isSubtype(typeArrayArrayInt,typeArrayObject));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testArrayOfPrimitiveIsSubtypeOfObject() throws java.lang.Throwable {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Exception {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertTrue(test.isSubtype(typeArrayInt,typeObject));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testSpecialStringSubclasses() throws java.lang.Exception {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Exception {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertTrue(test.isSubtype(typeDynamicString,typeString));
                assertTrue(test.isSubtype(typeStaticString,typeString));
                assertTrue(test.isSubtype(typeParameterString,typeString));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testEasyFirstCommonSuperclass() throws java.lang.Exception {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeObject,typeObject));
                assertEquals(typeString,test.getFirstCommonSuperclass(typeString,typeString));
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeString,typeObject));
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeObject,typeString));
                assertEquals(typeComparable,test.getFirstCommonSuperclass(typeString,typeInteger));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
// Slightly harder one
    public void testInterfaceFirstCommonSuperclass() throws java.lang.Exception {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeSerializable,typeObject));
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeObject,typeSerializable));
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeSerializable,typeClonable));
                assertEquals(typeSerializable,test.getFirstCommonSuperclass(typeSerializable,typeSerializable));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testArrayFirstCommonSuperclass() throws java.lang.Exception {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeArrayInteger,typeObject));
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeSerializable,typeArrayClonable));
                assertEquals(typeArrayComparable,test.getFirstCommonSuperclass(typeArrayString,typeArrayInteger));
                assertEquals(typeArrayInt,test.getFirstCommonSuperclass(typeArrayInt,typeArrayInt));
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeArrayChar,typeArrayInt));
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeArrayString,typeArrayInt));
                assertEquals(typeArrayObject,test.getFirstCommonSuperclass(typeArrayArraySerializable,typeArrayString));
                assertEquals(typeObject,test.getFirstCommonSuperclass(typeArrayArrayString,typeArrayInt));
            }
        });
    }
/*
             * (non-Javadoc)
             * 
             * @see edu.umd.cs.findbugs.RunnableWithExceptions#run()
             */
    public void testArrayFirstCommonSuperclassTricky() throws java.lang.Exception {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertEquals(typeArrayObject,test.getFirstCommonSuperclass(typeArrayArrayInt,typeArrayArrayChar));
                assertEquals(typeArrayObject,test.getFirstCommonSuperclass(typeArrayArrayInt,typeArrayArrayArrayChar));
                assertEquals(typeArrayArrayObject,test.getFirstCommonSuperclass(typeArrayArrayArrayChar,typeArrayArrayArrayInt));
                assertEquals(typeArrayArrayArrayChar,test.getFirstCommonSuperclass(typeArrayArrayArrayChar,typeArrayArrayArrayChar));
            }
        });
    }
// Sanity check
    public void testInterfaces() throws java.lang.Exception {
        this.executeFindBugsTest(new edu.umd.cs.findbugs.RunnableWithExceptions() {
            public void run() throws java.lang.Throwable {
                edu.umd.cs.findbugs.ba.ch.Subtypes2 test = getSubtypes2();
                assertEquals(typeCollection,test.getFirstCommonSuperclass(typeCollection,typeHashSet));
                assertEquals(typeCollection,test.getFirstCommonSuperclass(typeHashSet,typeCollection));
                assertEquals(typeCollection,test.getFirstCommonSuperclass(typeList,typeHashSet));
            }
        });
    }
}
/*
         * ObjectType typeList; ObjectType typeMap; ObjectType typeCollection;
         * ObjectType typeHashSet;
         */
