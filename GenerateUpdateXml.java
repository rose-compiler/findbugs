/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.tools;
import edu.umd.cs.findbugs.tools.*;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.Plugin;
public class GenerateUpdateXml extends java.lang.Object {
    public GenerateUpdateXml() {
    }
    public static void main(java.lang.String[] args) {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection dfc = edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        for (edu.umd.cs.findbugs.Plugin p : dfc.plugins()){
            java.lang.System.out.println(p.getPluginId());
            java.lang.System.out.println(p.getReleaseDate());
            java.lang.System.out.println(p.getVersion());
            java.lang.System.out.println();
        }
;
    }
}
