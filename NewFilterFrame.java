/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * 
 * Lets you choose your new filter, shouldn't let you choose filters that
 * wouldn't filter anything out including filters that you already have
 * 
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import edu.umd.cs.findbugs.gui2.BugAspects.SortableValue;
public class NewFilterFrame extends edu.umd.cs.findbugs.gui2.FBDialog {
    private javax.swing.JList list = new javax.swing.JList();
    private static edu.umd.cs.findbugs.gui2.NewFilterFrame instance = null;
    public static void open() {
        if (instance == null) {
            instance = new edu.umd.cs.findbugs.gui2.NewFilterFrame();
            instance.setVisible(true);
            instance.toFront();
        }
    }
    public static void close() {
        if (instance != null) {
            instance.dispose();
            instance = null;
        }
    }
    public NewFilterFrame() {
        super(edu.umd.cs.findbugs.gui2.PreferencesFrame.getInstance());
        this.setContentPane(new javax.swing.JPanel() {
            public java.awt.Insets getInsets() {
                return new java.awt.Insets(3, 3, 3, 3);
            }
        });
        this.setLayout(new java.awt.BorderLayout());
        javax.swing.JPanel north = new javax.swing.JPanel();
        north.setLayout(new javax.swing.BoxLayout(north, javax.swing.BoxLayout.X_AXIS));
        north.add(javax.swing.Box.createHorizontalStrut(3));
        north.add(new javax.swing.JLabel(edu.umd.cs.findbugs.L10N.getLocalString("dlg.filter_bugs_lbl","Filter out all bugs whose") + " "));
// Argh divider
        edu.umd.cs.findbugs.gui2.Sortables[] sortables = edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getAvailableSortables();
        edu.umd.cs.findbugs.gui2.Sortables[] valuesWithoutDivider = new edu.umd.cs.findbugs.gui2.Sortables[sortables.length - 1];
        int index = 0;
        for (int x = 0; x < sortables.length; x++) {
            if (sortables[x] != edu.umd.cs.findbugs.gui2.Sortables.DIVIDER) {
                valuesWithoutDivider[index] = sortables[x];
                index++;
            }
        }
        final javax.swing.JComboBox comboBox = new javax.swing.JComboBox(valuesWithoutDivider);
        comboBox.setRenderer(new javax.swing.ListCellRenderer() {
            final java.awt.Color SELECTED_BACKGROUND = new java.awt.Color(183, 184, 204);
            public java.awt.Component getListCellRendererComponent(javax.swing.JList list, java.lang.Object value, int index, boolean isSelected, boolean cellHasFocus) {
                javax.swing.JLabel result = new javax.swing.JLabel();
                result.setFont(result.getFont().deriveFont(edu.umd.cs.findbugs.gui2.Driver.getFontSize()));
                result.setOpaque(true);
                result.setText(value.toString().toLowerCase());
                if (isSelected) result.setBackground(SELECTED_BACKGROUND);
                return result;
            }
        });
        comboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edu.umd.cs.findbugs.gui2.Sortables filterBy = (edu.umd.cs.findbugs.gui2.Sortables) (comboBox.getSelectedItem()) ;
                java.lang.String[] listData = filterBy.getAllSorted();
                for (int i = 0; i < listData.length; i++) listData[i] = filterBy.formatValue(listData[i]);
                list.setListData(listData);
            }
        });
        comboBox.validate();
        north.add(comboBox);
        north.add(new javax.swing.JLabel(" " + edu.umd.cs.findbugs.L10N.getLocalString("dlg.is","is") + " "));
        java.lang.String[] filterModes = {edu.umd.cs.findbugs.L10N.getLocalString("mode.equal_to","equal to"), edu.umd.cs.findbugs.L10N.getLocalString("mode.at_or_after","at or after"), edu.umd.cs.findbugs.L10N.getLocalString("mode.at_or_before","at or before")};
        final javax.swing.JComboBox filterModeComboBox = new javax.swing.JComboBox(filterModes);
        north.add(filterModeComboBox);
        north.add(new javax.swing.JLabel(":"));
        north.add(javax.swing.Box.createHorizontalGlue());
        javax.swing.JPanel south = new javax.swing.JPanel();
        javax.swing.JButton okButton = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.ok_btn","OK"));
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edu.umd.cs.findbugs.gui2.Sortables key = (edu.umd.cs.findbugs.gui2.Sortables) (comboBox.getSelectedItem()) ;
                java.lang.String[] values = key.getAllSorted();
                java.util.ArrayList<edu.umd.cs.findbugs.gui2.BugAspects.SortableValue> filterMe = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.BugAspects.SortableValue>();
                for (int i : list.getSelectedIndices()){
                    filterMe.add(new edu.umd.cs.findbugs.gui2.BugAspects.SortableValue(key, values[i]));
                }
;
                try {
                    edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getProject().getSuppressionFilter().addChild(edu.umd.cs.findbugs.gui2.FilterFactory.makeOrMatcher(filterMe));
                }
                catch (java.lang.RuntimeException e){
                    edu.umd.cs.findbugs.gui2.MainFrame.getInstance().showMessageDialog("Unable to create filter: " + e.getMessage());
                    close();
                    return;
                }
                edu.umd.cs.findbugs.gui2.FilterActivity.notifyListeners(edu.umd.cs.findbugs.gui2.FilterListener.Action.FILTERING,null);
                edu.umd.cs.findbugs.gui2.PreferencesFrame.getInstance().updateFilterPanel();
                edu.umd.cs.findbugs.gui2.MainFrame.getInstance().setProjectChanged(true);
                close();
            }
        });
        javax.swing.JButton cancelButton = new javax.swing.JButton(edu.umd.cs.findbugs.L10N.getLocalString("dlg.cancel_btn","Cancel"));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close();
            }
        });
        edu.umd.cs.findbugs.gui2.GuiUtil.addOkAndCancelButtons(south,okButton,cancelButton);
        list.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    edu.umd.cs.findbugs.gui2.Sortables key = (edu.umd.cs.findbugs.gui2.Sortables) (comboBox.getSelectedItem()) ;
                    java.lang.String[] values = key.getAllSorted();
                    edu.umd.cs.findbugs.gui2.FilterMatcher[] newFilters = new edu.umd.cs.findbugs.gui2.FilterMatcher[list.getSelectedIndices().length];
                    for (int i = 0; i < newFilters.length; i++) newFilters[i] = new edu.umd.cs.findbugs.gui2.FilterMatcher(key, values[list.getSelectedIndices()[i]]);
                    edu.umd.cs.findbugs.gui2.ProjectSettings.getInstance().addFilters(newFilters);
                    edu.umd.cs.findbugs.gui2.PreferencesFrame.getInstance().updateFilterPanel();
                    close();
                }
            }
        });
        this.add(north,java.awt.BorderLayout.NORTH);
        this.add(javax.swing.Box.createHorizontalStrut(2),java.awt.BorderLayout.WEST);
        this.add(new javax.swing.JScrollPane(list),java.awt.BorderLayout.CENTER);
        this.add(javax.swing.Box.createHorizontalStrut(2),java.awt.BorderLayout.EAST);
        this.add(south,java.awt.BorderLayout.SOUTH);
        comboBox.getActionListeners()[0].actionPerformed(null);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent arg0) {
                close();
            }
        });
        this.setTitle(edu.umd.cs.findbugs.L10N.getLocalString("dlg.create_new_filter_ttl","Create New Filter"));
        this.pack();
    }
// Dupe from OK button's ActionListener
// for (int i : list.getSelectedIndices())
// {
// FilterMatcher fm=new FilterMatcher(key,values[i]);
// if
// (!ProjectSettings.getInstance().getAllMatchers().contains(fm))
// ProjectSettings.getInstance().addFilter(fm);
// }
// Populate the box with initial values
    public static void main(java.lang.String[] args) {
        new edu.umd.cs.findbugs.gui2.NewFilterFrame().setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
