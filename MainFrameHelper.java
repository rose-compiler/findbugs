package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import edu.umd.cs.findbugs.gui.AnnotatedString;
public class MainFrameHelper extends java.lang.Object {
    public MainFrameHelper() {
    }
    public static javax.swing.JButton newButton(java.lang.String key, java.lang.String name) {
        javax.swing.JButton b = new javax.swing.JButton();
        edu.umd.cs.findbugs.gui.AnnotatedString.localiseButton(b,key,name,false);
        return b;
    }
    public static javax.swing.JMenuItem newJMenuItem(java.lang.String key, java.lang.String string, int vkF) {
        javax.swing.JMenuItem m = new javax.swing.JMenuItem();
        edu.umd.cs.findbugs.gui.AnnotatedString.localiseButton(m,key,string,false);
        m.setMnemonic(vkF);
        return m;
    }
    public static javax.swing.JMenuItem newJMenuItem(java.lang.String key, java.lang.String string) {
        javax.swing.JMenuItem m = new javax.swing.JMenuItem();
        edu.umd.cs.findbugs.gui.AnnotatedString.localiseButton(m,key,string,true);
        return m;
    }
    public static javax.swing.JMenu newJMenu(java.lang.String key, java.lang.String string) {
        javax.swing.JMenu m = new javax.swing.JMenu();
        edu.umd.cs.findbugs.gui.AnnotatedString.localiseButton(m,key,string,true);
        return m;
    }
    public static boolean isMacLookAndFeel() {
        java.lang.String name = javax.swing.UIManager.getLookAndFeel().getClass().getName();
        return name.startsWith("com.apple");
    }
    public static void attachAcceleratorKey(javax.swing.JMenuItem item, int keystroke) {
        attachAcceleratorKey(item,keystroke,0);
    }
    public static void attachAcceleratorKey(javax.swing.JMenuItem item, int keystroke, int additionalMask) {
// As far as I know, Mac is the only platform on which it is normal
// practice to use accelerator masks such as Shift and Alt, so
// if we're not running on Mac, just ignore them
        if ( !edu.umd.cs.findbugs.gui2.MainFrame.MAC_OS_X && additionalMask != 0) {
            return;
        }
        item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(keystroke,java.awt.Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | additionalMask));
    }
    public static void attachAcceleratorKeyNoCtrl(javax.swing.JMenuItem item, int keyEvent) {
        item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(keyEvent,0));
    }
}
