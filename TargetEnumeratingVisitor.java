/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Visitor to find all of the targets of an instruction whose InstructionHandle
 * is given. Note that we don't consider exception edges.
 * 
 * @author David Hovemeyer
 * @author Chadd Williams
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.bcel.generic.ATHROW;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.GotoInstruction;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.IfInstruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.ReturnInstruction;
import org.apache.bcel.generic.Select;
public class TargetEnumeratingVisitor extends org.apache.bcel.generic.EmptyVisitor implements edu.umd.cs.findbugs.ba.EdgeTypes {
    private org.apache.bcel.generic.InstructionHandle handle;
    private org.apache.bcel.generic.ConstantPoolGen constPoolGen;
    private java.util.LinkedList<edu.umd.cs.findbugs.ba.Target> targetList;
    private boolean isBranch;
    private boolean isReturn;
    private boolean isThrow;
    private boolean isExit;
/**
     * Constructor.
     * 
     * @param handle
     *            the handle of the instruction whose targets should be
     *            enumerated
     * @param constPoolGen
     *            the ConstantPoolGen object for the class
     */
    public TargetEnumeratingVisitor(org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.ConstantPoolGen constPoolGen) {
        super();
        this.handle = handle;
        this.constPoolGen = constPoolGen;
        targetList = new java.util.LinkedList<edu.umd.cs.findbugs.ba.Target>();
        isBranch = isReturn = isThrow = isExit = false;
        handle.getInstruction().accept(this);
    }
/**
     * Is the instruction the end of a basic block?
     */
    public boolean isEndOfBasicBlock() {
        return isBranch || isReturn || isThrow || isExit;
    }
/**
     * Is the analyzed instruction a method return?
     */
    public boolean instructionIsReturn() {
        return isReturn;
    }
/**
     * Is the analyzed instruction an explicit throw?
     */
    public boolean instructionIsThrow() {
        return isThrow;
    }
/**
     * Is the analyzed instruction an exit (call to System.exit())?
     */
    public boolean instructionIsExit() {
        return isExit;
    }
/**
     * Iterate over Target objects representing control flow targets and their
     * edge types.
     */
    public java.util.Iterator<edu.umd.cs.findbugs.ba.Target> targetIterator() {
        return targetList.iterator();
    }
    public void visitGotoInstruction(org.apache.bcel.generic.GotoInstruction ins) {
        isBranch = true;
        org.apache.bcel.generic.InstructionHandle target = ins.getTarget();
        if (target == null) throw new java.lang.IllegalStateException();
        targetList.add(new edu.umd.cs.findbugs.ba.Target(target, GOTO_EDGE));
    }
    public void visitIfInstruction(org.apache.bcel.generic.IfInstruction ins) {
        isBranch = true;
        org.apache.bcel.generic.InstructionHandle target = ins.getTarget();
        if (target == null) throw new java.lang.IllegalStateException();
        targetList.add(new edu.umd.cs.findbugs.ba.Target(target, IFCMP_EDGE));
        org.apache.bcel.generic.InstructionHandle fallThrough = handle.getNext();
        targetList.add(new edu.umd.cs.findbugs.ba.Target(fallThrough, FALL_THROUGH_EDGE));
    }
    public void visitSelect(org.apache.bcel.generic.Select ins) {
        isBranch = true;
// Add non-default switch edges.
        org.apache.bcel.generic.InstructionHandle[] targets = ins.getTargets();
        for (org.apache.bcel.generic.InstructionHandle target : targets){
            targetList.add(new edu.umd.cs.findbugs.ba.Target(target, SWITCH_EDGE));
        }
;
// Add default switch edge.
        org.apache.bcel.generic.InstructionHandle defaultTarget = ins.getTarget();
        if (defaultTarget == null) {
            throw new java.lang.IllegalStateException();
        }
        targetList.add(new edu.umd.cs.findbugs.ba.Target(defaultTarget, SWITCH_DEFAULT_EDGE));
    }
    public void visitReturnInstruction(org.apache.bcel.generic.ReturnInstruction ins) {
        isReturn = true;
    }
    public void visitATHROW(org.apache.bcel.generic.ATHROW ins) {
        isThrow = true;
    }
    public void visitINVOKESTATIC(org.apache.bcel.generic.INVOKESTATIC ins) {
// Find calls to System.exit(), since this effectively terminates the
// basic block.
        java.lang.String className = ins.getClassName(constPoolGen);
        java.lang.String methodName = ins.getName(constPoolGen);
        java.lang.String methodSig = ins.getSignature(constPoolGen);
        if (className.equals("java.lang.System") && methodName.equals("exit") && methodSig.equals("(I)V")) isExit = true;
    }
}
