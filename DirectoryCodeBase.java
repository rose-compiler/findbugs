package edu.umd.cs.findbugs.classfile.impl;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.impl.*;
/**
 * IScannableCodeBase implementation to read resources from a filesystem
 * directory.
 * 
 * @author David Hovemeyer
 */
/*
         * (non-Javadoc)
         * 
         * @see edu.umd.cs.findbugs.classfile.ICodeBaseIterator#hasNext()
         */
/*
         * (non-Javadoc)
         * 
         * @see edu.umd.cs.findbugs.classfile.ICodeBaseIterator#next()
         */
// Make the filename relative to the directory
// Update last modified time
/**
     * Constructor.
     * 
     * @param codeBaseLocator
     *            the codebase locator for this codebase
     * @param directory
     *            the filesystem directory
     */
/*
             * (non-Javadoc)
             * 
             * @see java.io.FileFilter#accept(java.io.File)
             */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.IScannableCodeBase#iterator()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getPathName()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#close()
     */
// Nothing to do
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.ICodeBase#lookupResource(java.lang.String)
     */
// Translate resource name, in case a resource name
// has been overridden and the resource is being accessed
// using the overridden name.
/**
     * Get the full path of given resource.
     * 
     * @param resourceName
     * @return
     */
/**
     * Get the resource name given a full filename.
     * 
     * @param fileName
     *            the full filename (which must be inside the directory)
     * @return the resource name (i.e., the filename with the directory stripped
     *         off)
     */
// FIXME: there is probably a more robust way to do this
// Strip off the directory part.
// The problem here is that we need to take the relative part of the
// filename
// and break it into components that we can then reconstruct into
// a resource name (using '/' characters to separate the components).
// Unfortunately, the File class does not make this task particularly
// easy.
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
abstract interface package-info {
}
