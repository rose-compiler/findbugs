/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Visitor to model the effect of bytecode instructions on ConstantFrames.
 * 
 * <p>
 * For now, only String constants are modeled. In the future we can add other
 * kinds of constants.
 * </p>
 * 
 * @see edu.umd.cs.findbugs.ba.constant.ConstantAnalysis
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.constant;
import edu.umd.cs.findbugs.ba.constant.*;
import org.apache.bcel.generic.BIPUSH;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.ICONST;
import org.apache.bcel.generic.IINC;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.LDC2_W;
import org.apache.bcel.generic.SIPUSH;
import edu.umd.cs.findbugs.ba.AbstractFrameModelingVisitor;
public class ConstantFrameModelingVisitor extends edu.umd.cs.findbugs.ba.AbstractFrameModelingVisitor {
    public ConstantFrameModelingVisitor(org.apache.bcel.generic.ConstantPoolGen cpg) {
        super(cpg);
    }
    public edu.umd.cs.findbugs.ba.constant.Constant getDefaultValue() {
        return edu.umd.cs.findbugs.ba.constant.Constant.NOT_CONSTANT;
    }
    public void visitIINC(org.apache.bcel.generic.IINC obj) {
// System.out.println("before iinc: " + getFrame());
        int v = obj.getIndex();
        int amount = obj.getIncrement();
        edu.umd.cs.findbugs.ba.constant.ConstantFrame f = this.getFrame();
        edu.umd.cs.findbugs.ba.constant.Constant c = f.getValue(v);
        if (c.isConstantInteger()) f.setValue(v,new edu.umd.cs.findbugs.ba.constant.Constant(c.getConstantInt() + amount));
        else f.setValue(v,edu.umd.cs.findbugs.ba.constant.Constant.NOT_CONSTANT);
    }
// System.out.println("after iinc: " + getFrame());
    public void visitICONST(org.apache.bcel.generic.ICONST obj) {
        java.lang.Number value = obj.getValue();
        edu.umd.cs.findbugs.ba.constant.Constant c = new edu.umd.cs.findbugs.ba.constant.Constant(value);
        this.getFrame().pushValue(c);
    }
    public void visitBIPUSH(org.apache.bcel.generic.BIPUSH obj) {
        java.lang.Number value = obj.getValue();
        edu.umd.cs.findbugs.ba.constant.Constant c = new edu.umd.cs.findbugs.ba.constant.Constant(value);
        this.getFrame().pushValue(c);
    }
    public void visitSIPUSH(org.apache.bcel.generic.SIPUSH obj) {
        java.lang.Number value = obj.getValue();
        edu.umd.cs.findbugs.ba.constant.Constant c = new edu.umd.cs.findbugs.ba.constant.Constant(value);
        this.getFrame().pushValue(c);
    }
    public void visitLDC(org.apache.bcel.generic.LDC obj) {
        java.lang.Object value = obj.getValue(this.getCPG());
        edu.umd.cs.findbugs.ba.constant.Constant c = new edu.umd.cs.findbugs.ba.constant.Constant(value);
        this.getFrame().pushValue(c);
    }
    public void visitLDC2_W(org.apache.bcel.generic.LDC2_W obj) {
        java.lang.Object value = obj.getValue(this.getCPG());
        edu.umd.cs.findbugs.ba.constant.Constant c = new edu.umd.cs.findbugs.ba.constant.Constant(value);
        this.getFrame().pushValue(c);
        this.getFrame().pushValue(c);
    }
}
