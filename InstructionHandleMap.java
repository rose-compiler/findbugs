/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Map BCEL InstructionHandles to some kind of value type.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.MethodGen;
public class InstructionHandleMap<ValueType> extends java.lang.Object {
    private java.lang.Object[] map;
    public InstructionHandleMap(org.apache.bcel.generic.MethodGen methodGen) {
        super();
        map = new java.lang.Object[methodGen.getMethod().getCode().getLength()];
    }
    public ValueType get(org.apache.bcel.generic.InstructionHandle handle) {
        return (ValueType) (map[handle.getPosition()]) ;
    }
    public ValueType put(org.apache.bcel.generic.InstructionHandle handle, ValueType value) {
        ValueType old = this.get(handle);
        map[handle.getPosition()] = value;
        return old;
    }
}
