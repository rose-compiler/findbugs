/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce the data (bytes) of a class.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine;
import edu.umd.cs.findbugs.classfile.engine.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.ICodeBaseEntry;
import edu.umd.cs.findbugs.classfile.MissingClassException;
import edu.umd.cs.findbugs.classfile.RecomputableClassAnalysisEngine;
import edu.umd.cs.findbugs.classfile.ResourceNotFoundException;
import edu.umd.cs.findbugs.classfile.analysis.ClassData;
import edu.umd.cs.findbugs.classfile.impl.ZipInputStreamCodeBaseEntry;
import edu.umd.cs.findbugs.io.IO;
public class ClassDataAnalysisEngine extends edu.umd.cs.findbugs.classfile.RecomputableClassAnalysisEngine<edu.umd.cs.findbugs.classfile.analysis.ClassData> {
    public ClassDataAnalysisEngine() {
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.classfile.analysis.ClassData analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.ClassDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
// Compute the resource name
        java.lang.String resourceName = descriptor.toResourceName();
// Look up the codebase entry for the resource
        edu.umd.cs.findbugs.classfile.ICodeBaseEntry codeBaseEntry;
        try {
            codeBaseEntry = analysisCache.getClassPath().lookupResource(resourceName);
        }
        catch (edu.umd.cs.findbugs.classfile.ResourceNotFoundException e){
            throw new edu.umd.cs.findbugs.classfile.MissingClassException(descriptor, e);
        }
        byte[] data;
        if (codeBaseEntry instanceof edu.umd.cs.findbugs.classfile.impl.ZipInputStreamCodeBaseEntry) {
            data = ((edu.umd.cs.findbugs.classfile.impl.ZipInputStreamCodeBaseEntry) (codeBaseEntry) ).getBytes();
        }
        else {
// Create a ByteArrayOutputStream to capture the class data
            int length = codeBaseEntry.getNumBytes();
            java.io.ByteArrayOutputStream byteSink;
            if (length >= 0) {
                byteSink = new java.io.ByteArrayOutputStream(length);
            }
            else {
                byteSink = new java.io.ByteArrayOutputStream();
            }
// Read the class data into the byte array
            java.io.InputStream in = null;
            try {
                in = codeBaseEntry.openResource();
                edu.umd.cs.findbugs.io.IO.copy(in,byteSink);
            }
            catch (java.io.IOException e){
                throw new edu.umd.cs.findbugs.classfile.MissingClassException(descriptor, e);
            }
            finally {
                if (in != null) {
                    edu.umd.cs.findbugs.io.IO.close(in);
                }
            }
            data = byteSink.toByteArray();
        }
        return new edu.umd.cs.findbugs.classfile.analysis.ClassData(descriptor, codeBaseEntry, data);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#registerWith(edu.umd.cs
     * .findbugs.classfile.IAnalysisCache)
     */
    public void registerWith(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache) {
        analysisCache.registerClassAnalysisEngine(edu.umd.cs.findbugs.classfile.analysis.ClassData.class,this);
    }
}
