/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Features of a class which may be used to identify it if it is renamed or
 * modified.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.model;
import edu.umd.cs.findbugs.model.*;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.FieldOrMethod;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.JavaClassAndMethod;
import edu.umd.cs.findbugs.ba.SignatureParser;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
import edu.umd.cs.findbugs.xml.XMLWriteable;
public class ClassFeatureSet extends java.lang.Object implements edu.umd.cs.findbugs.xml.XMLWriteable {
    final public static java.lang.String CLASS_NAME_KEY = "Class:";
    final public static java.lang.String METHOD_NAME_KEY = "Method:";
    final public static java.lang.String CODE_LENGTH_KEY = "CodeLength:";
    final public static java.lang.String FIELD_NAME_KEY = "Field:";
    private java.lang.String className;
    private boolean isInterface;
    private java.util.Set<java.lang.String> featureSet;
/**
     * Constructor. Creates an empty feature set.
     */
    public ClassFeatureSet() {
        super();
        this.featureSet = new java.util.HashSet<java.lang.String>();
    }
/**
     * Minimum code length required to add a CodeLength feature.
     */
    final public static int MIN_CODE_LENGTH = 10;
/**
     * Initialize from given JavaClass.
     * 
     * @param javaClass
     *            the JavaClass
     * @return this object
     */
    public edu.umd.cs.findbugs.model.ClassFeatureSet initialize(org.apache.bcel.classfile.JavaClass javaClass) {
        this.className = javaClass.getClassName();
        this.isInterface = javaClass.isInterface();
        this.addFeature(CLASS_NAME_KEY + transformClassName(javaClass.getClassName()));
        for (org.apache.bcel.classfile.Method method : javaClass.getMethods()){
            if ( !this.isSynthetic(method)) {
                java.lang.String transformedMethodSignature = transformMethodSignature(method.getSignature());
                if (method.isStatic() ||  !this.overridesSuperclassMethod(javaClass,method)) {
                    this.addFeature(METHOD_NAME_KEY + method.getName() + ":" + transformedMethodSignature);
                }
                org.apache.bcel.classfile.Code code = method.getCode();
                if (code != null && code.getCode() != null && code.getCode().length >= MIN_CODE_LENGTH) {
                    this.addFeature(CODE_LENGTH_KEY + method.getName() + ":" + transformedMethodSignature + ":" + code.getCode().length);
                }
            }
        }
;
        for (org.apache.bcel.classfile.Field field : javaClass.getFields()){
            if ( !this.isSynthetic(field)) {
                this.addFeature(FIELD_NAME_KEY + field.getName() + ":" + transformSignature(field.getSignature()));
            }
        }
;
        return this;
    }
/**
     * Determine if given method overrides a superclass or superinterface
     * method.
     * 
     * @param javaClass
     *            class defining the method
     * @param method
     *            the method
     * @return true if the method overrides a superclass/superinterface method,
     *         false if not
     * @throws ClassNotFoundException
     */
    private boolean overridesSuperclassMethod(org.apache.bcel.classfile.JavaClass javaClass, org.apache.bcel.classfile.Method method) {
        if (method.isStatic()) return false;
        try {
            org.apache.bcel.classfile.JavaClass[] superclassList = javaClass.getSuperClasses();
            if (superclassList != null) {
                edu.umd.cs.findbugs.ba.JavaClassAndMethod match = edu.umd.cs.findbugs.ba.Hierarchy.findMethod(superclassList,method.getName(),method.getSignature(),edu.umd.cs.findbugs.ba.Hierarchy.INSTANCE_METHOD);
                if (match != null) return true;
            }
            org.apache.bcel.classfile.JavaClass[] interfaceList = javaClass.getAllInterfaces();
            if (interfaceList != null) {
                edu.umd.cs.findbugs.ba.JavaClassAndMethod match = edu.umd.cs.findbugs.ba.Hierarchy.findMethod(interfaceList,method.getName(),method.getSignature(),edu.umd.cs.findbugs.ba.Hierarchy.INSTANCE_METHOD);
                if (match != null) return true;
            }
            return false;
        }
        catch (java.lang.ClassNotFoundException e){
            return true;
        }
    }
/**
     * Figure out if a class member (field or method) is synthetic.
     * 
     * @param member
     *            a field or method
     * @return true if the member is synthetic
     */
    private boolean isSynthetic(org.apache.bcel.classfile.FieldOrMethod member) {
// this never works, but worth a try
        if (member.isSynthetic()) return true;
        java.lang.String name = member.getName();
        if (name.startsWith("class$")) return true;
        if (name.startsWith("access$")) return true;
        return false;
    }
/**
     * @return Returns the className.
     */
    public java.lang.String getClassName() {
        return className;
    }
/**
     * @param className
     *            The className to set.
     */
    public void setClassName(java.lang.String className) {
        this.className = className;
    }
/**
     * @return Returns the isInterface.
     */
    public boolean isInterface() {
        return isInterface;
    }
/**
     * @param isInterface
     *            The isInterface to set.
     */
    public void setInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }
    public int getNumFeatures() {
        return featureSet.size();
    }
    public void addFeature(java.lang.String feature) {
        featureSet.add(feature);
    }
    public java.util.Iterator<java.lang.String> featureIterator() {
        return featureSet.iterator();
    }
    public boolean hasFeature(java.lang.String feature) {
        return featureSet.contains(feature);
    }
/**
     * Transform a class name by stripping its package name.
     * 
     * @param className
     *            a class name
     * @return the transformed class name
     */
    public static java.lang.String transformClassName(java.lang.String className) {
        int lastDot = className.lastIndexOf('\u002e');
        if (lastDot >= 0) {
            java.lang.String pkg = className.substring(0,lastDot);
            if ( !isUnlikelyToBeRenamed(pkg)) {
                className = className.substring(lastDot + 1);
            }
        }
        return className;
    }
/**
     * Return true if classes in the given package is unlikely to be renamed:
     * e.g., because they are part of a public API.
     * 
     * @param pkg
     *            the package name
     * @return true if classes in the package is unlikely to be renamed
     */
    public static boolean isUnlikelyToBeRenamed(java.lang.String pkg) {
        return pkg.startsWith("java.");
    }
/**
     * Transform a method signature to allow it to be compared even if any of
     * its parameter types are moved to another package.
     * 
     * @param signature
     *            a method signature
     * @return the transformed signature
     */
    public static java.lang.String transformMethodSignature(java.lang.String signature) {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append('\u0028');
        edu.umd.cs.findbugs.ba.SignatureParser parser = new edu.umd.cs.findbugs.ba.SignatureParser(signature);
        for (java.util.Iterator<java.lang.String> i = parser.parameterSignatureIterator(); i.hasNext(); ) {
            java.lang.String param = i.next();
            param = transformSignature(param);
            buf.append(param);
        }
        buf.append('\u0029');
        return buf.toString();
    }
/**
     * Transform a field or method parameter signature to allow it to be
     * compared even if it is moved to another package.
     * 
     * @param signature
     *            the signature
     * @return the transformed signature
     */
    public static java.lang.String transformSignature(java.lang.String signature) {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        int lastBracket = signature.lastIndexOf('\u005b');
        if (lastBracket > 0) {
            buf.append(signature.substring(0,lastBracket + 1));
            signature = signature.substring(lastBracket + 1);
        }
        if (signature.startsWith("L")) {
            signature = signature.substring(1,signature.length() - 1).replace('\u002f','\u002e');
            signature = transformClassName(signature);
            signature = "L" + signature.replace('\u002e','\u002f') + ";";
        }
        buf.append(signature);
        return buf.toString();
    }
/**
     * Minimum number of features which must be present in order to declare two
     * classes similar.
     */
    final public static int MIN_FEATURES = 5;
/**
     * Minimum similarity required to declare two classes similar.
     */
    final public static double MIN_MATCH = 0.60;
/**
     * Similarity of classes which don't have enough features to match exactly,
     * but whose class names match exactly.
     */
    final public static double EXACT_CLASS_NAME_MATCH = MIN_MATCH + 0.1;
    public static double similarity(edu.umd.cs.findbugs.model.ClassFeatureSet a, edu.umd.cs.findbugs.model.ClassFeatureSet b) {
// Some features must match exactly
        if (a.isInterface() != b.isInterface()) return 0.0;
        if (a.getNumFeatures() < MIN_FEATURES || b.getNumFeatures() < MIN_FEATURES) return a.getClassName().equals(b.getClassName()) ? EXACT_CLASS_NAME_MATCH : 0.0;
        int numMatch = 0;
        int max = java.lang.Math.max(a.getNumFeatures(),b.getNumFeatures());
        for (java.util.Iterator<java.lang.String> i = a.featureIterator(); i.hasNext(); ) {
            java.lang.String feature = i.next();
            if (b.hasFeature(feature)) {
                ++numMatch;
            }
        }
        return ((double) (numMatch)  / (double) (max) );
    }
    public boolean similarTo(edu.umd.cs.findbugs.model.ClassFeatureSet other) {
        return similarity(this,other) >= MIN_MATCH;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        if (args.length != 2) {
            java.lang.System.err.println("Usage: " + edu.umd.cs.findbugs.model.ClassFeatureSet.class.getName() + " <class 1> <class 2>");
            java.lang.System.exit(1);
        }
        org.apache.bcel.classfile.JavaClass a = org.apache.bcel.Repository.lookupClass(args[0]);
        org.apache.bcel.classfile.JavaClass b = org.apache.bcel.Repository.lookupClass(args[1]);
        edu.umd.cs.findbugs.model.ClassFeatureSet aFeatures = new edu.umd.cs.findbugs.model.ClassFeatureSet().initialize(a);
        edu.umd.cs.findbugs.model.ClassFeatureSet bFeatures = new edu.umd.cs.findbugs.model.ClassFeatureSet().initialize(b);
        java.lang.System.out.println("Similarity is " + similarity(aFeatures,bFeatures));
        java.lang.System.out.println("Classes are" + (aFeatures.similarTo(bFeatures) ? "" : " not") + " similar");
    }
    final public static java.lang.String ELEMENT_NAME = "ClassFeatureSet";
    final public static java.lang.String FEATURE_ELEMENT_NAME = "Feature";
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.xml.XMLWriteable#writeXML(edu.umd.cs.findbugs.xml
     * .XMLOutput)
     */
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        xmlOutput.openTag(ELEMENT_NAME,new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("class",className));
        for (java.util.Iterator<java.lang.String> i = this.featureIterator(); i.hasNext(); ) {
            java.lang.String feature = i.next();
            xmlOutput.openCloseTag(FEATURE_ELEMENT_NAME,new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("value",feature));
        }
        xmlOutput.closeTag(ELEMENT_NAME);
    }
}
