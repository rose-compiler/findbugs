package edu.umd.cs.findbugs.classfile.impl;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.impl.*;
/**
 * Implementation of IClassPathBuilder.
 * 
 * @author David Hovemeyer
 */
/**
     * Worklist item. Represents one codebase to be processed during the
     * classpath construction algorithm.
     */
/**
         * @return Returns the howDiscovered.
         */
/**
     * A codebase discovered during classpath building.
     */
// Fields
/**
     * Constructor.
     * 
     * @param classFactory
     *            the class factory
     * @param errorLogger
     *            the error logger
     */
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IClassPathBuilder#addCodeBase(edu.umd.cs
     * .findbugs.classfile.ICodeBaseLocator, boolean)
     */
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IClassPathBuilder#scanNestedArchives(boolean
     * )
     */
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IClassPathBuilder#build(edu.umd.cs.findbugs
     * .classfile.IClassPath,
     * edu.umd.cs.findbugs.classfile.IClassPathBuilderProgress)
     */
// Discover all directly and indirectly referenced codebases
// If not already located, try to locate any additional codebases
// containing classes required for analysis.
// Add all discovered codebases to the classpath
// Build collection of all application classes.
// Also, add resource name -> codebase entry mappings for application
// classes.
// An earlier entry takes precedence over this class
/**
     * Make an effort to find the codebases containing any files required for
     * analysis.
     */
// If we're running findbugs-full.jar, IT contains the contents
// of jsr305.jar and annotations.jar. So, add it to the classpath.
// Not running findbugs-full.jar: try to find jsr305.jar and
// annotations.jar.
//
// Found findbugs-full.jar: add it to the aux classpath.
// (This is a bit weird, since we only want to resolve a subset
// of its classes.)
//
/**
     * Probe a codebase to see if a given source exists in that code base.
     * 
     * @param resourceName
     *            name of a resource
     * @return true if the resource exists in the codebase, false if not
     */
// This method is based on the
// org.apache.bcel.util.ClassPath.getClassPath()
// method.
// Seed worklist with system codebases.
// addWorkListItemsForClasspath(workList,
// SystemProperties.getProperty("java.class.path"));
/**
     * Create a worklist that will add the FindBugs lib/annotations.jar to the
     * classpath.
     */
/**
     * Create a worklist that will add the FindBugs lib/jsr305.jar to the
     * classpath.
     */
//
// If the findbugs.home property is set,
// we should be able to find the jar file in
// the lib subdirectory.
//
//
// See if the required jar file is available on the class path.
//
// See if the searched-for jar file can be found
// alongside findbugs.jar.
/**
     * Add worklist items from given system classpath.
     * 
     * @param workList
     *            the worklist
     * @param path
     *            a system classpath
     */
/**
     * Add worklist items from given extensions directory.
     * 
     * @param workList
     *            the worklist
     * @param extDir
     *            an extensions directory
     */
/*
             * (non-Javadoc)
             * 
             * @see java.io.FileFilter#accept(java.io.File)
             */
/**
     * Process classpath worklist items. We will attempt to find all nested
     * archives and Class-Path entries specified in Jar manifests. This should
     * give us as good an idea as possible of all of the classes available (and
     * which are part of the application).
     * 
     * @param workList
     *            the worklist to process
     * @param progress
     *            IClassPathBuilderProgress callback
     * @throws InterruptedException
     * @throws IOException
     * @throws ResourceNotFoundException
     */
// Build the classpath, scanning codebases for nested archives
// and referenced codebases.
// See if we have encountered this codebase before
// If the codebase is not an app codebase and
// the worklist item says that it is an app codebase,
// change it. Otherwise, we have nothing to do.
// Detect .java files, which are probably human error
// If we are working on an application codebase,
// then failing to open/scan it is a fatal error.
// We issue warnings about problems with aux codebases,
// but continue anyway.
// Open the codebase and add it to the classpath
// Note that this codebase has been visited
// If it is a scannable codebase, check it for nested archives.
// In addition, if it is an application codebase then
// make a list of application classes.
// Check for a Jar manifest for additional aux classpath
// entries.
/**
     * Scan given codebase in order to
     * <ul>
     * <li>check the codebase for nested archives (adding any found to the
     * worklist)
     * <li>build a list of class resources found in the codebase
     * </ul>
     * 
     * @param workList
     *            the worklist
     * @param discoveredCodeBase
     *            the codebase to scan
     * @throws InterruptedException
     */
// Note the resource exists in this codebase
// If resource is a nested archive, add it to the worklist
/**
     * Attempt to parse data of given resource in order to divine the real name
     * of the class contained in the resource.
     * 
     * @param entry
     *            the resource
     */
/**
     * Check a codebase for a Jar manifest to examine for Class-Path entries.
     * 
     * @param workList
     *            the worklist
     * @param codeBase
     *            the codebase for examine for a Jar manifest
     * @throws IOException
     */
// See if this codebase has a jar manifest
// Do nothing - no Jar manifest found
// Try to read the manifest
// Create a codebase locator for the classpath entry
// relative to the codebase in which we discovered the Jar
// manifest
// Codebases found in Class-Path entries are always
// added to the aux classpath, not the application.
/**
     * Add a worklist item to the worklist. This method maintains the invariant
     * that all of the worklist items representing application codebases appear
     * <em>before</em> all of the worklist items representing auxiliary
     * codebases.
     * 
     * @param projectWorkList
     *            the worklist
     * @param itemToAdd
     *            the worklist item to add
     */
// Auxiliary codebases are always added at the end
// Adding an application codebase: position a ListIterator
// just before first auxiliary codebase (or at the end of the list
// if there are no auxiliary codebases)
// Add the codebase to the worklist
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.IClassPathBuilder#getAppClassList()
     */
abstract interface package-info {
}
