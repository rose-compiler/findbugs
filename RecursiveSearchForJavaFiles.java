/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
public class RecursiveSearchForJavaFiles extends java.lang.Object {
    public RecursiveSearchForJavaFiles() {
    }
    public static void main(java.lang.String[] args) {
        for (java.io.File f : search(new java.io.File(args[0])))java.lang.System.out.println(f.getPath());
;
    }
    public static java.util.Set<java.io.File> search(java.io.File root) {
        java.util.Set<java.io.File> result = new java.util.HashSet<java.io.File>();
        java.util.Set<java.io.File> directories = new java.util.HashSet<java.io.File>();
        java.util.LinkedList<java.io.File> worklist = new java.util.LinkedList<java.io.File>();
        directories.add(root);
        worklist.add(root);
        while ( !worklist.isEmpty()) {
            java.io.File next = worklist.removeFirst();
            java.io.File[] files = next.listFiles();
            if (files != null) for (java.io.File f : files){
                if (f.getName().endsWith(".java")) result.add(f);
                else if (f.isDirectory() && directories.add(f)) {
                    worklist.add(f);
                }
            }
;
        }
        return result;
    }
}
