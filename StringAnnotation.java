/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2008, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Bug annotation class for string values.
 * 
 * @author William Pugh
 * @see BugAnnotation
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import edu.umd.cs.findbugs.util.Strings;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class StringAnnotation extends java.lang.Object implements edu.umd.cs.findbugs.BugAnnotation {
/** Used for non-string constants (e.g., null) */
    static class QuotedStringMarker extends java.lang.Object {
        public QuotedStringMarker() {
        }
    }
    final private static long serialVersionUID = 1L;
    final public static java.lang.String DEFAULT_ROLE = "STRING_DEFAULT";
    final public static java.lang.String STRING_CONSTANT_ROLE = "STRING_CONSTANT";
    final public static java.lang.String STRING_NONSTRING_CONSTANT_ROLE = "STRING_NONSTRING_CONSTANT";
    final public static java.lang.String REGEX_ROLE = "STRING_REGEX";
    final public static java.lang.String ERROR_MSG_ROLE = "STRING_ERROR_MSG";
    final public static java.lang.String STRING_MESSAGE = "STRING_MESSAGE";
    final public static java.lang.String PARAMETER_NAME_ROLE = "STRING_PARAMETER_NAME";
    final public static java.lang.String TYPE_QUALIFIER_ROLE = "STRING_TYPE_QUALIFIER";
    final public static java.lang.String REMAINING_OBLIGATIONS_ROLE = "STRING_REMAINING_OBLIGATIONS";
    final public static java.lang.String FORMAT_STRING_ROLE = "STRING_FORMAT_STRING";
    final public static java.lang.String FORMAT_SPECIFIER_ROLE = "STRING_FORMAT_SPECIFIER";
    final private java.lang.String value;
    private java.lang.String description;
/**
     * Constructor.
     * 
     * @param value
     *            the String value
     */
    public StringAnnotation(java.lang.String value) {
        super();
        this.value = value;
        this.description = DEFAULT_ROLE;
    }
    public static edu.umd.cs.findbugs.StringAnnotation fromRawString(java.lang.String value) {
        return new edu.umd.cs.findbugs.StringAnnotation(edu.umd.cs.findbugs.util.Strings.escapeLFCRBackSlash(value));
    }
    public static edu.umd.cs.findbugs.StringAnnotation fromXMLEscapedString(java.lang.String value) {
        return new edu.umd.cs.findbugs.StringAnnotation(edu.umd.cs.findbugs.util.Strings.unescapeXml(value));
    }
    public java.lang.Object clone() {
        try {
            return super.clone();
        }
        catch (java.lang.CloneNotSupportedException e){
            throw new java.lang.AssertionError(e);
        }
    }
/**
     * Get the String value.
     * 
     * @return the String value
     */
    public java.lang.String getValue() {
        return value;
    }
    public void accept(edu.umd.cs.findbugs.BugAnnotationVisitor visitor) {
        visitor.visitStringAnnotation(this);
    }
    public java.lang.String format(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        java.lang.String txt = value;
        return txt;
    }
    public void setDescription(java.lang.String description) {
        this.description = description;
    }
    public java.lang.String getDescription() {
        return description;
    }
    public int hashCode() {
        return value.hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.StringAnnotation)) return false;
        return value.equals(((edu.umd.cs.findbugs.StringAnnotation) (o) ).value);
    }
    public int compareTo(edu.umd.cs.findbugs.BugAnnotation o) {
// BugAnnotations must be
        if ( !(o instanceof edu.umd.cs.findbugs.StringAnnotation)) 
// Comparable with any type of
// BugAnnotation
        return this.getClass().getName().compareTo(o.getClass().getName());
        return value.compareTo(((edu.umd.cs.findbugs.StringAnnotation) (o) ).value);
    }
    public java.lang.String toString() {
        java.lang.String pattern = edu.umd.cs.findbugs.I18N.instance().getAnnotationDescription(description);
        edu.umd.cs.findbugs.FindBugsMessageFormat format = new edu.umd.cs.findbugs.FindBugsMessageFormat(pattern);
        return format.format(new edu.umd.cs.findbugs.BugAnnotation[]{this},null);
    }
/*
     * ----------------------------------------------------------------------
     * XML Conversion support
     * ----------------------------------------------------------------------
     */
    final private static java.lang.String ELEMENT_NAME = "String";
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        this.writeXML(xmlOutput,false,false);
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean addMessages, boolean isPrimary) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributeList = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("value",value);
        java.lang.String role = this.getDescription();
        if ( !role.equals(DEFAULT_ROLE)) attributeList.addAttribute("role",role);
        edu.umd.cs.findbugs.BugAnnotationUtil.writeXML(xmlOutput,ELEMENT_NAME,this,attributeList,addMessages);
    }
    public boolean isSignificant() {
        return true;
    }
    public java.lang.String toString(edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        return this.toString();
    }
}
// vim:ts=4
