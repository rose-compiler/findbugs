/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import org.apache.bcel.Constants;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
import edu.umd.cs.findbugs.util.ClassName;
abstract public class AbstractMethod extends edu.umd.cs.findbugs.ba.AbstractClassMember implements edu.umd.cs.findbugs.ba.XMethod {
    public AbstractMethod(java.lang.String className, java.lang.String methodName, java.lang.String methodSig, int accessFlags) {
        super(className,methodName,methodSig,accessFlags);
    }
    public int getNumParams() {
// FIXME: cache this?
        return new edu.umd.cs.findbugs.ba.SignatureParser(this.getSignature()).getNumParameters();
    }
    public boolean isNative() {
        return (this.getAccessFlags() & org.apache.bcel.Constants.ACC_NATIVE) != 0;
    }
    public boolean isSynchronized() {
        return (this.getAccessFlags() & org.apache.bcel.Constants.ACC_SYNCHRONIZED) != 0;
    }
    public java.lang.String toString() {
        return edu.umd.cs.findbugs.ba.SignatureConverter.convertMethodSignature(this);
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.XMethod#getMethodDescriptor()
     */
    public edu.umd.cs.findbugs.classfile.MethodDescriptor getMethodDescriptor() {
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getMethodDescriptor(edu.umd.cs.findbugs.util.ClassName.toSlashedClassName(this.getClassName()),this.getName(),this.getSignature(),this.isStatic());
    }
    public edu.umd.cs.findbugs.ba.XMethod resolveAccessMethodForMethod() {
        edu.umd.cs.findbugs.classfile.MethodDescriptor access = this.getAccessMethodForMethod();
        if (access != null) return edu.umd.cs.findbugs.ba.XFactory.createXMethod(access);
        return this;
    }
}
