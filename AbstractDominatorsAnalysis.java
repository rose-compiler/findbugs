/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A dataflow analysis to compute dominator relationships between basic blocks.
 * Use the {@link #getResultFact} method to get the dominator set for a given
 * basic block. The dominator sets are represented using the
 * {@link java.util.BitSet} class, with the individual bits corresponding to the
 * IDs of basic blocks.
 * <p/>
 * <p>
 * Subclasses extend this class to compute either dominators or postdominators.
 * <p/>
 * <p>
 * An EdgeChooser may be specified to select which edges to take into account.
 * For example, exception edges could be ignored.
 * </p>
 * 
 * @author David Hovemeyer
 * @see DataflowAnalysis
 * @see CFG
 * @see BasicBlock
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.BitSet;
import java.util.Iterator;
import javax.annotation.CheckForNull;
import org.apache.bcel.generic.InstructionHandle;
abstract public class AbstractDominatorsAnalysis extends edu.umd.cs.findbugs.ba.BasicAbstractDataflowAnalysis<java.util.BitSet> {
    final private edu.umd.cs.findbugs.ba.CFG cfg;
    private edu.umd.cs.findbugs.ba.EdgeChooser edgeChooser;
/**
     * Constructor.
     * 
     * @param cfg
     *            the CFG to compute dominator relationships for
     * @param ignoreExceptionEdges
     *            true if exception edges should be ignored
     */
    public AbstractDominatorsAnalysis(edu.umd.cs.findbugs.ba.CFG cfg, final boolean ignoreExceptionEdges) {
        this(cfg,new edu.umd.cs.findbugs.ba.EdgeChooser() {
            public boolean choose(edu.umd.cs.findbugs.ba.Edge edge) {
                if (ignoreExceptionEdges && edge.isExceptionEdge()) return false;
                else return true;
            }
        });
    }
/**
     * Constructor.
     * 
     * @param cfg
     *            the CFG to compute dominator relationships for
     * @param edgeChooser
     *            EdgeChooser to choose which Edges to consider significant
     */
    public AbstractDominatorsAnalysis(edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.EdgeChooser edgeChooser) {
        super();
        this.cfg = cfg;
        this.edgeChooser = edgeChooser;
    }
    public java.util.BitSet createFact() {
        return new java.util.BitSet();
    }
    public void copy(java.util.BitSet source, java.util.BitSet dest) {
        dest.clear();
        dest.or(source);
    }
    public void initEntryFact(java.util.BitSet result) {
        result.clear();
    }
// No blocks dominate the entry block
    public boolean isTop(java.util.BitSet fact) {
// We represent TOP as a bitset with an illegal bit set
        return fact.get(cfg.getNumBasicBlocks());
    }
    public void makeFactTop(java.util.BitSet fact) {
        fact.set(cfg.getNumBasicBlocks());
    }
// We represent TOP as a bitset with an illegal bit set
    public boolean same(java.util.BitSet fact1, java.util.BitSet fact2) {
        return fact1.equals(fact2);
    }
    public void transfer(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle end, java.util.BitSet start, java.util.BitSet result) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        this.copy(start,result);
// Start with intersection of dominators of predecessors
        if ( !this.isTop(result)) {
            result.set(basicBlock.getLabel());
        }
    }
    public void meetInto(java.util.BitSet fact, edu.umd.cs.findbugs.ba.Edge edge, java.util.BitSet result) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        if ( !edgeChooser.choose(edge)) return;
        if (this.isTop(fact)) return;
        else if (this.isTop(result)) this.copy(fact,result);
        else result.and(fact);
    }
// Meet is intersection
/**
     * Get a bitset containing the unique IDs of all blocks which dominate (or
     * postdominate) the given block.
     * 
     * @param block
     *            a BasicBlock
     * @return BitSet of the unique IDs of all blocks that dominate (or
     *         postdominate) the BasicBlock
     */
    public java.util.BitSet getAllDominatorsOf(edu.umd.cs.findbugs.ba.BasicBlock block) {
        return this.getResultFact(block);
    }
/**
     * Get a bitset containing the unique IDs of all blocks in CFG dominated (or
     * postdominated, depending on how the analysis was done) by given block.
     * 
     * @param dominator
     *            we want to get all blocks dominated (or postdominated) by this
     *            block
     * @return BitSet of the ids of all blocks dominated by the given block
     */
    public java.util.BitSet getAllDominatedBy(edu.umd.cs.findbugs.ba.BasicBlock dominator) {
        java.util.BitSet allDominated = new java.util.BitSet();
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.BasicBlock> i = cfg.blockIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.BasicBlock block = i.next();
            java.util.BitSet dominators = this.getResultFact(block);
            if (dominators.get(dominator.getLabel())) allDominated.set(block.getLabel());
        }
        return allDominated;
    }
}
// vim:ts=4
