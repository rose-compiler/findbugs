package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Class to open input streams on source files. It maintains a "source path",
 * which is like a classpath, but for finding source files instead of class
 * files.
 */
/*
     * ----------------------------------------------------------------------
     * Helper classes
     * ----------------------------------------------------------------------
     */
/**
     * Cache of SourceFiles. We use this to avoid repeatedly having to read
     * frequently accessed source files.
     */
/**
         *
         */
/**
     * A repository of source files.
     */
/**
     * A directory containing source files.
     */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.ba.SourceFinder.SourceRepository#contains(java
         * .lang.String)
         */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.ba.SourceFinder.SourceRepository#getDataSource
         * (java.lang.String)
         */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.ba.SourceFinder.SourceRepository#isPlatformDependent
         * ()
         */
/**
     * A zip or jar archive containing source files.
     */
/*
     * ----------------------------------------------------------------------
     * Fields
     * ----------------------------------------------------------------------
     */
/*
     * ----------------------------------------------------------------------
     * Public methods
     * ----------------------------------------------------------------------
     */
/**
     * @return Returns the project.
     */
/**
     * Set the list of source directories.
     */
// Zip or jar archive
// Ignored - we won't use this archive
/**
     * Open an input stream on a source file in given package.
     *
     * @param packageName
     *            the name of the package containing the class whose source file
     *            is given
     * @param fileName
     *            the unqualified name of the source file
     * @return an InputStream on the source file
     * @throws IOException
     *             if a matching source file cannot be found
     */
/**
     * Open a source file in given package.
     *
     * @param packageName
     *            the name of the package containing the class whose source file
     *            is given
     * @param fileName
     *            the unqualified name of the source file
     * @return the source file
     * @throws IOException
     *             if a matching source file cannot be found
     */
// On windows the fileName specification is different between a file in
// a directory tree, and a
// file in a zip file. In a directory tree the separator used is '\',
// while in a zip it's '/'
// Therefore for each repository figure out what kind it is and use the
// appropriate separator.
// In all practicality, this code could just use the hardcoded '/' char,
// as windows can open
// files with this separator, but to allow for the mythical 'other'
// platform that uses an
// alternate separator, make a distinction
// Is the file in the cache already? Always cache it with the canonical
// name
// Find this source file, add its data to the cache
// Query each element of the source path to find the requested source
// file
// Found it
// always cache with
// canonicalName
/**
     * @param packageName
     * @param fileName
     * @return
     */
/**
     * @param packageName
     * @param fileName
     * @return
     */
// On windows the fileName specification is different between a file in
// a directory tree, and a
// file in a zip file. In a directory tree the separator used is '\',
// while in a zip it's '/'
// Therefore for each repository figure out what kind it is and use the
// appropriate separator.
// In all practicality, this code could just use the hardcoded '/' char,
// as windows can open
// files with this separator, but to allow for the mythical 'other'
// platform that uses an
// alternate separator, make a distinction
// Create a fully qualified source filename using the package name for
// both directories and zips
// Is the file in the cache already? Always cache it with the canonical
// name
// Find this source file, add its data to the cache
// Query each element of the source path to find the requested source
// file
/**
     * @param project
     */
// vim:ts=4
abstract interface package-info {
}
