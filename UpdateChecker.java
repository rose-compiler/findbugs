package edu.umd.cs.findbugs.updates;
import edu.umd.cs.findbugs.updates.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.WillClose;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.Plugin;
import edu.umd.cs.findbugs.SystemProperties;
import edu.umd.cs.findbugs.Version;
import edu.umd.cs.findbugs.util.MultiMap;
import edu.umd.cs.findbugs.util.Util;
import edu.umd.cs.findbugs.xml.OutputStreamXMLOutput;
import edu.umd.cs.findbugs.xml.XMLUtil;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
public class UpdateChecker extends java.lang.Object {
/**
     * @param force
     * @return
     */
/** protected for testing */
// for debugging:
//        writeXml(System.out, plugins, entryPoint);
/** protected for testing */
// package-private for testing
//            StringWriter stringWriter = new StringWriter();
//            XMLWriter xmlWriter = new XMLWriter(stringWriter);
//            xmlWriter.write(doc);
//            xmlWriter.close();
//            System.out.println("UPDATE RESPONSE: " + stringWriter.toString());
// protected for testing
// protected for testing
/** Should only be used once */
    public static class PluginUpdate extends java.lang.Object {
        final private edu.umd.cs.findbugs.Plugin plugin;
        final private java.lang.String version;
        final private java.util.Date date;
        final private java.lang.String url;
        final private java.lang.String message;
        public PluginUpdate(edu.umd.cs.findbugs.Plugin plugin, java.lang.String version, java.util.Date date, java.lang.String url, java.lang.String message) {
            super();
            this.plugin = plugin;
            this.version = version;
            this.date = date;
            this.url = url;
            this.message = message;
        }
        public edu.umd.cs.findbugs.Plugin getPlugin() {
            return plugin;
        }
        public java.lang.String getVersion() {
            return version;
        }
        public java.util.Date getDate() {
            return date;
        }
        public java.lang.String getUrl() {
            return url;
        }
        public java.lang.String getMessage() {
            return message;
        }
        public java.lang.String toString() {
            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat(PLUGIN_RELEASE_DATE_FMT);
            java.lang.StringBuilder buf = new java.lang.StringBuilder();
            java.lang.String name = this.getPlugin().isCorePlugin() ? "FindBugs" : "FindBugs plugin " + this.getPlugin().getShortDescription();
            buf.append(name + " " + this.getVersion());
            if (date == null) buf.append(" has been released");
            else buf.append(" was released " + format.format(date));
            buf.append(" (you have " + this.getPlugin().getVersion() + ")");
            buf.append("\n");
            buf.append("   " + message.replaceAll("\n","\n   "));
            if (url != null) buf.append("\nVisit " + url + " for details.");
            return buf.toString();
        }
    }
    final public static java.lang.String PLUGIN_RELEASE_DATE_FMT = "MM/dd/yyyy hh:mm aa z";
    final private static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.updates.UpdateChecker.class.getName());
    final private static java.lang.String KEY_DISABLE_ALL_UPDATE_CHECKS = "noUpdateChecks";
    final private static java.lang.String KEY_REDIRECT_ALL_UPDATE_CHECKS = "redirectUpdateChecks";
    final private static boolean ENV_FB_NO_UPDATE_CHECKS = java.lang.System.getenv("FB_NO_UPDATE_CHECKS") != null;
    final private edu.umd.cs.findbugs.updates.UpdateCheckCallback dfc;
    final private java.util.List<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> pluginUpdates = new java.util.concurrent.CopyOnWriteArrayList<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate>();
    public UpdateChecker(edu.umd.cs.findbugs.updates.UpdateCheckCallback dfc) {
        super();
        this.dfc = dfc;
    }
    public void checkForUpdates(java.util.Collection<edu.umd.cs.findbugs.Plugin> plugins, final boolean force) {
        if (this.updateChecksGloballyDisabled()) {
            dfc.pluginUpdateCheckComplete(pluginUpdates,force);
            return;
        }
        java.net.URI redirectUri = this.getRedirectURL(force);
        final java.util.concurrent.CountDownLatch latch;
        if (redirectUri != null) {
            latch = new java.util.concurrent.CountDownLatch(1);
            this.startUpdateCheckThread(redirectUri,plugins,latch);
        }
        else {
            edu.umd.cs.findbugs.util.MultiMap<java.net.URI, edu.umd.cs.findbugs.Plugin> pluginsByUrl = new edu.umd.cs.findbugs.util.MultiMap<java.net.URI, edu.umd.cs.findbugs.Plugin>(java.util.HashSet.class);
            for (edu.umd.cs.findbugs.Plugin plugin : plugins){
                java.net.URI uri = plugin.getUpdateUrl();
                if (uri == null) {
                    this.logError(java.util.logging.Level.FINE,"Not checking for updates for " + plugin.getShortDescription() + " - no update-url attribute in plugin XML file");
                    continue;
                }
                pluginsByUrl.add(uri,plugin);
            }
;
            latch = new java.util.concurrent.CountDownLatch(pluginsByUrl.keySet().size());
            for (java.net.URI uri : pluginsByUrl.keySet()){
                this.startUpdateCheckThread(uri,pluginsByUrl.get(uri),latch);
            }
;
        }
        this.waitForCompletion(latch,force);
    }
    public java.net.URI getRedirectURL(final boolean force) {
        java.lang.String redirect = dfc.getGlobalOption(KEY_REDIRECT_ALL_UPDATE_CHECKS);
        java.lang.String sysprop = java.lang.System.getProperty("findbugs.redirectUpdateChecks");
        if (sysprop != null) redirect = sysprop;
        edu.umd.cs.findbugs.Plugin setter = dfc.getGlobalOptionSetter(KEY_REDIRECT_ALL_UPDATE_CHECKS);
        java.net.URI redirectUri = null;
        java.lang.String pluginName = setter == null ? "<unknown plugin>" : setter.getShortDescription();
        if (redirect != null &&  !redirect.trim().equals("")) {
            try {
                redirectUri = new java.net.URI(redirect);
                this.logError(java.util.logging.Level.INFO,"Redirecting all plugin update checks to " + redirectUri + " (" + pluginName + ")");
            }
            catch (java.net.URISyntaxException e){
                java.lang.String error = "Invalid update check redirect URI in " + pluginName + ": " + redirect;
                this.logError(java.util.logging.Level.SEVERE,error);
                dfc.pluginUpdateCheckComplete(pluginUpdates,force);
                throw new java.lang.IllegalStateException(error);
            }
        }
        return redirectUri;
    }
    private long dontWarnAgainUntil() {
        java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.updates.UpdateChecker.class);
        java.lang.String oldSeen = prefs.get("last-plugin-update-seen","");
        if (oldSeen == null || oldSeen.equals("")) return 0;
        try {
            return java.lang.Long.parseLong(oldSeen) + DONT_REMIND_WINDOW;
        }
        catch (java.lang.Exception e){
            return 0;
        }
    }
    final static long DONT_REMIND_WINDOW = 3L * 24 * 60 * 60 * 1000;
    public boolean updatesHaveBeenSeenBefore(java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates) {
        long now = java.lang.System.currentTimeMillis();
        java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.updates.UpdateChecker.class);
        java.lang.String oldHash = prefs.get("last-plugin-update-hash","");
        java.lang.String newHash = java.lang.Integer.toString(this.buildPluginUpdateHash(updates));
        if (oldHash.equals(newHash) && this.dontWarnAgainUntil() > now) {
            LOGGER.fine("Skipping update dialog because these updates have been seen before");
            return true;
        }
        prefs.put("last-plugin-update-hash",newHash);
        prefs.put("last-plugin-update-seen",java.lang.Long.toString(now));
        return false;
    }
    private int buildPluginUpdateHash(java.util.Collection<edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate> updates) {
        java.util.HashSet<java.lang.String> builder = new java.util.HashSet<java.lang.String>();
        for (edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate update : updates){
            builder.add(update.getPlugin().getPluginId() + update.getVersion());
        }
;
        return builder.hashCode();
    }
    private void waitForCompletion(final java.util.concurrent.CountDownLatch latch, final boolean force) {
        edu.umd.cs.findbugs.util.Util.runInDameonThread(new java.lang.Runnable() {
            public void run() {
                try {
                    if ( !latch.await(15,java.util.concurrent.TimeUnit.SECONDS)) {
                        edu.umd.cs.findbugs.updates.UpdateChecker.this.logError(java.util.logging.Level.INFO,"Update check timed out");
                    }
                    dfc.pluginUpdateCheckComplete(pluginUpdates,force);
                }
                catch (java.lang.Exception ignored){
                    assert true;
                }
            }
        },"Plugin update checker");
    }
    public boolean updateChecksGloballyDisabled() {
        return ENV_FB_NO_UPDATE_CHECKS || this.getPluginThatDisabledUpdateChecks() != null;
    }
    public java.lang.String getPluginThatDisabledUpdateChecks() {
        java.lang.String disable = dfc.getGlobalOption(KEY_DISABLE_ALL_UPDATE_CHECKS);
        edu.umd.cs.findbugs.Plugin setter = dfc.getGlobalOptionSetter(KEY_DISABLE_ALL_UPDATE_CHECKS);
        java.lang.String pluginName = setter == null ? "<unknown plugin>" : setter.getShortDescription();
        java.lang.String disablingPlugin = null;
        if ("true".equalsIgnoreCase(disable)) {
            this.logError(java.util.logging.Level.INFO,"Skipping update checks due to " + KEY_DISABLE_ALL_UPDATE_CHECKS + "=true set by " + pluginName);
            disablingPlugin = pluginName;
        }
        else if (disable != null &&  !"false".equalsIgnoreCase(disable)) {
            java.lang.String error = "Unknown value '" + disable + "' for " + KEY_DISABLE_ALL_UPDATE_CHECKS + " in " + pluginName;
            this.logError(java.util.logging.Level.SEVERE,error);
            throw new java.lang.IllegalStateException(error);
        }
        return disablingPlugin;
    }
    private void startUpdateCheckThread(final java.net.URI url, final java.util.Collection<edu.umd.cs.findbugs.Plugin> plugins, final java.util.concurrent.CountDownLatch latch) {
        if (url == null) {
            this.logError(java.util.logging.Level.INFO,"Not checking for plugin updates w/ blank URL: " + this.getPluginNames(plugins));
            return;
        }
        final java.lang.String entryPoint = this.getEntryPoint();
        if ((entryPoint.contains("edu.umd.cs.findbugs.FindBugsTestCase") || entryPoint.contains("edu.umd.cs.findbugs.cloud.appEngine.AbstractWebCloudTest")) && (url.getScheme().equals("http") || url.getScheme().equals("https"))) {
            LOGGER.fine("Skipping update check because we're running in FindBugsTestCase and using " + url.getScheme());
            return;
        }
        edu.umd.cs.findbugs.util.Util.runInDameonThread(new java.lang.Runnable() {
            public void run() {
                try {
                    edu.umd.cs.findbugs.updates.UpdateChecker.this.actuallyCheckforUpdates(url,plugins,entryPoint);
                }
                catch (java.lang.Exception e){
                    if (e instanceof java.lang.IllegalStateException && e.getMessage().contains("Shutdown in progress")) return;
                    edu.umd.cs.findbugs.updates.UpdateChecker.this.logError(e,"Error doing update check at " + url);
                }
                finally {
                    latch.countDown();
                }
            }
        },"Check for updates");
    }
    protected void actuallyCheckforUpdates(java.net.URI url, java.util.Collection<edu.umd.cs.findbugs.Plugin> plugins, java.lang.String entryPoint) throws java.io.IOException {
        LOGGER.fine("Checking for updates at " + url + " for " + this.getPluginNames(plugins));
        java.net.HttpURLConnection conn = (java.net.HttpURLConnection) (url.toURL().openConnection()) ;
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.connect();
        java.io.OutputStream out = conn.getOutputStream();
        this.writeXml(out,plugins,entryPoint);
        int responseCode = conn.getResponseCode();
        if (responseCode != 200) {
            this.logError(edu.umd.cs.findbugs.SystemProperties.ASSERTIONS_ENABLED ? java.util.logging.Level.WARNING : java.util.logging.Level.FINE,"Error checking for updates at " + url + ": " + responseCode + " - " + conn.getResponseMessage());
        }
        else {
            this.parseUpdateXml(url,plugins,conn.getInputStream());
        }
        conn.disconnect();
    }
    final protected void writeXml(java.io.OutputStream out, java.util.Collection<edu.umd.cs.findbugs.Plugin> plugins, java.lang.String entryPoint) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.OutputStreamXMLOutput xmlOutput = new edu.umd.cs.findbugs.xml.OutputStreamXMLOutput(out);
        try {
            xmlOutput.beginDocument();
            xmlOutput.startTag("findbugs-invocation");
            xmlOutput.addAttribute("version",edu.umd.cs.findbugs.Version.RELEASE);
            java.lang.String applicationName = edu.umd.cs.findbugs.Version.getApplicationName();
            if (applicationName == null || applicationName.equals("")) {
                int lastDot = entryPoint.lastIndexOf('\u002e');
                if (lastDot ==  -1) applicationName = entryPoint;
                else applicationName = entryPoint.substring(lastDot + 1);
            }
            xmlOutput.addAttribute("app-name",applicationName);
            java.lang.String applicationVersion = edu.umd.cs.findbugs.Version.getApplicationVersion();
            if (applicationVersion == null) applicationVersion = "";
            xmlOutput.addAttribute("app-version",applicationVersion);
            xmlOutput.addAttribute("entry-point",entryPoint);
            xmlOutput.addAttribute("os",edu.umd.cs.findbugs.SystemProperties.getProperty("os.name",""));
            xmlOutput.addAttribute("java-version",this.getMajorJavaVersion());
            java.util.Locale locale = java.util.Locale.getDefault();
            xmlOutput.addAttribute("language",locale.getLanguage());
            xmlOutput.addAttribute("country",locale.getCountry());
            xmlOutput.addAttribute("uuid",getUuid());
            xmlOutput.stopTag(false);
            for (edu.umd.cs.findbugs.Plugin plugin : plugins){
                xmlOutput.startTag("plugin");
                xmlOutput.addAttribute("id",plugin.getPluginId());
                xmlOutput.addAttribute("name",plugin.getShortDescription());
                xmlOutput.addAttribute("version",plugin.getVersion());
                java.util.Date date = plugin.getReleaseDate();
                if (date != null) xmlOutput.addAttribute("release-date",java.lang.Long.toString(date.getTime()));
                xmlOutput.stopTag(true);
            }
;
            xmlOutput.closeTag("findbugs-invocation");
            xmlOutput.flush();
        }
        finally {
            xmlOutput.finish();
        }
    }
    void parseUpdateXml(java.net.URI url, java.util.Collection<edu.umd.cs.findbugs.Plugin> plugins, java.io.InputStream inputStream) {
        try {
            org.dom4j.Document doc = new org.dom4j.io.SAXReader().read(inputStream);
            java.util.List<org.dom4j.Element> pluginEls = edu.umd.cs.findbugs.xml.XMLUtil.selectNodes(doc,"fb-plugin-updates/plugin");
            java.util.Map<java.lang.String, edu.umd.cs.findbugs.Plugin> map = new java.util.HashMap<java.lang.String, edu.umd.cs.findbugs.Plugin>();
            for (edu.umd.cs.findbugs.Plugin p : plugins)map.put(p.getPluginId(),p);
;
            for (org.dom4j.Element pluginEl : pluginEls){
                java.lang.String id = pluginEl.attributeValue("id");
                edu.umd.cs.findbugs.Plugin plugin = map.get(id);
                if (plugin != null) {
                    this.checkPlugin(pluginEl,plugin);
                }
            }
;
        }
        catch (java.lang.Exception e){
            this.logError(e,"Could not parse plugin version update for " + url);
        }
        finally {
            edu.umd.cs.findbugs.util.Util.closeSilently(inputStream);
        }
    }
    private void checkPlugin(org.dom4j.Element pluginEl, edu.umd.cs.findbugs.Plugin plugin) {
        for (org.dom4j.Element release : (java.util.List<org.dom4j.Element>) (pluginEl.elements("release")) ){
            this.checkPluginRelease(plugin,release);
        }
;
    }
    private void checkPluginRelease(edu.umd.cs.findbugs.Plugin plugin, org.dom4j.Element maxEl) {
        java.util.Date updateDate = this.parseReleaseDate(maxEl);
        java.util.Date installedDate = plugin.getReleaseDate();
        if (updateDate != null && installedDate != null && updateDate.before(installedDate)) return;
        java.lang.String version = maxEl.attributeValue("version");
        if (version.equals(plugin.getVersion())) return;
        java.lang.String url = maxEl.attributeValue("url");
        java.lang.String message = maxEl.element("message").getTextTrim();
        pluginUpdates.add(new edu.umd.cs.findbugs.updates.UpdateChecker.PluginUpdate(plugin, version, updateDate, url, message));
    }
    protected void logError(java.util.logging.Level level, java.lang.String msg) {
        LOGGER.log(level,msg);
    }
    protected void logError(java.lang.Exception e, java.lang.String msg) {
        LOGGER.log(java.util.logging.Level.INFO,msg,e);
    }
    private java.util.Date parseReleaseDate(org.dom4j.Element releaseEl) {
        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat(PLUGIN_RELEASE_DATE_FMT);
        java.lang.String dateStr = releaseEl.attributeValue("date");
        if (dateStr == null) return null;
        try {
            return format.parse(dateStr);
        }
        catch (java.lang.Exception e){
            throw new java.lang.IllegalArgumentException("Error parsing " + dateStr, e);
        }
    }
    private java.lang.String getPluginNames(java.util.Collection<edu.umd.cs.findbugs.Plugin> plugins) {
        java.lang.String text = "";
        boolean first = true;
        for (edu.umd.cs.findbugs.Plugin plugin : plugins){
            text = (first ? "" : ", ") + plugin.getShortDescription();
            first = false;
        }
;
        return text;
    }
    private java.lang.String getEntryPoint() {
        java.lang.String lastFbClass = "<UNKNOWN>";
        for (java.lang.StackTraceElement s : java.lang.Thread.currentThread().getStackTrace()){
            java.lang.String cls = s.getClassName();
            if (cls.startsWith("edu.umd.cs.findbugs.")) {
                lastFbClass = cls;
            }
        }
;
        return lastFbClass;
    }
    private static java.util.Random random = new java.util.Random();
    private static synchronized java.lang.String getUuid() {
        try {
            java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.updates.UpdateChecker.class);
            long uuid = prefs.getLong("uuid",0);
            if (uuid == 0) {
                uuid = random.nextLong();
                prefs.putLong("uuid",uuid);
            }
            return java.lang.Long.toString(uuid,16);
        }
        catch (java.lang.Throwable e){
            return java.lang.Long.toString(42,16);
        }
    }
    private java.lang.String getMajorJavaVersion() {
        java.lang.String ver = edu.umd.cs.findbugs.SystemProperties.getProperty("java.version","");
        java.util.regex.Matcher m = java.util.regex.Pattern.compile("^\\d+\\.\\d+").matcher(ver);
        if (m.find()) {
            return m.group();
        }
        return "";
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection dfc = edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        edu.umd.cs.findbugs.updates.UpdateChecker checker = dfc.getUpdateChecker();
        if (checker.updateChecksGloballyDisabled()) java.lang.System.out.println("Update checkes are globally disabled");
        java.net.URI redirect = checker.getRedirectURL(false);
        if (redirect != null) java.lang.System.out.println("All update checks redirected to " + redirect);
        checker.writeXml(java.lang.System.out,dfc.plugins(),"UpdateChecker");
    }
}
