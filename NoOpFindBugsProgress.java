/**
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
public class NoOpFindBugsProgress extends java.lang.Object implements edu.umd.cs.findbugs.FindBugsProgress {
    public NoOpFindBugsProgress() {
    }
    public void reportNumberOfArchives(int numArchives) {
    }
    public void finishArchive() {
    }
    public void startAnalysis(int numClasses) {
    }
    public void finishClass() {
    }
    public void finishPerClassAnalysis() {
    }
    public void predictPassCount(int[] classesPerPass) {
    }
// noop
    public void startArchive(java.lang.String name) {
    }
}
// noop
