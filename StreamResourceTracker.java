/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Resource tracker which determines where streams are created, and how they are
 * used within the method.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;
import org.apache.bcel.generic.TypedInstruction;
import edu.umd.cs.findbugs.ResourceCollection;
import edu.umd.cs.findbugs.ba.BasicBlock;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Edge;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback;
import edu.umd.cs.findbugs.ba.ResourceTracker;
import edu.umd.cs.findbugs.ba.ResourceValueFrame;
import edu.umd.cs.findbugs.ba.ResourceValueFrameModelingVisitor;
public class StreamResourceTracker extends java.lang.Object implements edu.umd.cs.findbugs.ba.ResourceTracker<edu.umd.cs.findbugs.detect.Stream> {
    private edu.umd.cs.findbugs.detect.StreamFactory[] streamFactoryList;
    private edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback lookupFailureCallback;
    private edu.umd.cs.findbugs.ResourceCollection<edu.umd.cs.findbugs.detect.Stream> resourceCollection;
/**
     * Map of locations where streams are opened to the actual Stream objects.
     */
    private java.util.Map<edu.umd.cs.findbugs.ba.Location, edu.umd.cs.findbugs.detect.Stream> streamOpenLocationMap;
/**
     * Set of all open locations and escapes of uninteresting streams.
     */
// private HashSet<Location> uninterestingStreamEscapeSet;
    private java.util.HashSet<edu.umd.cs.findbugs.detect.Stream> uninterestingStreamEscapeSet;
/**
     * Set of all (potential) stream escapes.
     */
    private java.util.TreeSet<edu.umd.cs.findbugs.detect.StreamEscape> streamEscapeSet;
/**
     * Map of individual streams to equivalence classes. Any time a stream "A"
     * is wrapped with a stream "B", "A" and "B" belong to the same equivalence
     * class. If any stream in an equivalence class is closed, then we consider
     * all of the streams in the equivalence class as having been closed.
     */
    private java.util.Map<edu.umd.cs.findbugs.detect.Stream, edu.umd.cs.findbugs.detect.StreamEquivalenceClass> streamEquivalenceMap;
/**
     * Constructor.
     * 
     * @param streamFactoryList
     *            array of StreamFactory objects which determine where streams
     *            are created
     * @param lookupFailureCallback
     *            used when class hierarchy lookups fail
     */
// @SuppressWarnings("EI2")
    public StreamResourceTracker(edu.umd.cs.findbugs.detect.StreamFactory[] streamFactoryList, edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback lookupFailureCallback) {
        super();
        this.streamFactoryList = streamFactoryList;
        this.lookupFailureCallback = lookupFailureCallback;
        this.streamOpenLocationMap = new java.util.HashMap<edu.umd.cs.findbugs.ba.Location, edu.umd.cs.findbugs.detect.Stream>();
        this.uninterestingStreamEscapeSet = new java.util.HashSet<edu.umd.cs.findbugs.detect.Stream>();
        this.streamEscapeSet = new java.util.TreeSet<edu.umd.cs.findbugs.detect.StreamEscape>();
        this.streamEquivalenceMap = new java.util.HashMap<edu.umd.cs.findbugs.detect.Stream, edu.umd.cs.findbugs.detect.StreamEquivalenceClass>();
    }
/**
     * Set the precomputed ResourceCollection for the method.
     */
    public void setResourceCollection(edu.umd.cs.findbugs.ResourceCollection<edu.umd.cs.findbugs.detect.Stream> resourceCollection) {
        this.resourceCollection = resourceCollection;
    }
/**
     * Indicate that a stream escapes at the given target Location.
     * 
     * @param source
     *            the Stream that is escaping
     * @param target
     *            the target Location (where the stream escapes)
     */
    public void addStreamEscape(edu.umd.cs.findbugs.detect.Stream source, edu.umd.cs.findbugs.ba.Location target) {
        edu.umd.cs.findbugs.detect.StreamEscape streamEscape = new edu.umd.cs.findbugs.detect.StreamEscape(source, target);
        streamEscapeSet.add(streamEscape);
        if (edu.umd.cs.findbugs.detect.FindOpenStream.DEBUG) java.lang.System.out.println("Adding potential stream escape " + streamEscape);
    }
/**
     * Transitively mark all streams into which uninteresting streams (such as
     * System.out) escape. This handles the rule that wrapping an uninteresting
     * stream makes the wrapper uninteresting as well.
     */
    public void markTransitiveUninterestingStreamEscapes() {
        for (java.util.Iterator<edu.umd.cs.findbugs.detect.StreamEscape> i = streamEscapeSet.iterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.detect.StreamEscape streamEscape = i.next();
            if ( !this.isStreamOpenLocation(streamEscape.target)) {
                if (edu.umd.cs.findbugs.detect.FindOpenStream.DEBUG) java.lang.System.out.println("Eliminating false stream escape " + streamEscape);
                i.remove();
            }
        }
        for (java.util.Iterator<edu.umd.cs.findbugs.detect.Stream> i = resourceCollection.resourceIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.detect.Stream stream = i.next();
            edu.umd.cs.findbugs.detect.StreamEquivalenceClass equivalenceClass = new edu.umd.cs.findbugs.detect.StreamEquivalenceClass();
            equivalenceClass.addMember(stream);
            streamEquivalenceMap.put(stream,equivalenceClass);
        }
// Eliminate all stream escapes where the target isn't really
// a stream open location point.
// Build initial stream equivalence classes.
// Each stream starts out in its own separate
// equivalence class.
// Starting with the set of uninteresting stream open location points,
// propagate all uninteresting stream escapes. Iterate until there
// is no change. This also builds the map of stream equivalence classes.
        java.util.Set<edu.umd.cs.findbugs.detect.Stream> orig = new java.util.HashSet<edu.umd.cs.findbugs.detect.Stream>();
        do {
            orig.clear();
            orig.addAll(uninterestingStreamEscapeSet);
            for (edu.umd.cs.findbugs.detect.StreamEscape streamEscape : streamEscapeSet){
                if (this.isUninterestingStreamEscape(streamEscape.source)) {
                    if (edu.umd.cs.findbugs.detect.FindOpenStream.DEBUG) java.lang.System.out.println("Propagating stream escape " + streamEscape);
                    edu.umd.cs.findbugs.detect.Stream target = streamOpenLocationMap.get(streamEscape.target);
                    if (target == null) throw new java.lang.IllegalStateException();
                    uninterestingStreamEscapeSet.add(target);
// Combine equivalence classes for source and target
                    edu.umd.cs.findbugs.detect.StreamEquivalenceClass sourceClass = streamEquivalenceMap.get(streamEscape.source);
                    edu.umd.cs.findbugs.detect.StreamEquivalenceClass targetClass = streamEquivalenceMap.get(target);
                    if (sourceClass != targetClass) {
                        sourceClass.addAll(targetClass);
                        for (java.util.Iterator<edu.umd.cs.findbugs.detect.Stream> j = targetClass.memberIterator(); j.hasNext(); ) {
                            edu.umd.cs.findbugs.detect.Stream stream = j.next();
                            streamEquivalenceMap.put(stream,sourceClass);
                        }
                    }
                }
            }
;
        }
        while ( !orig.equals(uninterestingStreamEscapeSet));
    }
/**
     * Determine if an uninteresting stream escapes at given location.
     * markTransitiveUninterestingStreamEscapes() should be called first.
     * 
     * @param stream
     *            the stream
     * @return true if an uninteresting stream escapes at the location
     */
    public boolean isUninterestingStreamEscape(edu.umd.cs.findbugs.detect.Stream stream) {
        return uninterestingStreamEscapeSet.contains(stream);
    }
/**
     * Indicate that a stream is constructed at this Location.
     * 
     * @param streamOpenLocation
     *            the Location
     * @param stream
     *            the Stream opened at this Location
     */
    public void addStreamOpenLocation(edu.umd.cs.findbugs.ba.Location streamOpenLocation, edu.umd.cs.findbugs.detect.Stream stream) {
        if (edu.umd.cs.findbugs.detect.FindOpenStream.DEBUG) java.lang.System.out.println("Stream open location at " + streamOpenLocation);
        streamOpenLocationMap.put(streamOpenLocation,stream);
        if (stream.isUninteresting()) uninterestingStreamEscapeSet.add(stream);
    }
/**
     * Get the equivalence class for given stream. May only be called if
     * markTransitiveUninterestingStreamEscapes() has been called.
     * 
     * @param stream
     *            the stream
     * @return the set containing the equivalence class for the given stream
     */
    public edu.umd.cs.findbugs.detect.StreamEquivalenceClass getStreamEquivalenceClass(edu.umd.cs.findbugs.detect.Stream stream) {
        return streamEquivalenceMap.get(stream);
    }
/**
     * Determine if given Location is a stream open location point.
     * 
     * @param location
     *            the Location
     */
    private boolean isStreamOpenLocation(edu.umd.cs.findbugs.ba.Location location) {
        return streamOpenLocationMap.get(location) != null;
    }
    public edu.umd.cs.findbugs.detect.Stream isResourceCreation(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.ConstantPoolGen cpg) {
// Use precomputed map of Locations to Stream creations,
// if present. Note that we don't care about preexisting
// resources here.
        if (resourceCollection != null) return resourceCollection.getCreatedResource(new edu.umd.cs.findbugs.ba.Location(handle, basicBlock));
        org.apache.bcel.generic.Instruction ins = handle.getInstruction();
        if ( !(ins instanceof org.apache.bcel.generic.TypedInstruction)) return null;
        org.apache.bcel.generic.Type type = ((org.apache.bcel.generic.TypedInstruction) (ins) ).getType(cpg);
        if ( !(type instanceof org.apache.bcel.generic.ObjectType)) return null;
        edu.umd.cs.findbugs.ba.Location location = new edu.umd.cs.findbugs.ba.Location(handle, basicBlock);
// All StreamFactories are given an opportunity to
// look at the location and possibly identify a created stream.
        for (edu.umd.cs.findbugs.detect.StreamFactory aStreamFactoryList : streamFactoryList){
            edu.umd.cs.findbugs.detect.Stream stream = aStreamFactoryList.createStream(location,(org.apache.bcel.generic.ObjectType) (type) ,cpg,lookupFailureCallback);
            if (stream != null) return stream;
        }
;
        return null;
    }
    public boolean isResourceOpen(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.detect.Stream resource, edu.umd.cs.findbugs.ba.ResourceValueFrame frame) {
        return resource.isStreamOpen(basicBlock,handle,cpg,frame);
    }
    public boolean isResourceClose(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.detect.Stream resource, edu.umd.cs.findbugs.ba.ResourceValueFrame frame) {
        return resource.isStreamClose(basicBlock,handle,cpg,frame,lookupFailureCallback);
    }
    public boolean mightCloseResource(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.ConstantPoolGen cpg) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        return edu.umd.cs.findbugs.detect.Stream.mightCloseStream(basicBlock,handle,cpg);
    }
    public edu.umd.cs.findbugs.ba.ResourceValueFrameModelingVisitor createVisitor(edu.umd.cs.findbugs.detect.Stream resource, org.apache.bcel.generic.ConstantPoolGen cpg) {
        return new edu.umd.cs.findbugs.detect.StreamFrameModelingVisitor(cpg, this, resource);
    }
    public boolean ignoreImplicitExceptions(edu.umd.cs.findbugs.detect.Stream resource) {
        return resource.ignoreImplicitExceptions();
    }
    public boolean ignoreExceptionEdge(edu.umd.cs.findbugs.ba.Edge edge, edu.umd.cs.findbugs.detect.Stream resource, org.apache.bcel.generic.ConstantPoolGen cpg) {
        return false;
    }
    public boolean isParamInstance(edu.umd.cs.findbugs.detect.Stream resource, int slot) {
        return resource.getInstanceParam() == slot;
    }
}
// vim:ts=3
