/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 William Pugh
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Java main application to compute update a historical bug collection with
 * results from another build/analysis.
 * 
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.PackageStats;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.util.ClassName;
public class CountByPackagePrefix extends java.lang.Object {
    public CountByPackagePrefix() {
    }
/**
     *
     */
    final private static java.lang.String USAGE = "Usage: <cmd>  <prefixLength> [<bugs.xml>]";
    public static void main(java.lang.String[] args) throws org.dom4j.DocumentException, java.io.IOException {
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        if (args.length != 1 && args.length != 2) {
            java.lang.System.out.println(USAGE);
            return;
        }
        int prefixLength = java.lang.Integer.parseInt(args[0]);
        edu.umd.cs.findbugs.BugCollection origCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (args.length == 1) origCollection.readXML(java.lang.System.in);
        else origCollection.readXML(args[1]);
        java.util.Map<java.lang.String, java.lang.Integer> map = new java.util.TreeMap<java.lang.String, java.lang.Integer>();
        java.util.Map<java.lang.String, java.lang.Integer> ncss = new java.util.TreeMap<java.lang.String, java.lang.Integer>();
        for (edu.umd.cs.findbugs.BugInstance b : origCollection.getCollection()){
            java.lang.String prefix = edu.umd.cs.findbugs.util.ClassName.extractPackagePrefix(b.getPrimaryClass().getPackageName(),prefixLength);
            java.lang.Integer v = map.get(prefix);
            if (v == null) map.put(prefix,1);
            else map.put(prefix,v + 1);
        }
;
        for (edu.umd.cs.findbugs.PackageStats ps : origCollection.getProjectStats().getPackageStats()){
            java.lang.String prefix = edu.umd.cs.findbugs.util.ClassName.extractPackagePrefix(ps.getPackageName(),prefixLength);
            java.lang.Integer v = ncss.get(prefix);
            if (v == null) ncss.put(prefix,ps.size());
            else ncss.put(prefix,v + ps.size());
        }
;
        for (java.util.Map.Entry<java.lang.String, java.lang.Integer> e : map.entrySet()){
            java.lang.String prefix = e.getKey();
            int warnings = e.getValue();
            if (warnings == 0) continue;
            java.lang.Integer v = ncss.get(prefix);
            if (v == null || v.intValue() == 0) v = 1;
            int density = warnings * 1000000 / v;
            if (warnings < 3 || v < 2000) java.lang.System.out.printf("%4s %4d %4d %s%n"," ",warnings,v / 1000,prefix);
            else java.lang.System.out.printf("%4d %4d %4d %s%n",density,warnings,v / 1000,prefix);
        }
;
    }
}
