package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.MutableComboBoxModel;
import javax.swing.ProgressMonitor;
import javax.swing.ProgressMonitorInputStream;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;
import edu.umd.cs.findbugs.AWTEventQueueExecutor;
import edu.umd.cs.findbugs.IGuiCallback;
import edu.umd.cs.findbugs.util.LaunchBrowser;
abstract public class AbstractSwingGuiCallback extends java.lang.Object implements edu.umd.cs.findbugs.IGuiCallback {
    private edu.umd.cs.findbugs.AWTEventQueueExecutor bugUpdateExecutor = new edu.umd.cs.findbugs.AWTEventQueueExecutor();
    final private java.awt.Component parent;
    public AbstractSwingGuiCallback(java.awt.Component parent) {
        super();
        this.parent = parent;
    }
    public java.util.concurrent.ExecutorService getBugUpdateExecutor() {
        return bugUpdateExecutor;
    }
    public void showMessageDialogAndWait(final java.lang.String message) throws java.lang.InterruptedException {
        if (javax.swing.SwingUtilities.isEventDispatchThread()) javax.swing.JOptionPane.showMessageDialog(parent,message);
        else try {
            javax.swing.SwingUtilities.invokeAndWait(new java.lang.Runnable() {
                public void run() {
                    javax.swing.JOptionPane.showMessageDialog(parent,message);
                }
            });
        }
        catch (java.lang.reflect.InvocationTargetException e){
            throw new java.lang.IllegalStateException(e);
        }
    }
    public void showMessageDialog(final java.lang.String message) {
        if (javax.swing.SwingUtilities.isEventDispatchThread()) javax.swing.JOptionPane.showMessageDialog(parent,message);
        else javax.swing.SwingUtilities.invokeLater(new java.lang.Runnable() {
            public void run() {
                javax.swing.JOptionPane.showMessageDialog(parent,message);
            }
        });
    }
    public int showConfirmDialog(java.lang.String message, java.lang.String title, java.lang.String ok, java.lang.String cancel) {
        return javax.swing.JOptionPane.showOptionDialog(parent,message,title,javax.swing.JOptionPane.OK_CANCEL_OPTION,javax.swing.JOptionPane.PLAIN_MESSAGE,null,new java.lang.Object[]{ok, cancel},ok);
    }
    public java.io.InputStream getProgressMonitorInputStream(java.io.InputStream in, int length, java.lang.String msg) {
        javax.swing.ProgressMonitorInputStream pmin = new javax.swing.ProgressMonitorInputStream(parent, msg, in);
        javax.swing.ProgressMonitor pm = pmin.getProgressMonitor();
        if (length > 0) pm.setMaximum(length);
        return pmin;
    }
    public void displayNonmodelMessage(java.lang.String title, java.lang.String message) {
        edu.umd.cs.findbugs.gui2.DisplayNonmodelMessage.displayNonmodelMessage(title,message,parent,true);
    }
    public java.lang.String showQuestionDialog(java.lang.String message, java.lang.String title, java.lang.String defaultValue) {
        return (java.lang.String) (javax.swing.JOptionPane.showInputDialog(parent,message,title,javax.swing.JOptionPane.QUESTION_MESSAGE,null,null,defaultValue)) ;
    }
    public java.util.List<java.lang.String> showForm(java.lang.String message, java.lang.String title, java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> items) {
        int result = this.showFormDialog(message,title,items);
        if (result != javax.swing.JOptionPane.OK_OPTION) return null;
        this.updateFormItemsFromGui(items);
        java.util.List<java.lang.String> results = new java.util.ArrayList<java.lang.String>();
        for (edu.umd.cs.findbugs.IGuiCallback.FormItem item : items){
            results.add(item.getCurrentValue());
        }
;
        return results;
    }
    public boolean showDocument(java.net.URL u) {
        return edu.umd.cs.findbugs.util.LaunchBrowser.showDocument(u);
    }
    public boolean isHeadless() {
        return false;
    }
    public void invokeInGUIThread(java.lang.Runnable r) {
        javax.swing.SwingUtilities.invokeLater(r);
    }
    private void updateFormItemsFromGui(java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> items) {
        for (edu.umd.cs.findbugs.IGuiCallback.FormItem item : items){
            javax.swing.JComponent field = item.getField();
            if (field instanceof javax.swing.text.JTextComponent) {
                javax.swing.text.JTextComponent textComponent = (javax.swing.text.JTextComponent) (field) ;
                item.setCurrentValue(textComponent.getText());
            }
            else if (field instanceof javax.swing.JComboBox) {
                javax.swing.JComboBox box = (javax.swing.JComboBox) (field) ;
                java.lang.String value = (java.lang.String) (box.getSelectedItem()) ;
                item.setCurrentValue(value);
            }
            item.updated();
        }
;
        this.updateComboBoxes(items);
    }
    private void updateComboBoxes(java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> items) {
        for (edu.umd.cs.findbugs.IGuiCallback.FormItem item : items){
            javax.swing.JComponent field = item.getField();
            if (field instanceof javax.swing.JComboBox) {
                javax.swing.JComboBox box = (javax.swing.JComboBox) (field) ;
                java.util.List<java.lang.String> newPossibleValues = item.getPossibleValues();
                if ( !this.boxModelIsSame(box,newPossibleValues)) {
                    javax.swing.MutableComboBoxModel mmodel = (javax.swing.MutableComboBoxModel) (box.getModel()) ;
                    this.replaceBoxModelValues(mmodel,newPossibleValues);
                    mmodel.setSelectedItem(item.getCurrentValue());
                }
            }
        }
;
    }
    private void replaceBoxModelValues(javax.swing.MutableComboBoxModel mmodel, java.util.List<java.lang.String> newPossibleValues) {
        try {
            while (mmodel.getSize() > 0) mmodel.removeElementAt(0);
        }
        catch (java.lang.Exception e){
        }
        for (java.lang.String value : newPossibleValues){
            mmodel.addElement(value);
        }
;
    }
    private boolean boxModelIsSame(javax.swing.JComboBox box, java.util.List<java.lang.String> newPossibleValues) {
        boolean same = true;
        if (box.getModel().getSize() != newPossibleValues.size()) same = false;
        else for (int i = 0; i < box.getModel().getSize(); i++) {
            if ( !box.getModel().getElementAt(i).equals(newPossibleValues.get(i))) {
                same = false;
                break;
            }
        }
        return same;
    }
    private int showFormDialog(java.lang.String message, java.lang.String title, final java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> items) {
        javax.swing.JPanel panel = new javax.swing.JPanel();
        panel.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gbc = new java.awt.GridBagConstraints();
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.weightx = 1;
        gbc.weighty = 0;
        gbc.gridwidth = 2;
        gbc.gridy = 1;
        gbc.insets = new java.awt.Insets(5, 5, 5, 5);
        panel.add(new javax.swing.JLabel(message),gbc);
        gbc.gridwidth = 1;
        for (edu.umd.cs.findbugs.IGuiCallback.FormItem item : items){
            item.setItems(items);
            gbc.gridy++;
            panel.add(new javax.swing.JLabel(item.getLabel()),gbc);
            java.lang.String defaultValue = item.getDefaultValue();
            if (item.getPossibleValues() != null) {
                javax.swing.JComboBox box = this.createComboBox(items,item);
                panel.add(box,gbc);
            }
            else {
                javax.swing.JTextField field = this.createTextField(items,item);
                panel.add(field,gbc);
            }
        }
;
        return javax.swing.JOptionPane.showConfirmDialog(parent,panel,title,javax.swing.JOptionPane.OK_CANCEL_OPTION);
    }
    private javax.swing.JTextField createTextField(final java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> items, edu.umd.cs.findbugs.IGuiCallback.FormItem item) {
        java.lang.String defaultValue = item.getDefaultValue();
        javax.swing.JTextField field = (item.isPassword() ? new javax.swing.JPasswordField() : new javax.swing.JTextField());
        if (defaultValue != null) {
            field.setText(defaultValue);
        }
        item.setField(field);
        field.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
            public void insertUpdate(javax.swing.event.DocumentEvent e) {
                this.changed();
            }
            public void removeUpdate(javax.swing.event.DocumentEvent e) {
                this.changed();
            }
            public void changedUpdate(javax.swing.event.DocumentEvent e) {
                this.changed();
            }
            private void changed() {
                edu.umd.cs.findbugs.gui2.AbstractSwingGuiCallback.this.updateFormItemsFromGui(items);
            }
        });
        return field;
    }
    private javax.swing.JComboBox createComboBox(final java.util.List<edu.umd.cs.findbugs.IGuiCallback.FormItem> items, edu.umd.cs.findbugs.IGuiCallback.FormItem item) {
        javax.swing.DefaultComboBoxModel model = new javax.swing.DefaultComboBoxModel();
        javax.swing.JComboBox box = new javax.swing.JComboBox(model);
        item.setField(box);
        for (java.lang.String possibleValue : item.getPossibleValues()){
            model.addElement(possibleValue);
        }
;
        java.lang.String defaultValue = item.getDefaultValue();
        if (defaultValue == null) model.setSelectedItem(model.getElementAt(0));
        else model.setSelectedItem(defaultValue);
        box.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                edu.umd.cs.findbugs.gui2.AbstractSwingGuiCallback.this.updateFormItemsFromGui(items);
            }
        });
        return box;
    }
}
