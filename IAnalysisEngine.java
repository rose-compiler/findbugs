/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An engine for analyzing classes or methods.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile;
import edu.umd.cs.findbugs.classfile.*;
abstract public interface IAnalysisEngine<ResultType, DescriptorType> {
/**
     * Perform an analysis on class or method named by given descriptor.
     * 
     * @param analysisCache
     *            the analysis cache
     * @param descriptor
     *            the descriptor of the class or method to be analyzed
     * @return the result of the analysis of the class or method
     */
    abstract public ResultType analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, DescriptorType descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
/**
     * Register the analysis engine with given analysis cache.
     * 
     * @param analysisCache
     *            the analysis cache
     */
    abstract public void registerWith(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache);
}
