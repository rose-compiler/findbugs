/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2008, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Type matcher that determines if a candidate Type is a subtype of a given
 * Type.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.util;
import edu.umd.cs.findbugs.util.*;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.ba.ch.Subtypes2;
import edu.umd.cs.findbugs.bcel.BCELUtil;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
public class SubtypeTypeMatcher extends java.lang.Object implements edu.umd.cs.findbugs.util.TypeMatcher {
    private org.apache.bcel.generic.ReferenceType supertype;
/**
     * Constructor.
     * 
     * @param supertype
     *            a ReferenceType: this TypeMatcher will test whether or not
     *            candidate Types are subtypes of this Type
     */
    public SubtypeTypeMatcher(org.apache.bcel.generic.ReferenceType supertype) {
        super();
        this.supertype = supertype;
    }
/**
     * Constructor.
     * 
     * @param classDescriptor
     *            a ClassDescriptor naming a class: this TypeMatcher will test
     *            whether or not candidate Types are subtypes of the class
     */
    public SubtypeTypeMatcher(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor) {
        this(edu.umd.cs.findbugs.bcel.BCELUtil.getObjectTypeInstance(classDescriptor.toDottedClassName()));
    }
    public boolean matches(org.apache.bcel.generic.Type t) {
        if ( !(t instanceof org.apache.bcel.generic.ReferenceType)) {
            return false;
        }
        edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache();
        edu.umd.cs.findbugs.ba.ch.Subtypes2 subtypes2 = analysisCache.getDatabase(edu.umd.cs.findbugs.ba.ch.Subtypes2.class);
        try {
            return subtypes2.isSubtype((org.apache.bcel.generic.ReferenceType) (t) ,supertype);
        }
        catch (java.lang.ClassNotFoundException e){
            analysisCache.getErrorLogger().reportMissingClass(e);
            return false;
        }
    }
    public java.lang.String toString() {
        return "+" + supertype.toString();
    }
}
