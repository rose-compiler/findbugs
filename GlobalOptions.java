package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import javax.annotation.CheckForNull;
abstract public interface GlobalOptions {
    abstract java.lang.String getGlobalOption(java.lang.String key);
    abstract edu.umd.cs.findbugs.Plugin getGlobalOptionSetter(java.lang.String key);
}
