/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import edu.umd.cs.findbugs.config.AnalysisFeatureSetting;
import edu.umd.cs.findbugs.config.UserPreferences;
public class AnalysisOptions extends java.lang.Object {
/**
     *
     */
    public boolean relaxedReportingMode;
/**
     *
     */
    public boolean abridgedMessages;
/**
     *
     */
    public java.lang.String trainingInputDir;
/**
     *
     */
    public java.lang.String trainingOutputDir;
/**
     *
     */
    public java.lang.String sourceInfoFileName;
/**
     *
     */
    public edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] analysisFeatureSettingList;
/**
     *
     */
    public boolean scanNestedArchives;
/**
     *
     */
    public boolean applySuppression;
/**
     *
     */
    public boolean mergeSimilarWarnings;
/**
     *
     */
    public boolean noClassOk;
    java.lang.String releaseName;
    java.lang.String projectName;
/**
     *
     */
    public AnalysisOptions(boolean mergeSimilarWarnings) {
        super();
        this.mergeSimilarWarnings = mergeSimilarWarnings;
    }
    edu.umd.cs.findbugs.config.UserPreferences userPreferences;
}
