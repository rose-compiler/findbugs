package edu.umd.cs.findbugs.classfile.analysis;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.analysis.*;
/**
 * The "raw" version of an annotation appearing in a class file.
 * 
 * @author William Pugh
 */
/**
     * Constructor.
     * 
     * @param annotationClass
     *            the annotation class
     */
/**
     * Constructor.
     * 
     * @param annotationClass
     *            JVM signature of the annotation class
     */
/**
     * @return ClassDescriptor referring to the annotation class
     */
/**
     * Get the value of given annotation element. See <a href=
     * "http://asm.objectweb.org/current/doc/javadoc/user/org/objectweb/asm/AnnotationVisitor.html"
     * >AnnotationVisitor Javadoc</a> for information on what the object
     * returned could be.
     * 
     * @param name
     *            name of annotation element
     * @return the element value (primitive value, String value, enum value,
     *         Type, or array of one of the previous)
     */
/**
     * Get a descriptor specifying the type of an annotation element.
     * 
     * @param name
     *            name of annotation element
     * @return descriptor specifying the type of the annotation element
     */
/**
     * Get an AnnotationVisitor which can populate this AnnotationValue object.
     */
/*
             * (non-Javadoc)
             * 
             * @see
             * org.objectweb.asm.AnnotationVisitor#visitAnnotation(java.lang
             * .String, java.lang.String)
             */
/*
             * (non-Javadoc)
             * 
             * @see
             * org.objectweb.asm.AnnotationVisitor#visitArray(java.lang.String)
             */
/*
             * (non-Javadoc)
             * 
             * @see org.objectweb.asm.AnnotationVisitor#visitEnd()
             */
/*
             * (non-Javadoc)
             * 
             * @see
             * org.objectweb.asm.AnnotationVisitor#visitEnum(java.lang.String,
             * java.lang.String, java.lang.String)
             */
/**
         *
         */
/**
         *
         */
/**
         * @param name
         * @param result
         */
abstract interface package-info {
}
