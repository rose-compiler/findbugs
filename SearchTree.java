/*
 * Generic graph library
 * Copyright (C) 2000,2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// $Revision: 1.6 $
/**
 * SearchTree represents a search tree produced by a graph search algorithm,
 * such as BreadthFirstSearch or DepthFirstSearch.
 */
package edu.umd.cs.findbugs.graph;
import edu.umd.cs.findbugs.graph.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
public class SearchTree<VertexType & edu.umd.cs.findbugs.graph.GraphVertex<VertexType>> extends java.lang.Object {
    private VertexType m_vertex;
    private java.util.ArrayList<edu.umd.cs.findbugs.graph.SearchTree<VertexType>> m_childList;
/**
     * Create a new search tree.
     */
    public SearchTree(VertexType v) {
        super();
        m_vertex = v;
        m_childList = new java.util.ArrayList<edu.umd.cs.findbugs.graph.SearchTree<VertexType>>();
    }
/**
     * Get the vertex contained in this node.
     */
    public VertexType getVertex() {
        return m_vertex;
    }
/**
     * Add a child search tree.
     */
    public void addChild(edu.umd.cs.findbugs.graph.SearchTree<VertexType> child) {
        m_childList.add(child);
    }
/**
     * Return collection of children of this search tree. (Elements returned are
     * also SearchTree objects).
     */
    public java.util.Iterator<edu.umd.cs.findbugs.graph.SearchTree<VertexType>> childIterator() {
        return m_childList.iterator();
    }
/**
     * Add all vertices contained in this search tree to the given set.
     */
    public void addVerticesToSet(java.util.Set<VertexType> set) {
        set.add(this.m_vertex);
// Add the vertex for this object
// Add vertices for all children
        java.util.Iterator<edu.umd.cs.findbugs.graph.SearchTree<VertexType>> i = this.childIterator();
        while (i.hasNext()) {
            edu.umd.cs.findbugs.graph.SearchTree<VertexType> child = i.next();
            child.addVerticesToSet(set);
        }
    }
}
// vim:ts=4
