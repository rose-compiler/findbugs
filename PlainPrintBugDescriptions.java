/*
 * Generate HTML file containing bug descriptions
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.tools.html;
import edu.umd.cs.findbugs.tools.html.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import edu.umd.cs.findbugs.BugPattern;
import edu.umd.cs.findbugs.charsets.UTF8;
public class PlainPrintBugDescriptions extends edu.umd.cs.findbugs.tools.html.PrintBugDescriptions {
    private java.lang.String docTitle;
    private java.io.PrintStream out;
    public PlainPrintBugDescriptions(java.lang.String docTitle, java.io.OutputStream out) {
        super();
        this.docTitle = docTitle;
        this.out = edu.umd.cs.findbugs.charsets.UTF8.printStream(out);
    }
    protected java.lang.String getDocTitle() {
        return docTitle;
    }
    protected java.io.PrintStream getPrintStream() {
        return out;
    }
    protected void prologue() throws java.io.IOException {
        out.println("<html><head><title>" + docTitle + "</title>");
        this.header();
        out.println("</head><body>");
        this.beginBody();
        out.println("<h1>" + docTitle + "</h1>");
    }
    protected void emit(edu.umd.cs.findbugs.BugPattern bugPattern) throws java.io.IOException {
        out.println("<h2>" + bugPattern.getAbbrev() + ": " + bugPattern.getShortDescription() + "</h2>");
        out.println(bugPattern.getDetailText());
    }
    protected void epilogue() throws java.io.IOException {
        this.endBody();
        out.println("</body></html>");
    }
/** Extra stuff that can be printed in the &lt;head&gt; element. */
    protected void header() throws java.io.IOException {
    }
/** Extra stuff printed at the beginning of the &lt;body&gt; element. */
    protected void beginBody() throws java.io.IOException {
    }
/** Extra stuff printed at the end of the &lt;body&gt; element. */
    protected void endBody() throws java.io.IOException {
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        java.lang.String docTitle = "FindBugs Bug Descriptions";
        if (args.length > 0) docTitle = args[0];
        new edu.umd.cs.findbugs.tools.html.PlainPrintBugDescriptions(docTitle, java.lang.System.out).print();
    }
}
// vim:ts=3
