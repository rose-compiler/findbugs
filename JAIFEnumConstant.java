/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.jaif;
import edu.umd.cs.findbugs.jaif.*;
public class JAIFEnumConstant extends java.lang.Object {
    private java.lang.String name;
    public JAIFEnumConstant(java.lang.String name) {
        super();
        this.name = name;
    }
/**
     * @return Returns the name.
     */
    public java.lang.String getName() {
        return name;
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public java.lang.String toString() {
        return "enum constant " + name;
    }
}
