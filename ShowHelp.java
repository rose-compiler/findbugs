/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005,2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Show command line help.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.util.TreeSet;
public class ShowHelp extends java.lang.Object {
    public ShowHelp() {
    }
    public static void main(java.lang.String[] args) {
        java.lang.System.out.println("FindBugs version " + edu.umd.cs.findbugs.Version.RELEASE + ", " + edu.umd.cs.findbugs.Version.WEBSITE);
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        java.lang.System.out.println("Command line options");
        java.util.TreeSet<edu.umd.cs.findbugs.FindBugsMain> cmds = new java.util.TreeSet<edu.umd.cs.findbugs.FindBugsMain>();
        for (edu.umd.cs.findbugs.Plugin p : edu.umd.cs.findbugs.Plugin.getAllPlugins())for (edu.umd.cs.findbugs.FindBugsMain m : p.getAllFindBugsMain())cmds.add(m);
;
;
        for (edu.umd.cs.findbugs.FindBugsMain m : cmds)java.lang.System.out.printf("fb %-12s %-12s %s%n",m.cmd,m.kind,m.description);
;
        java.lang.System.out.println();
        showGeneralOptions();
    }
//        System.out.println();
//        System.out.println("GUI Options:");
//        FindBugsCommandLine guiCmd = new FindBugsCommandLine(true) {
//        };
//        guiCmd.printUsage(System.out);
//        System.out.println();
//        System.out.println("TextUI Options:");
//        FindBugs.showCommandLineOptions();
    public static void showSynopsis() {
        java.lang.System.out.println("Usage: findbugs [general options] [gui options]");
    }
    public static void showGeneralOptions() {
        java.lang.System.out.println("General options:");
        java.lang.System.out.println("  -jvmArgs args    Pass args to JVM");
        java.lang.System.out.println("  -maxHeap size    Maximum Java heap size in megabytes (default=768)");
        java.lang.System.out.println("  -javahome <dir>  Specify location of JRE");
    }
}
