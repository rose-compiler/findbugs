/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.WillClose;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.cloud.Cloud;
import edu.umd.cs.findbugs.model.ClassFeatureSet;
import edu.umd.cs.findbugs.xml.XMLOutput;
abstract public interface BugCollection extends java.lang.Iterable<edu.umd.cs.findbugs.BugInstance> {
    final public static java.lang.String ROOT_ELEMENT_NAME = "BugCollection";
    final public static java.lang.String SRCMAP_ELEMENT_NAME = "SrcMap";
    final public static java.lang.String PROJECT_ELEMENT_NAME = "Project";
    final public static java.lang.String ERRORS_ELEMENT_NAME = "Errors";
// 0.8.6
    final public static java.lang.String ANALYSIS_ERROR_ELEMENT_NAME = "AnalysisError";
// and
// earlier
// 0.8.7 and later
    final public static java.lang.String ERROR_ELEMENT_NAME = "Error";
// 0.8.7
    final public static java.lang.String ERROR_MESSAGE_ELEMENT_NAME = "ErrorMessage";
// and
// later
// 0.8.7 and
    final public static java.lang.String ERROR_EXCEPTION_ELEMENT_NAME = "Exception";
// later
// 0.8.7
    final public static java.lang.String ERROR_STACK_TRACE_ELEMENT_NAME = "StackTrace";
// and
// later
    final public static java.lang.String MISSING_CLASS_ELEMENT_NAME = "MissingClass";
    final public static java.lang.String SUMMARY_HTML_ELEMENT_NAME = "SummaryHTML";
    final public static java.lang.String APP_CLASS_ELEMENT_NAME = "AppClass";
// 0.9.2 and
    final public static java.lang.String CLASS_HASHES_ELEMENT_NAME = "ClassHashes";
// later
// 0.9.2 and later
    final public static java.lang.String HISTORY_ELEMENT_NAME = "History";
    abstract public edu.umd.cs.findbugs.Project getProject();
/**
     * Set the current release name.
     *
     * @param releaseName
     *            the current release name
     */
    abstract public void setReleaseName(java.lang.String releaseName);
/**
     * Get the current release name.
     *
     * @return current release name
     */
    abstract public java.lang.String getReleaseName();
/**
     * Get the project stats.
     */
    abstract public edu.umd.cs.findbugs.ProjectStats getProjectStats();
/**
     * Get the timestamp for the analyzed code (when it was compiled)
     *
     * @param timestamp
     *            the timestamp.
     */
    abstract public void setTimestamp(long timestamp);
/**
     * Get the timestamp for the analyzed code (when it was compiled)
     */
    abstract public long getTimestamp();
/**
     * Set the timestamp for when the analysis was performed.
     *
     * @param timestamp
     *            the analysis timestamp.
     */
    abstract public void setAnalysisTimestamp(long timestamp);
/**
     * Set the version of FindBugs used to perform the analysis
     *
     * @param analysisVersion
     *            the analysis version.
     */
    abstract public void setAnalysisVersion(java.lang.String analysisVersion);
/**
     * Get the timestamp for when the analysis was performed.
     */
    abstract public long getAnalysisTimestamp();
/**
     * Gets the AppVersion corresponding to the given sequence number.
     */
    abstract public edu.umd.cs.findbugs.AppVersion getAppVersionFromSequenceNumber(long target);
/**
     * Set the sequence number of the BugCollection.
     *
     * @param sequence
     *            the sequence number
     * @see BugCollection#getSequenceNumber()
     */
    abstract public void setSequenceNumber(long sequence);
/**
     * Get the sequence number of the BugCollection. This value represents the
     * number of times the user has analyzed a different version of the
     * application and updated the historical bug collection using the
     * UpdateBugCollection class.
     *
     * @return the sequence number
     */
    abstract public long getSequenceNumber();
    abstract public boolean isMultiversion();
    abstract public boolean hasDeadBugs();
/**
     * Clear all AppVersions representing previously-analyzed versions of the
     * application.
     */
    abstract public void clearAppVersions();
/**
     * Add an AppVersion representing a version of the analyzed application.
     *
     * @param appVersion
     *            the AppVersion
     */
    abstract public void addAppVersion(edu.umd.cs.findbugs.AppVersion appVersion);
/**
     * Get the current AppVersion.
     */
    abstract public edu.umd.cs.findbugs.AppVersion getCurrentAppVersion();
/**
     * Get an Iterator over AppVersions defined in the collection.
     */
    abstract public java.util.Iterator<edu.umd.cs.findbugs.AppVersion> appVersionIterator();
/**
     * Add a BugInstance to this BugCollection. This just calls add(bugInstance,
     * true).
     *
     * @param bugInstance
     *            the BugInstance
     * @return true if the BugInstance was added, or false if a matching
     *         BugInstance was already in the BugCollection
     */
    abstract public boolean add(edu.umd.cs.findbugs.BugInstance bugInstance);
/**
     * Add a BugInstance to this BugCollection.
     *
     * @param bugInstance
     *            the BugInstance
     * @param updateActiveTime
     *            true if the warning's active time should be updated to include
     *            the collection's current time
     * @return true if the BugInstance was added, or false if a matching
     *         BugInstance was already in the BugCollection
     */
    abstract public boolean add(edu.umd.cs.findbugs.BugInstance bugInstance, boolean updateActiveTime);
/**
     * Look up a BugInstance by its unique id.
     *
     * @param uniqueId
     *            the BugInstance's unique id.
     * @return the BugInstance with the given unique id, or null if there is no
     *         such BugInstance
     *
     *         This is deprecated; uniqueIDs are not persistent.
     */
    abstract public edu.umd.cs.findbugs.BugInstance lookupFromUniqueId(java.lang.String uniqueId);
/**
     * Add an analysis error.
     *
     * @param message
     *            the error message
     */
    abstract public void addError(java.lang.String message);
/**
     * Add an analysis error.
     *
     * @param error
     *            the AnalysisError object to add
     */
    abstract public void addError(edu.umd.cs.findbugs.AnalysisError error);
/**
     * Add a missing class message.
     *
     * @param message
     *            the missing class message
     */
    abstract public void addMissingClass(java.lang.String message);
    abstract public void setClassFeatureSet(edu.umd.cs.findbugs.model.ClassFeatureSet classFeatureSet);
    abstract public void writePrologue(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException;
    abstract public void writeEpilogue(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException;
    abstract public void clearClassFeatures();
    abstract public void clearMissingClasses();
/**
     * Read XML data from given file into this object, populating given Project
     * as a side effect.
     *
     * @param fileName
     *            name of the file to read
     */
    abstract public void readXML(java.lang.String fileName) throws org.dom4j.DocumentException, java.io.IOException;
/**
     * Read XML data from given input stream into this object, populating the
     * Project as a side effect. An attempt will be made to close the input
     * stream (even if an exception is thrown).
     *
     * @param in
     *            the InputStream
     */
    abstract public void readXML(java.io.InputStream in) throws org.dom4j.DocumentException, java.io.IOException;
/**
     * Read XML data from given reader into this object, populating the Project
     * as a side effect. An attempt will be made to close the reader (even if an
     * exception is thrown).
     *
     * @param reader
     *            the Reader
     */
    abstract public void readXML(java.io.Reader reader) throws org.dom4j.DocumentException, java.io.IOException;
/**
     * Write this BugCollection to a file as XML.
     *
     * @param fileName
     *            the file to write to
     */
    abstract public void writeXML(java.lang.String fileName) throws java.io.IOException;
/**
     * Write the BugCollection to given output stream as XML. The output stream
     * will be closed, even if an exception is thrown.
     *
     * @param out
     *            the OutputStream to write to
     */
    abstract public void writeXML(java.io.Writer out) throws java.io.IOException;
/**
     * Write the BugCollection to given output stream as XML using a UTF8 encoding.
     * The output stream
     * will be closed, even if an exception is thrown.
     *
     * @param out
     *            the OutputStream to write to
     */
    abstract public void writeXML(java.io.OutputStream out) throws java.io.IOException;
/**
     * Write the BugCollection to an XMLOutput object. The finish() method of
     * the XMLOutput object is guaranteed to be called.
     *
     * <p>
     * To write the SummaryHTML element, set property
     * findbugs.report.SummaryHTML to "true".
     * </p>
     *
     * @param xmlOutput
     *            the XMLOutput object
     */
    abstract public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException;
/**
     * Return an Iterator over all the BugInstance objects in the BugCollection.
     */
    abstract public java.util.Iterator<edu.umd.cs.findbugs.BugInstance> iterator();
/**
     * Return the Collection storing the BugInstance objects.
     */
    abstract public java.util.Collection<edu.umd.cs.findbugs.BugInstance> getCollection();
/**
     * Convert the BugCollection into a dom4j Document object.
     *
     * @return the Document representing the BugCollection as a dom4j tree
     */
    abstract public org.dom4j.Document toDocument();
/**
     * Create a new empty BugCollection with the same metadata as this one.
     *
     * @return a new empty BugCollection with the same metadata as this one
     */
    abstract public edu.umd.cs.findbugs.BugCollection createEmptyCollectionWithMetadata();
/**
     * Set whether textual messages should be added to any generated XML
     */
    abstract public void setWithMessages(boolean withMessages);
/**
     * Set whether we should minimize XML
     */
    abstract public void setMinimalXML(boolean minimalXML);
/**
     * Return whether textual messages will be added to any generated XML
     */
    abstract public boolean getWithMessages();
    abstract public edu.umd.cs.findbugs.BugInstance findBug(java.lang.String instanceHash, java.lang.String bugType, int lineNumber);
    abstract public boolean isApplySuppressions();
    abstract public void setApplySuppressions(boolean applySuppressions);
/**
     * Get the instance of user annotation plugin
     *
     * @return user annotation plugin 
     */
    abstract public edu.umd.cs.findbugs.cloud.Cloud getCloud();
    abstract public edu.umd.cs.findbugs.cloud.Cloud getCloudLazily();
    abstract public edu.umd.cs.findbugs.cloud.Cloud reinitializeCloud();
    abstract public void setXmlCloudDetails(java.util.Map<java.lang.String, java.lang.String> map);
    abstract public java.util.Map<java.lang.String, java.lang.String> getXmlCloudDetails();
/** Note that we are done adding bugs to this bug collection */
    abstract public void bugsPopulated();
}
