/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
package edu.umd.cs.findbugs.sourceViewer;
import edu.umd.cs.findbugs.sourceViewer.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.ComponentView;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
class NumberedViewFactory extends java.lang.Object implements javax.swing.text.ViewFactory {
    final edu.umd.cs.findbugs.sourceViewer.HighlightInformation highlight;
    public NumberedViewFactory(edu.umd.cs.findbugs.sourceViewer.HighlightInformation highlight) {
        super();
        this.highlight = highlight;
    }
    public javax.swing.text.View create(javax.swing.text.Element elem) {
        java.lang.String kind = elem.getName();
// System.out.println("Kind: " + kind);
        if (kind != null) if (kind.equals(javax.swing.text.AbstractDocument.ContentElementName)) {
            return new javax.swing.text.LabelView(elem);
        }
        else if (kind.equals(javax.swing.text.AbstractDocument.ParagraphElementName)) {
            return new edu.umd.cs.findbugs.sourceViewer.NumberedParagraphView(elem, highlight);
        }
        else if (kind.equals(javax.swing.text.AbstractDocument.SectionElementName)) {
            return new edu.umd.cs.findbugs.sourceViewer.NoWrapBoxView(elem, javax.swing.text.View.Y_AXIS);
        }
        else if (kind.equals(javax.swing.text.StyleConstants.ComponentElementName)) {
            return new javax.swing.text.ComponentView(elem);
        }
        else if (kind.equals(javax.swing.text.StyleConstants.IconElementName)) {
            return new javax.swing.text.IconView(elem);
        }
// default to text display
        return new javax.swing.text.LabelView(elem);
    }
}
