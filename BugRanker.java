/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Bug rankers are used to compute a bug rank for each bug instance. Bug ranks
 * 1-20 are for bugs that are visible to users. Bug rank 1 is more the most
 * relevant/scary bugs. A bug rank greater than 20 is for issues that should not
 * be shown to users.
 *
 *
 * The following bug rankers may exist:
 * <ul>
 * <li>core bug ranker (loaded from etc/bugrank.txt)
 * <li>a bug ranker for each plugin (loaded from <plugin>/etc/bugrank.txt)
 * <li>A global adjustment ranker (loaded from plugins/adjustBugrank.txt)
 * </ul>
 *
 * A bug ranker is comprised of a list of bug patterns, bug kinds and bug
 * categories. For each, either an absolute or relative bug rank is provided. A
 * relative rank is one preceeded by a + or -.
 *
 * For core bug detectors, the bug ranker search order is:
 * <ul>
 * <li>global adjustment bug ranker
 * <li>core bug ranker
 * </ul>
 *
 * For third party plugins, the bug ranker search order is:
 * <ul>
 * <li>global adjustment bug ranker
 * <li>plugin adjustment bug ranker
 * <li>core bug ranker
 * </ul>
 *
 * The overall search order is
 * <ul>
 * <li>Bug patterns, in search order across bug rankers
 * <li>Bug kinds, in search order across bug rankers
 * <li>Bug categories, in search order across bug rankers
 * </ul>
 *
 * Search stops at the first absolute bug rank found, and the result is the sum
 * of all of relative bug ranks plus the final absolute bug rank. Since all bug
 * categories are defined by the core bug ranker, we should always find an
 * absolute bug rank.
 *
 *
 *
 * @author Bill Pugh
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.annotation.CheckForNull;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.util.Util;
public class BugRanker extends java.lang.Object {
    static class Scorer extends java.lang.Object {
        public Scorer() {
        }
        final private java.util.HashMap<java.lang.String, java.lang.Integer> adjustment = new java.util.HashMap<java.lang.String, java.lang.Integer>();
        final private java.util.HashSet<java.lang.String> isRelative = new java.util.HashSet<java.lang.String>();
        int get(java.lang.String key) {
            java.lang.Integer v = adjustment.get(key);
            if (v == null) return 0;
            return v;
        }
        boolean isRelative(java.lang.String key) {
            return  !adjustment.containsKey(key) || isRelative.contains(key);
        }
        void storeAdjustment(java.lang.String key, java.lang.String value) {
            for (java.lang.String k : key.split(",")){
                char firstChar = value.charAt(0);
                if (firstChar == '\u002b') value = value.substring(1);
                int v = java.lang.Integer.parseInt(value);
                adjustment.put(k,v);
                if (firstChar == '\u002b' || firstChar == '\u002d') isRelative.add(k);
            }
;
        }
    }
    final static boolean PLUGIN_DEBUG = java.lang.Boolean.getBoolean("bugranker.plugin.debug");
/**
     * @param u
     *            may be null. In this case, a default value will be used for
     *            all bugs
     * @throws IOException
     */
    public BugRanker(java.net.URL u) throws java.io.IOException {
        super();
        if (u == null) {
            return;
        }
        java.io.BufferedReader in = edu.umd.cs.findbugs.charsets.UTF8.bufferedReader(u.openStream());
        try {
            while (true) {
                java.lang.String s = in.readLine();
                if (s == null) break;
                s = s.trim();
                if (s.length() == 0) continue;
                java.lang.String[] parts = s.split(" ");
                java.lang.String rank = parts[0];
                java.lang.String kind = parts[1];
                java.lang.String what = parts[2];
                if (kind.equals("BugPattern")) bugPatterns.storeAdjustment(what,rank);
                else if (kind.equals("BugKind")) bugKinds.storeAdjustment(what,rank);
                else if (kind.equals("Category")) bugCategories.storeAdjustment(what,rank);
                else edu.umd.cs.findbugs.ba.AnalysisContext.logError("Can't parse bug rank " + s);
            }
        }
        finally {
            edu.umd.cs.findbugs.util.Util.closeSilently(in);
        }
    }
    final private edu.umd.cs.findbugs.BugRanker.Scorer bugPatterns = new edu.umd.cs.findbugs.BugRanker.Scorer();
    final private edu.umd.cs.findbugs.BugRanker.Scorer bugKinds = new edu.umd.cs.findbugs.BugRanker.Scorer();
    final private edu.umd.cs.findbugs.BugRanker.Scorer bugCategories = new edu.umd.cs.findbugs.BugRanker.Scorer();
/**
     *
     */
    final public static java.lang.String FILENAME = "bugrank.txt";
    final public static java.lang.String ADJUST_FILENAME = "adjustBugrank.txt";
    private static int priorityAdjustment(int priority) {
        switch(priority){
            case edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY:{
                return 0;
            }
            case edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY:{
                return 2;
            }
            case edu.umd.cs.findbugs.Priorities.LOW_PRIORITY:{
                return 5;
            }
            default:{
                return 10;
            }
        }
    }
    private static int adjustRank(int patternRank, int priority) {
        int priorityAdjustment = priorityAdjustment(priority);
        if (patternRank > 20) return patternRank + priorityAdjustment;
        return java.lang.Math.max(1,java.lang.Math.min(patternRank + priorityAdjustment,20));
    }
    private static int rankBugPattern(edu.umd.cs.findbugs.BugPattern bugPattern, edu.umd.cs.findbugs.BugRanker[] rankers) {
        java.lang.String type = bugPattern.getType();
        int rank = 0;
        for (edu.umd.cs.findbugs.BugRanker b : rankers)if (b != null) {
            rank += b.bugPatterns.get(type);
            if ( !b.bugPatterns.isRelative(type)) return rank;
        }
;
        java.lang.String kind = bugPattern.getAbbrev();
        for (edu.umd.cs.findbugs.BugRanker b : rankers)if (b != null) {
            rank += b.bugKinds.get(kind);
            if ( !b.bugKinds.isRelative(kind)) return rank;
        }
;
        java.lang.String category = bugPattern.getCategory();
        for (edu.umd.cs.findbugs.BugRanker b : rankers)if (b != null) {
            rank += b.bugCategories.get(category);
            if ( !b.bugCategories.isRelative(category)) return rank;
        }
;
        return rank;
    }
    private static edu.umd.cs.findbugs.BugRanker getCoreRanker() {
        edu.umd.cs.findbugs.Plugin corePlugin = edu.umd.cs.findbugs.PluginLoader.getCorePluginLoader().getPlugin();
        return corePlugin.getBugRanker();
    }
    public static int findRank(edu.umd.cs.findbugs.BugInstance bug) {
        int patternRank = findRank(bug.getBugPattern(),bug.getDetectorFactory());
        return adjustRank(patternRank,bug.getPriority());
    }
    public static int findRank(edu.umd.cs.findbugs.BugPattern bugPattern, int priority) {
        int patternRank = findRank(bugPattern,null);
        return adjustRank(patternRank,priority);
    }
    private static edu.umd.cs.findbugs.AnalysisLocal<java.util.HashMap<edu.umd.cs.findbugs.BugPattern, java.lang.Integer>> rankForBugPattern = new edu.umd.cs.findbugs.AnalysisLocal<java.util.HashMap<edu.umd.cs.findbugs.BugPattern, java.lang.Integer>>() {
        protected java.util.HashMap<edu.umd.cs.findbugs.BugPattern, java.lang.Integer> initialValue() {
            return new java.util.HashMap<edu.umd.cs.findbugs.BugPattern, java.lang.Integer>();
        }
    };
    public static int findRank(edu.umd.cs.findbugs.BugPattern pattern, edu.umd.cs.findbugs.DetectorFactory detectorFactory) {
        boolean haveCache = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache() != null;
        if (haveCache) {
            java.lang.Integer cachedResult = rankForBugPattern.get().get(pattern);
            if (cachedResult != null) return cachedResult;
        }
        int rank;
        if (detectorFactory == null) rank = findRankUnknownPlugin(pattern);
        else {
            edu.umd.cs.findbugs.Plugin plugin = detectorFactory.getPlugin();
            edu.umd.cs.findbugs.BugRanker pluginRanker = plugin.getBugRanker();
            edu.umd.cs.findbugs.BugRanker coreRanker = getCoreRanker();
            if (pluginRanker == coreRanker) rank = rankBugPattern(pattern,coreRanker);
            else rank = rankBugPattern(pattern,pluginRanker,coreRanker);
        }
        if (haveCache) rankForBugPattern.get().put(pattern,rank);
        return rank;
    }
    private static int findRankUnknownPlugin(edu.umd.cs.findbugs.BugPattern pattern) {
        java.util.List<edu.umd.cs.findbugs.BugRanker> rankers = new java.util.ArrayList<edu.umd.cs.findbugs.BugRanker>();
        pluginLoop:for (edu.umd.cs.findbugs.Plugin plugin : edu.umd.cs.findbugs.Plugin.getAllPlugins()){
            if (plugin.isCorePlugin()) continue;
            if (false) {
                rankers.add(plugin.getBugRanker());
                continue pluginLoop;
            }
            for (edu.umd.cs.findbugs.DetectorFactory df : plugin.getDetectorFactories()){
                if (df.getReportedBugPatterns().contains(pattern)) {
                    if (PLUGIN_DEBUG) java.lang.System.out.println("Bug rank match " + plugin + " " + df + " for " + pattern);
                    rankers.add(plugin.getBugRanker());
                    continue pluginLoop;
                }
            }
;
            if (PLUGIN_DEBUG) java.lang.System.out.println("plugin " + plugin + " doesn't match " + pattern);
        }
;
        rankers.add(getCoreRanker());
        return rankBugPattern(pattern,rankers.toArray(new edu.umd.cs.findbugs.BugRanker[]{}));
    }
    public static void trimToMaxRank(edu.umd.cs.findbugs.BugCollection origCollection, int maxRank) {
        for (java.util.Iterator<edu.umd.cs.findbugs.BugInstance> i = origCollection.getCollection().iterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.BugInstance b = i.next();
            if (edu.umd.cs.findbugs.BugRanker.findRank(b) > maxRank) i.remove();
        }
    }
}
