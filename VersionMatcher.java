/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.filter;
import edu.umd.cs.findbugs.filter.*;
import javax.annotation.Nonnull;
public class VersionMatcher extends java.lang.Object {
    final protected long version;
    final protected edu.umd.cs.findbugs.filter.RelationalOp relOp;
    public int hashCode() {
        return (int) (version)  + relOp.hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if (o == null || this.getClass() != o.getClass()) return false;
        edu.umd.cs.findbugs.filter.VersionMatcher m = (edu.umd.cs.findbugs.filter.VersionMatcher) (o) ;
        return version == m.version && relOp.equals(m.relOp);
    }
    public VersionMatcher(long version, edu.umd.cs.findbugs.filter.RelationalOp relOp) {
        super();
        if (relOp == null) throw new java.lang.NullPointerException("relOp must be nonnull");
        this.version = version;
        this.relOp = relOp;
    }
}
