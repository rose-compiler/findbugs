/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004-2006 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.Iterator;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.LocalVariableTable;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.TrainingDetector;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.SignatureParser;
import edu.umd.cs.findbugs.ba.interproc.MethodPropertyDatabase;
import edu.umd.cs.findbugs.ba.interproc.ParameterProperty;
import edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
public class TrainLongInstantfParams extends edu.umd.cs.findbugs.visitclass.PreorderVisitor implements edu.umd.cs.findbugs.Detector, edu.umd.cs.findbugs.TrainingDetector {
    static class LongInstantParameterDatabase extends edu.umd.cs.findbugs.ba.interproc.MethodPropertyDatabase<edu.umd.cs.findbugs.ba.interproc.ParameterProperty> {
        public LongInstantParameterDatabase() {
        }
        protected edu.umd.cs.findbugs.ba.interproc.ParameterProperty decodeProperty(java.lang.String propStr) throws edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException {
            try {
                int longInstants = java.lang.Integer.parseInt(propStr);
                edu.umd.cs.findbugs.ba.interproc.ParameterProperty prop = new edu.umd.cs.findbugs.ba.interproc.ParameterProperty(longInstants);
                return prop;
            }
            catch (java.lang.NumberFormatException e){
                throw new edu.umd.cs.findbugs.ba.interproc.PropertyDatabaseFormatException("Invalid unconditional deref param set: " + propStr);
            }
        }
/*
         * (non-Javadoc)
         * 
         * @see
         * edu.umd.cs.findbugs.ba.interproc.MethodPropertyDatabase#encodeProperty
         * (Property)
         */
        protected java.lang.String encodeProperty(edu.umd.cs.findbugs.ba.interproc.ParameterProperty property) {
            return java.lang.String.valueOf(property.getParamsWithProperty());
        }
    }
    edu.umd.cs.findbugs.detect.TrainLongInstantfParams.LongInstantParameterDatabase database = new edu.umd.cs.findbugs.detect.TrainLongInstantfParams.LongInstantParameterDatabase();
    public TrainLongInstantfParams(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
    }
    public void visit(org.apache.bcel.classfile.Code obj) {
        if ( !this.getMethod().isPublic() &&  !this.getMethod().isProtected()) return;
        edu.umd.cs.findbugs.ba.SignatureParser p = new edu.umd.cs.findbugs.ba.SignatureParser(this.getMethodSig());
        org.apache.bcel.classfile.LocalVariableTable t = obj.getLocalVariableTable();
        if (t == null) return;
        edu.umd.cs.findbugs.ba.interproc.ParameterProperty property = new edu.umd.cs.findbugs.ba.interproc.ParameterProperty();
        int index = this.getMethod().isStatic() ? 0 : 1;
        int parameterNumber = 0;
        for (java.util.Iterator<java.lang.String> i = p.parameterSignatureIterator(); i.hasNext(); ) {
            java.lang.String s = i.next();
            org.apache.bcel.classfile.LocalVariable localVariable = t.getLocalVariable(index,0);
            if (localVariable != null) {
                java.lang.String name = localVariable.getName();
                if (s.equals("J") && (name.toLowerCase().indexOf("instant") >= 0 || name.startsWith("date"))) {
                    property.setParamWithProperty(parameterNumber,true);
                }
            }
            if (s.equals("J") || s.equals("D")) index += 2;
            else index += 1;
            parameterNumber++;
        }
// System.out.println(getFullyQualifiedMethodName() + " " + s + " " + index + " " + name);
        if ( !property.isEmpty()) {
            database.setProperty(this.getMethodDescriptor(),property);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.Detector#report()
     */
    public void report() {
        edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().storePropertyDatabase(database,"longInstant.db","long instant database");
    }
// System.out.println(database.entrySet().size() + " methods");
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        classContext.getJavaClass().accept(this);
    }
}
