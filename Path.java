/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004,2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A Path is a sequence of basic blocks.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.Iterator;
import org.apache.bcel.generic.InstructionHandle;
public class Path extends java.lang.Object {
    final private static int DEFAULT_CAPACITY = 8;
    final private static int INVALID_HASH_CODE =  -1;
    private int[] blockIdList;
    private int length;
    private int cachedHashCode;
/**
     * Constructor. Creates an empty Path.
     */
    public Path() {
        super();
        this.blockIdList = new int[DEFAULT_CAPACITY];
        this.length = 0;
        this.invalidate();
    }
/**
     * Append given BasicBlock id to the path.
     * 
     * @param id
     *            a BasicBlock id (label)
     */
    public void append(int id) {
        this.grow(length);
        blockIdList[length] = id;
        ++length;
        this.invalidate();
    }
/**
     * Determine whether or not the id of the given BasicBlock appears anywhere
     * in the path.
     * 
     * @param blockId
     *            the id (label) of a BasicBlock
     * @return true if the BasicBlock's id appears in the path, false if not
     */
    public boolean hasComponent(int blockId) {
        for (int i = 0; i < length; i++) {
            if (blockIdList[i] == blockId) {
                return true;
            }
        }
        return false;
    }
/**
     * Get the BasicBlock id at the given index in the path.
     * 
     * @param index
     *            an index in the Path (0 is the first component)
     * @return the id of the BasicBlock at the given index
     */
    public int getBlockIdAt(int index) {
        assert index < length;
        return blockIdList[index];
    }
/**
     * Get the number of components (BasicBlock ids) in the Path.
     * 
     * @return number of components in the Path
     */
    public int getLength() {
        return length;
    }
/**
     * Return an exact copy of this Path.
     * 
     * @return an exact copy of this Path
     */
    public edu.umd.cs.findbugs.ba.Path duplicate() {
        edu.umd.cs.findbugs.ba.Path dup = new edu.umd.cs.findbugs.ba.Path();
        dup.copyFrom(this);
        return dup;
    }
/**
     * Make this Path identical to the given one.
     * 
     * @param other
     *            a Path to which this object should be made identical
     */
    public void copyFrom(edu.umd.cs.findbugs.ba.Path other) {
        this.grow(other.length - 1);
        java.lang.System.arraycopy(other.blockIdList,0,this.blockIdList,0,other.length);
        this.length = other.length;
        this.cachedHashCode = other.cachedHashCode;
    }
/**
     * Accept a PathVisitor.
     * 
     * @param cfg
     *            the control flow graph
     * @param visitor
     *            a PathVisitor
     */
    public void acceptVisitor(edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.PathVisitor visitor) {
        if (this.getLength() > 0) {
            edu.umd.cs.findbugs.ba.BasicBlock startBlock = cfg.lookupBlockByLabel(this.getBlockIdAt(0));
            this.acceptVisitorStartingFromLocation(cfg,visitor,startBlock,startBlock.getFirstInstruction());
        }
    }
/**
     * Accept a PathVisitor, starting from a given BasicBlock and
     * InstructionHandle.
     * 
     * @param cfg
     *            the control flow graph
     * @param visitor
     *            a PathVisitor
     * @param startBlock
     *            BasicBlock where traversal should start
     * @param startHandle
     *            InstructionHandle within the start block where traversal
     *            should start
     */
    public void acceptVisitorStartingFromLocation(edu.umd.cs.findbugs.ba.CFG cfg, edu.umd.cs.findbugs.ba.PathVisitor visitor, edu.umd.cs.findbugs.ba.BasicBlock startBlock, org.apache.bcel.generic.InstructionHandle startHandle) {
// Find the start block in the path
        int index;
        for (index = 0; index < this.getLength(); index++) {
            if (this.getBlockIdAt(index) == startBlock.getLabel()) {
                break;
            }
        }
        assert index < this.getLength();
        java.util.Iterator<org.apache.bcel.generic.InstructionHandle> i = startBlock.instructionIterator();
// Position iterator at start instruction handle
        if (startHandle != startBlock.getFirstInstruction()) {
            while (i.hasNext()) {
                org.apache.bcel.generic.InstructionHandle handle = i.next();
                if (handle.getNext() == startHandle) {
                    break;
                }
            }
        }
        edu.umd.cs.findbugs.ba.BasicBlock basicBlock = startBlock;
        while (true) {
            visitor.visitBasicBlock(basicBlock);
// visit block
// visit instructions in block
            while (i.hasNext()) {
                visitor.visitInstructionHandle(i.next());
            }
            index++;
// end of path?
            if (index >= this.getLength()) {
                break;
            }
// visit edge
            edu.umd.cs.findbugs.ba.BasicBlock next = cfg.lookupBlockByLabel(this.getBlockIdAt(index));
            edu.umd.cs.findbugs.ba.Edge edge = cfg.lookupEdge(basicBlock,next);
            assert edge != null;
            visitor.visitEdge(edge);
            basicBlock = next;
            i = basicBlock.instructionIterator();
        }
    }
/**
     * Determine whether or not given Path is a prefix of this one.
     * 
     * @param path
     *            another Path
     * @return true if this Path is a prefix of the other Path, false otherwise
     */
    public boolean isPrefixOf(edu.umd.cs.findbugs.ba.Path path) {
        if (this.getLength() > path.getLength()) {
            return false;
        }
        for (int i = 0; i < this.getLength(); i++) {
            if (this.getBlockIdAt(i) != path.getBlockIdAt(i)) {
                return false;
            }
        }
        return true;
    }
    private void invalidate() {
        this.cachedHashCode = INVALID_HASH_CODE;
    }
    public int hashCode() {
        if (cachedHashCode == INVALID_HASH_CODE) {
            int value = 0;
            for (int i = 0; i < this.length; ++i) {
                value += (i * 1009 * blockIdList[i]);
            }
            cachedHashCode = value;
        }
        return cachedHashCode;
    }
    public boolean equals(java.lang.Object o) {
        if (o == null || o.getClass() != this.getClass()) return false;
        edu.umd.cs.findbugs.ba.Path other = (edu.umd.cs.findbugs.ba.Path) (o) ;
        if (this.length != other.length) return false;
        for (int i = 0; i < this.length; ++i) {
            if (this.blockIdList[i] != other.blockIdList[i]) return false;
        }
        return true;
    }
    final private static java.lang.String SYMBOLS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()";
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        for (int i = 0; i < length; ++i) {
            int block = blockIdList[i];
            if (block < SYMBOLS.length()) buf.append(SYMBOLS.charAt(block));
            else buf.append("'" + block + "'");
        }
        return buf.toString();
    }
    private void grow(int index) {
        if (index >= blockIdList.length) {
            int newLen = blockIdList.length;
            do {
                newLen *= 2;
            }
            while (index >= newLen);
            int[] arr = new int[newLen];
            java.lang.System.arraycopy(this.blockIdList,0,arr,0,length);
            this.blockIdList = arr;
        }
    }
}
// vim:ts=4
