/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Search for bug instances whose text annotations contain one of a set of
 * keywords.
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
abstract public class QueryBugAnnotations extends java.lang.Object {
    public QueryBugAnnotations() {
    }
// Bug's text annotation must contain one of the key
// words in order to match
    private java.util.HashSet<java.lang.String> keywordSet = new java.util.HashSet<java.lang.String>();
/**
     * Add a keyword to the query. A BugInstance's text annotation must contain
     * at least one keyword in order to match the query.
     * 
     * @param keyword
     *            the keyword
     */
    public void addKeyword(java.lang.String keyword) {
        keywordSet.add(keyword);
    }
/**
     * Scan bug instances contained in given file, reporting those whose text
     * annotations contain at least one of the keywords in the query.
     * 
     * @param filename
     *            an XML file containing bug instances
     */
    public void scan(java.lang.String filename) throws java.lang.Exception {
        edu.umd.cs.findbugs.BugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        bugCollection.readXML(filename);
        this.scan(bugCollection,filename);
    }
/**
     * Scan bug instances contained in given bug collection, reporting those
     * whose text annotations contain at least one of the keywords in the query.
     * 
     * @param bugCollection
     *            the bug collection
     * @param filename
     *            the XML file from which the bug collection was read
     */
    public void scan(edu.umd.cs.findbugs.BugCollection bugCollection, java.lang.String filename) throws java.lang.Exception {
        java.util.Iterator<edu.umd.cs.findbugs.BugInstance> i = bugCollection.iterator();
        while (i.hasNext()) {
            edu.umd.cs.findbugs.BugInstance bugInstance = i.next();
            java.util.Set<java.lang.String> contents = bugInstance.getTextAnnotationWords();
            for (java.lang.String aKeywordSet : keywordSet){
                if (contents.contains(aKeywordSet)) {
                    this.match(bugInstance,filename);
                    break;
                }
            }
;
        }
    }
/**
     * Called when a bug instance contains a query keyword.
     * 
     * @param bugInstance
     *            the bug instance containing the keyword
     * @param filename
     *            name of the file containing the bug instance
     */
    abstract protected void match(edu.umd.cs.findbugs.BugInstance bugInstance, java.lang.String filename) throws java.lang.Exception;
}
// vim:ts=4
