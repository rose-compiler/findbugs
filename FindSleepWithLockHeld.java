/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Find calls to Thread.sleep() made with a lock held.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.BitSet;
import java.util.Iterator;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.Instruction;
import edu.umd.cs.findbugs.BugAccumulator;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.LockDataflow;
import edu.umd.cs.findbugs.ba.LockSet;
public class FindSleepWithLockHeld extends java.lang.Object implements edu.umd.cs.findbugs.Detector {
    final private edu.umd.cs.findbugs.BugReporter bugReporter;
    final private edu.umd.cs.findbugs.BugAccumulator bugAccumulator;
    public FindSleepWithLockHeld(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
        this.bugAccumulator = new edu.umd.cs.findbugs.BugAccumulator(bugReporter);
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.JavaClass javaClass = classContext.getJavaClass();
        org.apache.bcel.classfile.Method[] methodList = javaClass.getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
            if ( !this.prescreen(classContext,method)) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("FindSleepWithLockHeld caught exception",e);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("FindSleepWithLockHeld caught exception",e);
            }
        }
;
    }
    private boolean prescreen(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) {
        java.util.BitSet bytecodeSet = classContext.getBytecodeSet(method);
        if (bytecodeSet == null) return false;
// method must acquire a lock
        if ( !bytecodeSet.get(org.apache.bcel.Constants.MONITORENTER) &&  !method.isSynchronized()) return false;
// and contain a static method invocation
        if ( !bytecodeSet.get(org.apache.bcel.Constants.INVOKESTATIC)) return false;
        return true;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
// System.out.println("Checking " + method);
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.LockDataflow lockDataflow = classContext.getLockDataflow(method);
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.INVOKESTATIC)) continue;
            if ( !this.isSleep((org.apache.bcel.generic.INVOKESTATIC) (ins) ,classContext.getConstantPoolGen())) continue;
            edu.umd.cs.findbugs.ba.LockSet lockSet = lockDataflow.getFactAtLocation(location);
            if (lockSet.getNumLockedObjects() > 0) {
                bugAccumulator.accumulateBug(new edu.umd.cs.findbugs.BugInstance(this, "SWL_SLEEP_WITH_LOCK_HELD", NORMAL_PRIORITY).addClassAndMethod(classContext.getJavaClass(),method),classContext,method,location);
            }
        }
        bugAccumulator.reportAccumulatedBugs();
    }
// System.out.println("Found sleep at " + location.getHandle());
    private boolean isSleep(org.apache.bcel.generic.INVOKESTATIC ins, org.apache.bcel.generic.ConstantPoolGen cpg) {
        java.lang.String className = ins.getClassName(cpg);
        if ( !className.equals("java.lang.Thread")) return false;
        java.lang.String methodName = ins.getMethodName(cpg);
        java.lang.String signature = ins.getSignature(cpg);
        return methodName.equals("sleep") && (signature.equals("(J)V") || signature.equals("(JI)V"));
    }
    public void report() {
    }
}
