/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Build a database of reference types stored into fields. This can be used in
 * the future to improve the precision of type analysis when values are loaded
 * from fields.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.Iterator;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FieldInstruction;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.TrainingDetector;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.XField;
import edu.umd.cs.findbugs.ba.type.FieldStoreType;
import edu.umd.cs.findbugs.ba.type.FieldStoreTypeDatabase;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.type.TypeFrame;
public class TrainFieldStoreTypes extends java.lang.Object implements edu.umd.cs.findbugs.Detector, edu.umd.cs.findbugs.TrainingDetector {
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    private edu.umd.cs.findbugs.ba.type.FieldStoreTypeDatabase database;
    public TrainFieldStoreTypes(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
        this.database = new edu.umd.cs.findbugs.ba.type.FieldStoreTypeDatabase();
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.Method[] methodList = classContext.getJavaClass().getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("Error compting field store types",e);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("Error compting field store types",e);
            }
        }
;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = classContext.getTypeDataflow(method);
        org.apache.bcel.generic.ConstantPoolGen cpg = classContext.getConstantPoolGen();
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
            short opcode = ins.getOpcode();
            if (opcode != org.apache.bcel.Constants.PUTFIELD && opcode != org.apache.bcel.Constants.PUTSTATIC) continue;
            org.apache.bcel.generic.FieldInstruction fins = (org.apache.bcel.generic.FieldInstruction) (ins) ;
            org.apache.bcel.generic.Type fieldType = fins.getType(cpg);
            if ( !(fieldType instanceof org.apache.bcel.generic.ReferenceType)) continue;
            edu.umd.cs.findbugs.ba.XField xfield = edu.umd.cs.findbugs.ba.Hierarchy.findXField(fins,cpg);
            if (xfield == null) continue;
            if (xfield.isPublic() || xfield.isProtected()) continue;
            edu.umd.cs.findbugs.ba.type.TypeFrame frame = typeDataflow.getFactAtLocation(location);
            if ( !frame.isValid()) continue;
            org.apache.bcel.generic.Type storeType = frame.getTopValue();
            if ( !(storeType instanceof org.apache.bcel.generic.ReferenceType)) continue;
            edu.umd.cs.findbugs.ba.type.FieldStoreType property = database.getProperty(xfield.getFieldDescriptor());
            if (property == null) {
                property = new edu.umd.cs.findbugs.ba.type.FieldStoreType();
                database.setProperty(xfield.getFieldDescriptor(),property);
            }
            property.addTypeSignature(storeType.getSignature());
        }
    }
// Field store instruction?
// Check if field type is a reference type
// Find the exact field being stored into
// Skip public and protected fields, since it is reasonable to
// assume
// we won't see every store to those fields
// The top value on the stack is the one which will be stored
// into the field
// Get or create the field store type set
// Add the store type to the set
    public void report() {
        database.purgeBoringEntries();
        edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().storePropertyDatabase(database,edu.umd.cs.findbugs.ba.type.FieldStoreTypeDatabase.DEFAULT_FILENAME,"store type database");
    }
}
