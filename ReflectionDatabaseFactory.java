/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A generic database factory that tries to create the database by (in order of
 * preference)
 * 
 * <ol>
 * <li>Invoking a static <b>create</b> method</li>
 * <li>Invoking a no-arg constructor
 * </ol>
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile;
import edu.umd.cs.findbugs.classfile.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
public class ReflectionDatabaseFactory<E> extends java.lang.Object implements edu.umd.cs.findbugs.classfile.IDatabaseFactory<E> {
    private java.lang.Class<E> databaseClass;
    public ReflectionDatabaseFactory(java.lang.Class<E> databaseClass) {
        super();
        this.databaseClass = databaseClass;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.IDatabaseFactory#createDatabase()
     */
    public E createDatabase() throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        E database;
        database = this.createUsingStaticCreateMethod();
        if (database != null) {
            return database;
        }
        database = this.createUsingConstructor();
        if (database != null) {
            return database;
        }
        throw new edu.umd.cs.findbugs.classfile.CheckedAnalysisException("Could not find a way to create database " + databaseClass.getName());
    }
/**
     * Try to create the database using a static create() method.
     * 
     * @return the database, or null if there is no static create() method
     * @throws CheckedAnalysisException
     */
    private E createUsingStaticCreateMethod() throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        java.lang.reflect.Method createMethod;
        try {
            createMethod = databaseClass.getMethod("create",new java.lang.Class[0]);
        }
        catch (java.lang.NoSuchMethodException e){
            return null;
        }
        if ( !java.lang.reflect.Modifier.isStatic(createMethod.getModifiers())) {
            return null;
        }
        if (createMethod.getReturnType() != databaseClass) {
            return null;
        }
        try {
            return databaseClass.cast(createMethod.invoke(null,new java.lang.Object[0]));
        }
        catch (java.lang.reflect.InvocationTargetException e){
            throw new edu.umd.cs.findbugs.classfile.CheckedAnalysisException("Could not create " + databaseClass.getName(), e);
        }
        catch (java.lang.IllegalAccessException e){
            throw new edu.umd.cs.findbugs.classfile.CheckedAnalysisException("Could not create " + databaseClass.getName(), e);
        }
    }
/**
     * Try to create the database using a no-arg constructor.
     * 
     * @return the database, or null if there is no no-arg constructor
     * @throws CheckedAnalysisException
     */
    private E createUsingConstructor() throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        java.lang.reflect.Constructor<E> constructor;
        try {
            constructor = databaseClass.getConstructor(new java.lang.Class[0]);
        }
        catch (java.lang.NoSuchMethodException e){
            return null;
        }
        try {
            return constructor.newInstance(new java.lang.Object[0]);
        }
        catch (java.lang.InstantiationException e){
            throw new edu.umd.cs.findbugs.classfile.CheckedAnalysisException("Could not create " + databaseClass.getName(), e);
        }
        catch (java.lang.IllegalAccessException e){
            throw new edu.umd.cs.findbugs.classfile.CheckedAnalysisException("Could not create " + databaseClass.getName(), e);
        }
        catch (java.lang.reflect.InvocationTargetException e){
            throw new edu.umd.cs.findbugs.classfile.CheckedAnalysisException("Could not create " + databaseClass.getName(), e);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IDatabaseFactory#registerWith(edu.umd.cs
     * .findbugs.classfile.IAnalysisCache)
     */
    public void registerWith(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache) {
        analysisCache.registerDatabaseFactory(databaseClass,this);
    }
}
