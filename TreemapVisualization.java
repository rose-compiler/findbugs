/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.PackageStats;
import edu.umd.cs.findbugs.Priorities;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.util.Bag;
public class TreemapVisualization extends java.lang.Object {
    public TreemapVisualization() {
    }
    static java.util.HashSet<java.lang.String> buggyPackages = new java.util.HashSet<java.lang.String>();
    static java.util.HashSet<java.lang.String> interiorPackages = new java.util.HashSet<java.lang.String>();
    static edu.umd.cs.findbugs.util.Bag<java.lang.String> goodCodeSize = new edu.umd.cs.findbugs.util.Bag<java.lang.String>(new java.util.TreeMap<java.lang.String, java.lang.Integer>());
    static edu.umd.cs.findbugs.util.Bag<java.lang.String> goodCodeCount = new edu.umd.cs.findbugs.util.Bag<java.lang.String>(new java.util.TreeMap<java.lang.String, java.lang.Integer>());
    public static void addInteriorPackages(java.lang.String packageName) {
        java.lang.String p = superpackage(packageName);
        if (p.length() > 0) {
            interiorPackages.add(p);
            addInteriorPackages(p);
        }
    }
/**
     * @param packageName
     * @return
     */
    private static java.lang.String superpackage(java.lang.String packageName) {
        int i = packageName.lastIndexOf('\u002e');
        if (i ==  -1) return "";
        java.lang.String p = packageName.substring(0,i);
        return p;
    }
    public static boolean isInteriorPackage(java.lang.String packageName) {
        return interiorPackages.contains(packageName);
    }
    public static void cleanCode(java.lang.String packageName, int loc, int classes) {
        java.lang.String superpackage = superpackage(packageName);
        if (buggyPackages.contains(superpackage) || interiorPackages.contains(superpackage) || superpackage.length() == 0) {
            goodCodeCount.add(packageName,classes);
            goodCodeSize.add(packageName,loc);
            if (superpackage.length() > 0) interiorPackages.add(superpackage);
        }
        else cleanCode(superpackage,loc,classes);
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
// load plugins
        edu.umd.cs.findbugs.SortedBugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        int argCount = 0;
        if (argCount < args.length) bugCollection.readXML(args[argCount++]);
        else bugCollection.readXML(java.lang.System.in);
        for (edu.umd.cs.findbugs.PackageStats p : bugCollection.getProjectStats().getPackageStats())if (p.getTotalBugs() > 0) {
            buggyPackages.add(p.getPackageName());
            addInteriorPackages(p.getPackageName());
        }
;
        for (edu.umd.cs.findbugs.PackageStats p : bugCollection.getProjectStats().getPackageStats())if (p.getTotalBugs() == 0) {
            cleanCode(p.getPackageName(),p.size(),p.getClassStats().size());
        }
;
        java.lang.System.out.println("LOC	Types	H	HM	Density");
        java.lang.System.out.println("INTEGER	INTEGER	INTEGER	INTEGER	FLOAT");
        for (edu.umd.cs.findbugs.PackageStats p : bugCollection.getProjectStats().getPackageStats())if (p.getTotalBugs() > 0) {
            int high = p.getBugsAtPriority(edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY);
            int normal = p.getBugsAtPriority(edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY);
            java.lang.System.out.printf("%d	%d	%d	%d	%g		%s",p.size(),p.getClassStats().size(),high,high + normal,(high + normal) * 1000.0 / p.size(),p.getPackageName().substring(11).replace('\u002e','\t'));
            if (isInteriorPackage(p.getPackageName())) java.lang.System.out.print("	*");
            java.lang.System.out.println();
        }
;
        for (java.util.Map.Entry<java.lang.String, java.lang.Integer> e : goodCodeSize.entrySet()){
            java.lang.System.out.printf("%d	%d	%d	%d	%g		%s%n",e.getValue(),goodCodeCount.getCount(e.getKey()),0,0,0.0,e.getKey().substring(11).replace('\u002e','\t'));
        }
;
    }
}
