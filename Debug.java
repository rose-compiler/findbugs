/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * For debugging purposes only... Make sure DEBUG is set to false before you
 * release a new version.
 * 
 * @author Dan
 * 
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
public class Debug extends java.lang.Object {
    public Debug() {
    }
    public static void println(java.lang.Object s) {
        if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.out.println(s);
    }
    public static void printf(java.lang.String format, java.lang.Object[] args) {
        if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.out.printf(format,args);
    }
    public static void println(java.lang.Exception e) {
        if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) e.printStackTrace();
    }
    public static void main(java.lang.String[] args) {
    }
}
