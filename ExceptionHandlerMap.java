/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * This class provides a convenient way of determining the exception handlers
 * for instructions in a method. Essentially, it's a a map of InstructionHandles
 * to lists of CodeExceptionGen objects. This class also maps instructions which
 * are the start of exception handlers to the CodeExceptionGen object
 * representing the handler.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.bcel.generic.CodeExceptionGen;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.MethodGen;
public class ExceptionHandlerMap extends java.lang.Object {
    private java.util.IdentityHashMap<org.apache.bcel.generic.InstructionHandle, java.util.List<org.apache.bcel.generic.CodeExceptionGen>> codeToHandlerMap;
    private java.util.IdentityHashMap<org.apache.bcel.generic.InstructionHandle, org.apache.bcel.generic.CodeExceptionGen> startInstructionToHandlerMap;
/**
     * Constructor.
     * 
     * @param methodGen
     *            the method to build the map for
     */
    public ExceptionHandlerMap(org.apache.bcel.generic.MethodGen methodGen) {
        super();
        codeToHandlerMap = new java.util.IdentityHashMap<org.apache.bcel.generic.InstructionHandle, java.util.List<org.apache.bcel.generic.CodeExceptionGen>>();
        startInstructionToHandlerMap = new java.util.IdentityHashMap<org.apache.bcel.generic.InstructionHandle, org.apache.bcel.generic.CodeExceptionGen>();
        this.build(methodGen);
    }
/**
     * Get the list of exception handlers (CodeExceptionGen objects) which are
     * specified to handle exceptions for the instruction whose handle is given.
     * Note that the handlers in the returned list are <b>in order of
     * priority</b>, as defined in the method's exception handler table.
     * 
     * @param handle
     *            the handle of the instruction we want the exception handlers
     *            for
     * @return the list of exception handlers, or null if there are no handlers
     *         registered for the instruction
     */
    public java.util.List<org.apache.bcel.generic.CodeExceptionGen> getHandlerList(org.apache.bcel.generic.InstructionHandle handle) {
        return codeToHandlerMap.get(handle);
    }
/**
     * If the given instruction is the start of an exception handler, get the
     * CodeExceptionGen object representing the handler.
     * 
     * @param start
     *            the instruction
     * @return the CodeExceptionGen object, or null if the instruction is not
     *         the start of an exception handler
     */
    public org.apache.bcel.generic.CodeExceptionGen getHandlerForStartInstruction(org.apache.bcel.generic.InstructionHandle start) {
        return startInstructionToHandlerMap.get(start);
    }
    private void build(org.apache.bcel.generic.MethodGen methodGen) {
        org.apache.bcel.generic.CodeExceptionGen[] handlerList = methodGen.getExceptionHandlers();
// Map handler start instructions to the actual exception handlers
        for (org.apache.bcel.generic.CodeExceptionGen exceptionHandler : handlerList){
            startInstructionToHandlerMap.put(exceptionHandler.getHandlerPC(),exceptionHandler);
        }
;
// For each instruction, determine which handlers it can reach
        org.apache.bcel.generic.InstructionHandle handle = methodGen.getInstructionList().getStart();
        while (handle != null) {
            int offset = handle.getPosition();
            handlerLoop:for (org.apache.bcel.generic.CodeExceptionGen exceptionHandler : handlerList){
                int startOfRange = exceptionHandler.getStartPC().getPosition();
                int endOfRange = exceptionHandler.getEndPC().getPosition();
                if (offset >= startOfRange && offset <= endOfRange) {
                    this.addHandler(handle,exceptionHandler);
// This handler is reachable from the instruction
// If this handler handles all exception types
// i.e., an ANY handler, or catch(Throwable...),
// then no further (lower-priority)
// handlers are reachable from the instruction.
                    if (edu.umd.cs.findbugs.ba.Hierarchy.isUniversalExceptionHandler(exceptionHandler.getCatchType())) break handlerLoop;
                }
            }
;
            handle = handle.getNext();
        }
    }
    private void addHandler(org.apache.bcel.generic.InstructionHandle handle, org.apache.bcel.generic.CodeExceptionGen exceptionHandler) {
        java.util.List<org.apache.bcel.generic.CodeExceptionGen> handlerList = codeToHandlerMap.get(handle);
        if (handlerList == null) {
            handlerList = new java.util.LinkedList<org.apache.bcel.generic.CodeExceptionGen>();
            codeToHandlerMap.put(handle,handlerList);
        }
        handlerList.add(exceptionHandler);
    }
}
// vim:ts=4
