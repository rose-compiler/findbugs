/**
 * A 1.4 file that provides utility methods for creating form- or grid-style
 * layouts with SpringLayout. These utilities are used by several programs, such
 * as SpringBox and SpringCompactGrid.
 * 
 * From the Swing tutorial.
 * 
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Component;
import java.awt.Container;
import javax.swing.Spring;
import javax.swing.SpringLayout;
public class SpringUtilities extends java.lang.Object {
    public SpringUtilities() {
    }
/**
     * A debugging utility that prints to stdout the component's minimum,
     * preferred, and maximum sizes.
     */
    public static void printSizes(java.awt.Component c) {
        if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) {
            java.lang.System.out.println("minimumSize = " + c.getMinimumSize());
            java.lang.System.out.println("preferredSize = " + c.getPreferredSize());
            java.lang.System.out.println("maximumSize = " + c.getMaximumSize());
        }
    }
/**
     * Aligns the first <code>rows</code> * <code>cols</code> components of
     * <code>parent</code> in a grid. Each component is as big as the maximum
     * preferred width and height of the components. The parent is made just big
     * enough to fit them all.
     * 
     * @param rows
     *            number of rows
     * @param cols
     *            number of columns
     * @param initialX
     *            x location to start the grid at
     * @param initialY
     *            y location to start the grid at
     * @param xPad
     *            x padding between cells
     * @param yPad
     *            y padding between cells
     */
    public static void makeGrid(java.awt.Container parent, int rows, int cols, int initialX, int initialY, int xPad, int yPad) {
        javax.swing.SpringLayout layout;
        try {
            layout = (javax.swing.SpringLayout) (parent.getLayout()) ;
        }
        catch (java.lang.ClassCastException exc){
            if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.err.println("The first argument to makeGrid must use SpringLayout.");
            return;
        }
        javax.swing.Spring xPadSpring = javax.swing.Spring.constant(xPad);
        javax.swing.Spring yPadSpring = javax.swing.Spring.constant(yPad);
        javax.swing.Spring initialXSpring = javax.swing.Spring.constant(initialX);
        javax.swing.Spring initialYSpring = javax.swing.Spring.constant(initialY);
        int max = rows * cols;
// Calculate Springs that are the max of the width/height so that all
// cells have the same size.
        javax.swing.Spring maxWidthSpring = layout.getConstraints(parent.getComponent(0)).getWidth();
        javax.swing.Spring maxHeightSpring = layout.getConstraints(parent.getComponent(0)).getWidth();
        for (int i = 1; i < max; i++) {
            javax.swing.SpringLayout.Constraints cons = layout.getConstraints(parent.getComponent(i));
            maxWidthSpring = javax.swing.Spring.max(maxWidthSpring,cons.getWidth());
            maxHeightSpring = javax.swing.Spring.max(maxHeightSpring,cons.getHeight());
        }
        for (int i = 0; i < max; i++) {
            javax.swing.SpringLayout.Constraints cons = layout.getConstraints(parent.getComponent(i));
            cons.setWidth(maxWidthSpring);
            cons.setHeight(maxHeightSpring);
        }
// Apply the new width/height Spring. This forces all the
// components to have the same size.
// Then adjust the x/y constraints of all the cells so that they
// are aligned in a grid.
        javax.swing.SpringLayout.Constraints lastCons = null;
        javax.swing.SpringLayout.Constraints lastRowCons = null;
        for (int i = 0; i < max; i++) {
            javax.swing.SpringLayout.Constraints cons = layout.getConstraints(parent.getComponent(i));
            if (i % cols == 0) {
                lastRowCons = lastCons;
                cons.setX(initialXSpring);
            }
            else {
                assert lastCons != null;
                cons.setX(javax.swing.Spring.sum(lastCons.getConstraint(javax.swing.SpringLayout.EAST),xPadSpring));
            }
            if (i / cols == 0) {
                cons.setY(initialYSpring);
            }
            else {
                assert lastRowCons != null;
                cons.setY(javax.swing.Spring.sum(lastRowCons.getConstraint(javax.swing.SpringLayout.SOUTH),yPadSpring));
            }
            lastCons = cons;
        }
// start of new row
// x position depends on previous component
// first row
// y position depends on previous row
        assert lastCons != null;
// Set the parent's size.
        javax.swing.SpringLayout.Constraints pCons = layout.getConstraints(parent);
        pCons.setConstraint(javax.swing.SpringLayout.SOUTH,javax.swing.Spring.sum(javax.swing.Spring.constant(yPad),lastCons.getConstraint(javax.swing.SpringLayout.SOUTH)));
        pCons.setConstraint(javax.swing.SpringLayout.EAST,javax.swing.Spring.sum(javax.swing.Spring.constant(xPad),lastCons.getConstraint(javax.swing.SpringLayout.EAST)));
    }
/* Used by makeCompactGrid. */
    private static javax.swing.SpringLayout.Constraints getConstraintsForCell(int row, int col, java.awt.Container parent, int cols) {
        javax.swing.SpringLayout layout = (javax.swing.SpringLayout) (parent.getLayout()) ;
        java.awt.Component c = parent.getComponent(row * cols + col);
        return layout.getConstraints(c);
    }
/**
     * Aligns the first <code>rows</code> * <code>cols</code> components of
     * <code>parent</code> in a grid. Each component in a column is as wide as
     * the maximum preferred width of the components in that column; height is
     * similarly determined for each row. The parent is made just big enough to
     * fit them all.
     * 
     * @param rows
     *            number of rows
     * @param cols
     *            number of columns
     * @param initialX
     *            x location to start the grid at
     * @param initialY
     *            y location to start the grid at
     * @param xPad
     *            x padding between cells
     * @param yPad
     *            y padding between cells
     */
    public static void makeCompactGrid(java.awt.Container parent, int rows, int cols, int initialX, int initialY, int xPad, int yPad) {
        javax.swing.SpringLayout layout;
        try {
            layout = (javax.swing.SpringLayout) (parent.getLayout()) ;
        }
        catch (java.lang.ClassCastException exc){
            if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.err.println("The first argument to makeCompactGrid must use SpringLayout.");
            return;
        }
// Align all cells in each column and make them the same width.
        javax.swing.Spring x = javax.swing.Spring.constant(initialX);
        for (int c = 0; c < cols; c++) {
            javax.swing.Spring width = javax.swing.Spring.constant(0);
            for (int r = 0; r < rows; r++) {
                width = javax.swing.Spring.max(width,getConstraintsForCell(r,c,parent,cols).getWidth());
            }
            for (int r = 0; r < rows; r++) {
                javax.swing.SpringLayout.Constraints constraints = getConstraintsForCell(r,c,parent,cols);
                constraints.setX(x);
                constraints.setWidth(width);
            }
            x = javax.swing.Spring.sum(x,javax.swing.Spring.sum(width,javax.swing.Spring.constant(xPad)));
        }
// Align all cells in each row and make them the same height.
        javax.swing.Spring y = javax.swing.Spring.constant(initialY);
        for (int r = 0; r < rows; r++) {
            javax.swing.Spring height = javax.swing.Spring.constant(0);
            for (int c = 0; c < cols; c++) {
                height = javax.swing.Spring.max(height,getConstraintsForCell(r,c,parent,cols).getHeight());
            }
            for (int c = 0; c < cols; c++) {
                javax.swing.SpringLayout.Constraints constraints = getConstraintsForCell(r,c,parent,cols);
                constraints.setY(y);
                constraints.setHeight(height);
            }
            y = javax.swing.Spring.sum(y,javax.swing.Spring.sum(height,javax.swing.Spring.constant(yPad)));
        }
// Set the parent's size.
        javax.swing.SpringLayout.Constraints pCons = layout.getConstraints(parent);
        pCons.setConstraint(javax.swing.SpringLayout.SOUTH,y);
        pCons.setConstraint(javax.swing.SpringLayout.EAST,x);
    }
}
