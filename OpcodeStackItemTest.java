/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import junit.framework.TestCase;
public class OpcodeStackItemTest extends junit.framework.TestCase {
    public OpcodeStackItemTest() {
    }
    public void testMergeIntAndZero() {
        edu.umd.cs.findbugs.OpcodeStack.Item intItem = new edu.umd.cs.findbugs.OpcodeStack.Item("I");
        edu.umd.cs.findbugs.OpcodeStack.Item zeroItem = new edu.umd.cs.findbugs.OpcodeStack.Item("I", 0);
        edu.umd.cs.findbugs.OpcodeStack.Item m1 = edu.umd.cs.findbugs.OpcodeStack.Item.merge(intItem,zeroItem);
        assertNull(m1.getConstant());
        edu.umd.cs.findbugs.OpcodeStack.Item m2 = edu.umd.cs.findbugs.OpcodeStack.Item.merge(zeroItem,intItem);
        assertNull(m2.getConstant());
    }
}
