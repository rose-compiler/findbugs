/*
 * Bytecode analysis framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Field property storing the types of values stored in a field. The idea is
 * that we may be able to determine a more precise type for values loaded from
 * the field than the field type alone would indicate.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.type;
import edu.umd.cs.findbugs.ba.type.*;
import java.util.HashSet;
import java.util.Iterator;
import org.apache.bcel.classfile.ClassFormatException;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.ch.Subtypes2;
public class FieldStoreType extends java.lang.Object {
    private java.util.HashSet<java.lang.String> typeSignatureSet;
    private org.apache.bcel.generic.ReferenceType loadType;
    public FieldStoreType() {
        super();
        this.typeSignatureSet = new java.util.HashSet<java.lang.String>();
    }
// TODO: type may be exact
    public void addTypeSignature(java.lang.String signature) {
        loadType = null;
        typeSignatureSet.add(signature);
    }
    public java.util.Iterator<java.lang.String> signatureIterator() {
        return typeSignatureSet.iterator();
    }
    public org.apache.bcel.generic.ReferenceType getLoadType(org.apache.bcel.generic.ReferenceType fieldType) {
        if (loadType == null) {
            this.computeLoadType(fieldType);
        }
        return loadType;
    }
    private void computeLoadType(org.apache.bcel.generic.ReferenceType fieldType) {
        org.apache.bcel.generic.ReferenceType leastSupertype = null;
        for (java.util.Iterator<java.lang.String> i = this.signatureIterator(); i.hasNext(); ) {
            try {
                java.lang.String signature = i.next();
                org.apache.bcel.generic.Type type = org.apache.bcel.generic.Type.getType(signature);
                if ( !(type instanceof org.apache.bcel.generic.ReferenceType)) continue;
                if (leastSupertype == null) {
                    leastSupertype = (org.apache.bcel.generic.ReferenceType) (type) ;
                }
                else {
                    if (edu.umd.cs.findbugs.ba.ch.Subtypes2.ENABLE_SUBTYPES2_FOR_COMMON_SUPERCLASS_QUERIES) {
                        leastSupertype = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getSubtypes2().getFirstCommonSuperclass(leastSupertype,(org.apache.bcel.generic.ReferenceType) (type) );
                    }
                    else {
                        leastSupertype = leastSupertype.getFirstCommonSuperclass((org.apache.bcel.generic.ReferenceType) (type) );
                    }
                }
            }
            catch (org.apache.bcel.classfile.ClassFormatException e){
            }
            catch (java.lang.ClassNotFoundException e){
                edu.umd.cs.findbugs.ba.AnalysisContext.reportMissingClass(e);
            }
        }
// FIXME: this will mangle interface types, since
// getFirstCommonSuperclass() ignores interfaces.
/*
                 * leastSupertype = (leastSupertype == null) ? (ReferenceType)
                 * type :
                 * leastSupertype.getFirstCommonSuperclass((ReferenceType)
                 * type);
                 */
// Bad signature: ignore
        try {
            if (leastSupertype != null && edu.umd.cs.findbugs.ba.Hierarchy.isSubtype(leastSupertype,fieldType)) loadType = leastSupertype;
        }
        catch (java.lang.ClassNotFoundException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.reportMissingClass(e);
        }
        if (loadType == null) loadType = fieldType;
    }
}
