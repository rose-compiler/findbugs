/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004-2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Build a BugCollection based on SAX events. This is intended to replace the
 * old DOM-based parsing of XML bug result files, which was very slow.
 *
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableSet;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.annotation.CheckForNull;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import edu.umd.cs.findbugs.ba.ClassHash;
import edu.umd.cs.findbugs.filter.AndMatcher;
import edu.umd.cs.findbugs.filter.BugMatcher;
import edu.umd.cs.findbugs.filter.ClassMatcher;
import edu.umd.cs.findbugs.filter.CompoundMatcher;
import edu.umd.cs.findbugs.filter.DesignationMatcher;
import edu.umd.cs.findbugs.filter.FieldMatcher;
import edu.umd.cs.findbugs.filter.Filter;
import edu.umd.cs.findbugs.filter.FirstVersionMatcher;
import edu.umd.cs.findbugs.filter.LastVersionMatcher;
import edu.umd.cs.findbugs.filter.LocalMatcher;
import edu.umd.cs.findbugs.filter.Matcher;
import edu.umd.cs.findbugs.filter.MethodMatcher;
import edu.umd.cs.findbugs.filter.NotMatcher;
import edu.umd.cs.findbugs.filter.OrMatcher;
import edu.umd.cs.findbugs.filter.PriorityMatcher;
import edu.umd.cs.findbugs.filter.RankMatcher;
import edu.umd.cs.findbugs.model.ClassFeatureSet;
import edu.umd.cs.findbugs.util.MapCache;
import edu.umd.cs.findbugs.util.Strings;
public class SAXBugCollectionHandler extends org.xml.sax.helpers.DefaultHandler {
    final private static java.lang.String FIND_BUGS_FILTER = "FindBugsFilter";
    final private static java.lang.String PROJECT = "Project";
    final private static java.lang.String BUG_COLLECTION = "BugCollection";
    final private static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.SAXBugCollectionHandler.class.getName());
/**
     * @param attributes
     * @param qName
     * @return
     */
    public java.lang.String getOptionalAttribute(org.xml.sax.Attributes attributes, java.lang.String qName) {
        return this.memoized(attributes.getValue(qName));
    }
    final private edu.umd.cs.findbugs.BugCollection bugCollection;
    final private edu.umd.cs.findbugs.Project project;
    final private java.util.Stack<edu.umd.cs.findbugs.filter.CompoundMatcher> matcherStack = new java.util.Stack<edu.umd.cs.findbugs.filter.CompoundMatcher>();
    private edu.umd.cs.findbugs.filter.Filter filter;
    final private edu.umd.cs.findbugs.util.MapCache<java.lang.String, java.lang.String> cache = new edu.umd.cs.findbugs.util.MapCache<java.lang.String, java.lang.String>(2000);
    final private java.util.ArrayList<java.lang.String> elementStack;
    final private java.lang.StringBuilder textBuffer;
    private edu.umd.cs.findbugs.BugInstance bugInstance;
    private edu.umd.cs.findbugs.BugAnnotationWithSourceLines bugAnnotationWithSourceLines;
    private edu.umd.cs.findbugs.AnalysisError analysisError;
// private ClassHash classHash;
    private edu.umd.cs.findbugs.model.ClassFeatureSet classFeatureSet;
    final private java.util.ArrayList<java.lang.String> stackTrace;
    private int nestingOfIgnoredElements = 0;
    final private java.io.File base;
    final private java.lang.String topLevelName;
    private java.lang.String cloudPropertyKey;
    public SAXBugCollectionHandler(java.lang.String topLevelName, edu.umd.cs.findbugs.BugCollection bugCollection, edu.umd.cs.findbugs.Project project, java.io.File base) {
        super();
        this.topLevelName = topLevelName;
        this.bugCollection = bugCollection;
        this.project = project;
        this.elementStack = new java.util.ArrayList<java.lang.String>();
        this.textBuffer = new java.lang.StringBuilder();
        this.stackTrace = new java.util.ArrayList<java.lang.String>();
        this.base = base;
    }
    public SAXBugCollectionHandler(edu.umd.cs.findbugs.BugCollection bugCollection, java.io.File base) {
        this(BUG_COLLECTION,bugCollection,bugCollection.getProject(),base);
    }
    public SAXBugCollectionHandler(edu.umd.cs.findbugs.BugCollection bugCollection) {
        this(BUG_COLLECTION,bugCollection,bugCollection.getProject(),null);
    }
    public SAXBugCollectionHandler(edu.umd.cs.findbugs.Project project, java.io.File base) {
        this(PROJECT,null,project,base);
    }
    public SAXBugCollectionHandler(edu.umd.cs.findbugs.filter.Filter filter, java.io.File base) {
        this(FIND_BUGS_FILTER,null,null,base);
        this.filter = filter;
        this.pushCompoundMatcher(filter);
    }
    java.util.regex.Pattern ignoredElement = java.util.regex.Pattern.compile("Message|ShortMessage|LongMessage");
    public boolean discardedElement(java.lang.String qName) {
        return ignoredElement.matcher(qName).matches();
    }
    public java.lang.String getTextContents() {
        return this.memoized(edu.umd.cs.findbugs.util.Strings.unescapeXml(textBuffer.toString()));
    }
    private java.lang.String memoized(java.lang.String s) {
        if (s == null) return s;
        java.lang.String result = cache.get(s);
        if (result != null) return result;
        cache.put(s,s);
        return s;
    }
    private static boolean DEBUG = false;
    public void startElement(java.lang.String uri, java.lang.String name, java.lang.String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
// URI should always be empty.
// So, qName is the name of the element.
        if (this.discardedElement(qName)) {
            nestingOfIgnoredElements++;
        }
        else if (nestingOfIgnoredElements > 0) {
        }
        else {
// We should be parsing the outer BugCollection element.
            if (elementStack.isEmpty() &&  !qName.equals(topLevelName)) throw new org.xml.sax.SAXException("Invalid top-level element (expected " + topLevelName + ", saw " + qName + ")");
            if (qName.equals(BUG_COLLECTION)) {
// Read and set the sequence number.
                java.lang.String version = this.getOptionalAttribute(attributes,"version");
                if (bugCollection instanceof edu.umd.cs.findbugs.SortedBugCollection) bugCollection.setAnalysisVersion(version);
// Read and set the sequence number.
                java.lang.String sequence = this.getOptionalAttribute(attributes,"sequence");
                long seqval = this.parseLong(sequence,0L);
                bugCollection.setSequenceNumber(seqval);
// Read and set timestamp.
                java.lang.String timestamp = this.getOptionalAttribute(attributes,"timestamp");
                long tsval = this.parseLong(timestamp, -1L);
                bugCollection.setTimestamp(tsval);
// Read and set timestamp.
                java.lang.String analysisTimestamp = this.getOptionalAttribute(attributes,"analysisTimestamp");
                if (analysisTimestamp != null) {
                    bugCollection.setAnalysisTimestamp(this.parseLong(analysisTimestamp, -1L));
                }
                java.lang.String analysisVersion = this.getOptionalAttribute(attributes,"version");
                if (analysisVersion != null) {
                    bugCollection.setAnalysisVersion(analysisVersion);
                }
// Set release name, if present.
                java.lang.String releaseName = this.getOptionalAttribute(attributes,"release");
                bugCollection.setReleaseName((releaseName != null) ? releaseName : "");
            }
            else if (this.isTopLevelFilter(qName)) {
                if (project != null) {
                    filter = new edu.umd.cs.findbugs.filter.Filter();
                    project.setSuppressionFilter(filter);
                }
                matcherStack.clear();
                this.pushCompoundMatcher(filter);
            }
            else if (qName.equals(PROJECT)) {
// Project element
                java.lang.String projectName = this.getOptionalAttribute(attributes,edu.umd.cs.findbugs.Project.PROJECTNAME_ATTRIBUTE_NAME);
                if (projectName != null) project.setProjectName(projectName);
            }
            else {
                java.lang.String outerElement = elementStack.get(elementStack.size() - 1);
                if (outerElement.equals(BUG_COLLECTION)) {
// Parsing a top-level element of the BugCollection
                    if (qName.equals("BugInstance")) {
// BugInstance element - get required type and priority
// attributes
                        java.lang.String type = this.getRequiredAttribute(attributes,"type",qName);
                        java.lang.String priority = this.getRequiredAttribute(attributes,"priority",qName);
                        try {
                            int prio = java.lang.Integer.parseInt(priority);
                            bugInstance = new edu.umd.cs.findbugs.BugInstance(type, prio);
                        }
                        catch (java.lang.NumberFormatException e){
                            throw new org.xml.sax.SAXException("BugInstance with invalid priority value \"" + priority + "\"", e);
                        }
                        java.lang.String firstVersion = this.getOptionalAttribute(attributes,"first");
                        if (firstVersion != null) {
                            bugInstance.setFirstVersion(java.lang.Long.parseLong(firstVersion));
                        }
                        java.lang.String lastVersion = this.getOptionalAttribute(attributes,"last");
                        if (lastVersion != null) {
                            bugInstance.setLastVersion(java.lang.Long.parseLong(lastVersion));
                        }
                        if (bugInstance.isDead() && bugInstance.getFirstVersion() > bugInstance.getLastVersion()) throw new java.lang.IllegalStateException("huh");
                        java.lang.String introducedByChange = this.getOptionalAttribute(attributes,"introducedByChange");
                        if (introducedByChange != null) {
                            bugInstance.setIntroducedByChangeOfExistingClass(java.lang.Boolean.parseBoolean(introducedByChange));
                        }
                        java.lang.String removedByChange = this.getOptionalAttribute(attributes,"removedByChange");
                        if (removedByChange != null) {
                            bugInstance.setRemovedByChangeOfPersistingClass(java.lang.Boolean.parseBoolean(removedByChange));
                        }
                        java.lang.String oldInstanceHash = this.getOptionalAttribute(attributes,"instanceHash");
                        if (oldInstanceHash == null) oldInstanceHash = this.getOptionalAttribute(attributes,"oldInstanceHash");
                        if (oldInstanceHash != null) {
                            bugInstance.setOldInstanceHash(oldInstanceHash);
                        }
                        java.lang.String firstSeen = this.getOptionalAttribute(attributes,"firstSeen");
                        if (firstSeen != null) {
                            try {
                                bugInstance.getXmlProps().setFirstSeen(edu.umd.cs.findbugs.BugInstance.firstSeenXMLFormat().parse(firstSeen));
                            }
                            catch (java.text.ParseException e){
                                LOGGER.warning("Could not parse first seen entry: " + firstSeen);
                            }
                        }
                        java.lang.String isInCloud = this.getOptionalAttribute(attributes,"isInCloud");
                        if (isInCloud != null) bugInstance.getXmlProps().setIsInCloud(java.lang.Boolean.parseBoolean(isInCloud));
                        java.lang.String reviewCount = this.getOptionalAttribute(attributes,"reviews");
                        if (reviewCount != null) {
                            bugInstance.getXmlProps().setReviewCount(java.lang.Integer.parseInt(reviewCount));
                        }
                        java.lang.String consensus = this.getOptionalAttribute(attributes,"consensus");
                        if (consensus != null) {
                            bugInstance.getXmlProps().setConsensus(consensus);
                        }
                    }
                    else if (qName.equals("FindBugsSummary")) {
                        java.lang.String timestamp = this.getRequiredAttribute(attributes,"timestamp",qName);
                        java.lang.String vmVersion = this.getOptionalAttribute(attributes,"vm_version");
                        java.lang.String totalClasses = this.getOptionalAttribute(attributes,"total_classes");
                        if (totalClasses != null && totalClasses.length() > 0) bugCollection.getProjectStats().setTotalClasses(java.lang.Integer.parseInt(totalClasses));
                        java.lang.String totalSize = this.getOptionalAttribute(attributes,"total_size");
                        if (totalSize != null && totalSize.length() > 0) bugCollection.getProjectStats().setTotalSize(java.lang.Integer.parseInt(totalSize));
                        java.lang.String referencedClasses = this.getOptionalAttribute(attributes,"referenced_classes");
                        if (referencedClasses != null && referencedClasses.length() > 0) bugCollection.getProjectStats().setReferencedClasses(java.lang.Integer.parseInt(referencedClasses));
                        bugCollection.getProjectStats().setVMVersion(vmVersion);
                        try {
                            bugCollection.getProjectStats().setTimestamp(timestamp);
                        }
                        catch (java.text.ParseException e){
                            throw new org.xml.sax.SAXException("Unparseable sequence number: '" + timestamp + "'", e);
                        }
                    }
                }
                else if (outerElement.equals("BugInstance")) {
                    this.parseBugInstanceContents(qName,attributes);
                }
                else if (outerElement.equals("Method") || outerElement.equals("Field") || outerElement.equals("Class") || outerElement.equals("Type")) {
                    if (qName.equals("SourceLine")) {
                        bugAnnotationWithSourceLines.setSourceLines(this.createSourceLineAnnotation(qName,attributes));
                    }
                }
                else if (outerElement.equals(edu.umd.cs.findbugs.BugCollection.ERRORS_ELEMENT_NAME)) {
                    if (qName.equals(edu.umd.cs.findbugs.BugCollection.ANALYSIS_ERROR_ELEMENT_NAME) || qName.equals(edu.umd.cs.findbugs.BugCollection.ERROR_ELEMENT_NAME)) {
                        analysisError = new edu.umd.cs.findbugs.AnalysisError("Unknown error");
                        stackTrace.clear();
                    }
                }
                else if (outerElement.equals("FindBugsSummary") && qName.equals("PackageStats")) {
                    java.lang.String packageName = this.getRequiredAttribute(attributes,"package",qName);
                    int numClasses = java.lang.Integer.valueOf(this.getRequiredAttribute(attributes,"total_types",qName));
                    int size = java.lang.Integer.valueOf(this.getRequiredAttribute(attributes,"total_size",qName));
                    bugCollection.getProjectStats().putPackageStats(packageName,numClasses,size);
                }
                else if (outerElement.equals("PackageStats")) {
                    if (qName.equals("ClassStats")) {
                        java.lang.String className = this.getRequiredAttribute(attributes,"class",qName);
                        java.lang.Boolean isInterface = java.lang.Boolean.valueOf(this.getRequiredAttribute(attributes,"interface",qName));
                        int size = java.lang.Integer.valueOf(this.getRequiredAttribute(attributes,"size",qName));
                        java.lang.String sourceFile = this.getOptionalAttribute(attributes,"sourceFile");
                        bugCollection.getProjectStats().addClass(className,sourceFile,isInterface,size,false);
                    }
                }
                else if (this.isTopLevelFilter(outerElement) || this.isCompoundElementTag(outerElement)) {
                    this.parseMatcher(qName,attributes);
                }
                else if (outerElement.equals("ClassFeatures")) {
                    if (qName.equals(edu.umd.cs.findbugs.model.ClassFeatureSet.ELEMENT_NAME)) {
                        java.lang.String className = this.getRequiredAttribute(attributes,"class",qName);
                        classFeatureSet = new edu.umd.cs.findbugs.model.ClassFeatureSet();
                        classFeatureSet.setClassName(className);
                    }
                }
                else if (outerElement.equals(edu.umd.cs.findbugs.model.ClassFeatureSet.ELEMENT_NAME)) {
                    if (qName.equals(edu.umd.cs.findbugs.model.ClassFeatureSet.FEATURE_ELEMENT_NAME)) {
                        java.lang.String value = this.getRequiredAttribute(attributes,"value",qName);
                        classFeatureSet.addFeature(value);
                    }
                }
                else if (outerElement.equals(edu.umd.cs.findbugs.BugCollection.HISTORY_ELEMENT_NAME)) {
                    if (qName.equals(edu.umd.cs.findbugs.AppVersion.ELEMENT_NAME)) {
                        try {
                            java.lang.String sequence = this.getRequiredAttribute(attributes,"sequence",qName);
                            java.lang.String timestamp = this.getOptionalAttribute(attributes,"timestamp");
                            java.lang.String releaseName = this.getOptionalAttribute(attributes,"release");
                            java.lang.String codeSize = this.getOptionalAttribute(attributes,"codeSize");
                            java.lang.String numClasses = this.getOptionalAttribute(attributes,"numClasses");
                            edu.umd.cs.findbugs.AppVersion appVersion = new edu.umd.cs.findbugs.AppVersion(java.lang.Long.valueOf(sequence));
                            if (timestamp != null) appVersion.setTimestamp(java.lang.Long.valueOf(timestamp));
                            if (releaseName != null) appVersion.setReleaseName(releaseName);
                            if (codeSize != null) appVersion.setCodeSize(java.lang.Integer.parseInt(codeSize));
                            if (numClasses != null) appVersion.setNumClasses(java.lang.Integer.parseInt(numClasses));
                            bugCollection.addAppVersion(appVersion);
                        }
                        catch (java.lang.NumberFormatException e){
                            throw new org.xml.sax.SAXException("Invalid AppVersion element", e);
                        }
                    }
                }
                else if (outerElement.equals(edu.umd.cs.findbugs.BugCollection.PROJECT_ELEMENT_NAME)) {
                    if (qName.equals(edu.umd.cs.findbugs.Project.CLOUD_ELEMENT_NAME)) {
                        java.lang.String cloudId = this.getRequiredAttribute(attributes,edu.umd.cs.findbugs.Project.CLOUD_ID_ATTRIBUTE_NAME,qName);
                        project.setCloudId(cloudId);
                        java.util.Map<java.lang.String, java.lang.String> map = new java.util.HashMap<java.lang.String, java.lang.String>();
                        for (int i = 0; i < attributes.getLength(); i++) {
                            map.put(attributes.getLocalName(i),attributes.getValue(i));
                        }
                        bugCollection.setXmlCloudDetails(java.util.Collections.unmodifiableMap(map));
                    }
                    else if (qName.equals(edu.umd.cs.findbugs.Project.PLUGIN_ELEMENT_NAME)) {
                        java.lang.String pluginId = this.getRequiredAttribute(attributes,edu.umd.cs.findbugs.Project.PLUGIN_ID_ATTRIBUTE_NAME,qName);
                        java.lang.Boolean enabled = java.lang.Boolean.valueOf(this.getRequiredAttribute(attributes,edu.umd.cs.findbugs.Project.PLUGIN_STATUS_ELEMENT_NAME,qName));
                        project.setPluginStatusTrinary(pluginId,enabled);
                    }
                }
                else if (outerElement.equals(edu.umd.cs.findbugs.Project.CLOUD_ELEMENT_NAME)) {
                    if (qName.equals(edu.umd.cs.findbugs.Project.CLOUD_PROPERTY_ELEMENT_NAME)) {
                        cloudPropertyKey = this.getRequiredAttribute(attributes,"key",qName);
                    }
                }
            }
        }
        textBuffer.delete(0,textBuffer.length());
        elementStack.add(qName);
    }
    private boolean isCompoundElementTag(java.lang.String qName) {
        return outerElementTags.contains(qName);
    }
    private boolean isTopLevelFilter(java.lang.String qName) {
        return qName.equals(FIND_BUGS_FILTER) || qName.equals("SuppressionFilter");
    }
    private void addMatcher(edu.umd.cs.findbugs.filter.Matcher m) {
        if (m == null) throw new java.lang.IllegalArgumentException("matcher must not be null");
        edu.umd.cs.findbugs.filter.CompoundMatcher peek = matcherStack.peek();
        if (peek == null) throw new java.lang.NullPointerException("Top of stack is null");
        peek.addChild(m);
        if (nextMatchedIsDisabled) {
            if (peek instanceof edu.umd.cs.findbugs.filter.Filter) ((edu.umd.cs.findbugs.filter.Filter) (peek) ).disable(m);
            else assert false;
            nextMatchedIsDisabled = false;
        }
    }
    private void pushCompoundMatcherAsChild(edu.umd.cs.findbugs.filter.CompoundMatcher m) {
        this.addMatcher(m);
        this.pushCompoundMatcher(m);
    }
    private void pushCompoundMatcher(edu.umd.cs.findbugs.filter.CompoundMatcher m) {
        if (m == null) throw new java.lang.IllegalArgumentException("matcher must not be null");
        matcherStack.push(m);
    }
    boolean nextMatchedIsDisabled;
    final private java.util.Set<java.lang.String> outerElementTags = unmodifiableSet(new java.util.HashSet<java.lang.String>(asList("And","Match","Or","Not")));
    private void parseMatcher(java.lang.String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
        if (DEBUG) java.lang.System.out.println(elementStack + " " + qName + " " + matcherStack);
        java.lang.String disabled = this.getOptionalAttribute(attributes,"disabled");
        nextMatchedIsDisabled = "true".equals(disabled);
        if (qName.equals("Bug")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.BugMatcher(this.getOptionalAttribute(attributes,"code"), this.getOptionalAttribute(attributes,"pattern"), this.getOptionalAttribute(attributes,"category")));
        }
        else if (qName.equals("Class")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.ClassMatcher(this.getRequiredAttribute(attributes,"name",qName)));
        }
        else if (qName.equals("FirstVersion")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.FirstVersionMatcher(this.getRequiredAttribute(attributes,"value",qName), this.getRequiredAttribute(attributes,"relOp",qName)));
        }
        else if (qName.equals("LastVersion")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.LastVersionMatcher(this.getRequiredAttribute(attributes,"value",qName), this.getRequiredAttribute(attributes,"relOp",qName)));
        }
        else if (qName.equals("Designation")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.DesignationMatcher(this.getRequiredAttribute(attributes,"designation",qName)));
        }
        else if (qName.equals("BugCode")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.BugMatcher(this.getRequiredAttribute(attributes,"name",qName), "", ""));
        }
        else if (qName.equals("Local")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.LocalMatcher(this.getRequiredAttribute(attributes,"name",qName)));
        }
        else if (qName.equals("BugPattern")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.BugMatcher("", this.getRequiredAttribute(attributes,"name",qName), ""));
        }
        else if (qName.equals("Priority") || qName.equals("Confidence")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.PriorityMatcher(this.getRequiredAttribute(attributes,"value",qName)));
        }
        else if (qName.equals("Rank")) {
            this.addMatcher(new edu.umd.cs.findbugs.filter.RankMatcher(this.getRequiredAttribute(attributes,"value",qName)));
        }
        else if (qName.equals("Package")) {
            java.lang.String pName = this.getRequiredAttribute(attributes,"name",qName);
            pName = pName.startsWith("~") ? pName : "~" + pName.replace(".","\\.");
            this.addMatcher(new edu.umd.cs.findbugs.filter.ClassMatcher(pName + "\\.[^.]+"));
        }
        else if (qName.equals("Method")) {
            java.lang.String name = this.getOptionalAttribute(attributes,"name");
            java.lang.String params = this.getOptionalAttribute(attributes,"params");
            java.lang.String returns = this.getOptionalAttribute(attributes,"returns");
            java.lang.String role = this.getOptionalAttribute(attributes,"role");
            this.addMatcher(new edu.umd.cs.findbugs.filter.MethodMatcher(name, params, returns, role));
        }
        else if (qName.equals("Field")) {
            java.lang.String name = this.getOptionalAttribute(attributes,"name");
            java.lang.String type = this.getOptionalAttribute(attributes,"type");
            this.addMatcher(new edu.umd.cs.findbugs.filter.FieldMatcher(name, type));
        }
        else if (qName.equals("Or")) {
            edu.umd.cs.findbugs.filter.CompoundMatcher matcher = new edu.umd.cs.findbugs.filter.OrMatcher();
            this.pushCompoundMatcherAsChild(matcher);
        }
        else if (qName.equals("And") || qName.equals("Match")) {
            edu.umd.cs.findbugs.filter.AndMatcher matcher = new edu.umd.cs.findbugs.filter.AndMatcher();
            this.pushCompoundMatcherAsChild(matcher);
            if (qName.equals("Match")) {
                java.lang.String classregex = this.getOptionalAttribute(attributes,"classregex");
                java.lang.String classMatch = this.getOptionalAttribute(attributes,"class");
                if (classregex != null) this.addMatcher(new edu.umd.cs.findbugs.filter.ClassMatcher("~" + classregex));
                else if (classMatch != null) this.addMatcher(new edu.umd.cs.findbugs.filter.ClassMatcher(classMatch));
            }
        }
        else if (qName.equals("Not")) {
            edu.umd.cs.findbugs.filter.NotMatcher matcher = new edu.umd.cs.findbugs.filter.NotMatcher();
            this.pushCompoundMatcherAsChild(matcher);
        }
        nextMatchedIsDisabled = false;
    }
    private void parseBugInstanceContents(java.lang.String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
// Parsing an attribute or property of a BugInstance
        edu.umd.cs.findbugs.BugAnnotation bugAnnotation = null;
        if (qName.equals("Class")) {
            java.lang.String className = this.getRequiredAttribute(attributes,"classname",qName);
            bugAnnotation = bugAnnotationWithSourceLines = new edu.umd.cs.findbugs.ClassAnnotation(className);
        }
        else if (qName.equals("Type")) {
            java.lang.String typeDescriptor = this.getRequiredAttribute(attributes,"descriptor",qName);
            edu.umd.cs.findbugs.TypeAnnotation typeAnnotation;
            bugAnnotation = bugAnnotationWithSourceLines = typeAnnotation = new edu.umd.cs.findbugs.TypeAnnotation(typeDescriptor);
            java.lang.String typeParameters = this.getOptionalAttribute(attributes,"typeParameters");
            if (typeParameters != null) typeAnnotation.setTypeParameters(edu.umd.cs.findbugs.util.Strings.unescapeXml(typeParameters));
        }
        else if (qName.equals("Method") || qName.equals("Field")) {
            java.lang.String classname = this.getRequiredAttribute(attributes,"classname",qName);
            java.lang.String fieldOrMethodName = this.getRequiredAttribute(attributes,"name",qName);
            java.lang.String signature = this.getRequiredAttribute(attributes,"signature",qName);
            if (qName.equals("Method")) {
                java.lang.String isStatic = this.getOptionalAttribute(attributes,"isStatic");
                if (isStatic == null) {
                    isStatic = "false";
                }
                bugAnnotation = bugAnnotationWithSourceLines = new edu.umd.cs.findbugs.MethodAnnotation(classname, fieldOrMethodName, signature, java.lang.Boolean.valueOf(isStatic));
            }
            else {
                java.lang.String isStatic = this.getRequiredAttribute(attributes,"isStatic",qName);
                bugAnnotation = bugAnnotationWithSourceLines = new edu.umd.cs.findbugs.FieldAnnotation(classname, fieldOrMethodName, signature, java.lang.Boolean.valueOf(isStatic));
            }
        }
        else if (qName.equals("SourceLine")) {
            edu.umd.cs.findbugs.SourceLineAnnotation sourceAnnotation = this.createSourceLineAnnotation(qName,attributes);
            if ( !sourceAnnotation.isSynthetic()) bugAnnotation = sourceAnnotation;
        }
        else if (qName.equals("Int")) {
            try {
                java.lang.String value = this.getRequiredAttribute(attributes,"value",qName);
                bugAnnotation = new edu.umd.cs.findbugs.IntAnnotation(java.lang.Integer.parseInt(value));
            }
            catch (java.lang.NumberFormatException e){
                throw new org.xml.sax.SAXException("Bad integer value in Int");
            }
        }
        else if (qName.equals("String")) {
            java.lang.String value = this.getRequiredAttribute(attributes,"value",qName);
            bugAnnotation = edu.umd.cs.findbugs.StringAnnotation.fromXMLEscapedString(value);
        }
        else if (qName.equals("LocalVariable")) {
            try {
                java.lang.String varName = this.getRequiredAttribute(attributes,"name",qName);
                int register = java.lang.Integer.parseInt(this.getRequiredAttribute(attributes,"register",qName));
                int pc = java.lang.Integer.parseInt(this.getRequiredAttribute(attributes,"pc",qName));
                bugAnnotation = new edu.umd.cs.findbugs.LocalVariableAnnotation(varName, register, pc);
            }
            catch (java.lang.NumberFormatException e){
                throw new org.xml.sax.SAXException("Invalid integer value in attribute of LocalVariable element");
            }
        }
        else if (qName.equals("Property")) {
// A BugProperty.
            java.lang.String propName = this.getRequiredAttribute(attributes,"name",qName);
            java.lang.String propValue = this.getRequiredAttribute(attributes,"value",qName);
            bugInstance.setProperty(propName,propValue);
        }
        else if (qName.equals("UserAnnotation")) {
// ignore AnnotationText for now; will handle in endElement
// optional
            java.lang.String s = this.getOptionalAttribute(attributes,"designation");
            if (s != null) {
                bugInstance.setUserDesignationKey(s,null);
            }
            s = this.getOptionalAttribute(attributes,"user");
// optional
            if (s != null) bugInstance.setUser(s);
            s = this.getOptionalAttribute(attributes,"timestamp");
// optional
            if (s != null) try {
                long timestamp = java.lang.Long.valueOf(s);
                bugInstance.setUserAnnotationTimestamp(timestamp);
            }
            catch (java.lang.NumberFormatException nfe){
            }
            s = this.getOptionalAttribute(attributes,"needsSync");
// optional
            if (s == null || s.equals("false")) bugInstance.setUserAnnotationDirty(false);
        }
        else throw new org.xml.sax.SAXException("Unknown bug annotation named " + qName);
        if (bugAnnotation != null) {
            java.lang.String role = this.getOptionalAttribute(attributes,"role");
            if (role != null) bugAnnotation.setDescription(role);
            this.setAnnotationRole(attributes,bugAnnotation);
            bugInstance.add(bugAnnotation);
        }
    }
    private long parseLong(java.lang.String s, long defaultValue) {
        long value;
        try {
            value = (s != null) ? java.lang.Long.parseLong(s) : defaultValue;
        }
        catch (java.lang.NumberFormatException e){
            value = defaultValue;
        }
        return value;
    }
/**
     * Extract a hash value from an element.
     *
     * @param qName
     *            name of element containing hash value
     * @param attributes
     *            element attributes
     * @return the decoded hash value
     * @throws SAXException
     */
    private byte[] extractHash(java.lang.String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
        java.lang.String encodedHash = this.getRequiredAttribute(attributes,"value",qName);
        byte[] hash;
        try {
            hash = edu.umd.cs.findbugs.ba.ClassHash.stringToHash(encodedHash);
        }
        catch (java.lang.IllegalArgumentException e){
            throw new org.xml.sax.SAXException("Invalid class hash", e);
        }
        return hash;
    }
    private void setAnnotationRole(org.xml.sax.Attributes attributes, edu.umd.cs.findbugs.BugAnnotation bugAnnotation) {
        java.lang.String role = this.getOptionalAttribute(attributes,"role");
        if (role != null) bugAnnotation.setDescription(role);
    }
    private edu.umd.cs.findbugs.SourceLineAnnotation createSourceLineAnnotation(java.lang.String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
        java.lang.String classname = this.getRequiredAttribute(attributes,"classname",qName);
        java.lang.String sourceFile = this.getOptionalAttribute(attributes,"sourcefile");
        if (sourceFile == null) sourceFile = edu.umd.cs.findbugs.SourceLineAnnotation.UNKNOWN_SOURCE_FILE;
// "start"/"end"
        java.lang.String startLine = this.getOptionalAttribute(attributes,"start");
// are now
// optional
// (were too
        java.lang.String endLine = this.getOptionalAttribute(attributes,"end");
// many "-1"s
// in the xml)
        java.lang.String startBytecode = this.getOptionalAttribute(attributes,"startBytecode");
        java.lang.String endBytecode = this.getOptionalAttribute(attributes,"endBytecode");
        java.lang.String synthetic = this.getOptionalAttribute(attributes,"synthetic");
        try {
            int sl = startLine != null ? java.lang.Integer.parseInt(startLine) :  -1;
            int el = endLine != null ? java.lang.Integer.parseInt(endLine) :  -1;
            int sb = startBytecode != null ? java.lang.Integer.parseInt(startBytecode) :  -1;
            int eb = endBytecode != null ? java.lang.Integer.parseInt(endBytecode) :  -1;
            edu.umd.cs.findbugs.SourceLineAnnotation s = new edu.umd.cs.findbugs.SourceLineAnnotation(classname, sourceFile, sl, el, sb, eb);
            if ("true".equals(synthetic)) s.setSynthetic(true);
            return s;
        }
        catch (java.lang.NumberFormatException e){
            throw new org.xml.sax.SAXException("Bad integer value in SourceLine element", e);
        }
    }
    public void endElement(java.lang.String uri, java.lang.String name, java.lang.String qName) throws org.xml.sax.SAXException {
// URI should always be empty.
// So, qName is the name of the element.
        if (this.discardedElement(qName)) {
            nestingOfIgnoredElements--;
        }
        else if (nestingOfIgnoredElements > 0) {
        }
        else if (qName.equals("Project")) {
        }
        else if (elementStack.size() > 1) {
            java.lang.String outerElement = elementStack.get(elementStack.size() - 2);
            if (this.isTopLevelFilter(qName) || this.isCompoundElementTag(qName)) {
                if (DEBUG) java.lang.System.out.println("  ending " + elementStack + " " + qName + " " + matcherStack);
                matcherStack.pop();
            }
            else if (outerElement.equals(BUG_COLLECTION)) {
                if (qName.equals("BugInstance")) {
                    bugCollection.add(bugInstance,false);
                }
            }
            else if (outerElement.equals(PROJECT)) {
                if (qName.equals("Jar")) project.addFile(this.makeAbsolute(this.getTextContents()));
                else if (qName.equals("SrcDir")) project.addSourceDir(this.makeAbsolute(this.getTextContents()));
                else if (qName.equals("AuxClasspathEntry")) project.addAuxClasspathEntry(this.makeAbsolute(this.getTextContents()));
            }
            else if (outerElement.equals(edu.umd.cs.findbugs.Project.CLOUD_ELEMENT_NAME) && qName.equals(edu.umd.cs.findbugs.Project.CLOUD_PROPERTY_ELEMENT_NAME)) {
                assert cloudPropertyKey != null;
                project.getCloudProperties().setProperty(cloudPropertyKey,this.getTextContents());
                cloudPropertyKey = null;
            }
            else if (outerElement.equals("BugInstance")) {
                if (qName.equals("UserAnnotation")) {
                    bugInstance.setAnnotationText(this.getTextContents(),null);
                }
            }
            else if (outerElement.equals(edu.umd.cs.findbugs.BugCollection.ERRORS_ELEMENT_NAME)) {
                if (qName.equals(edu.umd.cs.findbugs.BugCollection.ANALYSIS_ERROR_ELEMENT_NAME)) {
                    analysisError.setMessage(this.getTextContents());
                    bugCollection.addError(analysisError);
                }
                else if (qName.equals(edu.umd.cs.findbugs.BugCollection.ERROR_ELEMENT_NAME)) {
                    if (stackTrace.size() > 0) {
                        analysisError.setStackTrace(stackTrace.toArray(new java.lang.String[stackTrace.size()]));
                    }
                    bugCollection.addError(analysisError);
                }
                else if (qName.equals(edu.umd.cs.findbugs.BugCollection.MISSING_CLASS_ELEMENT_NAME)) {
                    bugCollection.addMissingClass(this.getTextContents());
                }
            }
            else if (outerElement.equals(edu.umd.cs.findbugs.BugCollection.ERROR_ELEMENT_NAME)) {
                if (qName.equals(edu.umd.cs.findbugs.BugCollection.ERROR_MESSAGE_ELEMENT_NAME)) {
                    analysisError.setMessage(this.getTextContents());
                }
                else if (qName.equals(edu.umd.cs.findbugs.BugCollection.ERROR_EXCEPTION_ELEMENT_NAME)) {
                    analysisError.setExceptionMessage(this.getTextContents());
                }
                else if (qName.equals(edu.umd.cs.findbugs.BugCollection.ERROR_STACK_TRACE_ELEMENT_NAME)) {
                    stackTrace.add(this.getTextContents());
                }
            }
            else if (outerElement.equals("ClassFeatures")) {
                if (qName.equals(edu.umd.cs.findbugs.model.ClassFeatureSet.ELEMENT_NAME)) {
                    bugCollection.setClassFeatureSet(classFeatureSet);
                    classFeatureSet = null;
                }
            }
        }
        elementStack.remove(elementStack.size() - 1);
    }
    private java.lang.String makeAbsolute(java.lang.String possiblyRelativePath) {
        if (possiblyRelativePath.contains("://") || possiblyRelativePath.startsWith("http:") || possiblyRelativePath.startsWith("https:") || possiblyRelativePath.startsWith("file:")) return possiblyRelativePath;
        if (base == null) return possiblyRelativePath;
        if (new java.io.File(possiblyRelativePath).isAbsolute()) return possiblyRelativePath;
        return new java.io.File(base.getParentFile(), possiblyRelativePath).getAbsolutePath();
    }
    public void characters(char[] ch, int start, int length) {
        textBuffer.append(ch,start,length);
    }
    private java.lang.String getRequiredAttribute(org.xml.sax.Attributes attributes, java.lang.String attrName, java.lang.String elementName) throws org.xml.sax.SAXException {
        java.lang.String value = attributes.getValue(attrName);
        if (value == null) throw new org.xml.sax.SAXException(elementName + " element missing " + attrName + " attribute");
        return this.memoized(edu.umd.cs.findbugs.util.Strings.unescapeXml(value));
    }
}
// vim:ts=4
