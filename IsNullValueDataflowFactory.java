/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce IsNullValueDataflow objects for an analyzed
 * method.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.ba.AssertionMethods;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DepthFirstSearch;
import edu.umd.cs.findbugs.ba.JavaClassAndMethod;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.ba.npe.IsNullValueAnalysis;
import edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class IsNullValueDataflowFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow> {
/**
     * Constructor.
     */
    public IsNullValueDataflowFactory() {
        super("null value analysis",edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.generic.MethodGen methodGen = this.getMethodGen(analysisCache,descriptor);
        if (methodGen == null) {
            throw new edu.umd.cs.findbugs.ba.MethodUnprofitableException(descriptor);
        }
        edu.umd.cs.findbugs.ba.CFG cfg = this.getCFG(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow vnaDataflow = this.getValueNumberDataflow(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.DepthFirstSearch dfs = this.getDepthFirstSearch(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.AssertionMethods assertionMethods = this.getAssertionMethods(analysisCache,descriptor.getClassDescriptor());
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = this.getTypeDataflow(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.npe.IsNullValueAnalysis invAnalysis = new edu.umd.cs.findbugs.ba.npe.IsNullValueAnalysis(descriptor, methodGen, cfg, vnaDataflow, typeDataflow, dfs, assertionMethods);
        invAnalysis.setClassAndMethod(new edu.umd.cs.findbugs.ba.JavaClassAndMethod(this.getJavaClass(analysisCache,descriptor.getClassDescriptor()), this.getMethod(analysisCache,descriptor)));
// Set return value and parameter databases
        edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow invDataflow = new edu.umd.cs.findbugs.ba.npe.IsNullValueDataflow(cfg, invAnalysis);
        invDataflow.execute();
        if (edu.umd.cs.findbugs.ba.ClassContext.DUMP_DATAFLOW_ANALYSIS) {
            invDataflow.dumpDataflow(invAnalysis);
        }
        return invDataflow;
    }
}
