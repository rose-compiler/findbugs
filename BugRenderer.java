/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 *  Sets colors for JTree nodes
 *  @author Dan
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.Priorities;
public class BugRenderer extends javax.swing.tree.DefaultTreeCellRenderer {
    public BugRenderer() {
    }
    public java.awt.Component getTreeCellRendererComponent(javax.swing.JTree tree, java.lang.Object node, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        java.awt.Component toReturn = super.getTreeCellRendererComponent(tree,node,selected,expanded,leaf,row,hasFocus);
        if ( !(node instanceof edu.umd.cs.findbugs.gui2.BugLeafNode)) return toReturn;
        else {
            edu.umd.cs.findbugs.BugInstance bug = ((edu.umd.cs.findbugs.gui2.BugLeafNode) (node) ).getBug();
            final java.awt.Color c;
            switch(bug.getPriority()){
                case edu.umd.cs.findbugs.Priorities.LOW_PRIORITY:{
                    c = new java.awt.Color(0.4f, 0.4f, 0.6f);
                    break;
                }
                case edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY:{
                    if (bug.isDead()) c = new java.awt.Color(0.2f, 0.2f, 0.2f);
                    else c = new java.awt.Color(255, 204, 0);
                    break;
                }
                case edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY:{
                    if (bug.isDead()) c = new java.awt.Color(.65f, 0.2f, 0.2f);
                    else c = new java.awt.Color(.85f, 0, 0);
                    break;
                }
                case edu.umd.cs.findbugs.Priorities.EXP_PRIORITY:{
                }
                case edu.umd.cs.findbugs.Priorities.IGNORE_PRIORITY:{
                }
                default:{
                    c = java.awt.Color.blue;
                    break;
                }
            }
            if (leaf) {
                javax.swing.Icon icon = new javax.swing.Icon() {
                    public void paintIcon(java.awt.Component comp, java.awt.Graphics g, int x, int y) {
                        java.awt.Graphics2D g2 = (java.awt.Graphics2D) (g) ;
                        g2.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING,java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
                        g2.setColor(c);
                        g2.fillOval(2,2,12,12);
                        g2.setColor(java.awt.Color.BLACK);
                        g2.drawOval(2,2,12,12);
                    }
                    public int getIconWidth() {
                        return 16;
                    }
                    public int getIconHeight() {
                        return 16;
                    }
                };
                ((edu.umd.cs.findbugs.gui2.BugRenderer) (toReturn) ).setLeafIcon(icon);
            }
            return toReturn;
        }
    }
}
