/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.visitclass;
import edu.umd.cs.findbugs.visitclass.*;
import javax.annotation.CheckForNull;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.Attribute;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.CodeException;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.InnerClass;
import org.apache.bcel.classfile.InnerClasses;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LineNumber;
import org.apache.bcel.classfile.LineNumberTable;
import org.apache.bcel.classfile.Method;
public class Util extends java.lang.Object {
    public Util() {
    }
/**
     * Determine the outer class of obj.
     * 
     * @param obj
     * @return JavaClass for outer class, or null if obj is not an outer class
     * @throws ClassNotFoundException
     */
    public static org.apache.bcel.classfile.JavaClass getOuterClass(org.apache.bcel.classfile.JavaClass obj) throws java.lang.ClassNotFoundException {
        for (org.apache.bcel.classfile.Attribute a : obj.getAttributes())if (a instanceof org.apache.bcel.classfile.InnerClasses) {
            for (org.apache.bcel.classfile.InnerClass ic : ((org.apache.bcel.classfile.InnerClasses) (a) ).getInnerClasses()){
                if (obj.getClassNameIndex() == ic.getInnerClassIndex()) {
// System.out.println("Outer class is " +
// ic.getOuterClassIndex());
                    org.apache.bcel.classfile.ConstantClass oc = (org.apache.bcel.classfile.ConstantClass) (obj.getConstantPool().getConstant(ic.getOuterClassIndex())) ;
                    java.lang.String ocName = oc.getBytes(obj.getConstantPool());
                    return org.apache.bcel.Repository.lookupClass(ocName);
                }
            }
;
        }
;
        return null;
    }
    public static int getSizeOfSurroundingTryBlock(org.apache.bcel.classfile.Method method, java.lang.String vmNameOfExceptionClass, int pc) {
        if (method == null) return java.lang.Integer.MAX_VALUE;
        return getSizeOfSurroundingTryBlock(method.getConstantPool(),method.getCode(),vmNameOfExceptionClass,pc);
    }
    public static org.apache.bcel.classfile.CodeException getSurroundingTryBlock(org.apache.bcel.classfile.ConstantPool constantPool, org.apache.bcel.classfile.Code code, java.lang.String vmNameOfExceptionClass, int pc) {
        int size = java.lang.Integer.MAX_VALUE;
        if (code.getExceptionTable() == null) return null;
        org.apache.bcel.classfile.CodeException result = null;
        for (org.apache.bcel.classfile.CodeException catchBlock : code.getExceptionTable()){
            if (vmNameOfExceptionClass != null) {
                org.apache.bcel.classfile.Constant catchType = constantPool.getConstant(catchBlock.getCatchType());
                if (catchType == null || catchType instanceof org.apache.bcel.classfile.ConstantClass &&  !((org.apache.bcel.classfile.ConstantClass) (catchType) ).getBytes(constantPool).equals(vmNameOfExceptionClass)) continue;
            }
            int startPC = catchBlock.getStartPC();
            int endPC = catchBlock.getEndPC();
            if (pc >= startPC && pc <= endPC) {
                int thisSize = endPC - startPC;
                if (size > thisSize) {
                    size = thisSize;
                    result = catchBlock;
                }
            }
        }
;
        return result;
    }
    public static int getSizeOfSurroundingTryBlock(org.apache.bcel.classfile.ConstantPool constantPool, org.apache.bcel.classfile.Code code, java.lang.String vmNameOfExceptionClass, int pc) {
        int size = java.lang.Integer.MAX_VALUE;
        int tightStartPC = 0;
        int tightEndPC = java.lang.Integer.MAX_VALUE;
        if (code.getExceptionTable() == null) return size;
        for (org.apache.bcel.classfile.CodeException catchBlock : code.getExceptionTable()){
            if (vmNameOfExceptionClass != null) {
                org.apache.bcel.classfile.Constant catchType = constantPool.getConstant(catchBlock.getCatchType());
                if (catchType == null || catchType instanceof org.apache.bcel.classfile.ConstantClass &&  !((org.apache.bcel.classfile.ConstantClass) (catchType) ).getBytes(constantPool).equals(vmNameOfExceptionClass)) continue;
            }
            int startPC = catchBlock.getStartPC();
            int endPC = catchBlock.getEndPC();
            if (pc >= startPC && pc <= endPC) {
                int thisSize = endPC - startPC;
                if (size > thisSize) {
                    size = thisSize;
                    tightStartPC = startPC;
                    tightEndPC = endPC;
                }
            }
        }
;
        if (size == java.lang.Integer.MAX_VALUE) return size;
        size = (size + 7) / 8;
// try to guestimate number of lines that correspond
        org.apache.bcel.classfile.LineNumberTable lineNumberTable = code.getLineNumberTable();
        if (lineNumberTable == null) return size;
        int count = 0;
        for (org.apache.bcel.classfile.LineNumber line : lineNumberTable.getLineNumberTable()){
            if (line.getStartPC() > tightEndPC) break;
            if (line.getStartPC() >= tightStartPC) count++;
        }
;
        return count;
    }
}
