package edu.umd.cs.findbugs.classfile.analysis;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.analysis.*;
/**
 * ClassInfo represents important metadata about a loaded class, such as its
 * superclass, access flags, codebase entry, etc.
 * 
 * @author David Hovemeyer
 */
/* final */
/**
         * Mapping from one method signature to its bridge method signature
         */
/**
         * @return Returns the classDescriptor.
         */
/**
         * @param fieldDescriptorList
         *            The fieldDescriptorList to set.
         */
/**
         * @param methodDescriptorList
         *            The methodDescriptorList to set.
         */
/**
         * @param immediateEnclosingClass
         *            The immediateEnclosingClass to set.
         */
/**
     * 
     * @param classDescriptor
     *            ClassDescriptor representing the class name
     * @param superclassDescriptor
     *            ClassDescriptor representing the superclass name
     * @param interfaceDescriptorList
     *            ClassDescriptors representing implemented interface names
     * @param codeBaseEntry
     *            codebase entry class was loaded from
     * @param accessFlags
     *            class's access flags
     * @param referencedClassDescriptorList
     *            ClassDescriptors of all classes/interfaces referenced by the
     *            class
     * @param calledClassDescriptors
     *            TODO
     * @param fieldDescriptorList
     *            FieldDescriptors of fields defined in the class
     * @param methodInfoList
     *            MethodDescriptors of methods defined in the class
     * @param usesConcurrency
     *            TODO
     * @param hasStubs
     *            TODO
     */
/**
     * @return Returns the fieldDescriptorList.
     */
/**
     * @return Returns the methodDescriptorList.
     */
/**
     * @return Returns the methodDescriptorList.
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.XClass#findMethod(java.lang.String,
     * java.lang.String, boolean)
     */
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.XClass#findMethod(edu.umd.cs.findbugs.classfile
     * .MethodDescriptor)
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.XClass#findField(java.lang.String,
     * java.lang.String, boolean)
     */
/**
     * @return Returns the immediateEnclosingClass.
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#getPackageName()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#getPackageName()
     */
/**
     * Destructively add an annotation to the object. In general, this is not a
     * great idea, since it could cause the same class to appear to have
     * different annotations at different times. However, this method is
     * necessary for "built-in" annotations that FindBugs adds to system
     * classes. As long as we add such annotations early enough that nobody will
     * notice, we should be ok.
     * 
     * @param annotationValue
     *            an AnnotationValue to add to the class
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.XClass#getSourceSignature()
     */
abstract interface package-info {
}
