/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A resource creation point. This serves as an embodiment of the resource for
 * use with ResourceValueAnalysis.
 * 
 * @author David Hovemeyer
 * @see edu.umd.cs.findbugs.ba.ResourceValueAnalysis
 * @see ResourceTrackingDetector
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import edu.umd.cs.findbugs.ba.Location;
public class ResourceCreationPoint extends java.lang.Object {
/**
     * Location in the method where the resource is created.
     */
    final private edu.umd.cs.findbugs.ba.Location location;
/**
     * The type of the resource.
     */
    final private java.lang.String resourceClass;
/**
     * Constructor.
     * 
     * @param location
     *            location where resource is created
     * @param resourceClass
     *            the name of the resource's class
     */
    public ResourceCreationPoint(edu.umd.cs.findbugs.ba.Location location, java.lang.String resourceClass) {
        super();
        this.location = location;
        this.resourceClass = resourceClass;
    }
/**
     * Get the location where the resource is created.
     */
    public edu.umd.cs.findbugs.ba.Location getLocation() {
        return location;
    }
/**
     * Get the name of the resource's class.
     */
    public java.lang.String getResourceClass() {
        return resourceClass;
    }
}
// vim:ts=4
