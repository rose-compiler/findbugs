/*
 * Machine Learning support for FindBugs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Split a camel case identifier into individual words.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.util;
import edu.umd.cs.findbugs.util.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
public class SplitCamelCaseIdentifier extends java.lang.Object {
    private java.lang.String ident;
/**
     * Constructor.
     * 
     * @param ident
     *            the identifier to split into words
     */
    public SplitCamelCaseIdentifier(java.lang.String ident) {
        super();
        this.ident = ident;
    }
/**
     * Split the identifier into words.
     * 
     * @return Collection of words in the identifier
     */
    public java.util.Collection<java.lang.String> split() {
        java.lang.String s = ident;
        java.util.Set<java.lang.String> result = new java.util.HashSet<java.lang.String>();
        while (s.length() > 0) {
            java.lang.StringBuilder buf = new java.lang.StringBuilder();
            char first = s.charAt(0);
            buf.append(first);
            int i = 1;
            if (s.length() > 1) {
                boolean camelWord;
                if (java.lang.Character.isLowerCase(first)) {
                    camelWord = true;
                }
                else {
                    char next = s.charAt(i++);
                    buf.append(next);
                    camelWord = java.lang.Character.isLowerCase(next);
                }
                while (i < s.length()) {
                    char c = s.charAt(i);
                    if (java.lang.Character.isUpperCase(c)) {
                        if (camelWord) break;
                    }
                    else if ( !camelWord) {
                        break;
                    }
                    buf.append(c);
                    ++i;
                }
                if ( !camelWord && i < s.length()) {
                    buf.deleteCharAt(buf.length() - 1);
                    --i;
                }
            }
            result.add(buf.toString().toLowerCase(java.util.Locale.US));
            s = s.substring(i);
        }
        return result;
    }
}
