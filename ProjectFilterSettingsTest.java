/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.config;
import edu.umd.cs.findbugs.config.*;
import junit.framework.Assert;
import junit.framework.TestCase;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.Priorities;
public class ProjectFilterSettingsTest extends junit.framework.TestCase {
    public ProjectFilterSettingsTest() {
    }
    edu.umd.cs.findbugs.config.ProjectFilterSettings plain;
    edu.umd.cs.findbugs.config.ProjectFilterSettings otherPlain;
    edu.umd.cs.findbugs.config.ProjectFilterSettings changed;
    edu.umd.cs.findbugs.config.ProjectFilterSettings changed2;
    edu.umd.cs.findbugs.config.ProjectFilterSettings changed3;
    edu.umd.cs.findbugs.config.ProjectFilterSettings changed4;
    protected void setUp() {
        plain = edu.umd.cs.findbugs.config.ProjectFilterSettings.createDefault();
        otherPlain = edu.umd.cs.findbugs.config.ProjectFilterSettings.createDefault();
        changed = edu.umd.cs.findbugs.config.ProjectFilterSettings.createDefault();
        changed.setMinPriority("High");
        changed2 = edu.umd.cs.findbugs.config.ProjectFilterSettings.createDefault();
        changed2.removeCategory("MALICIOUS_CODE");
        changed3 = edu.umd.cs.findbugs.config.ProjectFilterSettings.createDefault();
        changed3.addCategory("FAKE_CATEGORY");
        changed4 = edu.umd.cs.findbugs.config.ProjectFilterSettings.createDefault();
        changed4.setMinPriority("High");
        changed4.removeCategory("MALICIOUS_CODE");
        changed4.addCategory("FAKE_CATEGORY");
    }
    public void testPlainPrio() {
        junit.framework.Assert.assertTrue(plain.getMinPriority().equals(edu.umd.cs.findbugs.config.ProjectFilterSettings.DEFAULT_PRIORITY));
    }
    public void testPlainCategories() {
        int count = 0;
        for (java.lang.String category : edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getBugCategories())if ( !category.equals("NOISE")) {
            junit.framework.Assert.assertTrue(plain.containsCategory(category));
            ++count;
        }
;
        junit.framework.Assert.assertEquals(count,plain.getActiveCategorySet().size());
    }
    public void testAddCategory() {
        junit.framework.Assert.assertTrue(plain.containsCategory("FAKE_CATEGORY"));
        plain.addCategory("FAKE_CATEGORY");
        junit.framework.Assert.assertTrue(plain.containsCategory("FAKE_CATEGORY"));
    }
// unknown
// categories
// should be
// unhidden
// by
// default
    public void testRemoveCategory() {
        junit.framework.Assert.assertTrue(plain.containsCategory("MALICIOUS_CODE"));
        plain.removeCategory("MALICIOUS_CODE");
        junit.framework.Assert.assertFalse(plain.containsCategory("MALICIOUS_CODE"));
    }
    public void testSetMinPriority() {
        plain.setMinPriority("High");
        junit.framework.Assert.assertTrue(plain.getMinPriority().equals("High"));
        junit.framework.Assert.assertTrue(plain.getMinPriorityAsInt() == edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY);
        plain.setMinPriority("Medium");
        junit.framework.Assert.assertTrue(plain.getMinPriority().equals("Medium"));
        junit.framework.Assert.assertTrue(plain.getMinPriorityAsInt() == edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY);
        plain.setMinPriority("Low");
        junit.framework.Assert.assertTrue(plain.getMinPriority().equals("Low"));
        junit.framework.Assert.assertTrue(plain.getMinPriorityAsInt() == edu.umd.cs.findbugs.Priorities.LOW_PRIORITY);
    }
    public void testEquals() {
        junit.framework.Assert.assertEquals(plain,otherPlain);
        junit.framework.Assert.assertFalse(plain.equals(changed));
        junit.framework.Assert.assertFalse(changed.equals(plain));
        junit.framework.Assert.assertFalse(plain.equals(changed2));
        junit.framework.Assert.assertFalse(changed2.equals(plain));
        junit.framework.Assert.assertTrue(plain.equals(changed3));
        junit.framework.Assert.assertTrue(changed3.equals(plain));
        junit.framework.Assert.assertFalse(plain.equals(changed4));
        junit.framework.Assert.assertFalse(changed4.equals(plain));
    }
// The activeBugCategorySet doesn't matter for equals(), only
// the hiddenBugCategorySet does (along with minPriority and
// displayFalseWarnings) so 'plain' and 'changed3' should test equal.
    public void testEncodeDecode() {
        edu.umd.cs.findbugs.config.ProjectFilterSettings copyOfPlain = edu.umd.cs.findbugs.config.ProjectFilterSettings.fromEncodedString(plain.toEncodedString());
        edu.umd.cs.findbugs.config.ProjectFilterSettings.hiddenFromEncodedString(copyOfPlain,plain.hiddenToEncodedString());
        junit.framework.Assert.assertEquals(plain,copyOfPlain);
        edu.umd.cs.findbugs.config.ProjectFilterSettings copyOfChanged4 = edu.umd.cs.findbugs.config.ProjectFilterSettings.fromEncodedString(changed4.toEncodedString());
        edu.umd.cs.findbugs.config.ProjectFilterSettings.hiddenFromEncodedString(copyOfChanged4,changed4.hiddenToEncodedString());
        junit.framework.Assert.assertEquals(changed4,copyOfChanged4);
    }
    public void testDisplayFalseWarnings() {
        junit.framework.Assert.assertEquals(plain,otherPlain);
        junit.framework.Assert.assertFalse(plain.displayFalseWarnings());
        plain.setDisplayFalseWarnings(true);
        junit.framework.Assert.assertFalse(plain.equals(otherPlain));
        edu.umd.cs.findbugs.config.ProjectFilterSettings copyOfPlain = edu.umd.cs.findbugs.config.ProjectFilterSettings.fromEncodedString(plain.toEncodedString());
        junit.framework.Assert.assertTrue(copyOfPlain.displayFalseWarnings());
        junit.framework.Assert.assertEquals(copyOfPlain,plain);
        junit.framework.Assert.assertEquals(plain,copyOfPlain);
    }
}
// vim:ts=4
