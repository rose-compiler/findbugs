/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004-2008 University of Maryland
 * Copyright (C) 2008 Google
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.Iterator;
import java.util.Set;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKEINTERFACE;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InvokeInstruction;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.Priorities;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Hierarchy2;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.type.TypeFrame;
public class CallToUnsupportedMethod extends java.lang.Object implements edu.umd.cs.findbugs.Detector {
    edu.umd.cs.findbugs.BugReporter bugReporter;
    public CallToUnsupportedMethod(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.JavaClass javaClass = classContext.getJavaClass();
        org.apache.bcel.classfile.Method[] methodList = javaClass.getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.MethodUnprofitableException e){
// move along; nothing to see
                assert true;
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                java.lang.String msg = "Detector " + this.getClass().getName() + " caught exception while analyzing " + javaClass.getClassName() + "." + method.getName() + " : " + method.getSignature();
                bugReporter.logError(msg,e);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                java.lang.String msg = "Detector " + this.getClass().getName() + " caught exception while analyzing " + javaClass.getClassName() + "." + method.getName() + " : " + method.getSignature();
                bugReporter.logError(msg,e);
            }
        }
;
    }
/**
     * @param classContext
     * @param method
     */
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException, edu.umd.cs.findbugs.ba.MethodUnprofitableException {
        if (method.isSynthetic() || (method.getAccessFlags() & org.apache.bcel.Constants.ACC_BRIDGE) == org.apache.bcel.Constants.ACC_BRIDGE) return;
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = classContext.getTypeDataflow(method);
        org.apache.bcel.generic.ConstantPoolGen constantPoolGen = classContext.getConstantPoolGen();
        locationLoop:for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> iter = cfg.locationIterator(); iter.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = iter.next();
            org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.InvokeInstruction)) continue;
            if (ins instanceof org.apache.bcel.generic.INVOKEINTERFACE) continue;
            org.apache.bcel.generic.InvokeInstruction inv = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
            edu.umd.cs.findbugs.ba.type.TypeFrame frame = typeDataflow.getFactAtLocation(location);
            java.lang.String methodName = inv.getMethodName(constantPoolGen);
            if (methodName.toLowerCase().indexOf("unsupported") >= 0) continue;
            java.lang.String methodSig = inv.getSignature(constantPoolGen);
            if (methodSig.equals("()Ljava/lang/UnsupportedOperationException;")) continue;
            java.util.Set<edu.umd.cs.findbugs.ba.XMethod> targets;
            try {
                targets = edu.umd.cs.findbugs.ba.Hierarchy2.resolveMethodCallTargets(inv,frame,constantPoolGen);
            }
            catch (java.lang.ClassNotFoundException e){
                edu.umd.cs.findbugs.ba.AnalysisContext.reportMissingClass(e);
                continue locationLoop;
            }
            if (targets.isEmpty()) continue locationLoop;
            int priority = targets.size() == 1 ? edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY : edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY;
            for (edu.umd.cs.findbugs.ba.XMethod m : targets){
                if ( !m.isUnsupported()) continue locationLoop;
                edu.umd.cs.findbugs.ba.XClass xc = edu.umd.cs.findbugs.ba.AnalysisContext.currentXFactory().getXClass(m.getClassDescriptor());
                if ( !(inv instanceof org.apache.bcel.generic.INVOKESTATIC) &&  !(m.isFinal() || xc.isFinal())) priority = edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY;
                if (xc == null || xc.isAbstract()) {
                    try {
                        if ( !edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getSubtypes2().hasSubtypes(m.getClassDescriptor())) continue locationLoop;
                    }
                    catch (java.lang.ClassNotFoundException e){
                        edu.umd.cs.findbugs.ba.AnalysisContext.reportMissingClass(e);
                        continue locationLoop;
                    }
                }
            }
;
            edu.umd.cs.findbugs.BugInstance bug = new edu.umd.cs.findbugs.BugInstance(this, "DMI_UNSUPPORTED_METHOD", priority).addClassAndMethod(classContext.getJavaClass(),method).addCalledMethod(constantPoolGen,inv).addSourceLine(classContext,method,location);
            bugReporter.reportBug(bug);
        }
    }
// Only consider invoke instructions
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.Detector#report()
     */
    public void report() {
    }
}
// TODO Auto-generated method stub
