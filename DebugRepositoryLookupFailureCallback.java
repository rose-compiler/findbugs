/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * DebugRepositoryLookupFailureCallback implementation for debugging. (Test
 * drivers, etc.) It just prints a message and exits.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import edu.umd.cs.findbugs.AbstractBugReporter;
import edu.umd.cs.findbugs.annotations.SuppressWarnings;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class DebugRepositoryLookupFailureCallback extends java.lang.Object implements edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback {
    public DebugRepositoryLookupFailureCallback() {
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback#reportMissingClass
     * (java.lang.ClassNotFoundException)
     */
    public void reportMissingClass(java.lang.ClassNotFoundException ex) {
        java.lang.String missing = edu.umd.cs.findbugs.AbstractBugReporter.getMissingClassName(ex);
        if (missing == null || missing.charAt(0) == '\u005b') return;
        java.lang.System.out.println("Missing class");
        ex.printStackTrace();
        java.lang.System.exit(1);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IErrorLogger#reportMissingClass(edu.umd
     * .cs.findbugs.classfile.ClassDescriptor)
     */
    public void reportMissingClass(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor) {
        java.lang.System.out.println("Missing class: " + classDescriptor);
        java.lang.System.exit(1);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback#logError(java.
     * lang.String)
     */
    public void logError(java.lang.String message) {
        java.lang.System.err.println("Error: " + message);
        java.lang.System.exit(1);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback#logError(java.
     * lang.String, java.lang.Throwable)
     */
    public void logError(java.lang.String message, java.lang.Throwable e) {
        if (e instanceof edu.umd.cs.findbugs.ba.MissingClassException) {
            edu.umd.cs.findbugs.ba.MissingClassException missingClassEx = (edu.umd.cs.findbugs.ba.MissingClassException) (e) ;
            java.lang.ClassNotFoundException cnfe = missingClassEx.getClassNotFoundException();
            this.reportMissingClass(cnfe);
// Don't report dataflow analysis exceptions due to missing classes.
// Too much noise.
            return;
        }
        if (e instanceof edu.umd.cs.findbugs.ba.MethodUnprofitableException) {
// TODO: log this
            return;
        }
        java.lang.System.err.println("Error: " + message);
        e.printStackTrace();
        java.lang.System.exit(1);
    }
/**
     * Report that we skipped some analysis of a method
     * 
     * @param method
     */
    public void reportSkippedAnalysis(edu.umd.cs.findbugs.classfile.MethodDescriptor method) {
        java.lang.System.err.println("Skipping " + method);
    }
}
