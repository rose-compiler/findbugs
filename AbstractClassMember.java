/*
 * Bytecode Analysis Framework
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import org.apache.bcel.Constants;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
abstract public class AbstractClassMember extends java.lang.Object implements edu.umd.cs.findbugs.ba.ClassMember {
    final private java.lang.String className;
    final private java.lang.String name;
    final private java.lang.String signature;
    final private int accessFlags;
    private boolean resolved;
    private int cachedHashCode = 0;
    static int slashCountClass = 0;
    static int dottedCountClass = 0;
    static int slashCountSignature = 0;
    static int dottedCountSignature = 0;
    public AbstractClassMember(java.lang.String className, java.lang.String name, java.lang.String signature, int accessFlags) {
        super();
        if (className.indexOf('\u002e') >= 0) {
            dottedCountClass++;
        }
        else if (className.indexOf('\u002f') >= 0) {
            assert false;
            slashCountClass++;
            className = className.replace('\u002f','\u002e');
        }
        if (signature.indexOf('\u002e') >= 0) {
            assert false;
            signature = signature.replace('\u002e','\u002f');
            dottedCountSignature++;
        }
        else if (signature.indexOf('\u002f') >= 0) slashCountSignature++;
        this.className = edu.umd.cs.findbugs.classfile.DescriptorFactory.canonicalizeString(className);
        this.name = edu.umd.cs.findbugs.classfile.DescriptorFactory.canonicalizeString(name);
        this.signature = edu.umd.cs.findbugs.classfile.DescriptorFactory.canonicalizeString(signature);
        this.accessFlags = accessFlags;
    }
    public java.lang.String getClassName() {
        return className;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#getClassDescriptor()
     */
    public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() {
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getClassDescriptorForDottedClassName(className);
    }
    public java.lang.String getName() {
        return name;
    }
    public java.lang.String getPackageName() {
        int lastDot = className.lastIndexOf('\u002e');
        if (lastDot ==  -1) return className;
        return className.substring(0,lastDot);
    }
    public java.lang.String getSignature() {
        return signature;
    }
    public boolean isReferenceType() {
        return signature.startsWith("L") || signature.startsWith("[");
    }
    public int getAccessFlags() {
        return accessFlags;
    }
    public boolean isStatic() {
        return (accessFlags & org.apache.bcel.Constants.ACC_STATIC) != 0;
    }
    public boolean isFinal() {
        return (accessFlags & org.apache.bcel.Constants.ACC_FINAL) != 0;
    }
    public boolean isPublic() {
        return (accessFlags & org.apache.bcel.Constants.ACC_PUBLIC) != 0;
    }
    public boolean isProtected() {
        return (accessFlags & org.apache.bcel.Constants.ACC_PROTECTED) != 0;
    }
    public boolean isPrivate() {
        return (accessFlags & org.apache.bcel.Constants.ACC_PRIVATE) != 0;
    }
// public int compareTo(ClassMember other) {
// // This may be compared to any kind of PackageMember object.
// // If the other object is a different kind of field,
// // just compare class names.
// if (this.getClass() != other.getClass())
// return this.getClass().getName().compareTo(other.getClass().getName());
//
// int cmp;
// cmp = className.compareTo(other.getClassName());
// if (cmp != 0)
// return cmp;
// cmp = name.compareTo(other.getName());
// if (cmp != 0)
// return cmp;
// return signature.compareTo(other.getSignature());
// }
// public int compareTo(FieldOrMethodName other) {
//
// int cmp = getClassDescriptor().compareTo(other.getClassDescriptor());
// if (cmp != 0)
// return cmp;
// cmp = name.compareTo(other.getName());
// if (cmp != 0)
// return cmp;
// cmp = signature.compareTo(other.getSignature());
// if (cmp != 0)
// return cmp;
// return (this.isStatic() ? 1 : 0) - (other.isStatic() ? 1 : 0);
// }
// public int compareTo(Object other) {
// return compareTo((FieldOrMethodName) other);
// }
    public boolean isResolved() {
        return resolved;
    }
    void markAsResolved() {
        resolved = true;
    }
    public int hashCode() {
        if (cachedHashCode == 0) {
            cachedHashCode = className.hashCode() ^ name.hashCode() ^ signature.hashCode();
        }
        return cachedHashCode;
    }
    public boolean equals(java.lang.Object o) {
        if (o == null || this.getClass() != o.getClass()) return false;
        edu.umd.cs.findbugs.ba.AbstractClassMember other = (edu.umd.cs.findbugs.ba.AbstractClassMember) (o) ;
        return className.equals(other.className) && name.equals(other.name) && signature.equals(other.signature);
    }
    public java.lang.String toString() {
        return className + "." + name;
    }
}
