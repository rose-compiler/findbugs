/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.util.Arrays;
import junit.framework.Assert;
import junit.framework.TestCase;
public class MethodHashTest extends junit.framework.TestCase {
    public MethodHashTest() {
    }
    byte[] hash;
    java.lang.String s;
    byte[] sameHash;
    byte[] greaterHash;
    byte[] lesserHash;
    byte[] shorterHash;
    byte[] longerHash;
/*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws java.lang.Exception {
        hash = new byte[]{0x06, 0x04, (byte) (0xDE) , (byte) (0xAD) , (byte) (0xBE) , (byte) (0xEF) };
        s = "0604deadbeef";
        sameHash = new byte[]{0x06, 0x04, (byte) (0xDE) , (byte) (0xAD) , (byte) (0xBE) , (byte) (0xEF) };
        greaterHash = new byte[]{0x06, 0x05, (byte) (0xDE) , (byte) (0xAD) , (byte) (0xBE) , (byte) (0xEF) };
        lesserHash = new byte[]{0x06, 0x03, (byte) (0xDE) , (byte) (0xAD) , (byte) (0xBE) , (byte) (0xEF) };
        shorterHash = new byte[]{0x06, 0x04, (byte) (0xDE) , (byte) (0xAD) , (byte) (0xBE) };
        longerHash = new byte[]{0x06, 0x04, (byte) (0xDE) , (byte) (0xAD) , (byte) (0xBE) , (byte) (0xEF) , (byte) (0x01) };
    }
    public void testHashToString() {
        java.lang.String s2 = edu.umd.cs.findbugs.ba.ClassHash.hashToString(hash);
        junit.framework.Assert.assertEquals(s,s2);
    }
    public void testStringToHash() {
        byte[] hash2 = edu.umd.cs.findbugs.ba.ClassHash.stringToHash(s);
        junit.framework.Assert.assertTrue(java.util.Arrays.equals(hash,hash2));
    }
    public void testSame() {
        junit.framework.Assert.assertTrue(edu.umd.cs.findbugs.ba.MethodHash.compareHashes(hash,sameHash) == 0);
        junit.framework.Assert.assertTrue(edu.umd.cs.findbugs.ba.MethodHash.compareHashes(sameHash,hash) == 0);
    }
    public void testGreater() {
        junit.framework.Assert.assertTrue(edu.umd.cs.findbugs.ba.MethodHash.compareHashes(hash,greaterHash) < 0);
    }
    public void testLesser() {
        junit.framework.Assert.assertTrue(edu.umd.cs.findbugs.ba.MethodHash.compareHashes(hash,lesserHash) > 0);
    }
    public void testShorter() {
        junit.framework.Assert.assertTrue(edu.umd.cs.findbugs.ba.MethodHash.compareHashes(hash,shorterHash) > 0);
    }
    public void testLonger() {
        junit.framework.Assert.assertTrue(edu.umd.cs.findbugs.ba.MethodHash.compareHashes(hash,longerHash) < 0);
    }
}
