/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.IOException;
import java.util.Date;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.ba.SourceFile;
import edu.umd.cs.findbugs.ba.SourceFinder;
import edu.umd.cs.findbugs.cloud.Cloud;
public class BackdateHistoryUsingSource extends java.lang.Object {
    public BackdateHistoryUsingSource() {
    }
    final private static java.lang.String USAGE = "Usage: <cmd>   <bugs.xml> [<out.xml>]";
    public static void main(java.lang.String[] args) throws org.dom4j.DocumentException, java.io.IOException {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        if (args.length < 1 || args.length > 2) {
            java.lang.System.out.println(USAGE);
            return;
        }
        edu.umd.cs.findbugs.BugCollection origCollection;
        origCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        origCollection.readXML(args[0]);
        edu.umd.cs.findbugs.ba.SourceFinder sourceFinder = new edu.umd.cs.findbugs.ba.SourceFinder(origCollection.getProject());
        for (edu.umd.cs.findbugs.BugInstance b : origCollection){
            edu.umd.cs.findbugs.SourceLineAnnotation s = b.getPrimarySourceLineAnnotation();
            if ( !s.isSourceFileKnown()) continue;
            if ( !sourceFinder.hasSourceFile(s)) continue;
            edu.umd.cs.findbugs.ba.SourceFile sourceFile = sourceFinder.findSourceFile(s);
            long when = sourceFile.getLastModified();
            if (when > 0) {
                java.util.Date firstSeen = new java.util.Date(when);
                b.getXmlProps().setFirstSeen(firstSeen);
                java.lang.System.out.println("Set first seen to " + firstSeen);
            }
        }
;
        edu.umd.cs.findbugs.cloud.Cloud cloud = origCollection.getCloud();
        cloud.bugsPopulated();
        if (cloud.getSigninState() != edu.umd.cs.findbugs.cloud.Cloud.SigninState.SIGNED_IN && cloud.getSigninState() != edu.umd.cs.findbugs.cloud.Cloud.SigninState.NO_SIGNIN_REQUIRED) {
            cloud.signIn();
            if (cloud.getSigninState() != edu.umd.cs.findbugs.cloud.Cloud.SigninState.SIGNED_IN && cloud.getSigninState() != edu.umd.cs.findbugs.cloud.Cloud.SigninState.NO_SIGNIN_REQUIRED) {
                throw new java.lang.IllegalStateException("Unable to sign in; state : " + cloud.getSigninState());
            }
        }
        cloud.waitUntilIssueDataDownloaded();
        if (args.length > 1) origCollection.writeXML(args[1]);
        cloud.waitUntilNewIssuesUploaded();
        cloud.shutdown();
    }
}
