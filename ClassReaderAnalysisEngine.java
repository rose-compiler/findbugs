/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce an ASM ClassReader for a class.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.asm;
import edu.umd.cs.findbugs.classfile.engine.asm.*;
import edu.umd.cs.findbugs.asm.FBClassReader;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.RecomputableClassAnalysisEngine;
import edu.umd.cs.findbugs.classfile.analysis.ClassData;
public class ClassReaderAnalysisEngine extends edu.umd.cs.findbugs.classfile.RecomputableClassAnalysisEngine<edu.umd.cs.findbugs.asm.FBClassReader> {
    public ClassReaderAnalysisEngine() {
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.asm.FBClassReader analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.ClassDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        edu.umd.cs.findbugs.classfile.analysis.ClassData classData = analysisCache.getClassAnalysis(edu.umd.cs.findbugs.classfile.analysis.ClassData.class,descriptor);
        edu.umd.cs.findbugs.asm.FBClassReader classReader = new edu.umd.cs.findbugs.asm.FBClassReader(classData.getData());
        return classReader;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#registerWith(edu.umd.cs
     * .findbugs.classfile.IAnalysisCache)
     */
    public void registerWith(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache) {
        analysisCache.registerClassAnalysisEngine(edu.umd.cs.findbugs.asm.FBClassReader.class,this);
    }
}
