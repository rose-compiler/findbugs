package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.io.File;
import java.util.Iterator;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.BugAccumulator;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.SystemProperties;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.ba.constant.Constant;
import edu.umd.cs.findbugs.ba.constant.ConstantDataflow;
import edu.umd.cs.findbugs.ba.constant.ConstantFrame;
public class DumbMethodInvocations extends java.lang.Object implements edu.umd.cs.findbugs.Detector {
    final private edu.umd.cs.findbugs.BugReporter bugReporter;
    final private edu.umd.cs.findbugs.BugAccumulator bugAccumulator;
    public DumbMethodInvocations(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
        this.bugAccumulator = new edu.umd.cs.findbugs.BugAccumulator(bugReporter);
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.Method[] methodList = classContext.getJavaClass().getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
            try {
                this.analyzeMethod(classContext,method);
                bugAccumulator.reportAccumulatedBugs();
            }
            catch (edu.umd.cs.findbugs.ba.MethodUnprofitableException mue){
// otherwise
                if (edu.umd.cs.findbugs.SystemProperties.getBoolean("unprofitable.debug")) bugReporter.logError("skipping unprofitable method in " + this.getClass().getName());
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("Detector " + this.getClass().getName() + " caught exception",e);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("Detector " + this.getClass().getName() + " caught exception",e);
            }
        }
;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.constant.ConstantDataflow constantDataflow = classContext.getConstantDataflow(method);
        org.apache.bcel.generic.ConstantPoolGen cpg = classContext.getConstantPoolGen();
        org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
        java.lang.String sourceFile = classContext.getJavaClass().getSourceFileName();
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.InvokeInstruction)) continue;
            org.apache.bcel.generic.InvokeInstruction iins = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
            edu.umd.cs.findbugs.ba.constant.ConstantFrame frame = constantDataflow.getFactAtLocation(location);
            if ( !frame.isValid()) {
                continue;
            }
            if (iins.getName(cpg).equals("getConnection") && iins.getSignature(cpg).equals("(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/sql/Connection;") && iins.getClassName(cpg).equals("java.sql.DriverManager")) {
                edu.umd.cs.findbugs.ba.constant.Constant operandValue = frame.getTopValue();
                if (operandValue.isConstantString()) {
                    java.lang.String password = operandValue.getConstantString();
                    if (password.length() == 0) bugAccumulator.accumulateBug(new edu.umd.cs.findbugs.BugInstance(this, "DMI_EMPTY_DB_PASSWORD", NORMAL_PRIORITY).addClassAndMethod(methodGen,sourceFile),classContext,methodGen,sourceFile,location);
                    else bugAccumulator.accumulateBug(new edu.umd.cs.findbugs.BugInstance(this, "DMI_CONSTANT_DB_PASSWORD", NORMAL_PRIORITY).addClassAndMethod(methodGen,sourceFile),classContext,methodGen,sourceFile,location);
                }
            }
            if (iins.getName(cpg).equals("substring") && iins.getSignature(cpg).equals("(I)Ljava/lang/String;") && iins.getClassName(cpg).equals("java.lang.String")) {
                edu.umd.cs.findbugs.ba.constant.Constant operandValue = frame.getTopValue();
                if ( !operandValue.isConstantInteger()) continue;
                int v = operandValue.getConstantInt();
                if (v == 0) bugAccumulator.accumulateBug(new edu.umd.cs.findbugs.BugInstance(this, "DMI_USELESS_SUBSTRING", NORMAL_PRIORITY).addClassAndMethod(methodGen,sourceFile),classContext,methodGen,sourceFile,location);
            }
            else if (iins.getName(cpg).equals("<init>") && iins.getSignature(cpg).equals("(Ljava/lang/String;)V") && iins.getClassName(cpg).equals("java.io.File")) {
                edu.umd.cs.findbugs.ba.constant.Constant operandValue = frame.getTopValue();
                if ( !operandValue.isConstantString()) continue;
                java.lang.String v = operandValue.getConstantString();
                if (this.isAbsoluteFileName(v) &&  !v.startsWith("/etc/") &&  !v.startsWith("/dev/") &&  !v.startsWith("/proc")) {
                    int priority = NORMAL_PRIORITY;
                    if (v.startsWith("/tmp")) priority = LOW_PRIORITY;
                    else if (v.indexOf("/home") >= 0) priority = HIGH_PRIORITY;
                    bugAccumulator.accumulateBug(new edu.umd.cs.findbugs.BugInstance(this, "DMI_HARDCODED_ABSOLUTE_FILENAME", priority).addClassAndMethod(methodGen,sourceFile).addString(v).describe("FILE_NAME"),classContext,methodGen,sourceFile,location);
                }
            }
        }
    }
// This basic block is probably dead
    private boolean isAbsoluteFileName(java.lang.String v) {
        if (v.startsWith("/dev/")) return false;
        if (v.startsWith("/")) return true;
        if (v.startsWith("C:")) return true;
        if (v.startsWith("c:")) return true;
        try {
            java.io.File f = new java.io.File(v);
            return f.isAbsolute();
        }
        catch (java.lang.RuntimeException e){
            return false;
        }
    }
    public void report() {
    }
}
