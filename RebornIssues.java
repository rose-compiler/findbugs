/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Mine historical information from a BugCollection. The BugCollection should be
 * built using UpdateBugCollection to record the history of analyzing all
 * versions over time.
 * 
 * @author David Hovemeyer
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.SortedBugCollection;
public class RebornIssues extends java.lang.Object {
    static class CommandLine extends edu.umd.cs.findbugs.config.CommandLine {
        public CommandLine() {
        }
        public void handleOption(java.lang.String option, java.lang.String optionalExtraPart) {
            throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
        public void handleOptionWithArgument(java.lang.String option, java.lang.String argument) {
            throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
    }
    edu.umd.cs.findbugs.BugCollection bugCollection;
    public RebornIssues() {
        super();
    }
    public RebornIssues(edu.umd.cs.findbugs.BugCollection bugCollection) {
        super();
        this.bugCollection = bugCollection;
    }
    public void setBugCollection(edu.umd.cs.findbugs.BugCollection bugCollection) {
        this.bugCollection = bugCollection;
    }
    public edu.umd.cs.findbugs.workflow.RebornIssues execute() {
        java.util.Map<java.lang.String, java.util.List<edu.umd.cs.findbugs.BugInstance>> map = new java.util.HashMap<java.lang.String, java.util.List<edu.umd.cs.findbugs.BugInstance>>();
        for (edu.umd.cs.findbugs.BugInstance b : bugCollection.getCollection())if (b.getFirstVersion() != 0 || b.getLastVersion() !=  -1) {
            java.util.List<edu.umd.cs.findbugs.BugInstance> lst = map.get(b.getInstanceHash());
            if (lst == null) {
                lst = new java.util.LinkedList<edu.umd.cs.findbugs.BugInstance>();
                map.put(b.getInstanceHash(),lst);
            }
            lst.add(b);
        }
;
        for (java.util.List<edu.umd.cs.findbugs.BugInstance> lst : map.values()){
            if (lst.size() > 1) {
                java.util.TreeSet<java.lang.Long> removalTimes = new java.util.TreeSet<java.lang.Long>();
                java.util.TreeSet<java.lang.Long> additionTimes = new java.util.TreeSet<java.lang.Long>();
                java.lang.String bugPattern = "XXX";
                for (edu.umd.cs.findbugs.BugInstance b : lst){
                    bugPattern = b.getBugPattern().getType();
                    if (b.getFirstVersion() > 0) additionTimes.add(b.getFirstVersion());
                    if (b.getLastVersion() !=  -1) removalTimes.add(b.getLastVersion());
                }
;
                java.util.Iterator<java.lang.Long> aI = additionTimes.iterator();
                if ( !aI.hasNext()) continue;
                long a = aI.next();
                loop:for (java.lang.Long removed : removalTimes){
                    while (a <= removed) {
                        if ( !aI.hasNext()) break loop;
                        a = aI.next();
                    }
                    java.lang.System.out.printf("%5d %5d %s%n",removed,a,bugPattern);
                }
;
            }
        }
;
        return this;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
// load plugins
        edu.umd.cs.findbugs.workflow.RebornIssues reborn = new edu.umd.cs.findbugs.workflow.RebornIssues();
        edu.umd.cs.findbugs.workflow.RebornIssues.CommandLine commandLine = new edu.umd.cs.findbugs.workflow.RebornIssues.CommandLine();
        int argCount = commandLine.parse(args,0,2,"Usage: " + edu.umd.cs.findbugs.workflow.RebornIssues.class.getName() + " [options] [<xml results> [<history]] ");
        edu.umd.cs.findbugs.SortedBugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (argCount < args.length) bugCollection.readXML(args[argCount++]);
        else bugCollection.readXML(java.lang.System.in);
        reborn.setBugCollection(bugCollection);
        reborn.execute();
    }
}
