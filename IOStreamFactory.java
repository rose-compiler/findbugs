/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A StreamFactory for normal java.io streams that are created using NEW
 * instructions.
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import org.apache.bcel.Constants;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.ObjectType;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.ObjectTypeFactory;
import edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback;
public class IOStreamFactory extends java.lang.Object implements edu.umd.cs.findbugs.detect.StreamFactory {
    private org.apache.bcel.generic.ObjectType baseClassType;
    private org.apache.bcel.generic.ObjectType[] uninterestingSubclassTypeList;
    private java.lang.String bugType;
    public IOStreamFactory(java.lang.String baseClass, java.lang.String[] uninterestingSubclassList, java.lang.String bugType) {
        super();
        this.baseClassType = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance(baseClass);
        this.uninterestingSubclassTypeList = new org.apache.bcel.generic.ObjectType[uninterestingSubclassList.length];
        for (int i = 0; i < uninterestingSubclassList.length; ++i) {
            this.uninterestingSubclassTypeList[i] = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance(uninterestingSubclassList[i]);
        }
        this.bugType = bugType;
    }
    public edu.umd.cs.findbugs.detect.Stream createStream(edu.umd.cs.findbugs.ba.Location location, org.apache.bcel.generic.ObjectType type, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback lookupFailureCallback) {
        try {
            org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
            if (ins.getOpcode() != org.apache.bcel.Constants.NEW) return null;
            if (edu.umd.cs.findbugs.ba.Hierarchy.isSubtype(type,baseClassType)) {
                boolean isUninteresting = false;
                for (org.apache.bcel.generic.ObjectType aUninterestingSubclassTypeList : uninterestingSubclassTypeList){
                    if (edu.umd.cs.findbugs.ba.Hierarchy.isSubtype(type,aUninterestingSubclassTypeList)) {
                        isUninteresting = true;
                        break;
                    }
                }
;
                edu.umd.cs.findbugs.detect.Stream result = new edu.umd.cs.findbugs.detect.Stream(location, type.getClassName(), baseClassType.getClassName()).setIgnoreImplicitExceptions(true);
                if ( !isUninteresting) result.setInteresting(bugType);
                return result;
            }
        }
        catch (java.lang.ClassNotFoundException e){
            lookupFailureCallback.reportMissingClass(e);
        }
        return null;
    }
}
// vim:ts=3
