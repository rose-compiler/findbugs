/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Special ReferenceType representing the type of a caught exception. Keeps
 * track of the entire set of exceptions that can be caught, and whether they
 * are explicit or implicit.
 */
package edu.umd.cs.findbugs.ba.type;
import edu.umd.cs.findbugs.ba.type.*;
import org.apache.bcel.Constants;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;
public class ExceptionObjectType extends org.apache.bcel.generic.ObjectType implements org.apache.bcel.Constants, edu.umd.cs.findbugs.ba.type.ExtendedTypes {
/**
     *
     */
    final private static long serialVersionUID = 1L;
    private edu.umd.cs.findbugs.ba.type.ExceptionSet exceptionSet;
/**
     * Constructor.
     * 
     * @param className
     *            the class name
     * @param exceptionSet
     *            the set of exceptions
     */
    public ExceptionObjectType(java.lang.String className, edu.umd.cs.findbugs.ba.type.ExceptionSet exceptionSet) {
        super(className);
        this.exceptionSet = exceptionSet;
    }
/**
     * Initialize object from an exception set.
     * 
     * @param exceptionSet
     *            the exception set
     * @return a Type that is a supertype of all of the exceptions in the
     *         exception set
     */
    public static org.apache.bcel.generic.Type fromExceptionSet(edu.umd.cs.findbugs.ba.type.ExceptionSet exceptionSet) throws java.lang.ClassNotFoundException {
        org.apache.bcel.generic.Type commonSupertype = exceptionSet.getCommonSupertype();
        if (commonSupertype.getType() != T_OBJECT) return commonSupertype;
        org.apache.bcel.generic.ObjectType exceptionSupertype = (org.apache.bcel.generic.ObjectType) (commonSupertype) ;
        java.lang.String className = exceptionSupertype.getClassName();
        if (className.equals("java.lang.Throwable")) return exceptionSupertype;
        return new edu.umd.cs.findbugs.ba.type.ExceptionObjectType(className, exceptionSet);
    }
    public byte getType() {
        return T_EXCEPTION;
    }
    public int hashCode() {
        return this.getSignature().hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if (o == null) return false;
        if (o.getClass() != this.getClass()) return false;
        edu.umd.cs.findbugs.ba.type.ExceptionObjectType other = (edu.umd.cs.findbugs.ba.type.ExceptionObjectType) (o) ;
        return this.getSignature().equals(other.getSignature()) && exceptionSet.equals(other.exceptionSet);
    }
/**
     * Return the exception set.
     * 
     * @return the ExceptionSet
     */
    public edu.umd.cs.findbugs.ba.type.ExceptionSet getExceptionSet() {
        return exceptionSet;
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append("<exception:");
        boolean first = true;
        for (edu.umd.cs.findbugs.ba.type.ExceptionSet.ThrownExceptionIterator i = exceptionSet.iterator(); i.hasNext(); ) {
            if (first) first = false;
            else buf.append('\u002c');
            buf.append(i.next().toString());
        }
        buf.append(">");
        return buf.toString();
    }
}
// vim:ts=4
