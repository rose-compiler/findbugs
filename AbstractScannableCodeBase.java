/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Abstract base class for implementations of IScannableCodeBase. Provides an
 * implementation of the getCodeBaseLocator(), containsSourceFiles(),
 * setApplicationCodeBase(), and isApplicationCodeBase() methods.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.impl;
import edu.umd.cs.findbugs.classfile.impl.*;
import java.util.HashMap;
import java.util.Map;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.classfile.ICodeBaseLocator;
import edu.umd.cs.findbugs.classfile.IScannableCodeBase;
abstract public class AbstractScannableCodeBase extends java.lang.Object implements edu.umd.cs.findbugs.classfile.IScannableCodeBase {
    private edu.umd.cs.findbugs.classfile.ICodeBaseLocator codeBaseLocator;
    private boolean isAppCodeBase;
    private int howDiscovered;
    private long lastModifiedTime;
    private java.util.Map<java.lang.String, java.lang.String> resourceNameTranslationMap;
    public AbstractScannableCodeBase(edu.umd.cs.findbugs.classfile.ICodeBaseLocator codeBaseLocator) {
        super();
        this.codeBaseLocator = codeBaseLocator;
        this.lastModifiedTime =  -1L;
        this.resourceNameTranslationMap = new java.util.HashMap<java.lang.String, java.lang.String>();
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getCodeBaseLocator()
     */
    public edu.umd.cs.findbugs.classfile.ICodeBaseLocator getCodeBaseLocator() {
        return codeBaseLocator;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IScannableCodeBase#containsSourceFiles()
     */
    public boolean containsSourceFiles() {
        return false;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.ICodeBase#setApplicationCodeBase(boolean)
     */
    public void setApplicationCodeBase(boolean isAppCodeBase) {
        this.isAppCodeBase = isAppCodeBase;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#isApplicationCodeBase()
     */
    public boolean isApplicationCodeBase() {
        return isAppCodeBase;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#setHowDiscovered(int)
     */
    public void setHowDiscovered(int howDiscovered) {
        this.howDiscovered = howDiscovered;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getHowDiscovered()
     */
    public int getHowDiscovered() {
        return howDiscovered;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#setLastModifiedTime(long)
     */
    public void setLastModifiedTime(long lastModifiedTime) {
        if (lastModifiedTime > 0 && edu.umd.cs.findbugs.FindBugs.validTimestamp(lastModifiedTime)) {
            this.lastModifiedTime = lastModifiedTime;
        }
    }
    public void addLastModifiedTime(long lastModifiedTime) {
        if (lastModifiedTime > 0 && edu.umd.cs.findbugs.FindBugs.validTimestamp(lastModifiedTime) && this.lastModifiedTime < lastModifiedTime) {
            this.lastModifiedTime = lastModifiedTime;
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.classfile.ICodeBase#getLastModifiedTime()
     */
    public long getLastModifiedTime() {
        return lastModifiedTime;
    }
    public void addResourceNameTranslation(java.lang.String origResourceName, java.lang.String newResourceName) {
        if ( !origResourceName.equals(newResourceName)) resourceNameTranslationMap.put(origResourceName,newResourceName);
    }
    public java.lang.String translateResourceName(java.lang.String resourceName) {
        java.lang.String translatedName = resourceNameTranslationMap.get(resourceName);
        return translatedName != null ? translatedName : resourceName;
    }
}
