/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Base class for detectors that analyze CFG (and/or use CFG-based analyses).
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.bcel;
import edu.umd.cs.findbugs.bcel.*;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.Detector2;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
abstract public class CFGDetector extends java.lang.Object implements edu.umd.cs.findbugs.Detector2 {
    public CFGDetector() {
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.Detector2#finishPass()
     */
    public void finishPass() {
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.Detector2#getDetectorClassName()
     */
    public java.lang.String getDetectorClassName() {
        return this.getClass().getName();
    }
    protected edu.umd.cs.findbugs.ba.ClassContext classContext;
    protected org.apache.bcel.classfile.Method method;
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.Detector2#visitClass(edu.umd.cs.findbugs.classfile
     * .ClassDescriptor)
     */
    public void visitClass(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache();
        org.apache.bcel.classfile.JavaClass jclass = analysisCache.getClassAnalysis(org.apache.bcel.classfile.JavaClass.class,classDescriptor);
        classContext = analysisCache.getClassAnalysis(edu.umd.cs.findbugs.ba.ClassContext.class,classDescriptor);
        for (org.apache.bcel.classfile.Method m : classContext.getMethodsInCallOrder()){
            if (m.getCode() == null) {
                continue;
            }
            method = m;
            edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor = edu.umd.cs.findbugs.bcel.BCELUtil.getMethodDescriptor(jclass,method);
// Try to get MethodGen. If we can't get one,
// then this method should be skipped.
            org.apache.bcel.generic.MethodGen methodGen = analysisCache.getMethodAnalysis(org.apache.bcel.generic.MethodGen.class,methodDescriptor);
            if (methodGen == null) {
                continue;
            }
            edu.umd.cs.findbugs.ba.CFG cfg = analysisCache.getMethodAnalysis(edu.umd.cs.findbugs.ba.CFG.class,methodDescriptor);
            this.visitMethodCFG(methodDescriptor,cfg);
        }
;
    }
/**
     * Visit the CFG (control flow graph) of a method to be analyzed. Should be
     * overridded by subclasses.
     * 
     * @param methodDescriptor
     * @param cfg
     * @throws CheckedAnalysisException
     */
    abstract protected void visitMethodCFG(edu.umd.cs.findbugs.classfile.MethodDescriptor methodDescriptor, edu.umd.cs.findbugs.ba.CFG cfg) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
}
