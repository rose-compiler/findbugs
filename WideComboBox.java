package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Dimension;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
public class WideComboBox extends javax.swing.JComboBox {
    public WideComboBox() {
        super();
    }
    public WideComboBox(final java.lang.Object[] items) {
        super(items);
    }
    public WideComboBox(java.util.Vector items) {
        super(items);
    }
    public WideComboBox(javax.swing.ComboBoxModel aModel) {
        super(aModel);
    }
    private boolean layingOut = false;
    public void doLayout() {
        try {
            layingOut = true;
            super.doLayout();
        }
        finally {
            layingOut = false;
        }
    }
    public java.awt.Dimension getSize() {
        java.awt.Dimension dim = super.getSize();
        if ( !layingOut) {
            dim.width = java.lang.Math.max(dim.width,300);
            dim.height = java.lang.Math.max(dim.height,500);
        }
        return dim;
    }
}
