/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Parse an external annotation file.
 * 
 * @author David Hovemeyer
 * @see <a
 *      href="http://groups.csail.mit.edu/pag/jsr308/annotation-file-utilities/">Annotation
 *      File Utilities/</a>
 */
package edu.umd.cs.findbugs.jaif;
import edu.umd.cs.findbugs.jaif.*;
import java.io.IOException;
import java.io.Reader;
import java.util.Locale;
import edu.umd.cs.findbugs.charsets.UTF8;
public class JAIFParser extends java.lang.Object {
    private edu.umd.cs.findbugs.jaif.JAIFScanner scanner;
    private edu.umd.cs.findbugs.jaif.JAIFEvents callback;
    public JAIFParser(java.io.Reader reader, edu.umd.cs.findbugs.jaif.JAIFEvents callback) {
        super();
        this.scanner = new edu.umd.cs.findbugs.jaif.JAIFScanner(reader);
        this.callback = callback;
    }
    public void parse() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        this.parseAnnotationFile();
    }
    int getLineNumber() {
        return scanner.getLineNumber();
    }
    private edu.umd.cs.findbugs.jaif.JAIFToken expect(java.lang.String s) throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.nextToken();
        if ( !t.lexeme.equals(s)) {
            throw new edu.umd.cs.findbugs.jaif.JAIFSyntaxException(this, "Unexpected token " + t + " (was expecting " + s + ")");
        }
        return t;
    }
    private edu.umd.cs.findbugs.jaif.JAIFToken expect(edu.umd.cs.findbugs.jaif.JAIFTokenKind kind) throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.nextToken();
        if (t.kind != kind) {
            throw new edu.umd.cs.findbugs.jaif.JAIFSyntaxException(this, "Unexpected token " + t + " (was expecting a `" + kind.toString() + "' token)");
        }
        return t;
    }
    private void expectEndOfLine() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
// extract-annotations seems to sometimes produce multiple newlines
// where
// the grammar indicates that only one will appear.
// So, we treat any sequence of one or more newlines as one newline.
        int nlCount = 0;
        edu.umd.cs.findbugs.jaif.JAIFToken t;
        while (true) {
            if (scanner.atEOF()) {
                t = null;
                break;
            }
            t = scanner.peekToken();
            if (t.kind != edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE) {
                break;
            }
            ++nlCount;
            scanner.nextToken();
        }
        if (nlCount < 1) {
            java.lang.String msg = (t == null) ? "Unexpected end of file" : "Unexpected token " + t + " (was expecting <newline>)";
            throw new edu.umd.cs.findbugs.jaif.JAIFSyntaxException(this, msg);
        }
    }
    private java.lang.String readCompoundName() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        boolean firstToken = true;
        while (true) {
            edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.nextToken();
            assert t.kind == edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD;
            if (firstToken) {
                firstToken = false;
            }
            else if (t.lexeme.startsWith("@")) {
                throw new edu.umd.cs.findbugs.jaif.JAIFSyntaxException(this, "Illegal compound name (unexpected '@' character)");
            }
            buf.append(t.lexeme);
            t = scanner.peekToken();
            if (t.kind != edu.umd.cs.findbugs.jaif.JAIFTokenKind.DOT) {
                break;
            }
            else {
                buf.append(t.lexeme);
                scanner.nextToken();
            }
        }
        return buf.toString();
    }
    private java.lang.String readType() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        edu.umd.cs.findbugs.jaif.JAIFToken t = this.expect(edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        if (t.lexeme.equals("enum")) {
        }
        return buf.toString();
    }
    private void parseAnnotationFile() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        this.parsePackageDefinition();
        while ( !scanner.atEOF()) {
            this.parsePackageDefinition();
        }
    }
    private void parsePackageDefinition() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        this.expect("package");
        edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.peekToken();
        java.lang.String pkgName;
        if (t.kind != edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE) {
            pkgName = this.readCompoundName();
            this.expect(":");
            t = scanner.peekToken();
// Optional package name and package-level annotations
// Hmmm....the spec says just a plain identifier here.
// However, I'm pretty sure we want a compound name.
            while (t.isStartOfAnnotationName()) {
                this.parseAnnotation();
            }
        }
        else {
            pkgName = "";
        }
        this.expectEndOfLine();
        callback.startPackageDefinition(pkgName);
        while ( !scanner.atEOF()) {
            t = scanner.peekToken();
            if (t.lexeme.equals("package")) {
                break;
            }
            this.parseAnnotationDefinitionOrClassDefinition();
        }
        callback.endPackageDefinition(pkgName);
    }
    private void parseAnnotation() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        java.lang.String annotationName = this.readCompoundName();
        assert annotationName.startsWith("@");
        callback.startAnnotation(annotationName);
        edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.peekToken();
        if (t.kind == edu.umd.cs.findbugs.jaif.JAIFTokenKind.LPAREN) {
            this.parseAnnotationField();
            t = scanner.peekToken();
            while (t.kind != edu.umd.cs.findbugs.jaif.JAIFTokenKind.RPAREN) {
                this.expect(",");
                this.parseAnnotationField();
                t = scanner.peekToken();
            }
            assert t.kind == edu.umd.cs.findbugs.jaif.JAIFTokenKind.RPAREN;
            scanner.nextToken();
        }
        callback.endAnnotation(annotationName);
    }
    private void parseAnnotationField() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        edu.umd.cs.findbugs.jaif.JAIFToken id = this.expect(edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD);
        this.expect("=");
        java.lang.Object constant = this.parseConstant();
        callback.annotationField(id.lexeme,constant);
    }
    private java.lang.Object parseConstant() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.peekToken();
        switch(t.kind){
            case IDENTIFIER_OR_KEYWORD:{
                java.lang.String name = this.readCompoundName();
                return new edu.umd.cs.findbugs.jaif.JAIFEnumConstant(name);
            }
            case DECIMAL_LITERAL:{
                t = scanner.nextToken();
                return java.lang.Integer.parseInt(t.lexeme);
            }
            case OCTAL_LITERAL:{
                t = scanner.nextToken();
                return java.lang.Integer.parseInt(t.lexeme,8);
            }
            case HEX_LITERAL:{
                t = scanner.nextToken();
                return java.lang.Integer.parseInt(t.lexeme,16);
            }
            case FLOATING_POINT_LITERAL:{
                t = scanner.nextToken();
                boolean isFloat = t.lexeme.toLowerCase(java.util.Locale.ENGLISH).endsWith("f");
                if (isFloat) {
                    return java.lang.Float.parseFloat(t.lexeme);
                }
                else {
                    return java.lang.Double.parseDouble(t.lexeme);
                }
            }
            case STRING_LITERAL:{
                t = scanner.nextToken();
                return this.unparseStringLiteral(t.lexeme);
            }
            default:{
                throw new edu.umd.cs.findbugs.jaif.JAIFSyntaxException(this, "Illegal constant");
            }
        }
    }
// This is an enum constant specified by a compound name.
// Represent it as a JAIFEnumConstant object.
    private java.lang.Object unparseStringLiteral(java.lang.String lexeme) {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
// skip initial double quote char
        int where = 1;
        while (true) {
            assert where < lexeme.length();
            char c = lexeme.charAt(where);
            if (c == '"') {
                break;
            }
            if (c != '\\') {
                buf.append(c);
                where++;
                continue;
            }
            where++;
            assert where < lexeme.length();
            c = lexeme.charAt(where);
            switch(c){
                case '\u0062':{
                    buf.append('\b');
                    where++;
                    break;
                }
                case '\u0074':{
                    buf.append('\t');
                    where++;
                    break;
                }
                case '\u006e':{
                    buf.append('\n');
                    where++;
                    break;
                }
                case '\u0066':{
                    buf.append('\t');
                    where++;
                    break;
                }
                case '\u0072':{
                    buf.append('\r');
                    where++;
                    break;
                }
                case '"':{
                    buf.append('"');
                    where++;
                    break;
                }
                case '\'':{
                    buf.append('\'');
                    where++;
                    break;
                }
                case '\\':{
                    buf.append('\\');
                    where++;
                    break;
                }
                default:{
                    char value = (char) (0) ;
                    while (c >= '\u0030' && c <= '\u0037') {
                        value *= 8;
                        value += (c - '\u0030');
                        where++;
                        assert where < lexeme.length();
                        c = lexeme.charAt(where);
                    }
                    buf.append(value);
                }
            }
        }
        return buf.toString();
    }
    private void parseAnnotationDefinitionOrClassDefinition() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.peekToken();
        if (t.lexeme.equals("annotation")) {
            this.parseAnnotationDefinition();
        }
        else if (t.lexeme.equals("class")) {
            this.parseClassDefinition();
        }
        else {
            throw new edu.umd.cs.findbugs.jaif.JAIFSyntaxException(this, "Unexpected token " + t + " (expected `annotation' or `class')");
        }
    }
    private void parseAnnotationDefinition() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        this.expect("annotation");
        java.lang.String retention = null;
        edu.umd.cs.findbugs.jaif.JAIFToken t = scanner.peekToken();
        if (t.lexeme.equals("visible") || t.lexeme.equals("invisible") || t.lexeme.equals("source")) {
            retention = t.lexeme;
            scanner.nextToken();
        }
        java.lang.String annotationName = this.expect(edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD).lexeme;
        this.expect(edu.umd.cs.findbugs.jaif.JAIFTokenKind.COLON);
        this.expectEndOfLine();
        callback.startAnnotationDefinition(annotationName,retention);
        t = scanner.peekToken();
        while (t.kind != edu.umd.cs.findbugs.jaif.JAIFTokenKind.NEWLINE) {
            this.parseAnnotationFieldDefinition();
        }
    }
    private void parseAnnotationFieldDefinition() throws edu.umd.cs.findbugs.jaif.JAIFSyntaxException, java.io.IOException {
        java.lang.String type = this.readType();
        java.lang.String fieldName = this.expect(edu.umd.cs.findbugs.jaif.JAIFTokenKind.IDENTIFIER_OR_KEYWORD).lexeme;
        callback.annotationFieldDefinition(type,fieldName);
    }
    private void parseClassDefinition() {
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        if (args.length != 1) {
            java.lang.System.err.println("Usage: " + edu.umd.cs.findbugs.jaif.JAIFParser.class.getName() + " <jaif file>");
            java.lang.System.exit(1);
        }
        edu.umd.cs.findbugs.jaif.JAIFEvents callback = new edu.umd.cs.findbugs.jaif.JAIFEvents() {
            public void annotationField(java.lang.String fieldName, java.lang.Object constant) {
                java.lang.System.out.println("    " + fieldName + "=" + constant);
            }
            public void endAnnotation(java.lang.String annotationName) {
            }
            public void endPackageDefinition(java.lang.String pkgName) {
            }
            public void startAnnotation(java.lang.String annotationName) {
                java.lang.System.out.println("  annotation " + annotationName);
            }
            public void startPackageDefinition(java.lang.String pkgName) {
                java.lang.System.out.println("package " + pkgName);
            }
            public void startAnnotationDefinition(java.lang.String annotationName, java.lang.String retention) {
                java.lang.System.out.println("  annotation " + annotationName + " " + retention);
            }
            public void endAnnotationDefinition(java.lang.String annotationName) {
            }
            public void annotationFieldDefinition(java.lang.String type, java.lang.String fieldName) {
                java.lang.System.out.println("    " + type + " " + fieldName);
            }
        };
        edu.umd.cs.findbugs.jaif.JAIFParser parser = new edu.umd.cs.findbugs.jaif.JAIFParser(edu.umd.cs.findbugs.charsets.UTF8.fileReader(args[0]), callback);
        parser.parse();
    }
}
