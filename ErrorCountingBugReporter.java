/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2007, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A delegating bug reporter which counts reported bug instances, missing
 * classes, and serious analysis errors.
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.util.HashSet;
import java.util.Set;
public class ErrorCountingBugReporter extends edu.umd.cs.findbugs.DelegatingBugReporter {
    private int bugCount;
    private java.util.HashSet<java.lang.String> errors = new java.util.HashSet<java.lang.String>();
    private java.util.Set<java.lang.String> missingClassSet = new java.util.HashSet<java.lang.String>();
    public ErrorCountingBugReporter(edu.umd.cs.findbugs.BugReporter realBugReporter) {
        super(realBugReporter);
        this.bugCount = 0;
        realBugReporter.addObserver(new edu.umd.cs.findbugs.BugReporterObserver() {
            public void reportBug(edu.umd.cs.findbugs.BugInstance bugInstance) {
                ++bugCount;
            }
        });
    }
// Add an observer to record when bugs make it through
// all priority and filter criteria, so our bug count is
// accurate.
    public int getBugCount() {
        return bugCount;
    }
    public int getMissingClassCount() {
        return missingClassSet.size();
    }
    public int getErrorCount() {
        return errors.size();
    }
    public void logError(java.lang.String message) {
        if (errors.add(message)) super.logError(message);
    }
    public void reportMissingClass(java.lang.ClassNotFoundException ex) {
        java.lang.String missing = edu.umd.cs.findbugs.AbstractBugReporter.getMissingClassName(ex);
        if (missing == null || missing.startsWith("[") || missing.equals("java.lang.Synthetic")) {
            return;
        }
        if (missingClassSet.add(missing)) {
            super.reportMissingClass(ex);
        }
    }
}
