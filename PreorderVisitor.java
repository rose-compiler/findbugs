/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Interface to make the use of a visitor pattern programming style possible.
 * I.e. a class that implements this interface can traverse the contents of a
 * Java class just by calling the `accept' method which all classes have.
 * <p/>
 * Implemented by wish of <A HREF="http://www.inf.fu-berlin.de/~bokowski">Boris
 * Bokowski</A>.
 * <p/>
 * If don't like it, blame him. If you do like it thank me 8-)
 *
 * @author <A HREF="http://www.inf.fu-berlin.de/~dahm">M. Dahm</A>
 * @version 970819
 */
package edu.umd.cs.findbugs.visitclass;
import edu.umd.cs.findbugs.visitclass.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.bcel.classfile.AnnotationDefault;
import org.apache.bcel.classfile.AnnotationEntry;
import org.apache.bcel.classfile.Annotations;
import org.apache.bcel.classfile.Attribute;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.CodeException;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.EnclosingMethod;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.InnerClass;
import org.apache.bcel.classfile.InnerClasses;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LineNumber;
import org.apache.bcel.classfile.LineNumberTable;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.LocalVariableTable;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.classfile.ParameterAnnotations;
import org.apache.bcel.classfile.StackMapTable;
import org.apache.bcel.classfile.StackMapTableEntry;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.ba.XField;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.FieldDescriptor;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
import edu.umd.cs.findbugs.classfile.analysis.ClassInfo;
import edu.umd.cs.findbugs.classfile.analysis.FieldInfo;
import edu.umd.cs.findbugs.classfile.analysis.MethodInfo;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
import edu.umd.cs.findbugs.internalAnnotations.SlashedClassName;
public class PreorderVisitor extends edu.umd.cs.findbugs.visitclass.BetterVisitor implements edu.umd.cs.findbugs.visitclass.Constants2 {
    public PreorderVisitor() {
    }
// Available when visiting a class
    private org.apache.bcel.classfile.ConstantPool constantPool;
    private org.apache.bcel.classfile.JavaClass thisClass;
    private edu.umd.cs.findbugs.classfile.analysis.ClassInfo thisClassInfo;
    private edu.umd.cs.findbugs.classfile.analysis.MethodInfo thisMethodInfo;
    private edu.umd.cs.findbugs.classfile.analysis.FieldInfo thisFieldInfo;
    private java.lang.String className = "none";
    private java.lang.String dottedClassName = "none";
    private java.lang.String packageName = "none";
    private java.lang.String sourceFile = "none";
    private java.lang.String superclassName = "none";
    private java.lang.String dottedSuperclassName = "none";
// Available when visiting a method
    private boolean visitingMethod = false;
    private java.lang.String methodSig = "none";
    private java.lang.String dottedMethodSig = "none";
    private org.apache.bcel.classfile.Method method = null;
    private java.lang.String methodName = "none";
    private java.lang.String fullyQualifiedMethodName = "none";
// Available when visiting a field
    private org.apache.bcel.classfile.Field field;
    private boolean visitingField = false;
    private java.lang.String fullyQualifiedFieldName = "none";
    private java.lang.String fieldName = "none";
    private java.lang.String fieldSig = "none";
    private java.lang.String dottedFieldSig = "none";
    private boolean fieldIsStatic;
// Available when visiting a Code
    private org.apache.bcel.classfile.Code code;
    protected java.lang.String getStringFromIndex(int i) {
        org.apache.bcel.classfile.ConstantUtf8 name = (org.apache.bcel.classfile.ConstantUtf8) (constantPool.getConstant(i)) ;
        return name.getBytes();
    }
    protected int asUnsignedByte(byte b) {
        return 0xff & b;
    }
/**
     * Return the current Code attribute; assuming one is being visited
     *
     * @return current code attribute
     */
    public org.apache.bcel.classfile.Code getCode() {
        if (code == null) throw new java.lang.IllegalStateException("Not visiting Code");
        return code;
    }
    public java.util.Set<java.lang.String> getSurroundingCaughtExceptions(int pc) {
        return this.getSurroundingCaughtExceptions(pc,java.lang.Integer.MAX_VALUE);
    }
    public java.util.Set<java.lang.String> getSurroundingCaughtExceptions(int pc, int maxTryBlockSize) {
        java.util.HashSet<java.lang.String> result = new java.util.HashSet<java.lang.String>();
        if (code == null) throw new java.lang.IllegalStateException("Not visiting Code");
        int size = maxTryBlockSize;
        if (code.getExceptionTable() == null) return result;
        for (org.apache.bcel.classfile.CodeException catchBlock : code.getExceptionTable()){
            int startPC = catchBlock.getStartPC();
            int endPC = catchBlock.getEndPC();
            if (pc >= startPC && pc <= endPC) {
                int thisSize = endPC - startPC;
                if (size > thisSize) {
                    result.clear();
                    size = thisSize;
                    org.apache.bcel.classfile.Constant kind = constantPool.getConstant(catchBlock.getCatchType());
                    result.add("C" + catchBlock.getCatchType());
                }
                else if (size == thisSize) result.add("C" + catchBlock.getCatchType());
            }
        }
;
        return result;
    }
/**
     * Get lines of code in try block that surround pc
     *
     * @param pc
     * @return number of lines of code in try block
     */
    public int getSizeOfSurroundingTryBlock(int pc) {
        return this.getSizeOfSurroundingTryBlock(null,pc);
    }
/**
     * Get lines of code in try block that surround pc
     *
     * @param pc
     * @return number of lines of code in try block
     */
    public int getSizeOfSurroundingTryBlock(java.lang.String vmNameOfExceptionClass, int pc) {
        if (code == null) throw new java.lang.IllegalStateException("Not visiting Code");
        return edu.umd.cs.findbugs.visitclass.Util.getSizeOfSurroundingTryBlock(constantPool,code,vmNameOfExceptionClass,pc);
    }
    public org.apache.bcel.classfile.CodeException getSurroundingTryBlock(int pc) {
        return this.getSurroundingTryBlock(null,pc);
    }
    public org.apache.bcel.classfile.CodeException getSurroundingTryBlock(java.lang.String vmNameOfExceptionClass, int pc) {
        if (code == null) throw new java.lang.IllegalStateException("Not visiting Code");
        return edu.umd.cs.findbugs.visitclass.Util.getSurroundingTryBlock(constantPool,code,vmNameOfExceptionClass,pc);
    }
// Attributes
    public void visitCode(org.apache.bcel.classfile.Code obj) {
        code = obj;
        super.visitCode(obj);
        org.apache.bcel.classfile.CodeException[] exceptions = obj.getExceptionTable();
        for (org.apache.bcel.classfile.CodeException exception : exceptions)exception.accept(this);
;
        org.apache.bcel.classfile.Attribute[] attributes = obj.getAttributes();
        for (org.apache.bcel.classfile.Attribute attribute : attributes)attribute.accept(this);
;
        this.visitAfter(obj);
        code = null;
    }
/**
     * Called after visiting a code attribute
     *
     * @param obj
     *            Code that was just visited
     */
    public void visitAfter(org.apache.bcel.classfile.Code obj) {
    }
// Constants
    public void visitConstantPool(org.apache.bcel.classfile.ConstantPool obj) {
        super.visitConstantPool(obj);
        org.apache.bcel.classfile.Constant[] constant_pool = obj.getConstantPool();
        for (int i = 1; i < constant_pool.length; i++) {
            constant_pool[i].accept(this);
            byte tag = constant_pool[i].getTag();
            if ((tag == CONSTANT_Double) || (tag == CONSTANT_Long)) i++;
        }
    }
    private void doVisitField(org.apache.bcel.classfile.Field field) {
        if (visitingField) throw new java.lang.IllegalStateException("visitField called when already visiting a field");
        visitingField = true;
        this.field = field;
        try {
            fieldName = fieldSig = dottedFieldSig = fullyQualifiedFieldName = null;
            thisFieldInfo = (edu.umd.cs.findbugs.classfile.analysis.FieldInfo) (thisClassInfo.findField(this.getFieldName(),this.getFieldSig(),field.isStatic())) ;
            if (thisFieldInfo == null) {
                throw new java.lang.AssertionError("Can't get field info for " + this.getFullyQualifiedFieldName());
            }
            fieldIsStatic = field.isStatic();
            field.accept(this);
            org.apache.bcel.classfile.Attribute[] attributes = field.getAttributes();
            for (org.apache.bcel.classfile.Attribute attribute : attributes)attribute.accept(this);
;
        }
        finally {
            visitingField = false;
            this.field = null;
            this.thisFieldInfo = null;
        }
    }
    public void doVisitMethod(org.apache.bcel.classfile.Method method) {
        if (visitingMethod) throw new java.lang.IllegalStateException("doVisitMethod called when already visiting a method");
        visitingMethod = true;
        try {
            this.method = method;
            methodName = methodSig = dottedMethodSig = fullyQualifiedMethodName = null;
            thisMethodInfo = (edu.umd.cs.findbugs.classfile.analysis.MethodInfo) (thisClassInfo.findMethod(this.getMethodName(),this.getMethodSig(),method.isStatic())) ;
            if (thisMethodInfo == null) throw new java.lang.AssertionError("Can't get method info for " + this.getFullyQualifiedMethodName());
            this.method.accept(this);
            org.apache.bcel.classfile.Attribute[] attributes = method.getAttributes();
            for (org.apache.bcel.classfile.Attribute attribute : attributes)attribute.accept(this);
;
        }
        finally {
            visitingMethod = false;
            this.method = null;
            this.thisMethodInfo = null;
        }
    }
    public boolean amVisitingMainMethod() {
        if ( !visitingMethod) throw new java.lang.IllegalStateException("Not visiting a method");
        if ( !method.isStatic()) return false;
        if ( !this.getMethodName().equals("main")) return false;
        if ( !this.getMethodSig().equals("([Ljava/lang/String;)V")) return false;
        return true;
    }
// Extra classes (i.e. leaves in this context)
    public void visitInnerClasses(org.apache.bcel.classfile.InnerClasses obj) {
        super.visitInnerClasses(obj);
        org.apache.bcel.classfile.InnerClass[] inner_classes = obj.getInnerClasses();
        for (org.apache.bcel.classfile.InnerClass inner_class : inner_classes)inner_class.accept(this);
;
    }
    public void visitAfter(org.apache.bcel.classfile.JavaClass obj) {
    }
    public boolean shouldVisit(org.apache.bcel.classfile.JavaClass obj) {
        return true;
    }
    boolean visitMethodsInCallOrder;
    protected boolean isVisitMethodsInCallOrder() {
        return visitMethodsInCallOrder;
    }
    protected void setVisitMethodsInCallOrder(boolean visitMethodsInCallOrder) {
        this.visitMethodsInCallOrder = visitMethodsInCallOrder;
    }
    protected java.lang.Iterable<org.apache.bcel.classfile.Method> getMethodVisitOrder(org.apache.bcel.classfile.JavaClass obj) {
        return java.util.Arrays.asList(obj.getMethods());
    }
// General classes
    public void visitJavaClass(org.apache.bcel.classfile.JavaClass obj) {
        this.setupVisitorForClass(obj);
        if (this.shouldVisit(obj)) {
            constantPool.accept(this);
            org.apache.bcel.classfile.Field[] fields = obj.getFields();
            org.apache.bcel.classfile.Attribute[] attributes = obj.getAttributes();
            for (org.apache.bcel.classfile.Field field : fields)this.doVisitField(field);
;
            boolean didInCallOrder = false;
            if (visitMethodsInCallOrder) {
                try {
                    edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache();
                    edu.umd.cs.findbugs.classfile.ClassDescriptor c = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(obj);
                    edu.umd.cs.findbugs.ba.ClassContext classContext = analysisCache.getClassAnalysis(edu.umd.cs.findbugs.ba.ClassContext.class,c);
                    didInCallOrder = true;
                    for (org.apache.bcel.classfile.Method m : classContext.getMethodsInCallOrder())this.doVisitMethod(m);
;
                }
                catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
                    edu.umd.cs.findbugs.ba.AnalysisContext.logError("Error trying to visit methods in order",e);
                }
            }
            if ( !didInCallOrder) for (org.apache.bcel.classfile.Method m : this.getMethodVisitOrder(obj))this.doVisitMethod(m);
;
            for (org.apache.bcel.classfile.Attribute attribute : attributes)attribute.accept(this);
;
            this.visitAfter(obj);
        }
    }
    public void setupVisitorForClass(org.apache.bcel.classfile.JavaClass obj) {
        constantPool = obj.getConstantPool();
        thisClass = obj;
        org.apache.bcel.classfile.ConstantClass c = (org.apache.bcel.classfile.ConstantClass) (constantPool.getConstant(obj.getClassNameIndex())) ;
        className = this.getStringFromIndex(c.getNameIndex());
        dottedClassName = className.replace('\u002f','\u002e');
        packageName = obj.getPackageName();
        sourceFile = obj.getSourceFileName();
        dottedSuperclassName = obj.getSuperclassName();
        superclassName = dottedSuperclassName.replace('\u002e','\u002f');
        edu.umd.cs.findbugs.classfile.ClassDescriptor cDesc = edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(className);
        if ( !edu.umd.cs.findbugs.FindBugs.isNoAnalysis()) try {
            thisClassInfo = (edu.umd.cs.findbugs.classfile.analysis.ClassInfo) (edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getClassAnalysis(edu.umd.cs.findbugs.ba.XClass.class,cDesc)) ;
        }
        catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
            throw new java.lang.AssertionError("Can't find ClassInfo for " + cDesc);
        }
        super.visitJavaClass(obj);
    }
    public void visitLineNumberTable(org.apache.bcel.classfile.LineNumberTable obj) {
        super.visitLineNumberTable(obj);
        org.apache.bcel.classfile.LineNumber[] line_number_table = obj.getLineNumberTable();
        for (org.apache.bcel.classfile.LineNumber aLine_number_table : line_number_table)aLine_number_table.accept(this);
;
    }
    public void visitLocalVariableTable(org.apache.bcel.classfile.LocalVariableTable obj) {
        super.visitLocalVariableTable(obj);
        org.apache.bcel.classfile.LocalVariable[] local_variable_table = obj.getLocalVariableTable();
        for (org.apache.bcel.classfile.LocalVariable aLocal_variable_table : local_variable_table)aLocal_variable_table.accept(this);
;
    }
// Accessors
    public edu.umd.cs.findbugs.ba.XClass getXClass() {
        if (thisClassInfo == null) throw new java.lang.AssertionError("XClass information not set");
        return thisClassInfo;
    }
    public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() {
        return thisClassInfo;
    }
    public edu.umd.cs.findbugs.ba.XMethod getXMethod() {
        return thisMethodInfo;
    }
    public edu.umd.cs.findbugs.classfile.MethodDescriptor getMethodDescriptor() {
        return thisMethodInfo;
    }
    public edu.umd.cs.findbugs.ba.XField getXField() {
        return thisFieldInfo;
    }
    public edu.umd.cs.findbugs.classfile.FieldDescriptor getFieldDescriptor() {
        return thisFieldInfo;
    }
/** Get the constant pool for the current or most recently visited class */
    public org.apache.bcel.classfile.ConstantPool getConstantPool() {
        return constantPool;
    }
/**
     * Get the slash-formatted class name for the current or most recently
     * visited class
     */
    public java.lang.String getClassName() {
        return className;
    }
/** Get the dotted class name for the current or most recently visited class */
    public java.lang.String getDottedClassName() {
        return dottedClassName;
    }
/**
     * Get the (slash-formatted?) package name for the current or most recently
     * visited class
     */
    public java.lang.String getPackageName() {
        return packageName;
    }
/** Get the source file name for the current or most recently visited class */
    public java.lang.String getSourceFile() {
        return sourceFile;
    }
/**
     * Get the slash-formatted superclass name for the current or most recently
     * visited class
     */
    public java.lang.String getSuperclassName() {
        return superclassName;
    }
/**
     * Get the dotted superclass name for the current or most recently visited
     * class
     */
    public java.lang.String getDottedSuperclassName() {
        return dottedSuperclassName;
    }
/** Get the JavaClass object for the current or most recently visited class */
    public org.apache.bcel.classfile.JavaClass getThisClass() {
        return thisClass;
    }
/** If currently visiting a method, get the method's fully qualified name */
    public java.lang.String getFullyQualifiedMethodName() {
        if ( !visitingMethod) throw new java.lang.IllegalStateException("getFullyQualifiedMethodName called while not visiting method");
        if (fullyQualifiedMethodName == null) {
            this.getDottedSuperclassName();
            this.getMethodName();
            this.getDottedMethodSig();
            java.lang.StringBuilder ref = new java.lang.StringBuilder(5 + dottedClassName.length() + methodName.length() + dottedMethodSig.length());
            ref.append(dottedClassName).append(".").append(methodName).append(" : ").append(dottedMethodSig);
            fullyQualifiedMethodName = ref.toString();
        }
        return fullyQualifiedMethodName;
    }
/**
     * is the visitor currently visiting a method?
     */
    public boolean visitingMethod() {
        return visitingMethod;
    }
/**
     * is the visitor currently visiting a field?
     */
    public boolean visitingField() {
        return visitingField;
    }
/** If currently visiting a field, get the field's Field object */
    public org.apache.bcel.classfile.Field getField() {
        if ( !visitingField) throw new java.lang.IllegalStateException("getField called while not visiting field");
        return field;
    }
/** If currently visiting a method, get the method's Method object */
    public org.apache.bcel.classfile.Method getMethod() {
        if ( !visitingMethod) throw new java.lang.IllegalStateException("getMethod called while not visiting method");
        return method;
    }
/** If currently visiting a method, get the method's name */
    public java.lang.String getMethodName() {
        if ( !visitingMethod) throw new java.lang.IllegalStateException("getMethodName called while not visiting method");
        if (methodName == null) methodName = this.getStringFromIndex(method.getNameIndex());
        return methodName;
    }
    static java.util.regex.Pattern argumentSignature = java.util.regex.Pattern.compile("\\[*([BCDFIJSZ]|L[^;]*;)");
    public static int getNumberArguments(java.lang.String signature) {
        int count = 0;
        int pos = 1;
        boolean inArray = false;
        while (true) {
            switch(signature.charAt(pos++)){
                case '\u0029':{
                    return count;
                }
                case '\u005b':{
                    if ( !inArray) count++;
                    inArray = true;
                    break;
                }
                case '\u004c':{
                    if ( !inArray) count++;
                    while (signature.charAt(pos) != '\u003b') pos++;
                    pos++;
                    inArray = false;
                    break;
                }
                default:{
                    if ( !inArray) count++;
                    inArray = false;
                    break;
                }
            }
        }
    }
    public int getNumberMethodArguments() {
        return getNumberArguments(this.getMethodSig());
    }
/**
     * If currently visiting a method, get the method's slash-formatted
     * signature
     */
    public java.lang.String getMethodSig() {
        if ( !visitingMethod) throw new java.lang.IllegalStateException("getMethodSig called while not visiting method");
        if (methodSig == null) methodSig = this.getStringFromIndex(method.getSignatureIndex());
        return methodSig;
    }
/** If currently visiting a method, get the method's dotted method signature */
    public java.lang.String getDottedMethodSig() {
        if ( !visitingMethod) throw new java.lang.IllegalStateException("getDottedMethodSig called while not visiting method");
        if (dottedMethodSig == null) dottedMethodSig = this.getMethodSig().replace('\u002f','\u002e');
        return dottedMethodSig;
    }
/** If currently visiting a field, get the field's name */
    public java.lang.String getFieldName() {
        if ( !visitingField) throw new java.lang.IllegalStateException("getFieldName called while not visiting field");
        if (fieldName == null) fieldName = this.getStringFromIndex(field.getNameIndex());
        return fieldName;
    }
/** If currently visiting a field, get the field's slash-formatted signature */
    public java.lang.String getFieldSig() {
        if ( !visitingField) throw new java.lang.IllegalStateException("getFieldSig called while not visiting field");
        if (fieldSig == null) fieldSig = this.getStringFromIndex(field.getSignatureIndex());
        return fieldSig;
    }
/** If currently visiting a field, return whether or not the field is static */
    public boolean getFieldIsStatic() {
        if ( !visitingField) throw new java.lang.IllegalStateException("getFieldIsStatic called while not visiting field");
        return fieldIsStatic;
    }
/** If currently visiting a field, get the field's fully qualified name */
    public java.lang.String getFullyQualifiedFieldName() {
        if ( !visitingField) throw new java.lang.IllegalStateException("getFullyQualifiedFieldName called while not visiting field");
        if (fullyQualifiedFieldName == null) fullyQualifiedFieldName = this.getDottedClassName() + "." + this.getFieldName() + " : " + this.getFieldSig();
        return fullyQualifiedFieldName;
    }
/** If currently visiting a field, get the field's dot-formatted signature */
    public java.lang.String getDottedFieldSig() {
        if ( !visitingField) throw new java.lang.IllegalStateException("getDottedFieldSig called while not visiting field");
        if (dottedFieldSig == null) dottedFieldSig = fieldSig.replace('\u002f','\u002e');
        return dottedFieldSig;
    }
    public java.lang.String toString() {
        if (visitingMethod) return this.getClass().getSimpleName() + " analyzing " + this.getClassName() + "." + this.getMethodName() + this.getMethodSig();
        else if (visitingField) return this.getClass().getSimpleName() + " analyzing " + this.getClassName() + "." + this.getFieldName();
        return this.getClass().getSimpleName() + " analyzing " + this.getClassName();
    }
/*
     * (non-Javadoc)
     *
     * @see
     * org.apache.bcel.classfile.Visitor#visitAnnotation(org.apache.bcel.classfile
     * .Annotations)
     */
    public void visitAnnotation(org.apache.bcel.classfile.Annotations arg0) {
    }
// TODO Auto-generated method stub
/*
     * (non-Javadoc)
     *
     * @see
     * org.apache.bcel.classfile.Visitor#visitAnnotationDefault(org.apache.bcel
     * .classfile.AnnotationDefault)
     */
    public void visitAnnotationDefault(org.apache.bcel.classfile.AnnotationDefault arg0) {
    }
// TODO Auto-generated method stub
/*
     * (non-Javadoc)
     *
     * @see
     * org.apache.bcel.classfile.Visitor#visitAnnotationEntry(org.apache.bcel
     * .classfile.AnnotationEntry)
     */
    public void visitAnnotationEntry(org.apache.bcel.classfile.AnnotationEntry arg0) {
    }
// TODO Auto-generated method stub
/*
     * (non-Javadoc)
     *
     * @see
     * org.apache.bcel.classfile.Visitor#visitEnclosingMethod(org.apache.bcel
     * .classfile.EnclosingMethod)
     */
    public void visitEnclosingMethod(org.apache.bcel.classfile.EnclosingMethod arg0) {
    }
// TODO Auto-generated method stub
/*
     * (non-Javadoc)
     *
     * @see
     * org.apache.bcel.classfile.Visitor#visitParameterAnnotation(org.apache
     * .bcel.classfile.ParameterAnnotations)
     */
    public void visitParameterAnnotation(org.apache.bcel.classfile.ParameterAnnotations arg0) {
    }
/*
     * (non-Javadoc)
     *
     * @see
     * org.apache.bcel.classfile.Visitor#visitStackMapTable(org.apache.bcel.
     * classfile.StackMapTable)
     */
    public void visitStackMapTable(org.apache.bcel.classfile.StackMapTable arg0) {
    }
// TODO Auto-generated method stub
/*
     * (non-Javadoc)
     *
     * @see
     * org.apache.bcel.classfile.Visitor#visitStackMapTableEntry(org.apache.
     * bcel.classfile.StackMapTableEntry)
     */
    public void visitStackMapTableEntry(org.apache.bcel.classfile.StackMapTableEntry arg0) {
    }
}
// TODO Auto-generated method stub
