/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2007, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Test cases for ReturnPathType class.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.npe;
import edu.umd.cs.findbugs.ba.npe.*;
import junit.framework.Assert;
import junit.framework.TestCase;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
public class ReturnPathTypeTest extends junit.framework.TestCase {
    public ReturnPathTypeTest() {
    }
    edu.umd.cs.findbugs.ba.npe.ReturnPathType top;
    edu.umd.cs.findbugs.ba.npe.ReturnPathType normal;
    edu.umd.cs.findbugs.ba.npe.ReturnPathType abnormal;
/*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws java.lang.Exception {
        top = new edu.umd.cs.findbugs.ba.npe.ReturnPathType();
        normal = new edu.umd.cs.findbugs.ba.npe.ReturnPathType();
        normal.setCanReturnNormally(true);
        abnormal = new edu.umd.cs.findbugs.ba.npe.ReturnPathType();
        abnormal.setCanReturnNormally(false);
    }
    public void testTop() throws java.lang.Exception {
        junit.framework.Assert.assertFalse(top.isValid());
        junit.framework.Assert.assertTrue(top.isTop());
        try {
            top.canReturnNormally();
            junit.framework.Assert.assertTrue(false);
        }
        catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
        }
    }
    public void testCanReturnNormally() throws java.lang.Exception {
        junit.framework.Assert.assertTrue(normal.isValid());
        junit.framework.Assert.assertTrue(normal.canReturnNormally());
    }
    public void testCannotReturnNormally() throws java.lang.Exception {
        junit.framework.Assert.assertTrue(abnormal.isValid());
        junit.framework.Assert.assertFalse(abnormal.canReturnNormally());
    }
    public void testMergeWithTop() throws java.lang.Exception {
        normal.mergeWith(top);
        junit.framework.Assert.assertTrue(normal.canReturnNormally());
        abnormal.mergeWith(top);
        junit.framework.Assert.assertFalse(abnormal.canReturnNormally());
    }
    public void testTopMergeWithNormalReturn() throws java.lang.Exception {
        top.mergeWith(normal);
        junit.framework.Assert.assertTrue(top.canReturnNormally());
    }
    public void testTopMergeWithAbnormalReturn() throws java.lang.Exception {
        top.mergeWith(abnormal);
        junit.framework.Assert.assertFalse(top.canReturnNormally());
    }
    public void testNormalMergeWIthAbnormal() throws java.lang.Exception {
        normal.mergeWith(abnormal);
        junit.framework.Assert.assertTrue(normal.canReturnNormally());
    }
    public void testAbnormalMergeWithNormal() throws java.lang.Exception {
        abnormal.mergeWith(normal);
        junit.framework.Assert.assertTrue(abnormal.canReturnNormally());
    }
    public void testNormalMergeWithNormal() throws java.lang.Exception {
        edu.umd.cs.findbugs.ba.npe.ReturnPathType otherNormal = new edu.umd.cs.findbugs.ba.npe.ReturnPathType();
        otherNormal.setCanReturnNormally(true);
        normal.mergeWith(otherNormal);
        junit.framework.Assert.assertTrue(normal.canReturnNormally());
    }
    public void testAbnormalMergeWithAbnormal() throws java.lang.Exception {
        edu.umd.cs.findbugs.ba.npe.ReturnPathType otherAbnormal = new edu.umd.cs.findbugs.ba.npe.ReturnPathType();
        otherAbnormal.setCanReturnNormally(false);
        abnormal.mergeWith(otherAbnormal);
        junit.framework.Assert.assertFalse(abnormal.canReturnNormally());
    }
}
