package edu.umd.cs.findbugs.ml;
import edu.umd.cs.findbugs.ml.*;
import java.util.Collection;
import junit.framework.Assert;
import junit.framework.TestCase;
import edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier;
public class SplitCamelCaseIdentifierTest extends junit.framework.TestCase {
    public SplitCamelCaseIdentifierTest() {
    }
    edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier splitter;
    edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier splitter2;
    edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier splitterLong;
    edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier allLower;
    edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier allUpper;
    edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier capitalized;
    protected void setUp() throws java.lang.Exception {
        splitter = new edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier("displayGUIWindow");
        splitter2 = new edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier("DisplayGUIWindow");
        splitterLong = new edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier("nowIsTheWINTEROfOURDiscontent");
        allLower = new edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier("foobar");
        allUpper = new edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier("NSA");
        capitalized = new edu.umd.cs.findbugs.util.SplitCamelCaseIdentifier("Maryland");
    }
    public void testSplit() {
        java.util.Collection<java.lang.String> words = splitter.split();
        this.checkContents(words,new java.lang.String[]{"display", "gui", "window"});
    }
    public void testSplit2() {
        java.util.Collection<java.lang.String> words = splitter2.split();
        this.checkContents(words,new java.lang.String[]{"display", "gui", "window"});
    }
    public void testSplitLong() {
        java.util.Collection<java.lang.String> words = splitterLong.split();
        this.checkContents(words,new java.lang.String[]{"now", "is", "the", "winter", "of", "our", "discontent"});
    }
    public void testAllLower() {
        java.util.Collection<java.lang.String> words = allLower.split();
        this.checkContents(words,new java.lang.String[]{"foobar"});
    }
    public void testAllUpper() {
        java.util.Collection<java.lang.String> words = allUpper.split();
        this.checkContents(words,new java.lang.String[]{"nsa"});
    }
    public void testCapitalized() {
        java.util.Collection<java.lang.String> words = capitalized.split();
        this.checkContents(words,new java.lang.String[]{"maryland"});
    }
    private void checkContents(java.util.Collection<java.lang.String> words, java.lang.String[] expected) {
        junit.framework.Assert.assertEquals(expected.length,words.size());
        for (java.lang.String anExpected : expected){
            junit.framework.Assert.assertTrue(words.contains(anExpected));
        }
;
    }
}
