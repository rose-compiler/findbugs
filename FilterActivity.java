/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import javax.annotation.CheckForNull;
import javax.swing.tree.TreePath;
public class FilterActivity extends java.lang.Object {
    public static class FilterActivityNotifier extends java.lang.Object {
        public FilterActivityNotifier() {
        }
        public void notifyListeners(edu.umd.cs.findbugs.gui2.FilterListener.Action whatsGoingOnCode, javax.swing.tree.TreePath optionalPath) {
            edu.umd.cs.findbugs.gui2.FilterActivity.notifyListeners(whatsGoingOnCode,optionalPath);
        }
    }
    public FilterActivity() {
    }
    final private static java.util.HashSet<edu.umd.cs.findbugs.gui2.FilterListener> listeners = new java.util.HashSet<edu.umd.cs.findbugs.gui2.FilterListener>();
    public static boolean addFilterListener(edu.umd.cs.findbugs.gui2.FilterListener newListener) {
        return listeners.add(newListener);
    }
    public static void removeFilterListener(edu.umd.cs.findbugs.gui2.FilterListener toRemove) {
        listeners.remove(toRemove);
    }
    public static void notifyListeners(edu.umd.cs.findbugs.gui2.FilterListener.Action whatsGoingOnCode, javax.swing.tree.TreePath optionalPath) {
        java.util.Collection<edu.umd.cs.findbugs.gui2.FilterListener> currentListeners = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.FilterListener>(edu.umd.cs.findbugs.gui2.FilterActivity.listeners);
        switch(whatsGoingOnCode){
            case FILTERING:{
            }
            case UNFILTERING:{
                for (edu.umd.cs.findbugs.gui2.FilterListener i : currentListeners)i.clearCache();
;
                break;
            }
        }
        edu.umd.cs.findbugs.gui2.MainFrame.getInstance().updateStatusBar();
    }
}
