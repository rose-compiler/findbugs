/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.cloud.username;
import edu.umd.cs.findbugs.cloud.username.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import edu.umd.cs.findbugs.BugCollection;
import edu.umd.cs.findbugs.PropertyBundle;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.cloud.CloudPlugin;
import edu.umd.cs.findbugs.util.LaunchBrowser;
import edu.umd.cs.findbugs.util.Util;
public class WebCloudNameLookup extends java.lang.Object implements edu.umd.cs.findbugs.cloud.username.NameLookup {
    public WebCloudNameLookup() {
    }
    final private static java.lang.String APPENGINE_HOST_PROPERTY_NAME = "webcloud.host";
    final private static java.lang.String KEY_SAVE_SESSION_INFO = "save_session_info";
    final static java.lang.String KEY_APPENGINECLOUD_SESSION_ID = "webcloud_session_id";
/** if "true", prevents session info from being saved between launches. */
    final private static java.lang.String SYSPROP_NEVER_SAVE_SESSION = "webcloud.never_save_session";
    final private static java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(edu.umd.cs.findbugs.cloud.username.WebCloudNameLookup.class.getName());
    final private static int USER_SIGNIN_TIMEOUT_SECS = 60;
    private java.lang.Long sessionId;
    private java.lang.String username;
    private java.lang.String url;
    public boolean signIn(edu.umd.cs.findbugs.cloud.CloudPlugin plugin, edu.umd.cs.findbugs.BugCollection bugCollection) throws java.io.IOException {
        this.loadProperties(plugin);
        if (this.softSignin()) return true;
        if (sessionId == null) sessionId = this.loadOrCreateSessionId();
        LOGGER.info("Opening browser for session " + sessionId);
        java.net.URL u = new java.net.URL(url + "/browser-auth/" + sessionId);
        edu.umd.cs.findbugs.util.LaunchBrowser.showDocument(u);
        for (int i = 0; i < USER_SIGNIN_TIMEOUT_SECS; i++) {
            if (this.checkAuthorized(this.getAuthCheckUrl(sessionId))) {
                return true;
            }
            try {
                java.lang.Thread.sleep(1000);
            }
            catch (java.lang.InterruptedException e){
                break;
            }
        }
        LOGGER.info("Sign-in timed out for " + sessionId);
// wait 1 minute for the user to sign in
        throw new java.io.IOException("Sign-in timed out");
    }
    public void loadProperties(edu.umd.cs.findbugs.cloud.CloudPlugin plugin) {
        edu.umd.cs.findbugs.PropertyBundle pluginProps = plugin.getProperties();
        url = pluginProps.getProperty(APPENGINE_HOST_PROPERTY_NAME);
        if (url == null) throw new java.lang.IllegalStateException("Host not specified for " + plugin.getId());
    }
/**
     * If the user can be authenticated due to an existing session id, do so
     *
     * @return true if we could authenticate the user
     * @throws IOException
     */
    public boolean softSignin() throws java.io.IOException {
        if (url == null) throw new java.lang.IllegalStateException("Null host");
        this.checkResolveHost();
        if (sessionId != null) {
            if (this.checkAuthorized(this.getAuthCheckUrl(sessionId))) {
                LOGGER.fine("Skipping soft init; session ID already exists - " + sessionId);
                return true;
            }
            else {
                sessionId = null;
            }
        }
// check the previously used session ID
        long id = this.loadSessionId();
        if (id == 0) return false;
        boolean authorized = this.checkAuthorized(this.getAuthCheckUrl(id));
        if (authorized) {
            LOGGER.info("Authorized with session ID: " + id);
            this.sessionId = id;
        }
        return authorized;
    }
    public void checkResolveHost() throws java.net.UnknownHostException {
        try {
            java.lang.String host = new java.net.URL(url).getHost();
            java.net.InetAddress.getByName(host);
        }
        catch (java.net.MalformedURLException e){
            assert true;
        }
    }
    private java.net.URL getAuthCheckUrl(long sessionId) throws java.net.MalformedURLException {
        return new java.net.URL(url + "/check-auth/" + sessionId);
    }
    public static void setSaveSessionInformation(boolean save) {
        java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.cloud.username.WebCloudNameLookup.class);
        prefs.putBoolean(KEY_SAVE_SESSION_INFO,save);
        if ( !save) {
            clearSavedSessionInformation();
        }
    }
    public static boolean isSavingSessionInfoEnabled() {
        return  !java.lang.Boolean.getBoolean(SYSPROP_NEVER_SAVE_SESSION) && java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.cloud.username.WebCloudNameLookup.class).getBoolean(KEY_SAVE_SESSION_INFO,true);
    }
    public static void clearSavedSessionInformation() {
        java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.cloud.username.WebCloudNameLookup.class);
        prefs.remove(KEY_APPENGINECLOUD_SESSION_ID);
    }
    public static void saveSessionInformation(long sessionId) {
        assert sessionId != 0;
        java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.cloud.username.WebCloudNameLookup.class).putLong(KEY_APPENGINECLOUD_SESSION_ID,sessionId);
    }
    public java.lang.Long getSessionId() {
        return sessionId;
    }
    public java.lang.String getUsername() {
        return username;
    }
    public java.lang.String getHost() {
        return url;
    }
// ======================= end of public methods =======================
    private long loadOrCreateSessionId() {
        long id = this.loadSessionId();
        if (id != 0) {
            LOGGER.info("Using saved session ID: " + id);
            return id;
        }
        java.security.SecureRandom r = new java.security.SecureRandom();
        while (id == 0) id = r.nextLong();
//        if (id == 0) { // 0 is reserved for no session id
//            id = 42;
//        }
        if (isSavingSessionInfoEnabled()) saveSessionInformation(id);
        return id;
    }
/**
     * @return session id if already exists, or 0 if it doesn't
     */
    private long loadSessionId() {
        java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userNodeForPackage(edu.umd.cs.findbugs.cloud.username.WebCloudNameLookup.class);
        return prefs.getLong(KEY_APPENGINECLOUD_SESSION_ID,0);
    }
    private boolean checkAuthorized(java.net.URL response) throws java.io.IOException {
        java.net.HttpURLConnection connection = (java.net.HttpURLConnection) (response.openConnection()) ;
        int responseCode = connection.getResponseCode();
        if (responseCode == 200) {
            java.io.InputStream in = connection.getInputStream();
            java.io.BufferedReader reader = edu.umd.cs.findbugs.charsets.UTF8.bufferedReader(in);
            try {
                java.lang.String status = reader.readLine();
                sessionId = java.lang.Long.parseLong(reader.readLine());
                username = reader.readLine();
                edu.umd.cs.findbugs.util.Util.closeSilently(reader);
                if ("OK".equals(status)) {
                    LOGGER.info("Authorized session " + sessionId);
                    return true;
                }
            }
            finally {
                reader.close();
            }
        }
        connection.disconnect();
        return false;
    }
}
