/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A BugAnnotation specifying a particular field in particular class.
 * 
 * @author David Hovemeyer
 * @see BugAnnotation
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FieldInstruction;
import org.apache.bcel.generic.GETFIELD;
import org.apache.bcel.generic.GETSTATIC;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.PUTSTATIC;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.SignatureConverter;
import edu.umd.cs.findbugs.ba.SourceInfoMap;
import edu.umd.cs.findbugs.ba.XFactory;
import edu.umd.cs.findbugs.ba.XField;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.FieldDescriptor;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
import edu.umd.cs.findbugs.util.ClassName;
import edu.umd.cs.findbugs.visitclass.DismantleBytecode;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class FieldAnnotation extends edu.umd.cs.findbugs.PackageMemberAnnotation {
    final private static long serialVersionUID = 1L;
    final public static java.lang.String DEFAULT_ROLE = "FIELD_DEFAULT";
    final public static java.lang.String DID_YOU_MEAN_ROLE = "FIELD_DID_YOU_MEAN";
    final public static java.lang.String VALUE_OF_ROLE = "FIELD_VALUE_OF";
    final public static java.lang.String LOADED_FROM_ROLE = VALUE_OF_ROLE;
    final public static java.lang.String STORED_ROLE = "FIELD_STORED";
    final public static java.lang.String INVOKED_ON_ROLE = "FIELD_INVOKED_ON";
    final public static java.lang.String ARGUMENT_ROLE = "FIELD_ARGUMENT";
    private java.lang.String fieldName;
    private java.lang.String fieldSig;
    private java.lang.String fieldSourceSig;
    private boolean isStatic;
/**
     * Constructor.
     * 
     * @param className
     *            the name of the class containing the field
     * @param fieldName
     *            the name of the field
     * @param fieldSig
     *            the type signature of the field
     */
    public FieldAnnotation(java.lang.String className, java.lang.String fieldName, java.lang.String fieldSig, boolean isStatic) {
        super(className,DEFAULT_ROLE);
        if (fieldSig.indexOf(".") >= 0) {
            assert false : "signatures should not be dotted: " + fieldSig;
            fieldSig = fieldSig.replace('\u002e','\u002f');
        }
        this.fieldName = fieldName;
        this.fieldSig = fieldSig;
        this.isStatic = isStatic;
    }
    public FieldAnnotation(java.lang.String className, java.lang.String fieldName, java.lang.String fieldSig, java.lang.String fieldSourceSig, boolean isStatic) {
        this(className,fieldName,fieldSig,isStatic);
        this.fieldSourceSig = fieldSourceSig;
    }
/**
     * Constructor.
     * 
     * @param className
     *            the name of the class containing the field
     * @param fieldName
     *            the name of the field
     * @param fieldSig
     *            the type signature of the field
     * @param accessFlags
     *            accessFlags for the field
     */
    public FieldAnnotation(java.lang.String className, java.lang.String fieldName, java.lang.String fieldSig, int accessFlags) {
        this(className,fieldName,fieldSig,(accessFlags & org.apache.bcel.Constants.ACC_STATIC) != 0);
    }
/**
     * Factory method. Class name, field name, and field signatures are taken
     * from the given visitor, which is visiting the field.
     * 
     * @param visitor
     *            the visitor which is visiting the field
     * @return the FieldAnnotation object
     */
    public static edu.umd.cs.findbugs.FieldAnnotation fromVisitedField(edu.umd.cs.findbugs.visitclass.PreorderVisitor visitor) {
        return new edu.umd.cs.findbugs.FieldAnnotation(visitor.getDottedClassName(), visitor.getFieldName(), visitor.getFieldSig(), visitor.getFieldIsStatic());
    }
/**
     * Factory method. Class name, field name, and field signatures are taken
     * from the given visitor, which is visiting a reference to the field (i.e.,
     * a getfield or getstatic instruction).
     * 
     * @param visitor
     *            the visitor which is visiting the field reference
     * @return the FieldAnnotation object
     */
    public static edu.umd.cs.findbugs.FieldAnnotation fromReferencedField(edu.umd.cs.findbugs.visitclass.DismantleBytecode visitor) {
        java.lang.String className = visitor.getDottedClassConstantOperand();
        return new edu.umd.cs.findbugs.FieldAnnotation(className, visitor.getNameConstantOperand(), visitor.getSigConstantOperand(), visitor.getRefFieldIsStatic());
    }
/**
     * Factory method. Construct from class name and BCEL Field object.
     * 
     * @param className
     *            the name of the class which defines the field
     * @param field
     *            the BCEL Field object
     * @return the FieldAnnotation
     */
    public static edu.umd.cs.findbugs.FieldAnnotation fromBCELField(java.lang.String className, org.apache.bcel.classfile.Field field) {
        return new edu.umd.cs.findbugs.FieldAnnotation(className, field.getName(), field.getSignature(), field.isStatic());
    }
/**
     * Factory method. Construct from class name and BCEL Field object.
     * 
     * @param jClass
     *            the class which defines the field
     * @param field
     *            the BCEL Field object
     * @return the FieldAnnotation
     */
    public static edu.umd.cs.findbugs.FieldAnnotation fromBCELField(org.apache.bcel.classfile.JavaClass jClass, org.apache.bcel.classfile.Field field) {
        return new edu.umd.cs.findbugs.FieldAnnotation(jClass.getClassName(), field.getName(), field.getSignature(), field.isStatic());
    }
/**
     * Factory method. Construct from a FieldDescriptor.
     * 
     * @param fieldDescriptor
     *            the FieldDescriptor
     * @return the FieldAnnotation
     */
    public static edu.umd.cs.findbugs.FieldAnnotation fromFieldDescriptor(edu.umd.cs.findbugs.classfile.FieldDescriptor fieldDescriptor) {
        return new edu.umd.cs.findbugs.FieldAnnotation(fieldDescriptor.getClassDescriptor().getDottedClassName(), fieldDescriptor.getName(), fieldDescriptor.getSignature(), fieldDescriptor.isStatic());
    }
    public static edu.umd.cs.findbugs.FieldAnnotation fromXField(edu.umd.cs.findbugs.ba.XField fieldDescriptor) {
        return new edu.umd.cs.findbugs.FieldAnnotation(fieldDescriptor.getClassName(), fieldDescriptor.getName(), fieldDescriptor.getSignature(), fieldDescriptor.getSourceSignature(), fieldDescriptor.isStatic());
    }
    public edu.umd.cs.findbugs.ba.XField toXField() {
        return edu.umd.cs.findbugs.ba.XFactory.createXField(className,fieldName,fieldSig,isStatic);
    }
    public edu.umd.cs.findbugs.classfile.FieldDescriptor toFieldDescriptor() {
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getFieldDescriptor(this);
    }
/**
     * Get the field name.
     */
    public java.lang.String getFieldName() {
        return fieldName;
    }
/**
     * Get the type signature of the field.
     */
    public java.lang.String getFieldSignature() {
        return fieldSig;
    }
/**
     * Return whether or not the field is static.
     */
    public boolean isStatic() {
        return isStatic;
    }
/**
     * Is the given instruction a read of a field?
     * 
     * @param ins
     *            the Instruction to check
     * @param cpg
     *            ConstantPoolGen of the method containing the instruction
     * @return the Field if the instruction is a read of a field, null otherwise
     */
    public static edu.umd.cs.findbugs.FieldAnnotation isRead(org.apache.bcel.generic.Instruction ins, org.apache.bcel.generic.ConstantPoolGen cpg) {
        if (ins instanceof org.apache.bcel.generic.GETFIELD || ins instanceof org.apache.bcel.generic.GETSTATIC) {
            org.apache.bcel.generic.FieldInstruction fins = (org.apache.bcel.generic.FieldInstruction) (ins) ;
            java.lang.String className = fins.getClassName(cpg);
            return new edu.umd.cs.findbugs.FieldAnnotation(className, fins.getName(cpg), fins.getSignature(cpg), fins instanceof org.apache.bcel.generic.GETSTATIC);
        }
        else return null;
    }
/**
     * Is the instruction a write of a field?
     * 
     * @param ins
     *            the Instruction to check
     * @param cpg
     *            ConstantPoolGen of the method containing the instruction
     * @return the Field if instruction is a write of a field, null otherwise
     */
    public static edu.umd.cs.findbugs.FieldAnnotation isWrite(org.apache.bcel.generic.Instruction ins, org.apache.bcel.generic.ConstantPoolGen cpg) {
        if (ins instanceof org.apache.bcel.generic.PUTFIELD || ins instanceof org.apache.bcel.generic.PUTSTATIC) {
            org.apache.bcel.generic.FieldInstruction fins = (org.apache.bcel.generic.FieldInstruction) (ins) ;
            java.lang.String className = fins.getClassName(cpg);
            return new edu.umd.cs.findbugs.FieldAnnotation(className, fins.getName(cpg), fins.getSignature(cpg), fins instanceof org.apache.bcel.generic.PUTSTATIC);
        }
        else return null;
    }
    public void accept(edu.umd.cs.findbugs.BugAnnotationVisitor visitor) {
        visitor.visitFieldAnnotation(this);
    }
    protected java.lang.String formatPackageMember(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        if (key.equals("") || key.equals("hash")) return className + "." + fieldName;
        else if (key.equals("givenClass")) {
            java.lang.String primaryClassName = primaryClass.getClassName();
            if (className.equals(primaryClassName)) return this.getNameInClass(primaryClass);
            else return shorten(primaryClass.getPackageName(),className) + "." + fieldName;
        }
        else if (key.equals("name")) return fieldName;
        else if (key.equals("fullField")) {
            edu.umd.cs.findbugs.ba.SignatureConverter converter = new edu.umd.cs.findbugs.ba.SignatureConverter(fieldSig);
            java.lang.StringBuilder result = new java.lang.StringBuilder();
            if (isStatic) result.append("static ");
            result.append(converter.parseNext());
            result.append('\u0020');
            result.append(className);
            result.append('\u002e');
            result.append(fieldName);
            return result.toString();
        }
        else throw new java.lang.IllegalArgumentException("unknown key " + key);
    }
/**
     * @param primaryClass
     * @return
     */
    private java.lang.String getNameInClass(edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        if (primaryClass == null) return className + "." + fieldName;
        java.lang.String givenPackageName = primaryClass.getPackageName();
        java.lang.String thisPackageName = this.getPackageName();
        if (thisPackageName.equals(givenPackageName)) if (thisPackageName.length() == 0) return fieldName;
        else return className.substring(thisPackageName.length() + 1) + "." + fieldName;
        return className + "." + fieldName;
    }
    public int hashCode() {
        return className.hashCode() + fieldName.hashCode() + fieldSig.hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.FieldAnnotation)) return false;
        edu.umd.cs.findbugs.FieldAnnotation other = (edu.umd.cs.findbugs.FieldAnnotation) (o) ;
        return className.equals(other.className) && fieldName.equals(other.fieldName) && fieldSig.equals(other.fieldSig) && isStatic == other.isStatic;
    }
    public int compareTo(edu.umd.cs.findbugs.BugAnnotation o) {
// BugAnnotations must be
        if ( !(o instanceof edu.umd.cs.findbugs.FieldAnnotation)) 
// Comparable with any type of
// BugAnnotation
        return this.getClass().getName().compareTo(o.getClass().getName());
        edu.umd.cs.findbugs.FieldAnnotation other = (edu.umd.cs.findbugs.FieldAnnotation) (o) ;
        int cmp;
        cmp = className.compareTo(other.className);
        if (cmp != 0) return cmp;
        cmp = fieldName.compareTo(other.fieldName);
        if (cmp != 0) return cmp;
        return fieldSig.compareTo(other.fieldSig);
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.PackageMemberAnnotation#getSourceLines()
     */
    public edu.umd.cs.findbugs.SourceLineAnnotation getSourceLines() {
        if (sourceLines == null) {
// Create source line annotation for field on demand
            edu.umd.cs.findbugs.ba.AnalysisContext currentAnalysisContext = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext();
            if (currentAnalysisContext == null) sourceLines = new edu.umd.cs.findbugs.SourceLineAnnotation(className, sourceFileName,  -1,  -1,  -1,  -1);
            else {
                edu.umd.cs.findbugs.ba.SourceInfoMap.SourceLineRange fieldLine = currentAnalysisContext.getSourceInfoMap().getFieldLine(className,fieldName);
                if (fieldLine == null) sourceLines = new edu.umd.cs.findbugs.SourceLineAnnotation(className, sourceFileName,  -1,  -1,  -1,  -1);
                else sourceLines = new edu.umd.cs.findbugs.SourceLineAnnotation(className, sourceFileName, fieldLine.getStart(), fieldLine.getEnd(),  -1,  -1);
            }
        }
        return sourceLines;
    }
/*
     * ----------------------------------------------------------------------
     * XML Conversion support
     * ----------------------------------------------------------------------
     */
    final private static java.lang.String ELEMENT_NAME = "Field";
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        this.writeXML(xmlOutput,false,false);
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean addMessages, boolean isPrimary) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributeList = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("classname",this.getClassName()).addAttribute("name",this.getFieldName()).addAttribute("signature",this.getFieldSignature());
        if (fieldSourceSig != null) attributeList.addAttribute("sourceSignature",fieldSourceSig);
        attributeList.addAttribute("isStatic",java.lang.String.valueOf(this.isStatic()));
        if (isPrimary) attributeList.addAttribute("primary","true");
        java.lang.String role = this.getDescription();
        if ( !role.equals(DEFAULT_ROLE)) attributeList.addAttribute("role",role);
        xmlOutput.openTag(ELEMENT_NAME,attributeList);
        this.getSourceLines().writeXML(xmlOutput,addMessages,false);
        if (addMessages) {
            xmlOutput.openTag(edu.umd.cs.findbugs.BugAnnotation.MESSAGE_TAG);
            xmlOutput.writeText(this.toString());
            xmlOutput.closeTag(edu.umd.cs.findbugs.BugAnnotation.MESSAGE_TAG);
        }
        xmlOutput.closeTag(ELEMENT_NAME);
    }
}
// vim:ts=4
