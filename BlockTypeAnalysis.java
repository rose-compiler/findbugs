/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//import org.apache.bcel.classfile.Method;
/**
 * Dataflow analysis to determine the nesting of catch and finally blocks within
 * a method.
 * 
 * @see BlockType
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import javax.annotation.CheckForNull;
import org.apache.bcel.generic.CodeExceptionGen;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.ObjectType;
public class BlockTypeAnalysis extends edu.umd.cs.findbugs.ba.BasicAbstractDataflowAnalysis<edu.umd.cs.findbugs.ba.BlockType> {
    private edu.umd.cs.findbugs.ba.DepthFirstSearch dfs;
/**
     * Constructor.
     * 
     * @param dfs
     *            a DepthFirstSearch for the method to be analyzed
     */
    public BlockTypeAnalysis(edu.umd.cs.findbugs.ba.DepthFirstSearch dfs) {
        super();
        this.dfs = dfs;
    }
    public edu.umd.cs.findbugs.ba.BlockType createFact() {
        return new edu.umd.cs.findbugs.ba.BlockType();
    }
    public void copy(edu.umd.cs.findbugs.ba.BlockType source, edu.umd.cs.findbugs.ba.BlockType dest) {
        dest.copyFrom(source);
    }
    public void initEntryFact(edu.umd.cs.findbugs.ba.BlockType result) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        result.setNormal();
    }
    public void makeFactTop(edu.umd.cs.findbugs.ba.BlockType fact) {
        fact.setTop();
    }
    public boolean isTop(edu.umd.cs.findbugs.ba.BlockType fact) {
        return fact.isTop();
    }
    public boolean isForwards() {
        return true;
    }
    public edu.umd.cs.findbugs.ba.BlockOrder getBlockOrder(edu.umd.cs.findbugs.ba.CFG cfg) {
        return new edu.umd.cs.findbugs.ba.ReversePostOrder(cfg, dfs);
    }
    public boolean same(edu.umd.cs.findbugs.ba.BlockType fact1, edu.umd.cs.findbugs.ba.BlockType fact2) {
        return fact1.sameAs(fact2);
    }
    public void transfer(edu.umd.cs.findbugs.ba.BasicBlock basicBlock, org.apache.bcel.generic.InstructionHandle end, edu.umd.cs.findbugs.ba.BlockType start, edu.umd.cs.findbugs.ba.BlockType result) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        result.copyFrom(start);
        if (start.isValid()) {
            if (basicBlock.isExceptionHandler()) {
                org.apache.bcel.generic.CodeExceptionGen exceptionGen = basicBlock.getExceptionGen();
                org.apache.bcel.generic.ObjectType catchType = exceptionGen.getCatchType();
                if (catchType == null) {
                    result.pushFinally();
                }
                else {
                    result.pushCatch();
                }
            }
        }
    }
    public void meetInto(edu.umd.cs.findbugs.ba.BlockType fact, edu.umd.cs.findbugs.ba.Edge edge, edu.umd.cs.findbugs.ba.BlockType result) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        result.mergeWith(fact);
    }
}
// public static void main(String[] argv) throws Exception {
// if (argv.length != 1) {
// System.err.println("Usage: " + BlockTypeAnalysis.class.getName() +
// " <classfile>");
// System.exit(1);
// }
//
// DataflowTestDriver<BlockType, BlockTypeAnalysis> driver = new
// DataflowTestDriver<BlockType, BlockTypeAnalysis>() {
// /* (non-Javadoc)
// * @see
// edu.umd.cs.findbugs.ba.DataflowTestDriver#createDataflow(edu.umd.cs.findbugs.ba.ClassContext,
// org.apache.bcel.classfile.Method)
// */
// @Override
// public Dataflow<BlockType, BlockTypeAnalysis> createDataflow(ClassContext
// classContext, Method method) throws CFGBuilderException,
// DataflowAnalysisException {
// return classContext.getBlockTypeDataflow(method);
// }
// };
//
// driver.execute(argv[0]);
// }
// vim:ts=4
