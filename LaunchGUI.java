/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.launchGUI;
import edu.umd.cs.findbugs.launchGUI.*;
import java.awt.GraphicsEnvironment;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.gui2.FindBugsLayoutManagerFactory;
import edu.umd.cs.findbugs.gui2.GUISaveState;
import edu.umd.cs.findbugs.gui2.MainFrame;
import edu.umd.cs.findbugs.gui2.SplitLayout;
public class LaunchGUI extends java.lang.Object {
    public LaunchGUI() {
    }
    public static void launchGUI(edu.umd.cs.findbugs.SortedBugCollection bugs) {
        if (java.awt.GraphicsEnvironment.isHeadless()) {
            throw new java.lang.IllegalStateException("Running in GUI headless mode, can't open GUI");
        }
        edu.umd.cs.findbugs.gui2.GUISaveState.loadInstance();
        try {
            edu.umd.cs.findbugs.gui2.FindBugsLayoutManagerFactory factory = new edu.umd.cs.findbugs.gui2.FindBugsLayoutManagerFactory(edu.umd.cs.findbugs.gui2.SplitLayout.class.getName());
            edu.umd.cs.findbugs.gui2.MainFrame.makeInstance(factory);
            edu.umd.cs.findbugs.gui2.MainFrame instance = edu.umd.cs.findbugs.gui2.MainFrame.getInstance();
            instance.waitUntilReady();
            instance.openBugCollection(bugs);
        }
        catch (java.lang.RuntimeException e){
            throw e;
        }
        catch (java.lang.Exception e){
            throw new java.lang.RuntimeException(e);
        }
    }
}
