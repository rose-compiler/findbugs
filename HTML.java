/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.util;
import edu.umd.cs.findbugs.util.*;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTMLWriter;
public class HTML extends java.lang.Object {
    final private static class HTMLtoPlainTextWriter2 extends javax.swing.text.html.HTMLWriter {
        boolean inPre = false;
        boolean startingParagraph = false;
/**
         * @param w
         * @param doc
         */
        public HTMLtoPlainTextWriter2(java.io.Writer w, javax.swing.text.html.HTMLDocument doc) {
            super(w,doc);
            this.setLineLength(80);
            this.setCanWrapLines(true);
        }
        protected void startTag(javax.swing.text.Element elem) throws java.io.IOException {
            java.lang.String name = elem.getName();
            startingParagraph = true;
            if (name.equals("ul")) {
                super.incrIndent();
                this.write("  ");
            }
            else if (name.equals("pre")) {
                inPre = true;
            }
            else if (name.equals("li")) {
                super.incrIndent();
                this.write("* ");
            }
            else if (name.equals("p")) {
            }
        }
        protected void writeEmbeddedTags(javax.swing.text.AttributeSet attr) throws java.io.IOException {
        }
        protected void endTag(javax.swing.text.Element elem) throws java.io.IOException {
            java.lang.String name = elem.getName();
            if (name.equals("p")) {
                this.writeLineSeparator();
                this.indent();
            }
            else if (name.equals("pre")) {
                inPre = false;
            }
            else if (name.equals("ul")) {
                super.decrIndent();
                this.writeLineSeparator();
                this.indent();
            }
            else if (name.equals("li")) {
                super.decrIndent();
                this.writeLineSeparator();
                this.indent();
            }
        }
        protected void incrIndent() {
        }
        protected void decrIndent() {
        }
        protected void emptyTag(javax.swing.text.Element elem) throws javax.swing.text.BadLocationException, java.io.IOException {
            if (elem.getName().equals("content")) super.emptyTag(elem);
        }
        protected void text(javax.swing.text.Element elem) throws javax.swing.text.BadLocationException, java.io.IOException {
            java.lang.String contentStr = this.getText(elem);
            if ( !inPre) {
                contentStr = contentStr.replaceAll("\\s+"," ");
                if (startingParagraph) {
                    while (contentStr.length() > 0 && contentStr.charAt(0) == '\u0020') contentStr = contentStr.substring(1);
                }
                startingParagraph = false;
            }
            if (contentStr.length() > 0) {
                this.setCanWrapLines( !inPre);
                this.write(contentStr);
            }
        }
    }
    public HTML() {
        super();
    }
    public static void convertHtmlToText(java.io.Reader reader, java.io.Writer writer) throws javax.swing.text.BadLocationException, java.io.IOException {
        javax.swing.text.EditorKit kit = new javax.swing.text.html.HTMLEditorKit();
        javax.swing.text.html.HTMLDocument doc = new javax.swing.text.html.HTMLDocument();
        kit.read(reader,doc,0);
        edu.umd.cs.findbugs.util.HTML.HTMLtoPlainTextWriter2 x = new edu.umd.cs.findbugs.util.HTML.HTMLtoPlainTextWriter2(writer, doc);
        x.write();
        writer.close();
    }
    public static java.lang.String convertHtmlSnippetToText(java.lang.String htmlSnippet) throws javax.swing.text.BadLocationException, java.io.IOException {
        java.io.StringWriter writer = new java.io.StringWriter();
        java.io.StringReader reader = new java.io.StringReader("<HTML><BODY>" + htmlSnippet + "</BODY></HTML>");
        convertHtmlToText(reader,writer);
        return writer.toString();
    }
}
