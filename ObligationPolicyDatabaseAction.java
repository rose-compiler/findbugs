/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2008, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * An action applied by an entry in the ObligationPolicyDatabase. Adds or
 * removes an obligation.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba.obl;
import edu.umd.cs.findbugs.ba.obl.*;
import javax.annotation.Nullable;
public class ObligationPolicyDatabaseAction extends java.lang.Object {
    final private edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseActionType actionType;
    final private edu.umd.cs.findbugs.ba.obl.Obligation obligation;
    final public static edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseAction CLEAR = new edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseAction(edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseActionType.CLEAR, null);
    public ObligationPolicyDatabaseAction(edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseActionType actionType, edu.umd.cs.findbugs.ba.obl.Obligation obligation) {
        super();
        this.actionType = actionType;
        this.obligation = obligation;
    }
    public edu.umd.cs.findbugs.ba.obl.ObligationPolicyDatabaseActionType getActionType() {
        return actionType;
    }
    public edu.umd.cs.findbugs.ba.obl.Obligation getObligation() {
        return obligation;
    }
    public void apply(edu.umd.cs.findbugs.ba.obl.StateSet stateSet, int basicBlockId) throws edu.umd.cs.findbugs.ba.obl.ObligationAcquiredOrReleasedInLoopException {
        switch(actionType){
            case ADD:{
                stateSet.addObligation(obligation,basicBlockId);
                break;
            }
            case DEL:{
                stateSet.deleteObligation(obligation,basicBlockId);
                break;
            }
            case CLEAR:{
                stateSet.clear();
                break;
            }
            default:{
                assert false;
            }
        }
    }
    public java.lang.String toString() {
        return "[" + actionType + " " + obligation + "]";
    }
}
