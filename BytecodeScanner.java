package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Scan the raw bytecodes of a method. This is useful in order to find out
 * quickly whether or not a method uses particular instructions.
 * 
 * @author David Hovemeyer
 */
/**
     * Callback interface to report scanned instructions.
     */
/**
         * Called to indicate that a particular bytecode has been scanned.
         * 
         * @param opcode
         *            the opcode of the instruction
         * @param index
         *            the bytecode offset of the instruction
         */
/**
     * Convert the unsigned value of a byte into a short.
     * 
     * @param value
     *            the byte
     * @return the byte's unsigned value as a short
     */
/**
     * Extract an int from bytes at the given offset in the array.
     * 
     * @param arr
     *            the array
     * @param offset
     *            the offset in the array
     */
/**
     * Scan the raw bytecodes of a method.
     * 
     * @param instructionList
     *            the bytecodes
     * @param callback
     *            the callback object
     */
// Single byte instructions.
// Two byte instructions.
// Instructions that can be used with the WIDE prefix.
// Skip opcode and two immediate bytes.
// Skip opcode and one immediate byte.
// IINC is a special case for WIDE handling
// Skip opcode, two byte index, and two byte immediate
// value.
// Skip opcode, one byte index, and one byte immedate value.
// Three byte instructions.
// Four byte instructions.
// Five byte instructions.
// TABLESWITCH - variable length.
// Skip padding.
// skip the opcode
// offset should now be posited at the default value
// Extract min and max values.
// Skip to next instruction.
// LOOKUPSWITCH - variable length.
// Skip padding.
// skip the opcode
// offset should now be posited at the default value
// Extract number of value/offset pairs.
// Skip to next instruction.
// Wide prefix.
// vim:ts=4
abstract interface package-info {
}
