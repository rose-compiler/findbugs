package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.BitSet;
import java.util.Iterator;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.ReferenceType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.DeepSubtypeAnalysis;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.TypeAnnotation;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.type.NullType;
import edu.umd.cs.findbugs.ba.type.TopType;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.ba.type.TypeFrame;
public class FindNonSerializableValuePassedToWriteObject extends java.lang.Object implements edu.umd.cs.findbugs.Detector {
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    final private static boolean DEBUG = false;
    public FindNonSerializableValuePassedToWriteObject(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.Method[] methodList = classContext.getJavaClass().getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("Detector " + this.getClass().getName() + " caught exception",e);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
            }
        }
;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
        org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
        if (methodGen == null) return;
        java.util.BitSet bytecodeSet = classContext.getBytecodeSet(method);
        if (bytecodeSet == null) return;
// We don't adequately model instanceof interfaces yet
        if (bytecodeSet.get(org.apache.bcel.Constants.INSTANCEOF) || bytecodeSet.get(org.apache.bcel.Constants.CHECKCAST)) return;
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = classContext.getTypeDataflow(method);
        org.apache.bcel.generic.ConstantPoolGen cpg = classContext.getConstantPoolGen();
        java.lang.String sourceFile = classContext.getJavaClass().getSourceFileName();
        if (DEBUG) {
            java.lang.String methodName = methodGen.getClassName() + "." + methodGen.getName();
            java.lang.System.out.println("Checking " + methodName);
        }
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
            int pc = handle.getPosition();
            org.apache.bcel.generic.Instruction ins = handle.getInstruction();
            if ( !(ins instanceof org.apache.bcel.generic.InvokeInstruction)) continue;
            org.apache.bcel.generic.InvokeInstruction invoke = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
            java.lang.String mName = invoke.getMethodName(cpg);
            if ( !mName.equals("writeObject")) continue;
            java.lang.String cName = invoke.getClassName(cpg);
            if ( !cName.equals("java.io.ObjectOutput") &&  !cName.equals("java.io.ObjectOutputStream")) continue;
            edu.umd.cs.findbugs.ba.type.TypeFrame frame = typeDataflow.getFactAtLocation(location);
            if ( !frame.isValid()) {
                continue;
            }
            org.apache.bcel.generic.Type operandType = frame.getTopValue();
            if (operandType.equals(edu.umd.cs.findbugs.ba.type.TopType.instance())) {
                continue;
            }
            if ( !(operandType instanceof org.apache.bcel.generic.ReferenceType)) {
                continue;
            }
            org.apache.bcel.generic.ReferenceType refType = (org.apache.bcel.generic.ReferenceType) (operandType) ;
            if (refType.equals(edu.umd.cs.findbugs.ba.type.NullType.instance())) {
                continue;
            }
            try {
                double isSerializable = edu.umd.cs.findbugs.DeepSubtypeAnalysis.isDeepSerializable(refType);
                if (isSerializable >= 0.9) continue;
                org.apache.bcel.generic.ReferenceType problem = edu.umd.cs.findbugs.DeepSubtypeAnalysis.getLeastSerializableTypeComponent(refType);
                double isRemote = edu.umd.cs.findbugs.DeepSubtypeAnalysis.isDeepRemote(refType);
                if (isRemote >= 0.9) continue;
                if (isSerializable < isRemote) isSerializable = isRemote;
                edu.umd.cs.findbugs.SourceLineAnnotation sourceLineAnnotation = edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,methodGen,sourceFile,handle);
                bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "DMI_NONSERIALIZABLE_OBJECT_WRITTEN", isSerializable < 0.15 ? HIGH_PRIORITY : isSerializable > 0.5 ? LOW_PRIORITY : NORMAL_PRIORITY).addClassAndMethod(methodGen,sourceFile).addType(problem).describe(edu.umd.cs.findbugs.TypeAnnotation.FOUND_ROLE).addSourceLine(sourceLineAnnotation));
            }
            catch (java.lang.ClassNotFoundException e){
            }
        }
    }
// This basic block is probably dead
// unreachable
// Shouldn't happen - illegal bytecode
// ignore
    public void report() {
    }
}
