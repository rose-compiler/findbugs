/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Abstract base class for BugAnnotations describing constructs which are
 * contained in a Java package. Specifically, this includes classes, methods,
 * and fields.
 * 
 * @author David Hovemeyer
 * @see BugAnnotation
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
import edu.umd.cs.findbugs.internalAnnotations.SlashedClassName;
import edu.umd.cs.findbugs.util.ClassName;
abstract public class PackageMemberAnnotation extends edu.umd.cs.findbugs.BugAnnotationWithSourceLines {
    final private static long serialVersionUID =  -8208567669352996892L;
    final protected java.lang.String className;
    protected java.lang.String description;
/**
     * Constructor.
     * 
     * @param className
     *            name of the class
     */
    public PackageMemberAnnotation(java.lang.String className, java.lang.String description) {
        this(className,description,computeSourceFile(className));
    }
    private static java.lang.String computeSourceFile(java.lang.String className) {
        edu.umd.cs.findbugs.ba.AnalysisContext context = edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext();
        if (context != null) return context.lookupSourceFile(className);
        return edu.umd.cs.findbugs.SourceLineAnnotation.UNKNOWN_SOURCE_FILE;
    }
/**
     * Constructor.
     * 
     * @param className
     *            name of the class
     */
    public PackageMemberAnnotation(java.lang.String className, java.lang.String description, java.lang.String sourceFileName) {
        super();
        if (className.length() == 0) throw new java.lang.IllegalArgumentException("Empty classname not allowed");
        if (className.indexOf('\u002f') >= 0) {
            assert false : "classname " + className + " should be dotted";
            className = className.replace('\u002f','\u002e');
        }
        this.className = edu.umd.cs.findbugs.classfile.DescriptorFactory.canonicalizeString(className);
        this.sourceFileName = sourceFileName;
        if (description != null) description = description.intern();
        this.description = description;
    }
/**
     * Get the dotted class name.
     */
    final public java.lang.String getClassName() {
        return className;
    }
/**
     * Get the dotted class name.
     */
    final public java.lang.String getSlashedClassName() {
        return edu.umd.cs.findbugs.util.ClassName.toSlashedClassName(className);
    }
/**
     * Get the class descriptor.
     */
    final public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor() {
        return edu.umd.cs.findbugs.classfile.DescriptorFactory.instance().getClassDescriptorForDottedClassName(className);
    }
/**
     * Get the package name.
     */
    final public java.lang.String getPackageName() {
        int lastDot = className.lastIndexOf('\u002e');
        if (lastDot < 0) return "";
        else return className.substring(0,lastDot);
    }
/**
     * Format the annotation. Note that this version (defined by
     * PackageMemberAnnotation) only handles the "class" and "package" keys, and
     * calls formatPackageMember() for all other keys.
     * 
     * @param key
     *            the key
     * @return the formatted annotation
     */
    final public java.lang.String format(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        if (key.equals("class.givenClass")) return shorten(primaryClass.getPackageName(),className);
        if (key.equals("simpleClass")) return edu.umd.cs.findbugs.util.ClassName.extractSimpleName(className);
        if (key.equals("class")) return className;
        if (key.equals("package")) return this.getPackageName();
        if (key.equals("") && edu.umd.cs.findbugs.FindBugsDisplayFeatures.isAbridgedMessages() && primaryClass != null) return this.formatPackageMember("givenClass",primaryClass);
        return this.formatPackageMember(key,primaryClass);
    }
    public void setDescription(java.lang.String description) {
        this.description = description.intern();
    }
    public java.lang.String getDescription() {
        return description;
    }
/**
     * Shorten a type name of remove extraneous components. Candidates for
     * shortening are classes in same package as this annotation and classes in
     * the <code>java.lang</code> package.
     */
    protected static java.lang.String shorten(java.lang.String pkgName, java.lang.String typeName) {
        int index = typeName.lastIndexOf('\u002e');
        if (index >= 0) {
            java.lang.String otherPkg = typeName.substring(0,index);
            if (otherPkg.equals(pkgName) || otherPkg.equals("java.lang")) typeName = typeName.substring(index + 1);
        }
        return typeName;
    }
    protected static java.lang.String removePackage(java.lang.String typeName) {
        int index = typeName.lastIndexOf('\u002e');
        if (index >= 0) {
            return typeName.substring(index + 1);
        }
        return typeName;
    }
/**
     * Shorten a type name by removing the package name
     */
    protected static java.lang.String removePackageName(java.lang.String typeName) {
        int index = typeName.lastIndexOf('\u002e');
        if (index >= 0) {
            typeName = typeName.substring(index + 1);
        }
        return typeName;
    }
/**
     * Do default and subclass-specific formatting.
     * 
     * @param key
     *            the key specifying how to do the formatting
     * @param primaryClass
     *            TODO
     */
    abstract protected java.lang.String formatPackageMember(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass);
/**
     * All PackageMemberAnnotation object share a common toString()
     * implementation. It uses the annotation description as a pattern for
     * FindBugsMessageFormat, passing a reference to this object as the single
     * message parameter.
     */
    public java.lang.String toString() {
        return this.toString(null);
    }
    public java.lang.String toString(edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        java.lang.String pattern = edu.umd.cs.findbugs.I18N.instance().getAnnotationDescription(description);
        edu.umd.cs.findbugs.FindBugsMessageFormat format = new edu.umd.cs.findbugs.FindBugsMessageFormat(pattern);
        return format.format(new edu.umd.cs.findbugs.BugAnnotation[]{this},primaryClass);
    }
    public boolean isSignificant() {
        return true;
    }
}
// vim:ts=4
