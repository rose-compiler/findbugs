/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.CheckForNull;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.Attribute;
import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.Deprecated;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.ClassAnnotation;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.MethodAnnotation;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.Hierarchy2;
import edu.umd.cs.findbugs.ba.SignatureParser;
import edu.umd.cs.findbugs.ba.XClass;
import edu.umd.cs.findbugs.ba.XFactory;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.DescriptorFactory;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.props.AbstractWarningProperty;
import edu.umd.cs.findbugs.props.PriorityAdjustment;
import edu.umd.cs.findbugs.props.WarningPropertySet;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
public class Naming extends edu.umd.cs.findbugs.visitclass.PreorderVisitor implements edu.umd.cs.findbugs.Detector {
    public static class NamingProperty extends edu.umd.cs.findbugs.props.AbstractWarningProperty {
        public NamingProperty(java.lang.String name, edu.umd.cs.findbugs.props.PriorityAdjustment priorityAdjustment) {
            super(name,priorityAdjustment);
        }
        final public static edu.umd.cs.findbugs.detect.Naming.NamingProperty METHOD_IS_CALLED = new edu.umd.cs.findbugs.detect.Naming.NamingProperty("CONFUSING_METHOD_IS_CALLED", edu.umd.cs.findbugs.props.PriorityAdjustment.AT_MOST_MEDIUM);
        final public static edu.umd.cs.findbugs.detect.Naming.NamingProperty METHOD_IS_DEPRECATED = new edu.umd.cs.findbugs.detect.Naming.NamingProperty("CONFUSING_METHOD_IS_DEPRECATED", edu.umd.cs.findbugs.props.PriorityAdjustment.LOWER_PRIORITY);
    }
    java.lang.String baseClassName;
    boolean classIsPublicOrProtected;
    public static edu.umd.cs.findbugs.ba.XMethod definedIn(org.apache.bcel.classfile.JavaClass clazz, edu.umd.cs.findbugs.ba.XMethod m) {
        for (org.apache.bcel.classfile.Method m2 : clazz.getMethods())if (m.getName().equals(m2.getName()) && m.getSignature().equals(m2.getSignature()) && m.isStatic() == m2.isStatic()) return edu.umd.cs.findbugs.ba.XFactory.createXMethod(clazz,m2);
;
        return null;
    }
    public static boolean confusingMethodNamesWrongCapitalization(edu.umd.cs.findbugs.ba.XMethod m1, edu.umd.cs.findbugs.ba.XMethod m2) {
        if (m1.isStatic() != m2.isStatic()) return false;
        if (m1.getClassName().equals(m2.getClassName())) return false;
        if (m1.getName().equals(m2.getName())) return false;
        if (m1.getName().equalsIgnoreCase(m2.getName()) && removePackageNamesFromSignature(m1.getSignature()).equals(removePackageNamesFromSignature(m2.getSignature()))) return true;
        return false;
    }
    public static boolean confusingMethodNamesWrongPackage(edu.umd.cs.findbugs.ba.XMethod m1, edu.umd.cs.findbugs.ba.XMethod m2) {
        if (m1.isStatic() != m2.isStatic()) return false;
        if (m1.getClassName().equals(m2.getClassName())) return false;
        if ( !m1.getName().equals(m2.getName())) return false;
        if (m1.getSignature().equals(m2.getSignature())) return false;
        if (removePackageNamesFromSignature(m1.getSignature()).equals(removePackageNamesFromSignature(m2.getSignature()))) return true;
        return false;
    }
// map of canonicalName -> Set<XMethod>
    java.util.HashMap<java.lang.String, java.util.TreeSet<edu.umd.cs.findbugs.ba.XMethod>> canonicalToXMethod = new java.util.HashMap<java.lang.String, java.util.TreeSet<edu.umd.cs.findbugs.ba.XMethod>>();
    java.util.HashSet<java.lang.String> visited = new java.util.HashSet<java.lang.String>();
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    public Naming(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        classContext.getJavaClass().accept(this);
    }
    private boolean checkSuper(edu.umd.cs.findbugs.ba.XMethod m, java.util.Set<edu.umd.cs.findbugs.ba.XMethod> others) {
        if (m.isStatic()) return false;
        if (m.getName().equals("<init>") || m.getName().equals("<clinit>")) return false;
        for (edu.umd.cs.findbugs.ba.XMethod m2 : others){
            try {
                if ((confusingMethodNamesWrongCapitalization(m,m2) || confusingMethodNamesWrongPackage(m,m2)) && org.apache.bcel.Repository.instanceOf(m.getClassName(),m2.getClassName())) {
                    edu.umd.cs.findbugs.props.WarningPropertySet<edu.umd.cs.findbugs.detect.Naming.NamingProperty> propertySet = new edu.umd.cs.findbugs.props.WarningPropertySet<edu.umd.cs.findbugs.detect.Naming.NamingProperty>();
                    int priority = HIGH_PRIORITY;
                    boolean intentional = false;
                    edu.umd.cs.findbugs.ba.XMethod m3 = null;
                    try {
                        org.apache.bcel.classfile.JavaClass clazz = org.apache.bcel.Repository.lookupClass(m.getClassName());
                        if ((m3 = definedIn(clazz,m2)) != null) {
                            priority = NORMAL_PRIORITY;
                            intentional = true;
                        }
                    }
                    catch (java.lang.ClassNotFoundException e){
                        priority++;
                        edu.umd.cs.findbugs.ba.AnalysisContext.reportMissingClass(e);
                    }
                    edu.umd.cs.findbugs.ba.XFactory xFactory = edu.umd.cs.findbugs.ba.AnalysisContext.currentXFactory();
                    if (m3 == null && xFactory.isCalled(m)) propertySet.addProperty(edu.umd.cs.findbugs.detect.Naming.NamingProperty.METHOD_IS_CALLED);
                    else if (m.isDeprecated() || m2.isDeprecated()) propertySet.addProperty(edu.umd.cs.findbugs.detect.Naming.NamingProperty.METHOD_IS_DEPRECATED);
                    if ( !m.getName().equals(m2.getName()) && m.getName().equalsIgnoreCase(m2.getName())) {
                        java.lang.String pattern = intentional ? "NM_VERY_CONFUSING_INTENTIONAL" : "NM_VERY_CONFUSING";
                        java.util.Set<edu.umd.cs.findbugs.ba.XMethod> overrides = edu.umd.cs.findbugs.ba.Hierarchy2.findSuperMethods(m);
                        if ( !overrides.isEmpty()) {
                            if (intentional || this.allAbstract(overrides)) break;
                            priority++;
                        }
                        edu.umd.cs.findbugs.BugInstance bug = new edu.umd.cs.findbugs.BugInstance(this, pattern, priority).addClass(m.getClassName()).addMethod(m).addClass(m2.getClassName()).describe(edu.umd.cs.findbugs.ClassAnnotation.SUPERCLASS_ROLE).addMethod(m2).describe(edu.umd.cs.findbugs.MethodAnnotation.METHOD_DID_YOU_MEAN_TO_OVERRIDE);
                        if (m3 != null) bug.addMethod(m3).describe(edu.umd.cs.findbugs.MethodAnnotation.METHOD_OVERRIDDEN);
                        propertySet.decorateBugInstance(bug);
                        bugReporter.reportBug(bug);
                    }
                    else if ( !m.getSignature().equals(m2.getSignature()) && removePackageNamesFromSignature(m.getSignature()).equals(removePackageNamesFromSignature(m2.getSignature()))) {
                        java.lang.String pattern = intentional ? "NM_WRONG_PACKAGE_INTENTIONAL" : "NM_WRONG_PACKAGE";
                        java.util.Set<edu.umd.cs.findbugs.ba.XMethod> overrides = edu.umd.cs.findbugs.ba.Hierarchy2.findSuperMethods(m);
                        if ( !overrides.isEmpty()) {
                            if (intentional || this.allAbstract(overrides)) break;
                            priority++;
                        }
                        java.util.Iterator<java.lang.String> s = new edu.umd.cs.findbugs.ba.SignatureParser(m.getSignature()).parameterSignatureIterator();
                        java.util.Iterator<java.lang.String> s2 = new edu.umd.cs.findbugs.ba.SignatureParser(m2.getSignature()).parameterSignatureIterator();
                        while (s.hasNext()) {
                            java.lang.String p = s.next();
                            java.lang.String p2 = s2.next();
                            if ( !p.equals(p2)) {
                                edu.umd.cs.findbugs.BugInstance bug = new edu.umd.cs.findbugs.BugInstance(this, pattern, priority).addClass(m.getClassName()).addMethod(m).addClass(m2.getClassName()).describe(edu.umd.cs.findbugs.ClassAnnotation.SUPERCLASS_ROLE).addMethod(m2).describe(edu.umd.cs.findbugs.MethodAnnotation.METHOD_DID_YOU_MEAN_TO_OVERRIDE).addFoundAndExpectedType(p,p2);
                                if (m3 != null) bug.addMethod(m3).describe(edu.umd.cs.findbugs.MethodAnnotation.METHOD_OVERRIDDEN);
                                propertySet.decorateBugInstance(bug);
                                bugReporter.reportBug(bug);
                            }
                        }
                    }
                    return true;
                }
            }
            catch (java.lang.ClassNotFoundException e){
                edu.umd.cs.findbugs.ba.AnalysisContext.reportMissingClass(e);
            }
        }
;
        return false;
    }
/**
     * @param overrides
     * @return
     */
    private boolean allAbstract(java.util.Set<edu.umd.cs.findbugs.ba.XMethod> overrides) {
        boolean allAbstract = true;
        for (edu.umd.cs.findbugs.ba.XMethod m4 : overrides){
            if ( !m4.isAbstract()) allAbstract = false;
        }
;
        return allAbstract;
    }
    private boolean checkNonSuper(edu.umd.cs.findbugs.ba.XMethod m, java.util.Set<edu.umd.cs.findbugs.ba.XMethod> others) {
        if (m.isStatic()) return false;
        if (m.getName().startsWith("<init>") || m.getName().startsWith("<clinit>")) return false;
        for (edu.umd.cs.findbugs.ba.XMethod m2 : others){
            if (confusingMethodNamesWrongCapitalization(m,m2)) {
                edu.umd.cs.findbugs.ba.XMethod mm1 = m;
                edu.umd.cs.findbugs.ba.XMethod mm2 = m2;
                if (m.compareTo(m2) < 0) {
                    mm1 = m;
                    mm2 = m2;
                }
                else {
                    mm1 = m2;
                    mm2 = m;
                }
                bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_CONFUSING", LOW_PRIORITY).addClass(mm1.getClassName()).addMethod(mm1).addClass(mm2.getClassName()).addMethod(mm2));
                return true;
            }
        }
;
        return false;
    }
    public void report() {
        for (java.util.Map.Entry<java.lang.String, java.util.TreeSet<edu.umd.cs.findbugs.ba.XMethod>> e : canonicalToXMethod.entrySet()){
            java.util.TreeSet<edu.umd.cs.findbugs.ba.XMethod> conflictingMethods = e.getValue();
            java.util.HashSet<java.lang.String> trueNames = new java.util.HashSet<java.lang.String>();
            for (edu.umd.cs.findbugs.ba.XMethod m : conflictingMethods)trueNames.add(m.getName() + m.getSignature());
;
            if (trueNames.size() <= 1) continue;
            for (java.util.Iterator<edu.umd.cs.findbugs.ba.XMethod> j = conflictingMethods.iterator(); j.hasNext(); ) {
                if (this.checkSuper(j.next(),conflictingMethods)) j.remove();
            }
            for (edu.umd.cs.findbugs.ba.XMethod conflictingMethod : conflictingMethods){
                if (this.checkNonSuper(conflictingMethod,conflictingMethods)) break;
            }
;
        }
;
    }
    public java.lang.String stripPackageName(java.lang.String className) {
        if (className.indexOf('\u002e') >= 0) return className.substring(className.lastIndexOf('\u002e') + 1);
        else if (className.indexOf('\u002f') >= 0) return className.substring(className.lastIndexOf('\u002f') + 1);
        else return className;
    }
    public boolean sameSimpleName(java.lang.String class1, java.lang.String class2) {
        return class1 != null && class2 != null && this.stripPackageName(class1).equals(this.stripPackageName(class2));
    }
    public void visitJavaClass(org.apache.bcel.classfile.JavaClass obj) {
        if (obj.isSynthetic()) return;
        java.lang.String name = obj.getClassName();
        if ( !visited.add(name)) return;
        java.lang.String superClassName = obj.getSuperclassName();
        if ( !name.equals("java.lang.Object")) {
            if (this.sameSimpleName(superClassName,name)) {
                bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_SAME_SIMPLE_NAME_AS_SUPERCLASS", HIGH_PRIORITY).addClass(name).addClass(superClassName));
            }
            for (java.lang.String interfaceName : obj.getInterfaceNames())if (this.sameSimpleName(interfaceName,name)) {
                bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_SAME_SIMPLE_NAME_AS_INTERFACE", NORMAL_PRIORITY).addClass(name).addClass(interfaceName));
            }
;
        }
        if (obj.isInterface()) return;
        if (superClassName.equals("java.lang.Object") &&  !visited.contains(superClassName)) try {
            this.visitJavaClass(obj.getSuperClass());
        }
        catch (java.lang.ClassNotFoundException e){
        }
        super.visitJavaClass(obj);
    }
/**
     * Determine whether the class descriptor ultimately inherits from
     * java.lang.Exception
     * 
     * @param d
     *            class descriptor we want to check
     * @return true iff the descriptor ultimately inherits from Exception
     */
    private static boolean mightInheritFromException(edu.umd.cs.findbugs.classfile.ClassDescriptor d) {
        while (d != null) {
            try {
                if ("java.lang.Exception".equals(d.getDottedClassName())) {
                    return true;
                }
                edu.umd.cs.findbugs.ba.XClass classNameAndInfo = edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getClassAnalysis(edu.umd.cs.findbugs.ba.XClass.class,d);
                d = classNameAndInfo.getSuperclassDescriptor();
            }
            catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
// don't know
                return true;
            }
        }
        return false;
    }
    boolean hasBadMethodNames;
    boolean hasBadFieldNames;
/**
     * Eclipse uses reflection to initialize NLS message bundles. Classes which
     * using this mechanism are usualy extending org.eclipse.osgi.util.NLS class
     * and contains lots of public static String fields which are used as
     * message constants. Unfortunately these fields often has bad names which
     * does not follow Java code convention, so FB reports tons of warnings for
     * such Eclipse message fields.
     * 
     * @see edu.umd.cs.findbugs.detect.MutableStaticFields
     */
    private boolean isEclipseNLS;
    public void visit(org.apache.bcel.classfile.JavaClass obj) {
        java.lang.String name = obj.getClassName();
        java.lang.String[] parts = name.split("[$+.]");
        baseClassName = parts[parts.length - 1];
        for (java.lang.String p : name.split("[.]"))if (p.length() == 1) return;
;
        if (name.indexOf("Proto$") >= 0) return;
        classIsPublicOrProtected = obj.isPublic() || obj.isProtected();
        if (java.lang.Character.isLetter(baseClassName.charAt(0)) &&  !java.lang.Character.isUpperCase(baseClassName.charAt(0)) && baseClassName.indexOf("_") ==  -1) {
            int priority = classIsPublicOrProtected ? NORMAL_PRIORITY : LOW_PRIORITY;
            bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_CLASS_NAMING_CONVENTION", priority).addClass(this));
        }
        if (name.endsWith("Exception")) {
// Does it ultimately inherit from Throwable?
            if ( !mightInheritFromException(edu.umd.cs.findbugs.classfile.DescriptorFactory.createClassDescriptor(obj))) {
                bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_CLASS_NOT_EXCEPTION", NORMAL_PRIORITY).addClass(this));
            }
        }
        int badFieldNames = 0;
        for (org.apache.bcel.classfile.Field f : obj.getFields())if (f.getName().length() >= 2 && this.badFieldName(f)) badFieldNames++;
;
        hasBadFieldNames = badFieldNames > 3 && badFieldNames > obj.getFields().length / 3;
        int badMethodNames = 0;
        for (org.apache.bcel.classfile.Method m : obj.getMethods())if (this.badMethodName(m.getName())) badMethodNames++;
;
        hasBadMethodNames = badMethodNames > 3 && badMethodNames > obj.getMethods().length / 3;
        isEclipseNLS = "org.eclipse.osgi.util.NLS".equals(obj.getSuperclassName());
        super.visit(obj);
    }
    public void visit(org.apache.bcel.classfile.Field obj) {
        if (this.getFieldName().length() == 1) return;
        if (isEclipseNLS) {
            int flags = obj.getAccessFlags();
            if ((flags & ACC_STATIC) != 0 && ((flags & ACC_PUBLIC) != 0) && this.getFieldSig().equals("Ljava/lang/String;")) {
// ignore "public statis String InstallIUCommandTooltip;"
// messages from Eclipse NLS bundles
                return;
            }
        }
        if (this.badFieldName(obj)) {
            bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_FIELD_NAMING_CONVENTION", classIsPublicOrProtected && (obj.isPublic() || obj.isProtected()) &&  !hasBadFieldNames ? NORMAL_PRIORITY : LOW_PRIORITY).addClass(this).addVisitedField(this));
        }
    }
/**
     * @param obj
     * @return
     */
    private boolean badFieldName(org.apache.bcel.classfile.Field obj) {
        java.lang.String fieldName = obj.getName();
        return  !obj.isFinal() && java.lang.Character.isLetter(fieldName.charAt(0)) &&  !java.lang.Character.isLowerCase(fieldName.charAt(0)) && fieldName.indexOf("_") ==  -1 && java.lang.Character.isLetter(fieldName.charAt(1)) && java.lang.Character.isLowerCase(fieldName.charAt(1));
    }
    final private static java.util.regex.Pattern sigType = java.util.regex.Pattern.compile("L([^;]*/)?([^/]+;)");
    private static java.lang.String getSignatureOfOuterClass(org.apache.bcel.classfile.JavaClass obj) {
        for (org.apache.bcel.classfile.Field f : obj.getFields())if (f.getName().startsWith("this$")) return f.getSignature();
;
        return null;
    }
    private boolean markedAsNotUsable(org.apache.bcel.classfile.Method obj) {
        for (org.apache.bcel.classfile.Attribute a : obj.getAttributes())if (a instanceof org.apache.bcel.classfile.Deprecated) return true;
;
        org.apache.bcel.classfile.Code code = obj.getCode();
        if (code == null) return false;
        byte[] codeBytes = code.getCode();
        if (codeBytes.length > 1 && codeBytes.length < 10) {
            int lastOpcode = codeBytes[codeBytes.length - 1] & 0xff;
            if (lastOpcode != ATHROW) return false;
            for (int b : codeBytes)if ((b & 0xff) == RETURN) return false;
;
            return true;
        }
        return false;
    }
    private static org.apache.bcel.classfile.Method findVoidConstructor(org.apache.bcel.classfile.JavaClass clazz) {
        for (org.apache.bcel.classfile.Method m : clazz.getMethods())if (isVoidConstructor(clazz,m)) return m;
;
        return null;
    }
    public void visit(org.apache.bcel.classfile.Method obj) {
        java.lang.String mName = this.getMethodName();
        if (mName.length() == 1) return;
        if (mName.equals("isRequestedSessionIdFromURL") || mName.equals("isRequestedSessionIdFromUrl")) return;
        java.lang.String sig = this.getMethodSig();
        if (mName.equals(baseClassName) && sig.equals("()V")) {
            org.apache.bcel.classfile.Code code = obj.getCode();
            org.apache.bcel.classfile.Method realVoidConstructor = findVoidConstructor(this.getThisClass());
            if (code != null &&  !this.markedAsNotUsable(obj)) {
                int priority = NORMAL_PRIORITY;
                if (this.codeDoesSomething(code)) priority--;
                else if ( !obj.isPublic() && this.getThisClass().isPublic()) priority--;
                boolean instanceMembers = false;
                for (org.apache.bcel.classfile.Method m : this.getThisClass().getMethods())if ( !m.isStatic() && m != obj &&  !isVoidConstructor(this.getThisClass(),m)) instanceMembers = true;
;
                for (org.apache.bcel.classfile.Field f : this.getThisClass().getFields())if ( !f.isStatic()) instanceMembers = true;
;
                if ( !this.codeDoesSomething(code) &&  !instanceMembers && this.getSuperclassName().equals("java/lang/Object")) priority += 2;
                if (hasBadMethodNames) priority++;
                if ( !this.getXClass().getAnnotations().isEmpty()) priority++;
                if (realVoidConstructor != null) priority = LOW_PRIORITY;
                bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_METHOD_CONSTRUCTOR_CONFUSION", priority).addClassAndMethod(this).lowerPriorityIfDeprecated());
                return;
            }
        }
        else if (this.badMethodName(mName)) bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_METHOD_NAMING_CONVENTION", classIsPublicOrProtected && (obj.isPublic() || obj.isProtected()) &&  !hasBadMethodNames ? NORMAL_PRIORITY : LOW_PRIORITY).addClassAndMethod(this));
        if (obj.isAbstract()) return;
        if (obj.isPrivate()) return;
        if (mName.equals("equal") && sig.equals("(Ljava/lang/Object;)Z")) {
            bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_BAD_EQUAL", HIGH_PRIORITY).addClassAndMethod(this).lowerPriorityIfDeprecated());
            return;
        }
        if (mName.equals("hashcode") && sig.equals("()I")) {
            bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_LCASE_HASHCODE", HIGH_PRIORITY).addClassAndMethod(this).lowerPriorityIfDeprecated());
            return;
        }
        if (mName.equals("tostring") && sig.equals("()Ljava/lang/String;")) {
            bugReporter.reportBug(new edu.umd.cs.findbugs.BugInstance(this, "NM_LCASE_TOSTRING", HIGH_PRIORITY).addClassAndMethod(this).lowerPriorityIfDeprecated());
            return;
        }
        if (obj.isPrivate() || obj.isStatic() || mName.equals("<init>")) return;
        java.lang.String sig2 = removePackageNamesFromSignature(sig);
        java.lang.String allSmall = mName.toLowerCase() + sig2;
        edu.umd.cs.findbugs.ba.XMethod xm = this.getXMethod();
        {
            java.util.TreeSet<edu.umd.cs.findbugs.ba.XMethod> s = canonicalToXMethod.get(allSmall);
            if (s == null) {
                s = new java.util.TreeSet<edu.umd.cs.findbugs.ba.XMethod>();
                canonicalToXMethod.put(allSmall,s);
            }
            s.add(xm);
        }
    }
    private static boolean isVoidConstructor(org.apache.bcel.classfile.JavaClass clazz, org.apache.bcel.classfile.Method m) {
        java.lang.String outerClassSignature = getSignatureOfOuterClass(clazz);
        if (outerClassSignature == null) outerClassSignature = "";
        return m.getName().equals("<init>") && m.getSignature().equals("(" + outerClassSignature + ")V");
    }
/**
     * @param mName
     * @return
     */
    private boolean badMethodName(java.lang.String mName) {
        return mName.length() >= 2 && java.lang.Character.isLetter(mName.charAt(0)) &&  !java.lang.Character.isLowerCase(mName.charAt(0)) && java.lang.Character.isLetter(mName.charAt(1)) && java.lang.Character.isLowerCase(mName.charAt(1)) && mName.indexOf("_") ==  -1;
    }
    private boolean codeDoesSomething(org.apache.bcel.classfile.Code code) {
        byte[] codeBytes = code.getCode();
        return codeBytes.length > 1;
    }
    private static java.lang.String removePackageNamesFromSignature(java.lang.String sig) {
        int end = sig.indexOf(")");
        java.util.regex.Matcher m = sigType.matcher(sig.substring(0,end));
        return m.replaceAll("L$2") + sig.substring(end);
    }
}
