/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Ant task to generate HTML or plain text from a saved XML analysis results
 * file.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import org.apache.tools.ant.BuildException;
public class ConvertXmlToTextTask extends edu.umd.cs.findbugs.anttask.AbstractFindBugsTask {
    private boolean longBugCodes;
    private boolean applySuppression;
    private java.lang.String input;
    private java.lang.String output;
    private java.lang.String format = "html";
    public ConvertXmlToTextTask() {
        super("edu.umd.cs.findbugs.PrintingBugReporter");
        this.setFailOnError(true);
    }
/**
     * @param longBugCodes
     *            The longBugCodes to set.
     */
    public void setLongBugCodes(boolean longBugCodes) {
        this.longBugCodes = longBugCodes;
    }
/**
     * @param applySuppression
     *            The applySuppression to set.
     */
    public void setApplySuppression(boolean applySuppression) {
        this.applySuppression = applySuppression;
    }
/**
     * @param input
     *            The input to set.
     */
    public void setInput(java.lang.String input) {
        this.input = input;
    }
/**
     * @param output
     *            The output to set.
     */
    public void setOutput(java.lang.String output) {
        this.output = output;
    }
/**
     * @param input
     *            The input to set.
     */
    public void setInputFile(java.lang.String input) {
        this.input = input;
    }
/**
     * @param output
     *            The output to set.
     */
    public void setOutputFile(java.lang.String output) {
        this.output = output;
    }
/**
     * @param format
     *            The format to set.
     */
    public void setFormat(java.lang.String format) {
        this.format = format;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#checkParameters()
     */
    protected void checkParameters() {
        if (input == null) {
            throw new org.apache.tools.ant.BuildException("input attribute is required", this.getLocation());
        }
        if (output == null) {
            throw new org.apache.tools.ant.BuildException("output attribute is required", this.getLocation());
        }
        if ( !format.equals("text") &&  !(format.equals("html") || format.startsWith("html:"))) {
            throw new org.apache.tools.ant.BuildException("invalid value " + format + " for format attribute", this.getLocation());
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#configureFindbugsEngine
     * ()
     */
    protected void configureFindbugsEngine() {
        if (format.startsWith("html")) {
            this.addArg("-" + format);
        }
        if (longBugCodes) {
            this.addArg("-longBugCodes");
        }
        if (applySuppression) {
            this.addArg("-applySuppression");
        }
        this.addArg(input);
        this.addArg(output);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#beforeExecuteJavaProcess
     * ()
     */
    protected void beforeExecuteJavaProcess() {
        this.log("Converting " + input + " to " + output + " using format " + format);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.anttask.AbstractFindBugsTask#afterExecuteJavaProcess
     * (int)
     */
    protected void afterExecuteJavaProcess(int rc) {
        if (rc == 0) {
            this.log("Success");
        }
    }
}
