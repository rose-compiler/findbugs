/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Compute a hash of method names and signatures. This allows us to find out
 * when a class has been renamed, but not changed in any other obvious way.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.util.Util;
import edu.umd.cs.findbugs.xml.XMLOutput;
import edu.umd.cs.findbugs.xml.XMLWriteable;
public class ClassHash extends java.lang.Object implements edu.umd.cs.findbugs.xml.XMLWriteable, java.lang.Comparable<edu.umd.cs.findbugs.ba.ClassHash> {
/**
     * XML element name for a ClassHash.
     */
    final public static java.lang.String CLASS_HASH_ELEMENT_NAME = "ClassHash";
/**
     * XML element name for a MethodHash.
     */
    final public static java.lang.String METHOD_HASH_ELEMENT_NAME = "MethodHash";
// Fields
    private java.lang.String className;
    private byte[] classHash;
    private java.util.Map<edu.umd.cs.findbugs.ba.XMethod, edu.umd.cs.findbugs.ba.MethodHash> methodHashMap;
/**
     * Constructor.
     */
    public ClassHash() {
        super();
        this.methodHashMap = new java.util.HashMap<edu.umd.cs.findbugs.ba.XMethod, edu.umd.cs.findbugs.ba.MethodHash>();
    }
/**
     * Constructor.
     * 
     * @param classHash
     *            pre-computed class hash
     */
    public ClassHash(java.lang.String className, byte[] classHash) {
        this();
        this.className = className;
        this.classHash = new byte[classHash.length];
        java.lang.System.arraycopy(classHash,0,this.classHash,0,classHash.length);
    }
/**
     * Set method hash for given method.
     * 
     * @param method
     *            the method
     * @param methodHash
     *            the method hash
     */
    public void setMethodHash(edu.umd.cs.findbugs.ba.XMethod method, byte[] methodHash) {
        methodHashMap.put(method,new edu.umd.cs.findbugs.ba.MethodHash(method.getName(), method.getSignature(), method.isStatic(), methodHash));
    }
/**
     * @return Returns the className.
     */
    public java.lang.String getClassName() {
        return className;
    }
/**
     * Get class hash.
     * 
     * @return the class hash
     */
    public byte[] getClassHash() {
        return classHash;
    }
/**
     * Set class hash.
     * 
     * @param classHash
     *            the class hash value to set
     */
    public void setClassHash(byte[] classHash) {
        this.classHash = new byte[classHash.length];
        java.lang.System.arraycopy(classHash,0,this.classHash,0,classHash.length);
    }
/**
     * Get method hash for given method.
     * 
     * @param method
     *            the method
     * @return the MethodHash
     */
    public edu.umd.cs.findbugs.ba.MethodHash getMethodHash(edu.umd.cs.findbugs.ba.XMethod method) {
        return methodHashMap.get(method);
    }
/**
     * Compute hash for given class and all of its methods.
     * 
     * @param javaClass
     *            the class
     * @return this object
     */
    public edu.umd.cs.findbugs.ba.ClassHash computeHash(org.apache.bcel.classfile.JavaClass javaClass) {
        this.className = javaClass.getClassName();
        org.apache.bcel.classfile.Method[] methodList = new org.apache.bcel.classfile.Method[javaClass.getMethods().length];
        java.lang.System.arraycopy(javaClass.getMethods(),0,methodList,0,javaClass.getMethods().length);
        java.util.Arrays.sort(methodList,new java.util.Comparator<org.apache.bcel.classfile.Method>() {
            public int compare(org.apache.bcel.classfile.Method o1, org.apache.bcel.classfile.Method o2) {
                int cmp = o1.getName().compareTo(o2.getName());
                if (cmp != 0) return cmp;
                return o1.getSignature().compareTo(o2.getSignature());
            }
        });
// Sort methods
// sort by name, then signature
        org.apache.bcel.classfile.Field[] fieldList = new org.apache.bcel.classfile.Field[javaClass.getFields().length];
        java.lang.System.arraycopy(javaClass.getFields(),0,fieldList,0,javaClass.getFields().length);
        java.util.Arrays.sort(fieldList,new java.util.Comparator<org.apache.bcel.classfile.Field>() {
            public int compare(org.apache.bcel.classfile.Field o1, org.apache.bcel.classfile.Field o2) {
                int cmp = o1.getName().compareTo(o2.getName());
                if (cmp != 0) return cmp;
                return o1.getSignature().compareTo(o2.getSignature());
            }
        });
// Sort fields
/*
             * (non-Javadoc)
             * 
             * @see java.util.Comparator#compare(T, T)
             */
        java.security.MessageDigest digest = edu.umd.cs.findbugs.util.Util.getMD5Digest();
// Compute digest of method names and signatures, in order.
// Also, compute method hashes.
        java.nio.charset.CharsetEncoder encoder = java.nio.charset.Charset.forName("UTF-8").newEncoder();
        for (org.apache.bcel.classfile.Method method : methodList){
            work(digest,method.getName(),encoder);
            work(digest,method.getSignature(),encoder);
            edu.umd.cs.findbugs.ba.MethodHash methodHash = new edu.umd.cs.findbugs.ba.MethodHash().computeHash(method);
            methodHashMap.put(edu.umd.cs.findbugs.ba.XFactory.createXMethod(javaClass,method),methodHash);
        }
;
// Compute digest of field names and signatures.
        for (org.apache.bcel.classfile.Field field : fieldList){
            work(digest,field.getName(),encoder);
            work(digest,field.getSignature(),encoder);
        }
;
        classHash = digest.digest();
        return this;
    }
    private static void work(java.security.MessageDigest digest, java.lang.String s, java.nio.charset.CharsetEncoder encoder) {
        try {
            java.nio.CharBuffer cbuf = java.nio.CharBuffer.allocate(s.length());
            cbuf.put(s);
            cbuf.flip();
            java.nio.ByteBuffer buf = encoder.encode(cbuf);
// System.out.println("pos="+buf.position() +",limit=" +
// buf.limit());
            int nbytes = buf.limit();
            byte[] encodedBytes = new byte[nbytes];
            buf.get(encodedBytes);
            digest.update(encodedBytes);
        }
        catch (java.nio.charset.CharacterCodingException e){
        }
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        xmlOutput.startTag(CLASS_HASH_ELEMENT_NAME);
        xmlOutput.addAttribute("class",className);
        xmlOutput.addAttribute("value",hashToString(classHash));
        xmlOutput.stopTag(false);
        for (java.util.Map.Entry<edu.umd.cs.findbugs.ba.XMethod, edu.umd.cs.findbugs.ba.MethodHash> entry : methodHashMap.entrySet()){
            xmlOutput.startTag(METHOD_HASH_ELEMENT_NAME);
            xmlOutput.addAttribute("name",entry.getKey().getName());
            xmlOutput.addAttribute("signature",entry.getKey().getSignature());
            xmlOutput.addAttribute("isStatic",java.lang.String.valueOf(entry.getKey().isStatic()));
            xmlOutput.addAttribute("value",hashToString(entry.getValue().getMethodHash()));
            xmlOutput.stopTag(true);
        }
;
        xmlOutput.closeTag(CLASS_HASH_ELEMENT_NAME);
    }
    final private static char[] HEX_CHARS = {'\u0030', '\u0031', '\u0032', '\u0033', '\u0034', '\u0035', '\u0036', '\u0037', '\u0038', '\u0039', '\u0061', '\u0062', '\u0063', '\u0064', '\u0065', '\u0066'};
/**
     * Convert a hash to a string of hex digits.
     * 
     * @param hash
     *            the hash
     * @return a String representation of the hash
     */
    public static java.lang.String hashToString(byte[] hash) {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        for (byte b : hash){
            buf.append(HEX_CHARS[(b >> 4) & 0xF]);
            buf.append(HEX_CHARS[b & 0xF]);
        }
;
        return buf.toString();
    }
    private static int hexDigitValue(char c) {
        if (c >= '\u0030' && c <= '\u0039') return c - '\u0030';
        else if (c >= '\u0061' && c <= '\u0066') return 10 + (c - '\u0061');
        else if (c >= '\u0041' && c <= '\u0046') return 10 + (c - '\u0041');
        else throw new java.lang.IllegalArgumentException("Illegal hex character: " + c);
    }
/**
     * Convert a string of hex digits to a hash.
     * 
     * @param s
     *            string of hex digits
     * @return the hash value represented by the string
     */
    public static byte[] stringToHash(java.lang.String s) {
        if (s.length() % 2 != 0) throw new java.lang.IllegalArgumentException("Invalid hash string: " + s);
        byte[] hash = new byte[s.length() / 2];
        for (int i = 0; i < s.length(); i += 2) {
            byte b = (byte) (((hexDigitValue(s.charAt(i)) << 4) + hexDigitValue(s.charAt(i + 1)))) ;
            hash[i / 2] = b;
        }
        return hash;
    }
/**
     * Return whether or not this class hash has the same hash value as the one
     * given.
     * 
     * @param other
     *            another ClassHash
     * @return true if the hash values are the same, false if not
     */
    public boolean isSameHash(edu.umd.cs.findbugs.ba.ClassHash other) {
        return java.util.Arrays.equals(classHash,other.classHash);
    }
    public int hashCode() {
        if (classHash == null) return 0;
        int result = 1;
        for (byte element : classHash)result = 31 * result + element;
;
        return result;
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.ba.ClassHash)) return false;
        return this.isSameHash((edu.umd.cs.findbugs.ba.ClassHash) (o) );
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(T)
     */
    public int compareTo(edu.umd.cs.findbugs.ba.ClassHash other) {
        int cmp = edu.umd.cs.findbugs.ba.MethodHash.compareHashes(this.classHash,other.classHash);
// System.out.println(this + " <=> " + other + ": compareTo=" + cmp);
        return cmp;
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public java.lang.String toString() {
        return this.getClassName() + ":" + hashToString(this.classHash);
    }
}
