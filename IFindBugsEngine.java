/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Interface for a FindBugs engine class. An instance of this interface takes a
 * project, user configuration options, orchestrates the analysis of the classes
 * in the project, and reports the results to the configured BugReporter.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.util.Set;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.classfile.IClassObserver;
import edu.umd.cs.findbugs.config.AnalysisFeatureSetting;
import edu.umd.cs.findbugs.config.UserPreferences;
import edu.umd.cs.findbugs.filter.FilterException;
abstract public interface IFindBugsEngine {
/**
     * Get the BugReporter.
     * 
     * @return the BugReporter
     */
    abstract public edu.umd.cs.findbugs.BugReporter getBugReporter();
/**
     * Set the BugReporter.
     * 
     * @param bugReporter
     *            The BugReporter to set
     */
    abstract public void setBugReporter(edu.umd.cs.findbugs.BugReporter bugReporter);
/**
     * Set the Project.
     * 
     * @param project
     *            The Project to set
     */
    abstract public void setProject(edu.umd.cs.findbugs.Project project);
/**
     * Get the Project.
     * 
     * @return the Project
     */
    abstract public edu.umd.cs.findbugs.Project getProject();
/**
     * Set the progress callback that will be used to keep track of the progress
     * of the analysis.
     * 
     * @param progressCallback
     *            the progress callback
     */
    abstract public void setProgressCallback(edu.umd.cs.findbugs.FindBugsProgress progressCallback);
/**
     * Set filter of bug instances to include or exclude.
     * 
     * @param filterFileName
     *            the name of the filter file
     * @param include
     *            true if the filter specifies bug instances to include, false
     *            if it specifies bug instances to exclude
     */
    abstract public void addFilter(java.lang.String filterFileName, boolean include) throws edu.umd.cs.findbugs.filter.FilterException, java.io.IOException;
/**
     * Provide baseline of bugs not to report
     * 
     * @param baselineBugs
     *            the name of the xml bug baseline file
     * @throws DocumentException
     */
    abstract public void excludeBaselineBugs(java.lang.String baselineBugs) throws org.dom4j.DocumentException, java.io.IOException;
/**
     * Set the UserPreferences representing which Detectors should be used. If
     * UserPreferences are not set explicitly, the default set of Detectors will
     * be used.
     * 
     * @param userPreferences
     *            the UserPreferences
     */
    abstract public void setUserPreferences(edu.umd.cs.findbugs.config.UserPreferences userPreferences);
/**
     * Add an IClassObserver.
     * 
     * @param classObserver
     *            the IClassObserver
     */
    abstract public void addClassObserver(edu.umd.cs.findbugs.classfile.IClassObserver classObserver);
/**
     * Set the ClassScreener. This object chooses which individual classes to
     * analyze. By default, all classes are analyzed.
     * 
     * @param classScreener
     *            the ClassScreener to use
     */
    abstract public void setClassScreener(edu.umd.cs.findbugs.IClassScreener classScreener);
/**
     * Set relaxed reporting mode.
     * 
     * @param relaxedReportingMode
     *            true if relaxed reporting mode should be enabled, false if not
     */
    abstract public void setRelaxedReportingMode(boolean relaxedReportingMode);
/**
     * Set whether or not training output should be emitted.
     * 
     * @param trainingOutputDir
     *            directory to save training output in
     */
    abstract public void enableTrainingOutput(java.lang.String trainingOutputDir);
/**
     * Set whether or not training input should be used to make the analysis
     * more precise.
     * 
     * @param trainingInputDir
     *            directory to load training input from
     */
    abstract public void enableTrainingInput(java.lang.String trainingInputDir);
/**
     * Set analysis feature settings.
     * 
     * @param settingList
     *            list of analysis feature settings
     */
    abstract public void setAnalysisFeatureSettings(edu.umd.cs.findbugs.config.AnalysisFeatureSetting[] settingList);
/**
     * @return Returns the releaseName.
     */
    abstract public java.lang.String getReleaseName();
/**
     * @param releaseName
     *            The releaseName to set.
     */
    abstract public void setReleaseName(java.lang.String releaseName);
/**
     * @return Returns the projectName.
     */
    abstract public java.lang.String getProjectName();
/**
     * @param projectName
     *            The project name to set.
     */
    abstract public void setProjectName(java.lang.String projectName);
/**
     * Set the filename of the source info file containing line numbers for
     * fields and classes.
     * 
     * @param sourceInfoFile
     *            the source info filename
     */
    abstract public void setSourceInfoFile(java.lang.String sourceInfoFile);
/**
     * Execute FindBugs on the Project. All bugs found are reported to the
     * BugReporter object which was set when this object was constructed.
     * 
     * @throws java.io.IOException
     *             if an I/O exception occurs analyzing one of the files
     * @throws InterruptedException
     *             if the thread is interrupted while conducting the analysis
     */
    abstract public void execute() throws java.lang.InterruptedException, java.io.IOException;
/**
     * Get the name of the most recent class to be analyzed. This is useful for
     * diagnosing an unexpected exception. Returns null if no class has been
     * analyzed.
     */
    abstract public java.lang.String getCurrentClass();
/**
     * Get the number of bug instances that were reported during analysis.
     */
    abstract public int getBugCount();
/**
     * Get the number of errors that occurred during analysis.
     */
    abstract public int getErrorCount();
/**
     * Get the number of time missing classes were reported during analysis.
     */
    abstract public int getMissingClassCount();
/**
     * Get the UserPreferences.
     * 
     * @return the UserPreferences
     */
    abstract public edu.umd.cs.findbugs.config.UserPreferences getUserPreferences();
/**
     * Return whether or not training output should be emitted after analysis
     * completes.
     * 
     * @return true if training output should be emitted, false if not
     */
    abstract public boolean emitTrainingOutput();
/**
     * Get the training output directory.
     * 
     * @return the training output directory
     */
    abstract public java.lang.String getTrainingOutputDir();
/**
     * Return whether or not we should make use of training data.
     * 
     * @return true if training data should be used, false if not
     */
    abstract public boolean useTrainingInput();
/**
     * Get the training input database directory.
     * 
     * @return the training input database directory
     */
    abstract public java.lang.String getTrainingInputDir();
/**
     * Set whether or not nested archives should be scanned.
     * 
     * @param scanNestedArchives
     *            true if nested archives should be scanned, false if not
     */
    abstract public void setScanNestedArchives(boolean scanNestedArchives);
/**
     * Set whether or not to generate an empty output file if there were no
     * class files specified.
     * 
     * @param noClassOk
     *            true if FindBugs should generate empty output file
     */
    abstract public void setNoClassOk(boolean noClassOk);
/**
     * Set the DetectorFactoryCollection from which plugins/detectors may be
     * accessed.
     * 
     * @param detectorFactoryCollection
     *            the DetectorFactoryCollection
     */
    abstract public void setDetectorFactoryCollection(edu.umd.cs.findbugs.DetectorFactoryCollection detectorFactoryCollection);
/**
     * @param xmlWithAbridgedMessages
     */
    abstract public void setAbridgedMessages(boolean xmlWithAbridgedMessages);
    abstract public void setMergeSimilarWarnings(boolean mergeSimilarWarnings);
    abstract public void setApplySuppression(boolean applySuppression);
    abstract public void finishSettings();
    abstract void setRankThreshold(int rankThreshold);
    abstract void setBugReporterDecorators(java.util.Set<java.lang.String> explicitlyEnabled, java.util.Set<java.lang.String> explicitlyDisabled);
}
