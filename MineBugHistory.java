/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Mine historical information from a BugCollection. The BugCollection should be
 * built using UpdateBugCollection to record the history of analyzing all
 * versions over time.
 * 
 * @author David Hovemeyer
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import edu.umd.cs.findbugs.AppVersion;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.config.CommandLine;
public class MineBugHistory extends java.lang.Object {
/**
     * 
     */
/**
         * @return Returns the sequence.
         */
/** This is how dump() was implemented up to and including version 0.9.5. */
/** emit <code>width</code> space characters to <code>out</code> */
/**
     * equivalent to out.print(obj) except it may be padded on the left or right
     * 
     * @param width
     *            padding will occur if the stringified oxj is shorter than this
     * @param alignRight
     *            true to pad on the left, false to pad on the right
     * @param out
     *            the PrintStream printed to
     * @param obj
     *            the value to print (may be an auto-boxed primitive)
     */
// doesn't truncate if (s.length() > width)
/**
     * This implementation of dump() tries to better align columns (when viewed
     * with a fixed-width font) by padding with spaces instead of using tabs.
     * Also, timestamps are formatted more tersely (-formatDates option). The
     * bad news is that it requires a minimum of 112 columns.
     * 
     * @see #dumpOriginal(PrintStream)
     */
// out.println("seq\tversion\ttime\tclasses\tNCSS\tadded\tnewCode\tfixed\tremoved\tretained\tdead\tactive");
// note: if we were allowed to depend on JDK 1.5 we could use
// out.printf():
// Object line[] = { "seq", "version", "time", "classes", "NCSS",
// "added", "newCode", "fixed", "removed", "retained", "dead", "active"
// };
// out.printf("%3s %-19s %-16s %7s %7s %7s %7s %7s %7s %8s %6s %7s%n",
// line);
// out.print(i);
// '\t'
/** This is how dump() was implemented up to and including version 0.9.5. */
// newCode and retained are already comprised within active
// so we skip tehm
/**
     * Get key used to classify the presence and/or abscence of a BugInstance in
     * successive versions in the history.
     * 
     * @param activePrevious
     *            true if the bug was active in the previous version, false if
     *            not
     * @param activeCurrent
     *            true if the bug is active in the current version, false if not
     * @return the key: one of ADDED, RETAINED, REMOVED, and DEAD
     */
// !activePrevious
    class MineBugHistoryCommandLine extends edu.umd.cs.findbugs.config.CommandLine {
        public MineBugHistoryCommandLine() {
            super();
            this.addSwitch("-formatDates","render dates in textual form");
            this.addSwitch("-noTabs","delimit columns with groups of spaces for better alignment");
            this.addSwitch("-xml","output in XML format");
            this.addSwitch("-summary","just summarize changes over the last ten entries");
        }
        public void handleOption(java.lang.String option, java.lang.String optionalExtraPart) {
            if (option.equals("-formatDates")) edu.umd.cs.findbugs.workflow.MineBugHistory.this.setFormatDates(true);
            else if (option.equals("-noTabs")) edu.umd.cs.findbugs.workflow.MineBugHistory.this.setNoTabs();
            else if (option.equals("-xml")) edu.umd.cs.findbugs.workflow.MineBugHistory.this.setXml();
            else if (option.equals("-summary")) edu.umd.cs.findbugs.workflow.MineBugHistory.this.setSummary();
            else throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
        public void handleOptionWithArgument(java.lang.String option, java.lang.String argument) {
            throw new java.lang.IllegalArgumentException("unknown option: " + option);
        }
    }
    static class Version extends java.lang.Object {
        long sequence;
        int[] tuple = new int[TUPLE_SIZE];
        public Version(long sequence) {
            super();
            this.sequence = sequence;
        }
        public long getSequence() {
            return sequence;
        }
        void increment(int key) {
            tuple[key]++;
            if (key == ADDED || key == RETAINED || key == NEWCODE) tuple[ACTIVE_NOW]++;
        }
        int get(int key) {
            return tuple[key];
        }
    }
    final private static int WIDTH = 12;
    final static int ADDED = 0;
    final static int NEWCODE = 1;
    final static int REMOVED = 2;
    final static int REMOVEDCODE = 3;
    final static int RETAINED = 4;
    final static int DEAD = 5;
    final static int ACTIVE_NOW = 6;
    final static int TUPLE_SIZE = 7;
    final java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy.MM.dd", java.util.Locale.ENGLISH);
    edu.umd.cs.findbugs.SortedBugCollection bugCollection;
    edu.umd.cs.findbugs.workflow.MineBugHistory.Version[] versionList;
    java.util.Map<java.lang.Long, edu.umd.cs.findbugs.AppVersion> sequenceToAppVersionMap = new java.util.HashMap<java.lang.Long, edu.umd.cs.findbugs.AppVersion>();
    boolean formatDates = false;
    boolean noTabs = false;
    boolean summary = false;
    boolean xml = false;
    public MineBugHistory() {
        super();
    }
    public MineBugHistory(edu.umd.cs.findbugs.SortedBugCollection bugCollection) {
        super();
        this.bugCollection = bugCollection;
    }
    public void setBugCollection(edu.umd.cs.findbugs.SortedBugCollection bugCollection) {
        this.bugCollection = bugCollection;
    }
    public void setFormatDates(boolean value) {
        this.formatDates = value;
    }
    public void setNoTabs() {
        this.xml = false;
        this.noTabs = true;
        this.summary = false;
    }
    public void setXml() {
        this.xml = true;
        this.noTabs = false;
        this.summary = false;
    }
    public void setSummary() {
        this.xml = false;
        this.summary = true;
        this.noTabs = false;
    }
    public edu.umd.cs.findbugs.workflow.MineBugHistory execute() {
        long sequenceNumber = bugCollection.getSequenceNumber();
        int maxSequence = (int) (sequenceNumber) ;
        versionList = new edu.umd.cs.findbugs.workflow.MineBugHistory.Version[maxSequence + 1];
        for (int i = 0; i <= maxSequence; ++i) {
            versionList[i] = new edu.umd.cs.findbugs.workflow.MineBugHistory.Version(i);
        }
        for (java.util.Iterator<edu.umd.cs.findbugs.AppVersion> i = bugCollection.appVersionIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.AppVersion appVersion = i.next();
            long versionSequenceNumber = appVersion.getSequenceNumber();
            sequenceToAppVersionMap.put(versionSequenceNumber,appVersion);
        }
        edu.umd.cs.findbugs.AppVersion currentAppVersion = bugCollection.getCurrentAppVersion();
        sequenceToAppVersionMap.put(sequenceNumber,currentAppVersion);
        for (java.util.Iterator<edu.umd.cs.findbugs.BugInstance> j = bugCollection.iterator(); j.hasNext(); ) {
            edu.umd.cs.findbugs.BugInstance bugInstance = j.next();
            for (int i = 0; i <= maxSequence; ++i) {
                if (bugInstance.getFirstVersion() > i) continue;
                boolean activePrevious = bugInstance.getFirstVersion() < i && ( !bugInstance.isDead() || bugInstance.getLastVersion() >= i - 1);
                boolean activeCurrent =  !bugInstance.isDead() || bugInstance.getLastVersion() >= i;
                int key = this.getKey(activePrevious,activeCurrent);
                if (key == REMOVED &&  !bugInstance.isRemovedByChangeOfPersistingClass()) key = REMOVEDCODE;
                else if (key == ADDED &&  !bugInstance.isIntroducedByChangeOfExistingClass()) key = NEWCODE;
                versionList[i].increment(key);
            }
        }
        return this;
    }
    public void dump(java.io.PrintStream out) {
        if (xml) this.dumpXml(out);
        else if (noTabs) this.dumpNoTabs(out);
        else if (summary) this.dumpSummary(out);
        else this.dumpOriginal(out);
    }
    public void dumpSummary(java.io.PrintStream out) {
        java.lang.StringBuilder b = new java.lang.StringBuilder();
        for (int i = java.lang.Math.max(0,versionList.length - 10); i < versionList.length; ++i) {
            edu.umd.cs.findbugs.workflow.MineBugHistory.Version version = versionList[i];
            int added = version.get(ADDED) + version.get(NEWCODE);
            int removed = version.get(REMOVED) + version.get(REMOVEDCODE);
            b.append(" ");
            if (added > 0) {
                b.append('\u002b');
                b.append(added);
            }
            if (removed > 0) {
                b.append('\u002d');
                b.append(removed);
            }
            if (added == 0 && removed == 0) b.append('\u0030');
            int paddingNeeded = WIDTH - b.length() % WIDTH;
            if (paddingNeeded > 0) b.append("                                                     ".substring(0,paddingNeeded));
        }
        int errors = bugCollection.getErrors().size();
        if (errors > 0) b.append("     ").append(errors).append(" errors");
        out.println(b.toString());
    }
    public void dumpOriginal(java.io.PrintStream out) {
        out.println("seq	version	time	classes	NCSS	added	newCode	fixed	removed	retained	dead	active");
        for (int i = 0; i < versionList.length; ++i) {
            edu.umd.cs.findbugs.workflow.MineBugHistory.Version version = versionList[i];
            edu.umd.cs.findbugs.AppVersion appVersion = sequenceToAppVersionMap.get(version.getSequence());
            out.print(i);
            out.print('\t');
            out.print(appVersion != null ? appVersion.getReleaseName() : "");
            out.print('\t');
            if (formatDates) out.print("\"" + (appVersion != null ? dateFormat.format(new java.util.Date(appVersion.getTimestamp())) : "") + "\"");
            else out.print(appVersion != null ? appVersion.getTimestamp() / 1000 : 0L);
            out.print('\t');
            if (appVersion != null) {
                out.print(appVersion.getNumClasses());
                out.print('\t');
                out.print(appVersion.getCodeSize());
            }
            else out.print("	0	0");
            for (int j = 0; j < TUPLE_SIZE; ++j) {
                out.print('\t');
                out.print(version.get(j));
            }
            out.println();
        }
    }
    private static void pad(int width, java.io.PrintStream out) {
        while (width-- > 0) out.print('\u0020');
    }
    private static void print(int width, boolean alignRight, java.io.PrintStream out, java.lang.Object obj) {
        java.lang.String s = java.lang.String.valueOf(obj);
        int padLen = width - s.length();
        if (alignRight) pad(padLen,out);
        out.print(s);
        if ( !alignRight) pad(padLen,out);
    }
    public void dumpNoTabs(java.io.PrintStream out) {
        print(3,true,out,"seq");
        out.print('\u0020');
        print(19,false,out,"version");
        out.print('\u0020');
        print(formatDates ? 12 : 10,false,out,"time");
        print(1 + 7,true,out,"classes");
        print(1 + WIDTH,true,out,"NCSS");
        print(1 + WIDTH,true,out,"added");
        print(1 + WIDTH,true,out,"newCode");
        print(1 + WIDTH,true,out,"fixed");
        print(1 + WIDTH,true,out,"removed");
        print(1 + WIDTH,true,out,"retained");
        print(1 + WIDTH,true,out,"dead");
        print(1 + WIDTH,true,out,"active");
        out.println();
        for (int i = 0; i < versionList.length; ++i) {
            edu.umd.cs.findbugs.workflow.MineBugHistory.Version version = versionList[i];
            edu.umd.cs.findbugs.AppVersion appVersion = sequenceToAppVersionMap.get(version.getSequence());
            print(3,true,out,i);
            out.print('\u0020');
            print(19,false,out,appVersion != null ? appVersion.getReleaseName() : "");
            out.print('\u0020');
            long ts = (appVersion != null ? appVersion.getTimestamp() : 0L);
            if (formatDates) print(12,false,out,dateFormat.format(ts));
            else print(10,false,out,ts / 1000);
            out.print('\u0020');
            print(7,true,out,appVersion != null ? appVersion.getNumClasses() : 0);
            out.print('\u0020');
            print(WIDTH,true,out,appVersion != null ? appVersion.getCodeSize() : 0);
            for (int j = 0; j < TUPLE_SIZE; ++j) {
                out.print('\u0020');
                print(WIDTH,true,out,version.get(j));
            }
            out.println();
        }
    }
    public void dumpXml(java.io.PrintStream out) {
        out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        out.println("<history>");
        java.lang.String startData = "    <data ";
        java.lang.String stop = "/>";
        for (int i = 0; i < versionList.length; ++i) {
            edu.umd.cs.findbugs.workflow.MineBugHistory.Version version = versionList[i];
            edu.umd.cs.findbugs.AppVersion appVersion = sequenceToAppVersionMap.get(version.getSequence());
            out.print("  <historyItem ");
            out.print("seq=\"");
            out.print(i);
            out.print("\" ");
            out.print("version=\"");
            out.print(appVersion != null ? appVersion.getReleaseName() : "");
            out.print("\" ");
            out.print("time=\"");
            if (formatDates) out.print((appVersion != null ? new java.util.Date(appVersion.getTimestamp()).toString() : ""));
            else out.print(appVersion != null ? appVersion.getTimestamp() : 0L);
            out.print("\"");
            out.println(">");
            java.lang.String[] attributeName = new java.lang.String[TUPLE_SIZE];
            attributeName[0] = "added";
            attributeName[1] = "newCode";
            attributeName[2] = "fixed";
            attributeName[3] = "removed";
            attributeName[4] = "retained";
            attributeName[5] = "dead";
            attributeName[6] = "active";
            for (int j = 0; j < TUPLE_SIZE; ++j) {
                if (j == 1 || j == 4) {
                    continue;
                }
                out.print(startData + " name=\"" + attributeName[j] + "\" value=\"");
                out.print(version.get(j));
                out.print("\"");
                out.println(stop);
            }
            out.println("  </historyItem>");
        }
        out.print("</history>");
    }
    private int getKey(boolean activePrevious, boolean activeCurrent) {
        if (activePrevious) return activeCurrent ? RETAINED : REMOVED;
        else return activeCurrent ? ADDED : DEAD;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
// load plugins
        edu.umd.cs.findbugs.workflow.MineBugHistory mineBugHistory = new edu.umd.cs.findbugs.workflow.MineBugHistory();
        edu.umd.cs.findbugs.workflow.MineBugHistory.MineBugHistoryCommandLine commandLine = mineBugHistory.new MineBugHistoryCommandLine();
        int argCount = commandLine.parse(args,0,2,"Usage: " + edu.umd.cs.findbugs.workflow.MineBugHistory.class.getName() + " [options] [<xml results> [<history]] ");
        edu.umd.cs.findbugs.SortedBugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (argCount < args.length) bugCollection.readXML(args[argCount++]);
        else bugCollection.readXML(java.lang.System.in);
        mineBugHistory.setBugCollection(bugCollection);
        mineBugHistory.execute();
        java.io.PrintStream out = java.lang.System.out;
        try {
            if (argCount < args.length) {
                out = edu.umd.cs.findbugs.charsets.UTF8.printStream(new java.io.FileOutputStream(args[argCount++]),true);
            }
            mineBugHistory.dump(out);
        }
        finally {
            out.close();
        }
    }
}
