package edu.umd.cs.findbugs.cloud;
import edu.umd.cs.findbugs.cloud.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
public class MutableCloudTask extends java.lang.Object implements edu.umd.cs.findbugs.cloud.Cloud.CloudTask {
    private java.lang.String name;
    private java.util.concurrent.CopyOnWriteArrayList<edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener> listeners = new java.util.concurrent.CopyOnWriteArrayList<edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener>();
    private java.lang.String substatus = "";
    private double percentDone = 0;
/** A listener used only if no other listeners are present. */
    private edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener defaultListener;
    private boolean finished = false;
    private boolean useDefaultListener = true;
    public MutableCloudTask(java.lang.String name) {
        super();
        this.name = name;
    }
    public java.lang.String getName() {
        return name;
    }
    public java.lang.String getStatusLine() {
        return substatus;
    }
    public double getPercentCompleted() {
        return percentDone;
    }
    public void addListener(edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener listener) {
        listeners.addIfAbsent(listener);
    }
    public void removeListener(edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener listener) {
        listeners.remove(listener);
    }
    public boolean isFinished() {
        return finished;
    }
    public void setUseDefaultListener(boolean enabled) {
        this.useDefaultListener = enabled;
    }
    public void update(java.lang.String substatus, double percentDone) {
        this.substatus = substatus;
        this.percentDone = percentDone;
        for (edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener listener : this.getListeners()){
            listener.taskStatusUpdated(substatus,percentDone);
        }
;
    }
    public void finished() {
        finished = true;
        for (edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener listener : this.getListeners()){
            listener.taskFinished();
        }
;
        this.clearListeners();
    }
    public void failed(java.lang.String message) {
        finished = true;
        for (edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener listener : this.getListeners()){
            listener.taskFailed(message);
        }
;
        this.clearListeners();
    }
/** A listener used only if no other listeners are present. */
    public void setDefaultListener(edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener defaultListener) {
        this.defaultListener = defaultListener;
    }
    private java.util.List<edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener> getListeners() {
        java.util.List<edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener> myListeners = new java.util.ArrayList<edu.umd.cs.findbugs.cloud.Cloud.CloudTaskListener>(listeners);
        if (useDefaultListener && myListeners.isEmpty() && defaultListener != null) {
            myListeners.add(defaultListener);
        }
        return myListeners;
    }
/** I think this is a good idea for garbage collection purposes -Keith */
    private void clearListeners() {
        listeners.clear();
        defaultListener = null;
    }
    public boolean isUsingDefaultListener() {
        return listeners.isEmpty();
    }
}
