/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * This is the .fas file stored when projects are saved All project related
 * information goes here. Anything that would be shared between multiple
 * projects goes into GUISaveState instead
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.WillClose;
import edu.umd.cs.findbugs.gui2.BugTreeModel.BranchOperationException;
public class ProjectSettings extends java.lang.Object implements java.io.Serializable {
    final private static long serialVersionUID = 6505872267795979672L;
// Singleton
    public ProjectSettings() {
        super();
        allMatchers = new edu.umd.cs.findbugs.gui2.CompoundMatcher();
        filters = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.FilterMatcher>();
    }
    private static edu.umd.cs.findbugs.gui2.ProjectSettings instance;
    public static edu.umd.cs.findbugs.gui2.ProjectSettings newInstance() {
        instance = new edu.umd.cs.findbugs.gui2.ProjectSettings();
        return instance;
    }
    public static synchronized edu.umd.cs.findbugs.gui2.ProjectSettings getInstance() {
        if (instance == null) instance = new edu.umd.cs.findbugs.gui2.ProjectSettings();
        return instance;
    }
/**
     * The list of all defined filters
     */
    private java.util.ArrayList<edu.umd.cs.findbugs.gui2.FilterMatcher> filters;
/**
     * The CompoundMatcher enveloping all enabled matchers.
     */
    private edu.umd.cs.findbugs.gui2.CompoundMatcher allMatchers;
/**
     * Max number of previous comments stored.
     */
    private int maxSizeOfPreviousComments;
    public static void loadInstance(java.io.InputStream in) {
        try {
            instance = (edu.umd.cs.findbugs.gui2.ProjectSettings) (new java.io.ObjectInputStream(in).readObject()) ;
            edu.umd.cs.findbugs.gui2.PreferencesFrame.getInstance().updateFilterPanel();
        }
        catch (java.lang.ClassNotFoundException e){
            if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.err.println("Error in deserializing Settings:");
            edu.umd.cs.findbugs.gui2.Debug.println(e);
        }
        catch (java.io.IOException e){
            if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.err.println("IO error in deserializing Settings:");
            edu.umd.cs.findbugs.gui2.Debug.println(e);
            instance = newInstance();
        }
        finally {
            try {
                in.close();
            }
            catch (java.io.IOException e){
                assert false;
            }
        }
    }
    public void save(java.io.OutputStream out) {
        try {
            new java.io.ObjectOutputStream(out).writeObject(this);
        }
        catch (java.io.IOException e){
            if (edu.umd.cs.findbugs.gui2.MainFrame.GUI2_DEBUG) java.lang.System.err.println("Error serializing Settings:");
            edu.umd.cs.findbugs.gui2.Debug.println(e);
        }
        finally {
            try {
                out.close();
            }
            catch (java.io.IOException e){
// nothing to do
                assert true;
            }
        }
    }
    public void addFilter(edu.umd.cs.findbugs.gui2.FilterMatcher filter) {
        filters.add(filter);
        allMatchers.add(filter);
        if ( !(filter instanceof edu.umd.cs.findbugs.gui2.StackedFilterMatcher)) edu.umd.cs.findbugs.gui2.FilterActivity.notifyListeners(edu.umd.cs.findbugs.gui2.FilterListener.Action.FILTERING,null);
        else {
            edu.umd.cs.findbugs.gui2.StackedFilterMatcher theSame = (edu.umd.cs.findbugs.gui2.StackedFilterMatcher) (filter) ;
            edu.umd.cs.findbugs.gui2.FilterMatcher[] filtersInStack = theSame.getFilters();
            java.util.ArrayList<edu.umd.cs.findbugs.gui2.Sortables> order = edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSorter().getOrder();
            int sizeToCheck = filtersInStack.length;
            java.util.List<edu.umd.cs.findbugs.gui2.Sortables> sortablesToCheck = order.subList(0,sizeToCheck);
            edu.umd.cs.findbugs.gui2.Debug.println("Size to check" + sizeToCheck + " checking list" + sortablesToCheck);
            edu.umd.cs.findbugs.gui2.Debug.println("checking filters");
            java.util.ArrayList<java.lang.String> almostPath = new java.util.ArrayList<java.lang.String>();
            java.util.ArrayList<edu.umd.cs.findbugs.gui2.Sortables> almostPathSortables = new java.util.ArrayList<edu.umd.cs.findbugs.gui2.Sortables>();
            for (int x = 0; x < sortablesToCheck.size(); x++) {
                edu.umd.cs.findbugs.gui2.Sortables s = sortablesToCheck.get(x);
                for (edu.umd.cs.findbugs.gui2.FilterMatcher fm : filtersInStack){
                    if (s.equals(fm.getFilterBy())) {
                        almostPath.add(fm.getValue());
                        almostPathSortables.add(fm.getFilterBy());
                    }
                }
;
            }
            if (almostPath.size() == filtersInStack.length) {
                java.util.ArrayList<java.lang.String> finalPath = new java.util.ArrayList<java.lang.String>();
                for (int x = 0; x < almostPath.size(); x++) {
                    edu.umd.cs.findbugs.gui2.Sortables s = almostPathSortables.get(x);
                    if (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getSorter().getOrderBeforeDivider().contains(s)) finalPath.add(almostPath.get(x));
                }
                edu.umd.cs.findbugs.gui2.BugTreeModel model = (edu.umd.cs.findbugs.gui2.MainFrame.getInstance().getBugTreeModel());
                try {
                    model.sendEvent(model.removeBranch(finalPath),edu.umd.cs.findbugs.gui2.BugTreeModel.TreeModification.REMOVE);
                }
                catch (edu.umd.cs.findbugs.gui2.BugTreeModel.BranchOperationException e){
                    throw new java.lang.IllegalStateException("They added a stacked filter on a branch that doesn't exist... Whaa?");
                }
            }
            else {
                edu.umd.cs.findbugs.gui2.FilterActivity.notifyListeners(edu.umd.cs.findbugs.gui2.FilterListener.Action.FILTERING,null);
                throw new java.lang.IllegalStateException("What huh?  How'd they add a stacked filter matcher bigger than the number of branches in the tree?!");
            }
        }
        edu.umd.cs.findbugs.gui2.PreferencesFrame.getInstance().updateFilterPanel();
        edu.umd.cs.findbugs.gui2.MainFrame.getInstance().updateStatusBar();
    }
    public void addFilters(edu.umd.cs.findbugs.gui2.FilterMatcher[] newFilters) {
        for (edu.umd.cs.findbugs.gui2.FilterMatcher i : newFilters)if ( !filters.contains(i)) {
            filters.add(i);
            allMatchers.add(i);
        }
        else {
            filters.get(filters.indexOf(i)).setActive(true);
        }
;
        edu.umd.cs.findbugs.gui2.FilterActivity.notifyListeners(edu.umd.cs.findbugs.gui2.FilterListener.Action.FILTERING,null);
        edu.umd.cs.findbugs.gui2.PreferencesFrame.getInstance().updateFilterPanel();
        edu.umd.cs.findbugs.gui2.MainFrame.getInstance().updateStatusBar();
    }
    public boolean removeFilter(edu.umd.cs.findbugs.gui2.FilterMatcher filter) {
        boolean result = filters.remove(filter) && allMatchers.remove(filter);
        edu.umd.cs.findbugs.gui2.FilterActivity.notifyListeners(edu.umd.cs.findbugs.gui2.FilterListener.Action.UNFILTERING,null);
        edu.umd.cs.findbugs.gui2.PreferencesFrame.getInstance().updateFilterPanel();
        edu.umd.cs.findbugs.gui2.MainFrame.getInstance().updateStatusBar();
        return result;
    }
    java.util.ArrayList<edu.umd.cs.findbugs.gui2.FilterMatcher> getAllFilters() {
        return filters;
    }
/**
     * @return Returns the maximum number of previous comments stored.
     */
    public int getMaxSizeOfPreviousComments() {
        return maxSizeOfPreviousComments;
    }
/**
     * Sets the maximum number of previous comments stored.
     * 
     * @param num
     */
    public void setMaxSizeOfPreviousComments(int num) {
        maxSizeOfPreviousComments = num;
    }
}
