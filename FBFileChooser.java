/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * All FileChoosers are FBFileChoosers so font size will work
 * 
 * @author Kristin
 * 
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import edu.umd.cs.findbugs.SystemProperties;
public class FBFileChooser extends javax.swing.JFileChooser {
    public FBFileChooser() {
        super();
        this.addHiddenFileCheckBox();
        assert java.awt.EventQueue.isDispatchThread();
        this.setCurrentDirectory(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getStarterDirectoryForLoadBugs());
    }
/**
     * Sets size of font
     * 
     * @param size
     */
    protected void setFontSize(float size) {
        this.setFont(this.getFont().deriveFont(size));
        this.setFontSizeHelper(this.getComponents(),size);
    }
/*
     * Helps above method, runs through all components recursively.
     */
    protected void setFontSizeHelper(java.awt.Component[] comps, float size) {
        if (comps.length <= 0) return;
        for (java.awt.Component comp : comps){
            comp.setFont(comp.getFont().deriveFont(size));
            if (comp instanceof java.awt.Container) this.setFontSizeHelper(((java.awt.Container) (comp) ).getComponents(),size);
        }
;
    }
    public void addNotify() {
        super.addNotify();
        this.setFontSize(edu.umd.cs.findbugs.gui2.Driver.getFontSize());
    }
    private static void workAroundJFileChooserBug() {
// Travis McLeskey
// http://www.mcleskey.org/bugs.html
        try {
            java.lang.Object o = javax.swing.UIManager.getBorder("TableHeader.cellBorder");
            java.lang.reflect.Method m = o.getClass().getMethod("setHorizontalShift",new java.lang.Class[]{int.class});
            m.invoke(o,0);
        }
        catch (java.lang.NoSuchMethodException e){
            assert true;
        }
        catch (java.lang.reflect.InvocationTargetException e){
            assert true;
        }
        catch (java.lang.IllegalAccessException e){
            assert true;
        }
    }
    public int showOpenDialog(java.awt.Component parent) {
        assert java.awt.EventQueue.isDispatchThread();
        int x = super.showOpenDialog(parent);
        if (edu.umd.cs.findbugs.SystemProperties.getProperty("os.name").startsWith("Mac")) workAroundJFileChooserBug();
        edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setStarterDirectoryForLoadBugs(this.getCurrentDirectory());
        return x;
    }
    public int showSaveDialog(java.awt.Component parent) {
        assert java.awt.EventQueue.isDispatchThread();
        int x = super.showSaveDialog(parent);
        if (edu.umd.cs.findbugs.SystemProperties.getProperty("os.name").startsWith("Mac")) workAroundJFileChooserBug();
        edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setStarterDirectoryForLoadBugs(this.getCurrentDirectory());
        return x;
    }
    public int showDialog(java.awt.Component parent, java.lang.String approveButtonText) {
        assert java.awt.EventQueue.isDispatchThread();
        int x = super.showDialog(parent,approveButtonText);
        if (edu.umd.cs.findbugs.SystemProperties.getProperty("os.name").startsWith("Mac")) workAroundJFileChooserBug();
        edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setStarterDirectoryForLoadBugs(this.getCurrentDirectory());
        return x;
    }
    private void addHiddenFileCheckBox() {
        final javax.swing.JCheckBox showHiddenFileCheckBox = new javax.swing.JCheckBox("Show Hidden");
        javax.swing.JPanel accessory = new javax.swing.JPanel();
        accessory.setLayout(new java.awt.FlowLayout());
        accessory.add(showHiddenFileCheckBox);
        this.setAccessory(accessory);
        showHiddenFileCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent ae) {
                edu.umd.cs.findbugs.gui2.FBFileChooser.this.setFileHidingEnabled( !showHiddenFileCheckBox.isSelected());
            }
        });
    }
}
