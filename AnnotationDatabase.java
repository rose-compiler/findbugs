package edu.umd.cs.findbugs.ba;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Database to keep track of annotated fields/methods/classes/etc. for a
 * particular kind of annotation.
 *
 * @author William Pugh
 */
// private Subtypes subtypes;
// if (!Subtypes.DO_NOT_USE) {
// subtypes = AnalysisContext.currentAnalysisContext().getSubtypes();
// }
// TODO: Parameterize these values?
// Don't
// get inherited annotation
// check to see if method is defined in this class;
// if not, on't consider default annotations
// if not static
// associated with method
// <init> method parameters for inner classes don't inherit default
// annotations
// since some of them are synthetic
// synthetic elements should not inherit default annotations
// look for default annotation
/**
     * @param o
     * @return
     */
// Look in supermethod
// if (!Subtypes.DO_NOT_USE) {
// subtypes.addNamedClass(cName);
// }
// if (!Subtypes.DO_NOT_USE) {
// subtypes.addNamedClass(cName);
// }
// return annotation instanceof NullnessAnnotation; work around JDK bug
// if (!Subtypes.DO_NOT_USE) {
// subtypes.addNamedClass(cName);
// }
abstract interface package-info {
}
