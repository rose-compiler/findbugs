/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import org.apache.bcel.generic.AASTORE;
import org.apache.bcel.generic.ARETURN;
import org.apache.bcel.generic.ArrayInstruction;
import org.apache.bcel.generic.CHECKCAST;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FieldInstruction;
import org.apache.bcel.generic.INVOKEINTERFACE;
import org.apache.bcel.generic.INVOKESPECIAL;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.PUTFIELD;
import org.apache.bcel.generic.PUTSTATIC;
abstract public class ResourceValueFrameModelingVisitor extends edu.umd.cs.findbugs.ba.AbstractFrameModelingVisitor {
    public ResourceValueFrameModelingVisitor(org.apache.bcel.generic.ConstantPoolGen cpg) {
        super(cpg);
    }
    public edu.umd.cs.findbugs.ba.ResourceValue getDefaultValue() {
        return edu.umd.cs.findbugs.ba.ResourceValue.notInstance();
    }
/**
     * Subclasses must override this to model the effect of the given
     * instruction on the current frame.
     */
    abstract public void transferInstruction(org.apache.bcel.generic.InstructionHandle handle, edu.umd.cs.findbugs.ba.BasicBlock basicBlock) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException;
// Things to do:
// Automatically detect when resource instances escape:
// - putfield, putstatic
// - parameters to invoke, but subclasses may override
// - aastore; (conservative, since the dest array may not itself escape)
// - return (areturn)
    private void handleFieldStore(org.apache.bcel.generic.FieldInstruction ins) {
        try {
// If the resource instance is stored in a field, then it escapes
            edu.umd.cs.findbugs.ba.ResourceValueFrame frame = this.getFrame();
            edu.umd.cs.findbugs.ba.ResourceValue topValue = frame.getTopValue();
            if (topValue.equals(edu.umd.cs.findbugs.ba.ResourceValue.instance())) frame.setStatus(edu.umd.cs.findbugs.ba.ResourceValueFrame.ESCAPED);
        }
        catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
            throw new edu.umd.cs.findbugs.ba.InvalidBytecodeException("Stack underflow", e);
        }
        this.handleNormalInstruction(ins);
    }
    public void visitPUTFIELD(org.apache.bcel.generic.PUTFIELD putfield) {
        this.handleFieldStore(putfield);
    }
    private void handleArrayStore(org.apache.bcel.generic.ArrayInstruction ins) {
        try {
// If the resource instance is stored in an array, then we consider
// it as having escaped. This is conservative; ideally we would
// check whether this array is a field or gets passed out of the
// method.
            edu.umd.cs.findbugs.ba.ResourceValueFrame frame = this.getFrame();
            edu.umd.cs.findbugs.ba.ResourceValue topValue = frame.getTopValue();
            if (topValue.equals(edu.umd.cs.findbugs.ba.ResourceValue.instance())) {
                frame.setStatus(edu.umd.cs.findbugs.ba.ResourceValueFrame.ESCAPED);
            }
        }
        catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
            throw new edu.umd.cs.findbugs.ba.InvalidBytecodeException("Stack underflow", e);
        }
        this.handleNormalInstruction(ins);
    }
    public void visitAASTORE(org.apache.bcel.generic.AASTORE arr) {
        this.handleArrayStore(arr);
    }
    public void visitPUTSTATIC(org.apache.bcel.generic.PUTSTATIC putstatic) {
        this.handleFieldStore(putstatic);
    }
/**
     * Override this to check for methods that it is legal to pass the instance
     * to without the instance escaping. By default, we consider all methods to
     * be possible escape routes.
     * 
     * @param inv
     *            the InvokeInstruction to which the resource instance is passed
     *            as an argument
     * @param instanceArgNum
     *            the first argument the instance is passed in
     */
    protected boolean instanceEscapes(org.apache.bcel.generic.InvokeInstruction inv, int instanceArgNum) {
        return true;
    }
    private void handleInvoke(org.apache.bcel.generic.InvokeInstruction inv) {
        edu.umd.cs.findbugs.ba.ResourceValueFrame frame = this.getFrame();
        int numSlots = frame.getNumSlots();
        int numConsumed = this.getNumWordsConsumed(inv);
// See if the resource instance is passed as an argument
        int instanceArgNum =  -1;
        for (int i = numSlots - numConsumed, argCount = 0; i < numSlots; ++i, ++argCount) {
            edu.umd.cs.findbugs.ba.ResourceValue value = frame.getValue(i);
            if (value.equals(edu.umd.cs.findbugs.ba.ResourceValue.instance())) {
                instanceArgNum = argCount;
                break;
            }
        }
        if (instanceArgNum >= 0 && this.instanceEscapes(inv,instanceArgNum)) frame.setStatus(edu.umd.cs.findbugs.ba.ResourceValueFrame.ESCAPED);
        this.handleNormalInstruction(inv);
    }
    public void visitCHECKCAST(org.apache.bcel.generic.CHECKCAST obj) {
        try {
            edu.umd.cs.findbugs.ba.ResourceValueFrame frame = this.getFrame();
            edu.umd.cs.findbugs.ba.ResourceValue topValue;
            topValue = frame.getTopValue();
            if (topValue.equals(edu.umd.cs.findbugs.ba.ResourceValue.instance())) frame.setStatus(edu.umd.cs.findbugs.ba.ResourceValueFrame.ESCAPED);
        }
        catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Analysis error",e);
        }
    }
    public void visitINVOKEVIRTUAL(org.apache.bcel.generic.INVOKEVIRTUAL inv) {
        this.handleInvoke(inv);
    }
    public void visitINVOKEINTERFACE(org.apache.bcel.generic.INVOKEINTERFACE inv) {
        this.handleInvoke(inv);
    }
    public void visitINVOKESPECIAL(org.apache.bcel.generic.INVOKESPECIAL inv) {
        this.handleInvoke(inv);
    }
    public void visitINVOKESTATIC(org.apache.bcel.generic.INVOKESTATIC inv) {
        this.handleInvoke(inv);
    }
    public void visitARETURN(org.apache.bcel.generic.ARETURN ins) {
        try {
            edu.umd.cs.findbugs.ba.ResourceValueFrame frame = this.getFrame();
            edu.umd.cs.findbugs.ba.ResourceValue topValue = frame.getTopValue();
            if (topValue.equals(edu.umd.cs.findbugs.ba.ResourceValue.instance())) frame.setStatus(edu.umd.cs.findbugs.ba.ResourceValueFrame.ESCAPED);
        }
        catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
            throw new edu.umd.cs.findbugs.ba.InvalidBytecodeException("Stack underflow", e);
        }
        this.handleNormalInstruction(ins);
    }
}
// vim:ts=4
