package edu.umd.cs.findbugs.ba;
/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Perform dataflow analysis on a method using a control flow graph. Both
 * forward and backward analyses can be performed.
 * <ul>
 * <li>The "start" point of each block is the entry (forward analyses) or the
 * exit (backward analyses).
 * <li>The "result" point of each block is the exit (forward analyses) or the
 * entry (backward analyses).
 * </ul>
 * The analysis's transfer function is applied to transform the meet of the
 * results of the block's logical predecessors (the block's start facts) into
 * the block's result facts.
 *
 * @author David Hovemeyer
 * @see CFG
 * @see DataflowAnalysis
 */
/**
     * Constructor.
     *
     * @param cfg
     *            the control flow graph
     * @param analysis
     *            the DataflowAnalysis to be run
     */
// Initialize result facts
// Entry block: set to entry fact
// Set to top
// Maximum number of iterations before we assume there is a bug and give up.
/**
     * Run the algorithm. Afterwards, caller can use the getStartFact() and
     * getResultFact() methods to to get dataflow facts at start and result
     * points of each block.
     */
// For each block in CFG...
// Get start fact for block.
// Get result facts for block,
// Meet all of the logical predecessor results into this block's
// start.
// Special case: if the block is the logical entry, then it gets
// the special "entry fact".
// don't need to check to see if we need to recompute.
// may need to se sawBackEdge
// Get the predecessor result fact
// break;
// Get the predecessor result fact
// Apply the edge transfer function.
// Merge the predecessor fact (possibly transformed
// by the edge transfer function)
// into the block's start fact.
// making a copy of result facts (so we can detect if it
// changed).
// Apply the transfer function.
// See if the result changed.
/**
     * @param msg
     *            TODO
     *
     */
/**
     * Return the number of iterations of the main execution loop.
     */
/**
     * Get dataflow facts for start of given block.
     */
/**
     * Get dataflow facts for end of given block.
     */
/**
     * Get dataflow fact at (just before) given Location. Note "before" is meant
     * in the logical sense, so for backward analyses, before means after the
     * location in the control flow sense.
     *
     * @param location
     *            the Location
     * @return the dataflow value at given Location
     * @throws DataflowAnalysisException
     */
/* final */
/**
     * Get the dataflow fact representing the point just after given Location.
     * Note "after" is meant in the logical sense, so for backward analyses,
     * after means before the location in the control flow sense.
     *
     * @param location
     *            the Location
     * @return the dataflow value after given Location
     * @throws DataflowAnalysisException
     */
/* final */
/**
     * Get the fact that is true on the given control edge.
     *
     * @param edge
     *            the edge
     * @return the fact that is true on the edge
     * @throws DataflowAnalysisException
     */
/**
     * Get the analysis object.
     */
/**
     * Get the CFG object.
     */
/**
     * Return an Iterator over edges that connect given block to its logical
     * predecessors. For forward analyses, this is the incoming edges. For
     * backward analyses, this is the outgoing edges.
     */
/**
     * Get the "logical" entry block of the CFG. For forward analyses, this is
     * the entry block. For backward analyses, this is the exit block.
     */
// vim:ts=4
abstract interface package-info {
}
