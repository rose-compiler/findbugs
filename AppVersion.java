/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A version of an analyzed application. Application versions are uniquely
 * identified by a sequence number, which represents a run of FindBugs on the
 * application. Timestamp is when FindBugs was run (according to
 * System.currentTimeMillis()), and the release name is available if the user
 * provided it.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.util.Date;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
import edu.umd.cs.findbugs.xml.XMLWriteable;
public class AppVersion extends java.lang.Object implements edu.umd.cs.findbugs.xml.XMLWriteable, java.lang.Cloneable {
/**
     * XML element name for a stored AppVersion object.
     */
    final public static java.lang.String ELEMENT_NAME = "AppVersion";
    private long sequence;
    private long timestamp;
    private java.lang.String releaseName;
    private int numClasses;
    private int codeSize;
    public AppVersion(long sequence, long time, java.lang.String name) {
        super();
        this.sequence = sequence;
        this.timestamp = time;
        this.releaseName = name;
    }
    public AppVersion(long sequence, java.util.Date time, java.lang.String name) {
        super();
        this.sequence = sequence;
        this.timestamp = time.getTime();
        this.releaseName = name;
    }
    public AppVersion(long sequence) {
        super();
        this.sequence = sequence;
        this.timestamp = java.lang.System.currentTimeMillis();
        this.releaseName = "";
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */
    public java.lang.Object clone() {
        try {
            return super.clone();
        }
        catch (java.lang.CloneNotSupportedException e){
            throw new java.lang.AssertionError(e);
        }
    }
/**
     * @return Returns the sequence.
     */
    public long getSequenceNumber() {
        return sequence;
    }
/**
     * @return Returns the timestamp.
     */
    public long getTimestamp() {
        if (timestamp <= 0) return java.lang.System.currentTimeMillis();
        return timestamp;
    }
/**
     * @return Returns the releaseName.
     */
    public java.lang.String getReleaseName() {
        return releaseName;
    }
/**
     * @param timestamp
     *            The timestamp to set.
     */
    public edu.umd.cs.findbugs.AppVersion setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }
/**
     * @param releaseName
     *            The releaseName to set.
     */
    public edu.umd.cs.findbugs.AppVersion setReleaseName(java.lang.String releaseName) {
        this.releaseName = releaseName;
        return this;
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.xml.XMLWriteable#writeXML(edu.umd.cs.findbugs.xml
     * .XMLOutput)
     */
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        xmlOutput.openCloseTag(ELEMENT_NAME,new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("sequence",java.lang.String.valueOf(sequence)).addAttribute("timestamp",java.lang.String.valueOf(timestamp)).addAttribute("release",releaseName).addAttribute("codeSize",java.lang.String.valueOf(codeSize)).addAttribute("numClasses",java.lang.String.valueOf(numClasses)));
    }
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        buf.append(java.lang.String.valueOf(sequence));
        buf.append('\u002c');
        buf.append(java.lang.String.valueOf(timestamp));
        buf.append('\u002c');
        buf.append(releaseName);
        buf.append('\u002c');
        buf.append(codeSize);
        buf.append('\u002c');
        buf.append(codeSize);
        return buf.toString();
    }
/**
     * @param numClasses
     *            The numClasses to set.
     */
    public edu.umd.cs.findbugs.AppVersion setNumClasses(int numClasses) {
        this.numClasses = numClasses;
        return this;
    }
/**
     * @return Returns the numClasses.
     */
    public int getNumClasses() {
        return numClasses;
    }
/**
     * @param codeSize
     *            The codeSize to set.
     */
    public edu.umd.cs.findbugs.AppVersion setCodeSize(int codeSize) {
        this.codeSize = codeSize;
        return this;
    }
/**
     * @return Returns the codeSize.
     */
    public int getCodeSize() {
        return codeSize;
    }
}
