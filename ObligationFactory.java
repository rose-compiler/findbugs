/*
 * Bytecode Analysis Framework
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Factory for Obligation and ObligationSet objects to be used in an instance of
 * ObligationAnalysis.
 */
package edu.umd.cs.findbugs.ba.obl;
import edu.umd.cs.findbugs.ba.obl.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.annotation.CheckForNull;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.Type;
import edu.umd.cs.findbugs.annotations.SuppressWarnings;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.XMethod;
import edu.umd.cs.findbugs.bcel.BCELUtil;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
import edu.umd.cs.findbugs.classfile.Global;
import edu.umd.cs.findbugs.internalAnnotations.DottedClassName;
public class ObligationFactory extends java.lang.Object {
    private java.util.Map<java.lang.String, edu.umd.cs.findbugs.ba.obl.Obligation> classNameToObligationMap;
    private java.util.Set<java.lang.String> slashedClassNames = new java.util.HashSet<java.lang.String>();
// // XXX: this is just for debugging.
// static ObligationFactory lastInstance;
    public ObligationFactory() {
        super();
        this.classNameToObligationMap = new java.util.HashMap<java.lang.String, edu.umd.cs.findbugs.ba.obl.Obligation>();
    }
// lastInstance = this;
    public int getMaxObligationTypes() {
        return classNameToObligationMap.size();
    }
    public boolean signatureInvolvesObligations(java.lang.String sig) {
        for (java.lang.String c : slashedClassNames)if (sig.indexOf(c) >= 0) return true;
;
        return false;
    }
/**
     * Determine whether class named by given ClassDescriptor is an Obligation
     * type.
     * 
     * @param classDescriptor
     *            a class
     * @return true if the class is an Obligation type, false otherwise
     */
    public boolean isObligationType(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor) {
        try {
            return this.getObligationByType(edu.umd.cs.findbugs.bcel.BCELUtil.getObjectTypeInstance(classDescriptor.toDottedClassName())) != null;
        }
        catch (java.lang.ClassNotFoundException e){
            edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getErrorLogger().reportMissingClass(e);
            return false;
        }
    }
/**
     * Get an Iterator over known Obligation types.
     * 
     * @return Iterator over known Obligation types
     */
    public java.util.Iterator<edu.umd.cs.findbugs.ba.obl.Obligation> obligationIterator() {
        return classNameToObligationMap.values().iterator();
    }
/**
     * Look up an Obligation by type. This returns the first Obligation that is
     * a supertype of the type given (meaning that the given type could be an
     * instance of the returned Obligation).
     * 
     * @param type
     *            a type
     * @return an Obligation that is a supertype of the given type, or null if
     *         there is no such Obligation
     * @throws ClassNotFoundException
     */
    public edu.umd.cs.findbugs.ba.obl.Obligation getObligationByType(org.apache.bcel.generic.ObjectType type) throws java.lang.ClassNotFoundException {
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.obl.Obligation> i = this.obligationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.obl.Obligation obligation = i.next();
            if (edu.umd.cs.findbugs.ba.Hierarchy.isSubtype(type,obligation.getType())) return obligation;
        }
        return null;
    }
/**
     * Look up an Obligation by type. This returns the first Obligation that is
     * a supertype of the type given (meaning that the given type could be an
     * instance of the returned Obligation).
     * 
     * @param classDescriptor
     *            a ClassDescriptor naming a class type
     * @return an Obligation that is a supertype of the given type, or null if
     *         there is no such Obligation
     * @throws ClassNotFoundException
     */
    public edu.umd.cs.findbugs.ba.obl.Obligation getObligationByType(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor) {
        try {
            return this.getObligationByType(edu.umd.cs.findbugs.bcel.BCELUtil.getObjectTypeInstance(classDescriptor.toDottedClassName()));
        }
        catch (java.lang.ClassNotFoundException e){
            edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getErrorLogger().reportMissingClass(e);
            return null;
        }
    }
/**
     * Get array of Obligation types corresponding to the parameters of the
     * given method.
     * 
     * @param xmethod
     *            a method
     * @return array of Obligation types for each of the method's parameters; a
     *         null element means the corresponding parameter is not an
     *         Obligation type
     */
    public edu.umd.cs.findbugs.ba.obl.Obligation[] getParameterObligationTypes(edu.umd.cs.findbugs.ba.XMethod xmethod) {
        org.apache.bcel.generic.Type[] paramTypes = org.apache.bcel.generic.Type.getArgumentTypes(xmethod.getSignature());
        edu.umd.cs.findbugs.ba.obl.Obligation[] result = new edu.umd.cs.findbugs.ba.obl.Obligation[paramTypes.length];
        for (int i = 0; i < paramTypes.length; i++) {
            if ( !(paramTypes[i] instanceof org.apache.bcel.generic.ObjectType)) {
                continue;
            }
            try {
                result[i] = this.getObligationByType((org.apache.bcel.generic.ObjectType) (paramTypes[i]) );
            }
            catch (java.lang.ClassNotFoundException e){
                edu.umd.cs.findbugs.classfile.Global.getAnalysisCache().getErrorLogger().reportMissingClass(e);
            }
        }
        return result;
    }
    public edu.umd.cs.findbugs.ba.obl.Obligation addObligation(java.lang.String className) {
        int nextId = classNameToObligationMap.size();
        slashedClassNames.add(className.replace('\u002e','\u002f'));
        edu.umd.cs.findbugs.ba.obl.Obligation obligation = new edu.umd.cs.findbugs.ba.obl.Obligation(className, nextId);
        if (classNameToObligationMap.put(className,obligation) != null) {
            throw new java.lang.IllegalStateException("Obligation " + className + " added multiple times");
        }
        return obligation;
    }
    public edu.umd.cs.findbugs.ba.obl.Obligation getObligationById(int id) {
        for (edu.umd.cs.findbugs.ba.obl.Obligation obligation : classNameToObligationMap.values()){
            if (obligation.getId() == id) return obligation;
        }
;
        return null;
    }
    public edu.umd.cs.findbugs.ba.obl.Obligation getObligationByName(java.lang.String className) {
        return classNameToObligationMap.get(className);
    }
    public edu.umd.cs.findbugs.ba.obl.ObligationSet createObligationSet() {
/* getMaxObligationTypes(), */
        return new edu.umd.cs.findbugs.ba.obl.ObligationSet(this);
    }
}
// vim:ts=4
