package edu.umd.cs.findbugs.classfile.impl;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.impl.*;
/**
 * Implementation of IAnalysisCache. This object is responsible for registering
 * class and method analysis engines and caching analysis results.
 *
 * @author David Hovemeyer
 */
/**
     *
     */
/**
     * Maximum number of class analysis results to cache.
     */
// Fields
// runtimeException.fillInStackTrace();
// checkedAnalysisException.fillInStackTrace();
/**
     * Constructor.
     *
     * @param classPath
     *            the IClassPath to load resources from
     * @param errorLogger
     *            the IErrorLogger
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.classfile.IAnalysisCache#getClassPath()
     */
// System.out.println("ZZZ : purging all method analyses");
/**
     * Cleans up all cached data
     */
/**
     * @param analysisClass non null analysis type
     * @return map with analysis data for given type, can be null
     */
/**
     * Adds the data for given analysis type from given map to the cache
     * @param analysisClass non null analysis type
     * @param map non null, pre-filled map with analysis data for given type
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#getClassAnalysis(java.lang
     * .Class, edu.umd.cs.findbugs.classfile.ClassDescriptor)
     */
// Get the descriptor->result map for this analysis class,
// creating if necessary
// See if there is a cached result in the descriptor map
// No cached result - compute (or recompute)
// Perform the analysis
// If engine returned null, we need to construct
// an AbnormalAnalysisResult object to record that fact.
// Otherwise we will try to recompute the value in
// the future.
// Exception - make note
// Andrei: e.getStackTrace() cannot be null, but getter clones
// the stack...
// if (e.getStackTrace() == null)
// e.fillInStackTrace();
// Exception - make note
// Andrei: e.getStackTrace() cannot be null, but getter clones
// the stack...
// if (e.getStackTrace() == null)
// e.fillInStackTrace();
// Save the result
// Abnormal analysis result?
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#probeClassAnalysis(java.
     * lang.Class, edu.umd.cs.findbugs.classfile.ClassDescriptor)
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#getMethodAnalysis(java.lang
     * .Class, edu.umd.cs.findbugs.classfile.MethodDescriptor)
     */
/**
     * Analyze a method.
     *
     * @param classContext
     *            ClassContext storing method analysis objects for method's
     *            class
     * @param analysisClass
     *            class the method analysis object should belong to
     * @param methodDescriptor
     *            method descriptor identifying the method to analyze
     * @return the computed analysis object for the method
     * @throws CheckedAnalysisException
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#eagerlyPutMethodAnalysis
     * (java.lang.Class, edu.umd.cs.findbugs.classfile.MethodDescriptor,
     * java.lang.Object)
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#purgeMethodAnalyses(edu.
     * umd.cs.findbugs.classfile.MethodDescriptor)
     */
/**
     * Find or create a descriptor to analysis object map.
     *
     * @param <DescriptorType>
     *            type of descriptor used as the map's key type (ClassDescriptor
     *            or MethodDescriptor)
     * @param <E>
     *            type of analysis class
     * @param analysisClassToDescriptorMapMap
     *            analysis class to descriptor map map
     * @param engineMap
     *            analysis class to analysis engine map
     * @param analysisClass
     *            the analysis map
     * @return the descriptor to analysis object map
     */
// Create a MapCache that allows the analysis engine to
// decide that analysis results should be retained indefinitely.
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#registerClassAnalysisEngine
     * (java.lang.Class, edu.umd.cs.findbugs.classfile.IClassAnalysisEngine)
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#registerMethodAnalysisEngine
     * (java.lang.Class, edu.umd.cs.findbugs.classfile.IMethodAnalysisEngine)
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#registerDatabaseFactory(
     * java.lang.Class, edu.umd.cs.findbugs.classfile.IDatabaseFactory)
     */
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#getDatabase(java.lang.Class)
     */
// Find the database factory
// Create the database
// Error - record the analysis error
// FIXME: should catch and re-throw RuntimeExceptions?
/*
     * (non-Javadoc)
     *
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisCache#eagerlyPutDatabase(java.
     * lang.Class, java.lang.Object)
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.classfile.IAnalysisCache#getErrorLogger()
     */
/*
     * (non-Javadoc)
     *
     * @see edu.umd.cs.findbugs.classfile.IAnalysisCache#getProfiler()
     */
abstract interface package-info {
}
