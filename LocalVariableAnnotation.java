/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Bug annotation class for local variable names
 * 
 * @author William Pugh
 * @see BugAnnotation
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.IOException;
import java.util.BitSet;
import java.util.Iterator;
import javax.annotation.CheckForNull;
import org.apache.bcel.classfile.LineNumberTable;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.LocalVariableTable;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.IndexedInstruction;
import org.apache.bcel.generic.InstructionHandle;
import edu.umd.cs.findbugs.OpcodeStack.Item;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.Dataflow;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.LiveLocalStoreAnalysis;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.SignatureParser;
import edu.umd.cs.findbugs.util.EditDistance;
import edu.umd.cs.findbugs.visitclass.DismantleBytecode;
import edu.umd.cs.findbugs.visitclass.PreorderVisitor;
import edu.umd.cs.findbugs.xml.XMLAttributeList;
import edu.umd.cs.findbugs.xml.XMLOutput;
public class LocalVariableAnnotation extends java.lang.Object implements edu.umd.cs.findbugs.BugAnnotation {
    final private static long serialVersionUID = 1L;
    final public static java.lang.String DEFAULT_ROLE = "LOCAL_VARIABLE_DEFAULT";
    final public static java.lang.String NAMED_ROLE = "LOCAL_VARIABLE_NAMED";
    final public static java.lang.String UNKNOWN_ROLE = "LOCAL_VARIABLE_UNKNOWN";
    final public static java.lang.String PARAMETER_ROLE = "LOCAL_VARIABLE_PARAMETER";
    final public static java.lang.String PARAMETER_NAMED_ROLE = "LOCAL_VARIABLE_PARAMETER_NAMED";
    final public static java.lang.String PARAMETER_VALUE_SOURCE_ROLE = "LOCAL_VARIABLE_PARAMETER_VALUE_SOURCE";
    final public static java.lang.String PARAMETER_VALUE_SOURCE_NAMED_ROLE = "LOCAL_VARIABLE_PARAMETER_VALUE_SOURCE_NAMED";
    final public static java.lang.String VALUE_DOOMED_ROLE = "LOCAL_VARIABLE_VALUE_DOOMED";
    final public static java.lang.String VALUE_DOOMED_NAMED_ROLE = "LOCAL_VARIABLE_VALUE_DOOMED_NAMED";
    final public static java.lang.String DID_YOU_MEAN_ROLE = "LOCAL_VARIABLE_DID_YOU_MEAN";
    final public static java.lang.String INVOKED_ON_ROLE = "LOCAL_VARIABLE_INVOKED_ON";
    final public static java.lang.String ARGUMENT_ROLE = "LOCAL_VARIABLE_ARGUMENT";
    final public static java.lang.String VALUE_OF_ROLE = "LOCAL_VARIABLE_VALUE_OF";
    final private java.lang.String name;
    final int register;
    final int pc;
    final int line;
    private java.lang.String description;
/**
     * Constructor.
     * 
     * @param name
     *            the name of the local variable
     * @param register
     *            the local variable index
     * @param pc
     *            the bytecode offset of the instruction that mentions this
     *            local variable
     */
    public LocalVariableAnnotation(java.lang.String name, int register, int pc) {
        super();
        this.name = name;
        this.register = register;
        this.pc = pc;
        this.line =  -1;
        this.description = DEFAULT_ROLE;
        this.setDescription(name.equals("?") ? "LOCAL_VARIABLE_UNKNOWN" : "LOCAL_VARIABLE_NAMED");
    }
/**
     * Constructor.
     * 
     * @param name
     *            the name of the local variable
     * @param register
     *            the local variable index
     * @param pc
     *            the bytecode offset of the instruction that mentions this
     *            local variable
     */
    public LocalVariableAnnotation(java.lang.String name, int register, int pc, int line) {
        super();
        this.name = name;
        this.register = register;
        this.pc = pc;
        this.line = line;
        this.description = DEFAULT_ROLE;
        this.setDescription(name.equals("?") ? "LOCAL_VARIABLE_UNKNOWN" : "LOCAL_VARIABLE_NAMED");
    }
    public static edu.umd.cs.findbugs.LocalVariableAnnotation getLocalVariableAnnotation(org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.ba.Location location, org.apache.bcel.generic.IndexedInstruction ins) {
        int local = ins.getIndex();
        org.apache.bcel.generic.InstructionHandle handle = location.getHandle();
        int position1 = handle.getNext().getPosition();
        int position2 = handle.getPosition();
        return getLocalVariableAnnotation(method,local,position1,position2);
    }
    public static edu.umd.cs.findbugs.LocalVariableAnnotation getLocalVariableAnnotation(org.apache.bcel.classfile.Method method, int local, int position1, int position2) {
        org.apache.bcel.classfile.LocalVariableTable localVariableTable = method.getLocalVariableTable();
        java.lang.String localName = "?";
        if (localVariableTable != null) {
            org.apache.bcel.classfile.LocalVariable lv1 = localVariableTable.getLocalVariable(local,position1);
            if (lv1 == null) {
                lv1 = localVariableTable.getLocalVariable(local,position2);
                position1 = position2;
            }
            if (lv1 != null) localName = lv1.getName();
            else for (org.apache.bcel.classfile.LocalVariable lv : localVariableTable.getLocalVariableTable()){
                if (lv.getIndex() == local) {
                    if ( !localName.equals("?") &&  !localName.equals(lv.getName())) {
                        localName = "?";
// not a single consistent name
                        break;
                    }
                    localName = lv.getName();
                }
            }
;
        }
        org.apache.bcel.classfile.LineNumberTable lineNumbers = method.getLineNumberTable();
        if (lineNumbers == null) return new edu.umd.cs.findbugs.LocalVariableAnnotation(localName, local, position1);
        int line = lineNumbers.getSourceLine(position1);
        return new edu.umd.cs.findbugs.LocalVariableAnnotation(localName, local, position1, line);
    }
/**
     * Get a local variable annotation describing a parameter.
     * 
     * @param method
     *            a Method
     * @param local
     *            the local variable containing the parameter
     * @return LocalVariableAnnotation describing the parameter
     */
    public static edu.umd.cs.findbugs.LocalVariableAnnotation getParameterLocalVariableAnnotation(org.apache.bcel.classfile.Method method, int local) {
        edu.umd.cs.findbugs.LocalVariableAnnotation lva = getLocalVariableAnnotation(method,local,0,0);
        return lva;
    }
    public java.lang.Object clone() {
        try {
            return super.clone();
        }
        catch (java.lang.CloneNotSupportedException e){
            throw new java.lang.AssertionError(e);
        }
    }
    public void accept(edu.umd.cs.findbugs.BugAnnotationVisitor visitor) {
        visitor.visitLocalVariableAnnotation(this);
    }
    public java.lang.String format(java.lang.String key, edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
// System.out.println("format: " + key + " reg: " + register + " name: "
// + value);
        if (key.equals("hash")) {
            if (register < 0) return "??";
            return name;
        }
        if (register < 0) return "?";
        if (key.equals("register")) return java.lang.String.valueOf(register);
        else if (key.equals("pc")) return java.lang.String.valueOf(pc);
        else if (key.equals("name") || key.equals("givenClass")) return name;
        else if ( !name.equals("?")) return name;
        return "$L" + register;
    }
    public void setDescription(java.lang.String description) {
        this.description = description.intern();
    }
    public java.lang.String getDescription() {
        return description;
    }
    public int hashCode() {
        return name.hashCode();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.LocalVariableAnnotation)) return false;
        return name.equals(((edu.umd.cs.findbugs.LocalVariableAnnotation) (o) ).name);
    }
    public int compareTo(edu.umd.cs.findbugs.BugAnnotation o) {
// BugAnnotations must be
        if ( !(o instanceof edu.umd.cs.findbugs.LocalVariableAnnotation)) 
// Comparable with any type
// of BugAnnotation
        return this.getClass().getName().compareTo(o.getClass().getName());
        return name.compareTo(((edu.umd.cs.findbugs.LocalVariableAnnotation) (o) ).name);
    }
    public java.lang.String toString() {
        java.lang.String pattern = edu.umd.cs.findbugs.I18N.instance().getAnnotationDescription(description);
        edu.umd.cs.findbugs.FindBugsMessageFormat format = new edu.umd.cs.findbugs.FindBugsMessageFormat(pattern);
        return format.format(new edu.umd.cs.findbugs.BugAnnotation[]{this},null);
    }
/*
     * ----------------------------------------------------------------------
     * XML Conversion support
     * ----------------------------------------------------------------------
     */
    final private static java.lang.String ELEMENT_NAME = "LocalVariable";
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput) throws java.io.IOException {
        this.writeXML(xmlOutput,false,false);
    }
    public void writeXML(edu.umd.cs.findbugs.xml.XMLOutput xmlOutput, boolean addMessages, boolean isPrimary) throws java.io.IOException {
        edu.umd.cs.findbugs.xml.XMLAttributeList attributeList = new edu.umd.cs.findbugs.xml.XMLAttributeList().addAttribute("name",name).addAttribute("register",java.lang.String.valueOf(register)).addAttribute("pc",java.lang.String.valueOf(pc));
        java.lang.String role = this.getDescription();
        if ( !role.equals(DEFAULT_ROLE)) attributeList.addAttribute("role",role);
        edu.umd.cs.findbugs.BugAnnotationUtil.writeXML(xmlOutput,ELEMENT_NAME,this,attributeList,addMessages);
    }
    public boolean isNamed() {
        return register >= 0 &&  !name.equals("?");
    }
/**
     * @return name of local variable
     */
    public java.lang.String getName() {
        return name;
    }
    public int getPC() {
        return pc;
    }
    public int getRegister() {
        return register;
    }
    public boolean isSignificant() {
        return  !name.equals("?");
    }
/**
     * @param method
     * @param item
     * @param pc2
     * @return
     */
    public static edu.umd.cs.findbugs.LocalVariableAnnotation getLocalVariableAnnotation(org.apache.bcel.classfile.Method method, edu.umd.cs.findbugs.OpcodeStack.Item item, int pc) {
        int reg = item.getRegisterNumber();
        if (reg < 0) return null;
        return getLocalVariableAnnotation(method,reg,pc,item.getPC());
    }
    public static edu.umd.cs.findbugs.LocalVariableAnnotation getLocalVariableAnnotation(edu.umd.cs.findbugs.visitclass.DismantleBytecode visitor, edu.umd.cs.findbugs.OpcodeStack.Item item) {
        int reg = item.getRegisterNumber();
        if (reg < 0) return null;
        return getLocalVariableAnnotation(visitor.getMethod(),reg,visitor.getPC(),item.getPC());
    }
    public static edu.umd.cs.findbugs.LocalVariableAnnotation findMatchingIgnoredParameter(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method, java.lang.String name, java.lang.String signature) {
        try {
            edu.umd.cs.findbugs.ba.Dataflow<java.util.BitSet, edu.umd.cs.findbugs.ba.LiveLocalStoreAnalysis> llsaDataflow = classContext.getLiveLocalStoreDataflow(method);
            edu.umd.cs.findbugs.ba.CFG cfg;
            cfg = classContext.getCFG(method);
            edu.umd.cs.findbugs.LocalVariableAnnotation match = null;
            int lowestCost = java.lang.Integer.MAX_VALUE;
            java.util.BitSet liveStoreSetAtEntry = llsaDataflow.getAnalysis().getResultFact(cfg.getEntry());
            int localsThatAreParameters = edu.umd.cs.findbugs.visitclass.PreorderVisitor.getNumberArguments(method.getSignature());
            int startIndex = 0;
            if ( !method.isStatic()) startIndex = 1;
            edu.umd.cs.findbugs.ba.SignatureParser parser = new edu.umd.cs.findbugs.ba.SignatureParser(method.getSignature());
            java.util.Iterator<java.lang.String> signatureIterator = parser.parameterSignatureIterator();
            for (int i = startIndex; i < localsThatAreParameters + startIndex; i++) {
                java.lang.String sig = signatureIterator.next();
                if ( !liveStoreSetAtEntry.get(i) && signature.equals(sig)) {
                    edu.umd.cs.findbugs.LocalVariableAnnotation potentialMatch = edu.umd.cs.findbugs.LocalVariableAnnotation.getLocalVariableAnnotation(method,i,0,0);
                    potentialMatch.setDescription(DID_YOU_MEAN_ROLE);
                    if ( !potentialMatch.isNamed()) return potentialMatch;
                    int distance = edu.umd.cs.findbugs.util.EditDistance.editDistance(name,potentialMatch.getName());
                    if (distance < lowestCost) {
                        match = potentialMatch;
                        match.setDescription(DID_YOU_MEAN_ROLE);
                        lowestCost = distance;
                    }
                    else if (distance == lowestCost) {
                        match = null;
                    }
                }
            }
// parameter isn't live and signatures match
// not unique best match
            return match;
        }
        catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("",e);
        }
        catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("",e);
        }
        return null;
    }
    public static edu.umd.cs.findbugs.LocalVariableAnnotation findUniqueBestMatchingParameter(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method, java.lang.String name, java.lang.String signature) {
        edu.umd.cs.findbugs.LocalVariableAnnotation match = null;
        int localsThatAreParameters = edu.umd.cs.findbugs.visitclass.PreorderVisitor.getNumberArguments(method.getSignature());
        int startIndex = 0;
        if ( !method.isStatic()) startIndex = 1;
        edu.umd.cs.findbugs.ba.SignatureParser parser = new edu.umd.cs.findbugs.ba.SignatureParser(method.getSignature());
        java.util.Iterator<java.lang.String> signatureIterator = parser.parameterSignatureIterator();
        int lowestCost = java.lang.Integer.MAX_VALUE;
        for (int i = startIndex; i < localsThatAreParameters + startIndex; i++) {
            java.lang.String sig = signatureIterator.next();
            if (signature.equals(sig)) {
                edu.umd.cs.findbugs.LocalVariableAnnotation potentialMatch = edu.umd.cs.findbugs.LocalVariableAnnotation.getLocalVariableAnnotation(method,i,0,0);
                if ( !potentialMatch.isNamed()) continue;
                int distance = edu.umd.cs.findbugs.util.EditDistance.editDistance(name,potentialMatch.getName());
                if (distance < lowestCost) {
                    match = potentialMatch;
                    match.setDescription(DID_YOU_MEAN_ROLE);
                    lowestCost = distance;
                }
                else if (distance == lowestCost) {
                    match = null;
                }
            }
        }
// not unique best match
// signatures match
        if (lowestCost < 5) return match;
        return null;
    }
    public java.lang.String toString(edu.umd.cs.findbugs.ClassAnnotation primaryClass) {
        return this.toString();
    }
}
// vim:ts=4
