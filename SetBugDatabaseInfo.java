/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003-2005 William Pugh
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Java main application to compute update a historical bug collection with
 * results from another build/analysis.
 *
 * @author William Pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.DetectorFactoryCollection;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.PackageStats;
import edu.umd.cs.findbugs.Project;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.config.CommandLine;
import edu.umd.cs.findbugs.filter.Filter;
public class SetBugDatabaseInfo extends java.lang.Object {
/**
     *
     */
    static class SetInfoCommandLine extends edu.umd.cs.findbugs.config.CommandLine {
        java.lang.String revisionName;
        java.lang.String projectName;
        java.lang.String exclusionFilterFile;
        java.lang.String lastVersion;
        java.lang.String cloudId;
        java.util.HashMap<java.lang.String, java.lang.String> cloudProperties = new java.util.HashMap<java.lang.String, java.lang.String>();
        boolean withMessages = false;
        boolean purgeStats = false;
        boolean purgeClassStats = false;
        boolean purgeMissingClasses = false;
        boolean resetSource = false;
        boolean resetProject = false;
        boolean purgeDesignations = false;
        long revisionTimestamp = 0L;
        public java.util.List<java.lang.String> sourcePaths = new java.util.LinkedList<java.lang.String>();
        public java.util.List<java.lang.String> searchSourcePaths = new java.util.LinkedList<java.lang.String>();
        public SetInfoCommandLine() {
            super();
            this.addOption("-name","name","set name for (last) revision");
            this.addOption("-projectName","name","set name for project");
            this.addOption("-timestamp","when","set timestamp for (last) revision");
            this.addSwitch("-resetSource","remove all source search paths");
            this.addSwitch("-resetProject","remove all source search paths, analysis and auxilary classpath entries");
            this.addOption("-source","directory","Add this directory to the source search path");
            this.addSwitch("-purgeStats","purge/delete information about sizes of analyzed class files");
            this.addSwitch("-uploadDesignations","upload all designations to cloud");
            this.addSwitch("-purgeDesignations","purge/delete user designations (e.g., MUST_FIX or NOT_A_BUG");
            this.addSwitch("-purgeClassStats","purge/delete information about sizes of analyzed class files, but retain class stats");
            this.addSwitch("-purgeMissingClasses","purge list of missing classes");
            this.addOption("-findSource","directory","Find and add all relevant source directions contained within this directory");
            this.addOption("-suppress","filter file","Suppress warnings matched by this file (replaces previous suppressions)");
            this.addOption("-lastVersion","version","Trim the history to just include just the specified version");
            this.addSwitch("-withMessages","Add bug descriptions");
            this.addOption("-cloud","id","set cloud id");
            this.addOption("-cloudProperty","key=value","set cloud property");
        }
        protected void handleOption(java.lang.String option, java.lang.String optionExtraPart) throws java.io.IOException {
            if (option.equals("-withMessages")) withMessages = true;
            else if (option.equals("-resetSource")) resetSource = true;
            else if (option.equals("-resetProject")) resetProject = true;
            else if (option.equals("-purgeStats")) purgeStats = true;
            else if (option.equals("-purgeDesignations")) purgeDesignations = true;
            else if (option.equals("-purgeClassStats")) purgeClassStats = true;
            else if (option.equals("-purgeMissingClasses")) purgeMissingClasses = true;
            else throw new java.lang.IllegalArgumentException("no option " + option);
        }
        protected void handleOptionWithArgument(java.lang.String option, java.lang.String argument) throws java.io.IOException {
            if (option.equals("-name")) revisionName = argument;
            else if (option.equals("-cloud")) cloudId = argument;
            else if (option.equals("-cloudProperty")) {
                int e = argument.indexOf('\u003d');
                if (e ==  -1) throw new java.lang.IllegalArgumentException("Bad cloud property: " + argument);
                java.lang.String key = argument.substring(0,e);
                java.lang.String value = argument.substring(e + 1);
                cloudProperties.put(key,value);
            }
            else if (option.equals("-projectName")) projectName = argument;
            else if (option.equals("-suppress")) exclusionFilterFile = argument;
            else if (option.equals("-timestamp")) revisionTimestamp = java.util.Date.parse(argument);
            else if (option.equals("-source")) sourcePaths.add(argument);
            else if (option.equals("-lastVersion")) {
                lastVersion = argument;
            }
            else if (option.equals("-findSource")) searchSourcePaths.add(argument);
            else throw new java.lang.IllegalArgumentException("Can't handle option " + option);
        }
    }
    public SetBugDatabaseInfo() {
    }
    final private static java.lang.String USAGE = "Usage: <cmd>  [options] [<oldData> [<newData>]]";
    public static void main(java.lang.String[] args) throws org.dom4j.DocumentException, java.io.IOException {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        edu.umd.cs.findbugs.workflow.SetBugDatabaseInfo.SetInfoCommandLine commandLine = new edu.umd.cs.findbugs.workflow.SetBugDatabaseInfo.SetInfoCommandLine();
        int argCount = commandLine.parse(args,0,2,USAGE);
        edu.umd.cs.findbugs.SortedBugCollection origCollection = new edu.umd.cs.findbugs.SortedBugCollection();
        if (argCount < args.length) origCollection.readXML(args[argCount++]);
        else origCollection.readXML(java.lang.System.in);
        edu.umd.cs.findbugs.Project project = origCollection.getProject();
        if (commandLine.revisionName != null) origCollection.setReleaseName(commandLine.revisionName);
        if (commandLine.projectName != null) origCollection.getProject().setProjectName(commandLine.projectName);
        if (commandLine.revisionTimestamp != 0) origCollection.setTimestamp(commandLine.revisionTimestamp);
        origCollection.setWithMessages(commandLine.withMessages);
        if (commandLine.purgeDesignations) for (edu.umd.cs.findbugs.BugInstance b : origCollection){
            b.setUserDesignation(null);
        }
;
        if (commandLine.exclusionFilterFile != null) {
            project.setSuppressionFilter(edu.umd.cs.findbugs.filter.Filter.parseFilter(commandLine.exclusionFilterFile));
        }
        if (commandLine.resetProject) {
            project.getSourceDirList().clear();
            project.getFileList().clear();
            project.getAuxClasspathEntryList().clear();
        }
        boolean reinitializeCloud = false;
        if (commandLine.cloudId != null) {
            project.setCloudId(commandLine.cloudId);
            reinitializeCloud = true;
        }
        for (java.util.Map.Entry<java.lang.String, java.lang.String> e : commandLine.cloudProperties.entrySet()){
            project.getCloudProperties().setProperty(e.getKey(),e.getValue());
            reinitializeCloud = true;
        }
;
        if (commandLine.resetSource) project.getSourceDirList().clear();
        for (java.lang.String source : commandLine.sourcePaths)project.addSourceDir(source);
;
        if (commandLine.purgeStats) origCollection.getProjectStats().getPackageStats().clear();
        if (commandLine.purgeClassStats) for (edu.umd.cs.findbugs.PackageStats ps : origCollection.getProjectStats().getPackageStats()){
            ps.getClassStats().clear();
        }
;
        if (commandLine.purgeMissingClasses) origCollection.clearMissingClasses();
        if (commandLine.lastVersion != null) {
            long last = edu.umd.cs.findbugs.workflow.Filter.FilterCommandLine.getVersionNum(origCollection,commandLine.lastVersion,true);
            if (last < origCollection.getSequenceNumber()) {
                java.lang.String name = origCollection.getAppVersionFromSequenceNumber(last).getReleaseName();
                long timestamp = origCollection.getAppVersionFromSequenceNumber(last).getTimestamp();
                origCollection.setReleaseName(name);
                origCollection.setTimestamp(timestamp);
                origCollection.trimAppVersions(last);
            }
        }
        java.util.Map<java.lang.String, java.util.Set<java.lang.String>> missingFiles = new java.util.HashMap<java.lang.String, java.util.Set<java.lang.String>>();
        if ( !commandLine.searchSourcePaths.isEmpty()) {
            sourceSearcher = new edu.umd.cs.findbugs.workflow.SourceSearcher(project);
            for (edu.umd.cs.findbugs.BugInstance bug : origCollection.getCollection()){
                edu.umd.cs.findbugs.SourceLineAnnotation src = bug.getPrimarySourceLineAnnotation();
                if ( !sourceSearcher.sourceNotFound.contains(src.getClassName()) &&  !sourceSearcher.findSource(src)) {
                    java.util.Set<java.lang.String> paths = missingFiles.get(src.getSourceFile());
                    if (paths == null) {
                        paths = new java.util.HashSet<java.lang.String>();
                        missingFiles.put(src.getSourceFile(),paths);
                    }
                    java.lang.String fullPath = fullPath(src);
                    paths.add(fullPath);
                }
            }
;
            java.util.Set<java.lang.String> foundPaths = new java.util.HashSet<java.lang.String>();
            for (java.lang.String f : commandLine.searchSourcePaths)for (java.io.File javaFile : edu.umd.cs.findbugs.workflow.RecursiveSearchForJavaFiles.search(new java.io.File(f))){
                java.util.Set<java.lang.String> matchingMissingClasses = missingFiles.get(javaFile.getName());
                if (matchingMissingClasses == null) {
                }
                else for (java.lang.String sourcePath : matchingMissingClasses){
                    java.lang.String path = javaFile.getAbsolutePath();
                    if (path.endsWith(sourcePath)) {
                        java.lang.String dir = path.substring(0,path.length() - sourcePath.length());
                        foundPaths.add(dir);
                    }
                }
;
            }
;
;
            java.util.Set<java.lang.String> toRemove = new java.util.HashSet<java.lang.String>();
            for (java.lang.String p1 : foundPaths)for (java.lang.String p2 : foundPaths)if ( !p1.equals(p2) && p1.startsWith(p2)) {
                toRemove.add(p1);
                break;
            }
;
;
            foundPaths.removeAll(toRemove);
            for (java.lang.String dir : foundPaths){
                project.addSourceDir(dir);
                if (argCount < args.length) java.lang.System.out.println("Found " + dir);
            }
;
        }
        if (reinitializeCloud) origCollection.clearCloud();
// OK, now we know all the missing source files
// we also know all the .java files in the directories we were pointed
// to
        if (argCount < args.length) origCollection.writeXML(args[argCount++]);
        else origCollection.writeXML(java.lang.System.out);
    }
    static java.lang.String fullPath(edu.umd.cs.findbugs.SourceLineAnnotation src) {
        return src.getPackageName().replace('\u002e',java.io.File.separatorChar) + java.io.File.separatorChar + src.getSourceFile();
    }
    static edu.umd.cs.findbugs.workflow.SourceSearcher sourceSearcher;
}
