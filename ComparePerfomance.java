/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.tools;
import edu.umd.cs.findbugs.tools.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import edu.umd.cs.findbugs.xml.XMLUtil;
public class ComparePerfomance extends java.lang.Object {
    final int num;
    final java.util.Map<java.lang.String, int[]> performance = new java.util.TreeMap<java.lang.String, int[]>();
    public ComparePerfomance(java.lang.String[] args) throws java.io.IOException, org.dom4j.DocumentException {
        super();
        num = args.length;
        for (int i = 0; i < args.length; i++) {
            this.foo(new java.io.File(args[i]),i);
        }
    }
    public int[] getRecord(java.lang.String className) {
        int[] result = performance.get(className);
        if (result != null) return result;
        result = new int[num];
        performance.put(className,result);
        return result;
    }
    public void foo(java.io.File f, int i) throws java.io.IOException, org.dom4j.DocumentException {
        org.dom4j.Document doc;
        org.dom4j.io.SAXReader reader = new org.dom4j.io.SAXReader();
        java.lang.String fName = f.getName();
        java.io.InputStream in = new java.io.FileInputStream(f);
        if (fName.endsWith(".gz")) in = new java.util.zip.GZIPInputStream(in);
        doc = reader.read(in);
        org.dom4j.Node summary = doc.selectSingleNode("/BugCollection/FindBugsSummary");
        double cpu_seconds = java.lang.Double.parseDouble(summary.valueOf("@cpu_seconds"));
        this.putStats("cpu_seconds",i,(int) ((cpu_seconds * 1000)) );
        double gc_seconds = java.lang.Double.parseDouble(summary.valueOf("@gc_seconds"));
        this.putStats("gc_seconds",i,(int) ((gc_seconds * 1000)) );
        java.util.List<org.dom4j.Node> profileNodes = edu.umd.cs.findbugs.xml.XMLUtil.selectNodes(doc,"/BugCollection/FindBugsSummary/FindBugsProfile/ClassProfile");
        for (org.dom4j.Node n : profileNodes){
            java.lang.String name = n.valueOf("@name");
            int totalMilliseconds = java.lang.Integer.parseInt(n.valueOf("@totalMilliseconds"));
            int invocations = java.lang.Integer.parseInt(n.valueOf("@invocations"));
            this.putStats(name,i,totalMilliseconds);
        }
;
        in.close();
    }
/**
     * @param name
     * @param i
     * @param totalMilliseconds
     */
    public void putStats(java.lang.String name, int i, int totalMilliseconds) {
        int[] stats = this.getRecord(name);
        stats[i] = totalMilliseconds;
    }
    public void print() {
        for (java.util.Map.Entry<java.lang.String, int[]> e : performance.entrySet()){
            java.lang.String name = e.getKey();
            int lastDot = name.lastIndexOf('\u002e');
            java.lang.String simpleName = name.substring(lastDot + 1);
            java.lang.System.out.printf("%s,%s",name,simpleName);
            for (int x : e.getValue())java.lang.System.out.printf(",%d",x);
;
            java.lang.System.out.println();
        }
;
    }
    public static void main(java.lang.String[] args) throws java.lang.Exception {
        edu.umd.cs.findbugs.tools.ComparePerfomance p = new edu.umd.cs.findbugs.tools.ComparePerfomance(args);
        p.print();
    }
}
