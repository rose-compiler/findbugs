package edu.umd.cs.findbugs.cloud;
import edu.umd.cs.findbugs.cloud.*;
import java.io.IOException;
import java.net.URL;
import edu.umd.cs.findbugs.BugInstance;
abstract public interface BugFiler {
    abstract java.lang.String getBugStatus(java.lang.String bugUrl) throws java.lang.Exception;
    abstract java.net.URL file(edu.umd.cs.findbugs.BugInstance b) throws edu.umd.cs.findbugs.cloud.SignInCancelledException, java.io.IOException;
    abstract boolean ready();
}
