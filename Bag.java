/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Simple implementation of a Bag
 * 
 * @author pugh
 */
package edu.umd.cs.findbugs.util;
import edu.umd.cs.findbugs.util.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
public class Bag<E> extends java.lang.Object {
    final java.util.Map<E, java.lang.Integer> map;
    public Bag() {
        super();
        map = new java.util.HashMap<E, java.lang.Integer>();
    }
    public Bag(java.util.Map<E, java.lang.Integer> map) {
        super();
        this.map = map;
    }
    public boolean add(E e) {
        java.lang.Integer v = map.get(e);
        if (v == null) map.put(e,1);
        else map.put(e,v + 1);
        return true;
    }
    public boolean add(E e, int count) {
        java.lang.Integer v = map.get(e);
        if (v == null) map.put(e,count);
        else map.put(e,v + count);
        return true;
    }
    public java.util.Set<E> keySet() {
        return map.keySet();
    }
    public java.util.Collection<java.util.Map.Entry<E, java.lang.Integer>> entrySet() {
        return map.entrySet();
    }
    public int getCount(E e) {
        java.lang.Integer v = map.get(e);
        if (v == null) return 0;
        else return v;
    }
}
