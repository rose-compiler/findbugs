/*
 * Contributions to FindBugs
 * Copyright (C) 2009, Tom\u00e1s Pollak
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * This test runs a FindBugs analysis on the findbugsTestCases project and
 * checks if there are any unexpected bugs.
 *
 * The results are checked for the unexpected bugs of type
 * FB_MISSING_EXPECTED_WARNING or FB_UNEXPECTED_WARNING.
 *
 * @see ExpectWarning
 * @see NoWarning
 * @author Tom\u00e1s Pollak
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import static org.junit.Assert.assertFalse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import edu.umd.cs.findbugs.annotations.ExpectWarning;
import edu.umd.cs.findbugs.annotations.NoWarning;
import edu.umd.cs.findbugs.config.UserPreferences;
public class DetectorsTest extends java.lang.Object {
    public DetectorsTest() {
    }
/**
     *
     */
    final private static java.lang.String FB_UNEXPECTED_WARNING = "FB_UNEXPECTED_WARNING";
/**
     *
     */
    final private static java.lang.String FB_MISSING_EXPECTED_WARNING = "FB_MISSING_EXPECTED_WARNING";
    private edu.umd.cs.findbugs.BugCollectionBugReporter bugReporter;
    private edu.umd.cs.findbugs.IFindBugsEngine engine;
    public void setUp() throws java.lang.Exception {
        this.loadFindbugsPlugin();
    }
    public void testAllRegressionFiles() throws java.lang.InterruptedException, java.io.IOException {
        this.setUpEngine("../findbugsTestCases/build/classes/");
        engine.execute();
        assertFalse("No bugs were reported. Something is wrong with the configuration",bugReporter.getBugCollection().getCollection().isEmpty());
    }
// If there are zero bugs, then something's wrong
    public void testBug3053867() throws java.lang.InterruptedException, java.io.IOException {
        this.setUpEngine("../findbugsTestCases/build/classes/sfBugs/Bug3053867.class","../findbugsTestCases/build/classes/sfBugs/Bug3053867$Foo.class");
        engine.execute();
        assertFalse("No bugs were reported. Something is wrong with the configuration",bugReporter.getBugCollection().getCollection().isEmpty());
    }
// If there are zero bugs, then something's wrong
    public void checkForUnexpectedBugs() {
        java.util.List<edu.umd.cs.findbugs.BugInstance> unexpectedBugs = new java.util.ArrayList<edu.umd.cs.findbugs.BugInstance>();
        for (edu.umd.cs.findbugs.BugInstance bug : bugReporter.getBugCollection()){
            if (this.isUnexpectedBug(bug) && bug.getPriority() == edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY) {
                unexpectedBugs.add(bug);
                java.lang.System.out.println(bug.getMessageWithPriorityTypeAbbreviation());
                java.lang.System.out.println("  " + bug.getPrimarySourceLineAnnotation());
            }
        }
;
        if ( !unexpectedBugs.isEmpty()) org.junit.Assert.fail("Unexpected bugs (" + unexpectedBugs.size() + "):" + this.getBugsLocations(unexpectedBugs));
    }
/**
     * Returns a printable String concatenating bug locations.
     */
    private java.lang.String getBugsLocations(java.util.List<edu.umd.cs.findbugs.BugInstance> unexpectedBugs) {
        java.lang.StringBuilder message = new java.lang.StringBuilder();
        for (edu.umd.cs.findbugs.BugInstance bugInstance : unexpectedBugs){
            message.append("\n");
            if (bugInstance.getBugPattern().getType().equals(FB_MISSING_EXPECTED_WARNING)) message.append("missing ");
            else message.append("unexpected ");
            edu.umd.cs.findbugs.StringAnnotation pattern = (edu.umd.cs.findbugs.StringAnnotation) (bugInstance.getAnnotations().get(2)) ;
            message.append(pattern.getValue());
            message.append(" ");
            message.append(bugInstance.getPrimarySourceLineAnnotation());
        }
;
        return message.toString();
    }
/**
     * Returns if a bug instance is unexpected for this test.
     */
    private boolean isUnexpectedBug(edu.umd.cs.findbugs.BugInstance bug) {
        return FB_MISSING_EXPECTED_WARNING.equals(bug.getType()) || FB_UNEXPECTED_WARNING.equals(bug.getType());
    }
/**
     * Loads the default detectors from findbugs.jar, to isolate the test from
     * others that use fake plugins.
     */
    private void loadFindbugsPlugin() {
        edu.umd.cs.findbugs.DetectorFactoryCollection dfc = new edu.umd.cs.findbugs.DetectorFactoryCollection();
        edu.umd.cs.findbugs.DetectorFactoryCollection.resetInstance(dfc);
    }
/**
     * Sets up a FB engine to run on the 'findbugsTestCases' project. It enables
     * all the available detectors and reports all the bug categories. Uses a
     * normal priority threshold.
     */
    private void setUpEngine(java.lang.String[] analyzeMe) {
        this.engine = new edu.umd.cs.findbugs.FindBugs2();
        edu.umd.cs.findbugs.Project project = new edu.umd.cs.findbugs.Project();
        project.setProjectName("findbugsTestCases");
        this.engine.setProject(project);
        edu.umd.cs.findbugs.DetectorFactoryCollection detectorFactoryCollection = edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
        engine.setDetectorFactoryCollection(detectorFactoryCollection);
        bugReporter = new edu.umd.cs.findbugs.BugCollectionBugReporter(project);
        bugReporter.setPriorityThreshold(edu.umd.cs.findbugs.Priorities.LOW_PRIORITY);
        engine.setBugReporter(this.bugReporter);
        edu.umd.cs.findbugs.config.UserPreferences preferences = edu.umd.cs.findbugs.config.UserPreferences.createDefaultUserPreferences();
        edu.umd.cs.findbugs.DetectorFactory checkExpectedWarnings = detectorFactoryCollection.getFactory("CheckExpectedWarnings");
        preferences.enableDetector(checkExpectedWarnings,true);
        preferences.getFilterSettings().clearAllCategories();
        this.engine.setUserPreferences(preferences);
// This is ugly. We should think how to improve this.
        for (java.lang.String s : analyzeMe)project.addFile(s);
;
        project.addAuxClasspathEntry("lib/junit.jar");
        java.io.File lib = new java.io.File("../findbugsTestCases/lib");
        for (java.io.File f : lib.listFiles()){
            java.lang.String path = f.getPath();
            if (path.endsWith(".jar")) project.addAuxClasspathEntry(path);
        }
;
    }
}
