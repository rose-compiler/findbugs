/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2006 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
/**
 * FindBugsViewerTask.java -- Ant Task to launch the FindBugsFrame
 * 
 * To use, create a new task that refrences the ant task (such as
 * "findbugs-viewer"). Then call this task while passing in parameters to modify
 * it's behaviour. It supports several options that are the same as the findbugs
 * task:
 * 
 * -projectFile -debug -jvmargs -home -classpath -pluginList -timeout
 * 
 * It also adds some new options:
 * 
 * -look: string name representing look and feel. Can be "native", "plastic" or
 * "gtk" -loadbugs: file name of bug report to load
 * 
 * The below is an example of how this could be done in an ant script:
 * 
 * <taskdef name="findbugs" classname="edu.umd.cs.findbugs.anttask.FindBugsTask"
 * classpath="C:\dev\cvs.sourceforge.net\findbugs\lib\findbugs-ant.jar" />
 * <taskdef name="findbugs-viewer"
 * classname="edu.umd.cs.findbugs.anttask.FindBugsViewerTask"
 * classpath="C:\dev\cvs.sourceforge.net\findbugs\lib\findbugs-ant.jar" />
 * 
 * <property name="findbugs.home" location="C:\dev\cvs.sourceforge.net\findbugs"
 * /> <property name="findbugs.bugReport" location="bcel-fb.xml" />
 * 
 * <target name="findbugs-viewer" depends="jar"> <findbugs-viewer
 * home="${findbugs.home}" look="native" loadbugs="${findbugs.bugReport}"/>
 * </target>
 * 
 * Created on March 21, 2006, 12:57 PM
 * 
 * @author Mark McKay, mark@kitfox.com
 */
package edu.umd.cs.findbugs.anttask;
import edu.umd.cs.findbugs.anttask.*;
import java.io.File;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Ant.Reference;
import org.apache.tools.ant.taskdefs.Java;
import org.apache.tools.ant.types.Path;
import edu.umd.cs.findbugs.ExitCodes;
public class FindBugsViewerTask extends org.apache.tools.ant.Task {
// ten minutes
    final private static long DEFAULT_TIMEOUT =  -1;
// location to load bug report from
    private boolean debug = false;
    private java.io.File projectFile = null;
    private java.io.File loadbugs = null;
    private long timeout = DEFAULT_TIMEOUT;
    private java.lang.String jvmargs = "";
    private java.lang.String look = "native";
    private java.io.File homeDir = null;
    private org.apache.tools.ant.types.Path classpath = null;
    private org.apache.tools.ant.types.Path pluginList = null;
    private org.apache.tools.ant.taskdefs.Java findbugsEngine = null;
/** Creates a new instance of FindBugsViewerTask */
    public FindBugsViewerTask() {
        super();
    }
/**
     * Sets the file that contains the XML output of a findbugs report.
     * 
     * @param bugReport
     *            XML output from a findbugs session
     */
    public void setLoadbugs(java.io.File loadbugs) {
        this.loadbugs = loadbugs;
    }
/**
     * Set the project file
     */
    public void setProjectFile(java.io.File projectFile) {
        this.projectFile = projectFile;
    }
/**
     * Set the debug flag
     */
    public void setDebug(boolean flag) {
        this.debug = flag;
    }
/**
     * Set any specific jvm args
     */
    public void setJvmargs(java.lang.String args) {
        this.jvmargs = args;
    }
/**
     * Set look. One of "native", "gtk" or "plastic"
     */
    public void setLook(java.lang.String look) {
        this.look = look;
    }
/**
     * Set the home directory into which findbugs was installed
     */
    public void setHome(java.io.File homeDir) {
        this.homeDir = homeDir;
    }
/**
     * Path to use for classpath.
     */
    public org.apache.tools.ant.types.Path createClasspath() {
        if (classpath == null) {
            classpath = new org.apache.tools.ant.types.Path(this.getProject());
        }
        return classpath.createPath();
    }
/**
     * Adds a reference to a classpath defined elsewhere.
     */
    public void setClasspathRef(org.apache.tools.ant.taskdefs.Ant.Reference r) {
        this.createClasspath().setRefid(r);
    }
/**
     * the plugin list to use.
     */
    public void setPluginList(org.apache.tools.ant.types.Path src) {
        if (pluginList == null) {
            pluginList = src;
        }
        else {
            pluginList.append(src);
        }
    }
/**
     * Path to use for plugin list.
     */
    public org.apache.tools.ant.types.Path createPluginList() {
        if (pluginList == null) {
            pluginList = new org.apache.tools.ant.types.Path(this.getProject());
        }
        return pluginList.createPath();
    }
/**
     * Adds a reference to a plugin list defined elsewhere.
     */
    public void setPluginListRef(org.apache.tools.ant.taskdefs.Ant.Reference r) {
        this.createPluginList().setRefid(r);
    }
/**
     * Set timeout in milliseconds.
     * 
     * @param timeout
     *            the timeout
     */
    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
/**
     * Add an argument to the JVM used to execute FindBugs.
     * 
     * @param arg
     *            the argument
     */
    private void addArg(java.lang.String arg) {
        findbugsEngine.createArg().setValue(arg);
    }
    public void execute() throws org.apache.tools.ant.BuildException {
        findbugsEngine = (org.apache.tools.ant.taskdefs.Java) (this.getProject().createTask("java")) ;
        findbugsEngine.setTaskName(this.getTaskName());
        findbugsEngine.setFork(true);
        if (timeout > 0) {
            findbugsEngine.setTimeout(timeout);
        }
        if (debug) {
            jvmargs = jvmargs + " -Dfindbugs.debug=true";
        }
        findbugsEngine.createJvmarg().setLine(jvmargs);
        if (homeDir != null) {
// Use findbugs.home to locate findbugs.jar and the standard
// plugins. This is the usual means of initialization.
            java.io.File findbugsLib = new java.io.File(homeDir, "lib");
            java.io.File findbugsLibFindBugs = new java.io.File(findbugsLib, "findbugs.jar");
            java.io.File findBugsFindBugs = new java.io.File(homeDir, "findbugs.jar");
// log("executing using home dir [" + homeDir + "]");
            if (findbugsLibFindBugs.exists()) findbugsEngine.setClasspath(new org.apache.tools.ant.types.Path(this.getProject(), findbugsLibFindBugs.getPath()));
            else if (findBugsFindBugs.exists()) findbugsEngine.setClasspath(new org.apache.tools.ant.types.Path(this.getProject(), findBugsFindBugs.getPath()));
            else throw new java.lang.IllegalArgumentException("Can't find findbugs.jar in " + homeDir);
            findbugsEngine.setClassname("edu.umd.cs.findbugs.LaunchAppropriateUI");
            findbugsEngine.createJvmarg().setValue("-Dfindbugs.home=" + homeDir.getPath());
        }
        else {
            findbugsEngine.setClasspath(classpath);
            findbugsEngine.setClassname("edu.umd.cs.findbugs.LaunchAppropriateUI");
            this.addArg("-pluginList");
            this.addArg(pluginList.toString());
        }
        if (projectFile != null) {
            this.addArg("-project");
            this.addArg(projectFile.getPath());
        }
        if (loadbugs != null) {
            this.addArg("-loadbugs");
            this.addArg(loadbugs.getPath());
        }
        if (look != null) {
            this.addArg("-look:" + look);
        }
        this.log("Launching FindBugs Viewer...");
// findbugsEngine.setClassname("edu.umd.cs.findbugs.gui.FindBugsFrame");
        int rc = findbugsEngine.executeJava();
        if ((rc & edu.umd.cs.findbugs.ExitCodes.ERROR_FLAG) != 0) {
            throw new org.apache.tools.ant.BuildException("Execution of findbugs failed.");
        }
        if ((rc & edu.umd.cs.findbugs.ExitCodes.MISSING_CLASS_FLAG) != 0) {
            this.log("Classes needed for analysis were missing");
        }
    }
}
