/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author bill.pugh
 */
package edu.umd.cs.findbugs.workflow;
import edu.umd.cs.findbugs.workflow.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Date;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugRankCategory;
import edu.umd.cs.findbugs.FindBugs;
import edu.umd.cs.findbugs.ProjectStats;
import edu.umd.cs.findbugs.SortedBugCollection;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.charsets.UserTextFile;
import edu.umd.cs.findbugs.cloud.Cloud;
import edu.umd.cs.findbugs.config.CommandLine;
public class CloudSyncAndReport extends java.lang.Object {
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.config.CommandLine#handleOption(java.lang.String,
         * java.lang.String)
         */
/*
         * (non-Javadoc)
         *
         * @see
         * edu.umd.cs.findbugs.config.CommandLine#handleOptionWithArgument(java
         * .lang.String, java.lang.String)
         */
/**
     * @param options
     */
    static class Stats extends java.lang.Object {
        public Stats() {
        }
        int total;
        int recent;
    }
    static class CSRCommandLine extends edu.umd.cs.findbugs.config.CommandLine {
        final edu.umd.cs.findbugs.workflow.CloudSyncAndReport.CSPoptions options;
        public CSRCommandLine(edu.umd.cs.findbugs.workflow.CloudSyncAndReport.CSPoptions options) {
            super();
            this.options = options;
            this.addOption("-cloud","id","id of the cloud to use");
            this.addOption("-recent","hours","maximum age in hours for an issue to be recent");
            this.addOption("-cloudSummary","file","write a cloud summary to thie file");
        }
        protected void handleOption(java.lang.String option, java.lang.String optionExtraPart) throws java.io.IOException {
            throw new java.lang.IllegalArgumentException("Unknown option : " + option);
        }
        protected void handleOptionWithArgument(java.lang.String option, java.lang.String argument) throws java.io.IOException {
            if (option.equals("-cloud")) {
                options.cloudId = argument;
            }
            else if (option.equals("-recent")) {
                options.ageInHours = java.lang.Integer.parseInt(argument);
            }
            else if (option.equals("-cloudSummary")) {
                options.cloudSummary = argument;
            }
            else throw new java.lang.IllegalArgumentException("Unknown option : " + option);
        }
    }
    public static class CSPoptions extends java.lang.Object {
        public CSPoptions() {
        }
        public java.lang.String analysisFile;
        public java.lang.String cloudSummary;
        public java.lang.String cloudId;
        public int ageInHours = 22;
    }
    public static void main(java.lang.String[] argv) throws java.lang.Exception {
        edu.umd.cs.findbugs.FindBugs.setNoAnalysis();
        final edu.umd.cs.findbugs.workflow.CloudSyncAndReport.CSPoptions options = new edu.umd.cs.findbugs.workflow.CloudSyncAndReport.CSPoptions();
        final edu.umd.cs.findbugs.workflow.CloudSyncAndReport.CSRCommandLine commandLine = new edu.umd.cs.findbugs.workflow.CloudSyncAndReport.CSRCommandLine(options);
        int argCount = commandLine.parse(argv,0,1,"Usage: " + edu.umd.cs.findbugs.workflow.CloudSyncAndReport.class.getName() + " [options] [<results1> <results2> ... <resultsn>] ");
        if (argCount < argv.length) options.analysisFile = argv[argCount];
        edu.umd.cs.findbugs.workflow.CloudSyncAndReport csr = new edu.umd.cs.findbugs.workflow.CloudSyncAndReport(options);
        csr.load();
        csr.sync();
        java.io.PrintWriter out = edu.umd.cs.findbugs.charsets.UTF8.printWriter(java.lang.System.out);
        csr.report(out);
        out.flush();
        csr.shutdown();
        out.close();
    }
    final edu.umd.cs.findbugs.workflow.CloudSyncAndReport.CSPoptions options;
    final edu.umd.cs.findbugs.SortedBugCollection bugCollection = new edu.umd.cs.findbugs.SortedBugCollection();
    public CloudSyncAndReport(edu.umd.cs.findbugs.workflow.CloudSyncAndReport.CSPoptions options) {
        super();
        this.options = options;
    }
    public void load() throws org.dom4j.DocumentException, java.io.IOException {
        if (options.analysisFile == null) bugCollection.readXML(edu.umd.cs.findbugs.charsets.UTF8.bufferedReader(java.lang.System.in));
        else bugCollection.readXML(options.analysisFile);
        if (options.cloudId != null &&  !options.cloudId.equals(bugCollection.getProject().getCloudId())) {
            bugCollection.getProject().setCloudId(options.cloudId);
            bugCollection.reinitializeCloud();
        }
    }
    public void sync() {
        edu.umd.cs.findbugs.cloud.Cloud cloud = bugCollection.getCloud();
        cloud.initiateCommunication();
        cloud.waitUntilIssueDataDownloaded();
    }
    public void report(java.io.PrintWriter out) {
        java.util.TreeMap<edu.umd.cs.findbugs.BugRankCategory, edu.umd.cs.findbugs.workflow.CloudSyncAndReport.Stats> stats = new java.util.TreeMap<edu.umd.cs.findbugs.BugRankCategory, edu.umd.cs.findbugs.workflow.CloudSyncAndReport.Stats>();
        edu.umd.cs.findbugs.ProjectStats projectStats = bugCollection.getProjectStats();
        java.util.Collection<edu.umd.cs.findbugs.BugInstance> bugs = bugCollection.getCollection();
        edu.umd.cs.findbugs.cloud.Cloud cloud = bugCollection.getCloud();
        cloud.setMode(edu.umd.cs.findbugs.cloud.Cloud.Mode.COMMUNAL);
        out.printf("Cloud sync and summary report for %s%n",bugCollection.getProject().getProjectName());
        out.printf("Code dated %s%n",new java.util.Date(bugCollection.getTimestamp()));
        out.printf("Code analyzed %s%n",new java.util.Date(bugCollection.getAnalysisTimestamp()));
        out.printf("%7d total classes%n",projectStats.getNumClasses());
        out.printf("%7d total issues%n",bugs.size());
        long recentTimestamp = java.lang.System.currentTimeMillis() - options.ageInHours * 3600 * 1000L;
        int allRecentIssues = 0;
        for (edu.umd.cs.findbugs.BugInstance b : bugs){
            edu.umd.cs.findbugs.workflow.CloudSyncAndReport.Stats s = stats.get(edu.umd.cs.findbugs.BugRankCategory.getRank(b.getBugRank()));
            if (s == null) {
                s = new edu.umd.cs.findbugs.workflow.CloudSyncAndReport.Stats();
                stats.put(edu.umd.cs.findbugs.BugRankCategory.getRank(b.getBugRank()),s);
            }
            s.total++;
            long firstSeen = cloud.getFirstSeen(b);
            if (firstSeen > recentTimestamp) {
                s.recent++;
                allRecentIssues++;
            }
        }
;
        out.printf("%7d recent issues%n",allRecentIssues);
        if (options.cloudSummary != null && cloud.supportsCloudSummaries()) {
            try {
                java.io.PrintWriter cs = edu.umd.cs.findbugs.charsets.UserTextFile.printWriter(options.cloudSummary);
                cs.printf("%6s %6s %s%n","recent","total","Rank category");
                for (java.util.Map.Entry<edu.umd.cs.findbugs.BugRankCategory, edu.umd.cs.findbugs.workflow.CloudSyncAndReport.Stats> e : stats.entrySet()){
                    edu.umd.cs.findbugs.workflow.CloudSyncAndReport.Stats s = e.getValue();
                    if (s.total > 0) cs.printf("%6d %6d %s%n",s.recent,s.total,e.getKey());
                }
;
                cs.println();
                cloud.printCloudSummary(cs,bugs,null);
                cs.close();
            }
            catch (java.lang.Exception e){
                out.println("Error writing cloud summary to " + options.cloudSummary);
                e.printStackTrace(out);
            }
        }
    }
    public void shutdown() {
        edu.umd.cs.findbugs.cloud.Cloud cloud = bugCollection.getCloud();
        cloud.shutdown();
    }
}
