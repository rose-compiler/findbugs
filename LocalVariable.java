/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba.bcp;
import edu.umd.cs.findbugs.ba.bcp.*;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
public class LocalVariable extends java.lang.Object implements edu.umd.cs.findbugs.ba.bcp.Variable {
    final private edu.umd.cs.findbugs.ba.vna.ValueNumber valueNumber;
    public LocalVariable(edu.umd.cs.findbugs.ba.vna.ValueNumber valueNumber) {
        super();
        this.valueNumber = valueNumber;
    }
    public boolean sameAs(edu.umd.cs.findbugs.ba.bcp.Variable other) {
        if ( !(other instanceof edu.umd.cs.findbugs.ba.bcp.LocalVariable)) return false;
        edu.umd.cs.findbugs.ba.bcp.LocalVariable otherLocal = (edu.umd.cs.findbugs.ba.bcp.LocalVariable) (other) ;
        return valueNumber.equals(otherLocal.valueNumber);
    }
    public java.lang.String toString() {
        return valueNumber.toString();
    }
}
// vim:ts=4
