package edu.umd.cs.findbugs.ba;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, David Hovemeyer <daveho@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.ba.*;
/**
 * Global information about the source code for an application. Currently, this
 * object contains a map of source line information for fields and classes
 * (items we don't get line number information for directly in classfiles), and
 * also source line information for methods that don't appear directly in
 * classfiles, such as abstract and native methods.
 * 
 * @author David Hovemeyer
 */
/*
         * (non-Javadoc)
         * 
         * @see java.lang.Comparable#compareTo(T)
         */
/*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#hashCode()
         */
/*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#equals(java.lang.Object)
         */
/*
         * (non-Javadoc)
         * 
         * @see java.lang.Comparable#compareTo(T)
         */
/*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#hashCode()
         */
/*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#equals(java.lang.Object)
         */
/**
     * A range of source lines.
     */
/**
         * Constructor for a single line.
         */
/**
         * Constructor for a range of lines.
         * 
         * @param start
         *            start line in range
         * @param end
         *            end line in range
         */
/**
         * @return Returns the start.
         */
/**
         * @return Returns the end.
         */
/*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#toString()
         */
/**
     * Constructor. Creates an empty object.
     */
/**
     * Add a line number entry for a field.
     * 
     * @param className
     *            name of class containing the field
     * @param fieldName
     *            name of field
     * @param range
     *            the line number(s) of the field
     */
/**
     * Add a line number entry for a method.
     * 
     * @param className
     *            name of class containing the method
     * @param methodName
     *            name of method
     * @param methodSignature
     *            signature of method
     * @param range
     *            the line number of the method
     */
/**
     * Add line number entry for a class.
     * 
     * @param className
     *            name of class
     * @param range
     *            the line numbers of the class
     */
/**
     * Look up the line number range for a field.
     * 
     * @param className
     *            name of class containing the field
     * @param fieldName
     *            name of field
     * @return the line number range, or null if no line number is known for the
     *         field
     */
/**
     * Look up the line number range for a method.
     * 
     * @param className
     *            name of class containing the method
     * @param methodName
     *            name of method
     * @param methodSignature
     *            signature of method
     * @return the line number range, or null if no line number is known for the
     *         method
     */
/**
     * Look up the line number range for a class.
     * 
     * @param className
     *            name of the class
     * @return the line number range, or null if no line number is known for the
     *         class
     */
/**
     * Read source info from given InputStream. The stream is guaranteed to be
     * closed.
     * 
     * @param inputStream
     *            the InputStream
     * @throws IOException
     *             if an I/O error occurs, or if the format is invalid
     */
// Try to parse the version number string from the first
// line.
// null means that the line does not appear to be a version
// number.
// Check to see if version is supported.
// Only 1.0 supported for now.
// Version looks good. Skip to next line of file.
// Line number for class
// Line number for method
// Line number for field
// Note: we could complain if there are more tokens,
// but instead we'll just ignore them.
// ignore
/**
     * Parse the sourceInfo version string.
     * 
     * @param line
     *            the first line of the sourceInfo file
     * @return the version number constant, or null if the line does not appear
     *         to be a version string
     */
/**
     * Expect a particular token string to be returned by the given
     * StringTokenizer.
     * 
     * @param tokenizer
     *            the StringTokenizer
     * @param token
     *            the expectedToken
     * @return true if the expected token was returned, false if not
     */
abstract interface package-info {
}
