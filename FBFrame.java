/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston MA 02111-1307, USA
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.Component;
import java.awt.Container;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
public class FBFrame extends javax.swing.JFrame {
    public FBFrame() {
    }
/**
     * Sets size of font
     * 
     * @param size
     */
    protected void setFontSize(float size) {
        this.setFont(this.getFont().deriveFont(size));
        this.setFontSizeHelper(size,this.getComponents());
    }
/*
     * Helps above method, runs through all components recursively.
     */
/**
     * @deprecated Use {@link #setFontSizeHelper(float,Component[])} instead
     */
    protected void setFontSizeHelper(java.awt.Component[] comps, float size) {
        this.setFontSizeHelper(size,comps);
    }
/*
     * Helps above method, runs through all components recursively.
     */
    protected void setFontSizeHelper(float size, java.awt.Component[] comps) {
        for (java.awt.Component comp : comps){
            comp.setFont(comp.getFont().deriveFont(size));
            if (comp instanceof java.awt.Container) this.setFontSizeHelper(size,((java.awt.Container) (comp) ).getComponents());
        }
;
    }
    public void addNotify() {
        super.addNotify();
        try {
            this.setIconImage(javax.imageio.ImageIO.read(edu.umd.cs.findbugs.gui2.MainFrame.class.getResource("smallBuggy.png")));
        }
        catch (java.io.IOException e){
            edu.umd.cs.findbugs.gui2.Debug.println(e);
        }
        this.setFontSize(edu.umd.cs.findbugs.gui2.Driver.getFontSize());
    }
}
