/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.gui2;
import edu.umd.cs.findbugs.gui2.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import edu.umd.cs.findbugs.L10N;
public class SplitLayout extends java.lang.Object implements edu.umd.cs.findbugs.gui2.FindBugsLayoutManager {
    final edu.umd.cs.findbugs.gui2.MainFrame frame;
    javax.swing.JLabel sourceTitle;
    javax.swing.JSplitPane topLeftSPane;
    javax.swing.JSplitPane topSPane;
    javax.swing.JSplitPane summarySPane;
    javax.swing.JSplitPane mainSPane;
    javax.swing.JButton viewSource = new javax.swing.JButton("View in browser");
/**
     * @param frame
     */
    public SplitLayout(edu.umd.cs.findbugs.gui2.MainFrame frame) {
        super();
        this.frame = frame;
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.gui2.FindBugsLayoutManager#createWindowMenu()
     */
    public javax.swing.JMenu createWindowMenu() {
        return null;
    }
    public void resetCommentsInputPane() {
        if (topLeftSPane != null) {
            int position = topLeftSPane.getDividerLocation();
            topLeftSPane.setRightComponent(frame.createCommentsInputPanel());
            topLeftSPane.setDividerLocation(position);
        }
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.gui2.FindBugsLayoutManager#initialize()
     */
    public void initialize() {
        java.awt.Font buttonFont = viewSource.getFont();
        viewSource.setFont(buttonFont.deriveFont(buttonFont.getSize() / 2));
        viewSource.setPreferredSize(new java.awt.Dimension(150, 15));
        viewSource.setEnabled(false);
        topLeftSPane = new javax.swing.JSplitPane(javax.swing.JSplitPane.VERTICAL_SPLIT, frame.mainFrameTree.bugListPanel(), frame.createCommentsInputPanel());
        topLeftSPane.setOneTouchExpandable(true);
        topLeftSPane.setContinuousLayout(true);
        topLeftSPane.setDividerLocation(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getSplitTreeComments());
        this.removeSplitPaneBorders(topLeftSPane);
        javax.swing.JPanel sourceTitlePanel = new javax.swing.JPanel();
        sourceTitlePanel.setLayout(new java.awt.BorderLayout());
        javax.swing.JPanel sourcePanel = new javax.swing.JPanel();
        java.awt.BorderLayout sourcePanelLayout = new java.awt.BorderLayout();
        sourcePanelLayout.setHgap(3);
        sourcePanelLayout.setVgap(3);
        sourcePanel.setLayout(sourcePanelLayout);
        sourceTitle = new javax.swing.JLabel();
        sourceTitle.setText(edu.umd.cs.findbugs.L10N.getLocalString("txt.source_listing",""));
        sourceTitlePanel.setBorder(new javax.swing.border.EmptyBorder(3, 3, 3, 3));
        sourceTitlePanel.add(viewSource,java.awt.BorderLayout.EAST);
        sourceTitlePanel.add(sourceTitle,java.awt.BorderLayout.CENTER);
        sourcePanel.setBorder(new javax.swing.border.LineBorder(java.awt.Color.GRAY));
        sourcePanel.add(sourceTitlePanel,java.awt.BorderLayout.NORTH);
        sourcePanel.add(frame.createSourceCodePanel(),java.awt.BorderLayout.CENTER);
        sourcePanel.add(frame.createSourceSearchPanel(),java.awt.BorderLayout.SOUTH);
        topSPane = new javax.swing.JSplitPane(javax.swing.JSplitPane.HORIZONTAL_SPLIT, topLeftSPane, sourcePanel);
        topSPane.setOneTouchExpandable(true);
        topSPane.setContinuousLayout(true);
        topSPane.setDividerLocation(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getSplitTop());
        this.removeSplitPaneBorders(topSPane);
        summarySPane = frame.summaryTab();
        mainSPane = new javax.swing.JSplitPane(javax.swing.JSplitPane.VERTICAL_SPLIT, topSPane, summarySPane);
        mainSPane.setOneTouchExpandable(true);
        mainSPane.setContinuousLayout(true);
        mainSPane.setDividerLocation(edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().getSplitMain());
        this.removeSplitPaneBorders(mainSPane);
        frame.setLayout(new java.awt.BorderLayout());
        frame.add(mainSPane,java.awt.BorderLayout.CENTER);
        frame.add(frame.statusBar(),java.awt.BorderLayout.SOUTH);
    }
    private void removeSplitPaneBorders(javax.swing.JSplitPane pane) {
        pane.setUI(new javax.swing.plaf.basic.BasicSplitPaneUI() {
            public javax.swing.plaf.basic.BasicSplitPaneDivider createDefaultDivider() {
                return new javax.swing.plaf.basic.BasicSplitPaneDivider(this) {
                    public void setBorder(javax.swing.border.Border b) {
                    }
                };
            }
        });
        pane.setBorder(new javax.swing.border.EmptyBorder(3, 3, 3, 3));
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.gui2.FindBugsLayoutManager#makeCommentsVisible()
     */
    public void makeCommentsVisible() {
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.gui2.FindBugsLayoutManager#makeSourceVisible()
     */
    public void makeSourceVisible() {
    }
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.gui2.FindBugsLayoutManager#saveState()
     */
    public void saveState() {
        edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setSplitTreeComments(topLeftSPane.getDividerLocation());
        edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setSplitTop(topSPane.getDividerLocation());
        edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setSplitSummary(summarySPane.getDividerLocation());
        edu.umd.cs.findbugs.gui2.GUISaveState.getInstance().setSplitMain(mainSPane.getDividerLocation());
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.gui2.FindBugsLayoutManager#setSourceTitle(java.lang
     * .String)
     */
    public void setSourceTitle(java.lang.String title) {
        sourceTitle.setText(title);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.gui2.FindBugsLayoutManager#getSourceTitleComponent()
     */
    public javax.swing.JComponent getSourceViewComponent() {
        return viewSource;
    }
}
