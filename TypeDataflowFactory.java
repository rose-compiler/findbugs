/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Analysis engine to produce TypeDataflow objects for analyzed methods.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile.engine.bcel;
import edu.umd.cs.findbugs.classfile.engine.bcel.*;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.ba.AnalysisContext;
import edu.umd.cs.findbugs.ba.AnalysisFeatures;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DepthFirstSearch;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.ba.type.ExceptionSetFactory;
import edu.umd.cs.findbugs.ba.type.TypeAnalysis;
import edu.umd.cs.findbugs.ba.type.TypeDataflow;
import edu.umd.cs.findbugs.classfile.CheckedAnalysisException;
import edu.umd.cs.findbugs.classfile.IAnalysisCache;
import edu.umd.cs.findbugs.classfile.MethodDescriptor;
public class TypeDataflowFactory extends edu.umd.cs.findbugs.classfile.engine.bcel.AnalysisFactory<edu.umd.cs.findbugs.ba.type.TypeDataflow> {
/**
     * Constructor.
     */
    public TypeDataflowFactory() {
        super("type analysis",edu.umd.cs.findbugs.ba.type.TypeDataflow.class);
    }
/*
     * (non-Javadoc)
     * 
     * @see
     * edu.umd.cs.findbugs.classfile.IAnalysisEngine#analyze(edu.umd.cs.findbugs
     * .classfile.IAnalysisCache, java.lang.Object)
     */
    public edu.umd.cs.findbugs.ba.type.TypeDataflow analyze(edu.umd.cs.findbugs.classfile.IAnalysisCache analysisCache, edu.umd.cs.findbugs.classfile.MethodDescriptor descriptor) throws edu.umd.cs.findbugs.classfile.CheckedAnalysisException {
        org.apache.bcel.generic.MethodGen methodGen = this.getMethodGen(analysisCache,descriptor);
        if (methodGen == null) {
            throw new edu.umd.cs.findbugs.ba.MethodUnprofitableException(descriptor);
        }
        edu.umd.cs.findbugs.ba.CFG cfg = this.getCFG(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.DepthFirstSearch dfs = this.getDepthFirstSearch(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.type.ExceptionSetFactory exceptionSetFactory = this.getExceptionSetFactory(analysisCache,descriptor);
        org.apache.bcel.classfile.Method method = this.getMethod(analysisCache,descriptor);
        edu.umd.cs.findbugs.ba.type.TypeAnalysis typeAnalysis = new edu.umd.cs.findbugs.ba.type.TypeAnalysis(method, methodGen, cfg, dfs, edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getLookupFailureCallback(), exceptionSetFactory);
        if (edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getBoolProperty(edu.umd.cs.findbugs.ba.AnalysisFeatures.MODEL_INSTANCEOF)) {
            typeAnalysis.setValueNumberDataflow(this.getValueNumberDataflow(analysisCache,descriptor));
        }
        typeAnalysis.setFieldStoreTypeDatabase(edu.umd.cs.findbugs.ba.AnalysisContext.currentAnalysisContext().getFieldStoreTypeDatabase());
// Field store type database.
// If present, this can give us more accurate type information
// for values loaded from fields.
        edu.umd.cs.findbugs.ba.type.TypeDataflow typeDataflow = new edu.umd.cs.findbugs.ba.type.TypeDataflow(cfg, typeAnalysis);
        try {
            typeDataflow.execute();
        }
        catch (edu.umd.cs.findbugs.classfile.CheckedAnalysisException e){
            edu.umd.cs.findbugs.ba.AnalysisContext.logError("Error performing type dataflow analysis of " + descriptor,e);
            throw e;
        }
        if (edu.umd.cs.findbugs.ba.type.TypeAnalysis.DEBUG || edu.umd.cs.findbugs.ba.ClassContext.DUMP_DATAFLOW_ANALYSIS) {
            edu.umd.cs.findbugs.ba.ClassContext.dumpTypeDataflow(method,cfg,typeDataflow);
        }
        return typeDataflow;
    }
}
