package edu.umd.cs.findbugs.classfile.analysis;
/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import edu.umd.cs.findbugs.classfile.analysis.*;
/**
 * @author pugh
 */
/**
     * @param className
     * @param fieldName
     * @param fieldSignature
     * @param isStatic
     * @param accessFlags
     * @param fieldAnnotations
     * @param isResolved
     */
/*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#getAccessFlags()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isFinal()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isPrivate()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isProtected()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isPublic()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.AccessibleEntity#isResolved()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.XField#isReferenceType()
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.XField#isVolatile()
     */
/**
     * Destructively add an annotation. We do this for "built-in" annotations
     * that might not be directly evident in the code. It's not a great idea in
     * general, but we can get away with it as long as it's done early enough
     * (i.e., before anyone asks what annotations this field has.)
     * 
     * @param annotationValue
     *            an AnnotationValue representing a field annotation
     */
/*
     * (non-Javadoc)
     * 
     * @see edu.umd.cs.findbugs.ba.XField#getFieldDescriptor()
     */
/**
     * Create a FieldInfo object to represent an unresolved field.
     * <em>Don't call this directly - use XFactory instead.</em>
     * 
     * @param className
     *            name of class containing the field
     * @param name
     *            name of field
     * @param signature
     *            field signature
     * @param isStatic
     *            true if field is static, false otherwise
     * @return FieldInfo object representing the unresolved field
     */
// without seeing
// the definition
// we don't know
// if it has a
// generic type
abstract interface package-info {
}
