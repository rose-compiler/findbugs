package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import static org.apache.bcel.Constants.DCMPG;
import static org.apache.bcel.Constants.DCMPL;
import static org.apache.bcel.Constants.FCMPG;
import static org.apache.bcel.Constants.FCMPL;
import static org.apache.bcel.Constants.IAND;
import static org.apache.bcel.Constants.IF_ACMPEQ;
import static org.apache.bcel.Constants.IF_ACMPNE;
import static org.apache.bcel.Constants.IF_ICMPEQ;
import static org.apache.bcel.Constants.IF_ICMPGE;
import static org.apache.bcel.Constants.IF_ICMPGT;
import static org.apache.bcel.Constants.IF_ICMPLE;
import static org.apache.bcel.Constants.IF_ICMPLT;
import static org.apache.bcel.Constants.IF_ICMPNE;
import static org.apache.bcel.Constants.INVOKEINTERFACE;
import static org.apache.bcel.Constants.INVOKEVIRTUAL;
import static org.apache.bcel.Constants.IOR;
import static org.apache.bcel.Constants.ISUB;
import static org.apache.bcel.Constants.IXOR;
import static org.apache.bcel.Constants.LAND;
import static org.apache.bcel.Constants.LCMP;
import static org.apache.bcel.Constants.LOR;
import static org.apache.bcel.Constants.LSUB;
import static org.apache.bcel.Constants.LXOR;
import static org.apache.bcel.Constants.POP;
import java.util.BitSet;
import java.util.Iterator;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.MethodGen;
import edu.umd.cs.findbugs.BugAnnotation;
import edu.umd.cs.findbugs.BugInstance;
import edu.umd.cs.findbugs.BugReporter;
import edu.umd.cs.findbugs.Detector;
import edu.umd.cs.findbugs.FieldAnnotation;
import edu.umd.cs.findbugs.SourceLineAnnotation;
import edu.umd.cs.findbugs.SystemProperties;
import edu.umd.cs.findbugs.ba.CFG;
import edu.umd.cs.findbugs.ba.CFGBuilderException;
import edu.umd.cs.findbugs.ba.ClassContext;
import edu.umd.cs.findbugs.ba.DataflowAnalysisException;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.MethodUnprofitableException;
import edu.umd.cs.findbugs.ba.SignatureParser;
import edu.umd.cs.findbugs.ba.XField;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
import edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow;
import edu.umd.cs.findbugs.ba.vna.ValueNumberFrame;
import edu.umd.cs.findbugs.ba.vna.ValueNumberSourceInfo;
public class FindSelfComparison2 extends java.lang.Object implements edu.umd.cs.findbugs.Detector {
    private edu.umd.cs.findbugs.BugReporter bugReporter;
    public FindSelfComparison2(edu.umd.cs.findbugs.BugReporter bugReporter) {
        super();
        this.bugReporter = bugReporter;
    }
    public void visitClassContext(edu.umd.cs.findbugs.ba.ClassContext classContext) {
        org.apache.bcel.classfile.Method[] methodList = classContext.getJavaClass().getMethods();
        for (org.apache.bcel.classfile.Method method : methodList){
            if (method.getCode() == null) continue;
            try {
                this.analyzeMethod(classContext,method);
            }
            catch (edu.umd.cs.findbugs.ba.MethodUnprofitableException mue){
// otherwise
                if (edu.umd.cs.findbugs.SystemProperties.getBoolean("unprofitable.debug")) bugReporter.logError("skipping unprofitable method in " + this.getClass().getName());
            }
            catch (edu.umd.cs.findbugs.ba.CFGBuilderException e){
                bugReporter.logError("Detector " + this.getClass().getName() + " caught exception",e);
            }
            catch (edu.umd.cs.findbugs.ba.DataflowAnalysisException e){
                bugReporter.logError("Detector " + this.getClass().getName() + " caught exception",e);
            }
        }
;
    }
    private void analyzeMethod(edu.umd.cs.findbugs.ba.ClassContext classContext, org.apache.bcel.classfile.Method method) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException, edu.umd.cs.findbugs.ba.CFGBuilderException {
        edu.umd.cs.findbugs.ba.CFG cfg = classContext.getCFG(method);
        edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow valueNumberDataflow = classContext.getValueNumberDataflow(method);
        org.apache.bcel.generic.ConstantPoolGen cpg = classContext.getConstantPoolGen();
        org.apache.bcel.generic.MethodGen methodGen = classContext.getMethodGen(method);
        java.lang.String sourceFile = classContext.getJavaClass().getSourceFileName();
        for (java.util.Iterator<edu.umd.cs.findbugs.ba.Location> i = cfg.locationIterator(); i.hasNext(); ) {
            edu.umd.cs.findbugs.ba.Location location = i.next();
            org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
            switch(ins.getOpcode()){
                case INVOKEVIRTUAL:{
                }
                case INVOKEINTERFACE:{
                    org.apache.bcel.generic.InvokeInstruction iins = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
                    java.lang.String invoking = iins.getName(cpg);
                    if (invoking.equals("equals") || invoking.equals("compareTo")) {
                        if (methodGen.getName().toLowerCase().indexOf("test") >= 0) break;
                        if (methodGen.getClassName().toLowerCase().indexOf("test") >= 0) break;
                        if (classContext.getJavaClass().getSuperclassName().toLowerCase().indexOf("test") >= 0) break;
                        if (location.getHandle().getNext().getInstruction().getOpcode() == POP) break;
                        java.lang.String sig = iins.getSignature(cpg);
                        edu.umd.cs.findbugs.ba.SignatureParser parser = new edu.umd.cs.findbugs.ba.SignatureParser(sig);
                        if (parser.getNumParameters() == 1 && (invoking.equals("equals") && sig.endsWith(";)Z") || invoking.equals("compareTo") && sig.endsWith(";)I"))) this.checkForSelfOperation(classContext,location,valueNumberDataflow,"COMPARISON",method,methodGen,sourceFile);
                    }
                    break;
                }
                case LOR:{
                }
                case LAND:{
                }
                case LXOR:{
                }
                case LSUB:{
                }
                case IOR:{
                }
                case IAND:{
                }
                case IXOR:{
                }
                case ISUB:{
                    this.checkForSelfOperation(classContext,location,valueNumberDataflow,"COMPUTATION",method,methodGen,sourceFile);
                    break;
                }
                case FCMPG:{
                }
                case DCMPG:{
                }
                case DCMPL:{
                }
                case FCMPL:{
                    break;
                }
                case LCMP:{
                }
                case IF_ACMPEQ:{
                }
                case IF_ACMPNE:{
                }
                case IF_ICMPNE:{
                }
                case IF_ICMPEQ:{
                }
                case IF_ICMPGT:{
                }
                case IF_ICMPLE:{
                }
                case IF_ICMPLT:{
                }
                case IF_ICMPGE:{
                    this.checkForSelfOperation(classContext,location,valueNumberDataflow,"COMPARISON",method,methodGen,sourceFile);
                }
            }
        }
    }
/**
     * @param classContext
     *            TODO
     * @param location
     * @param method
     *            TODO
     * @param methodGen
     *            TODO
     * @param sourceFile
     *            TODO
     * @param string
     * @throws DataflowAnalysisException
     */
    private void checkForSelfOperation(edu.umd.cs.findbugs.ba.ClassContext classContext, edu.umd.cs.findbugs.ba.Location location, edu.umd.cs.findbugs.ba.vna.ValueNumberDataflow valueNumberDataflow, java.lang.String op, org.apache.bcel.classfile.Method method, org.apache.bcel.generic.MethodGen methodGen, java.lang.String sourceFile) throws edu.umd.cs.findbugs.ba.DataflowAnalysisException {
        edu.umd.cs.findbugs.ba.vna.ValueNumberFrame frame = valueNumberDataflow.getFactAtLocation(location);
        if ( !frame.isValid()) return;
        org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
        int opcode = ins.getOpcode();
        int offset = 1;
        if (opcode == LCMP || opcode == LXOR || opcode == LAND || opcode == LOR || opcode == LSUB) offset = 2;
        edu.umd.cs.findbugs.ba.vna.ValueNumber v0 = frame.getStackValue(0);
        edu.umd.cs.findbugs.ba.vna.ValueNumber v1 = frame.getStackValue(offset);
        if ( !v1.equals(v0)) return;
        if (v0.hasFlag(edu.umd.cs.findbugs.ba.vna.ValueNumber.CONSTANT_CLASS_OBJECT) || v0.hasFlag(edu.umd.cs.findbugs.ba.vna.ValueNumber.CONSTANT_VALUE)) return;
        int priority = HIGH_PRIORITY;
        if (opcode == ISUB || opcode == LSUB || opcode == INVOKEINTERFACE || opcode == INVOKEVIRTUAL) priority = NORMAL_PRIORITY;
        edu.umd.cs.findbugs.ba.XField field = edu.umd.cs.findbugs.ba.vna.ValueNumberSourceInfo.findXFieldFromValueNumber(method,location,v0,frame);
        edu.umd.cs.findbugs.BugAnnotation annotation;
        java.lang.String prefix;
        if (field != null) {
            if (field.isVolatile()) return;
            if (true) 
// don't report these; too many false positives
            return;
            annotation = edu.umd.cs.findbugs.FieldAnnotation.fromXField(field);
            prefix = "SA_FIELD_SELF_";
        }
        else {
            annotation = edu.umd.cs.findbugs.ba.vna.ValueNumberSourceInfo.findLocalAnnotationFromValueNumber(method,location,v0,frame);
            prefix = "SA_LOCAL_SELF_";
            if (opcode == ISUB) 
// only report this if simple detector reports it
            return;
        }
        if (annotation == null) return;
        edu.umd.cs.findbugs.SourceLineAnnotation sourceLine = edu.umd.cs.findbugs.SourceLineAnnotation.fromVisitedInstruction(classContext,methodGen,sourceFile,location.getHandle());
        int line = sourceLine.getStartLine();
        java.util.BitSet occursMultipleTimes = classContext.linesMentionedMultipleTimes(method);
        if (line > 0 && occursMultipleTimes.get(line)) return;
        edu.umd.cs.findbugs.BugInstance bug = new edu.umd.cs.findbugs.BugInstance(this, prefix + op, priority).addClassAndMethod(methodGen,sourceFile).add(annotation).addSourceLine(classContext,methodGen,sourceFile,location.getHandle());
        bugReporter.reportBug(bug);
    }
    public void report() {
    }
}
