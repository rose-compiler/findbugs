/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2004, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * StreamFactory for streams that are created as the result of calling a method
 * on an object.
 */
package edu.umd.cs.findbugs.detect;
import edu.umd.cs.findbugs.detect.*;
import java.util.BitSet;
import org.apache.bcel.Constants;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.ReferenceType;
import edu.umd.cs.findbugs.ba.Hierarchy;
import edu.umd.cs.findbugs.ba.Location;
import edu.umd.cs.findbugs.ba.ObjectTypeFactory;
import edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback;
public class MethodReturnValueStreamFactory extends java.lang.Object implements edu.umd.cs.findbugs.detect.StreamFactory {
    final private static java.util.BitSet invokeOpcodeSet = new java.util.BitSet();
    static {
        invokeOpcodeSet.set(org.apache.bcel.Constants.INVOKEINTERFACE);
        invokeOpcodeSet.set(org.apache.bcel.Constants.INVOKESPECIAL);
        invokeOpcodeSet.set(org.apache.bcel.Constants.INVOKESTATIC);
        invokeOpcodeSet.set(org.apache.bcel.Constants.INVOKEVIRTUAL);
    }
    private org.apache.bcel.generic.ObjectType baseClassType;
    private java.lang.String methodName;
    private java.lang.String methodSig;
    private boolean isUninteresting;
    private java.lang.String bugType;
/**
     * Constructor. The Streams created will be marked as uninteresting.
     * 
     * @param baseClass
     *            base class through which the method will be called (we check
     *            instances of the base class and all subtypes)
     * @param methodName
     *            name of the method called
     * @param methodSig
     *            signature of the method called
     */
    public MethodReturnValueStreamFactory(java.lang.String baseClass, java.lang.String methodName, java.lang.String methodSig) {
        super();
        this.baseClassType = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance(baseClass);
        this.methodName = methodName;
        this.methodSig = methodSig;
        this.isUninteresting = true;
    }
/**
     * Constructor. The Streams created will be marked as interesting.
     * 
     * @param baseClass
     *            base class through which the method will be called (we check
     *            instances of the base class and all subtypes)
     * @param methodName
     *            name of the method called
     * @param methodSig
     *            signature of the method called
     * @param bugType
     *            the bug type that should be reported if the stream is not
     *            closed on all paths out of the method
     */
    public MethodReturnValueStreamFactory(java.lang.String baseClass, java.lang.String methodName, java.lang.String methodSig, java.lang.String bugType) {
        super();
        this.baseClassType = edu.umd.cs.findbugs.ba.ObjectTypeFactory.getInstance(baseClass);
        this.methodName = methodName;
        this.methodSig = methodSig;
        this.isUninteresting = false;
        this.bugType = bugType;
    }
    public edu.umd.cs.findbugs.detect.Stream createStream(edu.umd.cs.findbugs.ba.Location location, org.apache.bcel.generic.ObjectType type, org.apache.bcel.generic.ConstantPoolGen cpg, edu.umd.cs.findbugs.ba.RepositoryLookupFailureCallback lookupFailureCallback) {
        try {
            org.apache.bcel.generic.Instruction ins = location.getHandle().getInstruction();
// For now, just support instance methods
            short opcode = ins.getOpcode();
            if ( !invokeOpcodeSet.get(opcode)) return null;
// Is invoked class a subtype of the base class we want
// FIXME: should test be different for INVOKESPECIAL and
// INVOKESTATIC?
            org.apache.bcel.generic.InvokeInstruction inv = (org.apache.bcel.generic.InvokeInstruction) (ins) ;
            org.apache.bcel.generic.ReferenceType classType = inv.getReferenceType(cpg);
            if ( !edu.umd.cs.findbugs.ba.Hierarchy.isSubtype(classType,baseClassType)) return null;
// See if method name and signature match
            java.lang.String methodName = inv.getMethodName(cpg);
            java.lang.String methodSig = inv.getSignature(cpg);
            if ( !this.methodName.equals(methodName) ||  !this.methodSig.equals(methodSig)) return null;
            java.lang.String streamClass = type.getClassName();
            if (streamClass.equals("java.sql.CallableStatement")) streamClass = "java.sql.PreparedStatement";
            edu.umd.cs.findbugs.detect.Stream result = new edu.umd.cs.findbugs.detect.Stream(location, streamClass, streamClass).setIgnoreImplicitExceptions(true).setIsOpenOnCreation(true);
            if ( !isUninteresting) result.setInteresting(bugType);
            return result;
        }
        catch (java.lang.ClassNotFoundException e){
            lookupFailureCallback.reportMissingClass(e);
        }
        return null;
    }
}
// vim:ts=3
