/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2007 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Common superinterface for code entities having access flags: classes, fields,
 * and methods.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import edu.umd.cs.findbugs.classfile.ClassDescriptor;
abstract public interface AccessibleEntity {
/**
     * Get the entity's access flags.
     */
    abstract public int getAccessFlags();
/**
     * Is the entity static?
     */
    abstract public boolean isStatic();
/**
     * Is the entity final?
     */
    abstract public boolean isFinal();
/**
     * Is the entity public?
     */
    abstract public boolean isPublic();
/**
     * Is the entity protected?
     */
    abstract public boolean isProtected();
/**
     * Is the entity private?
     */
    abstract public boolean isPrivate();
/**
     * Is the entity synthetic?
     */
    abstract public boolean isSynthetic();
/**
     * Is the entity deprecated?
     */
    abstract public boolean isDeprecated();
/**
     * Get the ClassDescriptor representing the class (if entity is a class) or
     * the class containing the entity (if a field or method).
     */
    abstract public edu.umd.cs.findbugs.classfile.ClassDescriptor getClassDescriptor();
}
