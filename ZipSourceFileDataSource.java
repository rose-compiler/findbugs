/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * A source file data source for source files residing in Zip or Jar archives.
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
public class ZipSourceFileDataSource extends java.lang.Object implements edu.umd.cs.findbugs.ba.SourceFileDataSource {
    private java.util.zip.ZipFile zipFile;
    private java.lang.String entryName;
    private java.util.zip.ZipEntry zipEntry;
    public ZipSourceFileDataSource(java.util.zip.ZipFile zipFile, java.lang.String entryName) {
        super();
        this.zipFile = zipFile;
        this.entryName = entryName;
        this.zipEntry = zipFile.getEntry(entryName);
    }
    public java.io.InputStream open() throws java.io.IOException {
        if (zipEntry == null) throw new java.io.FileNotFoundException("No zip entry for " + entryName);
        return zipFile.getInputStream(zipEntry);
    }
    public java.lang.String getFullFileName() {
        return entryName;
    }
/* (non-Javadoc)
     * @see edu.umd.cs.findbugs.ba.SourceFileDataSource#getLastModified()
     */
    public long getLastModified() {
        long time = zipEntry.getTime();
        if (time < 0) return 0;
        return time;
    }
}
// vim:ts=4
