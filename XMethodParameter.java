/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2005, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * @author pugh
 */
package edu.umd.cs.findbugs.ba;
import edu.umd.cs.findbugs.ba.*;
public class XMethodParameter extends java.lang.Object implements java.lang.Comparable<edu.umd.cs.findbugs.ba.XMethodParameter> {
/**
     * Create a new Method parameter reference
     * 
     * @param m
     *            the method of which this is a parameter to
     * @param p
     *            the parameter index (0 for first parameter)
     */
    public XMethodParameter(edu.umd.cs.findbugs.ba.XMethod m, int p) {
        super();
        method = m;
        parameter = p;
    }
    final private edu.umd.cs.findbugs.ba.XMethod method;
    final private int parameter;
    public edu.umd.cs.findbugs.ba.XMethod getMethod() {
        return method;
    }
    public int getParameterNumber() {
        return parameter;
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.ba.XMethodParameter)) return false;
        edu.umd.cs.findbugs.ba.XMethodParameter mp2 = (edu.umd.cs.findbugs.ba.XMethodParameter) (o) ;
        return parameter == mp2.parameter && method.equals(mp2.method);
    }
    public int hashCode() {
        return method.hashCode() + parameter;
    }
    public int compareTo(edu.umd.cs.findbugs.ba.XMethodParameter mp2) {
        int result = method.compareTo(mp2.method);
        if (result != 0) return result;
        return parameter - mp2.parameter;
    }
    public java.lang.String toString() {
        return "parameter " + parameter + " of " + method;
    }
}
