/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Filter a property database, only passing through the annotations on public or
 * protected methods
 */
package edu.umd.cs.findbugs.tools;
import edu.umd.cs.findbugs.tools.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.WillClose;
import org.apache.bcel.Constants;
import edu.umd.cs.findbugs.tools.FilterAndCombineBitfieldPropertyDatabase.Status;
import edu.umd.cs.findbugs.util.Util;
public class FilterPropertyDatabase extends java.lang.Object {
    public FilterPropertyDatabase() {
    }
    final static int FLAGS = org.apache.bcel.Constants.ACC_PROTECTED | org.apache.bcel.Constants.ACC_PUBLIC;
/**
     * @param args
     * @throws IOException
     */
    public static void main(java.lang.String[] args) throws java.io.IOException {
        java.io.InputStream inSource = java.lang.System.in;
        if (args.length > 0) inSource = new java.io.FileInputStream(args[0]);
        process(inSource);
    }
/**
     * @param inSource
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private static void process(java.io.InputStream inSource) throws java.io.IOException, java.io.UnsupportedEncodingException {
        java.io.BufferedReader in = null;
        try {
            in = new java.io.BufferedReader(edu.umd.cs.findbugs.util.Util.getReader(inSource));
            java.util.regex.Pattern p = java.util.regex.Pattern.compile("^(([^,]+),.+),([0-9]+)\\|(.+)$");
            while (true) {
                java.lang.String s = in.readLine();
                if (s == null) break;
                java.util.regex.Matcher m = p.matcher(s);
                if (m.find()) {
                    java.lang.String className = m.group(2);
                    if (edu.umd.cs.findbugs.tools.FilterAndCombineBitfieldPropertyDatabase.getStatus(className) == edu.umd.cs.findbugs.tools.FilterAndCombineBitfieldPropertyDatabase.Status.UNEXPOSED) continue;
                    int accFlags = java.lang.Integer.parseInt(m.group(3));
                    if ((accFlags & FLAGS) != 0) java.lang.System.out.println(s);
                }
            }
        }
        finally {
            edu.umd.cs.findbugs.util.Util.closeSilently(in);
        }
    }
}
