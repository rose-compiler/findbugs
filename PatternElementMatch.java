/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * PatternElementMatch represents matching a PatternElement against a single
 * instruction. The "prev" field points to the previous PatternElementMatch. By
 * building up sequences of PatternElementMatch objects in this way, we can
 * implement nondeterministic matching without having to copy anything.
 */
package edu.umd.cs.findbugs.ba.bcp;
import edu.umd.cs.findbugs.ba.bcp.*;
import org.apache.bcel.generic.InstructionHandle;
import edu.umd.cs.findbugs.ba.BasicBlock;
public class PatternElementMatch extends java.lang.Object {
    final private edu.umd.cs.findbugs.ba.bcp.PatternElement patternElement;
    final private org.apache.bcel.generic.InstructionHandle matchedInstruction;
    final private edu.umd.cs.findbugs.ba.BasicBlock basicBlock;
    final private int matchCount;
    final private edu.umd.cs.findbugs.ba.bcp.PatternElementMatch prev;
/**
     * Constructor.
     * 
     * @param patternElement
     *            the PatternElement being matched
     * @param matchedInstruction
     *            the instruction which matched the PatternElement
     * @param basicBlock
     *            the basic block containing the matched instruction
     * @param matchCount
     *            the index (starting at zero) of the instructions matching the
     *            PatternElement; multiple instructions can match the same
     *            PatternElement
     * @param prev
     *            the previous PatternElementMatch
     */
    public PatternElementMatch(edu.umd.cs.findbugs.ba.bcp.PatternElement patternElement, org.apache.bcel.generic.InstructionHandle matchedInstruction, edu.umd.cs.findbugs.ba.BasicBlock basicBlock, int matchCount, edu.umd.cs.findbugs.ba.bcp.PatternElementMatch prev) {
        super();
        this.patternElement = patternElement;
        this.matchedInstruction = matchedInstruction;
        this.basicBlock = basicBlock;
        this.matchCount = matchCount;
        this.prev = prev;
    }
/**
     * Get the PatternElement.
     */
    public edu.umd.cs.findbugs.ba.bcp.PatternElement getPatternElement() {
        return patternElement;
    }
/**
     * Get the matched instruction.
     */
    public org.apache.bcel.generic.InstructionHandle getMatchedInstructionInstructionHandle() {
        return matchedInstruction;
    }
/**
     * Get the basic block containing the matched instruction.
     */
    public edu.umd.cs.findbugs.ba.BasicBlock getBasicBlock() {
        return basicBlock;
    }
/*
     * Get the index of this instruction in terms of how many instructions have
     * matched this PatternElement. (0 for the first instruction to match the
     * PatternElement, etc.)
     */
    public int getMatchCount() {
        return matchCount;
    }
/**
     * Get the previous PatternMatchElement.
     */
    public edu.umd.cs.findbugs.ba.bcp.PatternElementMatch getPrev() {
        return prev;
    }
/**
     * Get the <em>first</em> instruction matched by the PatternElement with
     * given label.
     */
    public org.apache.bcel.generic.InstructionHandle getLabeledInstruction(java.lang.String label) {
        edu.umd.cs.findbugs.ba.bcp.PatternElementMatch first = this.getFirstLabeledMatch(label);
        return first != null ? first.getMatchedInstructionInstructionHandle() : null;
    }
/**
     * Get <em>first</em> match element with given label, if any.
     */
    public edu.umd.cs.findbugs.ba.bcp.PatternElementMatch getFirstLabeledMatch(java.lang.String label) {
        edu.umd.cs.findbugs.ba.bcp.PatternElementMatch cur = this;
        edu.umd.cs.findbugs.ba.bcp.PatternElementMatch result = null;
        while (cur != null) {
            java.lang.String elementLabel = cur.patternElement.getLabel();
            if (elementLabel != null && elementLabel.equals(label)) result = cur;
            cur = cur.prev;
        }
        return result;
    }
/**
     * Get <em>last</em> match element with given label, if any.
     */
    public edu.umd.cs.findbugs.ba.bcp.PatternElementMatch getLastLabeledMatch(java.lang.String label) {
        edu.umd.cs.findbugs.ba.bcp.PatternElementMatch cur = this;
        while (cur != null) {
            java.lang.String elementLabel = cur.patternElement.getLabel();
            if (elementLabel != null && elementLabel.equals(label)) return cur;
            cur = cur.prev;
        }
        return null;
    }
/**
     * Return whether or not the most recently matched instruction allows
     * trailing edges.
     */
    public boolean allowTrailingEdges() {
        return patternElement.allowTrailingEdges();
    }
    public java.lang.String toString() {
        java.lang.StringBuilder buf = new java.lang.StringBuilder();
        edu.umd.cs.findbugs.ba.bcp.PatternElementMatch cur = this;
        buf.append(cur.patternElement.toString());
        buf.append(", ");
        buf.append(cur.matchedInstruction.toString());
        buf.append(", ");
        buf.append(cur.matchCount);
        return buf.toString();
    }
    public int hashCode() {
// Do the simplest thing possible that works
        throw new java.lang.UnsupportedOperationException();
    }
    public boolean equals(java.lang.Object o) {
        if ( !(o instanceof edu.umd.cs.findbugs.ba.bcp.PatternElementMatch)) return false;
        edu.umd.cs.findbugs.ba.bcp.PatternElementMatch lhs = this;
        edu.umd.cs.findbugs.ba.bcp.PatternElementMatch rhs = (edu.umd.cs.findbugs.ba.bcp.PatternElementMatch) (o) ;
        while (lhs != null && rhs != null) {
            if (lhs.patternElement != rhs.patternElement || lhs.matchedInstruction != rhs.matchedInstruction || lhs.matchCount != rhs.matchCount) return false;
            lhs = lhs.prev;
            rhs = rhs.prev;
        }
        return lhs == rhs;
    }
}
// vim:ts=4
