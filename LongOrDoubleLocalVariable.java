/*
 * Bytecode Analysis Framework
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs.ba.bcp;
import edu.umd.cs.findbugs.ba.bcp.*;
import edu.umd.cs.findbugs.ba.vna.ValueNumber;
public class LongOrDoubleLocalVariable extends java.lang.Object implements edu.umd.cs.findbugs.ba.bcp.Variable {
    private edu.umd.cs.findbugs.ba.vna.ValueNumber topValue;
    private edu.umd.cs.findbugs.ba.vna.ValueNumber nextValue;
    public LongOrDoubleLocalVariable(edu.umd.cs.findbugs.ba.vna.ValueNumber topValue, edu.umd.cs.findbugs.ba.vna.ValueNumber nextValue) {
        super();
        this.topValue = topValue;
        this.nextValue = nextValue;
    }
    public boolean sameAs(edu.umd.cs.findbugs.ba.bcp.Variable other) {
        if ( !(other instanceof edu.umd.cs.findbugs.ba.bcp.LongOrDoubleLocalVariable)) return false;
        edu.umd.cs.findbugs.ba.bcp.LongOrDoubleLocalVariable otherLongOrDouble = (edu.umd.cs.findbugs.ba.bcp.LongOrDoubleLocalVariable) (other) ;
        return topValue.equals(otherLongOrDouble.topValue) && nextValue.equals(otherLongOrDouble.nextValue);
    }
}
// vim:ts=4
