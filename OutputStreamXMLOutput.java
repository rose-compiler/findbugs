/*
 * XML input/output support for FindBugs
 * Copyright (C) 2004,2005 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Write XML to an output stream.
 *
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.xml;
import edu.umd.cs.findbugs.xml.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import javax.annotation.WillCloseWhenClosed;
import edu.umd.cs.findbugs.annotations.DischargesObligation;
import edu.umd.cs.findbugs.util.Strings;
public class OutputStreamXMLOutput extends java.lang.Object implements edu.umd.cs.findbugs.xml.XMLOutput {
    final private static java.lang.String OPENING = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    private static java.lang.String getStylesheetCode(java.lang.String stylesheet) {
        if (stylesheet == null) return "";
        return "<?xml-stylesheet type=\"text/xsl\" href=\"" + stylesheet + "\"?>\n";
    }
    private java.io.Writer out;
    private int nestingLevel;
    private boolean newLine;
    private java.lang.String stylesheet;
/**
     * Constructor.
     *
     * @param os
     *            OutputStream to write XML output to
     */
    public OutputStreamXMLOutput(java.io.OutputStream os) {
        this(os,null);
    }
/**
     * Constructor.
     *
     * @param os
     *            OutputStream to write XML output to
     */
    public OutputStreamXMLOutput(java.io.Writer writer) {
        this(writer,null);
    }
/**
     * Constructor.
     *
     * @param os
     *            OutputStream to write XML output to
     * @param stylesheet
     *            name of stylesheet
     */
    public OutputStreamXMLOutput(java.io.OutputStream os, java.lang.String stylesheet) {
        super();
        this.out = new java.io.OutputStreamWriter(os, java.nio.charset.Charset.forName("UTF-8"));
        this.nestingLevel = 0;
        this.newLine = true;
        this.stylesheet = stylesheet;
    }
/*
    * @param os
    *            Writer to write XML output to
    * @param stylesheet
    *            name of stylesheet
    */
    public OutputStreamXMLOutput(java.io.Writer writer, java.lang.String stylesheet) {
        super();
        this.out = writer;
        this.nestingLevel = 0;
        this.newLine = true;
        this.stylesheet = stylesheet;
    }
    public void beginDocument() throws java.io.IOException {
        out.write(OPENING);
        out.write(getStylesheetCode(stylesheet));
        out.write("\n");
        newLine = true;
    }
    public void openTag(java.lang.String tagName) throws java.io.IOException {
        this.emitTag(tagName,false);
    }
    public void openTag(java.lang.String tagName, edu.umd.cs.findbugs.xml.XMLAttributeList attributeList) throws java.io.IOException {
        this.emitTag(tagName,attributeList.toString(),false);
    }
    public void openCloseTag(java.lang.String tagName) throws java.io.IOException {
        this.emitTag(tagName,true);
    }
    public void openCloseTag(java.lang.String tagName, edu.umd.cs.findbugs.xml.XMLAttributeList attributeList) throws java.io.IOException {
        this.emitTag(tagName,attributeList.toString(),true);
    }
    public void startTag(java.lang.String tagName) throws java.io.IOException {
        this.indent();
        ++nestingLevel;
        out.write("<" + tagName);
    }
    public void addAttribute(java.lang.String name, java.lang.String value) throws java.io.IOException {
        out.write('\u0020');
        out.write(name);
        out.write('\u003d');
        out.write('"');
        out.write(edu.umd.cs.findbugs.xml.XMLAttributeList.getQuotedAttributeValue(value));
        out.write('"');
    }
    public void stopTag(boolean close) throws java.io.IOException {
        if (close) {
            out.write("/>\n");
            --nestingLevel;
            newLine = true;
        }
        else {
            out.write(">");
            newLine = false;
        }
    }
    private void emitTag(java.lang.String tagName, boolean close) throws java.io.IOException {
        this.startTag(tagName);
        this.stopTag(close);
    }
    private void emitTag(java.lang.String tagName, java.lang.String attributes, boolean close) throws java.io.IOException {
        this.startTag(tagName);
        attributes = attributes.trim();
        if (attributes.length() > 0) {
            out.write(" ");
            out.write(attributes);
        }
        this.stopTag(close);
    }
    public void closeTag(java.lang.String tagName) throws java.io.IOException {
        --nestingLevel;
        if (newLine) this.indent();
        out.write("</" + tagName + ">\n");
        newLine = true;
    }
    public void writeText(java.lang.String text) throws java.io.IOException {
        out.write(edu.umd.cs.findbugs.util.Strings.escapeXml(text));
    }
    public void writeCDATA(java.lang.String cdata) throws java.io.IOException {
// FIXME: We just trust fate that the characters being written
// don't contain the string "]]>"
        assert (cdata.indexOf("]]") ==  -1);
        out.write("<![CDATA[");
        out.write(cdata);
        out.write("]]>");
        newLine = false;
    }
    public void flush() throws java.io.IOException {
        out.flush();
    }
    public void finish() throws java.io.IOException {
        out.close();
    }
    private void indent() throws java.io.IOException {
        if ( !newLine) out.write("\n");
        for (int i = 0; i < nestingLevel; ++i) {
            out.write("  ");
        }
    }
}
// vim:ts=4
