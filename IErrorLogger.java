/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2006, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Interface for objects that log various kinds of analysis errors.
 * 
 * @author David Hovemeyer
 */
package edu.umd.cs.findbugs.classfile;
import edu.umd.cs.findbugs.classfile.*;
abstract public interface IErrorLogger {
/**
     * Called to report a class lookup failure.
     * 
     * @param ex
     *            a ClassNotFoundException resulting from the class lookup
     *            failure
     */
    abstract public void reportMissingClass(java.lang.ClassNotFoundException ex);
/**
     * Called to report a class lookup failure.
     * 
     * @param classDescriptor
     *            ClassDescriptor of a missing class
     */
    abstract public void reportMissingClass(edu.umd.cs.findbugs.classfile.ClassDescriptor classDescriptor);
/**
     * Log an error that occurs while performing analysis.
     * 
     * @param message
     *            the error message
     */
    abstract public void logError(java.lang.String message);
/**
     * Log an error that occurs while performing analysis.
     * 
     * @param message
     *            the error message
     * @param e
     *            the exception which is the underlying cause of the error
     */
    abstract public void logError(java.lang.String message, java.lang.Throwable e);
/**
     * Report that we skipped some analysis of a method
     * 
     * @param method
     *            the method we skipped
     */
    abstract public void reportSkippedAnalysis(edu.umd.cs.findbugs.classfile.MethodDescriptor method);
}
