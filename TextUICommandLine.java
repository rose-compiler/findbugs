/*
 * FindBugs - Find Bugs in Java programs
 * Copyright (C) 2003-2008, University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 * Helper class to parse the command line and configure the IFindBugsEngine
 * object. As a side-effect it also configures a DetectorFactoryCollection (to
 * enable and disable detectors as requested).
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.zip.GZIPOutputStream;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import org.dom4j.DocumentException;
import edu.umd.cs.findbugs.annotations.SuppressWarnings;
import edu.umd.cs.findbugs.charsets.UTF8;
import edu.umd.cs.findbugs.config.UserPreferences;
import edu.umd.cs.findbugs.filter.FilterException;
import edu.umd.cs.findbugs.util.Util;
public class TextUICommandLine extends edu.umd.cs.findbugs.FindBugsCommandLine {
/**
     * Handling callback for choose() method, used to implement the
     * -chooseVisitors and -choosePlugins options.
     */
    abstract private static interface Chooser {
/**
         * Choose a detector, plugin, etc.
         *
         * @param enable
         *            whether or not the item should be enabled
         * @param what
         *            the item
         */
        abstract public void choose(boolean enable, java.lang.String what);
    }
    final private static boolean DEBUG = java.lang.Boolean.getBoolean("textui.debug");
    final private static int PRINTING_REPORTER = 0;
    final private static int SORTING_REPORTER = 1;
    final private static int XML_REPORTER = 2;
    final private static int EMACS_REPORTER = 3;
    final private static int HTML_REPORTER = 4;
    final private static int XDOCS_REPORTER = 5;
    private int bugReporterType = PRINTING_REPORTER;
    private boolean relaxedReportingMode = false;
    private boolean useLongBugCodes = false;
    private boolean showProgress = false;
    private boolean xmlMinimal = false;
    private boolean xmlWithMessages = false;
    private boolean xmlWithAbridgedMessages = false;
    private java.lang.String stylesheet = null;
    private boolean quiet = false;
    final private edu.umd.cs.findbugs.ClassScreener classScreener = new edu.umd.cs.findbugs.ClassScreener();
    final private java.util.Set<java.lang.String> enabledBugReporterDecorators = new java.util.LinkedHashSet<java.lang.String>();
    final private java.util.Set<java.lang.String> disabledBugReporterDecorators = new java.util.LinkedHashSet<java.lang.String>();
    private boolean setExitCode = false;
    private boolean noClassOk = false;
    private int priorityThreshold = edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY;
    private int rankThreshold = edu.umd.cs.findbugs.SystemProperties.getInt("findbugs.maxRank",20);
    private java.io.PrintStream outputStream = null;
    private java.util.Set<java.lang.String> bugCategorySet = null;
    private java.lang.String trainingOutputDir;
    private java.lang.String trainingInputDir;
    private java.lang.String releaseName = "";
    private java.lang.String projectName = "";
    private java.lang.String sourceInfoFile = null;
    private java.lang.String redoAnalysisFile = null;
    private boolean xargs = false;
    private boolean scanNestedArchives = true;
    private boolean applySuppression;
    private boolean printConfiguration;
    private boolean printVersion;
/**
     * Constructor.
     */
    public TextUICommandLine() {
        super();
        this.addSwitch("-showPlugins","show list of available detector plugins");
        this.startOptionGroup("Output options:");
        this.addSwitch("-justListOptions","throw an exception that lists the provided options");
        this.makeOptionUnlisted("-justListOptions");
        this.addSwitch("-timestampNow","set timestamp of results to be current time");
        this.addSwitch("-quiet","suppress error messages");
        this.addSwitch("-longBugCodes","report long bug codes");
        this.addSwitch("-progress","display progress in terminal window");
        this.addOption("-release","release name","set the release name of the analyzed application");
        this.addSwitch("-experimental","report of any confidence level including experimental bug patterns");
        this.addSwitch("-low","report warnings of any confidence level");
        this.addSwitch("-medium","report only medium and high confidence warnings [default]");
        this.addSwitch("-high","report only high confidence warnings");
        this.addOption("-maxRank","rank","only report issues with a bug rank at least as scary as that provided");
        this.addSwitch("-sortByClass","sort warnings by class");
        this.addSwitchWithOptionalExtraPart("-xml","withMessages","XML output (optionally with messages)");
        this.addSwitch("-xdocs","xdoc XML output to use with Apache Maven");
        this.addSwitchWithOptionalExtraPart("-html","stylesheet","Generate HTML output (default stylesheet is default.xsl)");
        this.addSwitch("-emacs","Use emacs reporting format");
        this.addSwitch("-relaxed","Relaxed reporting mode (more false positives!)");
        this.addSwitchWithOptionalExtraPart("-train","outputDir","Save training data (experimental); output dir defaults to '.'");
        this.addSwitchWithOptionalExtraPart("-useTraining","inputDir","Use training data (experimental); input dir defaults to '.'");
        this.addOption("-redoAnalysis","filename","Redo analysis using configureation from previous analysis");
        this.addOption("-sourceInfo","filename","Specify source info file (line numbers for fields/classes)");
        this.addOption("-projectName","project name","Descriptive name of project");
        this.addOption("-reanalyze","filename","redo analysis in provided file");
        this.addOption("-outputFile","filename","Save output in named file");
        this.addOption("-output","filename","Save output in named file");
        this.makeOptionUnlisted("-outputFile");
        this.addSwitchWithOptionalExtraPart("-nested","true|false","analyze nested jar/zip archives (default=true)");
        this.startOptionGroup("Output filtering options:");
        this.addOption("-bugCategories","cat1[,cat2...]","only report bugs in given categories");
        this.addOption("-onlyAnalyze","classes/packages","only analyze given classes and packages; end with .* to indicate classes in a package, .- to indicate a package prefix");
        this.addOption("-excludeBugs","baseline bugs","exclude bugs that are also reported in the baseline xml output");
        this.addOption("-exclude","filter file","exclude bugs matching given filter");
        this.addOption("-include","filter file","include only bugs matching given filter");
        this.addSwitch("-applySuppression","Exclude any bugs that match suppression filter loaded from fbp file");
        this.startOptionGroup("Detector (visitor) configuration options:");
        this.addOption("-visitors","v1[,v2...]","run only named visitors");
        this.addOption("-omitVisitors","v1[,v2...]","omit named visitors");
        this.addOption("-chooseVisitors","+v1,-v2,...","selectively enable/disable detectors");
        this.addOption("-choosePlugins","+p1,-p2,...","selectively enable/disable plugins");
        this.addOption("-adjustPriority","v1=(raise|lower)[,...]","raise/lower priority of warnings for given visitor(s)");
        this.startOptionGroup("Project configuration options:");
        this.addOption("-auxclasspath","classpath","set aux classpath for analysis");
        this.addSwitch("-auxclasspathFromInput","read aux classpath from standard input");
        this.addOption("-sourcepath","source path","set source path for analyzed classes");
        this.addSwitch("-exitcode","set exit code of process");
        this.addSwitch("-noClassOk","output empty warning file if no classes are specified");
        this.addSwitch("-xargs","get list of classfiles/jarfiles from standard input rather than command line");
        this.addOption("-cloud","id","set cloud id");
        this.addOption("-cloudProperty","key=value","set cloud property");
        this.addOption("-bugReporters","name,name2,-name3","bug reporter decorators to explicitly enable/disable");
        this.addSwitch("-printConfiguration","print configuration and exit, without running analysis");
        this.addSwitch("-version","print version, check for updates and exit, without running analysis");
    }
    public edu.umd.cs.findbugs.Project getProject() {
        return project;
    }
    public boolean getXargs() {
        return xargs;
    }
    public boolean setExitCode() {
        return setExitCode;
    }
    public boolean noClassOk() {
        return noClassOk;
    }
    public boolean quiet() {
        return quiet;
    }
    public boolean applySuppression() {
        return applySuppression;
    }
    public boolean justPrintConfiguration() {
        return printConfiguration;
    }
    public boolean justPrintVersion() {
        return printVersion;
    }
    java.util.Map<java.lang.String, java.lang.String> parsedOptions = new java.util.LinkedHashMap<java.lang.String, java.lang.String>();
    protected void handleOption(java.lang.String option, java.lang.String optionExtraPart) {
        parsedOptions.put(option,optionExtraPart);
        if (DEBUG) {
            if (optionExtraPart != null) java.lang.System.out.println("option " + option + ":" + optionExtraPart);
            else java.lang.System.out.println("option " + option);
        }
        if (option.equals("-showPlugins")) {
            java.lang.System.out.println("Available plugins:");
            int count = 0;
            for (java.util.Iterator<edu.umd.cs.findbugs.Plugin> i = edu.umd.cs.findbugs.DetectorFactoryCollection.instance().pluginIterator(); i.hasNext(); ) {
                edu.umd.cs.findbugs.Plugin plugin = i.next();
                java.lang.System.out.println("  " + plugin.getPluginId() + " (default: " + (plugin.isEnabledByDefault() ? "enabled" : "disabled") + ")");
                if (plugin.getShortDescription() != null) java.lang.System.out.println("    Description: " + plugin.getShortDescription());
                if (plugin.getProvider() != null) java.lang.System.out.println("    Provider: " + plugin.getProvider());
                if (plugin.getWebsite() != null) java.lang.System.out.println("    Website: " + plugin.getWebsite());
                ++count;
            }
            if (count == 0) {
                java.lang.System.out.println("  No plugins are available (FindBugs installed incorrectly?)");
            }
            java.lang.System.exit(0);
        }
        else if (option.equals("-experimental")) priorityThreshold = edu.umd.cs.findbugs.Priorities.EXP_PRIORITY;
        else if (option.equals("-longBugCodes")) useLongBugCodes = true;
        else if (option.equals("-progress")) {
            showProgress = true;
        }
        else if (option.equals("-timestampNow")) project.setTimestamp(java.lang.System.currentTimeMillis());
        else if (option.equals("-low")) priorityThreshold = edu.umd.cs.findbugs.Priorities.LOW_PRIORITY;
        else if (option.equals("-medium")) priorityThreshold = edu.umd.cs.findbugs.Priorities.NORMAL_PRIORITY;
        else if (option.equals("-high")) priorityThreshold = edu.umd.cs.findbugs.Priorities.HIGH_PRIORITY;
        else if (option.equals("-sortByClass")) bugReporterType = SORTING_REPORTER;
        else if (option.equals("-xml")) {
            bugReporterType = XML_REPORTER;
            if ( !optionExtraPart.equals("")) {
                if (optionExtraPart.equals("withMessages")) xmlWithMessages = true;
                else if (optionExtraPart.equals("withAbridgedMessages")) {
                    xmlWithMessages = true;
                    xmlWithAbridgedMessages = true;
                }
                else if (optionExtraPart.equals("minimal")) {
                    xmlWithMessages = false;
                    xmlMinimal = true;
                }
                else throw new java.lang.IllegalArgumentException("Unknown option: -xml:" + optionExtraPart);
            }
        }
        else if (option.equals("-emacs")) {
            bugReporterType = EMACS_REPORTER;
        }
        else if (option.equals("-relaxed")) {
            relaxedReportingMode = true;
        }
        else if (option.equals("-train")) {
            trainingOutputDir =  !optionExtraPart.equals("") ? optionExtraPart : ".";
        }
        else if (option.equals("-useTraining")) {
            trainingInputDir =  !optionExtraPart.equals("") ? optionExtraPart : ".";
        }
        else if (option.equals("-html")) {
            bugReporterType = HTML_REPORTER;
            if ( !optionExtraPart.equals("")) {
                stylesheet = optionExtraPart;
            }
            else {
                stylesheet = "default.xsl";
            }
        }
        else if (option.equals("-xdocs")) {
            bugReporterType = XDOCS_REPORTER;
        }
        else if (option.equals("-applySuppression")) {
            applySuppression = true;
        }
        else if (option.equals("-quiet")) {
            quiet = true;
        }
        else if (option.equals("-nested")) {
            scanNestedArchives = optionExtraPart.equals("") || java.lang.Boolean.valueOf(optionExtraPart).booleanValue();
        }
        else if (option.equals("-exitcode")) {
            setExitCode = true;
        }
        else if (option.equals("-auxclasspathFromInput")) {
            try {
                java.io.BufferedReader in = edu.umd.cs.findbugs.charsets.UTF8.bufferedReader(java.lang.System.in);
                while (true) {
                    java.lang.String s = in.readLine();
                    if (s == null) break;
                    this.addAuxClassPathEntries(s);
                }
                in.close();
            }
            catch (java.io.IOException e){
                throw new java.lang.RuntimeException(e);
            }
        }
        else if (option.equals("-noClassOk")) {
            noClassOk = true;
        }
        else if (option.equals("-xargs")) {
            xargs = true;
        }
        else if (option.equals("-justListOptions")) {
            throw new java.lang.RuntimeException("textui options are: " + parsedOptions);
        }
        else if (option.equals("-printConfiguration")) {
            printConfiguration = true;
        }
        else if (option.equals("-version")) {
            printVersion = true;
        }
        else {
            if (DEBUG) {
                java.lang.System.out.println("XXX: " + option);
            }
            super.handleOption(option,optionExtraPart);
        }
    }
    protected java.io.File outputFile;
    protected void handleOptionWithArgument(java.lang.String option, java.lang.String argument) throws java.io.IOException {
        parsedOptions.put(option,argument);
        if (DEBUG) {
            java.lang.System.out.println("option " + option + " is " + argument);
        }
        if (option.equals("-outputFile") || option.equals("-output")) {
            if (outputFile != null) throw new java.lang.IllegalArgumentException("output set twice; to " + outputFile + " and to " + argument);
            outputFile = new java.io.File(argument);
            java.lang.String fileName = outputFile.getName();
            java.lang.String extension = edu.umd.cs.findbugs.util.Util.getFileExtensionIgnoringGz(outputFile);
            if (bugReporterType == PRINTING_REPORTER && (extension.equals("xml") || extension.equals("fba"))) bugReporterType = XML_REPORTER;
            try {
                java.io.OutputStream oStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(outputFile));
                if (fileName.endsWith(".gz")) oStream = new java.util.zip.GZIPOutputStream(oStream);
                outputStream = edu.umd.cs.findbugs.charsets.UTF8.printStream(oStream);
            }
            catch (java.io.IOException e){
                java.lang.System.err.println("Couldn't open " + outputFile + " for output: " + e.toString());
                java.lang.System.exit(1);
            }
        }
        else if (option.equals("-cloud")) project.setCloudId(argument);
        else if (option.equals("-cloudProperty")) {
            int e = argument.indexOf('\u003d');
            if (e ==  -1) throw new java.lang.IllegalArgumentException("Bad cloud property: " + argument);
            java.lang.String key = argument.substring(0,e);
            java.lang.String value = argument.substring(e + 1);
            project.getCloudProperties().setProperty(key,value);
        }
        else if (option.equals("-bugReporters")) {
            for (java.lang.String s : argument.split(",")){
                if (s.charAt(0) == '\u002d') disabledBugReporterDecorators.add(s.substring(1));
                else if (s.charAt(0) == '\u002b') enabledBugReporterDecorators.add(s.substring(1));
                else enabledBugReporterDecorators.add(s);
            }
;
        }
        else if (option.equals("-maxRank")) {
            this.rankThreshold = java.lang.Integer.parseInt(argument);
        }
        else if (option.equals("-projectName")) {
            this.projectName = argument;
        }
        else if (option.equals("-release")) {
            this.releaseName = argument;
        }
        else if (option.equals("-redoAnalysis")) {
            redoAnalysisFile = argument;
        }
        else if (option.equals("-sourceInfo")) {
            sourceInfoFile = argument;
        }
        else if (option.equals("-visitors") || option.equals("-omitVisitors")) {
            boolean omit = option.equals("-omitVisitors");
            if ( !omit) {
                this.getUserPreferences().enableAllDetectors(false);
            }
// Explicitly enable or disable the selected detectors.
            java.util.StringTokenizer tok = new java.util.StringTokenizer(argument, ",");
            while (tok.hasMoreTokens()) {
                java.lang.String visitorName = tok.nextToken().trim();
                edu.umd.cs.findbugs.DetectorFactory factory = edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getFactory(visitorName);
                if (factory == null) throw new java.lang.IllegalArgumentException("Unknown detector: " + visitorName);
                this.getUserPreferences().enableDetector(factory, !omit);
            }
        }
        else if (option.equals("-chooseVisitors")) {
            this.choose(argument,"Detector choices",new edu.umd.cs.findbugs.TextUICommandLine.Chooser() {
                public void choose(boolean enabled, java.lang.String what) {
                    edu.umd.cs.findbugs.DetectorFactory factory = edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getFactory(what);
                    if (factory == null) throw new java.lang.IllegalArgumentException("Unknown detector: " + what);
                    if (edu.umd.cs.findbugs.FindBugs.DEBUG) {
                        java.lang.System.err.println("Detector " + factory.getShortName() + " " + (enabled ? "enabled" : "disabled") + ", userPreferences=" + java.lang.System.identityHashCode(edu.umd.cs.findbugs.TextUICommandLine.this.getUserPreferences()));
                    }
                    edu.umd.cs.findbugs.TextUICommandLine.this.getUserPreferences().enableDetector(factory,enabled);
                }
            });
        }
        else if (option.equals("-choosePlugins")) {
            this.choose(argument,"Plugin choices",new edu.umd.cs.findbugs.TextUICommandLine.Chooser() {
                public void choose(boolean enabled, java.lang.String what) {
                    edu.umd.cs.findbugs.Plugin plugin = edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getPluginById(what);
                    if (plugin == null) throw new java.lang.IllegalArgumentException("Unknown plugin: " + what);
                    plugin.setGloballyEnabled(enabled);
                }
            });
        }
        else if (option.equals("-adjustPriority")) {
// Selectively raise or lower the priority of warnings
// produced by specified detectors.
            java.util.StringTokenizer tok = new java.util.StringTokenizer(argument, ",");
            while (tok.hasMoreTokens()) {
                java.lang.String token = tok.nextToken();
                int eq = token.indexOf('\u003d');
                if (eq < 0) throw new java.lang.IllegalArgumentException("Illegal priority adjustment: " + token);
                java.lang.String adjustmentTarget = token.substring(0,eq);
                java.lang.String adjustment = token.substring(eq + 1);
                int adjustmentAmount;
                if (adjustment.equals("raise")) adjustmentAmount =  -1;
                else if (adjustment.equals("lower")) adjustmentAmount =  +1;
                else if (adjustment.equals("suppress")) adjustmentAmount =  +100;
                else throw new java.lang.IllegalArgumentException("Illegal priority adjustment value: " + adjustment);
                edu.umd.cs.findbugs.DetectorFactory factory = edu.umd.cs.findbugs.DetectorFactoryCollection.instance().getFactory(adjustmentTarget);
                if (factory != null) factory.setPriorityAdjustment(adjustmentAmount);
                else {
//
                    edu.umd.cs.findbugs.DetectorFactoryCollection i18n = edu.umd.cs.findbugs.DetectorFactoryCollection.instance();
                    edu.umd.cs.findbugs.BugPattern pattern = i18n.lookupBugPattern(adjustmentTarget);
                    if (pattern == null) throw new java.lang.IllegalArgumentException("Unknown detector: " + adjustmentTarget);
                    pattern.adjustPriority(adjustmentAmount);
                }
            }
        }
        else if (option.equals("-bugCategories")) {
            this.bugCategorySet = edu.umd.cs.findbugs.FindBugs.handleBugCategories(argument);
        }
        else if (option.equals("-onlyAnalyze")) {
// The argument is a comma-separated list of classes and packages
// to select to analyze. (If a list item ends with ".*",
// it specifies a package, otherwise it's a class.)
            java.util.StringTokenizer tok = new java.util.StringTokenizer(argument, ",");
            while (tok.hasMoreTokens()) {
                java.lang.String item = tok.nextToken();
                if (item.endsWith(".-")) classScreener.addAllowedPrefix(item.substring(0,item.length() - 1));
                else if (item.endsWith(".*")) classScreener.addAllowedPackage(item.substring(0,item.length() - 1));
                else classScreener.addAllowedClass(item);
            }
        }
        else if (option.equals("-exclude")) {
            project.getConfiguration().getExcludeFilterFiles().put(argument,true);
        }
        else if (option.equals("-excludeBugs")) {
            project.getConfiguration().getExcludeBugsFiles().put(argument,true);
        }
        else if (option.equals("-include")) {
            project.getConfiguration().getIncludeFilterFiles().put(argument,true);
        }
        else if (option.equals("-auxclasspath")) {
            this.addAuxClassPathEntries(argument);
        }
        else if (option.equals("-sourcepath")) {
            java.util.StringTokenizer tok = new java.util.StringTokenizer(argument, java.io.File.pathSeparator);
            while (tok.hasMoreTokens()) project.addSourceDir(new java.io.File(tok.nextToken()).getAbsolutePath());
        }
        else {
            super.handleOptionWithArgument(option,argument);
        }
    }
/**
     * Parse the argument as auxclasspath entries and add them
     *
     * @param argument
     */
    private void addAuxClassPathEntries(java.lang.String argument) {
        java.util.StringTokenizer tok = new java.util.StringTokenizer(argument, java.io.File.pathSeparator);
        while (tok.hasMoreTokens()) project.addAuxClasspathEntry(tok.nextToken());
    }
/**
     * Common handling code for -chooseVisitors and -choosePlugins options.
     *
     * @param argument
     *            the list of visitors or plugins to be chosen
     * @param desc
     *            String describing what is being chosen
     * @param chooser
     *            callback object to selectively choose list members
     */
    private void choose(java.lang.String argument, java.lang.String desc, edu.umd.cs.findbugs.TextUICommandLine.Chooser chooser) {
        java.util.StringTokenizer tok = new java.util.StringTokenizer(argument, ",");
        while (tok.hasMoreTokens()) {
            java.lang.String what = tok.nextToken().trim();
            if ( !what.startsWith("+") &&  !what.startsWith("-")) throw new java.lang.IllegalArgumentException(desc + " must start with " + "\"+\" or \"-\" (saw " + what + ")");
            boolean enabled = what.startsWith("+");
            chooser.choose(enabled,what.substring(1));
        }
    }
    public void configureEngine(edu.umd.cs.findbugs.IFindBugsEngine findBugs) throws edu.umd.cs.findbugs.filter.FilterException, java.io.IOException {
        findBugs.setDetectorFactoryCollection(edu.umd.cs.findbugs.DetectorFactoryCollection.instance());
// Load plugins
// Set the DetectorFactoryCollection (that has been configured
// by command line parsing)
        if (redoAnalysisFile != null) {
            edu.umd.cs.findbugs.SortedBugCollection bugs = new edu.umd.cs.findbugs.SortedBugCollection();
            try {
                bugs.readXML(redoAnalysisFile);
            }
            catch (org.dom4j.DocumentException e){
                java.io.IOException ioe = new java.io.IOException("Unable to parse " + redoAnalysisFile);
                ioe.initCause(e);
                throw ioe;
            }
            project = bugs.getProject().duplicate();
        }
        edu.umd.cs.findbugs.TextUIBugReporter textuiBugReporter;
        switch(bugReporterType){
            case PRINTING_REPORTER:{
                textuiBugReporter = new edu.umd.cs.findbugs.PrintingBugReporter();
                break;
            }
            case SORTING_REPORTER:{
                textuiBugReporter = new edu.umd.cs.findbugs.SortingBugReporter();
                break;
            }
            case XML_REPORTER:{
                {
                    edu.umd.cs.findbugs.XMLBugReporter xmlBugReporter = new edu.umd.cs.findbugs.XMLBugReporter(project);
                    xmlBugReporter.setAddMessages(xmlWithMessages);
                    xmlBugReporter.setMinimalXML(xmlMinimal);
                    textuiBugReporter = xmlBugReporter;
                }
                break;
            }
            case EMACS_REPORTER:{
                textuiBugReporter = new edu.umd.cs.findbugs.EmacsBugReporter();
                break;
            }
            case HTML_REPORTER:{
                textuiBugReporter = new edu.umd.cs.findbugs.HTMLBugReporter(project, stylesheet);
                break;
            }
            case XDOCS_REPORTER:{
                textuiBugReporter = new edu.umd.cs.findbugs.XDocsBugReporter(project);
                break;
            }
            default:{
                throw new java.lang.IllegalStateException();
            }
        }
        if (quiet) textuiBugReporter.setErrorVerbosity(edu.umd.cs.findbugs.BugReporter.SILENT);
        textuiBugReporter.setPriorityThreshold(priorityThreshold);
        textuiBugReporter.setRankThreshold(rankThreshold);
        textuiBugReporter.setUseLongBugCodes(useLongBugCodes);
        findBugs.setRankThreshold(rankThreshold);
        if (outputStream != null) textuiBugReporter.setOutputStream(outputStream);
        edu.umd.cs.findbugs.BugReporter bugReporter = textuiBugReporter;
        if (bugCategorySet != null) {
            bugReporter = new edu.umd.cs.findbugs.CategoryFilteringBugReporter(bugReporter, bugCategorySet);
        }
        findBugs.setBugReporter(bugReporter);
        findBugs.setProject(project);
        if (showProgress) {
            findBugs.setProgressCallback(new edu.umd.cs.findbugs.TextUIProgressCallback(java.lang.System.out));
        }
        findBugs.setUserPreferences(this.getUserPreferences());
        findBugs.setClassScreener(classScreener);
        findBugs.setRelaxedReportingMode(relaxedReportingMode);
        findBugs.setAbridgedMessages(xmlWithAbridgedMessages);
        if (trainingOutputDir != null) {
            findBugs.enableTrainingOutput(trainingOutputDir);
        }
        if (trainingInputDir != null) {
            findBugs.enableTrainingInput(trainingInputDir);
        }
        if (sourceInfoFile != null) {
            findBugs.setSourceInfoFile(sourceInfoFile);
        }
        findBugs.setAnalysisFeatureSettings(settingList);
        findBugs.setReleaseName(releaseName);
        findBugs.setProjectName(projectName);
        findBugs.setScanNestedArchives(scanNestedArchives);
        findBugs.setNoClassOk(noClassOk);
        findBugs.setBugReporterDecorators(enabledBugReporterDecorators,disabledBugReporterDecorators);
        if (applySuppression) {
            findBugs.setApplySuppression(true);
        }
        findBugs.finishSettings();
    }
/**
     * Handle -xargs command line option by reading jar file names from standard
     * input and adding them to the project.
     *
     * @throws IOException
     */
    public void handleXArgs() throws java.io.IOException {
        if (this.getXargs()) {
            java.io.BufferedReader in = edu.umd.cs.findbugs.charsets.UTF8.bufferedReader(java.lang.System.in);
            try {
                while (true) {
                    java.lang.String s = in.readLine();
                    if (s == null) break;
                    project.addFile(s);
                }
            }
            finally {
                edu.umd.cs.findbugs.util.Util.closeSilently(in);
            }
        }
    }
/**
     * @return Returns the userPreferences.
     */
    private edu.umd.cs.findbugs.config.UserPreferences getUserPreferences() {
        return project.getConfiguration();
    }
}
