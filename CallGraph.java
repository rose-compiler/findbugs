/*
 * FindBugs - Find bugs in Java programs
 * Copyright (C) 2003,2004 University of Maryland
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package edu.umd.cs.findbugs;
import edu.umd.cs.findbugs.*;
import java.util.IdentityHashMap;
import org.apache.bcel.classfile.Method;
import edu.umd.cs.findbugs.graph.AbstractGraph;
public class CallGraph extends edu.umd.cs.findbugs.graph.AbstractGraph<edu.umd.cs.findbugs.CallGraphEdge, edu.umd.cs.findbugs.CallGraphNode> {
    private java.util.IdentityHashMap<org.apache.bcel.classfile.Method, edu.umd.cs.findbugs.CallGraphNode> methodToNodeMap;
    public CallGraph() {
        super();
        this.methodToNodeMap = new java.util.IdentityHashMap<org.apache.bcel.classfile.Method, edu.umd.cs.findbugs.CallGraphNode>();
    }
    public edu.umd.cs.findbugs.CallGraphEdge createEdge(edu.umd.cs.findbugs.CallGraphNode source, edu.umd.cs.findbugs.CallGraphNode target, edu.umd.cs.findbugs.CallSite callSite) {
        edu.umd.cs.findbugs.CallGraphEdge edge = this.createEdge(source,target);
        edge.setCallSite(callSite);
        return edge;
    }
    public edu.umd.cs.findbugs.CallGraphNode addNode(org.apache.bcel.classfile.Method method) {
        edu.umd.cs.findbugs.CallGraphNode node = new edu.umd.cs.findbugs.CallGraphNode();
        this.addVertex(node);
        node.setMethod(method);
        methodToNodeMap.put(method,node);
        return node;
    }
    public edu.umd.cs.findbugs.CallGraphNode getNodeForMethod(org.apache.bcel.classfile.Method method) {
        return methodToNodeMap.get(method);
    }
    protected edu.umd.cs.findbugs.CallGraphEdge allocateEdge(edu.umd.cs.findbugs.CallGraphNode source, edu.umd.cs.findbugs.CallGraphNode target) {
        return new edu.umd.cs.findbugs.CallGraphEdge(source, target);
    }
}
// vim:ts=4
